 <div class="your-profile">
  <div class="ui-block-title ui-block-title-small">
    <h6 class="title">Your PROFILE</h6>
  </div>

  <div id="accordion" role="tablist" aria-multiselectable="true">
    <div class="card">
      <div class="card-header" role="tab" id="headingOne">
        <h6 class="mb-0">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            Profile Settings
            <svg class="olymp-dropdown-arrow-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon')); ?>"></use></svg>
          </a>
        </h6>
      </div>

      <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
        <ul class="your-profile-menu">
          <li>
            <a href="<?php echo e(route('profile')); ?>">Personal Information</a>
          </li>
          <li>
            <a href="<?php echo e(route('change-password')); ?>">Change Password</a>
          </li>
          <li>
            <a href="<?php echo e(route('hobbies-interest')); ?>">Hobbies and Interests</a>
          </li>
          <li>
            <a href="<?php echo e(route('education')); ?>">Education and Employement</a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="ui-block-title">
    <a href="#" class="h6 title">Notifications</a>
    <a href="#" class="items-round-little bg-primary">8</a>
  </div>
  <div class="ui-block-title">
    <a href="#" class="h6 title">Friend Requests</a>
    <a href="#" class="items-round-little bg-blue">4</a>
  </div>
</div>