<?php $__env->startSection('title'); ?>

    <?php if(!empty(Session::get('Profile - Timeline'))): ?> <?php echo e(Session::get('Profile - Timeline')); ?> <?php else: ?> Profile - Timeline <?php endif; ?>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('main-content'); ?>



<!-- Top Header-Profile -->



<?php echo $top_header_profile; ?>




<!-- ... end Top Header-Profile -->



<div class="container">

  <div class="row">



    <!-- Main Content -->



    <div class="col col-xl-9 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
      <div id="newsfeed-items-grid">
        <div class="ui-block">
            <?php echo $post_view; ?>

        </div>
      </div>



      <!-- <a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid"><svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg></a>

        <svg class="olymp-three-dots-icon">

          <use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use>

        </svg>

      </a> -->

    </div>



    <!-- ... end Main Content -->





    <!-- Left Sidebar -->



    <div class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">



      <div class="ui-block">

        <div class="ui-block-title">

          <h6 class="title">Profile Intro</h6>

        </div>

        <div class="ui-block-content">



          <!-- W-Personal-Info -->

          <ul class="widget w-personal-info item-block">

            <li>

              <span class="title">About Me:</span>

              <span class="text">Hi, I’m Yogesh, I’m 25 and I work as a Maths and Chemistry Tutor.</span>

            </li>

            <li>

              <span class="title">Education:</span>

              <span class="text">Bachelor Of Engg. (Computer Engg). Pass out Year is 2015.</span>

            </li>

            <li>

              <span class="title">Experience:</span>

              <span class="text">I have 4+ years of experience in teaching.</span>

            </li>

          </ul>

        </div>

      </div>

    </div>

    <!-- .. end W-Latest-Video -->



    <!-- ... end Left Sidebar -->



  </div>

</div>





<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>

<script type="text/javascript">



  // $(document).ready(function(){

  //   var id = $("select[name=state]").val();

  //       var selected = $('#dist_selected').val();

  //       $.ajax({

  //           type: "GET",

  //           url: "<?php echo e(route('selectDistrict')); ?>",

  //           data: {id: id,selected:selected},

  //           cache: false,

  //           success: function (data) {

  //               $('#district').html(data);

  //           }

  //       });

  //   });    

    function isNumberKey(evt)

    {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 32 && (charCode < 46 || charCode > 57) || (charCode == 32)) {

            alert("Please enter only digits");

            return false;

        }

        return true;

    }



$("input[name='phone_number']").on("keyup paste", function () {

            // Remove invalid chars from the input

            var input = this.value.replace(/[^0-9\(\)\s\-]/g, "");

            var inputlen = input.length;

            // Get just the numbers in the input

            var numbers = this.value.replace(/\D/g, '');

            var numberslen = numbers.length;

            // Value to store the masked input

            var newval = "";

            

            $(this).val(numbers.substring(0, 14));

        });



    // function changeState(id) {

    //     $.ajax({

    //         type: "GET",

    //         url: "<?php echo e(route('selectDistrict')); ?>",

    //         data: {id: id},

    //         cache: false,

    //         success: function (data) {

    //             $('#district').html(data);

    //         }

    //     });             

    // }

</script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('web.master_layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>