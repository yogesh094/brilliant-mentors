<!-- Fixed Sidebar Left -->


<?php 
$user_profile_url = url("user-profile-timeline/")."/".Auth::user()->id;
?>
<div class="fixed-sidebar">

    <div class="fixed-sidebar-left sidebar--small" id="sidebar-left">



        <a href="#" class="logo">

            <div class="img-wrap">

                <img src="<?php echo e(asset('resources/web-assets/img/logo.png')); ?>" alt="Brilliant Mentors">

            </div>

        </a>



        <div class="mCustomScrollbar" data-mcs-theme="dark">

            <ul class="left-menu">

                <li>

                    <a href="#" class="js-sidebar-open">

                        <svg class="olymp-menu-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="OPEN MENU">

                            <use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-menu-icon')); ?>">

                                <svg id="olymp-menu-icon" viewBox="0 0 41 32" width="100%" height="100%">

                                    <title>menu-icon</title>

                                    <path d="M4.571 0h-4.571v4.571h4.571v-4.571zM9.143 0v4.571h32v-4.571h-32zM13.714 13.714h-13.714v4.571h13.714v-4.571zM18.286 13.714v4.571h4.571v-4.571h-4.571zM27.429 18.286h13.714v-4.571h-13.714v4.571zM0 32h32v-4.569h-32v4.569zM36.571 32h4.571v-4.569h-4.571v4.569z"></path>

                                </svg>

                            </use>

                        </svg>

                    </a>

                </li>

                <li>

                    <a href="<?php echo e(url('/news-feed')); ?>">

                        <svg class="olymp-newsfeed-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="NEWSFEED"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-newsfeed-icon')); ?>">

                            <svg id="olymp-newsfeed-icon" viewBox="0 0 29 32" width="100%" height="100%">

                                <title>newsfeed-icon</title>

                                <path d="M6.4 25.602v-3.202h9.6v3.202h-9.6zM6.4 16h16v3.2h-16v-3.2zM22.4 25.602v6.398h-22.4v-28.8c0-1.766 1.434-3.2 3.2-3.2h22.4c1.766 0 3.2 1.434 3.2 3.2v9.6h-25.6v16.002h16v-6.402h9.6v3.202h-6.4zM25.6 9.6v-6.4h-22.4v6.4h22.4zM8 8h-3.2v-3.2h3.2v3.2zM12.8 8h-3.2v-3.2h3.2v3.2zM17.6 8h-3.2v-3.2h3.2v3.2zM25.6 16h3.2v3.2h-3.2v-3.2z"></path>

                            </svg>

                        </use></svg>

                    </a>

                </li>

                <li>

                    <a href="#">

                        <svg class="olymp-happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="FRIEND GROUPS"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-faces-icon')); ?>">

                            <svg id="olymp-happy-faces-icon" viewBox="0 0 33 32" width="100%" height="100%">

                            <title>happy-faces-icon</title>

                            <path d="M13.329 5.357c-7.364 0-13.333 5.965-13.333 13.325 0 5.971 3.929 11.023 9.344 12.716v-2.823c-3.908-1.584-6.667-5.409-6.667-9.885 0-5.889 4.776-10.665 10.667-10.665s10.667 4.776 10.667 10.665c0 4.476-2.759 8.303-6.667 9.885v2.815c5.404-1.7 9.323-6.745 9.323-12.708 0-7.36-5.969-13.325-13.333-13.325zM9.34 16.025h-2.667v2.665h2.667v-2.665zM17.34 16.025v2.665h2.667v-2.665h-2.667zM9.328 21.495c0 1.395 1.788 2.527 3.995 2.527s3.995-1.132 3.995-2.527v-0.135h-7.989v0.135zM12.007 32.025h2.667v-2.668h-2.667v2.668zM13.805 2.691h8.868c0 0 1.495 0.417 2.183 0.697l2.397-1.797c-1.747-0.993-3.761-1.565-5.913-1.565-2.856 0-5.475 1.001-7.535 2.665zM32.472 7.56l-2.131 2.129c0.195 0.751 0.331 1.523 0.331 2.336 0 1.756-0.488 3.392-1.333 4.789v4.14c2.452-2.195 4-5.38 4-8.931 0.001-1.58-0.311-3.083-0.867-4.464zM30.673 4.025h-2.667v2.667h2.667v-2.667z"></path>

                            </svg>

                        </use></svg>

                    </a>

                </li>

            </ul>

        </div>

    </div>



    <div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1">

        <a href="#" class="logo">

            <div class="img-wrap">

                <img src="<?php echo e(asset('resources/web-assets/img/logo.png')); ?>" alt="Brilliant Mentors">

            </div>

            <div class="title-block">

                <h6 class="logo-title">Brilliant Mentors</h6>

            </div>

        </a>



        <div class="mCustomScrollbar" data-mcs-theme="dark">

            <ul class="left-menu">

                <li>

                    <a href="#" class="js-sidebar-open">

                        <svg class="olymp-close-icon left-menu-icon">

                            <use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-close-icon')); ?>">

                                <svg id="olymp-close-icon" viewBox="0 0 32 32" width="100%" height="100%">

                                    <title>close-icon</title>

                                    <path d="M14.222 17.778h3.556v-3.556h-3.556v3.556zM31.084 3.429l-2.514-2.514-10.057 10.057 2.514 2.514 10.057-10.057zM0.916 28.571l2.514 2.514 10.057-10.055-2.516-2.514-10.055 10.055zM18.514 21.029l10.057 10.055 2.514-2.514-10.057-10.055-2.514 2.514zM0.916 3.431l10.057 10.055 2.516-2.514-10.059-10.057-2.514 2.516z"></path>

                                </svg>

                            </use>

                        </svg>

                        <span class="left-menu-title">Collapse Menu</span>

                    </a>

                </li>

                <li>

                    <a href="#">

                        <svg class="olymp-newsfeed-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Timeline">

                            <use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-newsfeed-icon')); ?>">

                                <svg id="olymp-menu-icon" viewBox="0 0 41 32" width="100%" height="100%">

                                    <title>menu-icon</title>

                                    <path d="M4.571 0h-4.571v4.571h4.571v-4.571zM9.143 0v4.571h32v-4.571h-32zM13.714 13.714h-13.714v4.571h13.714v-4.571zM18.286 13.714v4.571h4.571v-4.571h-4.571zM27.429 18.286h13.714v-4.571h-13.714v4.571zM0 32h32v-4.569h-32v4.569zM36.571 32h4.571v-4.569h-4.571v4.569z"></path>

                                </svg>

                            </use>

                        </svg>

                        <span class="left-menu-title">Timeline</span>

                    </a>

                </li>

                <li>

                    <a href="#">

                        <svg class="olymp-happy-faces-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="Connections">

                            <use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-faces-icon')); ?>">

                                <svg id="olymp-happy-faces-icon" viewBox="0 0 33 32" width="100%" height="100%">

                                    <title>happy-faces-icon</title>

                                    <path d="M13.329 5.357c-7.364 0-13.333 5.965-13.333 13.325 0 5.971 3.929 11.023 9.344 12.716v-2.823c-3.908-1.584-6.667-5.409-6.667-9.885 0-5.889 4.776-10.665 10.667-10.665s10.667 4.776 10.667 10.665c0 4.476-2.759 8.303-6.667 9.885v2.815c5.404-1.7 9.323-6.745 9.323-12.708 0-7.36-5.969-13.325-13.333-13.325zM9.34 16.025h-2.667v2.665h2.667v-2.665zM17.34 16.025v2.665h2.667v-2.665h-2.667zM9.328 21.495c0 1.395 1.788 2.527 3.995 2.527s3.995-1.132 3.995-2.527v-0.135h-7.989v0.135zM12.007 32.025h2.667v-2.668h-2.667v2.668zM13.805 2.691h8.868c0 0 1.495 0.417 2.183 0.697l2.397-1.797c-1.747-0.993-3.761-1.565-5.913-1.565-2.856 0-5.475 1.001-7.535 2.665zM32.472 7.56l-2.131 2.129c0.195 0.751 0.331 1.523 0.331 2.336 0 1.756-0.488 3.392-1.333 4.789v4.14c2.452-2.195 4-5.38 4-8.931 0.001-1.58-0.311-3.083-0.867-4.464zM30.673 4.025h-2.667v2.667h2.667v-2.667z"></path>

                                </svg>

                            </use>

                        </svg>

                        <span class="left-menu-title">Connections</span>

                    </a>

                </li>

            </ul>



            <div class="profile-completion">



                <div class="skills-item">

                    <div class="skills-item-info">

                        <span class="skills-item-title">Profile Completion</span>

                        <span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="76" data-from="0"></span><span class="units">76%</span></span>

                    </div>

                    <div class="skills-item-meter">

                        <span class="skills-item-meter-active bg-primary" style="width: 76%"></span>

                    </div>

                </div>



                <span>Complete <a href="#">your profile</a> so people can know more about you!</span>



            </div>

        </div>

    </div>

</div>



<!-- ... end Fixed Sidebar Left -->





<!-- Fixed Sidebar Left -->



<div class="fixed-sidebar fixed-sidebar-responsive">



    <div class="fixed-sidebar-left sidebar--small" id="sidebar-left-responsive">

        <a href="#" class="logo js-sidebar-open">

            <img src="<?php echo e(asset('resources/web-assets/img/logo.png')); ?>" alt="Brilliant Mentors">

        </a>



    </div>



    <div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1-responsive">

        <a href="#" class="logo">

            <div class="img-wrap">

                <img src="<?php echo e(asset('resources/web-assets/img/logo.png')); ?>" alt="Brilliant Mentors">

            </div>

            <div class="title-block">

                <h6 class="logo-title">Brilliant Mentors</h6>

            </div>

        </a>



        <div class="mCustomScrollbar" data-mcs-theme="dark">



            <div class="control-block">

                <div class="author-page author vcard inline-items">

                    <div class="author-thumb">

                        <img alt="author" src="<?php echo e(asset('resources/web-assets/img/author-page.jpg')); ?>" class="avatar">

                        <span class="icon-status online"></span>

                    </div>

                    <a href="#" class="author-name fn">

                        <div class="author-title">

                            <?php echo e(Auth::user()->first_name); ?> <?php echo e(Auth::user()->last_name); ?><svg class="olymp-dropdown-arrow-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon')); ?>">

                            <svg id="olymp-dropdown-arrow-icon" viewBox="0 0 48 32" width="100%" height="100%">

                            <title>dropdown-arrow-icon</title>

                            <path d="M41.888 0.104l-17.952 19.064-17.952-19.064-5.984 6.352 23.936 25.44 23.936-25.44z"></path>

                            </svg>

                        </use></svg>

                        </div>

                        <!-- <span class="author-subtitle">SPACE COWBOY</span> -->

                    </a>

                </div>

            </div>



            <div class="ui-block-title ui-block-title-small">

                <h6 class="title">MAIN SECTIONS</h6>

            </div>



            <ul class="left-menu">

                <li>

                    <a href="#" class="js-sidebar-open">

                        <svg class="olymp-close-icon left-menu-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-close-icon')); ?>"></use></svg>

                        <span class="left-menu-title">Collapse Menu</span>

                    </a>

                </li>

                <li>

                    <a href="<?php echo e(url('/news-feed')); ?>">

                        <svg class="olymp-newsfeed-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="NEWSFEED"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-newsfeed-icon')); ?>"></use></svg>

                        <span class="left-menu-title">Newsfeed</span>

                    </a>

                </li>



                <li>

                    <a href="#">

                        <svg class="olymp-happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="FRIEND GROUPS"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-faces-icon')); ?>"></use></svg>

                        <span class="left-menu-title">Friend Groups</span>

                    </a>

                </li>

            </ul>



            <div class="ui-block-title ui-block-title-small">

                <h6 class="title">YOUR ACCOUNT</h6>

            </div>


            
            <ul class="account-settings">

                <li>

                    <a href="<?php echo e($user_profile_url); ?>">



                        <svg class="olymp-menu-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-menu-icon')); ?>"></use></svg>



                        <span>Profile Timeline</span>

                    </a>

                </li>

                <li>

                    <a href="<?php echo e(url('profile')); ?>">



                        <svg class="olymp-menu-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-menu-icon')); ?>"></use></svg>



                        <span>Profile Settings</span>

                    </a>

                </li>

                <li>

                    <a href="#">

                        <svg class="olymp-logout-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-logout-icon')); ?>">

                            <svg id="olymp-logout-icon" viewBox="0 0 43 32" width="100%" height="100%">

                            <title>logout-icon</title>

                            <path d="M26.667 3.557c4.962 0 9.232 2.91 11.23 7.111h3.838c-2.197-6.212-8.105-10.668-15.068-10.668s-12.873 4.457-15.070 10.667h3.84c1.998-4.199 6.268-7.109 11.23-7.109zM26.667 28.446c-4.962 0-9.232-2.91-11.23-7.111h-3.84c2.199 6.21 8.107 10.665 15.070 10.665 6.962 0 12.871-4.455 15.070-10.665h-3.838c-2 4.201-6.27 7.111-11.232 7.111zM23.111 17.778v-3.556h-16.306l3.252-3.25-2.514-2.514-7.543 7.541 7.543 7.543 2.514-2.514-3.252-3.252h16.306zM39.111 14.224v3.556h3.556v-3.556h-3.556z"></path>

                            </svg>

                        </use></svg>



                        <span>Log Out</span>

                    </a>

                </li>

            </ul>



            <div class="ui-block-title ui-block-title-small">

                <h6 class="title">About Brilliant Mentors</h6>

            </div>



            <ul class="about-olympus">

                <li>

                    <a href="#">

                        <span>Terms and Conditions</span>

                    </a>

                </li>

                <li>

                    <a href="#">

                        <span>FAQs</span>

                    </a>

                </li>

                <li>

                    <a href="#">

                        <span>Careers</span>

                    </a>

                </li>

                <li>

                    <a href="#">

                        <span>Contact</span>

                    </a>

                </li>

            </ul>



        </div>

    </div>

</div>



<!-- ... end Fixed Sidebar Left -->





<!-- Fixed Sidebar Right -->



<div class="fixed-sidebar right">

    <div class="fixed-sidebar-right sidebar--small" id="sidebar-right">



        <div class="mCustomScrollbar" data-mcs-theme="dark">

            <ul class="chat-users">

                <li class="inline-items js-chat-open">

                    <div class="author-thumb">

                        <img alt="author" src="<?php echo e(asset('resources/web-assets/img/avatar67-sm.jpg')); ?>" class="avatar">

                        <span class="icon-status online"></span>

                    </div>

                </li>

            </ul>

        </div>



        <div class="search-friend inline-items">

            <a href="#" class="js-sidebar-open">

                <svg class="olymp-menu-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-menu-icon')); ?>"></use></svg>

            </a>

        </div>



        <a href="#" class="olympus-chat inline-items js-chat-open">

            <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

        </a>



    </div>



    <div class="fixed-sidebar-right sidebar--large" id="sidebar-right-1">



        <div class="mCustomScrollbar" data-mcs-theme="dark">



            <div class="ui-block-title ui-block-title-small">

                <a href="#" class="title">Close Friends</a>

            </div>



            <ul class="chat-users">

                <li class="inline-items js-chat-open">



                    <div class="author-thumb">

                        <img alt="author" src="<?php echo e(asset('resources/web-assets/img/avatar67-sm.jpg')); ?>" class="avatar">

                        <span class="icon-status online"></span>

                    </div>



                    <div class="author-status">

                        <a href="#" class="h6 author-name">Carol Summers</a>

                        <span class="status">ONLINE</span>

                    </div>



                    <div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>



                        <ul class="more-icons">

                            <li>

                                <svg data-toggle="tooltip" data-placement="top" data-original-title="START CONVERSATION" class="olymp-comments-post-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon')); ?>"></use></svg>

                            </li>


<!-- 
                            <li>

                                <svg data-toggle="tooltip" data-placement="top" data-original-title="ADD TO CONVERSATION" class="olymp-add-to-conversation-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-add-to-conversation-icon')); ?>"></use></svg>

                            </li>



                            <li>

                                <svg data-toggle="tooltip" data-placement="top" data-original-title="BLOCK FROM CHAT" class="olymp-block-from-chat-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-block-from-chat-icon')); ?>"></use></svg>

                            </li> -->

                        </ul>



                    </div>



                </li>


            </ul>

        </div>



        <div class="search-friend inline-items">

            <form class="form-group" >

                <input class="form-control" placeholder="Search Friends..." value="" type="text">

            </form>

            <a href="#" class="js-sidebar-open">

                <svg class="olymp-close-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-close-icon')); ?>"></use></svg>

            </a>

        </div>



        <a href="#" class="olympus-chat inline-items js-chat-open">



            <h6 class="olympus-chat-title">BRILLIANT MENTORS CHAT</h6>

            <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

        </a>



    </div>

</div>



<!-- ... end Fixed Sidebar Right -->





<!-- Fixed Sidebar Right-Responsive -->



<div class="fixed-sidebar right fixed-sidebar-responsive">



    <div class="fixed-sidebar-right sidebar--small" id="sidebar-right-responsive">



        <a href="#" class="olympus-chat inline-items js-chat-open">

            <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

        </a>



    </div>



</div>



<!-- ... end Fixed Sidebar Right-Responsive -->





<!-- Header-BP -->



<header class="header" id="site-header">



    <div class="page-title">

        <h6>Brilliant Mentors</h6>

    </div>



    <div class="header-content-wrapper">

        <form class="search-bar w-search notification-list friend-requests">

            <div class="form-group with-button">

                <input class="form-control js-user-search" placeholder="Search here people or pages..." type="text">

                <button>

                    <svg class="olymp-magnifying-glass-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon')); ?>">

                        <svg id="olymp-magnifying-glass-icon" viewBox="0 0 34 32" width="100%" height="100%">

                            <title>magnifying-glass-icon</title>

                            <path d="M20.809 3.57c-4.76-4.76-12.478-4.76-17.239 0s-4.76 12.48 0 17.239c4.76 4.76 12.48 4.76 17.239 0 4.76-4.759 4.76-12.478 0-17.239zM18.654 18.654c-3.57 3.57-9.361 3.57-12.93 0-3.57-3.57-3.57-9.359 0-12.93s9.361-3.57 12.93 0c3.57 3.569 3.57 9.359 0 12.93z"></path>

                            <path d="M24.022 21.907l2.154-2.156 2.157 2.155-2.154 2.156-2.157-2.155z"></path>

                            <path d="M28.34 28.364c-0.596 0.597-1.559 0.597-2.155 0l-6.464-6.464-0.834-0.852 4.3-4.3-1.312-1.314-6.466 6.466 8.62 8.619c1.783 1.783 4.683 1.783 6.464 0 1.783-1.781 1.783-4.681 0-6.464l-2.155 2.155c0.596 0.596 0.594 1.562 0 2.155z"></path>

                        </svg>

                    </use></svg>

                </button>

            </div>

        </form>



        <a href="#" class="link-find-friend">Find Friends</a>



        <div class="control-block">



            <div class="control-icon more has-items">

                <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                <div class="label-avatar bg-blue">0</div>



                <div class="more-dropdown more-with-triangle triangle-top-center">

                    <div class="ui-block-title ui-block-title-small">

                        <h6 class="title">FRIEND REQUESTS</h6>

                    </div>

                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <p class="marginleft15">No Friend Request</p>
                    </div>

                    <!-- <div class="mCustomScrollbar" data-mcs-theme="dark">

                        <ul class="notification-list friend-requests">

                            <li>

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar55-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <a href="#" class="h6 notification-friend">Tamara Romanoff</a>

                                    <span class="chat-message-item">Mutual Friend: Sarah Hetfield</span>

                                </div>

                                <span class="notification-icon">

                                    <a href="#" class="accept-request">

                                        <span class="icon-add without-text">

                                            <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                        </span>

                                    </a>



                                    <a href="#" class="accept-request request-del">

                                        <span class="icon-minus">

                                            <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                        </span>

                                    </a>



                                </span>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                </div>

                            </li>



                            <li>

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar56-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <a href="#" class="h6 notification-friend">Tony Stevens</a>

                                    <span class="chat-message-item">4 Friends in Common</span>

                                </div>

                                <span class="notification-icon">

                                    <a href="#" class="accept-request">

                                        <span class="icon-add without-text">

                                            <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>">

                                                <svg id="olymp-happy-face-icon" viewBox="0 0 32 32" width="100%" height="100%">

                                                <title>happy-face-icon</title>

                                                <path d="M16 0c-8.837 0-16 7.16-16 15.989 0 7.166 4.715 13.227 11.213 15.262v-3.39c-4.69-1.899-8-6.49-8-11.859 0-7.070 5.731-12.802 12.8-12.802s12.8 5.731 12.8 12.802c0 5.37-3.312 9.96-8 11.859v3.378c6.485-2.040 11.187-8.094 11.187-15.25 0-8.829-7.165-15.989-16-15.989zM11.211 12.8h-3.2v3.202h3.2v-3.202zM20.813 12.8v3.202h3.2v-3.202h-3.2zM11.198 19.365c0 1.675 2.146 3.032 4.794 3.032s4.794-1.357 4.794-3.032v-0.16h-9.587v0.16zM14.413 32.002h3.2v-3.2h-3.2v3.2z"></path>

                                                </svg>

                                            </use></svg>

                                        </span>

                                    </a>



                                    <a href="#" class="accept-request request-del">

                                        <span class="icon-minus">

                                            <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>">

                                                <svg id="olymp-chat---messages-icon" viewBox="0 0 40 32" width="100%" height="100%">

                                                    <title>chat---messages-icon</title>

                                                    <path d="M24.381 7.621h-21.333c-1.378 0-3.048 1.606-3.048 3.046v13.716c0 1.443 1.67 3.048 3.048 3.048v4.57l12.19-4.568v-3.051l-9.143 3.051v-3.051h-3.048v-13.714h21.333v16.763c1.378 0 3.048-1.605 3.048-3.048v-13.716c0-1.44-1.67-3.046-3.048-3.046zM18.286 27.432h3.048v-3.048h-3.048v3.048zM6.095 16.763h15.238v-3.046h-15.238v3.046zM6.095 21.336h9.143v-3.048h-9.143v3.048zM15.238 3.051h24.381c0-1.443-1.67-3.049-3.048-3.049h-21.333c-1.378 0-3.048 1.606-3.048 3.049v1.527h3.048v-1.527zM36.571 16.763l-4.571-0.002v3.051l-3.048-1.016v3.301l6.095 2.284v-4.568c0.779 0 1.524 0 1.524 0 1.378 0 3.048-1.606 3.048-3.049v-4.571h-3.048v4.571zM36.571 9.144h3.048v-3.048h-3.048v3.048z"></path>

                                                    </svg>

                                            </use></svg>

                                        </span>

                                    </a>



                                </span>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>">

                                        <svg id="olymp-thunder-icon" viewBox="0 0 26 32" width="100%" height="100%">

                                        <title>thunder-icon</title>

                                        <path d="M25.6 11.198h-8l6.4-11.198-18.669 0.005-5.331 17.597h4.798l-1.598 14.398 4.8-4.458v-4.914l-1.6 1.371 1.6-9.602h-3.2l3.2-11.2h9.6l-4.8 11.2h4.8v4.23l8-7.43zM11.2 22.4h3.2v-3.2h-3.2v3.2z"></path>

                                        </svg>

                                    </use></svg>

                                </div>

                            </li>



                            <li class="accepted">

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar57-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    You and <a href="#" class="h6 notification-friend">Mary Jane Stark</a> just became friends. Write on <a href="#" class="notification-link">her wall</a>.

                                </div>

                                <span class="notification-icon">

                                    <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                </span>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                    <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>">

                                        <svg id="olymp-little-delete" viewBox="0 0 32 32" width="100%" height="100%">

                                        <title>little-delete</title>

                                        <path d="M32 4.149l-3.973-3.979-11.936 11.941-11.941-11.941-3.979 3.979 11.941 11.936-11.941 11.936 3.979 3.979 11.941-11.936 11.936 11.936 3.973-3.979-11.936-11.936z"></path>

                                        </svg>

                                    </use></svg>

                                </div>

                            </li>



                            <li>

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar58-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <a href="#" class="h6 notification-friend">Stagg Clothing</a>

                                    <span class="chat-message-item">9 Friends in Common</span>

                                </div>

                                <span class="notification-icon">

                                    <a href="#" class="accept-request">

                                        <span class="icon-add without-text">

                                            <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                        </span>

                                    </a>



                                    <a href="#" class="accept-request request-del">

                                        <span class="icon-minus">

                                            <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                        </span>

                                    </a>



                                </span>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                </div>

                            </li>



                        </ul>

                    </div> -->



                    <!-- <a href="#" class="view-all bg-blue">Check all your Events</a> -->

                </div>

            </div>



            <div class="control-icon more has-items">

                <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                <div class="label-avatar bg-purple">0</div>



                <div class="more-dropdown more-with-triangle triangle-top-center">

                    <div class="ui-block-title ui-block-title-small">

                        <h6 class="title">Chat / Messages</h6>

                    </div>

                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <p class="marginleft15">No Messages</p>
                    </div>

                    <!-- <div class="mCustomScrollbar" data-mcs-theme="dark">

                        <ul class="notification-list chat-message">

                            <li class="message-unread">

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar59-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <a href="#" class="h6 notification-friend">Diana Jameson</a>

                                    <span class="chat-message-item">Hi James! It’s Diana, I just wanted to let you know that we have to reschedule...</span>

                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>

                                </div>

                                <span class="notification-icon">

                                    <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                                </span>

                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                </div>

                            </li>



                            <li>

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar60-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <a href="#" class="h6 notification-friend">Jake Parker</a>

                                    <span class="chat-message-item">Great, I’ll see you tomorrow!.</span>

                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>

                                </div>

                                <span class="notification-icon">

                                    <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                                </span>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                </div>

                            </li>

                            <li>

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar61-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <a href="#" class="h6 notification-friend">Elaine Dreyfuss</a>

                                    <span class="chat-message-item">We’ll have to check that at the office and see if the client is on board with...</span>

                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 9:56pm</time></span>

                                </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                                    </span>

                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                </div>

                            </li>



                            <li class="chat-group">

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar11-sm.jpg')); ?>" alt="author">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar12-sm.jpg')); ?>" alt="author">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar13-sm.jpg')); ?>" alt="author">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar10-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <a href="#" class="h6 notification-friend">You, Faye, Ed &amp; Jet +3</a>

                                    <span class="last-message-author">Ed:</span>

                                    <span class="chat-message-item">Yeah! Seems fine by me!</span>

                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">March 16th at 10:23am</time></span>

                                </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                                    </span>

                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                </div>

                            </li>

                        </ul>

                    </div> -->



                    <!-- <a href="#" class="view-all bg-purple">View All Messages</a> -->

                </div>

            </div>



            <div class="control-icon more has-items">

                <svg class="olymp-thunder-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-thunder-icon')); ?>"></use></svg>



                <div class="label-avatar bg-primary">0</div>



                <div class="more-dropdown more-with-triangle triangle-top-center">

                    <div class="ui-block-title ui-block-title-small">

                        <h6 class="title">Notifications</h6>

                    </div>

                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <p class="marginleft15">No Notifications</p>
                    </div>

                    <!-- <div class="mCustomScrollbar" data-mcs-theme="dark">

                        <ul class="notification-list">

                            <li>

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar62-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <div><a href="#" class="h6 notification-friend">Mathilda Brinker</a> commented on your new <a href="#" class="notification-link">profile status</a>.</div>

                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>

                                </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-comments-post-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon')); ?>"></use></svg>

                                    </span>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                    <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                                </div>

                            </li>



                            <li class="un-read">

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar63-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <div>You and <a href="#" class="h6 notification-friend">Nicholas Grissom</a> just became friends. Write on <a href="#" class="notification-link">his wall</a>.</div>

                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">9 hours ago</time></span>

                                </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                    </span>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                    <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                                </div>

                            </li>



                            <li class="with-comment-photo">

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar64-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <div><a href="#" class="h6 notification-friend">Sarah Hetfield</a> commented on your <a href="#" class="notification-link">photo</a>.</div>

                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 5:32am</time></span>

                                </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-comments-post-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon')); ?>"></use></svg>

                                    </span>



                                <div class="comment-photo">

                                    <img src="<?php echo e(asset('resources/web-assets/img/comment-photo1.jpg')); ?>" alt="photo">

                                    <span>“She looks incredible in that outfit! We should see each...”</span>

                                </div>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                    <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                                </div>

                            </li>



                            <li>

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar65-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <div><a href="#" class="h6 notification-friend">Green Goo Rock</a> invited you to attend to his event Goo in <a href="#" class="notification-link">Gotham Bar</a>.</div>

                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">March 5th at 6:43pm</time></span>

                                </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                    </span>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                    <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                                </div>

                            </li>



                            <li>

                                <div class="author-thumb">

                                    <img src="<?php echo e(asset('resources/web-assets/img/avatar66-sm.jpg')); ?>" alt="author">

                                </div>

                                <div class="notification-event">

                                    <div><a href="#" class="h6 notification-friend">James Summers</a> commented on your new <a href="#" class="notification-link">profile status</a>.</div>

                                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">March 2nd at 8:29pm</time></span>

                                </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-heart-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-heart-icon')); ?>">

                                            <svg id="olymp-heart-icon" viewBox="0 0 36 32" width="100%" height="100%">

                                            <title>heart-icon</title>

                                            <path d="M23.111 21.333h3.556v3.556h-3.556v-3.556z"></path>

                                            <path d="M32.512 2.997c-2.014-2.011-4.263-3.006-7.006-3.006-2.62 0-5.545 2.089-7.728 4.304-2.254-2.217-5.086-4.295-7.797-4.295-2.652 0-4.99 0.793-6.937 2.738-4.057 4.043-4.057 10.599 0 14.647 1.157 1.157 12.402 13.657 12.402 13.657 0.64 0.638 1.481 0.958 2.32 0.958s1.678-0.32 2.318-0.958l1.863-2.012-2.523-2.507-1.655 1.787c-2.078-2.311-11.095-12.324-12.213-13.442-1.291-1.285-2-2.994-2-4.811 0-1.813 0.709-3.518 2-4.804 1.177-1.175 2.54-1.698 4.425-1.698 0.464 0 2.215 0.236 5.303 3.273l2.533 2.492 2.492-2.532c2.208-2.242 4.201-3.244 5.196-3.244 1.769 0 3.113 0.588 4.496 1.97 1.289 1.284 1.998 2.99 1.998 4.804 0 1.815-0.709 3.522-1.966 4.775-0.087 0.085-0.098 0.094-1.9 2.041l-0.156 0.167 2.523 2.51 0.24-0.26c0 0 1.742-1.881 1.774-1.911 4.055-4.043 4.055-10.603-0.002-14.644z"></path>

                                            </svg>

                                        </use></svg>

                                    </span>



                                <div class="more">

                                    <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                                    <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                                </div>

                            </li>

                        </ul>

                    </div> -->



                    <a href="#" class="view-all bg-primary">View All Notifications</a>

                </div>

            </div>



            <div class="author-page author vcard inline-items more">

                <div class="author-thumb">

                    <img alt="author" src="<?php echo e(asset('resources/web-assets/img/author-page.jpg')); ?>" class="avatar">

                    <span class="icon-status online"></span>

                    <div class="more-dropdown more-with-triangle">

                        <div class="mCustomScrollbar" data-mcs-theme="dark">

                            <div class="ui-block-title ui-block-title-small">

                                <h6 class="title">Your Account</h6>

                            </div>



                            <ul class="account-settings">

                                <li>

                                    <a href="<?php echo e($user_profile_url); ?>">



                                        <svg class="olymp-menu-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-menu-icon')); ?>"></use></svg>



                                        <span>Profile Timeline</span>

                                    </a>

                                </li>

                                <li>

                                    <a href="<?php echo e(url('profile')); ?>">



                                        <svg class="olymp-menu-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-menu-icon')); ?>"></use></svg>



                                        <span>Profile Settings</span>

                                    </a>

                                </li>

                                <li>

                                    <a href="<?php echo e(route('logout')); ?>">

                                        <svg class="olytmp-logout-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-logout-icon')); ?>"></use></svg>



                                        <span>Log Out</span>

                                    </a>

                                </li>

                            </ul>

                        </div>



                    </div>

                </div>

                <a href="#" class="author-name fn">

                    <div class="author-title">

                        <?php echo e(Auth::user()->first_name); ?> <?php echo e(Auth::user()->last_name); ?><svg class="olymp-dropdown-arrow-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon')); ?>">

                        <svg id="olymp-dropdown-arrow-icon" viewBox="0 0 48 32" width="100%" height="100%">

                        <title>dropdown-arrow-icon</title>

                        <path d="M41.888 0.104l-17.952 19.064-17.952-19.064-5.984 6.352 23.936 25.44 23.936-25.44z"></path>

                        </svg>

                    </use></svg>

                    </div>

                    <!-- <span class="author-subtitle">SPACE COWBOY</span> -->

                </a>

            </div>



        </div>

    </div>



</header>



<!-- ... end Header-BP -->





<!-- Responsive Header-BP -->



<header class="header header-responsive" id="site-header-responsive">



    <div class="header-content-wrapper">

        <ul class="nav nav-tabs mobile-app-tabs" role="tablist">

            <li class="nav-item">

                <a class="nav-link" data-toggle="tab" href="#request" role="tab">

                    <div class="control-icon has-items">

                        <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                        <div class="label-avatar bg-blue">6</div>

                    </div>

                </a>

            </li>



            <li class="nav-item">

                <a class="nav-link" data-toggle="tab" href="#chat" role="tab">

                    <div class="control-icon has-items">

                        <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                        <div class="label-avatar bg-purple">2</div>

                    </div>

                </a>

            </li>



            <li class="nav-item">

                <a class="nav-link" data-toggle="tab" href="#notification" role="tab">

                    <div class="control-icon has-items">

                        <svg class="olymp-thunder-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-thunder-icon')); ?>"></use></svg>

                        <div class="label-avatar bg-primary">8</div>

                    </div>

                </a>

            </li>



            <li class="nav-item">

                <a class="nav-link" data-toggle="tab" href="#search" role="tab">

                    <svg class="olymp-magnifying-glass-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon')); ?>"></use></svg>

                    <svg class="olymp-close-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-close-icon')); ?>"></use></svg>

                </a>

            </li>

        </ul>

    </div>



    <!-- Tab panes -->

    <div class="tab-content tab-content-responsive">



        <div class="tab-pane " id="request" role="tabpanel">



            <div class="mCustomScrollbar" data-mcs-theme="dark">

                <div class="ui-block-title ui-block-title-small">

                    <h6 class="title">FRIEND REQUESTS</h6>

                    <a href="#">Find Friends</a>

                    <a href="#">Settings</a>

                </div>

                <ul class="notification-list friend-requests">

                    <li>

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar55-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <a href="#" class="h6 notification-friend">Tamara Romanoff</a>

                            <span class="chat-message-item">Mutual Friend: Sarah Hetfield</span>

                        </div>

                                    <span class="notification-icon">

                                        <a href="#" class="accept-request">

                                            <span class="icon-add without-text">

                                                <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                            </span>

                                        </a>



                                        <a href="#" class="accept-request request-del">

                                            <span class="icon-minus">

                                                <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                            </span>

                                        </a>



                                    </span>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                        </div>

                    </li>

                    <li>

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar56-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <a href="#" class="h6 notification-friend">Tony Stevens</a>

                            <span class="chat-message-item">4 Friends in Common</span>

                        </div>

                                    <span class="notification-icon">

                                        <a href="#" class="accept-request">

                                            <span class="icon-add without-text">

                                                <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                            </span>

                                        </a>



                                        <a href="#" class="accept-request request-del">

                                            <span class="icon-minus">

                                                <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                            </span>

                                        </a>



                                    </span>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                        </div>

                    </li>

                    <li class="accepted">

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar57-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            You and <a href="#" class="h6 notification-friend">Mary Jane Stark</a> just became friends. Write on <a href="#" class="notification-link">her wall</a>.

                        </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                    </span>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                            <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                        </div>

                    </li>

                    <li>

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar58-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <a href="#" class="h6 notification-friend">Stagg Clothing</a>

                            <span class="chat-message-item">9 Friends in Common</span>

                        </div>

                                    <span class="notification-icon">

                                        <a href="#" class="accept-request">

                                            <span class="icon-add without-text">

                                                <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                            </span>

                                        </a>



                                        <a href="#" class="accept-request request-del">

                                            <span class="icon-minus">

                                                <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                            </span>

                                        </a>



                                    </span>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                        </div>

                    </li>

                </ul>

                <a href="#" class="view-all bg-blue">Check all your Events</a>

            </div>



        </div>



        <div class="tab-pane " id="chat" role="tabpanel">



            <div class="mCustomScrollbar" data-mcs-theme="dark">

                <div class="ui-block-title ui-block-title-small">

                    <h6 class="title">Chat / Messages</h6>

                    <a href="#">Mark all as read</a>

                    <a href="#">Settings</a>

                </div>



                <ul class="notification-list chat-message">

                    <li class="message-unread">

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar59-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <a href="#" class="h6 notification-friend">Diana Jameson</a>

                            <span class="chat-message-item">Hi James! It’s Diana, I just wanted to let you know that we have to reschedule...</span>

                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>

                        </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                                    </span>

                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                        </div>

                    </li>



                    <li>

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar60-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <a href="#" class="h6 notification-friend">Jake Parker</a>

                            <span class="chat-message-item">Great, I’ll see you tomorrow!.</span>

                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>

                        </div>

                                    <span class="notification-icon">

                                        <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                                    </span>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                        </div>

                    </li>

                    <li>

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar61-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <a href="#" class="h6 notification-friend">Elaine Dreyfuss</a>

                            <span class="chat-message-item">We’ll have to check that at the office and see if the client is on board with...</span>

                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 9:56pm</time></span>

                        </div>

                                        <span class="notification-icon">

                                            <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                                        </span>

                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                        </div>

                    </li>



                    <li class="chat-group">

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar11-sm.jpg')); ?>" alt="author">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar12-sm.jpg')); ?>" alt="author">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar13-sm.jpg')); ?>" alt="author">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar10-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <a href="#" class="h6 notification-friend">You, Faye, Ed &amp; Jet +3</a>

                            <span class="last-message-author">Ed:</span>

                            <span class="chat-message-item">Yeah! Seems fine by me!</span>

                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">March 16th at 10:23am</time></span>

                        </div>

                                        <span class="notification-icon">

                                            <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon')); ?>"></use></svg>

                                        </span>

                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                        </div>

                    </li>

                </ul>



                <a href="#" class="view-all bg-purple">View All Messages</a>

            </div>



        </div>



        <div class="tab-pane " id="notification" role="tabpanel">



            <div class="mCustomScrollbar" data-mcs-theme="dark">

                <div class="ui-block-title ui-block-title-small">

                    <h6 class="title">Notifications</h6>

                    <a href="#">Mark all as read</a>

                    <a href="#">Settings</a>

                </div>



                <ul class="notification-list">

                    <li>

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar62-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <div><a href="#" class="h6 notification-friend">Mathilda Brinker</a> commented on your new <a href="#" class="notification-link">profile status</a>.</div>

                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>

                        </div>

                                        <span class="notification-icon">

                                            <svg class="olymp-comments-post-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon')); ?>"></use></svg>

                                        </span>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                            <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                        </div>

                    </li>



                    <li class="un-read">

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar63-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <div>You and <a href="#" class="h6 notification-friend">Nicholas Grissom</a> just became friends. Write on <a href="#" class="notification-link">his wall</a>.</div>

                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">9 hours ago</time></span>

                        </div>

                                        <span class="notification-icon">

                                            <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                        </span>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                            <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                        </div>

                    </li>



                    <li class="with-comment-photo">

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar64-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <div><a href="#" class="h6 notification-friend">Sarah Hetfield</a> commented on your <a href="#" class="notification-link">photo</a>.</div>

                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 5:32am</time></span>

                        </div>

                                        <span class="notification-icon">

                                            <svg class="olymp-comments-post-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon')); ?>"></use></svg>

                                        </span>



                        <div class="comment-photo">

                            <img src="<?php echo e(asset('resources/web-assets/img/comment-photo1.jpg')); ?>" alt="photo">

                            <span>“She looks incredible in that outfit! We should see each...”</span>

                        </div>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                            <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                        </div>

                    </li>



                    <li>

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar65-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <div><a href="#" class="h6 notification-friend">Green Goo Rock</a> invited you to attend to his event Goo in <a href="#" class="notification-link">Gotham Bar</a>.</div>

                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">March 5th at 6:43pm</time></span>

                        </div>

                                        <span class="notification-icon">

                                            <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon')); ?>"></use></svg>

                                        </span>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                            <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                        </div>

                    </li>



                    <li>

                        <div class="author-thumb">

                            <img src="<?php echo e(asset('resources/web-assets/img/avatar66-sm.jpg')); ?>" alt="author">

                        </div>

                        <div class="notification-event">

                            <div><a href="#" class="h6 notification-friend">James Summers</a> commented on your new <a href="#" class="notification-link">profile status</a>.</div>

                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">March 2nd at 8:29pm</time></span>

                        </div>

                                        <span class="notification-icon">

                                            <svg class="olymp-heart-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-heart-icon')); ?>"></use></svg>

                                        </span>



                        <div class="more">

                            <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use></svg>

                            <svg class="olymp-little-delete"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-little-delete')); ?>"></use></svg>

                        </div>

                    </li>

                </ul>



                <a href="#" class="view-all bg-primary">View All Notifications</a>

            </div>



        </div>



        <div class="tab-pane " id="search" role="tabpanel">





                <form class="search-bar w-search notification-list friend-requests">

                    <div class="form-group with-button">

                        <input class="form-control js-user-search" placeholder="Search here people or pages..." type="text">

                    </div>

                </form>





        </div>



    </div>

    <!-- ... end  Tab panes -->



</header>



<!-- ... end Responsive Header-BP -->

<div class="header-spacer header-spacer-small"></div>