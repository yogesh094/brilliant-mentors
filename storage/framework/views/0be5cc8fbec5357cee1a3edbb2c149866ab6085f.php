
<!-- Main Font -->
<script src="<?php echo e(asset('resources/web-assets/js/webfontloader.min.js')); ?>"></script>

<script>
	WebFont.load({
		google: {
			families: ['Roboto:300,400,500,700:latin']
		}
	});
</script>

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('resources/web-assets/Bootstrap/dist/css/bootstrap-reboot.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('resources/web-assets/Bootstrap/dist/css/bootstrap.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('resources/web-assets/Bootstrap/dist/css/bootstrap-grid.css')); ?>">

<!-- Main Styles CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('resources/web-assets/css/main.min.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('resources/web-assets/css/fonts.min.css')); ?>">
<link href="<?php echo e(asset('resources/web-assets/css/custom.css')); ?> " rel="stylesheet">