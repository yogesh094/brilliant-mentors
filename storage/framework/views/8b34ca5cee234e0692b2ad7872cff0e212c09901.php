<?php 

if(count($comment) > 0){

	$description = isset($comment->comment)?$comment->comment:'';
	$post_time = isset($comment->created_at)?time_ago($comment->created_at):'';
	$first_name = isset($comment->first_name)?time_ago($comment->first_name):'';
	$last_name = isset($comment->last_name)?time_ago($comment->last_name):'';
	$commenter_name = $first_name.' '.$last_name;	
}
?>

<li class="comment-item">
   <div class="post__author author vcard inline-items">
      <img src="<?php echo e(asset('resources/web-assets/img/author-page.jpg')); ?>" alt="author">
      <div class="author-date">
         <a class="h6 post__author-name fn" href="02-ProfilePage.html"><?php echo e($commenter_name); ?></a>
         <div class="post__date">
            <time class="published" datetime="2004-07-24T18:18">
            <?php echo e($post_time); ?>

            </time>
         </div>
      </div>
      <a href="#" class="more">
         <svg class="olymp-three-dots-icon">
            <use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon')); ?>"></use>
         </svg>
      </a>
   </div>
   <p><?php echo e($description); ?></p>
   <a href="#" class="post-add-icon inline-items">
      <svg class="olymp-heart-icon">
         <use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-heart-icon')); ?>"></use>
      </svg>
      <span>3</span>
   </a>
   <span id="comment_<?php echo e($comment->id); ?>" data-postId = "<?php echo e($comment->post_id); ?>" data-commenterId = "<?php echo e($comment->commenter_id); ?>"> </span>
   <a href="#" class="reply">Reply</a>
</li>



