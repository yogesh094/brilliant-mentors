<?php $__env->startSection('title'); ?>
    <?php if(!empty(Session::get('Brilliant Mentors'))): ?> <?php echo e(Session::get('Brilliant Mentors')); ?> <?php else: ?> Brilliant Mentors <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<div class="col-md-12 col-sm-12 col-xs-12">
<?php if(count($errors)): ?>
    <div class="alert alert-danger">
        <strong>Whoops!</strong> <?php echo e($errors->first()); ?>.
        <br/>
    </div>
<?php endif; ?>
</div>
<div class="content-bg-wrap"></div>

<!-- Header Standard Landing  -->

<div class="header--standard header--standard-landing" id="header--standard">
    <div class="container">
        <div class="header--standard-wrap">
            <a href="#" class="logo">
                <div class="img-wrap">
                    <img src="<?php echo e(asset('resources/web-assets/img/logo.png')); ?>" alt="Olympus">
                    <img src="<?php echo e(asset('resources/web-assets/img/logo-colored-small.png')); ?>" alt="Olympus" class="logo-colored">
                </div>
                <div class="title-block">
                    <h6 class="logo-title">Brilliant Mentors</h6>
                    <div class="sub-title">SOCIAL NETWORK</div>
                </div>
            </a>
        </div>
    </div>
</div>

<!-- ... end Header Standard Landing  -->
<div class="header-spacer--standard"></div>

<div class="container">
    <div class="row display-flex">
        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="landing-content">
                <h1>Welcome to the Biggest Social Network in the World</h1>
                <p>We are the best and biggest social network with 5 billion active users all around the world. Share you
                    thoughts, write blog posts, show your favourite music via Stopify, earn badges and much more!
                </p>
                <!-- <a href="#" class="btn btn-md btn-border c-white">Register Now!</a> -->
            </div>
        </div>

        <div class="col col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
            
            <!-- Login-Registration Form  -->
            
            <div class="registration-login-form">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                            <svg class="olymp-login-icon">
                                <use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-login-icon')); ?>">
                                    <svg id="olymp-login-icon" viewBox="0 0 29 32" width="100%" height="100%">
                                        <title>login-icon</title>
                                        <path d="M0 17.443c0 6.515 4.287 12.026 10.195 13.875v-3.081c-4.263-1.728-7.273-5.901-7.273-10.783 0-4.883 3.009-9.056 7.273-10.784v-3.1c-5.908 1.849-10.195 7.36-10.195 13.872zM18.922 3.578v3.092c4.263 1.728 7.273 5.901 7.273 10.783s-3.009 9.056-7.273 10.783v3.071c5.894-1.855 10.169-7.357 10.169-13.863 0-6.503-4.273-12.007-10.169-13.865zM13.104 14.545h2.909v-14.545h-2.909v14.545zM13.104 32h2.909v-2.909h-2.909v2.909z"></path>
                                    </svg>
                                </use>
                            </svg>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile" role="tab">
                            <svg class="olymp-register-icon">
                                <use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-register-icon')); ?>">
                                    <svg id="olymp-register-icon" viewBox="0 0 37 32" width="100%" height="100%">
                                        <title>register-icon</title>
                                        <path d="M16 3.213c3.24 0 6.192 1.214 8.446 3.2h4.346c-2.917-3.888-7.549-6.413-12.781-6.413-7.165 0-13.227 4.714-15.259 11.213h3.387c1.899-4.69 6.491-8 11.861-8zM16 28.813c-5.37 0-9.962-3.31-11.861-8h-3.378c2.040 6.485 8.094 11.187 15.25 11.187 5.222 0 9.842-2.515 12.762-6.387h-4.325c-2.256 1.986-5.208 3.2-8.448 3.2zM32 14.413v-4.8h-3.2v4.8h-4.8v3.2h4.8v4.8h3.2v-4.8h4.8v-3.2h-4.8zM3.2 14.413h-3.2v3.2h3.2v-3.2z"></path>
                                    </svg>
                                </use>
                            </svg>
                        </a>
                    </li>
                </ul>
            
                <!-- Tab panes -->
                <div class="tab-content">

                <?php if($errors->any()): ?>
                    <div class="alert alert-danger">
                        
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                    </div>
                <?php endif; ?>

                <?php if(Session::has('message')): ?>
                    <div class="alert alert-info"><?php echo Session::get('message'); ?></div>
                <?php endif; ?>
                <?php if(Session::has('error')): ?>
                    <div class="alert alert-danger"><?php echo Session::get('error'); ?></div>
                <?php endif; ?>
                <?php if(Session::has('success')): ?>
                    <div class="alert alert-success"><?php echo Session::get('success'); ?></div>
                <?php endif; ?>
                    <div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab">
                        <div class="title h6">Register to Brilliant Mentors</div>
                                            <form class="form content" role="form" method="POST" id="register-form" action="<?php echo e(url('/registration')); ?>">
                            <div class="row">

                                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">

                                    <div class="form-group label-floating is-empty">

                                        <span class="skills-item-title">

                                            <span class="radio">

                                                <label>

                                                    <input type="radio" name="user_type" value ="0" checked>

                                                    Are you Tutor?

                                                </label>

                                            </span>

                                        </span>

                                    </div>

                                </div>

                                <div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">

                                    <div class="form-group label-floating is-empty">

                                        <span class="skills-item-title">

                                            <span class="radio">

                                                <label>

                                                    <input type="radio" name="user_type" value ="1">

                                                    Are you Student?

                                                </label>

                                            </span>

                                        </span>

                                    </div>

                                </div>



                                <div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">

                                    <div class="form-group label-floating is-empty">

                                        <label class="control-label">First Name</label>

                                        <input class="form-control" placeholder="" id="first_name" name = "first_name" type="text" required>

                                        <span class="first_name-err error"></span>
                                    </div>

                                </div>

                                <div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">

                                    <div class="form-group label-floating is-empty">

                                        <label class="control-label">Last Name</label>

                                        <input class="form-control" placeholder="" type="text" name = "last_name" id="last_name" required>

                                        <span class="last_name-err error"></span>
                                    </div>

                                </div>

                                <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">

                                    <div class="form-group label-floating is-empty">

                                        <label class="control-label">Your Email</label>

                                        <input class="form-control" placeholder="" type="email" name = "email" id="email" required>

                                        <span class="email-err error"></span>
                                    </div>

                                    <div class="form-group label-floating is-empty">

                                        <label class="control-label">Your Password</label>

                                        <input class="form-control" placeholder="" type="password" name = "password" id="password" required>

                                        <span class="password-err error"></span>
                                    </div>
                                    <div class="form-group label-floating is-empty">

                                        <label class="control-label">Confirm Password</label>

                                        <input class="form-control" placeholder="" type="password" name = "confirm_password" id="confirm_password" required>

                                        <span class="password-err error"></span>
                                    </div>

                                    

                                    <div class="form-group label-floating is-empty">

                                        <label class="control-label">Mobile Number</label>

                                        <input class="form-control" placeholder="" type="text" name = "phone_number" id="phone_number" required>

                                        <span class="phone_number-err error"></span>
                                    </div>

                            

            

                                    <div class="form-group label-floating is-select">

                                        <label class="control-label">Your Gender</label>

                                        <select class="selectpicker form-control" name="gender">

                                            <option value="MALE" selected>Male</option>

                                            <option value="FEMALE">Female</option>

                                        </select>
                                        <span class="gender-err error"></span>

                                    </div>

                                    <div class="remember">

                                        <div class="checkbox">

                                            <label>

                                                <input name="optionsCheckboxes" type="checkbox" required="">

                                                I accept the <a href="#">Terms and Conditions</a> of the website

                                            </label>

                                        </div>

                                    </div>

            

                                    <button type="submit"  id="register-me" class="btn btn-purple btn-lg full-width">Complete Registration!</button>

                                </div>

                            </div>

                        </form>
                    </div>
            
                    <div class="tab-pane" id="profile" role="tabpanel" data-mh="log-tab">
                        <div class="title h6">Login to your Account</div>
                         <form class="form content" role="form" method="POST" id="login-form" action="<?php echo e(url('/loginCheck')); ?>">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="row">
                                <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Your Email</label>
                                        <input class="form-control" name="email" placeholder="" type="email">
                                    </div>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Your Password</label>
                                        <input class="form-control" name ="password" placeholder="" type="password">
                                    </div>
            
                                    <div class="remember">
            
                                        <div class="checkbox">
                                            <label>
                                                <input name="optionsCheckboxes" type="checkbox">
                                                Remember Me
                                            </label>
                                        </div>
                                        <!-- <a href="#" class="forgot">Forgot my Password</a> -->
                                    </div>
            
                                    <!-- <a href="#" class="btn btn-lg btn-primary full-width">Login</a> -->
                                    <button type="submit"  id="login-me" class="btn btn-primary btn-lg full-width">Login</button>

                                    <!-- <div class="or"></div>
            
                                    <a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left"><i class="fab fa-facebook-f" aria-hidden="true"></i>Login with Facebook</a>
            
                                    <a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left"><i class="fab fa-twitter" aria-hidden="true"></i>Login with Twitter</a>
             -->
            
                                   <!--  <p>Don’t you have an account? <a href="#">Register Now!</a> it’s really simple and you can start enjoing all the benefits!</p> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <!-- ... end Login-Registration Form  -->       </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
$(function(){
        $('.alert-success').fadeIn().delay(10000).fadeOut();
        $('.alert-info').fadeIn().delay(10000).fadeOut();
        $('.alert-danger').fadeIn().delay(10000).fadeOut();
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('web.master_layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>