

<?php $__env->startSection('title'); ?>
    <?php if(!empty(Session::get('Change Password'))): ?> <?php echo e(Session::get('Change Password')); ?> <?php else: ?> Change Password <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Main Header Account -->
<div class="main-header">
  <div class="content-bg-wrap bg-account"></div>
  <div class="container">
    <div class="row">
      <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
        <div class="main-header-content">
          <h1>Your Account Dashboard</h1>
          <p>Welcome to your account dashboard! Here you’ll find everything you need to change your profile
  information, settings, read notifications and requests, view your latest messages, change your pasword and much
  more! Also you can create or manage your own favourite page, have fun!</p>
        </div>
      </div>
    </div>
  </div>
  <img class="img-bottom" src="<?php echo e(asset('resources/web-assets/img/account-bottom.png')); ?>" alt="friends">
</div>

<!-- ... end Main Header Account -->




<!-- Your Account Personal Information -->

<div class="container">
  <div class="row">
    <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">

<?php if($errors->any()): ?>
    <div class="alert alert-danger">
        
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li><?php echo e($error); ?></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
    </div>
<?php endif; ?>

    <?php if(Session::has('message')): ?>
        <div class="alert alert-info"><?php echo Session::get('message'); ?></div>
    <?php endif; ?>
    <?php if(Session::has('error')): ?>
        <div class="alert alert-danger"><?php echo Session::get('error'); ?></div>
    <?php endif; ?>
    <?php if(Session::has('success')): ?>
        <div class="alert alert-success"><?php echo Session::get('success'); ?></div>
    <?php endif; ?>
      <div class="ui-block">
        <div class="ui-block-title">
          <h6 class="title">Change Password</h6>
        </div>
        <div class="ui-block-content">

          
          <!-- Change Password Form -->
          
          <form class="form content" role="form" method="POST" id="reset-password" action="<?php echo e(url('save-reset-Password')); ?>">
            <div class="row">
              <input type="hidden" name="user_id" value="<?php echo e($user_id); ?>">
              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
              <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="form-group label-floating">
                  <label class="control-label">Confirm Current Password</label>
                  <input class="form-control" name="current_password" placeholder="" type="password" value="Olympus-2017">
                </div>
              </div>
          
              <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group label-floating is-empty">
                  <label class="control-label">Your New Password</label>
                  <input class="form-control" name="password" placeholder="" type="password">
                </div>
              </div>
              <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group label-floating is-empty">
                  <label class="control-label">Confirm New Password</label>
                  <input class="form-control" name="confirm_password" placeholder="" type="password">
                </div>
              </div>
          
              <!-- <div class="col col-lg-12 col-sm-12 col-sm-12 col-12">
                <div class="remember">
          
                  <div class="checkbox">
                    <label>
                      <input name="optionsCheckboxes" type="checkbox">
                      Remember Me
                    </label>
                  </div>
          
                  <a href="#" class="forgot">Forgot my Password</a>
                </div>
              </div> -->
          
              <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <button class="btn btn-primary btn-lg full-width">Change Password Now!</button>
              </div>
          
            </div>
          </form>
          
          <!-- ... end Change Password Form -->
        </div>
      </div>
    </div>

    <div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">
      <div class="ui-block">

        
        <?php echo $profile_sidebar; ?>

        

      </div>
    </div>
  </div>
</div>

<!-- ... end Your Account Personal Information -->
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
$(function(){
        $('.alert-success').fadeIn().delay(10000).fadeOut();
        $('.alert-info').fadeIn().delay(10000).fadeOut();
        $('.alert-danger').fadeIn().delay(10000).fadeOut();
    });
</script>
<script type="text/javascript">

  $(document).ready(function(){
    var id = $("select[name=state]").val();
        var selected = $('#dist_selected').val();
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('selectDistrict')); ?>",
            data: {id: id,selected:selected},
            cache: false,
            success: function (data) {
                $('#district').html(data);
            }
        });
    });    
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 32 && (charCode < 46 || charCode > 57) || (charCode == 32)) {
            alert("Please enter only digits");
            return false;
        }
        return true;
    }

$("input[name='phone_number']").on("keyup paste", function () {
            // Remove invalid chars from the input
            var input = this.value.replace(/[^0-9\(\)\s\-]/g, "");
            var inputlen = input.length;
            // Get just the numbers in the input
            var numbers = this.value.replace(/\D/g, '');
            var numberslen = numbers.length;
            // Value to store the masked input
            var newval = "";
            
            $(this).val(numbers.substring(0, 14));
        });

    function changeState(id) {
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('selectDistrict')); ?>",
            data: {id: id},
            cache: false,
            success: function (data) {
                $('#district').html(data);
            }
        });             
    }
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('web.master_layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>