<?php $__env->startSection('title'); ?>
    <?php if(!empty(Session::get('My Profile'))): ?> <?php echo e(Session::get('My Profile')); ?> <?php else: ?> My Profile <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- Main Header Account -->
<div class="main-header">
  <div class="content-bg-wrap bg-account"></div>
  <div class="container">
    <div class="row">
      <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
        <div class="main-header-content">
          <h1>Your Account Dashboard</h1>
          <p>Welcome to your account dashboard! Here you’ll find everything you need to change your profile
  information, settings, read notifications and requests, view your latest messages, change your pasword and much
  more! Also you can create or manage your own favourite page, have fun!</p>
        </div>
      </div>
    </div>
  </div>
  <img class="img-bottom" src="<?php echo e(asset('resources/web-assets/img/account-bottom.png')); ?>" alt="friends">
</div>

<!-- ... end Main Header Account -->


<!-- Your Account Personal Information -->

<div class="container">
  <div class="row">
    <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
      <div class="ui-block">
        <div class="ui-block-title">
          <h6 class="title">Personal Information</h6>
        </div>
        <?php if(Session::has('error')): ?>

	    <div class="alert alert-danger"><?php echo Session::get('error'); ?></div>

	    <?php endif; ?>

	    <?php if(Session::has('success')): ?>

	    <div class="alert alert-success"><?php echo Session::get('success'); ?></div>

	    <?php endif; ?>

        <div class="ui-block-content">

          
          <!-- Personal Information Form  -->
          
          <form class="form content" role="form" method="POST" id="profile-personal-form" action="<?php echo e(url('/storeprofile')); ?>">
            <div class="row">
          
              <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group label-floating">
                  <?php $first_name = isset($userInfo['first_name'])?$userInfo['first_name']:''; ?>
                  <label class="control-label">First Name</label>
                  <input class="form-control" placeholder="" type="text" name="first_name" value=<?= $first_name ?> >
                </div>
          
                <div class="form-group label-floating">
                  <?php $email = isset($userInfo['email'])?$userInfo['email']:''; ?>
                  <label class="control-label">Your Email</label>
                  <input class="form-control" readonly="" placeholder="" name="email" type="email" value= <?= $email?>>
                </div>
          
                <div class="form-group date-time-picker label-floating" >
                  <?php $dob = isset($userInfo['dob'])?$userInfo['dob']:''; ?>
                  <label class="control-label">Your Birthday</label>
                  <input name="datetimepicker" value=<?= $dob ?> >
                  <span class="input-group-addon">
                              <svg class="olymp-month-calendar-icon icon"><use xlink:href="<?php echo e(asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-month-calendar-icon')); ?>"></use></svg>
                            </span>
                </div>
              	</div>
          
              <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <?php $last_name = isset($userInfo['last_name'])?$userInfo['last_name']:''; ?>
                <div class="form-group label-floating">
                  <label class="control-label">Last Name</label>
                  <input class="form-control" placeholder="" name="last_name" type="text" value=<?= $last_name ?> >
                </div>
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="form-group label-floating">
                  <?php $website = isset($userInfo['website'])?$userInfo['website']:''; ?>
                  <label class="control-label">Your Website</label>
                  <input class="form-control" placeholder="" name="website" type="text" value=<?= $website?> >
                </div>
          
          
                <div class="form-group label-floating is-empty">
                  <?php $phone_number = isset($userInfo['phone_number'])?$userInfo['phone_number']:''; ?>
                  <label class="control-label">Your Phone Number</label>
                  <input class="form-control" maxlength="10" placeholder="10" name="phone_number" type="text" value=<?= $phone_number ?> >
                </div>


              </div>
          
              <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="form-group label-floating is-select">
                  <label class="control-label">Your Country</label>
                  <select class="form-control" name="country">
                    <?php foreach($country as $value) { ?>
                      <option value=<?= $value['id'] ?> ><?= $value['name'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="form-group label-floating is-select">
                  <label class="control-label">Your State / Province</label>
                  <?php $stateVal = isset($userInfo['state'])?$userInfo['state']:''; ?>
                  <select class="form-control" name="state" value="<?php echo e($stateVal); ?>" onchange="changeState(this.value)" required>
                  <?php $__currentLoopData = $state; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                                <?php if($s->id == $stateVal): ?>
                                                    <option value="<?php echo e($s->id); ?>" selected ><?php echo e($s->name); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($s->id); ?>" ><?php echo e($s->name); ?></option>
                                                <?php endif; ?>     
                                                
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
              <input type='hidden' id="dist_selected" value="<?=isset($userInfo['city'])?$userInfo['city']:0 ?>" />
              <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
                <div class="form-group label-floating is-select">
                  <label class="control-label">Your City</label>
                  <select class="form-control" name="city" id="district">
                    
                  </select>
                </div>
              </div>
              <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group label-floating">
                <?php $about_u = isset($userInfo['about_u'])?$userInfo['about_u']:''; ?>
                  <label class="control-label">Write a little description about you</label>
                  <textarea class="form-control" name="about_u" value="<?= $about_u ?>"><?= $about_u ?></textarea>
                </div>
          
                <div class="form-group label-floating is-select">
                  <label class="control-label">Your Gender</label>
                  <select class="selectpicker form-control" name="gender">
                    <?php 
                      $m_selected = '';
                      $f_selected = '';
                      $gender = isset($userInfo['gender'])?$userInfo['gender']:''; 
                      if($gender == 'MALE') $m_selected = "selected";
                      if($gender == 'FEMALE') $f_selected = "selected";
                    ?>
                    <option value="MALE" <?= $m_selected ?> >Male</option>
                    <option value="FEMALE" <?= $f_selected ?> >Female</option>
                  </select>
                </div>
          
                <!-- <div class="form-group label-floating is-empty">
                 <?php $religion = isset($userInfo['religion'])?$userInfo['religion']:''; ?>
                  <label class="control-label">Religious Belifs</label>
                  <input class="form-control" name ="religion" placeholder="" value="<?= $religion ?>"  type="text">
                </div> -->
              </div>
              <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group label-floating is-empty">
                  <?php $birth_place = isset($userInfo['birth_place'])?$userInfo['birth_place']:''; ?>
                  <label class="control-label">Your Birthplace</label>
                  <input class="form-control" name ="birth_place" value="<?= $birth_place ?>" placeholder="" type="text">
                </div>
          
                <div class="form-group label-floating">
                  <?php $occupation = isset($userInfo['occupation'])?$userInfo['occupation']:''; ?>
                  <label class="control-label">Your Occupation</label>
                  <input class="form-control" placeholder="" name="occupation" value="<?= $occupation?>" type="text" value="UI/UX Designer">
                </div>

                <?php 
                      $m_selected = '';
                      $nm_selected = '';
                      $relationship = isset($userInfo['relationship'])?$userInfo['relationship']:''; 
                      if($relationship == 'MARRIED') $m_selected = "selected";
                      if($relationship == 'UNMARRIED') $nm_selected = "selected";
                    ?>
          
                <div class="form-group label-floating is-select">
                  <label class="control-label">Status</label>
                  <select class="selectpicker form-control" name="relationship">
                    <option value="MARRIED" <?= $m_selected ?>>Married</option>
                    <option value="UNMARRIED" <?= $nm_selected ?>>Not Married</option>
                  </select>
                </div>
          
                <!-- <div class="form-group label-floating">
                  <label class="control-label">Political Incline</label>
                  <input class="form-control" placeholder="" type="text" value="Democrat">
                </div> -->
              </div>
              <!-- <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="form-group with-icon label-floating">
                  <?php $fb_link = isset($userInfo['fb_link'])?$userInfo['fb_link']:''; ?>
                  <label class="control-label">Your Facebook Account</label>
                  <input class="form-control" type="text" name ="fb_link" value=<?= $fb_link ?>>
                  <i class="fab fa-facebook-f c-facebook" aria-hidden="true"></i>
                </div>
                <div class="form-group with-icon label-floating">
                  <?php $twitter_link = isset($userInfo['twitter_link'])?$userInfo['twitter_link']:''; ?>
                  <label class="control-label">Your Twitter Account</label>
                  <input class="form-control" type="text" name="twitter_link" value=<?=$twitter_link ?> >
                  <i class="fab fa-twitter c-twitter" aria-hidden="true"></i>
                </div>
                <div class="form-group with-icon label-floating is-empty">
                  <?php $insta_link = isset($userInfo['insta_link'])?$userInfo['insta_link']:''; ?>
                  <label class="control-label">Your Insta Account</label>
                  <input class="form-control" name="insta_link" type="text" value=<?=$insta_link ?>>
                  <i class="fa fa-rss c-rss" aria-hidden="true"></i>
                </div>
                
              </div> -->
              <!-- <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <button class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
              </div> -->
              <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <button class="btn btn-primary btn-lg full-width">Save all Changes</button>
              </div>
          
            </div>
          </form>
          
          <!-- ... end Personal Information Form  -->
        </div>
      </div>
    </div>

    <div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">
      <div class="ui-block">

      
        <!-- Your Profile  -->
          <?php echo $profile_sidebar; ?>


        
        <!-- ... end Your Profile  -->
        

      </div>
    </div>
  </div>
</div>

<!-- ... end Your Account Personal Information -->
<?php $__env->stopSection(); ?>
<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">

  $(document).ready(function(){
    var id = $("select[name=state]").val();
        var selected = $('#dist_selected').val();
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('selectDistrict')); ?>",
            data: {id: id,selected:selected},
            cache: false,
            success: function (data) {
                $('#district').html(data);
            }
        });
    });    
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 32 && (charCode < 46 || charCode > 57) || (charCode == 32)) {
            alert("Please enter only digits");
            return false;
        }
        return true;
    }

$("input[name='phone_number']").on("keyup paste", function () {
            // Remove invalid chars from the input
            var input = this.value.replace(/[^0-9\(\)\s\-]/g, "");
            var inputlen = input.length;
            // Get just the numbers in the input
            var numbers = this.value.replace(/\D/g, '');
            var numberslen = numbers.length;
            // Value to store the masked input
            var newval = "";
            
            $(this).val(numbers.substring(0, 14));
        });

    function changeState(id) {
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('selectDistrict')); ?>",
            data: {id: id},
            cache: false,
            success: function (data) {
                $('#district').html(data);
            }
        });             
    }

    function changeSream(id) {
        $.ajax({
            type: "GET",
            url: "<?php echo e(route('selectSubStream')); ?>",
            data: {id: id},
            cache: false,
            success: function (data) {
                $('#sub_stream_id').empty();
                $.each(data, function (key, value) {
                    $('#sub_stream_id').append('<option value="' + value.id + '">' + value.title + '</option>');
                });
            }
        });             
    }

    
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('web.master_layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>