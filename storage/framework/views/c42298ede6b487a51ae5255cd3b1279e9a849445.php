<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <link rel="icon" href="<?php echo e(asset('resources/web-assets/images/fav-icon.png')); ?>">
        <title>
            <?php $__env->startSection('title'); ?> 
            <?php echo $__env->yieldSection(); ?>
            - <?php if(!empty(Session::get('Brilliant Mentors'))): ?> <?php echo e(Session::get('Brilliant Mentors')); ?> <?php else: ?> Brilliant Mentors <?php endif; ?>
        </title>
        
        <?php $__env->startSection('style'); ?>
            <?php echo $__env->make('web.master_layout.style', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldSection(); ?>
        <?php echo $__env->yieldPushContent('styles'); ?>
    </head>

    <body class="landing-page">
        <?php if(!(Route::getCurrentRoute()->uri() == '/' || Route::getCurrentRoute()->uri() == 'login')): ?>
        <?php $__env->startSection('header'); ?>
            <?php echo $__env->make('web.master_layout.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldSection(); ?>
        <?php echo $__env->yieldPushContent('header'); ?>
        <?php endif; ?>

        <?php $__env->startSection('main-content'); ?>  
        <?php echo $__env->yieldSection(); ?>

        <?php $__env->startSection('script'); ?>
         <?php echo $__env->make('web.master_layout.script', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->yieldSection(); ?>
        <?php echo $__env->yieldPushContent('scripts'); ?>
    </body>
</html>