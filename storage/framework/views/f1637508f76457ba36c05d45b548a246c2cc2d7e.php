<div class="col col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group label-floating">
									<label class="control-label">Title or Place</label>
									 <?php $position = isset($employmentData['position'])?$employmentData['position']:''; ?>
									<input class="form-control" name="Employment[<?= $count ?>][position]" placeholder="" type="text" value=<?= $position?>>
								</div>
							</div>					
							<div class="col col-lg-3 col-md-3 col-sm-12 col-12">
			                <div class="form-group label-floating is-select">
			                  <label class="control-label">Select Started Month</label>
			                  <select class="form-control" name="Employment[<?= $count ?>][start_month]">
			                  <?php $start_month = isset($employmentData['start_month'])?$employmentData['start_month']:0; 
			                  ?>
			                  <?php $__currentLoopData = $month; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
			                  		<?php if($start_month == $value): ?>
			                  			<option value= <?= $key ?> Selected > <?= $value ?></option>
			                  		<?php else: ?>
			                  			<option value= <?= $key ?> > <?= $value ?></option>
			                  		<?php endif; ?>
			                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                  </select>
			                </div>
			                </div>
			                <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
			                <div class="form-group label-floating is-select">
			                  <label class="control-label">Select Started Year</label>
			                  <select class="form-control" name="Employment[<?= $count ?>][start_year]">
			                  <?php $start_year = isset($employmentData['start_year'])?$employmentData['start_year']:0; ?>
			                  <?php 
			                  	for ($i=1990; $i <= date("Y") ; $i++){
			                  	?>
			                  		<?php if($start_year == $i): ?>
			                  			<option value= <?= $i ?> Selected > <?= $i ?></option>
			                  		<?php else: ?>
			                  			<option value= <?= $i ?> > <?= $i ?></option>
			                  		<?php endif; ?>
			                  	<?php
			                  }
			                  ?>
			                  </select>
			                </div>
			                </div>
			                <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
			                <div class="form-group label-floating is-select">
			                  <label class="control-label">Select Ended Month</label>
			                  <select class="form-control" name="Employment[<?= $count ?>][end_month]">
			                  <?php $end_month = isset($employmentData['end_month'])?$employmentData['end_month']:0; ?>
			                  <?php $__currentLoopData = $month; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
			                  		<?php if($end_month == $value): ?>
			                  			<option value= <?= $key ?> Selected > <?= $value ?></option>
			                  		<?php else: ?>
			                  			<option value= <?= $key ?> > <?= $value ?></option>
			                  		<?php endif; ?>
			                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			                  </select>
			                </div>
			                </div>
			                <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
			                <div class="form-group label-floating is-select">
			                  <label class="control-label">Select Ended Year</label>
			                  <select class="form-control" name="Employment[<?= $count ?>][end_year]">
			                  
			                  <?php 
			                  $end_year = isset($employmentData['end_year'])?$employmentData['end_year']:0;
			                  	for ($i=1990; $i <= date("Y") ; $i++){
			                  	?>
			                  		<?php if($end_year == $i): ?>
			                  			<option value= <?= $i ?> Selected > <?= $i ?></option>
			                  		<?php else: ?>
			                  			<option value= <?= $i ?> > <?= $i ?></option>
			                  		<?php endif; ?>
			                  	<?php
			                  }
			                  ?>
			                  </select>
			                </div>
			                </div>
							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group label-floating">
								<?php $description = isset($employmentData['description'])?$employmentData['description']:''; ?>
									<label class="control-label">Description</label>
									<textarea class="form-control" placeholder="" name="Employment[<?= $count ?>][description]"  value= "<?= $description ?>" ><?= $description ?></textarea>
								</div>
							</div>