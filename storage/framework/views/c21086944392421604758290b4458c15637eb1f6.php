<?php $__env->startSection('title'); ?>

    <?php if(!empty(Session::get('Hobbies/Interests'))): ?> <?php echo e(Session::get('Hobbies/Interests')); ?> <?php else: ?> Hobbies/Interests <?php endif; ?>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('main-content'); ?>

<!-- Main Header Account -->

<div class="main-header">

  <div class="content-bg-wrap bg-account"></div>

  <div class="container">

    <div class="row">

      <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">

        <div class="main-header-content">

          <h1>Your Account Dashboard</h1>

          <p>Welcome to your account dashboard! Here you’ll find everything you need to change your profile

		  information, settings, read notifications and requests, view your latest messages, change your pasword and much

		  more! Also you can create or manage your own favourite page, have fun!</p>

        </div>

      </div>

    </div>

  </div>

  <img class="img-bottom" src="<?php echo e(asset('resources/web-assets/img/account-bottom.png')); ?>" alt="friends">

</div>



<!-- ... end Main Header Account -->





<!-- Your Account Personal Information -->



<div class="container">

	<div class="row">

		<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">

			<div class="ui-block">

				<div class="ui-block-title">

					<h6 class="title">Hobbies and Interests</h6>

				</div>
				<?php if(Session::has('error')): ?>

			    <div class="alert alert-danger"><?php echo Session::get('error'); ?></div>

			    <?php endif; ?>

			    <?php if(Session::has('success')): ?>

			    <div class="alert alert-success"><?php echo Session::get('success'); ?></div>

			    <?php endif; ?>

				<div class="ui-block-content">
					<!-- Form Hobbies and Interests -->
					<form role= "form" id="interest-form" method="POST" action="<?php echo e(url('/save-interest')); ?>">
						<div class="row">
							<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
							<div class="col col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="control-group" id="stream">
	                                <label class="control-label">Stream<span class="error">*</span> :</label>
	                                <div class="controls">
	                                    <div class="subject-info-box-1">
	                                        <select multiple="multiple" name="stream_id_unselected[]" id='stream_id1' class="form-control">
	                                            <?php $__currentLoopData = $stream; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							                      <option value="<?php echo e($s['id']); ?>"><?php echo e($s['title']); ?></option>
							                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                        </select>
	                                    </div>
	                                    <div class="subject-info-arrows text-center">
	                                        <input type='button' id='stream_idbtnAllRight' value='>>' class="btn-default drg-button" />
	                                        <input type='button' id='stream_idbtnRight' value='>' class="btn-default drg-button" />
	                                        <input type='button' id='stream_idbtnLeft' value='<' class="btn-default drg-button" />
	                                        <input type='button' id='stream_idbtnAllLeft' value='<<' class="btn-default drg-button" />
	                                    </div>
	                                    <div class="subject-info-box-2">
	                                        <select multiple="multiple" name="stream_id[]" id='stream_id2' class="form-control" required>
	                                            <?php $__currentLoopData = $stream_selected; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option value="<?php echo e($s['id']); ?>" selected><?php echo e($s['title']); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                        </select>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="control-group" id="substream">
	                                <label class="control-label">Sub Stream<span class="error">* </span>:</label>
	                                <div class="controls">
	                                    <div class="subject-info-box-1">
	                                        <select multiple="multiple" name="sub_stream_id_unselected[]" id='sub_stream_id1' class="form-control">
	                                        <?php $__currentLoopData = $sub_stream; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option value="<?php echo e($s['id']); ?>"><?php echo e($s['title']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
	                                    </div>
	                                    <div class="subject-info-arrows text-center">
	                                        <input type='button' id='sub_stream_idbtnAllRight' value='>>' class="btn-default drg-button" />
	                                        <input type='button' id='sub_stream_idbtnRight' value='>' class="btn-default drg-button" />
	                                        <input type='button' id='sub_stream_idbtnLeft' value='<' class="btn-default drg-button" />
	                                        <input type='button' id='sub_stream_idbtnAllLeft' value='<<' class="btn-default drg-button" />
	                                    </div>
	                                    <div class="subject-info-box-2">
	                                        <select multiple="multiple" name="sub_stream_id[]" id='sub_stream_id2' class="form-control" required>
	                                        <?php $__currentLoopData = $sub_stream_selected; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option value="<?php echo e($s['id']); ?>" selected><?php echo e($s['title']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
				                <button class="btn btn-primary btn-lg full-width">Save all Changes</button>
				            </div>
						</div>
					</form>
					<!-- ... end Form Hobbies and Interests -->
				</div>
			</div>
		</div>

		<div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">

			<div class="ui-block">

		<?php echo $profile_sidebar; ?>


			</div>

		</div>

	</div>

</div>



<!-- ... end Your Account Personal Information -->







<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
(function () {
        $('#stream_idbtnRight').click(function (e) {

            var selectedOpts = $('#stream_id1 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }
            $('#stream_id2').append($(selectedOpts).clone());
            $('#stream_id2 > option').attr('selected', 'selected');
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#stream_idbtnAllRight').click(function (e) {
            var selectedOpts = $('#stream_id1 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#stream_id2').append($(selectedOpts).clone());
            $('#stream_id2 option').attr('selected', 'selected');
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#stream_idbtnLeft').click(function (e) {
            var selectedOpts = $('#stream_id2 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#stream_id1').append($(selectedOpts).clone());
            $('#stream_id1 option').removeAttr("selected");
            $(selectedOpts).remove();
            e.preventDefault();
            $('#stream_id2 > option').attr('selected', 'selected');
        });

        $('#stream_idbtnAllLeft').click(function (e) {
            var selectedOpts = $('#stream_id2 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#stream_id1').append($(selectedOpts).clone());
            $('#stream_id1 option').removeAttr("selected");
            $(selectedOpts).remove();
            e.preventDefault();
            $('#stream_id2 > option').attr('selected', 'selected');
        });
}(jQuery));

(function () {
        $('#sub_stream_idbtnRight').click(function (e) {

            var selectedOpts = $('#sub_stream_id1 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }
            $('#sub_stream_id2').append($(selectedOpts).clone());
            $('#sub_stream_id2 > option').attr('selected', 'selected');
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#sub_stream_idbtnAllRight').click(function (e) {
            var selectedOpts = $('#sub_stream_id1 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#sub_stream_id2').append($(selectedOpts).clone());
            $('#sub_stream_id2 option').attr('selected', 'selected');
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#sub_stream_idbtnLeft').click(function (e) {
            var selectedOpts = $('#sub_stream_id2 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#sub_stream_id1').append($(selectedOpts).clone());
            $('#sub_stream_id1 option').removeAttr("selected");
            $(selectedOpts).remove();
            e.preventDefault();
            $('#sub_stream_id2 > option').attr('selected', 'selected');
        });

        $('#sub_stream_idbtnAllLeft').click(function (e) {
            var selectedOpts = $('#sub_stream_id2 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#sub_stream_id1').append($(selectedOpts).clone());
            $('#sub_stream_id1 option').removeAttr("selected");
            $(selectedOpts).remove();
            e.preventDefault();
            $('#sub_stream_id2 > option').attr('selected', 'selected');
        });
}(jQuery));

$('#stream_id1').on('change', function () {
    var id = $(this).val();
    var token = $('input[name="_token"]').val();
    var streamArray = new Array();//storing the selected values inside an array
    $('#stream_id1 option:selected').each(function (i, selected) {
        streamArray[i] = $(selected).val();
    });

    if (streamArray) {
    	$.ajax({
            type: "POST",
            url: "<?php echo e(route('selectSubStream')); ?>",
            data: {id: id, _token: token},
            cache: false,
            dataType: "json",
            success: function (data) {
                $('#sub_stream_id1').empty();
                $.each(data, function (key, value) {
                    $('#sub_stream_id1').append('<option value="' + value.id + '">' + value.title + '</option>');
                });
            }
        });
    } else {
        $('#sub_stream_id1').empty();
    }
});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('web.master_layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>