<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Session;
use Request;
use URL;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
//        if (Auth::guard($guard)->check()) {
//            return redirect('/dashboard');
//        }

      switch ($guard) {
        case 'admin':
        if (Auth::guard($guard)->check()) {
          return redirect('/panel/dashboard');
        }
        break;
        case 'panel':
        if (Auth::guard($guard)->check()) {
          return redirect('/panel/dashboard');
        }
        break;
        case 'user':
        if (Auth::check()) {
          return $next($request);
        }else{

          // (isset($request->category_id) && $request->category_id !=''? Session::put('category_id',$request->category_id):'');

          // (isset($request->booking_date) && $request->booking_date !=''? Session::put('booking_date',$request->booking_date):'');

          // (isset($request->start_time) && $request->start_time !=''? Session::put('start_time',$request->start_time):'');

          // (isset($request->description) && $request->description !=''? Session::put('description',$request->description):'');

          // (isset($request->term_condition) && $request->term_condition !=''? Session::put('term_condition',$request->term_condition):'');

          // if(strpos(URL::previous(), 'panel') !== false){
          //   return redirect('/')->with('checklogin', "login");
          // }else{
          //   return Redirect::back()->with('checklogin', "login");
          // }
           return Redirect::to('login');
          exit;
        }
        break;
        default:
        if (Auth::guard($guard)->check()) {
          return redirect('/');
        }
        break;
      }

      return $next($request);
    }
  }
