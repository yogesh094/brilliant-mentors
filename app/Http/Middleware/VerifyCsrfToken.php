<?php

namespace App\Http\Middleware;

use App\Models\Language;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware {

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'registration',
        'login',
        '/panel/question/Assignquestion',
        '/panel/appointment/assignAppoinment',
        'panel/appointment/updateAppoinmentStatus',
        'panel/appuser/pageStoreNumber',
        'panel/question/pageStoreNumber',
        'panel/appointment/customeNotification',
        'checkTimeConsultaion',
        '/cancelAppointment',
        '/refundAppointment'
    ];

}
