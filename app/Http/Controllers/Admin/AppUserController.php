<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUser;
use App\Models\Role;
use App\Models\RoleUser;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Session;

class AppUserController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $pager = 2;
        $userList = AppUser::get(); //where('user_role',2)->
        Session::forget('AppQuestionPage');
        if (empty(Session::get('AppUserPage'))) {
            Session::put('AppUserPage', 0);
        }

        return view('Admin.user.app.list', ['data' => $userList]);
    }

    public function create() {
        return view('Admin.user.app.add');
    }

    public function updatemultirecode(Request $request) {

        if ($request->action != '' && count($request->checkaction) > 0) {

            if ($request->action == 'inactive') {

                if (count($request->checkaction) > 0) {
                    foreach ($request->checkaction as $key => $inactive) {
                        $AppUser = AppUser::find($inactive);
                        $AppUser->status = 0;
                        $AppUser->save();
                    }
                }
                return redirect('/panel/appuser')->with('success', "User Inactive successfully.");
            } else if ($request->action == 'active') {

                if (count($request->checkaction) > 0) {
                    foreach ($request->checkaction as $key => $active) {
                        $AppUser = AppUser::find($active);
                        $AppUser->status = 1;
                        $AppUser->save();
                    }
                }
                return redirect('/panel/appuser')->with('success', "User Active successfully.");
            } elseif ($request->action == 'delete') {

                if (count($request->checkaction) > 0) {
                    foreach ($request->checkaction as $key => $delete) {
                        $AppUser = AppUser::find($delete);
                        $AppUser->delete();
                    }
                }
                return redirect('/panel/appuser')->with('success', "User Detete successfully.");
            }
        } else {
            return redirect('/panel/appuser')->with('error', 'Something went wrong please try later');
        }
    }

    public function arrayData(Datatables $datatables) {
        $builder = AppUser::query()->select('id', 'email', 'gender', 'status', 'created_at', 'updated_at', 'login_type', 'is_verified');
        //->where('user_role',2)

        return $datatables->eloquent($builder)
                        ->addColumn('checkbox', function ($user) {
                            return "<input class='' type='checkbox' id='checkaction" . $user->id . "' name='checkaction[]' value=" . $user->id . " onclick='onclickcheck(" . $user->id . ");'>";
                        })
                        ->editColumn('id', function ($user) {
                            return $user->id;
                        })
                        ->editColumn('email', function ($user) {
                            return "<a href=" . url('panel/appuser/edit/' . $user->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $user->email . "
                                </a>";
                        })
                        ->editColumn('gender', function($user) {
                            return "<a href=" . url('panel/appuser/edit/' . $user->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . ($user->gender == 1 ? "Male" : "Female") . "
                                </a>";
                        })
                        ->editColumn('is_verified', function($user) {
                            if ($user->is_verified == 1) {
                                return '<span class="badge bg-green"><i class="fa fa-check-circle"></i></span>';
                            } else {
                                return '<span class="badge bg-red"><i class="fa fa-times-circle"></i></span>';
                            }
                        })
                        ->editColumn('status', function($user) {
                            if ($user->status == 1) {
                                return '<span class="badge bg-green">Active</span>';
                            } else if ($user->status == 0) {
                                return '<span class="badge bg-red">Inactive</span>';
                            } else {
                                return '<span class="badge bg-yellow">Delete</span>';
                            }
                        })
                        ->editColumn('created_at', function ($user) {
                            if ($user->created_at !== null) {
                                return date("j-M-y g:m A", strtotime($user->created_at));
                            }
                            return '';
                        })
                        ->editColumn('updated_at', function ($user) {
                            if ($user->updated_at !== null) {
                                return date("j-M-y g:m A", strtotime($user->updated_at));
                            }
                            return '';
                        })
                        ->addColumn('action', function($user) {
                            return "<a href=" . url('panel/appuser/edit/' . $user->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>  
                                              <a href=" . url('panel/appuser/destroy/' . $user->id) . " class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                        })
                        ->rawColumns(['checkbox', 'id', 'email', 'gender', 'action', 'status', 'is_verified'])
                        ->toJson();
    }

    public function store(Request $request) {
        // echo "<pre>";print_r($request->all());die;

        if (isset($request->id) && $request->id > 0) {
            $rules = [
                'username' => 'required|max:255',
                'email' => "required|email|unique:appuser,email," . $request->id, 'id'
            ];
        } else {
            $rules = [
                'username' => 'required|max:255',
                'email' => "required|email|unique:appuser,email," . $request->id, 'id',
                'password' => 'required|min:6',
                'password_confirmation' => 'required|min:6|same:password',
            ];
        }

        $message = [
            "username.required" => "User name is required",
            "username.max" => "User name must be less than 255 characters",
            "email.required" => "Email is required",
            "email.email" => "Invalid e-mail address",
            "email.unique" => "E-mail address is already exist",
            //"gender.required"    => "Please select the gender",
            "password.required" => "Please enter password",
            "password.min" => "Password should be minimum 6 character length",
            "password_confirmation.required" => "Confirm password is required",
            "password_confirmation.min" => "Confirm password should be minimum 6 character length",
            "password_confirmation.same" => "Confirm password should be same as password",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);
        // process the login
        if ($validator->fails()) {
            if (Input::get('id') != null && Input::get('id') > 0) {
                return Redirect::to('panel/appuser/edit/' . Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
            } else {
                return Redirect::to('panel/appuser/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
            }
        }
        // echo "<pre>";print_r($request->all());die;
        $userObj = "";
        if (isset($request->id)) {
            $userObj = AppUser::find($request->id);
            $userObj->status = Input::get('status');
            $userObj->updated_at = date('Y-m-d H:i:s');
        } else {
            $userObj = new AppUser();
            $token = md5(rand(111111111, 999999999));
            //generate unique token - end

            $userObj->token = $token;
            $userObj->password = bcrypt($request->get('password'));
            $userObj->status = Input::get('status');
            $userObj->is_verified = 1;
            $userObj->created_at = date('Y-m-d H:i:s');
        }

        $userObj->username = Input::get('username');
        $userObj->email = Input::get('email');
        //$userObj->gender     = Input::get('gender');

        if ($userObj->save()) {
            //section for new user
            if (Input::get('id') != null && Input::get('id') > 0) {
                return redirect('/panel/appuser')->with('success', "User updated successfully.");
            } else {
                return redirect('/panel/appuser')->with('success', "User created successfully.");
            }
        } else {
            return redirect('/panel/appuser')->with('success', "something went wrong please try later.");
        }
    }

    // public function pageStoreNumber(Request $request){
    //      Session::put('AppUserPage',($request->pageNo - 1) * 10);
    //      echo Session::get('AppUserPage');
    // }
    public function edit($id) {
        $pageID = Session::get('AppUserPage');
        Session::put('AppUserPage', $pageID);
        if ($id > 0) {
            $userRolesArr = Role::pluck('name', 'id')->toArray();
            $user = AppUser::find($id);
            if (count($user) > 0) {
                return View::make('Admin.user.app.edit')->with('data', $user);
            }
            return View::make('Admin.error.404');
        }
    }

    public function destroy(Request $request, $id = 0) {
        $pageID = Session::get('AppUserPage');
        Session::put('AppUserPage', $pageID);
        $AppUserObj = AppUser::find($id);
        if (!empty($AppUserObj) > 0) {

            if ($AppUserObj->destroy($id)) {
                DB::table('question')->where('appuser_id', $id)->delete();
                return redirect('/panel/appuser')->with('success', "User deleted successfully.");
            } else {
                return redirect('/panel/appuser')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/appuser')->with('error', trans('Record not found'));
    }

}
