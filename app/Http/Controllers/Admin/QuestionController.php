<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Question;
use App\Models\Category;
use App\Models\Language;
use App\Models\Topic;
use App\Models\Notification;
use App\Library\General;
use App\Models\AppUser;
use App\Models\Setting;
use App\Models\EmailTemplate;
use Html;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
use Cache;
use Session;
use Mail;
/* To download data in excel */
use Excel;
use DB;

class QuestionController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        Session::forget('AppUserPage');
        if (empty(Session::get('AppQuestionPage'))) {
            Session::put('AppQuestionPage', 0);
        }
        $User = User::whereNotIn('user_role', [1])->where('status', 1)->get();
        return view('Admin.question.list', ["User" => $User]);
    }

    // return add view language keyword
    public function add(Request $request) {
        $languages = Language::all();
        $User = User::whereNotIn('user_role', [1])->where('status', 1)->get();
        $Category = Category::where("status", 1)->where('is_parent', 0)->get();
        $Topic = Topic::where("status", 1)->get();
        return view('Admin.question.add', ["languages" => $languages, "User" => $User, "Category" => $Category, "Topic" => $Topic]);
    }

    //return page number
    public function pageStoreNumber(Request $request) {
        Session::put('AppQuestionPage', ($request->pageNo - 1) * 10);
        echo Session::get('AppQuestionPage');
    }

    //return edit view language keyword
    public function edit(Request $request, $id = 0) {
        $pageID = Session::get('AppQuestionPage');
        Session::put('AppQuestionPage', $pageID);
        if ($id > 0) {

            $Question = Question::find($id);
            if (!empty($Question) > 0) {

                $languages = Language::all();
                $User = User::whereNotIn('user_role', [1])->where('status', 1)->get();
                $Category = Category::where('language_id', $Question->language_id)->where("status", 1)->where('is_parent', 0)->get();
                $parentCategory = Category::where('language_id', $Question->language_id)->where("status", 1)->where('is_parent', $Question->category_id)->get();
                $Topic = Topic::where('language_id', $Question->language_id)->where("status", 1)->get();

                return View::make('Admin.question.edit', ["languages" => $languages, "User" => $User, "Category" => $Category, "Topic" => $Topic, 'parentCategory' => $parentCategory])->with('Question', $Question);
            }
            return View::make('Admin.error.404');
        }
    }

    //add and edit language keyword
    public function getquestion(Request $request) {


        $QuestionArr = [];
        $Question = Question::where("id", $request->id)
                ->get()
                ->first();

        $QuestionArr = array(
            'id' => $Question->id,
            'language_id' => $Question->language_id,
            'expert_id' => $Question->expert_id,
            'question' => $Question->question
        );
        return $QuestionArr;
    }

    public function Assignquestion(Request $request) {
        $rules = [
            'expert_id' => 'required'
        ];
        $message = [
            "expert_id.required" => "Please select expert"
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'error' => 401,
                        'message' => $validator->messages()->first(),
            ]);
            exit;
        }
        if (isset($request->id) && $request->id > 0) {
            $Question = Question::find($request->id);
            $Question->updated_at = date('Y-m-d H:i:s');
        }
        $Question->expert_id = $request->expert_id;
        if ($Question->save()) {
            $userObj = User::where('id', $Question->expert_id)->first();
            $msg = Cache::get('expert_question_assigned') . ' ' . $userObj->first_name . ' ' . $userObj->last_name;

            $notification = Notification::where('question_id', $Question->id)->where('type_id', 1)->first();

            if (count($notification) > 0) {
                $notification = Notification::find($notification->id);
                $notification->updated_at = date('Y-m-d H:i:s');
            } else {
                $notification = new Notification;
            }
            $notification->language_id = ($Question->language_id != null ? $Question->language_id : '');
            $notification->expert_id = ($Question->expert_id != null ? $Question->expert_id : '');
            $notification->appuser_id = ($Question->appuser_id != null ? $Question->appuser_id : '');
            $notification->question_id = ($Question->id != null ? $Question->id : '');
            $notification->type_id = 1;
            $notification->is_read = 0;
            $notification->message = $msg;
            $notification->save();

            $send = General::pushNotification($notification->expert_id, $notification->appuser_id, $msg, 1, $Question->id);

            $appuserObj = AppUser::where('id', $Question->appuser_id)->first();
            $userObj = User::where('id', $notification->expert_id)->first();

            $msgEmail = str_replace('[username]', $appuserObj->username, Cache::get('expert_question_assigned_admin'));
            $emailTemplateObj = EmailTemplate::where('id', 7)->where('language_id', 1)->first();


            $emailFrom = Setting::where('slug', 'email')->first();

            $subject = $emailTemplateObj->subject;
            $body = $emailTemplateObj->body;
            $body = str_replace('[name]', $userObj->first_name, $body);
            $body = str_replace('[userName]', $appuserObj->username, $body);

            $data = array("name" => $userObj->first_name, 'body' => $body, "to" => $userObj->email, "subject" => "Assign To Expert", "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'FRONT_URL' => url(''));

            $res = Mail::send('emails.expertAssingQuestion', ['data' => $data], function($Emailmessage) use ($data) {
                        $Emailmessage->from($data['from'], $data['emailLabel']);
                        $Emailmessage->to($data['to'], $data['name']);
                        $Emailmessage->subject($data['subject']);
                    });

            if ($Question->language_id == 1) {
                $emailTemplateObj = EmailTemplate::where('id', 19)->where('language_id', 1)->first();
            } else {
                $emailTemplateObj = EmailTemplate::where('id', 20)->where('language_id', 2)->first();
            }

            $emailFrom = Setting::where('slug', 'email')->first();
            $subject = $emailTemplateObj->subject;
            $body = $emailTemplateObj->body;
            $body = str_replace('[name]', $userObj->first_name, $body);
            $body = str_replace('[userName]', $appuserObj->username, $body);

            $data1 = array("name" => $userObj->first_name, 'body' => $body, "to" => $appuserObj->email, "subject" => "Assign To Expert", "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'FRONT_URL' => url(''));

            $res = Mail::send('emails.expertAssingQuestion', ['data' => $data1], function($Emailmessage) use ($data1) {
                        $Emailmessage->from($data1['from'], $data1['emailLabel']);
                        $Emailmessage->to($data1['to'], $data1['name']);
                        $Emailmessage->subject($data1['subject']);
                    });
            return response()->json([
                        'status' => true,
                        'success' => 200,
                        'message' => "success",
            ]);
            exit;
        } else {
            return response()->json(['status' => false, 'error' => 401, 'message' => 'Something went wrong please try later']);
            exit;
        }


        //}
    }

    public function store(Request $request) {
        $pageID = Session::get('AppQuestionPage');
        Session::put('AppQuestionPage', $pageID);
        $this->validate($request, [
            "language_id" => "required",
            "question" => "required",
            "answer" => "required"
                ], [
            "language_id.required" => "language is required.",
            "question.required" => "question is required.",
            "answer.required" => "answer is required"
        ]);

        // check validation
        if (!isset($this->validate)) {

            if (isset($request->id) && $request->id > 0) {
                $Question = Question::find($request->id);
                $Question->expert_id = $request->expert_id != null ? $request->expert_id : $Question->expert_id;
                $Question->updated_at = date('Y-m-d H:i:s');
            } else {
                $Question = new Question();
                $Question->appuser_id = 0;
                $Question->expert_id = $request->expert_id;
            }

            $Question->language_id = $request->language_id;
            $Question->parent_category = $request->parent_category;
            $Question->category_id = $request->category_id;
            $Question->question = $request->question;
            $Question->answer = $request->answer;
            if ($request->is_private == 1) {
                $Question->is_private = 1;
            } else {
                $Question->is_private = 0;
            }

            $Question->status = $request->status;
            $Question->created_at = date('Y-m-d H:i:s');
            $Question->save();

            if (!empty($Question->answer) && !empty($Question->appuser_id) && $Question->appuser_id != 0) {

                $userObj = User::where('id', $Question->expert_id)->first();
                if ($request->answer != '') {
                    $msg = $userObj->first_name . ' ' . $userObj->last_name . ' ' . Cache::get('expert_answered');
                } else {
                    $msg = Cache::get('expert_question_assigned') . ' ' . $userObj->first_name . ' ' . $userObj->last_name;
                }

                $notification = Notification::where('question_id', $Question->id)->where('type_id', 1)->first();
                if (count($notification) > 0) {
                    $notification = Notification::find($notification->id);
                    $notification->updated_at = date('Y-m-d H:i:s');
                } else {
                    $notification = new Notification;
                }
                //$notification = new Notification;
                $notification->language_id = ($Question->language_id != null ? $Question->language_id : '');
                $notification->expert_id = ($Question->expert_id != null ? $Question->expert_id : '');
                $notification->appuser_id = ($Question->appuser_id != null ? $Question->appuser_id : '');
                $notification->question_id = ($Question->id != null ? $Question->id : '');
                $notification->type_id = 1;
                $notification->is_read = 0;
                $notification->message = $msg;
                $notification->updated_at = date('Y-m-d H:i:s');
                $notification->save();

                $send = General::pushNotification($notification->expert_id, $notification->appuser_id, $msg, 1, $Question->id);

                $appuserObj = AppUser::where('id', $Question->appuser_id)->first();
                $userObj = User::where('id', $notification->expert_id)->first();
                //$msgEmail = Cache::get('expert_answered') .' '.$appuserObj->username;
                if ($Question->answer != '') {
                    $emailTemplateObj = EmailTemplate::where('id', 8)->where('language_id', 1)->first();

                    $subject = $emailTemplateObj->subject;
                    $body = $emailTemplateObj->body;
                    $body = str_replace('[name]', $appuserObj->username, $body);
                    $body = str_replace('[userName]', $userObj->first_name . ' ' . $userObj->last_name, $body);

                    //$msgEmail = $userObj->first_name.' '. $userObj->last_name.' '.Cache::get('expert_answered');
                    $to = $appuserObj->email;
                    $name = $appuserObj->username;
                } else {
                    // $msgEmail = Cache::get('expert_question_assigned').' '.$appuserObj->username;
                    $emailTemplateObj = EmailTemplate::where('id', 7)->where('language_id', 1)->first();

                    $subject = $emailTemplateObj->subject;
                    $body = $emailTemplateObj->body;

                    $body = str_replace('[name]', $userObj->first_name, $body);
                    $body = str_replace('[userName]', $appuserObj->username, $body);

                    $to = $userObj->email;
                    $name = $userObj->first_name;
                    //$subject = "Expert Assign";
                }

                $emailFrom = Setting::where('slug', 'email')->first();
                $data = array("name" => $name, 'body' => $body, "to" => $to, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'FRONT_URL' => url(''));

                $res = Mail::send('emails.expertAssingQuestion', ['data' => $data], function($Emailmessage) use ($data) {
                            $Emailmessage->from($data['from'], $data['emailLabel']);
                            $Emailmessage->to($data['to'], $data['name']);
                            $Emailmessage->subject($data['subject']);
                        });

                // if($Question->answer == ''){
                //   if($Question->language_id == 1){
                //   $emailTemplateObj =  EmailTemplate::where('id',19)->where('language_id',1)->first();
                //   }else{
                //   $emailTemplateObj =  EmailTemplate::where('id',20)->where('language_id',2)->first();
                //   }
                // $emailFrom = Setting::where('slug','email')->first();
                // $subject  = $emailTemplateObj->subject;
                // $body     = $emailTemplateObj->body;
                // $body     =  str_replace('[name]',$userObj->first_name,$body);
                // $body     =  str_replace('[userName]',$appuserObj->username,$body);
                // $data = array("name"=>$userObj->first_name,'body' => $body,"to"=>$appuserObj->email,"subject"=>"Assign To Expert","from" => $emailFrom->value, "emailLabel" => $subject,'FRONT_URL' => url(''));
                // $res =Mail::send('emails.expertAssingQuestion',['data'=>$data],function($Emailmessage) use ($data){
                //             $Emailmessage->from($data['from'],$data['emailLabel']);
                //             $Emailmessage->to($data['to'],$data['name']);
                //             $Emailmessage->subject($data['subject']);
                // });
                // }
            }
            return redirect('panel/question')->with('success', 'Data submitted successfully');
        }
    }

    public function updatemultirecode(Request $request) {

        if ($request->action != '' && count($request->checkaction) > 0) {

            if ($request->action == 'inactive') {

                if (count($request->checkaction) > 0) {
                    foreach ($request->checkaction as $key => $inactive) {
                        $Question = Question::find($inactive);
                        $Question->status = 0;
                        $Question->save();
                    }
                }
                return redirect('/panel/question')->with('success', "Question Inactive successfully.");
            } else if ($request->action == 'active') {

                if (count($request->checkaction) > 0) {
                    foreach ($request->checkaction as $key => $active) {
                        $Question = Question::find($active);
                        $Question->status = 1;
                        $Question->save();
                    }
                }
                return redirect('/panel/question')->with('success', "Question Active successfully.");
            } elseif ($request->action == 'delete') {

                if (count($request->checkaction) > 0) {
                    foreach ($request->checkaction as $key => $delete) {
                        $Question = Question::find($delete);
                        $Question->delete();
                    }
                }
                return redirect('/panel/question')->with('success', "Question Detete successfully.");
            }
        } else {
            return redirect('/panel/question')->with('error', 'Something went wrong please try later');
        }
    }

    //delete language keyword
    public function delete(Request $request, $id = 0) {
        $pageID = Session::get('AppQuestionPage');
        Session::put('AppQuestionPage', $pageID);
        $Question = Question::find($id);
        if (count($Question) > 0) {
            if ($Question->delete()) {
                $notification = Notification::where('type_id', 1)->where('question_id', $id)->first();
                if (!empty($notification)) {
                    Notification::where('type_id', 1)->where('question_id', $id)->delete();
                }
                return redirect('/panel/question')->with('success', "Question deleted successfully.");
            } else {
                return redirect('/panel/question')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/question')->with('error', trans('Record not found'));
    }

    public function arrayData(Datatables $datatables) {

        $id = Auth::user()->id;

        // $builder = Question::query()
        //                 ->select('id', 'language_id', 'expert_id', 'category_id','topic_id','question','answer','status','created_at','is_popular')
        //                 ->where(function($query) use ($id)              {
        //                   if($id == 1) {
        //                       $query->orderBy('id', 'desc');
        //                   }else{
        //                       $query->where('expert_id',$id)->orderBy('id', 'desc');
        //                   }
        //                });

        $builder = DB::table('question')
                ->leftjoin('user', 'user.id', '=', 'question.expert_id')
                ->select('question.*')
                ->where(function($query) use ($id) {
            if ($id == 1) {
                $query->orderBy('question.id', 'desc');
            } else {
                $query->where('question.expert_id', $id)->orderBy('question.id', 'desc');
            }
        });


        //return $datatables->eloquent($builder)
        return Datatables::of($builder)
                        ->addColumn('checkbox', function ($Question) {
                            return "<input class='' type='checkbox' id='checkaction" . $Question->id . "' name='checkaction[]' value=" . $Question->id . " onclick='onclickcheck(" . $Question->id . ");'>";
                        })
                        ->editColumn('id', function ($Question) {
                            return $Question->id;
                        })
                        ->editColumn('expert_id', function ($Question) {
                            $user = "";
                            if ($Question->expert_id > 0) {
                                $user = User::where('id', $Question->expert_id)->first()->first_name . ' ' . User::where('id', $Question->expert_id)->first()->last_name;
                            }
                            return "<a href=" . url('panel/question/edit/' . $Question->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $user . "
                                </a>";
                        })
                        ->editColumn('question', function ($Question) {
                            return "<a href=" . url('panel/question/edit/' . $Question->id) . " class=\" btn-link btn-sm\" title='Edit'>
                                    " . $Question->question . "
                                </a>";
                        })
                        ->editColumn('status', function($Question) {
                            if ($Question->status == 1) {
                                return '<span class="badge bg-green">Active</span>';
                            } else if ($Question->status == 0) {
                                return '<span class="badge bg-red">Inactive</span>';
                            } else {
                                return '<span class="badge bg-yellow">Delete</span>';
                            }
                        })
                        ->addColumn('answer', function($Question) {

                            $html = '';
                            if (!empty($Question->answer)) {
                                $html .= '<img src="../img/likeIcon.png">';
                            }
                            return $html;
                        })
                        ->addColumn('action', function($Question) {
                            $Assign = '';
                            if (Auth::user()->id == '1') {
                                $Assign = " <a href='javascript:void(0)' class=\"btn btn-primary submit\" title='Assign' data-toggle='modal' data-id=" . $Question->id . "\" data-target='#ExperienceModel'><i class=\"fa fa-assign\"></i>Expert Assign</a>";
                            }
                            $html = '';

                            $html .= "<a href=" . url('panel/question/edit/' . $Question->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>";
                            if (Auth::user()->id == '1') {
                                $html .= " <a href=" . url('panel/question/delete/' . $Question->id) . " class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>" . $Assign;
                            }
                            $html .= '<img src="../img/view.png" style="width: 40px;">' . $Question->is_popular . "</i>";
                            return $html;
                        })
                        ->rawColumns(['checkbox', 'id', 'expert_id', 'question', 'answer', 'status', 'action'])
                        ->toJson();
    }

    public function getCategoryByLanguage(Request $request) {
        $category = Category::where("language_id", $request->id)->select('id', 'category')->where('is_parent', 0)->get()->toArray();
        return $category;
    }

    public function getTopicByLanguage(Request $request) {
        $topic = Topic::where("language_id", $request->id)->select('id', 'topic')->get()->toArray();
        return $topic;
    }

    public function getParentCategory(Request $request) {
        $subCategory = Category::where("is_parent", $request->id)->select('id', 'category')->get()->toArray();
        return $subCategory;
    }

}
