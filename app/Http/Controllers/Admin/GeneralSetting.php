<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Setting;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use Session;
use Excel;

class GeneralSettingController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        // echo "HEREr";die;
    }

    public function index() {
        Session::forget('AppQuestionPage');
        Session::forget('AppUserPage');
        $dataSettingObj = Setting::get();
        return view('Admin.general_setting.add', ['dataSetting' => $dataSettingObj]);
    }

    public function create() {
        $userRolesArr = Role::pluck('name', 'id')->toArray();
        return view('Admin.user.add', ['user_role' => $userRolesArr]);
    }

    public function store(Request $request) {

        // echo "<pre>";print_r($request->all());die;

        $isSave = false;
        $settingObj = "";
        foreach ($request->setting as $key => $fieldValue) {
            // echo "<pre>";echo $fieldValue['appname'];die;
            $settingObj = Setting::find($key);
            foreach ($fieldValue as $field => $value) {

                if ($field == "sandbox_pem" || $field == "live_pem") {
                    //echo "<pre>";print_r($value);die;

                    $path = public_path('uploads/pem/');
                    $fileName = time() . '_setting_' . $value->getClientOriginalName();

                    $settingObj->value = $fileName;
                    //copy($value->getRealPath(),$path.$fileName);
                    $value->move($path, $fileName);
                } elseif ($field == "video-consultaion") {
                    $path = public_path('uploads/slider/');
                    $fileName = time() . '_setting_' . $value->getClientOriginalName();

                    $settingObj->value = $fileName;
                    //copy($value->getRealPath(),$path.$fileName);
                    $value->move($path, $fileName);
                } else {
                    $settingObj->value = $value;
                    $settingObj->updated_at = date('Y-m-d H:i:s');
                }

                if ($settingObj->save()) {
                    $isSave = true;
                }
            }

            /* $settingObj->value      = $value;
              $settingObj->updated_at = date('Y-m-d H:i:s');
              if($settingObj->save()){
              $isSave = true;
              } */
        }

        if ($isSave) {
            return redirect('/panel/general-setting')->with('success', "Setting saved successfully.");
        } else {
            return redirect('/panel/general-setting')->with('success', "something went wrong to save the settings. please try later.");
        }
    }

}
