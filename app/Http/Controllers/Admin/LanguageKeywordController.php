<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\LanguageKeyword;
use App\Models\LanguageMulti;
use App\Models\Language;
use Html;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
/* To download data in excel */
use Excel;
use Session;
use DB;

class LanguageKeywordController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        Session::forget('AppQuestionPage');
        Session::forget('AppUserPage');
        $languages = Language::get();
        $LanguageKeyword = LanguageKeyword::get();
        $langId = 0;
        return view('Admin.language_keyword.addeditLang', ["languages" => $languages, "LanguageKeyword" => $LanguageKeyword, "langId" => $langId]);
    }

    // return add view language keyword
    public function add(Request $request) {

        $languages = Language::get()->toArray();


        return view('Admin.language_keyword.addLang', ["languages" => $languages]);
    }

    //return edit view language keyword
    /* public function edit(Request $request,$id = 0) {

      if ($id > 0) {

      $perLanguage = LanguageKeyword::find($id);
      $languages = Language::get()->toArray();
      if (!empty($perLanguage) > 0) {
      return View::make('Admin.language_keyword.editLang',['LanguageKeyword'=> $perLanguage,"languages" => $languages]);
      }
      return View::make('Admin.error.404');
      }

      } */

    public function edit(Request $request, $id = 0) {


        if ($id > 0) {

            $LanguageKeyword = LanguageKeyword::where("language_id", $id)->get();

            $languages = Language::get();
            //$LanguageKeyword = LanguageKeyword::get();
            return view('Admin.language_keyword.addeditLang', ["languages" => $languages, "LanguageKeyword" => $LanguageKeyword, "langId" => $id]);
        }
    }

    public function getlangkeyword(Request $request) {
        $LanguageKeyword = LanguageKeyword::where("language_id", $request->langId)->get();

        return view('Admin.language_keyword.getlangkeyword', ['LanguageKeyword' => $LanguageKeyword]);
    }

    //add and edit language keyword
    public function store(Request $request) {

        $this->validate($request, [
            //"keyword"             => "required|unique:language_keyword,keyword,".$request->id,'id',
            "keyword" => "required",
            "label" => "required"
                ], [
            "keyword.required" => "Keyword is required.",
            "label.required" => "label is required.",
                //"keyword.unique"      => "Keyword is already exist"
        ]);

        // check validation
        if (!isset($this->validate)) {

            $languageKey = new LanguageKeyword();
            $languageKey->keyword = $request->keyword;
            $languageKey->label = $request->label;
            $languageKey->language_id = $request->language_id;
            $languageKey->status = 1;
            $languageKey->created_at = date('Y-m-d H:i:s');
            $languageKey->save();
            return redirect('/panel/language/keyword/edit/' . $request->language_id);
        } else {
            return redirect('/panel/language/keyword/edit/' . $request->language_id)->with('error', 'Something went wrong please try later');
        }
    }

    public function multiStore(Request $request) {


        if (count($request->keyword) > 0) {

            foreach ($request->keyword as $id => $keyword) {

                foreach ($keyword as $languid => $value) {

                    $LanguageKeyword = LanguageKeyword::find($id);
                    $LanguageKeyword->updated_at = date('Y-m-d H:i:s');
                    $LanguageKeyword->language_id = $languid;
                    $LanguageKeyword->keyword = $value;
                    $LanguageKeyword->save();
                }
            }

            foreach ($request->label as $id => $label) {

                foreach ($label as $languid => $value) {

                    $LanguageKeyword = LanguageKeyword::find($id);
                    $LanguageKeyword->updated_at = date('Y-m-d H:i:s');
                    $LanguageKeyword->language_id = $languid;
                    $LanguageKeyword->label = $value;
                    $LanguageKeyword->save();
                }
            }

            return redirect('/panel/language/keyword/edit/' . $languid)->with('success', "Language keyword successfully.");
        } else {
            return redirect('/panel/language/keyword/edit/' . $languid)->with('error', 'Something went wrong please try later');
        }
    }

    //delete language keyword
    public function delete(Request $request, $id = 0) {

        $languageKey = LanguageKeyword::find($id);
        if (!empty($languageKey) > 0) {

            if ($languageKey->destroy($id)) {
                // return redirect('/language/keyword')->with('success', "Language deleted successfully.");
                return "true";
            } else {
                return redirect('/panel/language/keyword')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/language/keyword')->with('error', trans('Record not found'));
    }

    public function arrayData(Datatables $datatables) {
        $builder = LanguageKeyword::query()->select('id', 'language_id', 'keyword', 'label', 'status');

        return $datatables->eloquent($builder)
                        ->editColumn('language_id', function ($LanguageKeyword) {
                            return "<a href=" . url('/panel/language/keyword/edit/' . $LanguageKeyword->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . Language::where('id', $LanguageKeyword->language_id)->first()->name . "
                                </a>";
                        })
                        ->editColumn('keyword', function ($LanguageKeyword) {
                            return "<a href=" . url('/panel/language/keyword/edit/' . $LanguageKeyword->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $LanguageKeyword->keyword . "
                                </a>";
                        })
                        ->editColumn('label', function ($LanguageKeyword) {
                            return "<a href=" . url('/panel/language/keyword/edit/' . $LanguageKeyword->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $LanguageKeyword->label . "
                                </a>";
                        })
                        ->editColumn('status', function($LanguageKeyword) {
                            if ($LanguageKeyword->status == 1) {
                                return '<span class="badge bg-green">Active</span>';
                            } else if ($LanguageKeyword->status == 0) {
                                return '<span class="badge bg-red">Inactive</span>';
                            } else {
                                return '<span class="badge bg-yellow">Delete</span>';
                            }
                        })
                        ->addColumn('action', function($LanguageKeyword) {
                            return "<a href=" . url('/panel/language/keyword/edit/' . $LanguageKeyword->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>
                              <a href=" . url('/panel/language/keyword/delete/' . $LanguageKeyword->id) . " class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                        })
                        ->rawColumns(['language_id', 'keyword', 'label', 'action', 'status'])
                        ->toJson();
    }

}
