<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\AuthenticateUser;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Tests\Session\Flash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Auth;
use DB;
use Validator;
use Redirect;
use View;
use App\Models\User;
// use App\Model\AdminRole;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    /* public function __construct() {
      $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
      } */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    // 'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    public function __construct()
    {
//         $this->middleware('guest:admin', ['except' => 'logout']);
    }
    

    //return admin login view page
    public function index() {
        return View::make('Login.login');
    }
    
    //admin login
    public function loginAuth() {

        // echo bcrypt('admin');exit();

        $input = Input::all();
        $validate = Validator::make(Input::all(), [
                        'email' => 'required|max:32',
                        'password' => 'required|min:3',
                    ] , [
                        'required' => ': fireld is required.',
                        'email' => 'Enter valid email',
                        'min' => [
                            'numeric' => 'The :attribute must be at least :min.',
                        ],
                    ]
        );

        $auth = Auth::guard('panel')->attempt(
                        array(
                    'email' => Input::get('email'),
                    'password' => Input::get('password'),
                        ), true);

        if ($auth) {
            return Redirect::to('/panel/dashboard');
        } else {
            return Redirect::to('/panel/login')->with('error', 'Invalid Email / Username or Password.')->with('email',Input::get('email'));
        }
    }

    public function postLoginPayer() {

        $input = Input::all();
        $admin = 'payer';

        $v = Validator::make(Input::all(), [
                    'email' => 'required|max:32',
                    'password' => 'required|min:3',
                        ], [
                    'required' => ': fireld is required.',
                    'email' => 'Enter valid email',
                    'min' => [
                        'numeric' => 'The :attribute must be at least :min.',
                    ],
                        ]
        );

        $auth = Auth::guard('payer')->attempt(
                        array(
                    'email' => Input::get('email'),
                    'password' => Input::get('password'),
                        ), true);

        if ($auth) {
            return Redirect::to('payer/history');
        } else {
            return Redirect::to('payer/login')->with('error', 'Invalid email and password.');
           
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogout() {
        //Auth::logout();
        Auth::guard('panel')->logout();
        Session::flush();
        return redirect('panel')->with('success', 'You are logged out.');
    }

    public function logoutPayer() {
        Auth::guard('payer')->logout();
        Session::flush();
        return redirect('payer/login')->with('success', 'You are logged out.');
    }

    public function adminLogout(Request $request)
    {
        Auth::logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route( 'login' ));
    }
}
