<?php
namespace App\Http\Controllers\Admin;

/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use Excel;

class RoleController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        // echo "HERE";die;
        // echo Auth::User()->id;die;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('Admin.role.list');
    }

    public function addEditRole(Request $request) {

        $permission             = Permission::get();

        $roleobj                = [];
        $permissionRoleObj      = [];
        $permissionArr          = [];    
        
        foreach ($permission as $key => $value) {
            $permissionArr[$value->module][] = $value;
        }
        // echo "<pre>";print_r($permissionArr);die;
        if(isset($request->id) && $request->id > 0){
            $roleobj            = Role::find($request->id);
            $permissionRoleObj  = PermissionRole::where('role_id',$request->id)->get();
        }
        // echo "<pre>";print_r($permissionRoleObj);die;
        return view('Admin.role.addEdit',['permissionArr'=>$permissionArr,'role'=>$roleobj,'permissionRoleList'=>$permissionRoleObj]);
    }

    public function storeRole(Request $request) {
        // echo "<pre>";print_r($request->all());die;
        
        if(!isset($request->id)) {
            $rules['name'] = 'required|unique:roles,name';
        }else{
            $rules['name'] = 'required';
        }

        $rules = [
            'display_name'  => 'required',
            'description'   => 'required',
            // 'permission'    => 'required'
        ];

        $message = [
            "name.required"         => "Name is required",
            "name.unique"           => "Name address is already exist",
            "display_name.required" => "Display name is required",
            "description.required"  => "Description name is required",
            "permission.required"   => "Permission is required",
        ];

        $validator = Validator::make(Input::all(), $rules,$message);
        if ($validator->fails()) {
            if(Input::get('id') != null && Input::get('id') > 0){
                return Redirect::to('panel/role/edit/'.Input::get('id'))->withErrors($validator);
            } else {
                return Redirect::to('panel/role/add')->withErrors($validator);
            }
        }

        if(isset($request->id)){
            $role = Role::find($request->id);
        }else{
            $role = new Role();
        }

        $role->name         = $request->input('name');
        $role->display_name = $request->input('display_name');
        $role->description  = $request->input('description');
        $role->save();

        $permissionRoleObj = PermissionRole::where('role_id',$role->id)->delete();
        
        if(count($request->input('permission')) > 0){
            foreach ($request->input('permission') as $key => $value) {
                
                $permissionRoleObj = new PermissionRole();
                $permissionRoleObj->permission_id   = !empty($value)?$value:"";
                $permissionRoleObj->role_id         = $role->id;
                $permissionRoleObj->save();
                
            }
        }

        return redirect('/panel/role')->with('success', "Role save successfully.");
    }

    public function roleList(Datatables $datatables) {
         $builder = Role::query()->select('id','name', 'display_name', 'description', 'created_at', 'updated_at')->where('deleted_at',null)->whereNotIn('id',[1]);

        return $datatables->eloquent($builder)
                            ->editColumn('name', function ($role) {
                                return "<a href=".url('panel/role/edit/' . $role->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$role->name."
                                </a>";
                            })
                            ->editColumn('display_name', function ($role) {
                                return "<a href=".url('panel/role/edit/' . $role->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$role->display_name."
                                </a>";
                            })
                            // ->editColumn('description', function ($role) {
                            //     return "<a href=".url('role/edit/' . $role->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                            //         ".$role->description."
                            //     </a>";
                            // })
                           //  ->editColumn('created_at', function ($role) {
                           //      if($role->created_at !== null ) {
                           //          return  date("j-M-y g:m A",strtotime($role->created_at))  ;
                           //      } 
                           //      return '';
                           // })
                           // ->editColumn('updated_at', function ($role) {
                           //      if($role->updated_at !== null ) {
                           //          return  date("j-M-y g:m A",strtotime($role->updated_at))  ;
                           //      }
                           //      return '';
                           // })
                            ->addColumn('action', function($role) {
                                return "<a href=".url('panel/role/edit/' . $role->id)." class=\"btn btn-success btn-sm\" title='Set Permission'><i class=\"fa fa-pencil\"></i></a>";
                            })
                          ->rawColumns(['name','display_name','action'])
                          ->toJson();
    }

    public function destroyRole(Request $request) {
        $roleObj = Role::find($request->id);
        if (count($roleObj) > 0) {
            $roleObj->deleted_at = date('Y-m-d H:i:s');
            if($roleObj->save()){
                return redirect('/panel/role')->with('success', "Role deleted successfully.");
            }else{
                return redirect('/panel/role')->with('error', "Something went wrong to delete the role. Please try later.");
            }
        }else{
            return redirect('/panel/role')->with('error', "Something went wrong to delete the role. Please try later.");
        }
    }

    /*public function setRolePermission() {
        
    }

    public static function getModule(){
        $module = [
                'Users'=> ['add','edit','delete','list'],
                'Roles'=> ['edit','list'],
                'Payers'=> ['add','edit','delete','list'],
                'PayerType' => ['add','edit','delete','list'],
                'DonationType' => ['add','edit','delete','list'],
                'Payments'=>['list'],
                'Bank' => ['add','edit','delete','list'],
                'Enterprise' => ['add','edit','delete','list'],
                'MerchantType' => ['add','edit','delete','list'],
                'Merchant' => ['add','edit','delete','list'],
                'MerchantBankAccount' => ['add','edit','delete','list'],
                'MerchantSmtp' => ['add','edit','delete','list'],
                'ApiSetting' => ['add','edit','delete','list'],
                'ApiUsageSetting' => ['add','edit','delete','list'],
                'Rahebar' => ['add','edit','delete','list'],
                'Reports' => ['list'],
                'Settings'=> ['edit','list'],
                'Visitor'=> ['edit','list'],
                'AashirvadLetter' => ['add','edit','delete','list'],
                'MerchantDonationType' => ['add','edit','delete','list'],
                ];
        return $module;
    }*/
    
}
