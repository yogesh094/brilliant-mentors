<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUser;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Slider;
use App\Models\Cms;
use Illuminate\Support\Facades\Hash;
use App\Models\Language;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Session;

class SliderController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {
        Session::forget('AppQuestionPage');
      Session::forget('AppUserPage');
        return view('Admin.Slider.list');
    }

    public function create() {
        $languages = Language::all();
        return view('Admin.Slider.add',["languages" => $languages ]);

    }

    public function arrayData(Datatables $datatables) 
    {
         $builder = Slider::query()->select('id','language_id','name', 'image_name', 'image_path','title','description','type','status');
        

        return $datatables->eloquent($builder)
                            ->editColumn('id', function ($Slider) {
                                return $Slider->id;
                            })
                            ->editColumn('slider_image', function ($Slider) {
                                return "<img src='".url('/public'.$Slider->image_path)."' class='imagereport'/>";
                            })
                            ->editColumn('title', function ($Slider) {
                                return "<a href=".url('panel/slider/edit/' . $Slider->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$Slider->name."
                                </a>";
                            })
                            ->editColumn('status', function ($Slider) {
                                if($Slider->status == 1){
                                   return '<span class="badge bg-green">Active</span>';
                                } else {
                                    return  '<span class="badge bg-red">InActive</span>';
                                }
                            })
                            
                          ->addColumn('action', function($Slider) {
                            return "<a href=".url('/panel/slider/edit/' . $Slider->id)." class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>
                                                <a href=".url('/panel/slider/destroy/' . $Slider->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                          })
                          ->rawColumns(['id','slider_image','title','action','status'])
                          ->toJson();
    }

    public function store(Request $request) {

        if($request->file('slider_image') != ''){
           $rules = array(
            'slider_image' => 'required|mimes:png,jpg,bmp,gif,tif,jpeg,jpg | max:999999',
            'language_id' => 'required',
            'title' => 'required',
            'name' => 'required'
        );
        }else{
            $rules = array(
            'slider_image' => 'required',
            'language_id' => 'required',
            'title' => 'required',
            'name' => 'required'
        );
        }
       
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        
        if(isset($request->id)) {
            $Slider = Slider::find($request->id);
        } else {
            $Slider = new Slider();
        }
        
        if($request->file('slider_image') != ''){
        $file = $request->file('slider_image');
        $ImagesFileName = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $getRealPath = $file->getRealPath();
        $getSize = $file->getSize();
        $getMimeType = $file->getMimeType();
        $without_ext = preg_replace('/\\.[^.\\s]{3,4}$/', '', $ImagesFileName);
        $ImagesFileName = str_slug(strtolower($without_ext)).'-'.time().'.'.$ext;
        $destinationPath = public_path()."/uploads/slider/";
        $file->move($destinationPath,$ImagesFileName);
        $Slider->image_name      = $ImagesFileName;
        $Slider->image_path      = "/uploads/slider/".$ImagesFileName;
    }
            $Slider->language_id     = Input::get('language_id');
            $Slider->title           = Input::get('title');
            $Slider->description     = Input::get('description');
            $Slider->name            = Input::get('name');
            $Slider->type            = Input::get('type');
            $Slider->status          = Input::get('status');
            
          
            
       
        if($Slider->save()) {
            //section for new user
            if (Input::get('id') != null && Input::get('id') > 0) {
                return redirect('/panel/slider')->with('success', "Slider updated successfully.");
            } else {
                return redirect('/panel/slider')->with('success', "Slider created successfully.");
            }
        } else {
            return redirect('/panel/slider')->with('success', "something went wrong please try later.");
        }
        
    }

    public function edit($id) {
        // echo $id;die;
         $languages = Language::all();
         $Slider    = Slider::find($id);
        if($Slider == null) {
             return View::make('Admin.error.404');
        }       

        return View::make('Admin.Slider.edit',["data" => $Slider,"languages" => $languages]);
    }
      

   public function destroy(Request $request, $id = 0) {

        $SliderObj    = Slider::find($id);
        if (!empty($SliderObj) > 0) {

            if($SliderObj->destroy($id)) {
                return redirect('/panel/slider')->with('success', "Slider deleted successfully.");
            } else {
                return redirect('/panel/slider')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/slider')->with('error', trans('Record not found'));
    }
}


