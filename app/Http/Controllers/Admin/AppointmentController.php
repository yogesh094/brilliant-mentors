<?php

namespace App\Http\Controllers\Admin;
/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\CategoryAppointment;
use App\Models\AppUser;
use App\Models\LanguageMulti;
use App\Models\Topic;
use App\Models\Language;
use App\Models\Appointment;
use App\Models\Notification;
use App\Library\General;
use App\Library\webGeneral;
use App\Models\Setting;
use App\Models\PaymentOrder;
use App\Models\EmailTemplate;
use Carbon\Carbon;
use Html;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
use Cache;
use Mail;
/* To download data in excel */
use Excel;
use Session;
use DB;

class AppointmentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
      Session::forget('AppQuestionPage');
      Session::forget('AppUserPage');
        $User = User::whereNotIn('user_role', [1])->where('status',1)->get();
        return view('Admin.appointment.list',["User" => $User]);
    }

    // return add 
    public function add(Request $request) {
        $languages = Language::all();
        $User = AppUser::all();
        $ExpertUser = User::whereNotIn('user_role', [1])->where('status',1)->get();
        $Category = CategoryAppointment::where("status",1)->get();
        $Topic = Topic::where("status",1)->get();
        $start_time = Setting::where('slug','start_time')->first();
        $end_time   = Setting::where('slug','end_time')->first();
        $getTime    = webGeneral::hoursRange('0'.$start_time->value,$end_time->value);
        return view('Admin.appointment.add',["languages" => $languages,"AppUser" => $User,"Category" => $Category,"Topic" => $Topic,"User" => $ExpertUser,'getTime' => $getTime]);
    }

    //return edit 
    public function edit(Request $request,$id = 0) {
       if ($id > 0) {
            $appointment = Appointment::find($id);
            if (!empty($appointment) > 0) {
                $languages = Language::all();
                $AppUser = AppUser::all();
               $User = User::whereNotIn('user_role', [1])->where('status',1)->get();
                $Category = CategoryAppointment::where('language_id',$appointment->language_id)->where("status",1)->get();
                $Topic = Topic::where('language_id',$appointment->language_id)->where("status",1)->get();
                $start_time = Setting::where('slug','start_time')->first();
                $end_time   = Setting::where('slug','end_time')->first();
                $getTime    = webGeneral::hoursRange('0'.$start_time->value,$end_time->value);
                $select_time = $appointment->start_time.'-'.$appointment->end_time;
                return View::make('Admin.appointment.edit',["languages" => $languages,"AppUser" => $AppUser,"Category" => $Category,"Topic" => $Topic,"User" => $User,'getTime' => $getTime,'select_time' => $select_time])->with('appointment', $appointment);
            }
            return View::make('Admin.error.404');
        }else{
            return View::make('Admin.error.404');
        }
    }

    //add and edit 
    public function store(Request $request) {
        $this->validate($request, [
            //"title"         => "required",
            "description"   => "required",
            "expert_id"     => "required"
        ],
        [
            //"title.required"          => "Description Name is required.",
            "description.required"    => "Description Name is required.",
            "expert_id.required"      => "expert is required"
            
        ]);

        // check validation
        if (!isset($this->validate)) {
           $select_time = explode('-',$request->select_time);
           $appointmentObj = Appointment::where('expert_id',$request->expert_id)->where('booking_date',date('Y-m-d',strtotime($request->booking_date)))->where('start_time',$select_time[0])->where('end_time',$select_time[1])->get();

            if(isset($request->id) && $request->id > 0) {
              if(count($appointmentObj)){
                  return redirect('panel/appointment/edit/'.$request->id)->with('error', "Expert already assigned same date & time.");
                  exit;
                }
              $appointment                = Appointment::find($request->id);
              $appointment->updated_at    = date('Y-m-d H:i:s');
            } else {
              if(count($appointmentObj)){
                  return redirect('panel/appointment/add')->with('error', "Expert already assigned same date & time.");
                  exit;
                }
                $appointment            = new Appointment();
                $appointment->created_at        = date('Y-m-d H:i:s');
            }
            $appointment->language_id       = $request->language_id;
            $appointment->expert_id         = ($request->expert_id != null?$request->expert_id:'');
            $appointment->appuser_id        = $request->appuser_id;
            //$appointment->title             = $request->title;
            $appointment->category_id       = $request->category_id;
            //$appointment->topic_id          = $request->topic_id;
            $appointment->booking_date      = date('Y-m-d',strtotime($request->booking_date));
            $appointment->start_time        = $select_time[0];
            $appointment->end_time          = $select_time[1];
            $appointment->description       = $request->description;
            if($appointment->status == 0){
              if(!empty($request->expert_id) && $request->expert_id > 0){
              $appointment->status          = 1;
              }
            }
            $appointment->save();

            if(!empty($appointment->expert_id) && !empty($appointment->appuser_id)){

              
              $userObj =  User::where('id',$appointment->expert_id)->first();
              $msg = Cache::get('expert_appointment') .' '.$userObj->first_name.' '.$userObj->last_name;
              $notification = Notification::where('question_id',$appointment->id)->where('type_id',0)->first();
              if(count($notification) > 0 ){
                $notification->updated_at = date('Y-m-d H:i:s');
              }else{
                $notification = new Notification;
              }
              $notification->language_id  = ($appointment->language_id!=null?$appointment->language_id:'');
              $notification->expert_id    = ($appointment->expert_id !=null?$appointment->expert_id:'');
              $notification->appuser_id   = ($appointment->appuser_id !=null?$appointment->appuser_id:'');
              $notification->question_id   = ($appointment->id !=null?$appointment->id:'');
              $notification->type_id      = 0;
              $notification->message      = $msg;
              $notification->save();
              $send = General::pushNotification($notification->expert_id,$notification->appuser_id,$msg,0,$appointment->id);

         $appuserObj =  AppUser::where('id',$appointment->appuser_id)->first();
         //$msgEmail = Cache::get('expert_appointment') .' '.$appuserObj->username;
         $userObj = User::where('id',$notification->expert_id)->first();  

         $emailTemplateObj = EmailTemplate::where('id',6)->where('language_id',1)->first();
         $subject   = $emailTemplateObj->subject;
         $body      = $emailTemplateObj->body;

         $body     =  str_replace('[name]',$userObj->first_name,$body);
         $body     =  str_replace('[userName]',$appuserObj->username,$body);
         $body     =  str_replace('[booking_date]',date('d/m/Y',strtotime($appointment->booking_date)),$body);
         $body     =  str_replace('[start_time]',Carbon::parse($appointment->start_time)->format('g:i A'),$body);
         $body     =  str_replace('[end_time]',Carbon::parse($appointment->end_time)->format('g:i A'),$body);

          // $msgEmail = "Admin assigned your consultation with ".$appuserObj->username." on ".date('d/m/Y',strtotime($appointment->booking_date))." at ".Carbon::parse($appointment->start_time)->format('g:i A')." to ".Carbon::parse($appointment->end_time)->format('g:i A').". For more details please Login in your Expert Account.";
          
         $emailFrom = Setting::where('slug','email')->first();
          $data = array("name"=>$userObj->first_name,'body' => $body,"to"=>$userObj->email,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'FRONT_URL' => url(''));

        $res =Mail::send('emails.expertAssingConsultation',['data'=>$data],function($Emailmessage) use ($data){
                    $Emailmessage->from($data['from'],$data['emailLabel']);
                    $Emailmessage->to($data['to'],$data['name']);
                    $Emailmessage->subject($data['subject']);
        });
            }

            return redirect('panel/appointment')->with('success', "Data submitted successfully.");
        }


    }
    //delete 
    public function delete(Request $request, $id = 0) {

        $Appointment    = Appointment::find($id);
        if (count($Appointment) > 0) {

            if($Appointment->delete()) {
               $notification = Notification::where('type_id',0)->where('question_id',$id)->first();
              if(!empty($notification)){
                Notification::where('type_id',0)->where('question_id',$id)->delete();
              }
                return redirect('/panel/appointment')->with('success', "Appointment deleted successfully.");
            } else {
                return redirect('/panel/appointment')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/appointment')->with('error', trans('Record not found'));
    }


    public function arrayData(Datatables $datatables) {

        $id = Auth::user()->id;
        // $builder = Appointment::query()->select('id', 'language_id', 'appuser_id','category_id','booking_date','start_time','end_time','topic_id','title','description','status','payment_status','expert_id')
        //     ->where(function($query) use ($id)              {
        //       if($id != 1) {
        //       $query->where('expert_id',$id);
        //       }
        //       })
        //       ->orderBy('id', 'desc');

        $builder = DB::table('appointment')
                    ->leftjoin('appuser','appuser.id','=','appointment.appuser_id')
                    ->leftjoin('category_appointment','category_appointment.id','=','appointment.category_id')
                    ->leftjoin('user','user.id','=','appointment.expert_id')
                    ->select('appointment.*')
                    ->where(function($query) use ($id){
                      if($id != 1) {
                        $query->where('appointment.expert_id',$id);
                      }
                    })
                  ->orderBy('id', 'desc');


                           
        //return $datatables->eloquent($builder)
        return Datatables::of($builder)
        ->addColumn('checkbox', function ($appointment) {
                                return "<input class='' type='checkbox' id='checkaction".$appointment->id."' name='checkaction[]' value=".$appointment->id." onclick='onclickcheck(".$appointment->id.");'>";
                    })
                    ->editColumn('id', function ($appointment) {
                                return $appointment->id;
                            })
                    ->editColumn('expert_id', function ($appointment) {
                                $user = ""; 
                                if($appointment->expert_id > 0){
                                    $user = User::where('id',$appointment->expert_id)->first()->first_name.' '.User::where('id',$appointment->expert_id)->first()->last_name;
                                }
                                // return "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                //     ".$user."
                                // </a>";
                                return "<a href=\"javascript:void(0)\" class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$user."
                                </a>";
                            })
                    ->editColumn('language_id', function ($appointment) {
                                $getlanguageName = Language::where('id',$appointment->language_id)->pluck('name')->first();
                                 if($getlanguageName !== null ) {
                                    // return "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    //         ".$getlanguageName."
                                    //     </a>";
                                  return "<a href=\"javascript:void(0)\" class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$getlanguageName."
                                </a>";
                                }
                            })
                    ->editColumn('title', function ($appointment) {
                                // return "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                //     ".$appointment->title."
                                // </a>";

                                return "<a href=\"javascript:void(0)\" class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$appointment->title."
                                </a>";
                            })
                    ->editColumn('appuser_id', function ($appointment) {
                                $getData = AppUser::where('id',$appointment->appuser_id)->select('username','email')->first();
                                $setname = ($getData->username != null?$getData->username:$getData->email);
                                if($setname != null){
                                    // return "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    //     ".$setname."
                                    // </a>";

                                  return "<a href=\"javascript:void(0)\" class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$setname."
                                    </a>";
                                }
                            })
                      ->editColumn('category_id', function ($appointment) {
                                $getCategoryName = CategoryAppointment::where('id',$appointment->category_id)->pluck('category')->first();
                                 if($getCategoryName !== null ) {
                                    // return "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    //         ".$getCategoryName."
                                    //     </a>";
                                  return "<a href=\"javascript:void(0)\" class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$getCategoryName."
                                    </a>";
                                }
                            })
                      ->editColumn('topic_id', function ($appointment) {
                                $getTopicName = Topic::where('id',$appointment->topic_id)->pluck('topic')->first();
                                 if($getTopicName !== null ) {
                                    // return "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    //         ".$getTopicName."
                                    //     </a>";
                                  return "<a href=\"javascript:void(0)\" class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$getTopicName."
                                    </a>";
                                }
                            })
                      ->editColumn('booking_date', function ($appointment) {
                                $formatDate = date('d-M-Y',strtotime($appointment->booking_date));
                                // return "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                //     ".$formatDate."
                                // </a>";

                                return "<a href=\"javascript:void(0)\" class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$formatDate."
                                    </a>";
                            })
                      ->editColumn('start_time', function ($appointment) {
                                // return "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                //     ".$appointment->start_time." to ".$appointment->end_time."
                                // </a>";

                              return "<a href=\"javascript:void(0)\" class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$appointment->start_time." to ".$appointment->end_time."
                                    </a>";
                            })
                      ->editColumn('description', function ($appointment) {
                                // return "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                //     ".$appointment->description."
                                // </a>";

                              return "<a href=\"javascript:void(0)\" class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$appointment->description."
                                    </a>";
                            })
                      ->editColumn('payment_status',function($appointment) {
                                 if($appointment->payment_status == 1){
                                   return '<span class="badge bg-green">Approval</span>';
                                } else {
                                   return  '<span class="badge bg-black">Not Approval</span>';
                                }
                            })
                        ->editColumn('status',function($appointment) {
                                 if($appointment->status == 1){
                                   return '<span class="badge bg-green">Assigned</span>';
                                } else if($appointment->status == 0){
                                   return  '<span class="badge bg-red">Not Assign</span>';
                                } else if($appointment->status == 2){
                                   return '<span class="badge bg-blue-sky">Completed</span>'; 
                                }else if($appointment->status == 3){
                                  return '<span class="badge bg-black">Cancel</span>'; 
                                }else if($appointment->status == 4){
                                  return '<span class="badge bg-black">Expert Cancel</span>'; 
                                }
                            })
                          ->addColumn('action', function($appointment) {
                            $Assign = '';
                            $html = '';
                            if($appointment->status != 2 && $appointment->status != 3 && $appointment->status != 4){
                              if(Auth::user()->id == '1'){
                                $Assign = " <a href='javascript:void(0)' class=\"btn btn-primary submit\" title='Assign' data-toggle='modal' data-id=".$appointment->id." data-target='#expertModal'><i class=\"fa fa-assign\"></i>Expert Assign</a>";
                              }
                              $html .= "<a href=".url('panel/appointment/edit/' . $appointment->id)." class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>";
                            }
                            
                            if(Auth::user()->id == '1'){
                            $html .= " <a href=".url('panel/appointment/delete/' . $appointment->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>".$Assign;
                            }
                            if($appointment->payment_status == 1 && $appointment->status == 1 ){
                                $html .= "<a href=".url('panel/appointment/webrtc/' . $appointment->id)." class=\"btn btn-primary submit\" title='Video Call'>Video Call</a>";
                            }
                            if($appointment->status != 3 && $appointment->status != 4){
                              $html .= "<a href=".url('panel/appointment/cancel/' . $appointment->id)." class=\"btn btn-primary submit\" title='Cancel'>Cancel</a>";
                            }
                            
                            return $html;
                          })
                          ->rawColumns(['checkbox','expert_id','id','language_id','title','appuser_id','category_id','topic_id','booking_date','start_time','description','status','action','payment_status'])
                          ->toJson();
    }
 public function updatemultirecode(Request $request){
      
        if($request->action != '' && count($request->checkaction) > 0){

            if($request->action == 'inactive'){

             if(count($request->checkaction) > 0){
                foreach ($request->checkaction as $key => $inactive) {
                     $Appointment            = Appointment::find($inactive);
                     $Appointment->status    = 0;
                     $Appointment->save();
                }
             }
             return redirect('/panel/appointment')->with('success', "Appointment Inactive successfully.");

        }else if($request->action == 'active'){

            if(count($request->checkaction) > 0){
                foreach ($request->checkaction as $key => $active) {
                     $Appointment            = Appointment::find($active);
                     $Appointment->status    = 1;
                     $Appointment->save();
                }
             }
            return redirect('/panel/appointment')->with('success', "Appointment Active successfully.");
        }elseif ($request->action == 'delete') {

             if(count($request->checkaction) > 0){
                foreach ($request->checkaction as $key => $delete) {
                     $Appointment  = Appointment::find($delete);
                     $Appointment->delete();
                }
             }
            return redirect('/panel/appointment')->with('success', "Appointment Detete successfully.");
        }
    }else{
        return redirect('/panel/appointment')->with('error', 'Something went wrong please try later');
    }
        
    }
    public function getCategoryByLanguage(Request $request){
        $category = CategoryAppointment::where("language_id", $request->id)->select('id','category')->get()->toArray();
        return $category;
    }

    public function getTopicByLanguage(Request $request){
        $topic = Topic::where("language_id", $request->id)->select('id','topic')->get()->toArray();
        return $topic;
    }

    public function assignAppoinment(Request $request) {

    $appointmentObj = Appointment::where('expert_id',$request->expert_id)->where('booking_date',$request->booking_date)->where('start_time',$request->start_time)->where('end_time',$request->end_time)->get();
      if(count($appointmentObj) > 0){
          return response()->json(['status' => false, 'error'   => 401,'message' => 'Expert already assigned same date & time.']);
          exit;
        }else{
        if(isset($request->id) && $request->id > 0) {
            $appointment                = Appointment::find($request->id);
            $appointment->updated_at    = date('Y-m-d H:i:s');
        }

          $appointment->expert_id  = $request->expert_id;
          if($appointment->status == 0){
            $appointment->status     = 1;
          }
          $appointment->save();

        if(!empty($appointment->expert_id) && !empty($appointment->appuser_id)){

              $userObj =  User::where('id',$appointment->expert_id)->first();
              $msg = Cache::get('expert_appointment') .' '.$userObj->first_name.' '.$userObj->last_name;
              $notification = Notification::where('question_id',$appointment->id)->where('type_id',0)->first();
              if(count($notification) > 0 ){
                $notification->updated_at = date('Y-m-d H:i:s');
              }else{
                $notification = new Notification;
              }
              $notification->language_id  = ($appointment->language_id!=null?$appointment->language_id:'');
              $notification->expert_id    = ($appointment->expert_id !=null?$appointment->expert_id:'');
              $notification->appuser_id   = ($appointment->appuser_id !=null?$appointment->appuser_id:'');
              $notification->question_id   = ($appointment->id !=null?$appointment->id:'');
              $notification->type_id      = 0;
              $notification->message      = $msg;
              $notification->save();
              $send = General::pushNotification($notification->expert_id,$notification->appuser_id,$msg,0,$appointment->id);

         $appuserObj =  AppUser::where('id',$appointment->appuser_id)->first();
        //$msgEmail = Cache::get('expert_appointment') .' '.$appuserObj->username;
         //Carbon::parse($value['start_time'])->format('g:i A').'-'.Carbon::parse($value['end_time'])->format('g:i A')
         $userObj = User::where('id',$notification->expert_id)->first();  
         
         $emailTemplateObj = EmailTemplate::where('id',6)->where('language_id',1)->first();
         $subject   = $emailTemplateObj->subject;
         $body      = $emailTemplateObj->body;

         $body     =  str_replace('[name]',$userObj->first_name,$body);
         $body     =  str_replace('[userName]',$appuserObj->username,$body);
         $body     =  str_replace('[booking_date]',date('d/m/Y',strtotime($appointment->booking_date)),$body);
         $body     =  str_replace('[start_time]',Carbon::parse($appointment->start_time)->format('g:i A'),$body);
         $body     =  str_replace('[end_time]',Carbon::parse($appointment->end_time)->format('g:i A'),$body);


         // $msgEmail = "Admin assigned your consultation with ".$appuserObj->username." on ".date('d/m/Y',strtotime($appointment->booking_date))." at ".Carbon::parse($appointment->start_time)->format('g:i A')." to ".Carbon::parse($appointment->end_time)->format('g:i A').". For more details please Login in your Expert Account.";

         
        $emailFrom = Setting::where('slug','email')->first();
        $data = array("name"=>$userObj->first_name,'body' => $body,"to"=>$userObj->email,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'FRONT_URL' => url(''));

        $res =Mail::send('emails.expertAssingConsultation',['data'=>$data],function($Emailmessage) use ($data){
                    $Emailmessage->from($data['from'],$data['emailLabel']);
                    $Emailmessage->to($data['to'],$data['name']);
                    $Emailmessage->subject($data['subject']);
        });
            }
        if($appointment->id > 0){
              return response()->json([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => "success",
                    ]);
            exit;
            }else{
               return response()->json(['status' => false, 'error'   => 401,'message' => 'Something went wrong please try later']);
             exit;
            }
        }
        
       // return redirect('panel/appointment')->with('success', "Appointment assigned successfully.");
    }

    public function getAppoinment(Request $request){
        $AppointmentArr = [];
        $Appointment = Appointment::where("id", $request->id)
                                ->get()
                                ->first();

        $AppointmentArr = array(
                    'id' => $Appointment->id,
                    'expert_id' => $Appointment->expert_id,
                    'booking_date' => $Appointment->booking_date,
                    'start_time' => $Appointment->start_time,
                    'end_time' => $Appointment->end_time,
                );
        return $AppointmentArr;
    }

 public function webrtc(Request $request,$id){
        
        $random = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $Appointment = Appointment::find($id);
        $getData = AppUser::where('id',$Appointment->appuser_id)->select('username','email')->first();
         //$username = $getData->username;
        $username = Auth::user()->username;
        $room = $Appointment->room;
        $pushNotificationVoipUrl = url('panel/appointment/customeNotification');
        
     return view('Admin.webrtc.webrtc',['username' => $username,'room' => $room,'id' => $id,'appuser_id' => $Appointment->appuser_id,'pushNotificationVoipUrl' => $pushNotificationVoipUrl]);
    }
 public function customeNotificationIOS(Request $request){
     General::pushNotificationVoip($request->appuserid);
  }
 public function updateAppoinmentStatus(Request $request){
  if(isset($request->id) && $request->id > 0){
    $appointmentobj = Appointment::find($request->id);
    $appointmentobj->status = 2;
    $appointmentobj->save();
       return response()->json([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => "Update status successfully...",
                    ]);
            exit;
  }else{
    return response()->json(['status' => false, 'error'   => 401,'message' => 'Something went wrong please try later']);
  }
 }
 public function webrtcLogout(){
    Session::flush();
    Auth::logout();
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0"); // Proxies.
    session()->flash('alert-success', 'Success logged out');
    return Redirect::to('/panel/appointment');
 }

 public function cancelAppointment(Request $request,$id){
    $appointmentObj = Appointment::find($id);
    $appointmentObj->status = 4;
    $appointmentObj->cancel_type = null;
    if($appointmentObj->save()){

      if($appointmentObj->language_id == 1){
          $emailTemplateObj = EmailTemplate::where('id',13)->first();
      }else{
          $emailTemplateObj = EmailTemplate::where('id',14)->first();
      }
      $appUser = AppUser::where('id',$appointmentObj->appuser_id)->first();
      $userObj = User::where('id',$appointmentObj->expert_id)->first();
       $name = "admin";
      if(!empty($userObj)){
        $name = $userObj->first_name.' '.$userObj->last_name;
      }
      $setting  = Setting::where('slug','email')->first();
      $tomail   = $appUser->email;
      $subject  = $emailTemplateObj->subject;
      $body     = $emailTemplateObj->body;
      $body     =  str_replace('[UserName]',$name,$body);
      $body     =  str_replace('[RescheduleUrl]',url('video-consultaion/reschedule').'/'.$appointmentObj->id,$body);
      $body     =  str_replace('[Refund]',url('refund').'/'.base64_encode($appointmentObj->id),$body);
      $data     = array("name"=>$appUser->username,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'body' => $body,'FRONT_URL' => url(''));
        
        $res =Mail::send('emails.appointment-cancel',['data'=>$data],function($message) use ($data){
            $message->from($data['from'],$data['emailLabel']);
            $message->to($data['to'],$data['name']);
            $message->subject($data['subject']);
        });
          $paymentObj= PaymentOrder::where('appointment_id',$appointmentObj->id)->first();

          $PaymentOrder = PaymentOrder::find($paymentObj->id);
          $PaymentOrder->type = 0;
          $PaymentOrder->save();

      $userObj =  User::where('id',$appointmentObj->expert_id)->first();
      if(!empty($userObj)){
        $msg = Cache::get('expert_cancel_appointment').' '.$userObj->first_name.' '.$userObj->last_name;
      }else{
        $msg = Cache::get('expert_cancel_appointment').' admin';
      }
      

      $notification = Notification::where('question_id',$appointmentObj->id)->where('type_id',0)->first();

              if(count($notification) > 0 ){
                $notification = Notification::find($notification->id);
                $notification->updated_at = date('Y-m-d H:i:s');
              }else{
                $notification = new Notification;
              }
              $notification->language_id  = ($appointmentObj->language_id!=null?$appointmentObj->language_id:'');
              $notification->expert_id    = ($appointmentObj->expert_id !=null?$appointmentObj->expert_id:1);
              $notification->appuser_id   = ($appointmentObj->appuser_id !=null?$appointmentObj->appuser_id:'');
              $notification->question_id   = ($appointmentObj->id !=null?$appointmentObj->id:'');
              $notification->type_id      = 0;
              $notification->is_read      = 0;
              $notification->message      = $msg;
              $notification->save();
              
              $send = General::pushNotification($notification->expert_id,$notification->appuser_id,$msg,1,$appointmentObj->id);
              
        return redirect('/panel/appointment')->with('success', trans('Appointment Cancel successfully.'));
    }else{
      return redirect('/panel/appointment')->with('error', trans('Something wrong'));
    }
    
 }

}