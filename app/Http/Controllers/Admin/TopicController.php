<?php

namespace App\Http\Controllers\Admin;
/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\LanguageMulti;
use App\Models\Topic;
use App\Models\Language;
use Html;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
/* To download data in excel */
use Excel;
use Session;
use DB;

class TopicController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $languages = Language::get();
        $Topic = Topic::get();
        $langId = 0;
        return view('Admin.topic.addeditLang',["languages" => $languages,"Topic" => $Topic,"langId" => $langId ]);
    }

    // return add view language keyword
    public function add(Request $request) {
        
        $languages = Language::get()->toArray();
        return view('Admin.topic.addLang',["languages" => $languages]);
    }

    public function edit(Request $request,$id = 0) {
      if ($id > 0) {
           
        $Topic = Topic::where("language_id",$id)->get();
           
        $languages = Language::get();
        //$Topic = Topic::get();
        return view('Admin.topic.addeditLang',["languages" => $languages,"Topic" => $Topic,"langId" => $id]);
        }
        
    }

    public function getlangkeyword(Request $request){
        $Topic = Topic::where("language_id",$request->langId)->get();

        return view('Admin.topic.getlangkeyword',['Topic'=>$Topic]);
    }

   //add and edit language keyword
    public function store(Request $request) {

        $this->validate($request, [
            "topic"             => "required",
            "charges"               => "required"

        ],
        [
            "topic.required"    => "Topic is required.",
            "charges.required"    => "Charges is required.",
            //"keyword.unique"      => "Keyword is already exist"
        ]);

        // check validation
        if (!isset($this->validate)) {

            $topicKey                    = new Topic();
            $topicKey->topic             = $request->topic;
            $topicKey->charges           = $request->charges;
            $topicKey->language_id       = $request->language_id;
            $topicKey->status            = 1;
            $topicKey->created_at        = date('Y-m-d H:i:s');
            $topicKey->save();
            return redirect('/panel/topic/edit/'.$request->language_id);
        }else{
             return redirect('/panel/topic/edit/'.$request->language_id)->with('error', 'Something went wrong please try later');
        }


    }

    public function multiStore(Request $request){
       
       
        if(count($request->topic) > 0) {

               foreach ($request->topic as $id => $topic) {
                   
                   foreach ($topic as $languid => $value) {
                      
                    $Topic            = Topic::find($id);
                    $Topic->updated_at    = date('Y-m-d H:i:s');
                    $Topic->language_id   = $languid;
                    $Topic->topic       = $value;
                    $Topic->save();
                   }
                }

                foreach ($request->charges as $id => $charges) {
                   
                   foreach ($charges as $languid => $value) {
                      
                    $Topic                = Topic::find($id);
                    $Topic->updated_at    = date('Y-m-d H:i:s');
                    $Topic->language_id   = $languid;
                    $Topic->charges       = $value;
                    $Topic->save();
                   }
                }

                return redirect('/panel/topic/edit/'.$languid)->with('success', "Language keyword successfully.");
        }else{
            return redirect('/panel/topic/edit/'.$languid)->with('error', 'Something went wrong please try later');
        }
    }
   
  //delete language keyword
    public function delete(Request $request, $id = 0) {

        $topicKey    = Topic::find($id);
        if (!empty($topicKey) > 0) {

            if($topicKey->destroy($id)) {
               // return redirect('/language/keyword')->with('success', "Language deleted successfully.");
                return "true";
            } else {
                return redirect('/panel/topic/keyword')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/topic/keyword')->with('error', trans('Record not found'));
    }
   
    public function arrayData(Datatables $datatables) {
         $builder = Topic::query()->select('id', 'language_id', 'topic','charges', 'status');

        return $datatables->eloquent($builder)
                         ->editColumn('language_id', function ($Topic) {
                                return "<a href=".url('/panel/topic/edit/' . $Topic->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".Language::where('id',$Topic->language_id)->first()->name."
                                </a>";
                            })
                         ->editColumn('topic', function ($Topic) {
                                return "<a href=".url('/panel/topic/edit/' . $Topic->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$Topic->topic."
                                </a>";
                            })
                         ->editColumn('charges', function ($Topic) {
                                return "<a href=".url('/panel/topic/edit/' . $Topic->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$Topic->charges."
                                </a>";
                            })
                            ->editColumn('status',function($Topic) {
                                 if($Topic->status == 1){
                                   return '<span class="badge bg-green">Active</span>';
                                } else if($Topic->status == 0){
                                   return  '<span class="badge bg-red">Inactive</span>';
                                } else {
                                   return '<span class="badge bg-yellow">Delete</span>'; 
                                }
                            })
                            
                          ->addColumn('action', function($Topic) {
                            return "<a href=".url('/panel/topic/edit/' . $Topic->id)." class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>
                                                <a href=".url('/panel/topic/delete/' . $Topic->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                          })
                          ->rawColumns(['language_id','topic','charges','action','status'])
                          ->toJson();
    }
 
    
}