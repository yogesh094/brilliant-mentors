<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUser;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\EmailTemplate;
use Illuminate\Support\Facades\Hash;
use App\Models\Language;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Session;

class EmailTemplateController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {
     Session::forget('AppQuestionPage');
      Session::forget('AppUserPage');
        return view('Admin.email_template.list');
    }

    public function create() {
        $languages = Language::all();
        return view('Admin.email_template.add',["languages" => $languages ]);

    }

    public function arrayData(Datatables $datatables) 
    {
         $builder = EmailTemplate::query()->select('id','title','subject', 'body', 'status');
        

        return $datatables->eloquent($builder)
        ->editColumn('id', function ($EmailTemplate) {
                                return $EmailTemplate->id;
                            })
                            ->editColumn('title', function ($EmailTemplate) {
                                return "<a href=".url('panel/email-template/edit/' . $EmailTemplate->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                ".$EmailTemplate->title."
                                </a>";
                            })
                            ->editColumn('subject', function ($EmailTemplate) {
                                return "<a href=".url('panel/email-template/edit/' . $EmailTemplate->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$EmailTemplate->subject."
                                </a>";
                            })
                            // ->editColumn('body', function ($EmailTemplate) {
                            //     return "<a href=".url('admin/email-template/edit/' . $EmailTemplate->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                            //         ".$EmailTemplate->body."
                            //     </a>";
                            // })
                            ->editColumn('status', function ($EmailTemplate) {
                                if($EmailTemplate->status == 1){
                                   return '<span class="badge bg-green">Active</span>';
                                } else {
                                    return  '<span class="badge bg-red">InActive</span>';
                                }
                            })
                            
                          ->addColumn('action', function($EmailTemplate) {
                                                                
                            return "<a href=".url('/panel/email-template/edit/' . $EmailTemplate->id)." class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>
                                ";
                          })
                          ->rawColumns(['id','title','subject','body','status','action'])
                          ->toJson();
    }

    public function store(Request $request) {

       
         $this->validate($request, [
            "language_id.required"        => "language is required",
            "title.required"        => "Title is required",
            "subject.required"    => "subject is required" ]);

        // check validation
        if (!isset($this->validate)) {

            $EmailTemplateObj = "";
        
        if(isset($request->id)) {
            $EmailTemplateObj = EmailTemplate::find($request->id);
            $EmailTemplateObj->updated_at       = date('Y-m-d H:i:s');
            
        } else {
            $EmailTemplateObj = new EmailTemplate();
            $EmailTemplateObj->created_at       = date('Y-m-d H:i:s');
        }

            $EmailTemplateObj->language_id     = Input::get('language_id');
            $EmailTemplateObj->title           = Input::get('title');
            $EmailTemplateObj->subject         = Input::get('subject');
            $EmailTemplateObj->body            = Input::get('body');
            $EmailTemplateObj->status          = Input::get('status');
          
            
       
        if($EmailTemplateObj->save()) {
            //section for new user
            if (Input::get('id') != null && Input::get('id') > 0) {
                return redirect('/panel/email-template')->with('success', "Email Template updated successfully.");
            } else {
                return redirect('/panel/email-template')->with('success', "Email Template created successfully.");
            }
        } else {
            return redirect('/panel/email-template')->with('success', "something went wrong please try later.");
        }
        }else{
            return redirect('/panel/email-template')->with('success', "something went wrong please try later.");
        }
        

        
    }

    public function edit($id) {
         $languages = Language::all();
         $EmailTemplateObj    = EmailTemplate::find($id);
        if($EmailTemplateObj == null) {
             return View::make('Admin.error.404');
        }       

        return View::make('Admin.email_template.edit',["data" => $EmailTemplateObj,"languages" => $languages]);
    }
      

   public function destroy(Request $request, $id = 0) {

        $EmailTemplateObj    = EmailTemplate::find($id);
        if (!empty($EmailTemplateObj) > 0) {

            if($EmailTemplateObj->destroy($id)) {
                return redirect('/panel/email-template')->with('success', "Email Template deleted successfully.");
            } else {
                return redirect('/panel/email-template')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/email-template')->with('error', trans('Record not found'));
    }
}


