<?php

namespace App\Http\Controllers\Admin;
/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\PaymentOrder;
use App\Models\AppUser;
use App\Models\Appointment;
use App\Models\Category;
use App\Models\User;
use Html;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
/* To download data in excel */
use Excel;
use Session;
use DB;

class OrdersController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      Session::forget('AppQuestionPage');
      Session::forget('AppUserPage');
        return view('Admin.Orders.list');
    }

    //delete language keyword
    public function delete(Request $request, $id = 0) {
        $PaymentOrder    = PaymentOrder::find($id);
        if (count($PaymentOrder) > 0) {

            if($PaymentOrder->delete()) {
                return redirect('/panel/orders')->with('success', "Payment Order deleted successfully.");
            } else {
                return redirect('/panel/orders')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/orders')->with('error', trans('Record not found'));
    }

    public function view(Request $request, $id = 0){
        $PaymentOrder = PaymentOrder::find($id);
        $getUsernamer = AppUser::where('id',$PaymentOrder->user_id)->select('username','email')->first();
        $response = str_replace("Stripe\Charge JSON:", "", $PaymentOrder->response);
        $response = json_decode($response, true);
        $Appointment = Appointment::where("id",$PaymentOrder->appointment_id)->first();

        $getCategoryName = Category::where('id',$Appointment['category_id'])->pluck('category')->first();
        $getExpertName = User::where('id',$Appointment['expert_id'])->pluck('username')->first();
        return View::make('Admin.Orders.view',["PaymentOrder" => $PaymentOrder,"response" => $response,"getCategoryName" =>$getCategoryName,"Appointment" => $Appointment,"getExpertName" => $getExpertName,"getUsernamer" => $getUsernamer]);
    }
    public function updatemultirecode(Request $request){
      
        if($request->action != '' && count($request->checkaction) > 0){

            if($request->action == 'inactive'){

             if(count($request->checkaction) > 0){
                foreach ($request->checkaction as $key => $inactive) {
                     $PaymentOrder            = PaymentOrder::find($inactive);
                     $PaymentOrder->status    = 0;
                     $PaymentOrder->save();
                }
             }
             return redirect('/panel/orders')->with('success', "Orders Inactive successfully.");

        }else if($request->action == 'active'){

            if(count($request->checkaction) > 0){
                foreach ($request->checkaction as $key => $active) {
                     $PaymentOrder            = PaymentOrder::find($active);
                     $PaymentOrder->status    = 1;
                     $PaymentOrder->save();
                }
             }
            return redirect('/panel/orders')->with('success', "Orders Active successfully.");
        }elseif ($request->action == 'delete') {

             if(count($request->checkaction) > 0){
                foreach ($request->checkaction as $key => $delete) {
                     $PaymentOrder  = PaymentOrder::find($delete);
                     $PaymentOrder->delete();
                }
             }
            return redirect('/panel/orders')->with('success', "Orders Detete successfully.");
        }
    }else{
        return redirect('/panel/orders')->with('error', 'Something went wrong please try later');
    }
        
    }
    public function arrayData(Datatables $datatables) {
        // $builder = PaymentOrder::query()->select('id','user_id','charge_id', 'customer','amount','balance_transaction','currency','paid','brand','country','funding',
        //     'response','status','created_at')->orderBy('id', 'desc');
      $builder = DB::table('payment_order')
                    ->leftjoin('appuser','appuser.id','=','payment_order.user_id')
                    ->select('payment_order.*')
                   ->orderBy('payment_order.id', 'desc');

        //return $datatables->eloquent($builder)
        return Datatables::of($builder)
        ->addColumn('checkbox', function ($PaymentOrder) {
                                return "<input class='' type='checkbox' id='checkaction".$PaymentOrder->id."' name='checkaction[]' value=".$PaymentOrder->id." onclick='onclickcheck(".$PaymentOrder->id.");'>";
                    })
                        ->editColumn('id', function ($PaymentOrder) {
                                return $PaymentOrder->id;
                            })
                        ->editColumn('charge_id', function ($PaymentOrder) {
                                
                                return  $PaymentOrder->charge_id;
                                    
                            })
                        ->editColumn('user_id', function ($PaymentOrder) {
                                $username = AppUser::where('id',$PaymentOrder->user_id)->pluck('username')->first();
                                return  $username;
                                    
                            })
                            ->editColumn('customer', function ($PaymentOrder) {
                                return $PaymentOrder->customer;
                            })
                            ->editColumn('balance_transaction', function ($PaymentOrder) {
                                return $PaymentOrder->balance_transaction;
                            })
                            ->editColumn('currency',function($PaymentOrder) {
                                 return $PaymentOrder->currency;
                            })
                            ->editColumn('brand',function($PaymentOrder) {
                                 return $PaymentOrder->brand;
                                 
                            })->editColumn('status',function($PaymentOrder) {
                                 
                                if($PaymentOrder->status == "succeeded"){
                                           return '<span class="badge bg-green"><i class="fa fa-check-circle"></i></span>';
                                }else{
                                        return '<span class="badge bg-red"><i class="fa fa-times-circle"></i></span>'; 
                                    }

                            })
                            
                          ->addColumn('action', function($PaymentOrder) {
                           
                            //<a href=".url('admin/donate/view/' . $Donate->id)." class=\"btn btn-success btn-sm\" title='View'><i class=\"fa fa-eye-slash\"></i></a>
                            return "
                            <a href=".url('panel/orders/delete/' . $PaymentOrder->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>
                                                                <a href=".url('panel/orders/view/' . $PaymentOrder->id)." class=\"btn btn-success btn-sm\" title='View'><i class=\"fa fa-eye-slash\"></i></a>";
                          })
                          ->rawColumns(['checkbox','id','charge_id','user_id','customer','balance_transaction','currency','brand','status','action'])
                          ->toJson();
    }
    
}
