<?php

namespace App\Http\Controllers\Admin;
/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Library\General;
use App\Models\User;
use App\Models\Event;
use App\Models\EventType;
use App\Models\EventAppointment;

use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
/*
    To download excel file
*/
use Excel;

class EventController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('Admin.event.listEvent');
    }

    // return add view event
    public function add(Request $request) {

        $timeArr        = General::getTimeData();
        $eventTypeArr   = EventType::where('status',1)->pluck('title', 'id');
        return view('Admin.event.addEvent',["timeArr" => $timeArr,"eventTypeArr" => $eventTypeArr]);
    }

    //return edit view event
    public function edit(Request $request,$id = 0) {
        $event          = Event::find($id);
        $eventTypeArr   = EventType::where('status',1)->pluck('title', 'id');
        $timeArr        = General::getTimeData();
        return view('Admin.event.editEvent',["timeArr" => $timeArr, 'event' => $event  , 'eventTypeArr' => $eventTypeArr ]);    
    }

    //add and edit event
    public function store(Request $request) {

        $this->validate($request, [
            "event_title"                       => "required",
            "event_type"                        => "required"
        ],
        [
            "event_title.required"              => "Event title is required.",
            "event_type.required"               => "Event type is required."
        ]);

        // check validation
        if (!isset($this->validate)) {

            if(isset($request->id) && $request->id > 0) {
                $event                  = Event::find($request->id);
                $event->status          = $request->status;
                $event->updated_by      = Auth::user()->id;
                $event->updated_at      = date('Y-m-d H:i:s');
            } else {
                $event                  = new Event();
                $event->created_by      = Auth::user()->id;
                $event->created_at      = date('Y-m-d H:i:s');
            }

            $request->start_date        =  (isset($request->start_date) && $request->start_date != "")?date('Y-m-d',strtotime($request->start_date)):"";
            $request->end_date          =  (isset($request->end_date) && $request->end_date != "")?date('Y-m-d',strtotime($request->end_date)):"";

            $event->event_title         = $request->event_title;
            $event->event_type          = $request->event_type;
            $event->event_type_other    = $request->event_type_other;
            $event->target_budget       = $request->target_budget;
            $event->start_date          = $request->start_date;
            $event->start_time          = $request->start_time;
            $event->end_date            = $request->end_date;
            $event->end_time            = $request->end_time;
            $event->color_code          = $request->color_code;

            if($event->save()) {
                return redirect('event')->with('success', "Event saved successfully.");
            } else {
                return redirect('event')->with('error', "Something went wrong.");
            }
        }
    }

    //ajax data table of event list
    public function ajaxListData(Datatables $datatables) {

        $builder = Event::query()->select('id', 'event_title','event_type', 'start_date', 'start_time','end_date', 'end_time', 'status', 'created_at', 'updated_at');

        return $datatables->eloquent($builder)
                            ->editColumn('event_title', function ($event) {
                                return "<a href=".url('event/view/' . $event->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$event->event_title."
                                </a>";
                            })
                            ->editColumn('event_type', function ($event) {
                                return "<a href=".url('event/view/' . $event->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".($event->event_type)."
                                </a>";
                            })
                            ->editColumn('start_date', function ($event) {
                                return "<a href=".url('event/view/' . $event->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".($event->start_date)."
                                </a>";
                            })
                            ->editColumn('end_date', function ($event) {
                                return "<a href=".url('event/view/' . $event->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".($event->end_date)."
                                </a>";
                            })
                            ->editColumn('status',function($event) {
                                if($event->status == 1) {
                                   return '<span class="badge bg-green">Active</span>';
                                } else if($event->status == 0) {
                                   return  '<span class="badge bg-red">Inactive</span>';
                                } else {
                                   return '<span class="badge bg-yellow">Delete</span>';
                                }
                            })
                            ->editColumn('created_at', function ($event) {
                                if($event->created_at !== null ) {
                                    return  date("j-M-y g:m A",strtotime($event->created_at))  ;
                                }
                                return '';
                           })
                           ->editColumn('updated_at', function ($event) {
                                if($event->updated_at !== null ) {
                                    return  date("j-M-y g:m A",strtotime($event->updated_at))  ;
                                }
                                return '';
                           })

                          ->addColumn('action', function($event) {
                            return "<a href=".url('event/delete/' . $event->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                          })
                          ->rawColumns(['event_title','event_type','start_date','end_date','action','status'])
                          ->toJson();
    }

    //event view function
    public function eventView(Request $request,$id = 0) {

        $event          = Event::find($id);
        $eventTypeArr   = EventType::where('status',1)->pluck('title', 'id');
        $timeArr        = General::getTimeData();
        return view('Admin.event.eventView',["timeArr" => $timeArr, 'event' => $event  , 'eventTypeArr' => $eventTypeArr ]);    
    }

    //event appointment
    public function eventAppointment(Request $request, $eventId = 0) {
        $event          = Event::find($eventId);
        $eventTypeArr   = EventType::where('status',1)->pluck('title', 'id');
        $timeArr        = General::getTimeData();
        $reminderArr    = General::reminderData();
        return view('Admin.event.eventAppointment',["timeArr" => $timeArr, 'event' => $event  , 'eventTypeArr' => $eventTypeArr, 'reminderArr' => $reminderArr ]);
    }

    //store event appointment data
    public function storeEventAppointment(Request $request) {

        // echo "<pre>";print_r($request->all());exit;   
        $responseArr                        = []; 
        $eventAppointment                   = new EventAppointment();
        if(isset($request->eventAppointmentId) && $request->eventAppointmentId > 0) {
            $eventAppointment               = EventAppointment::find($request->eventAppointmentId);
            $eventAppointment->updated_by   = Auth::user()->id;
            $eventAppointment->updated_at   = date("Y-m-d H:i:s");
        } else {
            $eventAppointment->created_by   = Auth::user()->id;
            $eventAppointment->created_at   = date("Y-m-d H:i:s");
        }

        $request->start_date        =  (isset($request->start_date) && $request->start_date != "")?date('Y-m-d',strtotime($request->start_date)):"";
        $request->end_date          =  (isset($request->end_date) && $request->end_date != "")?date('Y-m-d',strtotime($request->end_date)):"";

        $eventAppointment->event_id         = $request->event_id;
        $eventAppointment->is_all_day       = isset($request->is_all_day)?$request->is_all_day:0;
        $eventAppointment->start_date       = $request->start_date;
        $eventAppointment->end_date         = $request->end_date;
        $eventAppointment->start_time       = $request->start_time;
        $eventAppointment->end_time         = $request->end_time;
        $eventAppointment->location         = $request->location;
        $eventAppointment->notes            = $request->notes;
        $eventAppointment->freq             = $request->freq;
        $eventAppointment->freq_interval    = $request->freq_interval;
        $eventAppointment->is_reminder_set  = isset($request->is_reminder_set)?$request->is_reminder_set:0;
        $eventAppointment->reminder_time    = isset($request->reminder_time)?$request->reminder_time:0;
        $eventAppointment->reminder_interval= isset($request->reminder_interval)?$request->reminder_interval:0;
        $eventAppointment->send_email       = isset($request->send_email)?$request->send_email:0;
        $eventAppointment->send_msg         = isset($request->send_msg)?$request->send_msg:0;
        // $eventAppointment->status           = isset($request->status)?$request->status:0;

        if($eventAppointment->save()){
            $responseArr['status']  = true;
        } else{
            $responseArr['status']  = false;
        }

        echo json_encode($responseArr);
     }
    
}
