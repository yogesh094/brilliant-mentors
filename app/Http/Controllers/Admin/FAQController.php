<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUser;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\FAQ;
use App\Models\Language;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Session;

class FAQController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {
        Session::forget('AppQuestionPage');
      Session::forget('AppUserPage');
        return view('Admin.faq.list');
    }

    public function create() {
        $languages = Language::all();
        return view('Admin.faq.add',["languages" => $languages ]);

    }

    public function arrayData(Datatables $datatables) 
    {
         $builder = FAQ::query()->select('id','language_id', 'question', 'answer','status');
        

        return $datatables->eloquent($builder)
                            
                            ->editColumn('question', function ($faq) {
                                return "<a href=".url('panel/faq/edit/' . $faq->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$faq->question."
                                </a>";
                            })
                            
                            ->editColumn('status', function ($faq) {
                                if($faq->status == 1){
                                   return '<span class="badge bg-green">Active</span>';
                                } else {
                                    return  '<span class="badge bg-red">InActive</span>';
                                }
                            })
                            
                          ->addColumn('action', function($faq) {
                            return "<a href=".url('/panel/faq/edit/' . $faq->id)." class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>
                                <a href=".url('/panel/faq/destroy/' . $faq->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                    return true;
                                     } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                          })
                          ->rawColumns(['question','answer','status','action'])
                          ->toJson();
    }

    public function store(Request $request) {

       
         $this->validate($request, [
            "language_id.required"        => "Title is required",
            "question.required"    => "question is required",
            "answer.required"        => "answer is required" ]);

        // check validation
        if (!isset($this->validate)) {

            $FAQObj = "";
        
        if(isset($request->id)) {
            $FAQObj = FAQ::find($request->id);
           
            $FAQObj->updated_at = date('Y-m-d H:i:s');
        } else {
            $FAQObj                = new FAQ();

           $FAQObj->created_at    = date('Y-m-d H:i:s');
        }

           
            $FAQObj->language_id = Input::get('language_id');
            $FAQObj->question   = Input::get('question');
            $FAQObj->answer     = Input::get('answer');
            $FAQObj->status     = Input::get('status');
          
            
       
        if($FAQObj->save()) {
            //section for new user
            if (Input::get('id') != null && Input::get('id') > 0) {
                return redirect('/panel/faq')->with('success', "FAQ updated successfully.");
            } else {
                return redirect('/panel/faq')->with('success', "FAQ created successfully.");
            }
        } else {
            return redirect('/panel/faq')->with('success', "something went wrong please try later.");
        }
        }else{
            return redirect('/panel/faq')->with('success', "something went wrong please try later.");
        }
        

        
    }

    public function edit($id) {
        // echo $id;die;
        $languages = Language::all();
         $FAQObj    = FAQ::find($id);
        if($FAQObj == null) {
             return View::make('Admin.error.404');
        }       

        return View::make('Admin.faq.edit',["data" => $FAQObj,"languages" => $languages]);
    }
      

   public function destroy(Request $request, $id = 0) {

        $FAQObj    = FAQ::find($id);
        if (!empty($FAQObj) > 0) {

            if($FAQObj->destroy($id)) {
                return redirect('/panel/faq')->with('success', "FAQ deleted successfully.");
            } else {
                return redirect('/panel/faq')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/faq')->with('error', trans('Record not found'));
    }
}


