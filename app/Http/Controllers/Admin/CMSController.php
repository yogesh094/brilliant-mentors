<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUser;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Cms;
use Illuminate\Support\Facades\Hash;
use App\Models\Language;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Session;

class CMSController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {
     Session::forget('AppQuestionPage');
      Session::forget('AppUserPage');
        return view('Admin.cms.list');
    }

    public function create() {
        $languages = Language::all();
        return view('Admin.cms.add',["languages" => $languages ]);

    }

    public function arrayData(Datatables $datatables) 
    {
         $builder = CMS::query()->select('id','title','slug', 'description', 'status');
        

        return $datatables->eloquent($builder)
        ->editColumn('id', function ($cms) {
                                return $cms->id;
                            })
                            ->editColumn('title', function ($cms) {
                                return "<a href=".url('panel/cms/edit/' . $cms->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                ".$cms->title."
                                </a>";
                            })
                            ->editColumn('slug', function ($cms) {
                                return "<a href=".url('panel/cms/edit/' . $cms->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$cms->slug."
                                </a>";
                            })
                            ->editColumn('status', function ($cms) {
                                if($cms->status == 1){
                                   return '<span class="badge bg-green">Active</span>';
                                } else {
                                    return  '<span class="badge bg-red">InActive</span>';
                                }
                            })
                            
                          ->addColumn('action', function($cms) {

                            // <a href=".url('/panel/cms/destroy/' . $cms->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                            //                                         return true;
                            //                                     } else {
                            //                                         return false;
                            //                                     }\" title='Delete'><i class=\"fa fa-trash\"></i></a>
                                                                
                            return "<a href=".url('/panel/cms/edit/' . $cms->id)." class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>
                                ";
                          })
                          ->rawColumns(['id','title','slug','status','action'])
                          ->toJson();
    }

    public function store(Request $request) {

       
         $this->validate($request, [
            "language_id.required"        => "language is required",
            "title.required"        => "Title is required",
            "desription.required"    => "Desription is required",
            "slug.required"        => "Slug is required" ]);

        // check validation
        if (!isset($this->validate)) {

            $CMSObj = "";
        
        if(isset($request->id)) {
            $CMSObj = CMS::find($request->id);
        } else {
            $CMSObj = new CMS();
        }

            $CMSObj->language_id     = Input::get('language_id');
            $CMSObj->title           = Input::get('title');
            $CMSObj->slug            = Input::get('slug');
            $CMSObj->description     = Input::get('description');
            $CMSObj->status          = Input::get('status');
          
            
       
        if($CMSObj->save()) {
            //section for new user
            if (Input::get('id') != null && Input::get('id') > 0) {
                return redirect('/panel/cms')->with('success', "CMS updated successfully.");
            } else {
                return redirect('/panel/cms')->with('success', "CMS created successfully.");
            }
        } else {
            return redirect('/panel/cms')->with('success', "something went wrong please try later.");
        }
        }else{
            return redirect('/panel/cms')->with('success', "something went wrong please try later.");
        }
        

        
    }

    public function edit($id) {
        // echo $id;die;
         $languages = Language::all();
         $CMSObj    = CMS::find($id);
        if($CMSObj == null) {
             return View::make('Admin.error.404');
        }       

        return View::make('Admin.cms.edit',["data" => $CMSObj,"languages" => $languages]);
    }
      

   public function destroy(Request $request, $id = 0) {

        $CMSObj    = CMS::find($id);
        if (!empty($CMSObj) > 0) {

            if($CMSObj->destroy($id)) {
                return redirect('/panel/cms')->with('success', "CMS deleted successfully.");
            } else {
                return redirect('/panel/cms')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/cms')->with('error', trans('Record not found'));
    }
}


