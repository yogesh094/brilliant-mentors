<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
use Illuminate\Support\Facades\Hash;
use App\Models\EmailTemplate;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use Session;
use Excel;

class UserController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        Session::forget('AppQuestionPage');
        Session::forget('AppUserPage');
        $pager = 2;
        $userList = User::get();
        return view('Admin.user.list', ['data' => $userList]);
    }

    public function create() {
        $userRolesArr = Role::where('deleted_at', null)->whereNotIn('id', [1])->pluck('name', 'id')->toArray();
        return view('Admin.user.add', ['user_role' => $userRolesArr]);
    }

    public function store(Request $request) {
        // echo "<pre>";print_r($request->all());die;
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => "required|email|unique:user,email," . $request->id, 'id',
            'phone_number' => 'required|numeric|min:10',
                //'user_role'     => 'required',
        ];

        // if(!isset($request->id)) {
        //     $rules['conf_password'] = 'required|same:password';
        // }

        $message = [
            "first_name.required" => "First name is required",
            "first_name.max" => "First name must be less than 255 characters",
            "last_name.required" => "Last name is required",
            "last_name.max" => "Last name must be less than 255 characters",
            "username.required" => "User name is required",
            "username.max" => "User name must be less than 255 characters",
            "email.required" => "Email is required",
            "email.email" => "Invalid e-mail address",
            "email.unique" => "E-mail address is already exist",
            "phone_number.required" => "Phone Number is required",
            "phone_number.numeric" => "Phone should be numeric",
            "phone_number.min" => "Minimum 10 characters",
            "phone_number.max" => "Maximum 12 characters",
                //"user_role.required"    => "Please select the user role",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);
        // process the login
        if ($validator->fails()) {
            if (Input::get('id') != null && Input::get('id') > 0) {
                return Redirect::to('panel/user/edit/' . Input::get('id'))->withErrors($validator)->withInput(Input::except('laravel_password'));
            } else {
                return Redirect::to('panel/user/create')->withErrors($validator)->withInput(Input::except('laravel_password'));
            }
        }

        $userObj = "";
        if (isset($request->id)) {
            $userObj = User::find($request->id);
            $userObj->status = $request->status;
            $userObj->updated_at = date('Y-m-d H:i:s');
        } else {
            $userObj = new User();

            //generate unique token - start
            $firstNameLetter = substr($request->first_name, 0, 1);
            $lastNameLetter = substr($request->last_name, 0, 1);
            $uniquetoken = User::generateToken();
            $uniqueString = $firstNameLetter . $lastNameLetter . $uniquetoken;
            //generate unique token - end

            $userObj->token = $uniqueString;
            $userObj->status = 0;
            $userObj->created_at = date('Y-m-d H:i:s');
        }

        $userObj->first_name = Input::get('first_name');
        $userObj->last_name = Input::get('last_name');
        $userObj->username = Input::get('username');
        $userObj->email = Input::get('email');
        $userObj->phone_number = Input::get('phone_number');
        // $userObj->password      = bcrypt(Input::get('password'));
        //$userObj->user_role     = $request['user_role'][0];
        $userObj->user_role = 2;
        if ($userObj->save()) {
            //section for new user
            if (!isset($request->id)) {

                //save the user role in role user table - start
                $roleUserObj = new RoleUser();
                $roleUserObj->user_id = $userObj->id;
                //$roleUserObj->role_id = $request['user_role'][0];
                $roleUserObj->role_id = 2;
                $roleUserObj->save();
                //save the user role in role user table - end
                //generating confirmation link
                $emailTemplateObj = EmailTemplate::where('id', 4)->where('language_id', 1)->first();
                $confirm_link = url('expert/confirm') . '/' . $userObj->token;
                $tomail = $request->email;
                $subject = 'Expert User Registration';
                //$body           = "Expert User created successfully";
                $body = $emailTemplateObj->body;
                $body = str_replace('[confirm_link]', $confirm_link, $body);

                $data = array("name" => $request->first_name, 'confirm_link' => $confirm_link, 'body' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'FRONT_URL' => url(''));

                $res = Mail::send('emails.signup', ['data' => $data], function($message) use ($data) {
                            $message->from($data['from'], $data['emailLabel']);
                            $message->to($data['to'], $data['name']);
                            $message->subject($data['subject']);
                        });
            }

            if (Input::get('id') != null && Input::get('id') > 0) {
                return redirect('panel/user')->with('success', "Expert updated successfully.");
            } else {
                return redirect('panel/user')->with('success', "Expert created successfully.");
            }
        } else {
            return redirect('panel/user')->with('error', "something went wrong please try later.");
        }
    }

    public function edit($id) {
        // echo $id;die;
        if ($id > 0) {
            $userRolesArr = Role::where('deleted_at', null)->whereNotIn('id', [1])->pluck('name', 'id')->toArray();
            $user = User::find($id);
            if (count($user) > 0) {
                return View::make('Admin.user.edit')->with('data', $user)->with('user_role', $userRolesArr);
            }
            return View::make('Admin.error.404');
        }
    }

    public function editConfirmPass(Request $request) {
        // echo \Auth::User()->id;die;
        $userObj = User::find(Auth::User()->id);
        if (Hash::check($request->password, $userObj->password)) {
            return "true";
        } else {
            return "false";
        }
    }

    public function sendResetPasswordLink(Request $request) {

        $userObj = User::find(Auth::User()->id);
        if (Hash::check($request->password, $userObj->password)) {
            $userObj = User::find($request->user_id);
            if (count($userObj) > 0) {
                //generate unique token - start
                $firstNameLetter = substr($userObj->first_name, 0, 1);
                $lastNameLetter = substr($userObj->last_name, 0, 1);
                $uniquetoken = User::generateToken();
                $uniqueString = $firstNameLetter . $lastNameLetter . $uniquetoken;
                //generate unique token - end
                //save new token - start
                $userObj->token = $uniqueString;
                $userObj->save();
                //save new token - end
                $emailTemplateObj = EmailTemplate::where('id', 3)->where('language_id', 1)->first();
                $reset_link = url('user/reset-password') . '/' . $userObj->token;
                $tomail = $userObj->email;
                $subject = $emailTemplateObj->subject;
                $body = $emailTemplateObj->body;
                $body = str_replace('[userName]', $userObj->first_name . " " . $userObj->last_name, $body);
                $body = str_replace('[reset_link]', $reset_link, $body);

                $data = array("user_name" => $userObj->first_name . " " . $userObj->last_name, 'reset_link' => $reset_link, 'body' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'FRONT_URL' => url(''));

                $res = Mail::send('emails.resetPass', ['data' => $data], function($message) use ($data) {
                            $message->from($data['from'], $data['emailLabel']);
                            $message->to($data['to'], $data['user_name']);
                            $message->subject($data['subject']);
                        });

                return "true";
            }
        } else {
            return "false";
        }
    }

    public function destroy(Request $request) {

        $userObj = User::find(Auth::User()->id);
        if (Hash::check($request->password, $userObj->password)) {
            $user = User::find($request->id);
            if (count($user) > 0) {
                $user->destroy($user->id);
                return redirect('/panel/user')->with('success', "Expert deleted successfully.");
            }
        } else {
            return redirect('/panel/user')->with('error', "You entered the wrong current password.");
        }
    }

    public function arrayData(Datatables $datatables) {
        $builder = User::query()->select('id', 'username', 'first_name', 'last_name', 'email', 'phone_number', 'user_role', 'status', 'created_at', 'updated_at')->whereNotIn('user_role', [1]);

        return $datatables->eloquent($builder)
                        ->editColumn('email', function ($user) {
                            return "<a href=" . url('panel/user/edit/' . $user->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $user->email . "
                                </a>";
                        })
                        ->editColumn('username', function ($user) {
                            return "<a href=" . url('panel/user/edit/' . $user->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $user->username . "
                                </a>";
                        })
                        ->editColumn('first_name', function ($user) {
                            return "<a href=" . url('panel/user/edit/' . $user->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $user->first_name . "
                                </a>";
                        })
                        ->editColumn('last_name', function ($user) {
                            return "<a href=" . url('panel/user/edit/' . $user->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $user->last_name . "
                                </a>";
                        })
                        ->editColumn('phone_number', function ($user) {
                            return "<a href=" . url('panel/user/edit/' . $user->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $user->phone_number . "
                                </a>";
                        })
                        ->editColumn('user_role', function($user) {
                            return "<a href=" . url('panel/user/edit/' . $user->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . ($user->user_role == 1 ? "Admin" : "User") . "
                                </a>";
                        })
                        ->editColumn('status', function($user) {
                            if ($user->status == 1) {
                                return '<span class="badge bg-green">Active</span>';
                            } else if ($user->status == 0) {
                                return '<span class="badge bg-red">Inactive</span>';
                            } else {
                                return '<span class="badge bg-yellow">Delete</span>';
                            }
                        })
                        ->editColumn('created_at', function ($user) {
                            if ($user->created_at !== null) {
                                return date("j-M-y g:m A", strtotime($user->created_at));
                            }
                            return '';
                        })
                        ->editColumn('updated_at', function ($user) {
                            if ($user->updated_at !== null) {
                                return date("j-M-y g:m A", strtotime($user->updated_at));
                            }
                            return '';
                        })
                        ->addColumn('action', function($user) {
                            return "<a href=" . url('panel/user/edit/' . $user->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>  
                                              <button type=\"button\" class=\"btn btn-danger delete-btn\"  data-id = '" . $user->id . "'  data-toggle=\"modal\" data-target=\".bs-example-modal-delete\" style=\"font-size: 13px;width: 31px;padding-left: 9px;height: 29px;\"><i class=\"fa fa-trash\"></i></button>";
                        })
                        ->rawColumns(['email', 'username', 'first_name', 'last_name', 'phone_number', 'user_role', 'action', 'status'])
                        ->toJson();
    }

    /*
      This function is used to download excel
     */

    public function download() {

        $data = User::query()->select('id', 'first_name', 'last_name', 'email', 'username', 'phone_number', 'user_role', 'status', 'created_at', 'updated_at')->get();
        $filename = "users";
        $export_array = [];
        foreach ($data as $key => $row) {
            array_push($export_array, [
                "id" => $row->id,
                "first_name" => $row->first_name,
                "last_name" => $row->last_name,
                "email" => $row->email,
                "username" => $row->username,
                "phone_number" => $row->phone_number,
                "user_role" => $row->user_role == 1 ? "Admin" : "User",
                "status" => $row->status == 1 ? "Active" : "In Active",
                "created_at" => date("j-M-y g:m A", strtotime($row->created_at)),
                "updated_at" => ($row->updated_at !== null) ? date("j-M-y g:m A", strtotime($row->updated_at)) : ""
            ]);
        }
        $data = $export_array;
        set_time_limit(0);
        ini_set('memory_limit', '2G');
        return Excel::create($filename, function($excel) use ($data, $filename) {
                    $excel->sheet($filename, function($sheet) use ($data) {
                        $sheet->fromArray($data, null, 'A1', true);
                        // Set auto size for sheet
                        $sheet->setAutoSize(true);
                        //$sheet->setWidth('A', 3);
                        //$sheet->setHeight(1, 20);
                        // Freeze the first column
                        $sheet->freezeFirstColumn();
                        // Font family
                        $sheet->setFontFamily('Comic Sans MS');

                        // Font size
                        $sheet->setFontSize(10);

                        // Font bold
                        //$sheet->setFontBold(true);
                        // Sets all borders
                        $sheet->setAllBorders('thin');

                        // Set border for cells
                        $sheet->setBorder('A1', 'thin');
                        $sheet->row(1, ['id', 'First Name', 'Last Name', 'Email', 'Username', 'Phone', 'Role', 'Status', 'Created On', 'Updated On']); // etc etc
                        // Set black background
                        $sheet->row(1, function($row) {
                            // call cell manipulation methods
                            $row->setBackground('#000000');
                            $row->setFontColor('#ffffff');
                        });
                    });
                })->download('xls');
    }

    public function viewChangePassword(Request $request) {
        Session::forget('AppQuestionPage');
        Session::forget('AppUserPage');
        return view('Admin.user.changePassword');
    }

    public function changePassword(Request $request) {
        $rules = [
            'current_password' => 'required|min:6',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password'
        ];
        $requestData = Input::all();

        $message = [
            "new_password.required" => "Password is required",
            "new_password.min" => "Password should be six characters long",
            "current_password.required" => "Password is required",
            "current_password.min" => "Password should be six characters long",
            "confirm_password.required" => "Confirm Password is required",
            "confirm_password.same" => "Confirm Password and New Password do not match.",
        ];

        $validator = Validator::make(Input::all(), $rules, $message);
        if ($validator->fails()) {
            if ($validator->fails()) {
                return Redirect::to('/panel/change-password')->withErrors($validator);
            }
        }
        //$userObj = User::where('id',$request->get('email'))->first();
        $userObj = User::where("id", Auth::User()->id)->first();
        if (count($userObj) > 0) {
            $validCredentials = Hash::check($request->get('current_password'), $userObj->password);
            if ($validCredentials) {
                $userObj->password = bcrypt($request->get('new_password'));
                $userObj->save();
                return redirect('/panel/change-password')->with('success', "Your Password Change successfully.");
            } else {
                return redirect('/panel/change-password')->with('error', "Invalid Password.");
            }
        }
    }

    public function viewProfile() {
        Session::forget('AppQuestionPage');
        Session::forget('AppUserPage');
        return View::make('Admin.user.profile');
    }

    public function profileStore(Request $request) {

        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255',
            'phone_number' => 'required|numeric|min:10'
        ];
        $requestData = Input::all();
        $message = [
            "first_name.required" => "First name is required",
            "first_name.max" => "First name must be less than 255 characters",
            "last_name.required" => "Last name is required",
            "last_name.max" => "Last name must be less than 255 characters",
            "username.required" => "User name is required",
            "username.max" => "User name must be less than 255 characters",
            "phone_number.required" => "Phone Number is required",
            "phone_number.numeric" => "Phone should be numeric",
            "phone_number.min" => "Minimum 10 characters",
            "phone_number.max" => "Maximum 12 characters"
        ];

        $validator = Validator::make(Input::all(), $rules, $message);
        if ($validator->fails()) {
            if ($validator->fails()) {
                return Redirect::to('/panel/profile')->withErrors($validator);
            }
        }

        $userObj = User::find(Auth::User()->id);
        $userObj->first_name = $request->first_name;
        $userObj->last_name = $request->last_name;
        $userObj->username = $request->username;
        $userObj->phone_number = $request->phone_number;
        if ($userObj->save()) {
            return redirect('/panel/profile')->with('success', "Your profile updated successfully.");
        } else {
            return redirect('/panel/profile')->with('error', "something went wrong please try later.");
        }
    }

}
