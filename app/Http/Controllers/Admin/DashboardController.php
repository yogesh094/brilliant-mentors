<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Mail;
use Redirect;
use Auth;
use App\Model\User;
use Response;
use Session;

class DashboardController extends Controller {

	// retun dashboard view
    public function index() {
    	Session::forget('AppQuestionPage');
    	Session::forget('AppUserPage');
    	return view('Admin.dashboard');
    }

    //return add form test
    public function add() {
    	return view('Admin.Admin.add');	
    }
}