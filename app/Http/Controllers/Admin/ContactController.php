<?php

namespace App\Http\Controllers\Admin;
/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Bloodtype;
use App\Models\Gender;
use App\Models\Maritalstatus;
use App\Models\Language;
use App\Models\Contact;
use App\Library\General;
use App\Models\Country;
use App\Models\Region;
use App\Models\ContactAddress;
use App\Models\Province;
use App\Models\District;
use App\Models\City;
use App\Models\Jobcategory;
use App\Models\Jobposition;
use App\Models\ContactSpouse;
use App\Models\ContactJob;
use App\Models\ContactBusiness;
use App\Models\ContactRepresentative;
use App\Models\ContactReferral;
use App\Models\ContactPoliticalPosition;
use App\Models\ContactDependent;
use App\Models\ContactNumber;
use App\Models\ContactCommunication;
use App\Models\ContactEducation;
use App\Models\ContactGreetingsLetter;

use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
use DB;
/*
    To download excel file
*/
use Excel;

class ContactController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('Admin.contact.listContact');
    }

    /*
        return add view language keyword
    */
    public function add(Request $request) {

        $bloodType      = Bloodtype::orderBy('bloodtype')->pluck('bloodtype', 'id');
        $gender         = Gender::pluck('gender', 'id');
        $maritalStatus  = Maritalstatus::pluck('marital_status', 'id');
        $languages      = Language::pluck('title', 'id');
        $nationality    = Country::pluck('nationality','id');
        $nations        = Country::get();

        $systemUniqueKey                = General::getRandomString(18);
        
        return view('Admin.contact.addContact',['systemUniqueKey' => $systemUniqueKey,'bloodType' => $bloodType, 'gender' => $gender,'maritalStatus' => $maritalStatus, 'languages' => $languages, 'nationality' => $nationality,'nations' => $nations  ]);
    }

    /*
        return edit view language keyword
    */
    public function edit(Request $request,$id = 0) {

        $contact        = Contact::where('id',$id)
                            ->with(['user_createby' => function($query) {
                                $query->select('id','first_name','last_name');
                            }])
                            ->with(['user_updateby' => function($query) {
                                $query->select('id','first_name','last_name');
                            }])
                            ->first();
        if(empty($contact)) {
            return View::make('Admin.error.404');
        }   
        // echo "<pre>";print_r($contact);die;
        $bloodType      = Bloodtype::orderBy('bloodtype')->pluck('bloodtype', 'id');
        $gender         = Gender::pluck('gender', 'id');
        $maritalStatus  = Maritalstatus::pluck('marital_status', 'id');
        $languages      = Language::pluck('title', 'id');
        $nationality    = Country::pluck('nationality','country_code');
        $countryCode    = DB::table('country')
            ->join('country_phonecodes', 'country.country_code', '=', 'country_phonecodes.iso')
            ->select('country_phonecodes.phonecode', 'country.id')
            ->pluck('phonecode','id');
        $nations        = Country::get();

        $region                     = Region::pluck('region','id');

        $jobCategory                = Jobcategory::whereIn('id',[1,35,27,39,57])->pluck('jobcategory','id');
        // $jobPositions   = Jobposition::pluck('jobposition','id');

        //contact address data
        $addressContact             = ContactAddress::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        // echo "<pre>";print_r($addressContact);die;
        $province                   = Province::where('country',isset($addressContact->country_id)?$addressContact->country_id:0)->pluck('province','id');
        $district                   = District::where('country',isset($addressContact->country_id)?$addressContact->country_id:0)->pluck('district','id');
        $city                       = City::where('country',isset($addressContact->country_id)?$addressContact->country_id:0)->pluck('city','id');



        //contact job position data - start
        $jobpositionDetail          = ContactJob::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        $jobPositions               = Jobposition::where('jobcategory',isset($jobpositionDetail->job_category)?$jobpositionDetail->job_category:0)->pluck('jobposition','id');
        $jobProvince                = Province::where('country',isset($jobpositionDetail->country_id)?$jobpositionDetail->country_id:0)->pluck('province','id');
        $jobDistrict                = District::where('country',isset($jobpositionDetail->country_id)?$jobpositionDetail->country_id:0)->pluck('district','id');
        $jobCity                    = City::where('country',isset($jobpositionDetail->country_id)?$jobpositionDetail->country_id:0)->pluck('city','id');
        //contact job position data - end

        //contact spouse data - start
        $spouseContact              = ContactSpouse::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact spouse data - end


        //contact business data - start
        $businessDetail             = ContactBusiness::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        $businessProvince           = Province::where('country',isset($businessDetail->country_id)?$businessDetail->country_id:0)->pluck('province','id');
        $businessDistrict           = District::where('country',isset($businessDetail->country_id)?$businessDetail->country_id:0)->pluck('district','id');
        $businessCity               = City::where('country',isset($businessDetail->country_id)?$businessDetail->country_id:0)->pluck('city','id');
        //contact business data - start

        //contact representative data - start
        $representativeContact      = ContactRepresentative::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact representative data - end

        //contact referral data - start
        $referralContact            = ContactReferral::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact referral data - end

        //contact Political Position data - start
        $politicalPositionContact   = ContactPoliticalPosition::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact Political Position data - end

        //contact Dependent data - start
        $dependentsContact          = ContactDependent::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact Dependent data - end

        //contact number data - start
        $contactNumber              = ContactNumber::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])    
                                        ->first();
        //contact number data - end

        //contact communication data - start
        $communicationContact       = ContactCommunication::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact communication data - end

        //contact education data - start
        $educationContact           = ContactEducation::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact education data - end

        //contact greetings letter data - start
        $greetingsContact           = ContactGreetingsLetter::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact education data - end

        $imageUrl   = "https://s3-ap-southeast-1.amazonaws.com/contactstorage/";

        return view('Admin.contact.editContact',['nations' => $nations  , 'contact' => $contact,'contactId' => $id,'addressContact' => $addressContact,'bloodType' => $bloodType, 'gender' => $gender,'maritalStatus' => $maritalStatus, 'languages' => $languages,'nationality' => $nationality, 'region' => $region,'province' => $province , 'district' => $district, 'city' => $city ,'jobCategory' => $jobCategory, 'jobpositions' => $jobPositions, 'jobProvince' => $jobProvince, 'jobDistrict' => $jobDistrict, 'jobCity' => $jobCity,'jobpositionDetail' => $jobpositionDetail,'spouseContact' => $spouseContact, 'businessDetail' => $businessDetail, 'businessProvince' => $businessProvince, 'businessDistrict' => $businessDistrict, 'businessCity' => $businessCity, 'representativeContact' => $representativeContact, 'referralContact' => $referralContact, 'politicalPositionContact' => $politicalPositionContact, 'dependentsContact' => $dependentsContact,'countryCode' => $countryCode, 'contactNumber' => $contactNumber, 'communicationContact' => $communicationContact, 'educationContact' => $educationContact, 'greetingsContact' => $greetingsContact,'imageUrl' => $imageUrl ]);
    }

    /*
        add and edit language keyword
    */
    public function store(Request $request) {

        $this->validate($request, [
            "title"                             => "required",
            "first_name"                        => "required",
            "father_name"                       => "required",
            "mother_name"                       => "required",
            "surname"                           => "required",
            "date_of_birth"                     => "required",
            "contact_unique_key"                => "required",
        ],
        [
            "title.required"                    => "Select the title.",
            "first_name.required"               => "Select the first name",
            "father_name.required"              => "Select the father name",
            "mother_name.required"              => "Select the mother name",
            "surname.required"                  => "Select the surname",
            "date_of_birth.required"            => "Select the date of birth",
            "contact_unique_key.required"       => "Contact unique is required.",
        ]);


        // check validation
        if (!isset($this->validate)) {

            if(isset($request->id) && $request->id > 0) {
                $contact                        = Contact::find($request->id);
                $contact->status                = $request->status;
                $contact->updated_by            = Auth::user()->id;
                $contact->updated_at            = date('Y-m-d H:i:s');
            } else {
                $contact                        = new Contact();

                $contact->contact_id            = General::getRandomString(10);
                $systemUniqueKey                = General::getRandomString(18);
                $contact->system_unique_key     = $systemUniqueKey;
                $contact->contact_unique_key    = $request->user_unique_key;
                $contact->qr_code               = $request->user_unique_key.$systemUniqueKey;
                $contact->created_at            = date('Y-m-d H:i:s');
                $contact->created_by            = Auth::user()->id;
            }

            $firstNameArr   = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => 'Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'];

            $fatherNameArr  = ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '6' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'];

            $surnameArr     = ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'];

            $fullName                           = ucfirst($firstNameArr[$request->first_name])." ".ucfirst($fatherNameArr[$request->father_name])." ".ucfirst($surnameArr[$request->surname]);

            $profession                         = (!empty($request->profession)?implode(',', $request->profession):NULL);
            $nationality                        = (!empty($request->nationality)?implode(',', $request->nationality):NULL);
            $languages                          = (!empty($request->languages)?implode(',', $request->languages):NULL);

            $suffix                             = (!empty($request->suffix)?implode(',',$request->suffix):NULL);

            $request->date_of_birth             =  date('Y-m-d',strtotime($request->date_of_birth));

            //device image upload
            if(count($request->file('photo')) > 0) {

                $imageUpload        = Contact::imageUpload($request->file('photo'));

                if($imageUpload == "") {
                    $contactImageName   = (isset(
            $request->old_contact_image)?$request->old_contact_image:"");
                } else {
                    $contactImageName   = $imageUpload;
                }

                // $contactImageFile    = $request->file('photo');
                // $contactImagePath    = public_path('../uploads/contact/');
                // $contactImageName    = uniqid().'.'.$contactImageFile->getClientOriginalExtension();
                // $contactImageFile->move($contactImagePath,$contactImageName);

                // if(!empty($request->old_contact_image)) {
                //     File::delete('uploads/contact/' . $request->old_contact_image);
                // }

            } else {
                $contactImageName    = (isset(
            $request->old_contact_image)?$request->old_contact_image:"");
            }

            $contact->title                     = $request->title;
            $contact->suffix                    = $suffix;
            $contact->first_name                = $request->first_name;
            $contact->father_name               = $request->father_name;
            $contact->mother_name               = $request->mother_name;
            $contact->surname                   = $request->surname;
            $contact->full_name                 = $fullName;
            $contact->nick_name                 = $request->nick_name;
            $contact->date_of_birth             = $request->date_of_birth;
            $contact->birth_place               = $request->birth_place;
            $contact->blood_type                = $request->blood_type;
            $contact->gender                    = $request->gender;
            $contact->marital_status            = $request->marital_status;
            $contact->nationality               = $nationality;
            $contact->languages                 = $languages;
            $contact->profession                = $profession;
            $contact->importance                = $request->importance;
            $contact->caution                   = $request->caution;
            $contact->sensitivity               = $request->sensitivity;
            $contact->private                   = $request->private;
            $contact->business_owner            = $request->business_owner;
            $contact->sponsorships              = $request->sponsorships;
            $contact->political_party           = $request->political_party;
            $contact->charitable_organizations  = $request->charitable_organizations;
            $contact->relationship_preference   = $request->relationship_preference;
            $contact->religion                  = $request->religion;
            $contact->internal_organizations    = $request->internal_organizations;
            $contact->administrative_group      = $request->administrative_group;
            $contact->photo                     = $contactImageName;

            //key section
            $contact->contact_id                = General::getRandomString(10);
            $systemUniqueKey                    = General::getRandomString(18);
            $contact->system_unique_key         = $systemUniqueKey;
            $contact->contact_unique_key        = $request->contact_unique_key;
            $contact->qr_code                   = $request->contact_unique_key.$systemUniqueKey;

            $contact->keywords                  = $request->keywords;
            $contact->notes                     = $request->notes;
            
            

            if($contact->save()) {
                return redirect('contact/edit/'.$contact->id.'#address')->with('successContact', "Contact saved successfully.");
            } else {
                if($contact->id > 0) {
                    return redirect('contact/edit/'.$contact->id.'#addContact')->with('errorContact', "Something went wrong.");
                } else {
                    return redirect('contact/add#addContact')->with('errorContact', "Something went wrong.");
                }
            }
        }
    }

    /*
        add and edit spouse contact
    */
    public function spouseStore(Request $request) {
        // echo "<pre>";print_r($request->all());die;
        // echo $request->spouse_photo->getClientOriginalExtension();die;
        $rules = [
            "spouse_title"      => "required",
            "spouse_first_name" => "required",
            "spouse_suffix"     => "required",
            "spouse_surname"    => "required"
        ];

        $message = [
            'spouse_title.required'         => 'Select the title',
            'spouse_first_name.required'    => 'Select the first name',
            'spouse_suffix.required'        => 'Select the Suffix',
            'spouse_surname.required'       => 'Select the Surname'
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#spouse')->withErrors($validator)->with('submit_spouse',$request->submit_spouse);
            }
        }

        //device image upload
        if(count($request->file('spouse_photo')) > 0) {

            $imageUpload        = Contact::imageUpload($request->file('spouse_photo'));

            if($imageUpload == "") {
                $contactImageName   = (isset(
        $request->old_contact_image)?$request->old_contact_image:"");
            } else {
                $contactImageName   = $imageUpload;
            }

            // $contactImageFile    = $request->file('spouse_photo');
            // $contactImagePath    = public_path('../uploads/contact/');
            // $contactImageName    = uniqid().'.'.$contactImageFile->getClientOriginalExtension();
            // $contactImageFile->move($contactImagePath,$contactImageName);

            // if(!empty($request->old_contact_spouse_image)) {
            //     File::delete('uploads/contact/' . $request->old_contact_spouse_image);
            // }
        } else {
            $contactImageName    = (isset(
        $request->old_contact_spouse_image)?$request->old_contact_spouse_image:"");
        }

        if(isset($request->id) && $request->id > 0) {
            $contactspouseObj               = ContactSpouse::where('contact_id',$request->id)->first();
            $contactspouseObj->updated_at   = date('Y-m-d H:i:s');
        } else {
            $contactspouseObj = new ContactSpouse;
            $contactspouseObj->created_by   = \Auth::user()->id;
            $contactspouseObj->created_at   = date('Y-m-d H:i:s');
        }

        $contactspouseObj->contact_id               = $request->contact_id;

        $contactspouseObj->title                    = $request->spouse_title;

        $contactspouseObj->suffix                   = (!empty($request->spouse_suffix)?implode(',', $request->spouse_suffix):'');

        $contactspouseObj->first_name               = $request->spouse_first_name;

        $contactspouseObj->father_name              = (!empty($request->spouse_father_name)?$request->spouse_father_name:'');

        $contactspouseObj->mother_name              = (!empty($request->spouse_mother_name)?$request->spouse_mother_name:'');

        $contactspouseObj->surname                  = $request->spouse_surname;

        $contactspouseObj->full_name                = $contactspouseObj->first_name.' '.$contactspouseObj->father_name.' '.$contactspouseObj->surname;

        $contactspouseObj->nickname                 = (!empty($request->spouse_nick_name)?$request->spouse_nick_name:'');

        $contactspouseObj->date_of_birth            = (!empty($request->spouse_date_of_birth)?date('Y-m-d',strtotime($request->spouse_date_of_birth)):'');

        $contactspouseObj->birth_place              = (!empty($request->spouse_birth_place)?$request->spouse_birth_place:'');

        $contactspouseObj->blood_type               = (!empty($request->spouse_blood_type)?$request->spouse_blood_type:'');

        $contactspouseObj->gender                   = (!empty($request->spouse_gender)?$request->spouse_gender:null);

        $contactspouseObj->number_of_dependents     = (!empty($request->spouse_number_of_dependents)?$request->spouse_number_of_dependents:null);

        $contactspouseObj->nationality              = (!empty($request->spouse_nationality)?implode(',', $request->spouse_nationality):'');

        $contactspouseObj->languages                = (!empty($request->spouse_languages)?implode(',', $request->spouse_languages):'');

        $contactspouseObj->profession               = (!empty($request->spouse_profession)?implode(',', $request->spouse_profession):'');

        $contactspouseObj->importance               = (!empty($request->spouse_importance)?$request->spouse_importance:'');

        $contactspouseObj->caution                  = (!empty($request->spouse_caution)?$request->spouse_caution:'');

        $contactspouseObj->sensitivity              = (!empty($request->spouse_sensitivity)?$request->spouse_sensitivity:'');

        $contactspouseObj->sponsorships             = (!empty($request->spouse_sponsorships)?implode(',', $request->spouse_sponsorships):'');

        $contactspouseObj->charitable_organizations = (!empty($request->spouse_charitable_organizations)?implode(',', $request->spouse_charitable_organizations):'');

        $contactspouseObj->political_party          = (!empty($request->spouse_political_party)?implode(',', $request->spouse_political_party):'');

        $contactspouseObj->photo                    = $contactImageName;

        $contactspouseObj->religion_id              = (!empty($request->spouse_religion)?$request->spouse_religion:null);

        $contactspouseObj->internal_organizations   = (!empty($request->spouse_internal_organizations)?implode(',', $request->spouse_internal_organizations):'');

        $contactspouseObj->administrative_group     = (!empty($request->spouse_administrative_group)?implode(',', $request->spouse_administrative_group):'');

        $contactspouseObj->status                   = (!empty($request->spouse_status)?$request->spouse_status:null);

        $contactspouseObj->notes                    = (!empty($request->spouse_notes)?$request->spouse_notes:'');

        $contactspouseObj->keywords                 = (!empty($request->spouse_keywords)?$request->spouse_keywords:'');

        $contactspouseObj->updated_by               = \Auth::user()->id;

        if($contactspouseObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#business')->with('spouseSuccess', "Contact Spouse saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#spouse')->with('error', "Something went wrong please try later.");
        }
    }

    /*
        ajax function to get country
    */
    public function getCountry(Request $request) {

        $countryArr     = Country::select('country','id','country_code')
                        ->where("region","$request->region")
                        ->orderBy('country','asc')
                        ->get();
        $classname      = isset($request->classname)?$request->classname:"country_$request->address_id";
        $idname         = isset($request->idname)?$request->idname:'country';

        $countryDropdown            = "";
        $countryDropdown            .= "<select class='form-control contact_address_country ".$classname."' name='country_id[]' id='".$idname."' data-id='$request->address_id' onchange='changeCountry(this)' >";
        $countryDropdown            .= "<option value='' disabled='disabled' selected='selected'>Select Country</option>";
        if(count($countryArr) > 0) {
            foreach ($countryArr as $key => $country) {
                $countryDropdown    .= "<option value='$country->country_code'>$country->country</option> ";
            }
        }
        $countryDropdown            .= "</select>";
        echo $countryDropdown;
        exit();
    }

    /*
        ajax function to get country backup
    */
    public function getCountry_backup(Request $request) {

        $countryArr     = Country::select('country','id','country_code')
                        ->where("region","$request->region")
                        ->orderBy('country','asc')
                        ->get();
        $classname      = isset($request->classname)?$request->classname:'country';
        $idname         = isset($request->idname)?$request->idname:'country';

        $countryDropdown            = "";
        $countryDropdown            .= "<select class='form-control ".$classname."' name='country_id' id='".$idname."' >";
        $countryDropdown            .= "<option value=''>Select Country</option>";
        if(count($countryArr) > 0) {
            foreach ($countryArr as $key => $country) {
                $countryDropdown    .= "<option value='$country->country_code'>$country->country</option> ";
            }
        }
        $countryDropdown            .= "</select>";
        echo $countryDropdown;
        exit();
    }

    /*
        ajax function to get job position
    */
    public function getJobPosition(Request $request) {

        $jobPositionArr    = Jobposition::select('jobposition','id')
                        ->where("jobcategory","$request->jobcategory")
                        ->orderBy('jobposition','asc')
                        ->get();
        $jobPositionDropdown   = "";
        $jobPositionDropdown   .= "<select class='form-control province' name='district_id'>";
        $jobPositionDropdown   .= "<option value=''>Select Job Positions</option>";
        if(count($jobPositionArr) > 0) {
            foreach ($jobPositionArr as $key => $jobPosition) {
                $jobPositionDropdown   .= "<option value='$jobPosition->id'>$jobPosition->jobposition</option> ";
            }
        }
        $jobPositionDropdown   .= "</select>";
        echo $jobPositionDropdown;
        exit();
    }

    /*
        ajax function to get city
    */
    public function getCity(Request $request) {

        $cityArr    = City::select('city','id')
                        ->where("country","$request->country")
                        ->orderBy('city','asc')
                        ->get();

        $cityDropdown   = "";
        $cityDropdown   .= "<select class='form-control city_$request->address_id' name='city_id[]'>";
        $cityDropdown   .= "<option value='' disabled='disabled' selected='selected'>Select City</option>";
        if(count($cityArr) > 0) {
            foreach ($cityArr as $key => $city) {
                $cityDropdown .= "<option value='$city->id'>$city->city</option> ";
            }
        }
        $cityDropdown   .= "</select>";
        echo $cityDropdown;
        exit();
    }

    /*
        ajax function to get province
    */
    public function getProvince(Request $request) {
        $provinceArr    = Province::select('province','id')
                        ->where("country","$request->country")
                        ->orderBy('province','asc')
                        ->get();

        $provinceDropdown   = "";
        $provinceDropdown   .= "<select class='form-control province_$request->address_id' name='province_id[]'>";
        $provinceDropdown   .= "<option value='' disabled='disabled' selected='selected'>Select Province</option>";
        if(count($provinceArr) > 0) {
            foreach ($provinceArr as $key => $province) {
                $provinceDropdown   .= "<option value='$province->id'>$province->province</option> ";
            }
        }
        $provinceDropdown   .= "</select>";
        echo $provinceDropdown;
        exit();
    }

    /*
        ajax function to get province
    */
    public function getDistrict(Request $request) {

        $districtArr    = District::select('district','id')
                        ->where("country","$request->country")
                        ->orderBy('district','asc')
                        ->get();
        $districtDropdown   = "";
        $districtDropdown   .= "<select class='form-control province_$request->address_id' name='district_id[]'>";
        $districtDropdown   .= "<option value='' disabled='disabled' selected='selected'>Select District</option>";
        if(count($districtArr) > 0) {
            foreach ($districtArr as $key => $district) {
                $districtDropdown   .= "<option value='$district->id'>$district->district</option> ";
            }
        }
        $districtDropdown   .= "</select>";
        echo $districtDropdown;
        exit();
    }

    /*
        add and edit contact address
    */
    public function contactAddressStore(Request $request) {

        // $requestArr = self::rearrangeSubmit($request->all());
        if(isset($request['newaddress'])) {
            echo count($request['newaddress']);
        }
        echo "<pre>";print_r($request->all());exit();
        // echo "<pre>";print_r($requestArr);exit();


        $rules = [
            "type"                          => "required",
            "delivery_address"              => "required",
            "region_id"                     => "required",
            "country_id"                    => "required",
            "postal_code"                   => "required"
        ];
        $message = [
            "type.required"                 => "Slect the address type",
            "delivery_address.required"     => "Select the delivery address",
            "region_id.required"            => "Select the region",
            "country_id.required"           => "Select the country",
            "postal_code.required"          => "Postal code is required"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#address')->withErrors($validator)->with('submit_address',$request->submit_address);
            }
        }

        if(isset($request->id) && $request->id > 0) {
            $addressContact                 = ContactAddress::where('id',$request->id)->first();
            $addressContact->updated_at     = date('Y-m-d H:i:s');
        } else {
            $addressContact                 = new ContactAddress();
            $addressContact->created_at     = date('Y-m-d H:i:s');
            $addressContact->created_by     = Auth::user()->id;
        }

        $addressContact->contact_id         = $request->contact_id;
        $addressContact->type               = $request->type;
        $addressContact->delivery_address   = $request->delivery_address;
        $addressContact->region_id          = $request->region_id;
        $addressContact->country_id         = $request->country_id;
        $addressContact->province_id        = (!empty($request->province_id)?$request->province_id:null);
        $addressContact->district_id        = (!empty($request->district_id)?$request->district_id:null);
        $addressContact->city_id            = (!empty($request->city_id)?$request->city_id:null);
        $addressContact->postal_code        = (!empty($request->postal_code)?$request->postal_code:'');
        $addressContact->po_box             = (!empty($request->po_box)?$request->po_box:'');
        $addressContact->address_line1      = (!empty($request->address_line1)?$request->address_line1:'');
        $addressContact->address_line2      = (!empty($request->address_line2)?$request->address_line2:'');
        $addressContact->website            = (!empty($request->website)?$request->website:'');
        $addressContact->importance         = (!empty($request->importance)?$request->importance:'');
        $addressContact->caution            = (!empty($request->caution)?$request->caution:'');
        $addressContact->sensitivity        = (!empty($request->sensitivity)?$request->sensitivity:'');
        $addressContact->notes              = (!empty($request->notes)?$request->notes:'');
        $addressContact->keywords           = (!empty($request->keywords)?$request->keywords:'');
        $addressContact->updated_by         = Auth::user()->id;


        if($addressContact->save()) {
            return redirect('/contact/edit/'.$request->contact_id.'#jobposition')->with('addressSuccess', "Contact address saved successfully.");
        } else {
            return redirect('/contact/edit/'.$request->contact_id.'#jobposition')->with('addressError', "Something went wrong.");
        }
    }

    /*
        delete language keyword
    */
    public function delete(Request $request, $id = 0) {

        $contact    = Contact::find($id);
        if (count($contact) > 0) {

            if($contact->delete()) {
                return redirect('/contact')->with('success', "Contact deleted successfully.");
            } else {
                return redirect('/contact')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/contact')->with('error', trans('Record not found'));
    }

    public function arrayData(Datatables $datatables) {

        $builder = Contact::query()->select('id', 'title', 'blood_type','gender','religion','marital_status' ,'full_name','nick_name', 'birth_place', 'status', 'created_at', 'updated_at');


        return $datatables->eloquent($builder)
                            ->addColumn('city', function ($contact) {
                                 $addressContact       =
                                 ContactAddress::where('contact_id',$contact->id)->first();

                                 if($addressContact !== null  && isset($addressContact->city_id) ) {
                                   $city =  City::select('city')
                                            ->where("id",$addressContact->city_id)
                                            ->get();

                                    return "<a href=".url('contact/view/' . $contact->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                            ".$city[0]["city"]."
                                        </a>";
                                 }

                            })
                            ->addColumn('email', function ($contact) {
                                 $communicationContact       =

                                 ContactCommunication::where('contact_id',$contact->id)->first();
                                 if($communicationContact !== null  && isset($communicationContact->email) && $communicationContact->email !== "") {

                                    return "<a href=".url('contact/view/' . $contact->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                            ".$communicationContact->email."
                                        </a>";
                                 }

                            })
                            ->addColumn('phone', function ($contact) {
                                 $contactNumber              = ContactNumber::where('contact_id',$contact->id)->first();
                                 if($contactNumber !== null  && isset($contactNumber->number) && $contactNumber->number !== "") {

                                    return "<a href=".url('contact/view/' . $contact->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                            ".$contactNumber->number."
                                        </a>";
                                 }

                            })

                            ->editColumn('title', function ($contact) {

                                $titleList = ['Mr.' ,'Mrs.' , 'Miss.'];
                                $title = isset($contact->title) ?  $titleList[$contact->title-1] : "";
                                                    return "<a href=".url('contact/view/' . $contact->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                                        ".$title."
                                                    </a>";
                                                })
                            ->editColumn('gender', function ($contact) {
                                $genderList = ['Female' ,'Male'];
                                $gender = isset($contact->gender) ?  $genderList[$contact->gender-1] : "";
                                                    return "<a href=".url('contact/view/' . $contact->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                                        ".$gender."
                                                    </a>";
                             })
                             ->editColumn('religion', function ($contact) {
                                $religionList = ['Christian',  'Muslim'];
                                $religion = isset($contact->religion) ?  $religionList[$contact->religion-1] : "";
                                                    return "<a href=".url('contact/view/' . $contact->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                                        ".$religion."
                                                    </a>";
                             })
                            ->editColumn('full_name', function ($contact) {
                                return "<a href=".url('contact/view/' . $contact->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$contact->full_name."
                                </a>";
                            })
                            ->editColumn('nick_name', function ($contact) {
                                return "<a href=".url('contact/view/' . $contact->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".($contact->nick_name)."
                                </a>";
                            })
                              ->editColumn('birth_place', function ($contact) {
                                return "<a href=".url('contact/view/' . $contact->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".($contact->birth_place)."
                                </a>";
                            })
                            ->editColumn('status',function($contact) {
                                 if($contact->status == 1){
                                   return '<span class="badge bg-green">Active</span>';
                                } else if($contact->status == 0){
                                   return  '<span class="badge bg-red">Inactive</span>';
                                } else {
                                   return '<span class="badge bg-yellow">Delete</span>';
                                }
                            })
                            ->editColumn('created_at', function ($contact) {
                                if($contact->created_at !== null ) {
                                    return  date("j-M-y g:m A",strtotime($contact->created_at))  ;
                                }
                                return '';
                           })
                           ->editColumn('updated_at', function ($contact) {
                                if($contact->updated_at !== null ) {
                                    return  date("j-M-y g:m A",strtotime($contact->updated_at))  ;
                                }
                                return '';
                           })

                          ->addColumn('action', function($contact) {
                            return "<a href=".url('contact/delete/' . $contact->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                          })
                          ->rawColumns(['city','email', 'phone' ,'title','gender','religion','full_name','nick_name','birth_place','action','status'])
                          ->toJson();
    }

    /*
        This function is used to download excel
    */
    public function download(){

        $data   = Contact::query()->select('id', 'title', 'suffix', 'first_name','father_name', 'mother_name' ,
            'surname' ,'full_name', 'nick_name','date_of_birth','birth_place' ,'blood_type' ,'gender' ,'marital_status',
            'status','created_at','updated_at'
        )->get();



        $filename = "contact";
        $export_array = [];
        foreach ($data as $key => $row) {
            array_push($export_array,
                [
                   "id"          => $row->id,
                   "title"       => $row->title,
                   "suffix"      => $row->suffix,
                   "first_name"  => $row->first_name,
                   "father_name"  => $row->father_name,
                   "mother_name"  => $row->mother_name,
                   "surname"      => $row->surname,
                   "full_name"    => $row->full_name,
                   "nick_name"    => $row->nick_name,
                   "date_of_birth" => ($row->date_of_birth !== null) ? date("d-M-y",strtotime($row->date_of_birth)) : "",
                   "birth_place"   => $row->birth_place,
                   "gender"        => $row->gender !== null ? ($row->gender == 1? "Male" : "Female"): "",
                   "status"     => $row->status == 1 ? "Active" : "In Active",
                   "created_at" => date("j-M-y g:m A",strtotime( $row->created_at)),
                   "updated_at" => ($row->updated_at !== null) ? date("j-M-y g:m A",strtotime($row->updated_at)) : ""
                ]);
        }
        $data = $export_array;
            set_time_limit(0);
            ini_set('memory_limit', '2G');
            return Excel::create($filename, function($excel) use ($data,$filename) {
                $excel->sheet($filename, function($sheet) use ($data)
                {
                    $sheet->fromArray($data, null, 'A1', true);
                    // Set auto size for sheet
                    $sheet->setAutoSize(true);
                    //$sheet->setWidth('A', 3);
                    //$sheet->setHeight(1, 20);
                    // Freeze the first column
                    $sheet->freezeFirstColumn();
                    // Font family
                    $sheet->setFontFamily('Comic Sans MS');

                    // Font size
                    $sheet->setFontSize(10);

                    // Font bold
                    //$sheet->setFontBold(true);
                    // Sets all borders
                    $sheet->setAllBorders('thin');

                    // Set border for cells
                    $sheet->setBorder('A1', 'thin');
                    $sheet->row(1, ['Id', 'Title',  'Suffix' ,'First Name' , 'Father Name','Mother Name' ,'Surname'
                        , 'Full Name' , 'Nick Name', 'Birth Date' , 'Birth Place' , 'Gender'
                    ,'Status' , 'Created On', 'Updated On']); // etc etc
                    // Set black background
                    $sheet->row(1, function($row) {
                        // call cell manipulation methods
                        $row->setBackground('#000000');
                        $row->setFontColor('#ffffff');
                    });
                });
            })->download('xls');
    }

    /*
        Store Contact job position
    */
    public function jobPositionStore(Request $request) {
        // echo "<pre>";print_r($request->all());die;

        $rules = [
            "job_type"          => "required",
            "start_date"        => "required",
            // "exit_date"         => "after:start_date",
            // "oranization"       => "required",
            // "job_category"      => "required",
            // "job_positions"     => "required",
            // "linkedin"          => "required",
            // "region_id"         => "required",
            // "country_id"        => "required",
            // "province_id"       => "required",
            // "district_id"       => "required",
            // "city_id"           => "required",
            // "postal_code"       => "required",
            // "po_box"            => "required",
            // "address_line_1"    => "required",
            // "address_line_2"    => "required",
            // "website"           => "required",
            // "status"            => "required",
            // "keywords"          => "required",
            // "notes"             => "required"
        ];
        $message = [
            "job_type.required"         => "Select the job type",
            "start_date.required"       => "Select the start date",
            // "exit_date.after"           => "Exit date must be a greater than start date",
            // "oranization.required"      => "Organization is required",
            // "job_category.required"     => "Select the job category",
            // "job_positions.required"    => "Select the job position",
            // "linkedin.required"         => "Linkedin is required",
            // "region_id.required"        => "Select the region",
            // "country_id.required"       => "Select the country",
            // "province_id.required"      => "Select the province",
            // "district_id.required"      => "Select the district",
            // "city_id.required"          => "Select the city",
            // "postal_code.required"      => "Postal code is required",
            // "po_box.required"           => "Po Box is required",
            // "address_line_1.required"   => "Address line 1 is required",
            // "address_line_2.required"   => "Address line 2 is required",
            // "website.required"          => "Website is required",
            // "status.required"           => "Select the status",
            // "keywords.required"         => "Keywords is required",
            // "notes.required"            => "Notes is required"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#jobposition')->withErrors($validator)->with('submit_job_position',$request->submit_job_position);
            }
        }

        if(isset($request->id) && $request->id > 0) {
            $contactJobPositionObj              = ContactJob::where('id',$request->id)->first();
            $contactJobPositionObj->updated_by  = \Auth::user()->id;
            $contactJobPositionObj->updated_at  = date('Y-m-d H:i:s');
        } else {
            $contactJobPositionObj = new ContactJob;
            $contactJobPositionObj->created_by  = \Auth::user()->id;
            $contactJobPositionObj->created_at  = date('Y-m-d H:i:s');
        }

        $contactJobPositionObj->contact_id      = $request->contact_id;
        $contactJobPositionObj->job_type        = $request->job_type;
        $contactJobPositionObj->oranization     = (!empty($request->oranization)?$request->oranization:'');
        $contactJobPositionObj->job_category    = (!empty($request->job_category)?$request->job_category:'');
        $contactJobPositionObj->job_positions   = (!empty($request->job_positions)?$request->job_positions:'');
        $contactJobPositionObj->linkedin        = (!empty($request->linkedin)?$request->linkedin:'');
        $contactJobPositionObj->start_date      = (!empty($request->start_date)?date('Y-m-d',strtotime($request->start_date)):'');
        $contactJobPositionObj->exit_date       = (!empty($request->exit_date)?date('Y-m-d',strtotime($request->exit_date)):'');
        $contactJobPositionObj->region_id       = (!empty($request->region_id)?$request->region_id:null);
        $contactJobPositionObj->country_id      = (!empty($request->country_id)?$request->country_id:'');
        $contactJobPositionObj->province_id     = (!empty($request->province_id)?$request->province_id:null);
        $contactJobPositionObj->district_id     = (!empty($request->district_id)?$request->district_id:null);
        $contactJobPositionObj->city_id         = (!empty($request->city_id)?$request->city_id:null);
        $contactJobPositionObj->postal_code     = (!empty($request->postal_code)?$request->postal_code:'');
        $contactJobPositionObj->po_box          = (!empty($request->po_box)?$request->po_box:'');
        $contactJobPositionObj->address_line_1  = (!empty($request->address_line_1)?$request->address_line_1:'');
        $contactJobPositionObj->address_line_2  = (!empty($request->address_line_2)?$request->address_line_2:'');
        $contactJobPositionObj->website         = (!empty($request->website)?$request->website:'');
        $contactJobPositionObj->status          = (!empty($request->status)?$request->status:null);
        $contactJobPositionObj->keywords        = (!empty($request->keywords)?$request->keywords:'');
        $contactJobPositionObj->notes           = (!empty($request->notes)?$request->notes:'');

        if($contactJobPositionObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#spouse')->with('jobpositionSuccess', "Job Positions saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#jobposition')->with('jobpositionError', "Something went wrong please try later.");
        }
    }

    /*
        Store Contact business
    */
    public function businessStore(Request $request) {
        // echo "<pre>";print_r($request->all());die;

        $rules = [
            // "business_type"         => "required",
            // "organization"          => "required",
            // "description"           => "required",
            "start_at"              => "required",
            // "number_of_employees"   => "required",
            // "region_id"             => "required",
            // "country_id"            => "required",
            // "province_id"           => "required",
            // "district_id"           => "required",
            // "city_id"               => "required",
            // "postal_code"           => "required",
            // "po_box"                => "required",
            // "address_line_1"        => "required",
            // "address_line_2"        => "required",
            // "website"               => "required",
            // "Keywords"              => "required",
            // "notes"                 => "required"
        ];
        $message = [
            // "business_type.required"            => "Select the business type",
            // "organization.required"             => "Organization is required",
            // "description.required"              => "Description is required",
            "start_at.required"                 => "Select the start date",
            // "number_of_employees.required"      => "Select the number of employees",
            // "region_id.required"                => "Select the region",
            // "country_id.required"               => "Select the country",
            // "province_id.required"              => "Select the province",
            // "district_id.required"              => "Select the district",
            // "city_id.required"                  => "Select the city",
            // "postal_code.required"              => "Postal code is required",
            // "po_box.required"                   => "Po Box is required",
            // "address_line_1.required"           => "Address line 1 is required",
            // "address_line_2.required"           => "Address line 2 is required",
            // "website.required"                  => "Website is required",
            // "Keywords.required"                 => "Keywords is required",
            // "notes.required"                    => "Notes is required"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#business')->withErrors($validator)->with('submit_business',$request->submit_business);
            }
        }

        if(isset($request->id) && $request->id > 0) {
            $contactbusinessObj                 = ContactBusiness::where('id',$request->id)->first();
            $contactbusinessObj->updated_at     = date('Y-m-d H:i:s');
        } else {
            $contactbusinessObj = new ContactBusiness;
            $contactbusinessObj->created_by     = \Auth::user()->id;
            $contactbusinessObj->created_at     = date('Y-m-d H:i:s');
        }

        $contactbusinessObj->contact_id             = $request->contact_id;
        $contactbusinessObj->business_type          = (!empty($request->business_type)?$request->business_type:'');
        $contactbusinessObj->organization           = (!empty($request->organization)?$request->organization:'');
        $contactbusinessObj->description            = (!empty($request->description)?$request->description:'');
        $contactbusinessObj->start_at               = (!empty($request->start_at)?date('Y-m-d',strtotime($request->start_at)):'');
        $contactbusinessObj->number_of_employees    = (!empty($request->number_of_employees)?$request->number_of_employees:null);
        $contactbusinessObj->region_id              = (!empty($request->region_id)?$request->region_id:null);
        $contactbusinessObj->country_id             = (!empty($request->country_id)?$request->country_id:'');
        $contactbusinessObj->province_id            = (!empty($request->province_id)?$request->province_id:null);
        $contactbusinessObj->district_id            = (!empty($request->district_id)?$request->district_id:null);
        $contactbusinessObj->city_id                = (!empty($request->city_id)?$request->city_id:null);
        $contactbusinessObj->postal_code            = (!empty($request->postal_code)?$request->postal_code:'');
        $contactbusinessObj->po_box                 = (!empty($request->po_box)?$request->po_box:'');
        $contactbusinessObj->address_line_1         = (!empty($request->address_line_1)?$request->address_line_1:'');
        $contactbusinessObj->address_line_2         = (!empty($request->address_line_2)?$request->address_line_2:'');
        $contactbusinessObj->website                = (!empty($request->website)?$request->website:'');
        $contactbusinessObj->Keywords               = (!empty($request->Keywords)?$request->Keywords:'');
        $contactbusinessObj->notes                  = (!empty($request->notes)?$request->notes:'');
        $contactbusinessObj->updated_by             = \Auth::user()->id;

        if($contactbusinessObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#representative')->with('businessSuccess', "Contact Business saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#business')->with('businessError', "Something went wrong please try later.");
        }
    }

    /*
        add and edit representative contact
    */
    public function representativeStore(Request $request) {
        // echo "<pre>";print_r($request->all());die;
        $rules = [
            "representative_title"      => "required",
            "representative_first_name" => "required",
            "representative_suffix"     => "required",
            "representative_surname"    => "required"
        ];

        $message = [
            'representative_title.required'         => 'Select the title',
            'representative_first_name.required'    => 'Select the first name',
            'representative_suffix.required'        => 'Select the suffix',
            'representative_surname.required'       => 'Select the surname'
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#representative')->withErrors($validator)->with('submit_representative',$request->submit_representative);
            }
        }

        //device image upload
        if(count($request->file('representative_photo')) > 0) {
            $imageUpload        = Contact::imageUpload($request->file('representative_photo'));

            if($imageUpload == "") {
                $contactImageName   = (isset(
            $request->old_contact_image)?$request->old_contact_image:"");
            } else {
                $contactImageName   = $imageUpload;
            }

            // $contactImageFile    = $request->file('representative_photo');
            // $contactImagePath    = public_path('../uploads/contact/');
            // $contactImageName    = uniqid().'.'.$contactImageFile->getClientOriginalExtension();
            // $contactImageFile->move($contactImagePath,$contactImageName);

            // if(!empty($request->old_contact_representative_image)) {
            //     File::delete('uploads/contact/' . $request->old_contact_representative_image);
            // }
        } else {
            $contactImageName    = (isset(
        $request->old_contact_representative_image)?$request->old_contact_representative_image:"");
        }

        if(isset($request->id) && $request->id > 0) {
            $contactrepresentativeObj               = ContactRepresentative::where('id',$request->id)->first();
            $contactrepresentativeObj->updated_at   = date('Y-m-d H:i:s');
        } else {
            $contactrepresentativeObj = new ContactRepresentative;
            $contactrepresentativeObj->created_by   = \Auth::user()->id;
            $contactrepresentativeObj->created_at   = date('Y-m-d H:i:s');
        }

        $contactrepresentativeObj->contact_id       = $request->contact_id;
        $contactrepresentativeObj->title            = $request->representative_title;

        $contactrepresentativeObj->suffix           = (!empty($request->representative_suffix)?implode(',', $request->representative_suffix):'');

        $contactrepresentativeObj->first_name       = $request->representative_first_name;

        $contactrepresentativeObj->father_name      = (!empty($request->representative_father_name)?$request->representative_father_name:'');

        $contactrepresentativeObj->mother_name      = (!empty($request->representative_mother_name)?$request->representative_mother_name:'');

        $contactrepresentativeObj->surname          = $request->representative_surname;

        $contactrepresentativeObj->nickname         = (!empty($request->representative_nick_name)?$request->representative_nick_name:'');

        $contactrepresentativeObj->full_name        = $contactrepresentativeObj->first_name.' '.$contactrepresentativeObj->father_name.' '.$contactrepresentativeObj->surname;

        $contactrepresentativeObj->date_of_birth    = (!empty($request->representative_date_of_birth)?date('Y-m-d',strtotime($request->representative_date_of_birth)):'');

        $contactrepresentativeObj->birth_place      = (!empty($request->representative_birth_place)?$request->representative_birth_place:'');

        $contactrepresentativeObj->blood_type       = (!empty($request->representative_blood_type)?$request->representative_blood_type:'');

        $contactrepresentativeObj->gender           = (!empty($request->representative_gender)?$request->representative_gender:null);

        $contactrepresentativeObj->number_of_dependents = (!empty($request->representative_number_of_dependents)?$request->representative_number_of_dependents:null);

        $contactrepresentativeObj->nationality      = (!empty($request->representative_nationality)?implode(',', $request->representative_nationality):'');

        $contactrepresentativeObj->languages        = (!empty($request->representative_languages)?implode(',', $request->representative_languages):'');

        $contactrepresentativeObj->profession       = (!empty($request->representative_profession)?implode(',', $request->representative_profession):'');

        $contactrepresentativeObj->importance       = (!empty($request->representative_importance)?$request->representative_importance:'');

        $contactrepresentativeObj->caution          = (!empty($request->representative_caution)?$request->representative_caution:'');

        $contactrepresentativeObj->sensitivity      = (!empty($request->representative_sensitivity)?$request->representative_sensitivity:'');

        $contactrepresentativeObj->photo            = $contactImageName;

        $contactrepresentativeObj->private          = (!empty($request->representative_private)?$request->representative_private:null);

        $contactrepresentativeObj->business_owner   = (!empty($request->representative_business_owner)?$request->representative_business_owner:null);

        $contactrepresentativeObj->sponsorships     = (!empty($request->representative_sponsorships)?implode(',', $request->representative_sponsorships):'');

        $contactrepresentativeObj->charitable_organizations = (!empty($request->representative_charitable_organizations)?implode(',', $request->representative_charitable_organizations):'');

        $contactrepresentativeObj->political_party  = (!empty($request->representative_political_party)?implode(',', $request->representative_political_party):'');

        $contactrepresentativeObj->relationship_preference  = (!empty($request->representative_relationship_preference)?implode(',', $request->representative_relationship_preference):'');

        $contactrepresentativeObj->religion_id      = (!empty($request->representative_religion)?$request->representative_religion:null);

        $contactrepresentativeObj->internal_organizations   = (!empty($request->representative_internal_organizations)?implode(',', $request->representative_internal_organizations):'');

        $contactrepresentativeObj->administrative_group     = (!empty($request->representative_administrative_group)?implode(',', $request->representative_administrative_group):'');

        $contactrepresentativeObj->status           = (!empty($request->representative_status)?$request->representative_status:null);

        $contactrepresentativeObj->notes            = (!empty($request->representative_notes)?$request->representative_notes:'');

        $contactrepresentativeObj->keywords         = (!empty($request->representative_keywords)?$request->representative_keywords:'');

        $contactrepresentativeObj->updated_by       = \Auth::user()->id;
        // echo "<pre>";print_r($contactrepresentativeObj);die;
        if($contactrepresentativeObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#referral')->with('representativeSuccess', "Contact Representative saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#representative')->with('representativeError', "Something went wrong please try later.");
        }
    }

    /*
        add and edit referral contact
    */
    public function referralStore(Request $request) {
        $rules = [
            "referral_title"      => "required",
            "referral_first_name" => "required",
            "referral_suffix"     => "required",
            "referral_surname"    => "required"
        ];
        $message = [
            'referral_title.required'         => 'Select the title',
            'referral_first_name.required'    => 'Select the first name',
            'referral_suffix.required'        => 'Select the Suffix',
            'referral_surname.required'       => 'Select the surname'
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#referral')->withErrors($validator)->with('submit_referral',$request->submit_referral);
            }
        }

        //device image upload
        if(count($request->file('referral_photo')) > 0) {
            $imageUpload        = Contact::imageUpload($request->file('referral_photo'));

            if($imageUpload == "") {
                $contactImageName   = (isset(
            $request->old_contact_image)?$request->old_contact_image:"");
            } else {
                $contactImageName   = $imageUpload;
            }

            // $contactImageFile    = $request->file('referral_photo');
            // $contactImagePath    = public_path('../uploads/contact/');
            // $contactImageName    = uniqid().'.'.$contactImageFile->getClientOriginalExtension();
            // $contactImageFile->move($contactImagePath,$contactImageName);

            // if(!empty($request->old_contact_representative_image)) {
            //     File::delete('uploads/contact/' . $request->old_contact_representative_image);
            // }
        } else {
            $contactImageName    = (isset(
        $request->old_contact_referral_image)?$request->old_contact_referral_image:"");
        }

        if(isset($request->id) && $request->id > 0) {
            $contactreferralObj                 = ContactReferral::where('id',$request->id)->first();
            $contactreferralObj->updated_at     = date('Y-m-d H:i:s');
        } else {
            $contactreferralObj = new ContactReferral;
            $contactreferralObj->created_by     = \Auth::user()->id;
            $contactreferralObj->created_at     = date('Y-m-d H:i:s');
        }

        $contactreferralObj->contact_id       = $request->contact_id;
        $contactreferralObj->title            = $request->referral_title;

        $contactreferralObj->suffix           = (!empty($request->referral_suffix)?implode(',', $request->referral_suffix):'');

        $contactreferralObj->first_name       = $request->referral_first_name;

        $contactreferralObj->father_name      = (!empty($request->referral_father_name)?$request->referral_father_name:'');

        $contactreferralObj->mother_name      = (!empty($request->referral_mother_name)?$request->referral_mother_name:'');

        $contactreferralObj->surname          = $request->referral_surname;

        $contactreferralObj->nickname         = (!empty($request->referral_nick_name)?$request->referral_nick_name:'');

        $contactreferralObj->full_name        = $contactreferralObj->first_name.' '.$contactreferralObj->father_name.' '.$contactreferralObj->surname;

        $contactreferralObj->date_of_birth    = (!empty($request->referral_date_of_birth)?date('Y-m-d',strtotime($request->referral_date_of_birth)):'');

        $contactreferralObj->birth_place      = (!empty($request->referral_birth_place)?$request->referral_birth_place:'');

        $contactreferralObj->blood_type       = (!empty($request->referral_blood_type)?$request->referral_blood_type:'');

        $contactreferralObj->gender           = (!empty($request->referral_gender)?$request->referral_gender:null);

        $contactreferralObj->number_of_dependents = (!empty($request->referral_number_of_dependents)?$request->referral_number_of_dependents:null);

        $contactreferralObj->nationality      = (!empty($request->referral_nationality)?implode(',', $request->referral_nationality):'');

        $contactreferralObj->languages        = (!empty($request->referral_languages)?implode(',', $request->referral_languages):'');

        $contactreferralObj->profession       = (!empty($request->referral_profession)?implode(',', $request->referral_profession):'');

        $contactreferralObj->importance       = (!empty($request->referral_importance)?$request->referral_importance:'');

        $contactreferralObj->caution          = (!empty($request->referral_caution)?$request->referral_caution:'');

        $contactreferralObj->sensitivity      = (!empty($request->referral_sensitivity)?$request->referral_sensitivity:'');

        $contactreferralObj->photo            = $contactImageName;

        $contactreferralObj->private          = (!empty($request->referral_private)?$request->referral_private:null);

        $contactreferralObj->business_owner   = (!empty($request->referral_business_owner)?$request->referral_business_owner:null);

        $contactreferralObj->sponsorships     = (!empty($request->referral_sponsorships)?implode(',', $request->referral_sponsorships):'');

        $contactreferralObj->charitable_organizations = (!empty($request->referral_charitable_organizations)?implode(',', $request->referral_charitable_organizations):'');

        $contactreferralObj->political_party  = (!empty($request->referral_political_party)?implode(',', $request->referral_political_party):'');

        $contactreferralObj->relationship_preference  = (!empty($request->referral_relationship_preference)?implode(',', $request->referral_relationship_preference):'');

        $contactreferralObj->religion_id      = (!empty($request->referral_religion)?$request->referral_religion:null);

        $contactreferralObj->internal_organizations   = (!empty($request->referral_internal_organizations)?implode(',', $request->referral_internal_organizations):'');

        $contactreferralObj->administrative_group     = (!empty($request->referral_administrative_group)?implode(',', $request->referral_administrative_group):'');

        $contactreferralObj->status           = (!empty($request->referral_status)?$request->referral_status:null);

        $contactreferralObj->notes            = (!empty($request->referral_notes)?$request->referral_notes:'');

        $contactreferralObj->keywords         = (!empty($request->referral_keywords)?$request->referral_keywords:'');

        $contactreferralObj->updated_by       = \Auth::user()->id;
        // echo "<pre>";print_r($contactreferralObj);die;
        if($contactreferralObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#politicalPositions')->with('referralSuccess', "Contact Referral saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#referral')->with('error', "Something went wrong please try later.");
        }
    }

    /*
        view contact detail
    */
    public function view(Request $request,$id = 0) {

        $contact        = Contact::where('id',$id)
                            ->with(['user_createby' => function($query) {
                                $query->select('id','first_name','last_name');
                            }])
                            ->with(['user_updateby' => function($query) {
                                $query->select('id','first_name','last_name');
                            }])
                            ->first();
                            
        if(empty($contact)) {
            return View::make('Admin.error.404');
        }                              
        
        $bloodType      = Bloodtype::orderBy('bloodtype')->pluck('bloodtype', 'id');
        $gender         = Gender::pluck('gender', 'id');
        $maritalStatus  = Maritalstatus::pluck('marital_status', 'id');
        $languages      = Language::pluck('title', 'id');
        $nationality    = Country::pluck('nationality','country_code');
        $countryCode    = Country::pluck('country_code','id');

        $nations        = Country::get();

        //echo $nationality['ET']."<pre>";print_r($nationality);exit();

        $region         = Region::pluck('region','id');

        $jobCategory    = Jobcategory::whereIn('id',[1,35,27,39,57])->pluck('jobcategory','id');


        //contact address data
        $addressContact = ContactAddress::where('contact_id',$id)
                            ->with(['user_createby' => function($query) {
                                $query->select('id','first_name','last_name');
                            }])
                            ->with(['user_updateby' => function($query) {
                                $query->select('id','first_name','last_name');
                            }])
                            ->first();
                            

        $province       = Province::where('country',isset($addressContact->country_id)?$addressContact->country_id:0)->pluck('province','id');
        $district       = District::where('country',isset($addressContact->country_id)?$addressContact->country_id:0)->pluck('district','id');
        $city           = City::pluck('city','id');

        //contact job position data - start
        $jobpositionDetail  = ContactJob::where('contact_id',$id)
                                ->with(['user_createby' => function($query) {
                                    $query->select('id','first_name','last_name');
                                }])
                                ->with(['user_updateby' => function($query) {
                                    $query->select('id','first_name','last_name');
                                }])
                            ->first();
                                    
        $jobPositions       = Jobposition::where('jobcategory',isset($jobpositionDetail->job_category)?$jobpositionDetail->job_category:0)->pluck('jobposition','id');
        $jobProvince        = Province::where('country',isset($jobpositionDetail->country_id)?$jobpositionDetail->country_id:0)->pluck('province','id');
        $jobDistrict        = District::where('country',isset($jobpositionDetail->country_id)?$jobpositionDetail->country_id:0)->pluck('district','id');
        $jobCity            = City::where('country',isset($jobpositionDetail->country_id)?$jobpositionDetail->country_id:0)->pluck('city','id');
        //contact job position data - end

        //contact spouse data
        $spouseContact  = ContactSpouse::where('contact_id',$id)->first();

        //contact business data - start
        $businessDetail     = ContactBusiness::where('contact_id',$id)
                                ->with(['user_createby' => function($query) {
                                    $query->select('id','first_name','last_name');
                                }])
                                ->with(['user_updateby' => function($query) {
                                    $query->select('id','first_name','last_name');
                                }])
                                ->first();
        $businessProvince   = Province::where('country',isset($businessDetail->country_id)?$businessDetail->country_id:0)->pluck('province','id');
        $businessDistrict   = District::where('country',isset($businessDetail->country_id)?$businessDetail->country_id:0)->pluck('district','id');
        $businessCity       = City::where('country',isset($businessDetail->country_id)?$businessDetail->country_id:0)->pluck('city','id');

        //contact representative data - start
        $representativeContact      = ContactRepresentative::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact representative data - end

        //contact referral data - start
        $referralContact            = ContactReferral::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact referral data - end

        //contact Political Position data - start
        $politicalPositionContact   = ContactPoliticalPosition::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact Political Position data - end

        //contact Dependent data - start
        $dependentsContact          = ContactDependent::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact Dependent data - end

        //contact number data - start
        $contactNumber              = ContactNumber::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact number data - end

        //contact communication data - start
        $communicationContact       = ContactCommunication::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact communication data - end

        //contact education data - start
        $educationContact           = ContactEducation::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact education data - end

        //contact greetings letter data - start
        $greetingsContact           = ContactGreetingsLetter::where('contact_id',$id)
                                        ->with(['user_createby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->with(['user_updateby' => function($query) {
                                            $query->select('id','first_name','last_name');
                                        }])
                                        ->first();
        //contact education data - end

        $imageUrl   = "https://s3-ap-southeast-1.amazonaws.com/contactstorage/";

        return view('Admin.contact.viewContact',['nations' => $nations  , 'contact' => $contact,'contactId' => $id,'addressContact' => $addressContact,'bloodType' => $bloodType, 'gender' => $gender,'maritalStatus' => $maritalStatus, 'languages' => $languages,'nationality' => $nationality, 'region' => $region,'province' => $province , 'district' => $district, 'city' => $city ,'jobCategory' => $jobCategory, 'jobpositions' => $jobPositions, 'jobProvince' => $jobProvince, 'jobDistrict' => $jobDistrict, 'jobCity' => $jobCity,'jobpositionDetail' => $jobpositionDetail,'spouseContact' => $spouseContact, 'businessDetail' => $businessDetail, 'businessProvince' => $businessProvince, 'businessDistrict' => $businessDistrict, 'businessCity' => $businessCity, 'representativeContact' => $representativeContact, 'referralContact' => $referralContact, 'politicalPositionContact' => $politicalPositionContact, 'dependentsContact' => $dependentsContact,'countryCode' => $countryCode, 'contactNumber' => $contactNumber, 'communicationContact' => $communicationContact, 'educationContact' => $educationContact, 'greetingsContact' => $greetingsContact, 'imageUrl' => $imageUrl ]);
    }

    /*
        Store Contact Political Positions
    */
    public function politicalPositionsStore(Request $request) {
        // echo "<pre>";print_r($request->all());die;

        $rules = [
            "political_posotion_type"       => "required",
            "political_posotion_held"       => "required",
            "political_posotion_start_date" => "required",
            // "political_posotion_exit_date"  => "after:political_posotion_start_date"
        ];
        $message = [
            "political_posotion_type.required"          => "Select the political posotion type",
            "political_posotion_held.required"          => "Select the political posotion held",
            "political_posotion_start_date.required"    => "Select the start date",
            // "political_posotion_exit_date.after"        => "Exit date must be a greater than start date"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#politicalPositions')->withErrors($validator)->with('submit_political_positions',$request->submit_political_positions);
            }
        }
        if(isset($request->id) && $request->id > 0) {
            $contactPoliticalPositionsObj               = ContactPoliticalPosition::where('id',$request->id)->first();
            $contactPoliticalPositionsObj->updated_at   = date('Y-m-d H:i:s');
        } else {
            $contactPoliticalPositionsObj = new ContactPoliticalPosition;
            $contactPoliticalPositionsObj->created_by   = \Auth::user()->id;
            $contactPoliticalPositionsObj->created_at   = date('Y-m-d H:i:s');
        }

        $contactPoliticalPositionsObj->contact_id           = $request->contact_id;
        $contactPoliticalPositionsObj->type                 = $request->political_posotion_type;
        $contactPoliticalPositionsObj->political_posotion   = $request->political_posotion_held;
        $contactPoliticalPositionsObj->description          = isset($request->political_posotion_description)?$request->political_posotion_description:'';
        $contactPoliticalPositionsObj->greeting             = isset($request->political_posotion_greeting)?$request->political_posotion_greeting:'';
        $contactPoliticalPositionsObj->political_party      = isset($request->political_posotion_party)?$request->political_posotion_party:'';
        $contactPoliticalPositionsObj->start_at             = date('Y-m-d',strtotime($request->political_posotion_start_date));
        $contactPoliticalPositionsObj->end_at               = isset($request->political_posotion_exit_date)?date('Y-m-d',strtotime($request->political_posotion_exit_date)):'';
        $contactPoliticalPositionsObj->notes                = isset($request->political_posotion_notes)?$request->political_posotion_notes:'';
        $contactPoliticalPositionsObj->keywords             = isset($request->political_posotion_keywords)?$request->political_posotion_keywords:'';
        $contactPoliticalPositionsObj->updated_by           = \Auth::user()->id;
        if($contactPoliticalPositionsObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#dependents')->with('politicalSuccess', "Political Positions saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#politicalPositions')->with('politicalError', "Something went wrong please try later.");
        }
    }

    /*
        add and edit dependents contact
    */
    public function dependentsStore(Request $request) {
        // echo "<pre>";print_r($request->all());die;
        $rules = [
            "dependents_title"      => "required",
            "dependents_first_name" => "required",
            "dependents_suffix"     => "required",
            "dependents_surname"    => "required"
        ];
        $message = [
            'dependents_title.required'         => 'Select the title',
            'dependents_first_name.required'    => 'Select the first name',
            'dependents_suffix.required'        => 'Select the suffix',
            'dependents_surname.required'       => 'Select the surname'
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#dependents')->withErrors($validator)->with('submit_dependents',$request->submit_dependents);
            }
        }

        //device image upload
        if(count($request->file('dependents_photo')) > 0) {
            $imageUpload        = Contact::imageUpload($request->file('dependents_photo'));

            if($imageUpload == "") {
                $contactImageName   = (isset(
            $request->old_contact_image)?$request->old_contact_image:"");
            } else {
                $contactImageName   = $imageUpload;
            }

            // $contactImageFile    = $request->file('dependents_photo');
            // $contactImagePath    = public_path('../uploads/contact/');
            // $contactImageName    = uniqid().'.'.$contactImageFile->getClientOriginalExtension();
            // $contactImageFile->move($contactImagePath,$contactImageName);

            // if(!empty($request->old_contact_dependents_image)) {
            //     File::delete('uploads/contact/' . $request->old_contact_dependents_image);
            // }
        } else {
            $contactImageName    = (isset(
        $request->old_contact_dependents_image)?$request->old_contact_dependents_image:"");
        }

        if(isset($request->id) && $request->id > 0) {
            $contactdependentsObj               = ContactDependent::where('id',$request->id)->first();
            $contactdependentsObj->updated_at   = date('Y-m-d H:i:s');
        } else {
            $contactdependentsObj = new ContactDependent;
            $contactdependentsObj->created_by   = \Auth::user()->id;
            $contactdependentsObj->created_at   = date('Y-m-d H:i:s');
        }

        $contactdependentsObj->contact_id               = $request->contact_id;
        $contactdependentsObj->title                    = $request->dependents_title;

        $contactdependentsObj->suffix                   = (!empty($request->dependents_suffix)?implode(',', $request->dependents_suffix):'');

        $contactdependentsObj->first_name               = $request->dependents_first_name;

        $contactdependentsObj->father_name              = (!empty($request->dependents_father_name)?$request->dependents_father_name:'');

        $contactdependentsObj->mother_name              = (!empty($request->dependents_mother_name)?$request->dependents_mother_name:'');

        $contactdependentsObj->surname                  = $request->dependents_surname;

        $contactdependentsObj->full_name                = $contactdependentsObj->first_name.' '.$contactdependentsObj->father_name.' '.$contactdependentsObj->surname;

        $contactdependentsObj->nickname                 = (!empty($request->dependents_nick_name)?$request->dependents_nick_name:'');

        $contactdependentsObj->date_of_birth            = (!empty($request->dependents_date_of_birth)?date('Y-m-d',strtotime($request->dependents_date_of_birth)):'');

        $contactdependentsObj->birth_place              = (!empty($request->dependents_birth_place)?$request->dependents_birth_place:'');

        $contactdependentsObj->blood_type               = (!empty($request->dependents_blood_type)?$request->dependents_blood_type:'');

        $contactdependentsObj->gender                   = (!empty($request->dependents_gender)?$request->dependents_gender:null);

        $contactdependentsObj->number_of_dependents     = (!empty($request->dependents_number_of_dependents)?$request->dependents_number_of_dependents:null);

        $contactdependentsObj->nationality              = (!empty($request->dependents_nationality)?implode(',', $request->dependents_nationality):'');

        $contactdependentsObj->languages                = (!empty($request->dependents_languages)?implode(',', $request->dependents_languages):'');

        $contactdependentsObj->profession               = (!empty($request->dependents_profession)?implode(',', $request->dependents_profession):'');

        $contactdependentsObj->importance               = (!empty($request->dependents_importance)?$request->dependents_importance:'');

        $contactdependentsObj->caution                  = (!empty($request->dependents_caution)?$request->dependents_caution:'');

        $contactdependentsObj->sensitivity              = (!empty($request->dependents_sensitivity)?$request->dependents_sensitivity:'');

        $contactdependentsObj->photo                    = $contactImageName;

        $contactdependentsObj->religion_id              = (!empty($request->dependents_religion)?$request->dependents_religion:null);

        $contactdependentsObj->internal_organizations   = (!empty($request->dependents_internal_organizations)?implode(',', $request->dependents_internal_organizations):'');

        $contactdependentsObj->administrative_group     = (!empty($request->dependents_administrative_group)?implode(',', $request->dependents_administrative_group):'');

        $contactdependentsObj->status                   = (!empty($request->dependents_status)?$request->dependents_status:null);

        $contactdependentsObj->notes                    = (!empty($request->dependents_notes)?$request->dependents_notes:'');

        $contactdependentsObj->keywords                 = (!empty($request->dependents_keywords)?$request->dependents_keywords:'');

        $contactdependentsObj->updated_by               = \Auth::user()->id;
        // echo "<pre>";print_r($contactdependentsObj);die;
        if($contactdependentsObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#contactNumber')->with('dependentSuccess', "Contact Dependent saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#dependents')->with('dependentError', "Something went wrong please try later.");
        }
    }

    /*
        add and edit contact number
    */
    public function contactNumberStore(Request $request) {

        if(isset($request->id) && $request->id > 0) {
            $contactNumberObj               = ContactNumber::where('id',$request->id)->first();
            $contactNumberObj->updated_at   = date('Y-m-d H:i:s');
        } else {
            $contactNumberObj = new ContactNumber;
            $contactNumberObj->created_by   = \Auth::user()->id;
            $contactNumberObj->created_at   = date('Y-m-d H:i:s');
        }

        $contactNumberObj->contact_id             = $request->contact_id;
        $contactNumberObj->contact_political_position_id = (!empty($request->political_position_id)?$request->political_position_id:null);

        $contactNumberObj->contact_dependents_id  = (!empty($request->contact_dependents_id)?$request->contact_dependents_id:null);

        $contactNumberObj->contact_spouse_id      = (!empty($request->contact_spouse_id)?$request->contact_spouse_id:null);

        $contactNumberObj->contact_referral_id    = (!empty($request->contact_referral_id)?$request->contact_referral_id:null);

        $contactNumberObj->contact_representative_id = (!empty($request->contact_representative_id)?$request->contact_representative_id:null);

        $contactNumberObj->country_code           = (!empty($request->contact_country_code)?$request->contact_country_code:null);

        $contactNumberObj->number                 = (!empty($request->contact_phone_number)?$request->contact_phone_number:'');

        $contactNumberObj->phone_type             = (!empty($request->contact_number_type)?$request->contact_number_type:null);

        $contactNumberObj->category               = (!empty($request->contact_category)?$request->contact_category:'');

        $contactNumberObj->fax_number             = (!empty($request->contact_fax)?$request->contact_fax:'');

        $contactNumberObj->importance             = (!empty($request->contact_importance)?$request->contact_importance:'');

        $contactNumberObj->caution                = (!empty($request->contact_caution)?$request->contact_caution:'');

        $contactNumberObj->sensitivity             = (!empty($request->contact_sensitivity)?$request->contact_sensitivity:'');

        $contactNumberObj->notes                  = (!empty($request->contact_notes)?$request->contact_notes:'');

        $contactNumberObj->keywords               = (!empty($request->contact_keywords)?$request->contact_keywords:'');

        $contactNumberObj->updated_by             = \Auth::user()->id;
        // echo "<pre>";print_r($contactNumberObj);die;
        if($contactNumberObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#communication')->with('contactNumberSuccess', "Contact Number saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#contactNumber')->with('contactNumberError', "Something went wrong please try later.");
        }
    }

    /*
        add and edit communication
    */
    public function communicationStore(Request $request) {

        $rules = [];
        if(isset($request->communication_email) && !empty($request->communication_email)) {
            $rules["communication_email"]       = 'email';
        }
        if(isset($request->communication_email_cc) && !empty($request->communication_email_cc)) {
            $rules["communication_email_cc"]    = 'email';
        }
        if(isset($request->communication_email_bcc) && !empty($request->communication_email_bcc)) {

            $rules["communication_email_bcc"]   = 'email';
        }


        $message = [
            'communication_email.email'      => 'Enter valid email address',
            'communication_email_cc.email'   => 'Enter valid email address',
            'communication_email_bcc.email'  => 'Enter valid email address'
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#communication')->withErrors($validator)->with('submit_communication',$request->submit_communication);
            }
        }

        if(isset($request->id) && $request->id > 0) {
            $contactCommunicationObj                = ContactCommunication::where('id',$request->id)->first();
            $contactCommunicationObj->updated_at    = date('Y-m-d H:i:s');
        } else {
            $contactCommunicationObj = new ContactCommunication;
            $contactCommunicationObj->created_by    = \Auth::user()->id;
            $contactCommunicationObj->created_at    = date('Y-m-d H:i:s');
        }

        $contactCommunicationObj->contact_id                    = $request->contact_id;
        $contactCommunicationObj->contact_political_position_id = (!empty($request->contact_political_position_id)?$request->contact_political_position_id:null);

        $contactCommunicationObj->contact_dependents_id         = (!empty($request->contact_dependents_id)?$request->contact_dependents_id:null);

        $contactCommunicationObj->contact_spouse_id             = (!empty($request->contact_spouse_id)?$request->contact_spouse_id:null);

        $contactCommunicationObj->contact_referral_id           = (!empty($request->contact_referral_id)?$request->contact_referral_id:null);

        $contactCommunicationObj->contact_representative_id     = (!empty($request->contact_representative_id)?$request->contact_representative_id:null);

        $contactCommunicationObj->email         = (!empty($request->communication_email)?$request->communication_email:'');

        $contactCommunicationObj->email_type    = (!empty($request->communication_email_type)?$request->communication_email_type:NULL);

        $contactCommunicationObj->category      = (!empty($request->communication_category)?$request->communication_category:'');

        $contactCommunicationObj->email_cc      = (!empty($request->communication_email_cc)?$request->communication_email_cc:'');

        $contactCommunicationObj->email_bcc     = (!empty($request->communication_email_bcc)?$request->communication_email_bcc:'');

        $contactCommunicationObj->skype         = (!empty($request->communication_skype)?$request->communication_skype:'');

        $contactCommunicationObj->facebook      = (!empty($request->communication_facebook)?$request->communication_facebook:'');

        $contactCommunicationObj->twitter       = (!empty($request->communication_twitter)?$request->communication_twitter:'');

        $contactCommunicationObj->instagram     = (!empty($request->communication_instagram)?$request->communication_instagram:'');

        $contactCommunicationObj->importance    = (!empty($request->communication_importance)?$request->communication_importance:'');

        $contactCommunicationObj->caution       = (!empty($request->communication_caution)?$request->communication_caution:'');

        $contactCommunicationObj->sensitivity    = (!empty($request->communication_sensitivity)?$request->communication_sensitivity:'');

        $contactCommunicationObj->notes         = (!empty($request->communication_notes)?$request->communication_notes:'');

        $contactCommunicationObj->keywords      = (!empty($request->communication_keywords)?$request->communication_keywords:'');

        $contactCommunicationObj->updated_by    = \Auth::user()->id;
        // echo "<pre>";print_r($contactCommunicationObj);die;
        if($contactCommunicationObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#education')->with('communicationSuccess', "Contact Communication saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#communication')->with('communicationError', "Something went wrong please try later.");
        }
    }

    /*
        add and edit education contact
    */
    public function educationStore(Request $request) {
        // echo "<pre>";print_r($request->all());die;
        $rules = [
            "education_from_date"   => "required",
            // "education_end_date"    => "after:education_from_date"
        ];
        $message = [
            'education_from_date.required'  => 'Select the from date',
            // 'education_end_date.after'      => 'To date must be a greater than from date'
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#education')->withErrors($validator)->with('submit_education',$request->submit_education);
            }
        }

        //device image upload
        if(count($request->file('education_certificate')) > 0) {

            $imageUpload        = Contact::imageUpload($request->file('education_certificate'));

            if($imageUpload == "") {
                $contactImageName   = (isset(
            $request->old_contact_image)?$request->old_contact_image:"");
            } else {
                $contactImageName   = $imageUpload;
            }

            // $contactImageFile    = $request->file('education_certificate');
            // $contactImagePath    = public_path('../uploads/contact/');
            // $contactImageName    = uniqid().'.'.$contactImageFile->getClientOriginalExtension();
            // $contactImageFile->move($contactImagePath,$contactImageName);

            // if(!empty($request->old_contact_education_image)) {
            //     File::delete('uploads/contact/' . $request->old_contact_education_image);
            // }
        } else {
            $contactImageName    = (isset(
        $request->old_contact_education_image)?$request->old_contact_education_image:"");
        }

        if(isset($request->id) && $request->id > 0) {
            $contactEducationObj                = ContactEducation::where('id',$request->id)->first();
            $contactEducationObj->updated_at    = date('Y-m-d H:i:s');
        } else {
            $contactEducationObj = new ContactEducation;
            $contactEducationObj->created_by    = \Auth::user()->id;
            $contactEducationObj->created_at    = date('Y-m-d H:i:s');
        }

        $contactEducationObj->contact_id                    = $request->contact_id;
        $contactEducationObj->contact_political_position_id = (!empty($request->contact_political_position_id)?$request->contact_political_position_id:null);
        $contactEducationObj->contact_dependents_id         = (!empty($request->contact_dependents_id)?$request->contact_dependents_id:null);
        $contactEducationObj->contact_spouse_id             = (!empty($request->contact_spouse_id)?$request->contact_spouse_id:null);
        $contactEducationObj->contact_referral_id           = (!empty($request->contact_referral_id)?$request->contact_referral_id:null);
        $contactEducationObj->contact_representative_id     = (!empty($request->contact_representative_id)?$request->contact_representative_id:null);

        $contactEducationObj->qualification = (!empty($request->education_qualification)?$request->education_qualification:'');

        $contactEducationObj->institute     = (!empty($request->education_institute)?$request->education_institute:'');

        $contactEducationObj->achivement    = (!empty($request->education_achivement)?$request->education_achivement:'');

        $contactEducationObj->sensitivity    = (!empty($request->education_sensitivity)?$request->education_sensitivity:'');

        $contactEducationObj->from_date     = date('Y-m-d',strtotime($request->education_from_date));

        $contactEducationObj->end_date      = (!empty($request->education_end_date)?date('Y-m-d',strtotime($request->education_end_date)):'');

        $contactEducationObj->certificate   = $contactImageName;

        $contactEducationObj->notes         = (!empty($request->education_notes)?$request->education_notes:'');

        $contactEducationObj->keywords      = (!empty($request->education_keywords)?$request->education_keywords:'');

        $contactEducationObj->updated_by    = \Auth::user()->id;

        if($contactEducationObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#greetingsLetter')->with('educationSuccess', "Contact Education saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#education')->with('educationError', "Something went wrong please try later.");
        }
    }

    /*
        add and edit greetings Letter of contact
    */
    public function greetingsLetterStore(Request $request) {
        // echo "<pre>";print_r($request->all());die;
        $rules = [
            "greeting_title"        => "required",
            "greeting_intro"        => "required",
            "greeting_ending"       => "required"
        ];
        $message = [
            'greeting_title.required'   => 'Select the title',
            'greeting_intro.required'   => 'Select the greeting intro',
            'greeting_ending.required'  => 'Select the greeting ending'
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login
        if ($validator->fails()) {
            if(Input::get('contact_id') != null && Input::get('contact_id') > 0){
                return Redirect::to('contact/edit/'.Input::get('contact_id').'#greetingsLetter')->withErrors($validator)->with('submit_greetings_letter',$request->submit_greetings_letter);
            }
        }

        if(isset($request->id) && $request->id > 0) {
            $contactGreetingObj                = ContactGreetingsLetter::where('id',$request->id)->first();
            $contactGreetingObj->updated_at    = date('Y-m-d H:i:s');
        } else {
            $contactGreetingObj = new ContactGreetingsLetter;
            $contactGreetingObj->created_by    = \Auth::user()->id;
            $contactGreetingObj->created_at    = date('Y-m-d H:i:s');
        }

        $contactGreetingObj->contact_id                    = $request->contact_id;
        $contactGreetingObj->contact_political_position_id = (!empty($request->contact_political_position_id)?$request->contact_political_position_id:null);
        $contactGreetingObj->contact_dependents_id         = (!empty($request->contact_dependents_id)?$request->contact_dependents_id:null);
        $contactGreetingObj->contact_spouse_id             = (!empty($request->contact_spouse_id)?$request->contact_spouse_id:null);
        $contactGreetingObj->contact_referral_id           = (!empty($request->contact_referral_id)?$request->contact_referral_id:null);
        $contactGreetingObj->contact_representative_id     = (!empty($request->contact_representative_id)?$request->contact_representative_id:null);

        $contactGreetingObj->title = (!empty($request->greeting_title)?$request->greeting_title:null);

        $contactGreetingObj->grettings_intro    = (!empty($request->greeting_intro)?$request->greeting_intro:'');

        $contactGreetingObj->grettings_ending   = (!empty($request->greeting_ending)?$request->greeting_ending:'');

        $contactGreetingObj->importance        = (!empty($request->greeting_importance)?$request->greeting_importance:'');

        $contactGreetingObj->caution           = (!empty($request->greeting_caution)?$request->greeting_caution:'');

        $contactGreetingObj->sensitivity        = (!empty($request->greeting_sensitivity)?$request->greeting_sensitivity:'');

        $contactGreetingObj->notes             = (!empty($request->greeting_notes)?$request->greeting_notes:'');

        $contactGreetingObj->keywords          = (!empty($request->greeting_keywords)?$request->greeting_keywords:'');

        $contactGreetingObj->updated_by        = \Auth::user()->id;
        if($contactGreetingObj->save()) {
            return redirect('contact/edit/'.$request->contact_id.'#greetingsLetter')->with('greetingSuccess', "Contact Greetings Letter saved successfully.");
        } else {
            return redirect('contact/edit/'.$request->contact_id.'#greetingsLetter')->with('greetingError', "Something went wrong please try later.");
        }
    }

    /*
        add Address repeated fields using ajax
    */
    public function getMultipleAddress(Request $request) {
        // echo $request->address_id;
        $address_id = $request->address_id+1;
        $region = Region::pluck('region','id');
        // echo "<pre>";print_r($region);die;
        $province                   = Province::where('country',isset($addressContact->country_id)?$addressContact->country_id:0)->pluck('province','id');
        $district                   = District::where('country',isset($addressContact->country_id)?$addressContact->country_id:0)->pluck('district','id');
        $city                       = City::where('country',isset($addressContact->country_id)?$addressContact->country_id:0)->pluck('city','id');

        $nations = Country::get();
        $addressTypeArr             = General::addressTypeArr();
        $importanceArr              = General::importanceArr();
        $causionArr                 = General::causionArr();
        $sensitivityArr             = General::sensitivityArr();
        return view('Admin.contact.addressMultiple',['address_id'=>$address_id,'region'=>$region,"nations" => $nations,"province" => $province, "district" => $district, "city" => $city, "addressTypeArr" => $addressTypeArr,"importanceArr" => $importanceArr, "causionArr" => $causionArr, "sensitivityArr" => $sensitivityArr]);
    }

    public static function rearrangeSubmit( $arr ){
        $file_ary = array();
        $file_count = count($arr['id']);
        $file_keys = array_keys($arr);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $arr[$key][$i];
            }
        }

        return $file_ary;
    }
}
