<?php

namespace App\Http\Controllers\Admin;
/* Datatables support */
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\LanguageKeyword;
use App\Models\LanguageMulti;
use App\Models\Language;
use App\Models\CategoryAppointment;
use Html;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
/* To download data in excel */
use Excel;
use Session;

class CategoryAppointmentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      Session::forget('AppQuestionPage');
      Session::forget('AppUserPage');
        return view('Admin.category_appointment.list');
    }

    // return add view language keyword
    public function add(Request $request) {
        $languages = Language::all();
        return view('Admin.category_appointment.add',["languages"=>$languages]);
    }

    //return edit view language keyword
    public function edit(Request $request,$id = 0) {

       if ($id > 0) {
           $languages = Language::all();
            $perCategory = CategoryAppointment::find($id);
            if (!empty($perCategory) > 0) {
                return View::make('Admin.category_appointment.edit')->with('category', $perCategory)->with('languages',$languages);
            }
            return View::make('Admin.error.404');
        }
    }

    //add and edit language keyword
    public function store(Request $request) {

        $this->validate($request, [
            "name"             => "required|unique:category_appointment,category,".$request->id,'id',
        ],
        [
            "name.required"    => "Category Name is required.",
            "name.unique"      => "Category Name is already exist",
        ]);

        // check validation
        if (!isset($this->validate)) {

            if(isset($request->id) && $request->id > 0) {
                $category            = CategoryAppointment::find($request->id);
                $category->updated_at    = date('Y-m-d H:i:s');
            } else {
                $category            = new CategoryAppointment();
            }
            $category->category         = $request->name;
            $category->language_id      = $request->language_id;
            $category->status           = $request->status;
            $category->charges          = $request->charges;
            $category->created_at       = date('Y-m-d H:i:s');
            $category->save();

           return redirect('panel/appointment/category');

        }


    }
public function updatemultirecode(Request $request){
      
        if($request->action != '' && count($request->checkaction) > 0){

            if($request->action == 'inactive'){

             if(count($request->checkaction) > 0){
                foreach ($request->checkaction as $key => $inactive) {
                     $Category            = CategoryAppointment::find($inactive);
                     $Category->status    = 0;
                     $Category->save();
                }
             }
             return redirect('/panel/appointment/category')->with('success', "category Inactive successfully.");

        }else if($request->action == 'active'){

            if(count($request->checkaction) > 0){
                foreach ($request->checkaction as $key => $active) {
                     $Category            = CategoryAppointment::find($active);
                     $Category->status    = 1;
                     $Category->save();
                }
             }
            return redirect('/panel/appointment/category')->with('success', "category Active successfully.");
        }elseif ($request->action == 'delete') {

             if(count($request->checkaction) > 0){
                foreach ($request->checkaction as $key => $delete) {
                     $Category  = CategoryAppointment::find($delete);
                     $Category->delete();
                }
             }
            return redirect('/panel/appointment/category')->with('success', "category Detete successfully.");
        }
    }else{
        return redirect('/panel/appointment/category')->with('error', 'Something went wrong please try later');
    }
        
    }
    //delete language keyword
    public function delete(Request $request, $id = 0) {

        $Category    = CategoryAppointment::find($id);
        if (count($Category) > 0) {

            if($Category->delete()) {
                return redirect('/panel/appointment/category')->with('success', "Category deleted successfully.");
            } else {
                return redirect('/panel/appointment/category')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/appointment/category')->with('error', trans('Record not found'));
    }
    public function arrayData(Datatables $datatables) {
         $builder = CategoryAppointment::query()->select('id', 'category', 'status','charges');

        return $datatables->eloquent($builder)
        ->addColumn('checkbox', function ($category) {
                                return "<input class='' type='checkbox' id='checkaction".$category->id."' name='checkaction[]' value=".$category->id." onclick='onclickcheck(".$category->id.");'>";
                    })
                    ->editColumn('id', function ($category) {
                                return $category->id;
                            })
                         ->editColumn('category', function ($category) {
                                return "<a href=".url('panel/appointment/category/edit/' . $category->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$category->category."
                                </a>";
                            })
                         
                            ->editColumn('charges', function ($category) {
                                return "<a href=".url('panel/appointment/category/edit/' . $category->id)." class=\"btn btn-link btn-sm\" title='Edit'>
                                    ".$category->charges."
                                </a>";
                            })
                            ->editColumn('status',function($category) {
                                 if($category->status == 1){
                                   return '<span class="badge bg-green">Active</span>';
                                } else if($category->status == 0){
                                   return  '<span class="badge bg-red">Inactive</span>';
                                } else {
                                   return '<span class="badge bg-yellow">Delete</span>'; 
                                }
                            })
                          ->addColumn('action', function($category) {
                            return "<a href=".url('panel/appointment/category/edit/' . $category->id)." class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>
                                                <a href=".url('panel/appointment/category/delete/' . $category->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }\" title='Delete'><i class=\"fa fa-trash\"></i></a>";
                          })
                          ->rawColumns(['checkbox','id','category','status','action','charges'])
                          ->toJson();
    }
    
}
