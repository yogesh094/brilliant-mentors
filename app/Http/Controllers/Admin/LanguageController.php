<?php

namespace App\Http\Controllers\Admin;

/* Datatables support */

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\LanguageKeyword;
use App\Models\LanguageMulti;
use App\Models\Language;
use Html;
use Input;
use Validator;
use Redirect;
use View;
use Auth;
use Session;
/* To download data in excel */
use Excel;

class LanguageController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        
    }

    public function index() {
        //echo Auth::user()->user_role;
        Session::forget('AppQuestionPage');
        Session::forget('AppUserPage');
        return view('Admin.language.list');
    }

    // return add view language keyword
    public function add(Request $request) {

        // $items = Items::where('active', true)->orderBy('name')->pluck('name', 'id');

        return view('Admin.language.addLang');
    }

    //return edit view language keyword
    public function edit(Request $request, $id = 0) {

        if ($id > 0) {

            $perLanguage = Language::find($id);
            if (!empty($perLanguage) > 0) {
                return View::make('Admin.language.editLang')->with('language', $perLanguage);
            }
            return View::make('Admin.error.404');
        }
    }

    //add and edit language keyword
    public function store(Request $request) {

        $this->validate($request, [
            "name" => "required|unique:language,name," . $request->id, 'id',
            "code" => "required|unique:language,code," . $request->id, 'id'
                ], [
            "name.required" => "Name is required.",
            "name.unique" => "Name is already exist",
            "code.required" => "Code is required.",
            "code.unique" => "Code is already exist",
        ]);

        // check validation
        if (!isset($this->validate)) {

            if (isset($request->id) && $request->id > 0) {
                $language = Language::find($request->id);
                $language->updated_at = date('Y-m-d H:i:s');
            } else {
                $language = new Language();
            }
            $language->name = $request->name;
            $language->code = $request->code;
            $language->status = $request->status;
            $language->created_at = date('Y-m-d H:i:s');
            $language->save();

            return redirect('panel/language');
        }
    }

    //delete language keyword
    public function delete(Request $request, $id = 0) {

        $Language = Language::find($id);
        if (count($Language) > 0) {

            if ($Language->delete()) {
                return redirect('/panel/language')->with('success', "Language deleted successfully.");
            } else {
                return redirect('/panel/language')->with('error', 'Something went wrong please try later');
            }
        }
        return redirect('/panel/language')->with('error', trans('Record not found'));
    }

    public function arrayData(Datatables $datatables) {
        $builder = Language::query()->select('id', 'name', 'code', 'status');

        return $datatables->eloquent($builder)
                        ->editColumn('name', function ($language) {
                            return "<a href=" . url('panel/language/edit/' . $language->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $language->name . "
                                </a>";
                        })
                        ->editColumn('code', function ($language) {
                            return "<a href=" . url('panel/language/edit/' . $language->id) . " class=\"btn btn-link btn-sm\" title='Edit'>
                                    " . $language->code . "
                                </a>";
                        })
                        ->editColumn('status', function($language) {
                            if ($language->status == 1) {
                                return '<span class="badge bg-green">Active</span>';
                            } else if ($language->status == 0) {
                                return '<span class="badge bg-red">Inactive</span>';
                            } else {
                                return '<span class="badge bg-yellow">Delete</span>';
                            }
                        })
                        // <a href=".url('admin/language/delete/' . $language->id)." class=\"btn btn-danger btn-sm\" onclick=\"if (confirm('Are You Sure? Do You Want To Delete?')) {
                        //                                         return true;
                        //                                     } else {
                        //                                         return false;
                        //                                     }\" title='Delete'><i class=\"fa fa-trash\"></i></a>
                        ->addColumn('action', function($language) {
                            return "<a href=" . url('panel/language/edit/' . $language->id) . " class=\"btn btn-success btn-sm\" title='Edit'><i class=\"fa fa-pencil\"></i></a>";
                        })
                        ->rawColumns(['name', 'code', 'status', 'action'])
                        ->toJson();
    }

}
