<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUser;
use App\Models\Appointment;
use App\Models\Category;
use App\Models\CategoryAppointment;
use App\Models\Topic;
use Illuminate\Support\Facades\Hash;
use App\Models\AllStoreAppointment;
use App\Models\PaymentOrder;
use App\Models\Notification;
use App\Library\webGeneral;
use App\Models\Question;
use App\Models\Setting;
use App\Models\EmailTemplate;
use Carbon\Carbon;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use Cache;
use DB;

class AppointmentController extends Controller
{
    protected $customerId;
    public function __construct() {
      header("Content-Type: application/json");
      //$headers = getallheaders();
      //$headers = app('request');
      $headers['apiToken'] = app('request')->headers->get('apitoken');
      $apiToken = isset($headers['apiToken'])?$headers['apiToken']:'';
       if($apiToken != ''){
            $customer = AppUser::where('token',$apiToken)->get()->toArray();
            if(count($customer) > 0){
                $this->customerId = $customer[0]['id'];
            }else{
                echo json_encode([
                        'status'    => false,
                        'error'     => 201,
                        'message'   => 'Invalid token',
                    ]);
                exit;
            }
       }else{
            echo json_encode([
                        'status'    => false,
                        'error'     => 401,
                        'message'   => 'Token is required',
                    ]);
            exit;
       }
    }

    public function bookAppointment(Request $request){
        $rules = [
            'language_id'   => 'required',
            'booking_date'  => 'required',
            'booking_time'  => 'required',
            //'title'         => 'required',
            'description'   => 'required',
            'category_id'   => 'required',
            //'topic_id'      => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"  => "Please provide language Id",
            "booking_date.required" => "Please provide booking date",
            "booking_time.required" => "Please provide booking time",
            //"title.required"        => "Please provide title",
            "description.required"  => "Please provide description",
            "category_id.required"  => "Please provide category Id",
            //"topic_id.required"     => "Please provide topic Id",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make($_POST, $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }

        // Get Topic charge 
        // $getTopicCharges = Topic::where('id',$request->get('topic_id'))->pluck('charges')->first();
    $getCategoryCharges = CategoryAppointment::where('id',$request->get('category_id'))->pluck('charges')->first();


    $booking_time = ($request->get('booking_time') !=null?$request->get('booking_time'):'');
        $time = explode('-',$booking_time);

        $start_time = str_replace(' pm','',$time[0]);
        $start_time = str_replace(' am','',$time[0]);

        $end_time = str_replace(' pm','',$time[1]);
        $end_time = str_replace(' am','',$time[1]);

    $User = User::whereNotIn('user_role', [1])->where('status',1)->get();

    $checkAppointmentUserObj = DB::table('appointment')
      ->select('appointment.*')
      ->where('appointment.appuser_id',$this->customerId)
      ->where('appointment.booking_date', $request->get('booking_date'))
      ->where('appointment.start_time', $start_time)
      ->where('appointment.end_time', $end_time)->first();
      if(!empty($checkAppointmentUserObj)){
         echo json_encode([
                        'status'    => false,
                        'error'     => 404,
                        'message'   => Cache::get('diffent_date_user'),
                    ]);
        exit;
      }

 $AppointmentObj = DB::table('appointment')
                      ->select('appointment.*')
    ->join('payment_order', 'payment_order.appointment_id', '=', 'appointment.id')
    ->where('payment_order.status', 'succeeded')
    ->where('appointment.booking_date', $request->get('booking_date'))
    ->where('appointment.start_time', $start_time)
    ->where('appointment.end_time', $end_time)->get();
 if(count($AppointmentObj) > 0 && count($User) <= count($AppointmentObj)){
      echo json_encode([
                        'status'    => false,
                        'error'     => 404,
                        'message'   => Cache::get('total_appointment_book'),
                    ]);
        exit;
 }
        // Add Appoinment
        //$appoinment = new Appointment;
        if(isset($request->appointment_id) && $request->appointment_id > 0){
            $appoinment = Appointment::find($request->appointment_id);
        }else{
            $appoinment = new AllStoreAppointment();
            $appoinment->room = ($request->get('room') !=null?$request->get('room'):'');
        }
        
        $appoinment->language_id = ($request->get('language_id') !=null?$request->get('language_id'):'');
        $appoinment->appuser_id = $this->customerId;
        $appoinment->category_id  = ($request->get('category_id') !=null?$request->get('category_id'):'');
        $appoinment->booking_date = ($request->get('booking_date') !=null?$request->get('booking_date'):'');
        $appoinment->start_time = $start_time;
        $appoinment->end_time = $end_time;
        
        $appoinment->description = ($request->get('description') !=null?$request->get('description'):'');
        
        $appoinment->created_at = date("Y-m-d H:i:s");
        if($appoinment->save()){

       if(isset($request->appointment_id) && $request->appointment_id > 0){
          $getcharges = CategoryAppointment::where('id',$request->category_id)->pluck('charges')->first();
            $paymentOrderObj = PaymentOrder::where('appointment_id',$appoinment->id)->first();
            $PaymentOrder = PaymentOrder::find($paymentOrderObj->id);
            $PaymentOrder->type = 1;
            $PaymentOrder->user_id = $this->customerId;
            $PaymentOrder->appointment_id = $appoinment->id;  
            $PaymentOrder->amount = $getcharges;
            $PaymentOrder->status = 'succeeded';
            $PaymentOrder->created_at = date('Y-m-d H:i:s');
            $PaymentOrder->save();

            $Appointment = Appointment::find($appoinment->id);
            $Appointment->cancel_type = 1;
            $Appointment->payment_status = 1;
            if($appoinment->expert_id != null){
                $Appointment->status = 1;
            }else{
                $Appointment->status = 0;
            }
      
            $Appointment->save();
            $userObj = User::where('id',$Appointment->expert_id)->where('status',1)->first();
            $appUserObj = AppUser::where('id',$Appointment->appuser_id)->first();
            $setting  = Setting::where('slug','email')->first();
            $emailTemplateObj =  EmailTemplate::where('id',16)->where('language_id',1)->first();
              $subject  = $emailTemplateObj->subject;
              $body     = $emailTemplateObj->body;
              if(!empty($userObj)){
                $tomail   = $userObj->email;
                $body     =  str_replace('[ExpertName]',$userObj->first_name.' '.$userObj->last_name,$body);
              }else{
                $tomail   = Cache::get('email');
                $body     =  str_replace('[ExpertName]','admin',$body);
              }
              $body     =  str_replace('[UserName]',$appUserObj->username,$body);
              
          $emailData = array("name"=>$appUserObj->username,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'body' => $body,'FRONT_URL' => url(''));
        
          $res = Mail::send('emails.appointment-cancel',['data'=>$emailData],function($message) use ($emailData){
            $message->from($emailData['from'],$emailData['emailLabel']);
            $message->to($emailData['to'],$emailData['name']);
            $message->subject($emailData['subject']);
        });

        $emailTemplateObj =  EmailTemplate::where('id',21)->where('language_id',1)->first();

        $subject  = $emailTemplateObj->subject;
        $tomail   = $appUserObj->email;
        $body     = $emailTemplateObj->body;
        $body     =  str_replace('[UserName]',$appUserObj->username,$body);

        $emailData = array("name"=>$appUserObj->username,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'body' => $body,'FRONT_URL' => url(''));
        
        $res = Mail::send('emails.appointment-cancel',['data'=>$emailData],function($message) use ($emailData){
            $message->from($emailData['from'],$emailData['emailLabel']);
            $message->to($emailData['to'],$emailData['name']);
            $message->subject($emailData['subject']);
        });
            $data['reschedule'] = 'reschedule';
            $messages = Cache::get('appointment_rescheduled');
            
          }else{
            $messages = Cache::get('appointment_scheduled');
          }
          // $data['id'] = $appoinment->id;
          // $data['booking_date'] = ($request->get('booking_date') !=null?$request->get('booking_date'):'');
          // $data['booking_time'] = ($request->get('booking_time') !=null?$request->get('booking_time'):'');
          // $data['description'] = ($request->get('description') !=null?$request->get('description'):'');
          // $data['charges'] = $getCategoryCharges;
          // $data['currency'] = Cache::get('currency');
          

          $getCategoryName = CategoryAppointment::where('id',$appoinment->category_id)->pluck('category')->first();
          $getcharges = CategoryAppointment::where('id',$appoinment->category_id)->pluck('charges')->first();

          $data['id'] = $appoinment->id;
          $data['booking_date'] = date('Y-m-d', strtotime($appoinment->booking_date));
          $data['start_time'] = $appoinment->start_time;
          $data['end_time'] = $appoinment->end_time;
          $data['category'] = $getCategoryName;
          $data['charges'] = $getcharges;
          $data['description'] = $appoinment->description;
          $data['currency'] = Cache::get('currency');

            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => $messages,
                        'data'      => $data
                    ]);
            exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'     => 401,
                        'message'   => Cache::get('went_wrong'),
                    ]);
            exit;
        }
    
    }
    
    public function getAppointment(Request $request){
         $rules = [
            'language_id'   => 'required',
            'appuser_id'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Language Id is required",
            "appuser_id.required"       => "user id is required",
        ];

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        //  $AppointmentObj = Appointment::where('appuser_id',$request->get('appuser_id'))->where('language_id',$request->get('language_id'))->get()->toArray();
         
        $AppointmentObj = DB::table('appointment')
          ->select('appointment.*')
          ->join('payment_order', 'payment_order.appointment_id', '=', 'appointment.id')
          //->where('payment_order.status', 'succeeded')
          ->where('appointment.appuser_id',$request->get('appuser_id'))
          ->where('appointment.language_id',$request->get('language_id'))
          ->orderBy('appointment.id','DESC')
          ->get();
          
         if(count($AppointmentObj) > 0){
            $dataArr = array();
            foreach ($AppointmentObj as $key => $Appointment) {
               $getExpertName = User::where('id',$Appointment->expert_id)->pluck('username')->where('status',1)->first();
               $getExpertName1 = User::where('id',$Appointment->expert_id)->first();
               //$getTopicName = Topic::where('id',$Appointment['topic_id'])->pluck('topic')->first();
                $getCategoryName = CategoryAppointment::where('id',$Appointment->category_id)->pluck('category')->first();
               $getCategoryCharges = CategoryAppointment::where('id',$Appointment->category_id)->pluck('charges')->first();

              
        $check48hours = webGeneral::check48hours(date("Y-m-d H:i:s"),$Appointment->booking_date.' '.$Appointment->start_time);

        $paymentOrder = PaymentOrder::where('appointment_id',$Appointment->id)->first();

          $refundBtn = '';
        if($paymentOrder->type == 2){
            $refundBtn = 4; // diseble refund btn
        }
            if($Appointment->cancel_type == 1){
                $cancel_type = 2; //appoinment already book on Reschedule
            }elseif($check48hours <= 48){
              $cancel_type = 0; //refund

            }elseif($Appointment->status == 4){

              $cancel_type = 4; //enable refund btn

            }else{
              $cancel_type= 1; //Reschedule
            }

               $dataArr[$key]['id'] = isset($Appointment->id)?$Appointment->id:'';
               $dataArr[$key]['expert_name'] = (!empty($getExpertName1)?$getExpertName1->username:"");
               $dataArr[$key]['category_name'] = isset($getCategoryName)?$getCategoryName:'';
               $dataArr[$key]['booking_date'] = isset($Appointment->booking_date)?$Appointment->booking_date:'';
               $dataArr[$key]['start_time'] = isset($Appointment->start_time)?$Appointment->start_time:'';
               $dataArr[$key]['end_time'] = isset($Appointment->end_time)?$Appointment->end_time:'';
               $dataArr[$key]['payment_status'] = $Appointment->payment_status;
               $dataArr[$key]['category_id'] = $Appointment->category_id;
               $dataArr[$key]['description'] = $Appointment->description;

               if($refundBtn == 4){
                $dataArr[$key]['status'] = 4;
               }else if($Appointment->status != 4){
                $dataArr[$key]['status'] = $Appointment->status;
               }else{
                $dataArr[$key]['status'] = "";
               }
               
                //1 is Reschedule appoinment
               $dataArr[$key]['rescheduleBtnEnable'] = $cancel_type;

               $dataArr[$key]['charges'] = $getCategoryCharges;
               $dataArr[$key]['currency'] = Cache::get('currency');

            }
            echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Get Appoinment List',
                          'data'      => $dataArr
                      ]);
            exit;

         }else{
            echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_appointment'),
                      ]);
            exit;
         }

    }

    public function getNotification(Request $request){
        $rules = [
            'language_id'   => 'required',
            // 'limit'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Language Id is required",
            // "limit.required"       => "Limit is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        // $notificationObj  = DB::table('notification')
        //                     // ->join('appuser', 'notification.appuser_id', '=', 'appuser.id')
        //                     ->join('user', 'notification.expert_id', '=', 'user.id')
        //                     ->join('question', 'notification.question_id', '=', 'question.id')
        //                     ->select('notification.*','user.username as expertName','question.answer','question.question')
        //                     ->where('notification.language_id',$request->get('language_id'))
        //                     ->where('notification.appuser_id',$this->customerId)
        //                     ->orderBy('notification.id','desc')
        //                     ->groupBy('notification.question_id')
        //                     ->get();

        // if(count($notificationObj) > 0){
        //     $data = array();
        //     foreach ($notificationObj as $key => $resultData) {
        //         $data[$key]['notification_id']  = $resultData->id;
        //         $data[$key]['language_id']      = $resultData->language_id;
        //         $data[$key]['question_id']      = $resultData->question_id;
        //         $data[$key]['question']         = $resultData->question;
        //         $data[$key]['expertName']       = $resultData->expertName;
        //         $data[$key]['answer']           = $resultData->answer;
        //         $data[$key]['message']          = $resultData->message;
        //         $data[$key]['is_read']          = $resultData->is_read;
        //         $data[$key]['datetime']         = $resultData->created_at;
        //     }
        $limit = $request->get('limit') != null?$request->get('limit'):10;
        $page = $request->get('page') != null?$request->get('page') - 1:1;
        
        $start = $page * $limit;
        // $limit = $request->get('limit') != null?$request->get('limit'):5;
        // $start = $request->get('page') != null?$request->get('page'):0;
        $Notification = Notification::where('appuser_id',$this->customerId)->where('language_id',$request->get('language_id'))->orderBy('updated_at', 'DESC')->take($limit)->skip($start)->get();
        $data = [];
          if(count($Notification) > 0){
            foreach ($Notification as $key => $notifications) {
              $data[$key]['id']               = $notifications->id;
              $data[$key]['language_id']      = $notifications->language_id;
              $data[$key]['type_id']          = $notifications->type_id;
              $data[$key]['question_id']      = $notifications->question_id;
              $data[$key]['message']          = $notifications->message;
              $data[$key]['is_read']          = $notifications->is_read;
              $data[$key]['datetime']         = date('Y-m-d H:i:s', strtotime($notifications->updated_at));
              $description = "";
              $answer = "";
              if($notifications->type_id == 1){
                $questionObj = Question::where('id',$notifications->question_id)->first();
                if(!empty($questionObj)){
                $description = str_limit($questionObj->question,100);
                $answer = $questionObj->answer;

              }
            }else if($notifications->type_id == 0){
              $AppointmentObj = Appointment::where('id',$notifications->question_id)->first();
              if(!empty($AppointmentObj)){
                 $description = str_limit($AppointmentObj->description,200);
              }
          }
          $data[$key]['description'] = $description;
          $data[$key]['answer'] = $answer;
              
            }
            echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Notifications Listing',
                          'data'      => $data
                      ]);
            exit;
          }
            

        else{
          echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
          exit;
        }

    }


    public function readNotification(Request $request) {

        $rules = [
            'notification_id' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "notification_id.required" => 'Please provide your notification id',
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first(),
            ]);
            exit;
        }

        $notification = Notification::find($request->get('notification_id'));
        if (count($notification) > 0) {
            $notification->is_read = '1';
            $notification->save();
            echo json_encode([
                'status' => true,
                'success' => 200,
                'message' => 'Notification Readed',
            ]);
            exit;
        }
    }

 public function cancelAppointment(Request $request){
    $rules = [
            'appointment_id' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "appointment_id.required" => 'Please provide your appointment id',
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first(),
            ]);
            exit;
        }

        $Appointment = Appointment::where('id',$request->appointment_id)->first();
        $appointmentobj = Appointment::find($Appointment->id);
        $appointmentobj->status = 3;
        $appointmentobj->save();
        
        if($appointmentobj->expert_id != null && $appointmentobj->expert_id > 0){
          $userObj = User::where('id',$appointmentobj->expert_id)->where('status',1)->first();
          $appUserObj = AppUser::where('id',$appointmentobj->appuser_id)->first();
          $setting  = Setting::where('slug','email')->first();
          $getCategoryName = CategoryAppointment::where('id',$appointmentobj->category_id)->pluck('category')->first();
          $emailTemplateObj =  EmailTemplate::where('id',15)->where('language_id',1)->first();
          $booking_time = Carbon::parse($appointmentobj->booking_date)->format('F d,Y').'('.Carbon::parse($appointmentobj->start_time)->format('g:i A').'-'.Carbon::parse($appointmentobj->end_time)->format('g:i A').')';

          $subject  = $emailTemplateObj->subject;
          $tomail   = $userObj->email;
          $body     = $emailTemplateObj->body;

          $body     =  str_replace('[UserName]',$appUserObj->username,$body);
          $body     =  str_replace('[ExpertName]',$userObj->first_name.' '.$userObj->last_name,$body);
          $body     =  str_replace('[date-time]',$booking_time,$body);
          $body     =  str_replace('[Category]',$getCategoryName,$body);
          $body     =  str_replace('[Description]',$appointmentobj->description,$body);

        $data = array("name"=>$appUserObj->username,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'body' => $body,'FRONT_URL' => url(''));
        
        $res = Mail::send('emails.appointment-cancel',['data'=>$data],function($message) use ($data){
            $message->from($data['from'],$data['emailLabel']);
            $message->to($data['to'],$data['name']);
            $message->subject($data['subject']);
        });

        }
        //$data['cancel_type'] = $cancel_type;
        $data['appointment_id'] = $Appointment->id;

        echo json_encode([
                'status' => true,
                'success' => 200,
                'message' => 'Appointment Cancel',
                'data' => $data
            ]);
            exit;
    //    }else{
    //     echo json_encode([
    //             'status' => false,
    //             'error' => 401,
    //             'message' => 'Appoinment not found'
    //         ]);
    //         exit;
    // }
        
 }

}
