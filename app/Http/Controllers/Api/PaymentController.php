<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\AppUser;
use App\Models\Language;
use App\Models\Category;
use App\Models\CategoryAppointment;
use App\Models\Topic;
use App\Models\Slider;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\LanguageKeyword;
use App\Models\Setting;
use App\Models\PaymentOrder;
use App\Models\Appointment;
use App\Models\User;
use App\Models\AllStoreAppointment;
use App\Models\EmailTemplate;
use Illuminate\Support\Facades\Hash;
use Stripe\Error\Card;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Carbon\Carbon;
use Mail;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use DB;
use Cache;

class PaymentController extends Controller {

    protected $customerId;

    public function __construct() {
        header("Content-Type: application/json");
        //$headers = getallheaders();
        $headers['apiToken'] = app('request')->headers->get('apitoken');
        $apiToken = isset($headers['apiToken']) ? $headers['apiToken'] : '';
        if ($apiToken != '') {
            $customer = AppUser::where('token', $apiToken)->get()->toArray();
            if (count($customer) > 0) {
                $this->customerId = $customer[0]['id'];
            } else {
                echo json_encode([
                    'status' => false,
                    'error' => 201,
                    'message' => 'Invalid token',
                ]);
                exit;
            }
        } else {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => 'Token is required',
            ]);
            exit;
        }
    }

    public function paymetStore(Request $request) {
        // echo base_path('stripe/init.php');
        // exit;
        // require_once(base_path('stripe/vendor/autoload.php'));
        $rules = [
            'clientToken' => 'required',
            'amount' => 'required',
            'user_id' => 'required',
                //'appointment_id' => 'required'
        ];
        $requestData = Input::all();
        $message = [
            "clientToken.required" => "Please provide token",
            "amount.required" => "Please provide amount",
            "user_id.required" => "Please provide user id",
                //"appointment_id.required" => "Please provide appointment id",
        ];
        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first(),
            ]);
            exit;
        }
        require_once(base_path('stripe/init.php'));
        \Stripe\Stripe::setApiKey('sk_test_1a2ZxW34hF5TeVPbCOHLl4dc');

        try {
            // $clientToken =  \Stripe\Token::create(array(
            //       "card" => array(
            //         "number" =>$request->cardnumber,
            //         "exp_month" => $request->cardexpirymonth,
            //         "exp_year" => $request->cardexpiryyear,
            //         "cvc" => $request->cardcvc
            //       )
            //     ));
            $clientToken = $request->clientToken;

            if (isset($clientToken)) {


                $customer = \Stripe\Customer::create(array(
                            "email" => "paying.user@example.com",
                            "source" => $clientToken,
                ));

                if (isset($customer->id)) {
                    $charge = \Stripe\Charge::create(array(
                                "amount" => $request->amount * 100,
                                "currency" => "usd",
                                "customer" => $customer->id,
                                "description" => "Test payment."
                    ));

                    if (isset($charge)) {
                        if (!isset($request->booking_id)) {
                            $appointmentObj = AllStoreAppointment::where('id', $request->appointment_id)->first();

                            $User = User::whereNotIn('user_role', [1])->where('status', 1)->get();

                            $checkAppointmentUserObj = DB::table('appointment')
                                            ->select('appointment.*')
                                            ->where('appointment.appuser_id', $this->customerId)
                                            ->where('appointment.booking_date', $appointmentObj->booking_date)
                                            ->where('appointment.start_time', $appointmentObj->start_time)
                                            ->where('appointment.end_time', $appointmentObj->end_time)->first();
                            if (!empty($checkAppointmentUserObj)) {
                                echo json_encode([
                                    'status' => false,
                                    'error' => 404,
                                    'message' => Cache::get('diffent_date_user'),
                                ]);
                                exit;
                            }

                            $AppointmentObj = DB::table('appointment')
                                            ->select('appointment.*')
                                            ->join('payment_order', 'payment_order.appointment_id', '=', 'appointment.id')
                                            ->where('payment_order.status', 'succeeded')
                                            ->where('appointment.booking_date', $appointmentObj->booking_date)
                                            ->where('appointment.start_time', $appointmentObj->start_time)
                                            ->where('appointment.end_time', $appointmentObj->end_time)->get();

                            if (count($AppointmentObj) > 0 && count($User) <= count($AppointmentObj)) {
                                echo json_encode([
                                    'status' => false,
                                    'error' => 404,
                                    'message' => Cache::get('total_appointment_book'),
                                ]);
                                exit;
                            }

                            $appoinment = new Appointment();
                            $appoinment->language_id = $appointmentObj->language_id;
                            $appoinment->appuser_id = $this->customerId;
                            $appoinment->room = $appointmentObj->room;
                            $appoinment->category_id = $appointmentObj->category_id;
                            $appoinment->booking_date = $appointmentObj->booking_date;
                            $appoinment->start_time = $appointmentObj->start_time;
                            $appoinment->end_time = $appointmentObj->end_time;
                            $appoinment->description = $appointmentObj->description;
                            $appoinment->created_at = date("Y-m-d H:i:s");
                            $appoinment->save();
                        }


                        $data = [];
                        $data['charge_id'] = $charge->id;
                        $data['response'] = $charge;
                        //$json = json_decode($charge,true);
                        if (isset($request->booking_id) && $request->booking_id > 0) {
                            $paymentOrderObj = PaymentOrder::where('appointment_id', $request->booking_id)->first();
                            $appoinmentId = $request->booking_id;
                            $PaymentOrder = PaymentOrder::find($paymentOrderObj->id);
                        } else {
                            $appoinmentId = $appoinment->id;
                            $PaymentOrder = new PaymentOrder;
                        }

                        $PaymentOrder->charge_id = $data['charge_id'];
                        $PaymentOrder->user_id = $request->user_id;
                        $PaymentOrder->appointment_id = $appoinmentId;

                        $PaymentOrder->response = $data['response'];
                        $PaymentOrder->amount = $charge->amount;
                        //if(isset($charge->response)){
                        $PaymentOrder->balance_transaction = $charge->balance_transaction;
                        $PaymentOrder->currency = $charge->currency;
                        $PaymentOrder->paid = $charge->paid;
                        $PaymentOrder->brand = $charge->source->brand;
                        $PaymentOrder->country = $charge->source->country;
                        $PaymentOrder->funding = $charge->source->funding;
                        $PaymentOrder->customer = $charge->customer;
                        //}
                        $PaymentOrder->status = $charge->status;
                        $PaymentOrder->created_at = date('Y-m-d H:i:s');
                        $PaymentOrder->save();
                        $Appointment = Appointment::find($appoinmentId);
                        $Appointment->payment_status = ($charge->status == 'succeeded' ? 1 : 0);
                        $Appointment->save();
                        $Appointment = Appointment::where('id', $appoinmentId)->first();
                        $getCategoryName = CategoryAppointment::where('id', $Appointment['category_id'])->pluck('category')->first();
                        $getcharges = CategoryAppointment::where('id', $Appointment['category_id'])->pluck('charges')->first();
                        $data = [];
                        $data['booking_date'] = date('Y-m-d', strtotime($Appointment->booking_date));
                        $data['start_time'] = $Appointment->start_time;
                        $data['end_time'] = $Appointment->end_time;
                        $data['category'] = $getCategoryName;
                        $data['charges'] = $getcharges;
                        $data['description'] = $Appointment->description;
                        $data['currency'] = Cache::get('currency');
                        //echo 'Payment done successfully !Charge Id is '.$charge->id;
                        //$PaymentOrder = PaymentOrder::where('appointment_id',$Appointment->id)->first();
                        $response = str_replace("Stripe\Charge JSON:", "", $PaymentOrder->response);
                        $response = json_decode($response, true);


                        $userObj = AppUser::where('id', $Appointment->appuser_id)->first();
                        $emailFrom = Setting::where('slug', 'email')->first();
                        $getcharges = CategoryAppointment::where('id', $Appointment['category_id'])->pluck('charges')->first();
                        $msg = "Payment succeeded";
                        $booking_time = Carbon::parse($Appointment->start_time)->format('g:i A') . '-' . Carbon::parse($Appointment->end_time)->format('g:i A');

                        if ($Appointment->language_id == 2) {
                            $emailTemplateObj = EmailTemplate::where('id', 12)->where('language_id', 2)->first();
                        } else {
                            $emailTemplateObj = EmailTemplate::where('id', 11)->where('language_id', 1)->first();
                        }

                        $subject = $emailTemplateObj->subject;
                        $body = $emailTemplateObj->body;
                        $body = str_replace('[InvoiceID]', $Appointment->id, $body);
                        $body = str_replace('[Date]', Carbon::parse($Appointment->created_at)->format('d/m/Y'), $body);
                        $body = str_replace('[UserName]', $userObj->username, $body);
                        $body = str_replace('[email]', $userObj->email, $body);
                        $body = str_replace('[ChangeID]', $response['id'], $body);
                        $body = str_replace('[CardID]', $response['source']['id'], $body);
                        $body = str_replace('[Amount]', Cache::get('currency') . $getcharges, $body);
                        $body = str_replace('[Balance_Transaction]', $response['balance_transaction'], $body);
                        $body = str_replace('[Currency]', $response['currency'], $body);
                        $body = str_replace('[CustomerID]', $response['customer'], $body);
                        $body = str_replace('[Card_Brand]', $response['source']['brand'], $body);
                        $body = str_replace('[Exp_Month]', $response['source']['exp_month'], $body);
                        $body = str_replace('[Exp_Year]', $response['source']['exp_year'], $body);
                        $body = str_replace('[payment_description]', $response['description'], $body);
                        $body = str_replace('[category]', $getCategoryName, $body);
                        $body = str_replace('[booking_date]', Carbon::parse($Appointment->booking_date)->format('d/m/Y'), $body);
                        $body = str_replace('[booking_time]', $booking_time, $body);
                        $body = str_replace('[description]', $Appointment->description, $body);

                        $emailFrom = Cache::get('email');
                        $msg = "Payment succeeded";
                        $dataEmail = array("name" => $userObj->username, 'body' => $body, "to" => $userObj->email, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'FRONT_URL' => url(''), 'emails' => array($userObj->email, Cache::get('email')));
                        //$emails = [$dataEmail['to'], 'firdosh.a@latitudetechnolabs.com'];

                        $res = Mail::send('emails.appoinment-book', ['data' => $dataEmail], function($Emailmessage) use ($dataEmail) {
                                    $Emailmessage->from($dataEmail['from'], $dataEmail['emailLabel']);
                                    $Emailmessage->to([$dataEmail['to'], $dataEmail['from']], $dataEmail['name']);
                                    $Emailmessage->subject($dataEmail['subject']);
                                });

                        // $dataEmail = array("name"=>$userObj->username,'message' => $msg,"to"=>$userObj->email,"subject"=>"Payment","from" => $emailFrom->value, "emailLabel" => "Payment","PaymentOrder" => $PaymentOrder,"response" => $response,"getCategoryName" =>$getCategoryName,"Appointment" => $Appointment,'AppointmentId' =>$Appointment->id,'date' => Carbon::parse($Appointment->created_at)->format('d/m/Y'),'booking_date' => Carbon::parse($Appointment->booking_date)->format('d/m/Y'),'booking_time' => $booking_time,'currency' => Cache::get('currency'),'getcharges' => $getcharges);
                        // $res =Mail::send('emails.appoinment-book',['data'=>$dataEmail],function($Emailmessage) use ($dataEmail){
                        //       $Emailmessage->from($dataEmail['from'],$dataEmail['emailLabel']);
                        //       $Emailmessage->to($dataEmail['to'],$dataEmail['name']);
                        //       $Emailmessage->subject($dataEmail['subject']);
                        //       });


                        echo json_encode([
                            'status' => true,
                            'success' => 200,
                            'message' => "Payment done successfully",
                            'data' => $data
                        ]);
                        exit;
                    } else {
                        //echo 'Payment Failed !';
                        echo json_encode([
                            'status' => false,
                            'error' => 401,
                            'message' => 'Payment Failed !',
                        ]);
                        exit;
                    }
                } else {
                    //echo "Customer account not created.";
                    echo json_encode([
                        'status' => false,
                        'error' => 401,
                        'message' => 'Customer account not created!',
                    ]);
                    exit;
                }
            }
        } catch (\Stripe\Error\Authentication $e) {
            //Session::put(‘error’,$e->getMessage());
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
            //return redirect()->route(‘addmoney.paywithstripe’);
        } catch (\Stripe\Error\InvalidRequest $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (\Stripe\Error\RateLimit $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (\Stripe\Error\Card $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (\Stripe\Error\ApiConnection $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (\Stripe\Error\Base $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (Exception $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        }
    }

    public function refundPayment(Request $request) {

        $rules = [
            'appointment_id' => 'required',
            'language_id' => 'required'
        ];
        $requestData = Input::all();
        $message = [
            "appointment_id.required" => "Please provide appointment id",
            "language_id.required" => "Please provide language id"
        ];
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first(),
            ]);
            exit;
        }

        $appoinment = Appointment::find($request->appointment_id);
        $appoinment->cancel_type = 0;
        //$appoinment->status = 5; // user is refund
        $appoinment->save();

        $paymentOrderObj = PaymentOrder::where('appointment_id', $request->appointment_id)->first();

        require_once(base_path('stripe/init.php'));

        \Stripe\Stripe::setApiKey('sk_test_1a2ZxW34hF5TeVPbCOHLl4dc');
        try {
            $refund = \Stripe\Refund::create([
                        'charge' => $paymentOrderObj->charge_id,
                        'amount' => $paymentOrderObj->amount,
            ]);

            $PaymentOrder = PaymentOrder::find($paymentOrderObj->id);
            $PaymentOrder->refund_response = $refund;
            $PaymentOrder->type = 2;
            $PaymentOrder->save();

            $appUserObj = AppUser::where('id', $appoinment->appuser_id)->first();
            $setting = Setting::where('slug', 'email')->first();
            if ($appoinment->expert_id != null && $appoinment->expert_id > 0) {
                $userObj = User::where('id', $appoinment->expert_id)->where('status', 1)->first();
                $emailTemplateObj = EmailTemplate::where('id', 17)->where('language_id', 1)->first();

                $subject = $emailTemplateObj->subject;
                $tomail = $userObj->email;
                $body = $emailTemplateObj->body;

                $body = str_replace('[UserName]', $appUserObj->username, $body);
                $body = str_replace('[ExpertName]', $userObj->first_name . ' ' . $userObj->last_name, $body);

                $data = array("name" => $appUserObj->username, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'body' => $body, 'FRONT_URL' => url(''));

                $res = Mail::send('emails.appointment-cancel', ['data' => $data], function($message) use ($data) {
                            $message->from($data['from'], $data['emailLabel']);
                            $message->to($data['to'], $data['name']);
                            $message->subject($data['subject']);
                        });
            }
            $emailTemplateObj = EmailTemplate::where('id', 22)->where('language_id', 1)->first();

            $subject = $emailTemplateObj->subject;
            $tomail = $appUserObj->email;
            $body = $emailTemplateObj->body;
            $body = str_replace('[UserName]', $appUserObj->username, $body);

            $data = array("name" => $appUserObj->username, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'body' => $body, 'FRONT_URL' => url(''));

            $res = Mail::send('emails.appointment-cancel', ['data' => $data], function($message) use ($data) {
                        $message->from($data['from'], $data['emailLabel']);
                        $message->to($data['to'], $data['name']);
                        $message->subject($data['subject']);
                    });
            echo json_encode([
                'status' => true,
                'success' => 200,
                'message' => Cache::get('refund_message'),
            ]);
            exit;
        } catch (\Stripe\Error\Authentication $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (\Stripe\Error\InvalidRequest $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (\Stripe\Error\RateLimit $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (\Stripe\Error\Card $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (\Stripe\Error\ApiConnection $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (\Stripe\Error\Base $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        } catch (Exception $e) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $e->getMessage(),
            ]);
            exit;
        }
    }

}
