<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\AppUser;
use App\Models\User;
use App\Models\Question;
use App\Models\Notification;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Cache;

class QuestionController extends Controller
{

	protected $customerId;
    public function __construct() {
      header("Content-Type: application/json");
      // date_default_timezone_set('Asia/Kolkata');
      $headers = getallheaders();
       // echo "<pre>";print_r($headers);exit;
      $apiToken = isset($headers['apiToken'])?$headers['apiToken']:'';
       if($apiToken != ''){
			$customer = AppUser::where('token',$apiToken)->get()->toArray();
       		if(count($customer) > 0){
       			$this->customerId = $customer[0]['id'];
       		}else{
       			echo json_encode([
                        'status'    => false,
                        'error'		=> 201,
                        'message'   => 'Invalid token',
                    ]);
       			exit;
       		}
       }else{
       		echo json_encode([
                        'status'    => false,
                        'error'		=> 401,
                        'message'   => 'Token is required',
                    ]);
       		exit;
       }
    }

    public function addQuestion(Request $request){
    	$rules = [
            'language_id'   => 'required',
            'question'      => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
            "question.required"         => "Question is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }


        $questionObj = new Question;
        $questionObj->language_id = $request->get('language_id');
        $questionObj->appuser_id = $this->customerId;
        $questionObj->question = $request->get('question');

        if ($questionObj->save()) {
          echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => Cache::get('add_question'),
                    ]);
          exit;
        } else {
            echo json_encode([
                        'status'    => false,
                        'error'   => 401,
                        'message'   => Cache::get('went_wrong'),
                    ]);
            exit;
        }

    }

    public function searchQuestion(Request $request){
      $rules = [
            'language_id'     => 'required',
            //'keyword'     => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
            //"keyword.required"          => "Keyword is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }
        $limit = $request->get('limit') != null?$request->get('limit'):5;
        $page = $request->get('page') != null?$request->get('page') - 1:'0';
        $start = $page * $limit;
        $category_id = $request->get('category_id') != null?$request->get('category_id'):'';
        //$topic_id = $request->get('topic_id') != null?$request->get('topic_id'):'';
        $words = "";
        if($request->get('keyword') != ''){
          $words = explode(' ',$request->get('keyword'));
        }
        
        //$search_filter = $request->get('search_filter') != null?$request->get('search_filter'):0;
        $search_filter = $request->get('search_filter');

        $getData = DB::table('question')
                    ->select(DB::raw('question.*,user.username'))
                    ->leftjoin('user', 'question.expert_id', '=', 'user.id')
                    ->leftjoin('category', 'category.id', '=', 'question.category_id')
                    // ->where('question', 'like','%'.$request->get('keyword').'%')
                    
                    ->where(function($query) use ($words)  {
                      if($words != ''){
                        foreach ($words as $key => $keyword) {
                        if(!empty($keyword)){
                            if($key == 0){
                              $query->where('question', 'like','%'.$keyword.'%');
                            }else{
                              $query->orWhere('question', 'like','%'.$keyword.'%');
                            }
                        }
                      }
                    }
                      
                     })
              			->where(function($query) use ($category_id)  {
      			            if(!empty($category_id)) {
      			                $query->where('question.category_id', $category_id);
      			            }
      			         })
              			// ->where(function($query) use ($topic_id)  {
      			        //     if(!empty($topic_id)) {
      			        //         $query->where('topic_id', $topic_id);
      			        //     }
      			        //  })
                  	->where('question.language_id',$request->get('language_id'))
                    //->where('question.is_private',0)
                    ->where(function($query) use ($search_filter)  {
                        if(!empty($search_filter && $search_filter == 0)) {
                            $query->orderBy('created_at', 'desc');
                        }
                        //is_popular
                        if(!empty($search_filter && $search_filter == 1)) {
                            $query->orderBy('is_popular', 'desc');
                        }
                        if(!empty($search_filter && $search_filter == 2)) {
                          $query->where('category.is_seasonal', 1);
                        }
                     })
                    ->take($limit)->skip($start)
                  	->get();

        $data = array();
        if(count($getData) > 0){
            foreach ($getData as $key => $resultData) {
            $data[$key]['id'] = $resultData->id;
            $data[$key]['category_id'] = isset($resultData->category_id)?$resultData->category_id:'';
            //$data[$key]['topic_id'] = isset($resultData->topic_id)?$resultData->topic_id:'';
            $data[$key]['expert_id'] = isset($resultData->expert_id)?$resultData->expert_id:'';
            $data[$key]['expert_name'] = isset($resultData->username)?$resultData->username:'';
            $data[$key]['question'] = isset($resultData->question)?$resultData->question:'';
            $data[$key]['answer'] = isset($resultData->answer)?$resultData->answer:'';
            $data[$key]['date'] = date('Y-m-d H:i:s', strtotime($resultData->updated_at));
          }
          echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Search Listing',
                          'data'      => $data
                      ]);
          exit;
        }else{
          echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
          exit;
        }
        
    }

    public function quesionSuggestion(Request $request){
        $rules = [
            'language_id'   => 'required',
            'question'      => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
            "question.required"         => "Question is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $getData = Question::where('question', 'like','%'.$request->get('question').'%')
                  ->where('language_id',$request->get('language_id'))
                  ->get();

        $data = array();
        if(count($getData) > 0){
            foreach ($getData as $key => $resultData) {
            $data[$key]['id'] = $resultData->id;
            $data[$key]['expert_id'] = isset($resultData->expert_id)?$resultData->expert_id:'';
            $data[$key]['question'] = isset($resultData->question)?$resultData->question:'';
            $data[$key]['answer'] = isset($resultData->answer)?$resultData->answer:'';
          }
          echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Suggested Answer',
                          'data'      => $data
                      ]);
          exit;
        }else{
          echo json_encode([
                          'status'    => false,
                          'success'   => 201,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
          exit;
        }
        
    }

    public function getAnswer(Request $request){
    	$rules = [
            'language_id'   => 'required',
            'question_id'      => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
            "question_id.required"      => "Question Id is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }
        //$question = Question::find($request->get('question_id'));
        //$question->is_popular = is_popular+1
        DB::table('question')->where('id', $request->get('question_id'))->increment('is_popular');

        $questionObj = Question::where('id',$request->get('question_id'))->where('language_id',$request->get('language_id'))->first();

        if(count($questionObj) > 0){

          $getExpertName = User::where('id',$questionObj->expert_id)->pluck('username')->first();
        	$data['question'] = isset($questionObj->question)?$questionObj->question:'';
        	$data['answer'] = isset($questionObj->answer)?$questionObj->answer:'';
          $data['expert_name'] = isset($getExpertName)?$getExpertName:'';
          $data['datetime'] = isset($questionObj->updated_at)?$questionObj->updated_at->format('Y-m-d H:m:s'):'';
        	echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Answer',
                          'data'      => $data
                      ]);
       		exit;
        }else{
        	echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
       		exit;
        }

    }

    public function getLatestQuestionData(Request $request){
    	$rules = [
            'language_id'   => 'required',
            // 'limit'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Language Id is required",
            // "limit.required"       => "Limit is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $limit = $request->get('limit') != null?$request->get('limit'):'4';
  	    $questionObj = Question::where('language_id',$request->get('language_id'))->orderBy('id','desc')->take($limit)->get();

        $data = array();
        if(count($questionObj) > 0){
            foreach ($questionObj as $key => $resultData) {
              $data[$key]['question'] = isset($resultData->question)?$resultData->question:'';
        	    $data[$key]['answer'] = isset($resultData->answer)?$resultData->answer:'';
          }
          	echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Latest 4 Data',
                          'data'      => $data
                      ]);
          	exit;
        }else{
          	echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
          	exit;
        }
        
    }

    public function getNotification(Request $request){
        $rules = [
            'language_id'   => 'required',
            // 'limit'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Language Id is required",
            // "limit.required"       => "Limit is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $notificationObj  = DB::table('notification')
                            // ->join('appuser', 'notification.appuser_id', '=', 'appuser.id')
                            ->join('user', 'notification.expert_id', '=', 'user.id')
                            ->join('question', 'notification.question_id', '=', 'question.id')
                            ->select('notification.*','user.username as expertName','question.answer','question.question')
                            ->where('notification.language_id',$request->get('language_id'))
                            ->where('notification.appuser_id',$this->customerId)
                            ->orderBy('notification.id','desc')
                            ->groupBy('notification.question_id')
                            ->get();

        if(count($notificationObj) > 0){
            $data = array();
            foreach ($notificationObj as $key => $resultData) {
                $data[$key]['notification_id']  = $resultData->id;
                $data[$key]['language_id']      = $resultData->language_id;
                $data[$key]['question_id']      = $resultData->question_id;
                $data[$key]['question']         = $resultData->question;
                $data[$key]['expertName']       = $resultData->expertName;
                $data[$key]['answer']           = $resultData->answer;
                $data[$key]['message']          = $resultData->message;
                $data[$key]['is_read']          = $resultData->is_read;
                $data[$key]['datetime']         = $resultData->created_at;
            }

            echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Latest 4 Data',
                          'data'      => $data
                      ]);
            exit;

        }else{
          echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
          exit;
        }

    }


    public function readNotification(Request $request) {

        $rules = [
            'notification_id' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "notification_id.required" => 'Please provide your notification id',
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                'status' => false,
                'error' => 401,
                'message' => $validator->messages()->first(),
            ]);
            exit;
        }

        $notification = Notification::find($request->get('notification_id'));
        if (count($notification) > 0) {
            $notification->is_read = '1';
            $notification->save();
            echo json_encode([
                'status' => true,
                'success' => 200,
                'message' => 'Notification Readed',
            ]);
            exit;
        }
    }

    public function getQuestion(Request $request){
    $rules = [
            'language_id'   => 'required',
            'appuser_id'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Language Id is required",
            "appuser_id.required"       => "user id is required",
        ];

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $questionObj = Question::where('appuser_id',$request->get('appuser_id'))->where('language_id',$request->get('language_id'))->get()->toArray();
        $dataArr = array();
        if(count($questionObj) > 0){

          foreach ($questionObj as $key => $questionVal) {
            $getExpertName = User::where('id',$questionVal['expert_id'])->pluck('username')->first();
          $dataArr[$key]['id'] = isset($questionVal['id'])?$questionVal['id']:'';
          $dataArr[$key]['question'] = $questionVal['question'];
          $dataArr[$key]['answer'] = $questionVal['answer'];
          $dataArr[$key]['expert_name'] = isset($getExpertName)?$getExpertName:'';
          $dataArr[$key]['date'] = date('Y-m-d H:i:s', strtotime($questionVal['updated_at']));
          // $dataArr[$key]['answer'] = isset($question['answer'])?$question['answer']:'';

          }
          echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => Cache::get('list_question'),
                          'data'      => $dataArr
                      ]);
            exit;
        }else{
          echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_question'),
                      ]);
            exit;
        }
        
  }

}