<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\AppUser;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Cache;

class UserController extends Controller
{
   	public function __construct() {
        header("Content-Type: application/json");
        // date_default_timezone_set('Asia/Kolkata');
        $headers = getallheaders();
       // echo "<pre>";print_r($headers);exit;
       $apiToken = isset($headers['apiToken'])?$headers['apiToken']:'';
       if($apiToken != ''){
       		if($apiToken != 'L@titude2018'){
       			echo json_encode([
                        'status'    => false,
                        'error'		=> 201,
                        'message'   => 'Invalid token',
                    ]);
       			exit;
       		}
       }else{
       		echo json_encode([
                        'status'    => false,
                        'error'		=> 401,
                        'message'   => 'Token is required',
                    ]);
       		exit;
       }
    }

    public function register(Request $request){
    	$rules = [
            // 'username' => 'required',
            'email' => 'email|required|unique:appuser',
            'password' => 'required|min:6',
            'device_type' => 'required',
            'device_id' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            // "username.required" => 'Please provide your Nick Name',
            "email.email" => "Invalid e-mail address",
            "email.required" => "Please provide email",
            "password.required" => "Please provide password",
            "password.min" => "Password should be minimum 6 character length",
            "device_type.required" => 'Please provide your device type',
            "device_id.required" => 'Please provide your device id',
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
                echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }
        
        $customer = new AppUser;
        $customer->username = ($request->get('username') != null)? $request->get('username'):'';
        $customer->email = $request->get('email');
        $customer->password = bcrypt($request->get('password'));
        $token = md5(rand(111111111, 999999999));
        $customer->token = $token;
        $customer->device_type = $request->get('device_type');
        $customer->device_id = $request->get('device_id');
        $customer->language_id = 1;
        $customer->status = 1;
        // $customer->device_token = $request->get('device_token');
        $customer->created_at = date("Y-m-d H:i:s");
        if ($customer->save()) {
        	echo json_encode([
                        'status'    => true,
                        'success'		=> 200,
                        'message'   => Cache::get('register'),
                        'data' => [
                        	"userId"=> $customer->id,
                            "email"=> $customer->email,
                            "gender"=> ($customer->gender == 1 ? 'Male' : 'Female'),
                            "language_id" => $customer->language_id,
	                       	"token" => $token
                    	]
                    ]);
            exit;
        } else {
            echo json_encode([
                        'status'    => false,
                        'error'		=> 401,
                        'message'   => Cache::get('went_wrong'),
                    ]);
            exit;
        }
    }

    public function login(Request $request){
        $rules = [
            'device_type' => 'required',
            'device_id' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "device_type.required" => 'Please provide your device type',
            "device_id.required" => 'Please provide your device id',
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }


        $login_type = ($request->get('login_type') != null)?$request->get('login_type'):'0';
        $email = ($request->get('email') != null)?$request->get('email'):'';
        $user = AppUser::where('email', $email)->first();
        $token = md5(rand(111111111, 999999999));
        if($login_type == 1){

            $userCheck = AppUser::where('facebook_id', $request->get('social_id'))->first();
            if(count($userCheck) > 0){

                $userCheck->device_type = $request->get('device_type');
                $userCheck->device_id = $request->get('device_id');
                $userCheck->login_type = 1;
                $userCheck->status = 1;
                $userCheck->is_login = 1;
                $userCheck->update();
                $id = $userCheck['id'];
            }else{
                if($user)
                {
                    $userObj = AppUser::find($user['id']);
                }else{
                    $userObj = new AppUser;
                    $userObj->token = $token;
                    $userObj->language_id = 1;
                    $userObj->is_verified = 1;
                }
                $userObj->username = ($request->get('username') != null)? $request->get('username'):'';
                $userObj->email = ($request->get('email') != null)?$request->get('email'):'';
                $userObj->facebook_id = $request->get('social_id');
                $userObj->device_type = $request->get('device_type');
                $userObj->login_type = 1;
                $userObj->device_id = $request->get('device_id');
                $userObj->status = 1;
                $userObj->is_login = 1;
                $userObj->save();
                
                $id = $userObj->id;
            }
            $user = AppUser::where('id', $id)->first();
            echo json_encode([
                        'status'    => true,
                        'success'     => 200,
                        'message'   => Cache::get('login'),
                        'data' => [
                            "userId"=> $user->id,
                            "username"=> $user->username,
                            "email"=> $user->email,
                            "language_id"=> $user->language_id,
                            "token" => $user->token
                        ]
                    ]);
            exit;

        }
        if ($login_type == 2) {
            $userCheck = AppUser::where('google_id', $request->get('social_id'))->first();

            if(count($userCheck) > 0){
                $userCheck->device_type = $request->get('device_type');
                $userCheck->device_id = $request->get('device_id');
                $userCheck->login_type = 2;
                $userCheck->status = 1;
                $userCheck->is_login = 1;
                $userCheck->update();
                $id = $userCheck['id'];
            }else{
                if($user)
                {
                    $userObj = AppUser::find($user['id']);
                }else{
                    $userObj = new AppUser;
                    $userObj->token = $token;
                    $userObj->language_id = 1;
                    $userObj->is_verified = 1;
                }
                $userObj->username = ($request->get('username') != null)?$request->get('username'):'';
                $userObj->email = ($request->get('email') != null)?$request->get('email'):'';
                $userObj->google_id = $request->get('social_id');
                $userObj->login_type = 2;
                $userObj->device_type = $request->get('device_type');
                $userObj->device_id = $request->get('device_id');
                $userObj->is_login = 1;
                $userObj->status = 1;
                $userObj->save();

                $id = $userObj->id;
            }
            $user = AppUser::where('id', $id)->first();
            echo json_encode([
                        'status'    => true,
                        'success'     => 200,
                        'message'   => Cache::get('login'),
                        'data' => [
                            "userId"=> $user->id,
                            "username"=> $user->username,
                            "email"=> $user->email,
                            "language_id"=> $user->language_id,
                            "token" => $user->token
                        ]
                    ]);
            exit;
        }
        
        if ($login_type == 0){
            $user = AppUser::where('email', $request->get('email'))->first();
                if(count($user) > 0){
                    $validCredentials = Hash::check($request->get('password'), $user->password);
                    if($validCredentials){
                        // $userObj->status = 1;
                        $userObj = AppUser::find($user['id']);
                        $userObj->is_login = 1;
                        $userObj->login_type = 0;
                        $userObj->device_id = $request->get('device_id');
                        $userObj->device_type = $request->get('device_type');
                        $userObj->save();
                        
                        $userObj = AppUser::where('id',$user->id)->first();
                      
                        echo json_encode([
                                    'status'    => true,
                                    'success'     => 200,
                                    'message'   => Cache::get('login'),
                                    'data' => [
                                        "userId"=> $user->id,
                                        "username"=> $user->username,
                                        "email"=> $user->email,
                                        "language_id"=> $user->language_id,
                                        "token" => $user->token
                                    ]
                                ]);
                        exit;
                    }else{
                        echo json_encode([
                                    'status'    => false,
                                    'error'     => 401,
                                    'message'   => Cache::get('invalid_pass'),
                                ]);
                        exit;
                    }
                }else{
                    echo json_encode([
                                    'status'    => false,
                                    'error'     => 401,
                                    'message'   => Cache::get('email_not_register'),
                                ]);
                    exit;
                }
        }
    }
    
    public function forgotPassword(Request $request){
    	$rules = [
            'email' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "email.email" => "Invalid e-mail address",
            "email.required" => "Please provide email",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }

        $userObj = AppUser::where('email', $request->get('email'))->first();
        if(count($userObj) > 0){

                $tomail  = $request->get('email');
                $subject  = 'Religion Email Verification';
                $body  = 'Religion Email Verification';

                $code = mt_rand(1000, 9999);
                // $confirm_link = env('FRONT_URL') . '/api/account-activation/' . $code;
                $data = array("name"=>$request->get('first_name'),"code"=>$code,'message'=>$body,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => 'Religion');
                
                $res =Mail::send('Api.user.forgotPasswordCode',['data'=>$data],function($message) use ($data){
                    $message->from($data['from'],$data['emailLabel']);
                    $message->to($data['to'],$data['name']);
                    $message->subject($data['subject']);
                });
                
                $userObj->verifycode = $code;
                $userObj->save();
                echo json_encode([
                        'status'    => true,
                        'success'	=> 200,
                        'message'   => Cache::get('sent_code'),
                        'data' => [
                                "token" => $userObj->token
                            ]
                    ]);
        }else{
        	echo json_encode([
	                        'status'    => false,
	                        'error'		=> 401,
	                        'message'   => Cache::get('email_not_register'),
	                    ]);
	            	exit;
        }

    }

    public function verifycode(Request $request){
        $rules = [
            'code'          => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "code.required" => "Code is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $userObj = AppUser::where('verifycode',$request->get('code'))->first();
        if(count($userObj) > 0){
            $userObj->is_verified = '1';
            $userObj->save();
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => Cache::get('verified'),
                    ]);
                exit;
        }else{
            echo json_encode([
                            'status'    => false,
                            'error'     => 401,
                            'message'   => Cache::get('code_invalid'),
                        ]);
                    exit;
        }
    }

    public function resetPassword(Request $request){
        $rules = [
            'email'      => 'required',
            'password'      => 'required|min:6',
        ];

        $requestData = Input::all();

        $message = [
            "email.email" => "Invalid e-mail address",
            "email.required" => "Please provide email",
            "password.required"         => "Password is required",
            "password.min"              => "Password should be six characters long",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $userObj = AppUser::where('email',$request->get('email'))->first();
        if(count($userObj) > 0){
            $userObj->password = bcrypt($request->get('password'));
            $userObj->save();
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => Cache::get('password_changed'),
                    ]);
                exit;
        }else{
            echo json_encode([
                            'status'    => false,
                            'error'     => 401,
                            'message'   => Cache::get('email_invalid'),
                        ]);
                    exit;
        }
    }
}


