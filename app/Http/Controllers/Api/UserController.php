<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\AppUser;
use App\Models\FAQ;
use Illuminate\Support\Facades\Hash;
use App\Models\CMS;
use App\Models\EmailTemplate;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Cache;

class UserController extends Controller
{
   	public function __construct() {
        header("Content-Type: application/json");
        //$headers = getallheaders();
        //$headers = app('request');
       $headers['apiToken'] = app('request')->headers->get('apitoken');
       $apiToken = isset($headers['apiToken'])?$headers['apiToken']:'';
       if($apiToken != ''){
       		if($apiToken != 'L@titude2018'){
       			echo json_encode([
                        'status'    => false,
                        'error'		=> 201,
                        'message'   => 'Invalid token',
                    ]);
       			exit;
       		}
       }else{
       		echo json_encode([
                        'status'    => false,
                        'error'		=> 401,
                        'message'   => 'Token is required',
                    ]);
       		exit;
       }
    }

    public function register(Request $request){
    	$rules = [
            // 'username' => 'required',
            'email' => 'email|required|unique:appuser',
            'password' => 'required|min:6',
            'device_type' => 'required',
            'device_id' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            // "username.required" => 'Please provide your Nick Name',
            "email.email" => "Invalid e-mail address",
            "email.required" => "Please provide email",
            "password.required" => "Please provide password",
            "password.min" => "Password should be minimum 6 character length",
            "device_type.required" => 'Please provide your device type',
            "device_id.required" => 'Please provide your device id',
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
                echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }
        $token = md5(rand(111111111, 999999999));
        $room = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

        $customer = new AppUser;
        $username = explode('@',$request->get('email'));
        $customer->username = ($request->get('username') != null)? $request->get('username'):$username[0];
        $customer->email = $request->get('email');
        $customer->password = bcrypt($request->get('password'));
        $customer->room = $room;
        $customer->token = $token;
        $customer->device_type = $request->get('device_type');
        $customer->device_id = $request->get('device_id');
        $customer->language_id = 1;
        $customer->status = 1;
        // $customer->device_token = $request->get('device_token');
        $customer->created_at = date("Y-m-d H:i:s");
        if ($customer->save()) {
            $user = AppUser::where('email', $request->get('email'))->first();
            $token = $user->token;

        // if(Session::get('selectedLang') == 2){
        //       $emailTemplateObj =  EmailTemplate::where('id',5)->where('language_id',2)->first();
        // }else{
        //       $emailTemplateObj =  EmailTemplate::where('id',4)->where('language_id',1)->first();
        // }
     $emailTemplateObj =  EmailTemplate::where('id',4)->where('language_id',1)->first();
       $confirm_link = env('FRONT_URL') . '/account-activation/' . $token;
       $tomail   = $user->email;
       $subject  = $emailTemplateObj->subject;
       $body     = $emailTemplateObj->body;
       $body     =  str_replace('[confirm_link]',$confirm_link,$body);
            
        $data = array("name"=>"Dear Friend","confirm_link"=>$confirm_link,'body'=>$body,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'FRONT_URL' => url(''));
        
        $res =Mail::send('emails.signup',['data'=>$data],function($message) use ($data){
            $message->from($data['from'],$data['emailLabel']);
            $message->to($data['to'],$data['name']);
            $message->subject($data['subject']);
        });
        //Account activation link send to your email address.
        	echo json_encode([
                        'status'    => true,
                        'success'		=> 200,
                        'message'   => Cache::get('register_verification_message'),
                    ]);
            exit;
        } else {
            echo json_encode([
                        'status'    => false,
                        'error'		=> 401,
                        'message'   => Cache::get('went_wrong'),
                    ]);
            exit;
        }
    }

    public function login(Request $request){
        $rules = [
            'device_type' => 'required',
            'device_id' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "device_type.required" => 'Please provide your device type',
            "device_id.required" => 'Please provide your device id',
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }


        $login_type = ($request->get('login_type') != null)?$request->get('login_type'):'0';
        $email = ($request->get('email') != null)?$request->get('email'):'';
        $user = AppUser::where('email', $email)->first();
        $random = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        
        $token = md5(rand(111111111, 999999999));
        if($login_type == 1){

            $userCheck = AppUser::where('facebook_id', $request->get('social_id'))->where('status',1)->first();
            if(count($userCheck) > 0){

                $userCheck->device_type = $request->get('device_type');
                $userCheck->device_id = $request->get('device_id');
                $userCheck->login_type = 1;
                $userCheck->status = 1;
                $userCheck->is_login = 1;
                $userCheck->token = $token;
                if($user->room == null){
                    $userCheck->room = $random;
                    }
                //$userCheck->token = $token;
                $userCheck->update();
                $id = $userCheck['id'];
            }else{
                if($user)
                {
                    $userObj = AppUser::find($user['id']);
                    if($user->room == null){
                        $userObj->room = $random;
                    }
                }else{
                    $userObj = new AppUser;
                    
                    //$userObj->token = $token;
                    $userObj->room = $random;
                    $userObj->language_id = 1;
                    $userObj->is_verified = 1;
                }
                
                $userObj->username = ($request->get('username') != null)? $request->get('username'):'';
                $userObj->email = ($request->get('email') != null)?$request->get('email'):'';
                $userObj->facebook_id = $request->get('social_id');
                $userObj->device_type = $request->get('device_type');
                $userObj->login_type = 1;
                $userObj->device_id = $request->get('device_id');
                $userObj->status = 1;
                $userObj->is_login = 1;
                $userObj->token = $token;
                $userObj->room = $random;
                $userObj->save();
                
                $id = $userObj->id;
            }
            $user = AppUser::where('id', $id)->first();
            echo json_encode([
                        'status'    => true,
                        'success'     => 200,
                        'message'   => Cache::get('login'),
                        'data' => [
                            "userId"=> $user->id,
                            "username"=> $user->username,
                            "email"=> $user->email,
                            "language_id"=> $user->language_id,
                            "token" => $user->token,
                            "room" => $user->room
                        ]
                    ]);
            exit;

        }
        if ($login_type == 2) {
            $userCheck = AppUser::where('google_id', $request->get('social_id'))->where('status',1)->first();

            if(count($userCheck) > 0){
                $userCheck->device_type = $request->get('device_type');
                $userCheck->device_id = $request->get('device_id');
                $userCheck->login_type = 2;
                $userCheck->status = 1;
                $userCheck->is_login = 1;
                $userCheck->token = $token;
                if($user->room == null){
                    $userCheck->room = $random;
                    }
                $userCheck->update();
                $id = $userCheck['id'];
            }else{
                if($user)
                {
                    $userObj = AppUser::find($user['id']);
                    if($user->room == null){
                    $userObj->room = $random;
                    }
                }else{
                    $userObj = new AppUser;
                    //$userObj->token = $token;
                    $userObj->room = $random;
                    $userObj->language_id = 1;
                    $userObj->is_verified = 1;
                }
                
                $userObj->username = ($request->get('username') != null)?$request->get('username'):'';
                $userObj->email = ($request->get('email') != null)?$request->get('email'):'';
                $userObj->google_id = $request->get('social_id');
                $userObj->login_type = 2;
                $userObj->device_type = $request->get('device_type');
                $userObj->device_id = $request->get('device_id');
                $userObj->is_login = 1;
                $userObj->status = 1;
                $userObj->token = $token;
                $userObj->save();

                $id = $userObj->id;
            }
            $user = AppUser::where('id', $id)->first();
            echo json_encode([
                        'status'    => true,
                        'success'     => 200,
                        'message'   => Cache::get('login'),
                        'data' => [
                            "userId"=> $user->id,
                            "username"=> $user->username,
                            "email"=> $user->email,
                            "language_id"=> $user->language_id,
                            "token" => $user->token,
                            "room" => $user->room
                        ]
                    ]);
            exit;
        }
        
        if ($login_type == 0){
            $user = AppUser::where('email', $request->get('email'))->where('status',1)->first();
                if(count($user) > 0){
                    if(isset($user->is_verified) && $user->is_verified == '1'){
                    $validCredentials = Hash::check($request->get('password'), $user->password);
                    if($validCredentials){
                        // $userObj->status = 1;
                        $userObj = AppUser::find($user['id']);
                        $userObj->is_login = 1;
                        $userObj->login_type = 0;
                        $userObj->device_id = $request->get('device_id');
                        $userObj->device_type = $request->get('device_type');
                        $userObj->token = $token;
                        if($user->room == null){
                            $userObj->room = $random;
                        }
                        $userObj->save();
                        
                        $userObj = AppUser::where('id',$user->id)->first();
                      
                        echo json_encode([
                                    'status'    => true,
                                    'success'     => 200,
                                    'message'   => Cache::get('login'),
                                    'data' => [
                                        "userId"=> $userObj->id,
                                        "username"=> $userObj->username,
                                        "email"=> $userObj->email,
                                        "language_id"=> $userObj->language_id,
                                        "token" => $userObj->token,
                                        "room" => $userObj->room
                                    ]
                                ]);
                        exit;
                    }else{
                        echo json_encode([
                                    'status'    => false,
                                    'error'     => 401,
                                    'message'   => Cache::get('invalid_pass'),
                                ]);
                        exit;
                    } }else{
                        echo json_encode([
                                'status'    => false,
                                'error'     => 401,
                                'message'   => Cache::get('incomplete_verification'),
                                'data' => [
                                "is_verified"=> $user->is_verified,
                                "token"      => $user->token
                                ]
                            ]);
                        exit;
                    }


                }else{
                    echo json_encode([
                                    'status'    => false,
                                    'error'     => 401,
                                    'message'   => Cache::get('email_not_register'),
                                ]);
                    exit;
                }
        }
    }
    
    public function forgotPassword(Request $request){
    	$rules = [
            'email' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "email.email" => "Invalid e-mail address",
            "email.required" => "Please provide email",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }

        $userObj = AppUser::where('email', $request->get('email'))->first();
        if(count($userObj) > 0){
        $emailTemplateObj =  EmailTemplate::where('id',18)->where('language_id',1)->first();
        $code = mt_rand(1000, 9999);
                $tomail   = $request->get('email');
                $subject  = $emailTemplateObj->subject;
                $body     = $emailTemplateObj->body;
                $body     =  str_replace('[userName]',$userObj->username,$body);
                $body     =  str_replace('[VerificationCode]',$code,$body);

                
                // $confirm_link = env('FRONT_URL') . '/api/account-activation/' . $code;
                // $data = array("name"=>$userObj->username,"code"=>$code,'message'=>$body,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => 'Religion');
                
                // $res =Mail::send('Api.user.forgotPasswordCode',['data'=>$data],function($message) use ($data){
                //     $message->from($data['from'],$data['emailLabel']);
                //     $message->to($data['to'],$data['name']);
                //     $message->subject($data['subject']);
                // });

                $data = array("name"=>$userObj->username,'body'=>$body,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'FRONT_URL' => url(''));
                
                $res =Mail::send('emails.forgotPass',['data'=>$data],function($message) use ($data){
                    $message->from($data['from'],$data['emailLabel']);
                    $message->to($data['to'],$data['name']);
                    $message->subject($data['subject']);
                });
                $token = md5(rand(111111111, 999999999));
                $userObj->verifycode = $code;
                $userObj->token = $token;
                $userObj->save();
                echo json_encode([
                        'status'    => true,
                        'success'	=> 200,
                        'message'   => Cache::get('sent_code'),
                        'data' => [
                                "token" => $userObj->token
                            ]
                    ]);
        }else{
        	echo json_encode([
	                        'status'    => false,
	                        'error'		=> 401,
	                        'message'   => Cache::get('email_not_register'),
	                    ]);
	            	exit;
        }

    }

    public function verifycode(Request $request){
        $rules = [
            'code'          => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "code.required" => "Code is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $userObj = AppUser::where('verifycode',$request->get('code'))->first();
        if(count($userObj) > 0){
            $userObj->is_verified = '1';
            $userObj->save();
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => Cache::get('verified'),
                    ]);
                exit;
        }else{
            echo json_encode([
                            'status'    => false,
                            'error'     => 401,
                            'message'   => Cache::get('code_invalid'),
                        ]);
                    exit;
        }
    }

    public function resetPassword(Request $request){
        $rules = [
            'email'      => 'required',
            'password'      => 'required|min:6',
        ];

        $requestData = Input::all();

        $message = [
            "email.email" => "Invalid e-mail address",
            "email.required" => "Please provide email",
            "password.required"         => "Password is required",
            "password.min"              => "Password should be six characters long",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $userObj = AppUser::where('email',$request->get('email'))->first();
        if(count($userObj) > 0){
            $token = md5(rand(111111111, 999999999));
            $userObj->token = $token;
            $userObj->password = bcrypt($request->get('password'));
            $userObj->save();
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => Cache::get('password_changed'),
                    ]);
                exit;
        }else{
            echo json_encode([
                            'status'    => false,
                            'error'     => 401,
                            'message'   => Cache::get('email_invalid'),
                        ]);
                    exit;
        }
    }
    public function logout(Request $request){
        $userObj = AppUser::where('id',$request->user_id)->first();
        if(count($userObj) > 0){
            $userObj->is_login = 0;
            $userObj->save();
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'You have logged out successfully',
                    ]);
                exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'     => 401,
                        'message'   => 'User not found',
                    ]);
                exit;
        }
    }

    public function aboutUs(Request $request){

        $rules = [
            'language_id'      => 'required'
        ];
        $requestData = Input::all();
        $message = [
            "language_id.required" => "Please provide language id"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $aboutObj = CMS::where('slug','about_us')->where('language_id',$request->language_id)->where('status',1)->first();
        $data = [];
        if(!empty($aboutObj)){
            $data['title'] = $aboutObj->title;
            $data['description'] = $aboutObj->description;
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'about us',
                        'data' => $data
                    ]);
            exit;
        }else{

            echo json_encode([
                            'status'    => false,
                            'error'     => 401,
                            'message'   => Cache::get('not_found'),
                        ]);
                    exit;
        }
    }

    public function policy(Request $request){

        $rules = [
            'language_id'      => 'required'
        ];
        $requestData = Input::all();
        $message = [
            "language_id.required" => "Please provide language id"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $policyObj = CMS::where('slug','Privacy-policy')->where('language_id',$request->language_id)->where('status',1)->first();
        $data = [];
        if(!empty($policyObj)){
            $data['title'] = $policyObj->title;
            $data['description'] = $policyObj->description;
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'Privacy policy',
                        'data' => $data
                    ]);
            exit;
        }else{

            echo json_encode([
                            'status'    => false,
                            'error'     => 401,
                            'message'   => Cache::get('not_found'),
                        ]);
            exit;
        }
    }
     
    public function help(Request $request){
        $rules = [
            'language_id'      => 'required'
        ];
        $requestData = Input::all();
        $message = [
            "language_id.required" => "Please provide language id"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }
       $faqObj = FAQ::where('status',1)->where('language_id',$request->language_id)->get();
       if(count($faqObj)){

            foreach ($faqObj as $key => $value) {
               $data[$key]['question'] = $value->question;
               $data[$key]['answer'] = $value->answer;
            }
        echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'Help',
                        'data' => $data
                    ]);
        exit;
       }else{
        echo json_encode([
                            'status'    => false,
                            'error'     => 401,
                            'message'   => Cache::get('not_found'),
                        ]);
        exit;
       }
    }

    public function allPages(Request $request){
    $rules = [
            'language_id'      => 'required'
        ];
        $requestData = Input::all();
        $message = [
            "language_id.required" => "Please provide language id"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }
      if($request->language_id == 1){
        $data['aboutus'] = url('aboutusAppEnglish');
        $data['privacy'] = url('privacyAppEnglish');
        $data['help']   = url('helpAppEnglish');
      }else{
        $data['aboutus'] = url('aboutusAppArabic');
        $data['privacy'] = url('privacyAppArabic');
        $data['help'] = url('helpAppArabic');
      }
      echo json_encode([
        'status'    => true,
        'success'   => 200,
        'message'   => 'pages',
        'data' => $data
        ]);
     exit;
  }
}


