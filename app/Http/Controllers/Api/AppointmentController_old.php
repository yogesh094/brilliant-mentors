<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUser;
use App\Models\Appointment;
use App\Models\Topic;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use Cache;
use DB;

header("Content-Type: application/json");
class AppointmentController extends Controller
{
    protected $customerId;
    public function __construct() {
      header("Content-Type: application/json");
      // date_default_timezone_set('Asia/Kolkata');
      $headers = getallheaders();
       // echo "<pre>";print_r($headers);exit;
      $apiToken = isset($headers['apiToken'])?$headers['apiToken']:'';
       if($apiToken != ''){
            $customer = AppUser::where('token',$apiToken)->get()->toArray();
            if(count($customer) > 0){
                $this->customerId = $customer[0]['id'];
            }else{
                echo json_encode([
                        'status'    => false,
                        'error'     => 201,
                        'message'   => 'Invalid token',
                    ]);
                exit;
            }
       }else{
            echo json_encode([
                        'status'    => false,
                        'error'     => 401,
                        'message'   => 'Token is required',
                    ]);
            exit;
       }
    }

    public function bookAppointment(Request $request){
        $rules = [
            'language_id'   => 'required',
            'booking_date'  => 'required',
            'booking_time'  => 'required',
            'title'         => 'required',
            'description'   => 'required',
            'category_id'   => 'required',
            'topic_id'      => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"  => "Please provide language Id",
            "booking_date.required" => "Please provide booking date",
            "booking_time.required" => "Please provide booking time",
            "title.required"        => "Please provide title",
            "description.required"  => "Please provide description",
            "category_id.required"  => "Please provide category Id",
            "topic_id.required"     => "Please provide topic Id",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make($_POST, $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }

        // Get Topic charge 
        $getTopicCharges = Topic::where('id',$request->get('topic_id'))->pluck('charges')->first();


        $booking_time = ($request->get('booking_time') !=null?$request->get('booking_time'):'');
        $time = explode('-',$booking_time);

        // Add Appoinment
        $appoinment = new Appointment;
        $appoinment->language_id = ($request->get('language_id') !=null?$request->get('language_id'):'');
        $appoinment->appuser_id = $this->customerId;
        $appoinment->category_id  = ($request->get('category_id') !=null?$request->get('category_id'):'');
        $appoinment->booking_date = ($request->get('booking_date') !=null?$request->get('booking_date'):'');
        $appoinment->start_time = $time[0];
        $appoinment->end_time = $time[1];
        $appoinment->topic_id = ($request->get('topic_id') !=null?$request->get('topic_id'):'');
        $appoinment->title = ($request->get('title') !=null?$request->get('title'):'');
        $appoinment->description = ($request->get('description') !=null?$request->get('description'):'');

        if($appoinment->save()){
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => Cache::get('appointment_scheduled'),
                        'data'      => [
                                'booking_date'  => ($request->get('booking_date') !=null?$request->get('booking_date'):''),
                                'booking_time'  => ($request->get('booking_time') !=null?$request->get('booking_time'):''),
                                'charges' => $getTopicCharges,
                                'currency' => Cache::get('currency')
                        ]
                    ]);
            exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'     => 401,
                        'message'   => Cache::get('went_wrong'),
                    ]);
            exit;
        }
    
    }
    
    public function getAppointment(Request $request){
         $rules = [
            'language_id'   => 'required',
            'appuser_id'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Language Id is required",
            "appuser_id.required"       => "user id is required",
        ];

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

         $AppointmentObj = Appointment::where('appuser_id',$request->get('appuser_id'))->where('language_id',$request->get('language_id'))->get()->toArray();
         if(count($AppointmentObj) > 0){
            $dataArr = array();
            foreach ($AppointmentObj as $key => $Appointment) {
               $getExpertName = User::where('id',$Appointment['expert_id'])->pluck('username')->first();
               $getTopicName = Topic::where('id',$Appointment['topic_id'])->pluck('topic')->first();
               $dataArr[$key]['id'] = isset($Appointment['id'])?$Appointment['id']:'';
               $dataArr[$key]['expert_name'] = isset($getExpertName)?$getExpertName:'';
               $dataArr[$key]['topic_name'] = isset($getTopicName)?$getTopicName:'';
               $dataArr[$key]['booking_date'] = isset($Appointment['booking_date'])?$Appointment['booking_date']:'';
               $dataArr[$key]['start_time'] = isset($Appointment['start_time'])?$Appointment['start_time']:'';
               $dataArr[$key]['end_time'] = isset($Appointment['end_time'])?$Appointment['end_time']:'';
            }
            echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Get Appoinment List',
                          'data'      => $dataArr
                      ]);
            exit;

         }else{
            echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_appointment'),
                      ]);
            exit;
         }

    }
}
