<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\AppUser;
use App\Models\User;
use App\Models\Question;
use App\Models\Notification;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Cache;

class QuestionController extends Controller
{

	protected $customerId;
    public function __construct() {
      header("Content-Type: application/json");
      //$headers = getallheaders();
      //$headers = app('request');
      $headers['apiToken'] = app('request')->headers->get('apitoken');
      $apiToken = isset($headers['apiToken'])?$headers['apiToken']:'';
       if($apiToken != ''){
            if($apiToken == 'L@titude2018'){
                
            }else{
                $customer = AppUser::where('token',$apiToken)->get()->toArray();
                if(count($customer) > 0){
                $this->customerId = $customer[0]['id'];
                }else{
                echo json_encode([
                        'status'    => false,
                        'error'     => 201,
                        'message'   => 'Invalid token',
                    ]);
                exit;
                }
            }
      
       }else{
          echo json_encode([
                        'status'    => false,
                        'error'   => 401,
                        'message'   => 'Token is required',
                    ]);
          exit;
       }
      // if($apiToken != '' && $apiToken != 'L@titude2018'){
      // $customer = AppUser::where('token',$apiToken)->get()->toArray();
      //     if(count($customer) > 0){
      //       $this->customerId = $customer[0]['id'];
      //     }else{
      //       echo json_encode([
      //                   'status'    => false,
      //                   'error'   => 201,
      //                   'message'   => 'Invalid token',
      //               ]);
      //       exit;
      //     }
      //  }
       // else{
       //    echo json_encode([
       //                  'status'    => false,
       //                  'error'   => 401,
       //                  'message'   => 'Token is required',
       //              ]);
       //    exit;
       // }
    }

    public function addQuestion(Request $request){
      if(isset($request->token) && $request->token == 'L@titude2018'){
        echo json_encode([
                        'status'    => false,
                        'error'   => 401,
                        'message'   => 'Please login first user',
                    ]);
            exit;
      }

    	$rules = [
            'language_id'   => 'required',
            'question'      => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
            "question.required"         => "Question is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

      $questionObj = Question::where('appuser_id',$this->customerId)->where('question',$request->get('question'))->first();

      if(!empty($questionObj)){
        echo json_encode([
                        'status'    => false,
                        'error'   => 401,
                        'message'   => Cache::get('question_already'),
                    ]);
            exit;
      }else{
        $questionObj = new Question;
        $questionObj->language_id = $request->get('language_id');
        $questionObj->appuser_id = $this->customerId;
        $questionObj->question = $request->get('question');

        if ($questionObj->save()) {
          echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => Cache::get('add_question'),
                    ]);
          exit;
        } else {
            echo json_encode([
                        'status'    => false,
                        'error'   => 401,
                        'message'   => Cache::get('went_wrong'),
                    ]);
            exit;
        }
      }
    }

    public function searchQuestion(Request $request){
      $rules = [
            'language_id'     => 'required',
            //'keyword'     => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
            //"keyword.required"          => "Keyword is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }
        $limit = $request->get('limit') != null?$request->get('limit'):5;
       // $page = $request->get('page') != null?$request->get('page') - 1:'0';
        //$start = $page * $limit;
        $start = $request->get('page') != null?$request->get('page') - 1:0;

        $category_id = $request->get('category_id') != null?$request->get('category_id'):'';
        $SubCategory = $request->get('SubCategory') != ''?$request->get('SubCategory'):'';
        
        //$topic_id = $request->get('topic_id') != null?$request->get('topic_id'):'';
        // echo json_encode([
        //                   'status'    => false,
        //                   'error'   => 401,
        //                   'message'   => $categoryArr,
        //               ]);
        //   exit;
        // $words = [];
        // if($request->get('keyword') != ''){
        //   $words = explode(' ',$request->get('keyword'));
        // }
        $words = $request->get('keyword');
        //$search_filter = $request->get('search_filter') != null?$request->get('search_filter'):0;
        $search_filter = $request->get('search_filter');
        $column = "question.question";
        $orderBy = "RAND()";
        if(isset($search_filter) && $search_filter == 0) {
            //$query->orderBy('question.id', 'DESC');
          $column = "question.id";
          $orderBy = "DESC";
          }
                        //is_popular
      if(isset($search_filter) && $search_filter == 1) {
           //$query->orderBy('question.is_popular', 'DESC');
        $column = "question.is_popular";
        $orderBy = "DESC";
        }
        $getData = DB::table('question')
                    ->select(DB::raw('question.*,user.username'))
                    ->leftjoin('user', 'question.expert_id', '=', 'user.id')
                    ->leftjoin('category', 'category.id', '=', 'question.category_id')
                    // ->where('question', 'like','%'.$request->get('keyword').'%')
                    ->orderBy($column, $orderBy)
                    ->where(function($query) use ($words)  {
                      if($words != ''){
                         $query->Where('question', 'like','%'.$words.'%');
                      }
                    //   if(count($words) > 0){
                    //     foreach ($words as $key => $keyword) {
                    //     if(!empty($keyword)){
                    //         if($key == 0){
                    //           $query->where('question', 'like','%'.$keyword.'%');
                    //         }else{
                    //           $query->orWhere('question', 'like','%'.$keyword.'%');
                    //         }
                    //     }
                    //   }
                    // }
                      
                     })
              			->where(function($query) use ($category_id)  {
      			            if(!empty($category_id)) {
                        $category = explode(',', $category_id);
                        $query->whereIn('question.category_id', $category);
      			            }
      			         })
                    // ->Where(function($query) use ($SubCategory)  {
                    //     if(!empty($SubCategory)) {
                    //     $parent_category = explode(',', $SubCategory);
                    //     $query->whereIn('question.parent_category', $parent_category);
                    //     }
                    //  })
                    ->where(function($query) use ($search_filter)  {
                      
                        if(isset($search_filter) && $search_filter == 2) {
                          $query->where('category.is_seasonal', 1);
                        }
                     })
                  	->where('question.language_id',$request->get('language_id'))
                    ->where('question.is_private',0)
                    ->take($limit)->skip($start)
                  	->get();
        $data = array();
        $subCategoryArr = [];
         if($SubCategory != ''){
            $subCategoryArr = explode(',', $SubCategory);
         }
        if(count($getData) > 0){
            foreach ($getData as $key => $resultData) {
              if(!empty($subCategoryArr)){
                if(in_array($resultData->parent_category, $subCategoryArr) || $resultData->parent_category == 0){
                    $data[$key]['id'] = $resultData->id;
                    $data[$key]['category_id'] = isset($resultData->category_id)?$resultData->category_id:'';
                    $data[$key]['expert_id'] = isset($resultData->expert_id)?$resultData->expert_id:'';
                    $data[$key]['expert_name'] = isset($resultData->username)?$resultData->username:'';
                    $data[$key]['question'] = isset($resultData->question)?$resultData->question:'';
                    $data[$key]['answer'] = isset($resultData->answer)?$resultData->answer:'';
                    $data[$key]['date'] = date('Y-m-d H:i:s', strtotime($resultData->updated_at));
                }
              }else{
                  $data[$key]['id'] = $resultData->id;
                  $data[$key]['category_id'] = isset($resultData->category_id)?$resultData->category_id:'';
                  $data[$key]['expert_id'] = isset($resultData->expert_id)?$resultData->expert_id:'';
                  $data[$key]['expert_name'] = isset($resultData->username)?$resultData->username:'';
                  $data[$key]['question'] = isset($resultData->question)?$resultData->question:'';
                  $data[$key]['answer'] = isset($resultData->answer)?$resultData->answer:'';
                  $data[$key]['date'] = date('Y-m-d H:i:s', strtotime($resultData->updated_at));
              }
            
          }
          echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Search Listing',
                          'data'      => array_values($data)
                      ]);
          exit;
        }else{
          echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
          exit;
        }
        
    }

    public function quesionSuggestion(Request $request){
        $rules = [
            'language_id'   => 'required',
            'question'      => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
            "question.required"         => "Question is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }
        $words = explode(' ',$request->get('question'));
        foreach ($words as $key => $keyword) {
             $getData = Question::where('question', 'like','%'.$keyword.'%')
                  ->where('language_id',$request->get('language_id'))
                  ->get();           
        }
        $data = array();
        if(count($getData) > 0){
            foreach ($getData as $key => $resultData) {
              if($resultData->answer != ''){
                $data[$key]['id'] = $resultData->id;
            $data[$key]['expert_id'] = isset($resultData->expert_id)?$resultData->expert_id:'';
            $data[$key]['question'] = isset($resultData->question)?$resultData->question:'';
            $data[$key]['answer'] = isset($resultData->answer)?$resultData->answer:'';
              }
            
          }
          if(!empty($data)){
            echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Suggested Answer',
                          'data'      => array_values($data)
                      ]);
          exit;
        }else{
          echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
          exit;
        }
          
        }else{
          echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
          exit;
        }
        
    }

    public function getAnswer(Request $request){
    	$rules = [
            'language_id'   => 'required',
            'question_id'      => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
            "question_id.required"      => "Question Id is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }
        //$question = Question::find($request->get('question_id'));
        //$question->is_popular = is_popular+1
        DB::table('question')->where('id', $request->get('question_id'))->increment('is_popular');

        $questionObj = Question::where('id',$request->get('question_id'))->where('language_id',$request->get('language_id'))->first();

        if(count($questionObj) > 0){

          $getExpertName = User::where('id',$questionObj->expert_id)->pluck('username')->where('status',1)->first();
        	$data['question'] = isset($questionObj->question)?$questionObj->question:'';
        	$data['answer'] = isset($questionObj->answer)?$questionObj->answer:'';
          $data['expert_name'] = isset($getExpertName)?$getExpertName:'';
          $data['datetime'] = isset($questionObj->updated_at)?$questionObj->updated_at->format('Y-m-d H:m:s'):'';
        	echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Answer',
                          'data'      => $data
                      ]);
       		exit;
        }else{
        	echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
       		exit;
        }

    }

    public function getLatestQuestionData(Request $request){
    	$rules = [
            'language_id'   => 'required',
            // 'limit'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Language Id is required",
            // "limit.required"       => "Limit is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $limit = $request->get('limit') != null?$request->get('limit'):'5';
  	    // $questionObj = Question::where('language_id',$request->get('language_id'))->orderBy('id','desc')->take($limit)->get();
         $questionObj = DB::table('question')
                    ->select(DB::raw('question.*,user.username'))
                    ->leftjoin('user', 'question.expert_id', '=', 'user.id')
                    ->where('question.is_private',0)
                    ->where('question.language_id',$request->get('language_id'))
                    ->orderBy('question.is_popular','desc')
                    ->take($limit)
                    ->get();
        $data = array();
        if(count($questionObj) > 0){
            foreach ($questionObj as $key => $resultData) {
              $data[$key]['id'] = isset($resultData->id)?$resultData->id:'';
              $data[$key]['question'] = isset($resultData->question)?$resultData->question:'';
        	    $data[$key]['answer'] = isset($resultData->answer)?$resultData->answer:'';
          }
          	echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => 'Latest 5 Data',
                          'data'      => $data
                      ]);
          	exit;
        }else{
          	echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_found'),
                      ]);
          	exit;
        }
        
    }

    
    public function getQuestion(Request $request){
    $rules = [
            'language_id'   => 'required',
            'appuser_id'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Language Id is required",
            "appuser_id.required"       => "user id is required",
        ];

        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $questionObj = Question::where('appuser_id',$request->get('appuser_id'))->where('language_id',$request->get('language_id'))->get()->toArray();
        $dataArr = array();
        if(count($questionObj) > 0){

          foreach ($questionObj as $key => $questionVal) {
            $getExpertName = User::where('id',$questionVal['expert_id'])->pluck('username')->where('status',1)->first();
          $dataArr[$key]['id'] = isset($questionVal['id'])?$questionVal['id']:'';
          $dataArr[$key]['question'] = $questionVal['question'];
          $dataArr[$key]['answer'] = $questionVal['answer'];
          $dataArr[$key]['expert_name'] = isset($getExpertName)?$getExpertName:'';
          $dataArr[$key]['date'] = date('Y-m-d H:i:s', strtotime($questionVal['updated_at']));
          // $dataArr[$key]['answer'] = isset($question['answer'])?$question['answer']:'';

          }
          echo json_encode([
                          'status'    => true,
                          'success'   => 200,
                          'message'   => Cache::get('list_question'),
                          'data'      => $dataArr
                      ]);
            exit;
        }else{
          echo json_encode([
                          'status'    => false,
                          'error'   => 401,
                          'message'   => Cache::get('not_question'),
                      ]);
            exit;
        }
        
  }
}