<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\AppUser;
use App\Models\Language;
use App\Models\Category;
use App\Models\CategoryAppointment;
use App\Models\Topic;
use App\Models\Slider;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\LanguageKeyword;
use App\Models\Appointment;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Cache;

class GeneralController extends Controller
{

	protected $customerId;
    public function __construct() {
        header("Content-Type: application/json");
        //$headers = getallheaders();
        //$headers = app('request');
        $headers['apiToken'] = app('request')->headers->get('apitoken');
        $apiToken = isset($headers['apiToken'])?$headers['apiToken']:'';
        
       if($apiToken != ''){
            if($apiToken == 'L@titude2018'){
                
            }else{
                $customer = AppUser::where('token',$apiToken)->get()->toArray();
                if(count($customer) > 0){
                $this->customerId = $customer[0]['id'];
                }else{
                echo json_encode([
                        'status'    => false,
                        'error'     => 201,
                        'message'   => 'Invalid token',
                    ]);
                exit;
                }
            }
			
       }else{
       		echo json_encode([
                        'status'    => false,
                        'error'		=> 401,
                        'message'   => 'Token is required',
                    ]);
       		exit;
       }
   	}

    public function getLanguage(Request $request){
        $languageList = Language::all();
        $data = array();
        if(count($languageList) > 0){
            foreach ($languageList as $key => $language) {
                $data[$key]['id'] = $language->id;
                $data[$key]['name'] = $language->name;
                $data[$key]['code'] = $language->code;
                $data[$key]['status'] = $language->status;
            }
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'Language Listing',
                        'data'      => $data
                    ]);
            exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'       => 401,
                        'message'   => 'Data not found',
                    ]);
            exit;
        }
    }

    public function setLanguage(Request $request){
        $rules = [
            'language_id' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Please provide email",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }
        $userObj = AppUser::where('id',$this->customerId)->first();
        if(count($userObj) > 0){
            $userObj->language_id = $request->get('language_id');
            $userObj->save();

            echo json_encode([
                        'status'        => true,
                        'success'       => 200,
                        'language_id'   => $request->get('language_id'),
                        'message'       => 'User language is changed',
                    ]);
                exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'     => 401,
                        'message'   => 'User not found',
                    ]);
                exit;
        }
    }

    public function getLanguageKeyword(Request $request){
        $rules = [
            'language_id' => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required" => "Please provide email",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }

        $getLanguageKeyword =  LanguageKeyword::where('language_id',$request->get('language_id'))->get();

        if(count($getLanguageKeyword) > 0){
            $keywordData = array();
            foreach ($getLanguageKeyword as $key => $languageKeyword) {
                $keywordData[$key]['keyword'] = $languageKeyword->keyword;
                $keywordData[$key]['label'] = $languageKeyword->label;
            }
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'Keyword Listing',
                        'data'      => $keywordData
                    ]);
            exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'       => 401,
                        'message'   => 'Data not found',
                    ]);
            exit;
        }
    }

    public function getCategory(Request $request){
        $rules = [
            'language_id'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }
        $catData = array();
        $categoryList = Category::where('language_id',$request->get('language_id'))->where('is_parent',0)->get();

        if(count($categoryList) > 0){
            foreach ($categoryList as $key => $cat) {
                $categoryObj = Category::where('language_id',$request->get('language_id'))->where('is_parent',$cat->id)->get();
                $catData[$key]['id'] = $cat->id;
                $catData[$key]['name'] = $cat->category;
                $parentArr = array();
                foreach ($categoryObj as $per => $parent) {
                    $parentArr[$per]['id'] = $parent->id;
                    $parentArr[$per]['name'] = $parent->category;
                }
                $catData[$key]['sub_category'] = $parentArr;
            }
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'Category Listing',
                        'data'      => $catData,
                    ]);
            exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'       => 401,
                        'message'   => 'Data not found',
                    ]);
            exit;
        }
    }

    public function getCategoryAppointment(Request $request){
        $rules = [
            'language_id'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }
        $catData = array();
        $categoryList = CategoryAppointment::where('language_id',$request->get('language_id'))->get();

        if(count($categoryList) > 0){
            foreach ($categoryList as $key => $cat) {
                $catData[$key]['id'] = $cat->id;
                $catData[$key]['name'] = $cat->category;
                $catData[$key]['charges'] = $cat->charges;
                $catData[$key]['currency'] = Cache::get('currency');
            }
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'Category Listing',
                        'data'      => $catData,
                    ]);
            exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'       => 401,
                        'message'   => 'Data not found',
                    ]);
            exit;
        }
    }

    public function getTopics(Request $request){
        $rules = [
            'language_id'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "language_id.required"      => "Language Id is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }
        $topicData = array();
        $topicList = Topic::where('language_id',$request->get('language_id'))->get();

        if(count($topicList) > 0){
            foreach ($topicList as $key => $topic) {
                $topicData[$key]['id'] = $topic->id;
                $topicData[$key]['name'] = $topic->topic;
            }
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'Topic Listing',
                        'data'      => $topicData
                    ]);
            exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'       => 401,
                        'message'   => 'Data not found',
                    ]);
            exit;
        }
    }
   	
    

    public function getSlider(Request $request)
    {
        $rules = [
            'language_id'   => 'required',
            'type_id'       => 'required', //   1=>header_slider,2=>content_slider
        ];
        $message = [
            "language_id.required"  => "Language Id is required",
            "type_id.required"      => "Type Id is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }


        $sliderObj = Slider::where('type',$request->get('type_id'))->where('language_id',$request->get('language_id'))->get();

        $sliderData = array();
        if(count($sliderObj) > 0){
            foreach ($sliderObj as $key => $slider) {
                // $sliderData[$key]['id'] = $slider->id;
                // $sliderData[$key]['name'] = $slider->name;
                // $sliderData[$key]['image_name'] = $slider->image_name;
                $sliderData[$key]['image_path'] = url('/')."/public".$slider->image_path;
                $sliderData[$key]['title'] = $slider->title;
                $sliderData[$key]['description'] = $slider->description;
                // $sliderData[$key]['status'] = $slider->status;
            }
            echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'Slider Listing',
                        'data'      => $sliderData
                    ]);
            exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'       => 401,
                        'message'   => 'Data not found',
                    ]);
            exit;
        }
    }

    public function setting(Request $request){
    	
    	$data = array();
    	$data['currency'] = Cache::get('currency');
    	$data['start_time'] = Cache::get('start_time');
    	$data['end_time'] = Cache::get('end_time');
    	$data['time_difference'] = Cache::get('time_difference');
    	echo json_encode([
                        'status'    => true,
                        'success'	=> 200,
                        'message'   => 'Setting List',
                        'data'		=> $data
                    ]);
            	exit;
    }

    public function changePassword(Request $request){

       $rules = [
            'email'         => 'required',
            'language_id'         => 'required',
            //'current_password'      => 'min:6',
            //'new_password'      => 'min:6',
            'confirm_password' => 'same:new_password'
        ];

        $requestData = Input::all();

        $message = [
            "email.email" => "Invalid e-mail address",
            "email.required" => "Please provide email",
            "language_id.required" => "Language id required",
            //"new_password.required"         => "Password is required",
            "new_password.min"              => "Password should be six characters long",
            //"current_password.required"         => "Password is required",
            "current_password.min"              => "Password should be six characters long",
            //"confirm_password.required"    => "Confirm Password is required",   
            "confirm_password.same"        => "Confirm Password and New Password do not match.",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $userObj = AppUser::where('email',$request->get('email'))->where('is_verified',1)->first();
        if(count($userObj) > 0){
            $userObj = AppUser::find($userObj->id);
             if($request->get('username') != ''){
               $userObj->username =  $request->get('username');
            }
            if($request->file('profile_image') != ''){

              //   $destination_path_thumb_image = public_path()."/uploads/profile/";
              //    $thumbFile = $request->file('profile_image');
              //    $thumbFileName = $thumbFile->getClientOriginalName();
              //    if(file_exists($destination_path_thumb_image."/".$thumbFileName)){
              //   $thumbFileName = pathinfo($thumbFileName, PATHINFO_FILENAME)."_".time().".".$thumbFile->getClientOriginalExtension();
              // }
              //  $thumbFile->move($destination_path_thumb_image, $thumbFileName);


               
                 $file = $request->file('profile_image');
                 $ImagesFileName = $file->getClientOriginalName();
                 $ext = $file->getClientOriginalExtension();
                 $getRealPath = $file->getRealPath();
                 $getSize = $file->getSize();
                 $getMimeType = $file->getMimeType();
                 $without_ext = preg_replace('/\\.[^.\\s]{3,4}$/', '', $ImagesFileName);
                 $ImagesFileName = str_slug(strtolower($without_ext)).'-'.time().'.'.$ext;
                 $destinationPath = public_path()."/uploads/profile/";
                 $file->move($destinationPath,$ImagesFileName);

                $userObj->profile_image          =  $ImagesFileName;
                $userObj->profile_path           =  $destinationPath;
                $userObj->profile_url            =  url('/public')."/uploads/profile/".$ImagesFileName;
                //url('/public').$albumData->url."/".$albumData->name
                }
            if($request->get('current_password') == ""){
              if($userObj->save()){
                $data['user_id'] = $userObj->id;
                $data['profile_image'] = $userObj->profile_image;
                $data['profile_path'] = $userObj->profile_path;
                $data['profile_url'] = $userObj->profile_url;
                echo json_encode([
                        'status'    => true,
                        'success'     => 200,
                        'message'   => Cache::get('profile_change')
                    ]);
                exit;
                }else{
                    echo json_encode([
                            'status'    => false,
                            'error'     => 401,
                            'message'   => Cache::get('went_wrong'),
                        ]);
                    exit;
                }
            }
            
            if($request->get('current_password') != ''){
                $validCredentials = Hash::check($request->get('current_password'), $userObj->password);
                if($validCredentials){
                 if($request->get('new_password') != ''){
                       $userObj->password = bcrypt($request->get('new_password'));
                        $userObj->save();
                        echo json_encode([
                        'status'    => true,
                        'success'     => 200,
                        'message'   => Cache::get('profile_change'),
                    ]);
                        exit; 
                    }else{
                       echo json_encode([
                                    'status'    => false,
                                    'error'     => 401,
                                    'message'   => Cache::get('enter_password'),
                                ]);
                        exit; 
                    }
                
            }else{
                echo json_encode([
                                    'status'    => false,
                                    'error'     => 401,
                                    'message'   => Cache::get('invalid_pass'),
                                ]);
                        exit;
            }
            
            }
            


            
        }else{
            echo json_encode([
                            'status'    => false,
                            'error'     => 401,
                            'message'   => Cache::get('email_not_register'),
                        ]);
                    exit;
        }
    }

    public function getProfile(Request $request){

        $rules = [
            'user_id'   => 'required',
        ];

        $requestData = Input::all();

        $message = [
            "user_id.required"      => "user  Id is required",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

        $AppUser = AppUser::where('id',$request->user_id)->get()->first();
        if(!empty($AppUser)){
            $profileImage = "";
            if($AppUser->profile_image != ''){
                $profileImage = url('/')."/public/uploads/profile/".$AppUser->profile_image;
            }
            $Data['profile_image'] = $profileImage;
            $Data['username'] = $AppUser->username;
                //$sliderData[$key]['description'] = $slider->description;
                echo json_encode([
                        'status'    => true,
                        'success'   => 200,
                        'message'   => 'Get Profile',
                        'data'      => $Data
                    ]);
            exit;
        }else{
            echo json_encode([
                        'status'    => false,
                        'error'       => 401,
                        'message'   => 'Data not found',
                    ]);
            exit;
        }
        
    }

    public function getBookingTime(Request $request){

        $rules = [
            'booking_date'   => 'required',
        ];
        $requestData = Input::all();
        $message = [
            "booking_date.required"      => "booking date is required",
        ];
        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }

     //$appointmentObj = Appointment::where('booking_date',$request->get('booking_date'))->get()->toArray();
    $appointmentObj = DB::table('appointment')
                      ->select('appointment.*')
                      ->join('payment_order', 'payment_order.appointment_id', '=', 'appointment.id')
                      ->where('payment_order.status', 'succeeded')
                      ->where('appointment.booking_date', $request->get('booking_date'))
                      ->get()->toArray();
    
      $User = User::whereNotIn('user_role', [1])->where('status',1)->get();

      $data = [];
      if(count($appointmentObj) > 0){

        if(count($User) <= count($appointmentObj)){
            foreach ($appointmentObj as $key => $value) {

            $data[$key] = Carbon::parse($value->start_time)->format('h:i a').'-'.Carbon::parse($value->end_time)->format('h:i a');

            }
        }
        $data1 = [];
        if($this->customerId > 0){
           // $data['gettimw'] = 'gettimw';
         $appointmentUserObj = DB::table('appointment')
                      ->select('appointment.*')
                      ->where('appointment.booking_date', $request->get('booking_date'))
                      ->where('appointment.appuser_id', $this->customerId)
                      ->get()->toArray();
        foreach ($appointmentUserObj as $key => $value) {

            $data1[$key] = Carbon::parse($value->start_time)->format('h:i a').'-'.Carbon::parse($value->end_time)->format('h:i a');

            }
        }
        $timeArr = array_values(array_unique(array_merge($data,$data1)));
        echo json_encode([
                    'status'    => true,
                    'success'     => 200,
                    'message'   => 'time listing',
                    'data'      => $timeArr
                ]);
        exit;
    
     }else{
        echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => 'No data',
                ]);
        exit;
      }
      
  // $data = [];
  //     if(count($appointmentObj) > 0){
        
  //       // foreach ($appointmentObj as $key => $value) {
  //       // $data[$key] = Carbon::parse($value['start_time'])->format('h:i a').'-'.Carbon::parse($value['end_time'])->format('h:i a');
  //       // }
  //       foreach ($appointmentObj as $key => $value) {
  //       $data[$key] = Carbon::parse($value->start_time)->format('h:i a').'-'.Carbon::parse($value->end_time)->format('h:i a');
  //       }
        
  //     }else{
  //       echo json_encode([
  //                   'status'    => false,
  //                   'error'      => 401,
  //                   'message'   => 'No data',
  //               ]);
  //       exit;
  //     }
      

 
    }
}
