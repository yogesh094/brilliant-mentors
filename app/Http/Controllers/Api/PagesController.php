<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\AppUser;
use App\Models\User;
use App\Models\Question;
use App\Models\Notification;
use App\Models\CMS;
use Illuminate\Support\Facades\Hash;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Mail;
use DB;
use Cache;

class PagesController extends Controller
{

  public function __construct() {
        header("Content-Type: application/json");
        //$headers = app('request');
        $headers['apiToken'] = app('request')->headers->get('apitoken');
    }

  public function allPages(Request $request){
    $rules = [
            'language_id'      => 'required'
        ];
        $requestData = Input::all();
        $message = [
            "language_id.required" => "Please provide language id"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
            
        }
         $askquestionObj = CMS::where('slug','ask_question')->where('language_id',$request->language_id)->where('status',1)->first();
        $data = [];
        
      if($request->language_id == 1){
        $data['aboutus'] = url('aboutusAppEnglish');
        $data['privacy'] = url('privacyAppEnglish');
        $data['help']   = url('helpAppEnglish');
        $data['contact'] = url('contactAppEnglish');
        $data['ask_question_title'] = $askquestionObj->title;
        $data['ask_question_description'] = $askquestionObj->description;
      }else{
        $data['aboutus'] = url('aboutusAppArabic');
        $data['privacy'] = url('privacyAppArabic');
        $data['help'] = url('helpAppArabic');
        $data['contact'] = url('contactAppArabic');
        $data['ask_question_title'] = $askquestionObj->title;
        $data['ask_question_description'] = $askquestionObj->description;
      }
      echo json_encode([
        'status'    => true,
        'success'   => 200,
        'message'   => 'pages',
        'data' => $data
        ]);
     exit;
  }

    public function TermCondition(Request $request){

        $rules = [
            'language_id'      => 'required'
        ];
        $requestData = Input::all();
        $message = [
            "language_id.required" => "Please provide language id"
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            echo json_encode([
                    'status'    => false,
                    'error'      => 401,
                    'message'   => $validator->messages()->first(),
                ]);
                exit;
        }
        $askTermConditionObj = CMS::where('slug','term-condition')->where('language_id',$request->language_id)->where('status',1)->first();
        if(!empty($askTermConditionObj)){
           $data['term_condition_title'] = $askTermConditionObj->title;
           $data['term_condition_description'] = $askTermConditionObj->description;
            echo json_encode([
                'status'    => true,
                'success'   => 200,
                'message'   => 'term condition',
                'data' => $data
                ]);
            exit; 
        }else{
            echo json_encode([
                'status'    => false,
                'error'   => 401,
                'message'   => 'no data found'
                ]);
            exit; 
        }
        
    }
    public function test(){
        $column = "question.id";
        $orderBy = "DESC";
        $words = "";
        $category_id = '20,16';
        $SubCategory = '21';
        $limit = 20;
        $start = 0;
        $search_filter = 0;
        $getData = DB::table('question')
                    ->select(DB::raw('question.*,user.username'))
                    ->leftjoin('user', 'question.expert_id', '=', 'user.id')
                    ->leftjoin('category', 'category.id', '=', 'question.category_id')
                    ->orderBy($column, $orderBy)
                    ->where(function($query) use ($words)  {
                      if($words != ''){
                         $query->Where('question', 'like','%'.$words.'%');
                      }
                      
                     })
                    ->where(function($query) use ($category_id)  {
                            if(!empty($category_id)) {
                        $category = explode(',', $category_id);
                        $query->whereIn('question.category_id', $category);
                            }
                         })
                    // ->Where(function($query) use ($SubCategory)  {
                    //     if(!empty($SubCategory)) {
                    //     $parent_category = explode(',', $SubCategory);
                    //     $query->whereIn('question.parent_category', $parent_category);
                    //     }
                    //  })
                    ->where(function($query) use ($search_filter)  {
                      
                        if(isset($search_filter) && $search_filter == 2) {
                          $query->where('category.is_seasonal', 1);
                        }
                     })
                    ->where('question.language_id',1)
                    ->where('question.is_private',0)
                    
                    ->take($limit)->skip($start)

                    ->get()->toArray();
         $Arr = [];
         if($SubCategory != ''){
            $Arr = explode(',', $SubCategory);
         }
         
         
         $data = [];
         foreach ($getData as $key => $resultData) {
            if(!empty($Arr)){
                if(in_array($resultData->parent_category, $Arr) || $resultData->parent_category == 0){
                $data[$key]['id'] = $resultData->id;
                $data[$key]['category_id'] = isset($resultData->category_id)?$resultData->category_id:'';
                $data[$key]['SubCategory'] = isset($resultData->parent_category)?$resultData->parent_category:'';
                $data[$key]['expert_id'] = isset($resultData->expert_id)?$resultData->expert_id:'';
                $data[$key]['expert_name'] = isset($resultData->username)?$resultData->username:'';
                $data[$key]['question'] = isset($resultData->question)?$resultData->question:'';
            //$data[$key]['answer'] = isset($resultData->answer)?$resultData->answer:'';
                $data[$key]['date'] = date('Y-m-d H:i:s', strtotime($resultData->updated_at));
           }
        }else{
            $data[$key]['id'] = $resultData->id;
                $data[$key]['category_id'] = isset($resultData->category_id)?$resultData->category_id:'';
                $data[$key]['SubCategory'] = isset($resultData->parent_category)?$resultData->parent_category:'';
                $data[$key]['expert_id'] = isset($resultData->expert_id)?$resultData->expert_id:'';
                $data[$key]['expert_name'] = isset($resultData->username)?$resultData->username:'';
                $data[$key]['question'] = isset($resultData->question)?$resultData->question:'';
            //$data[$key]['answer'] = isset($resultData->answer)?$resultData->answer:'';
                $data[$key]['date'] = date('Y-m-d H:i:s', strtotime($resultData->updated_at));
        }
            
         
         }
        echo '<pre>';
        print_r($data);
    }
}