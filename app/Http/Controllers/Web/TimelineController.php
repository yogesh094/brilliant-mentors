<?php
namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Cms;
use App\Models\Notification;
use Illuminate\Support\Facades\Hash;
use App\Models\Language;
use App\Models\LanguageKeyword;
use Illuminate\Support\Facades\Session;
use App\Models\AppUser;
use App\Models\Question;
use App\Models\Setting;
use App\Models\Slider;
use Illuminate\Support\Facades\Mail;
use App\Models\Message;
use App\Library\webGeneral;
use App\Models\EmailTemplate;
use App\Models\Employment;
use App\Models\PostMedia;
use App\Models\Post;
use App\Models\Education;
use DateTime;
use DB;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Cache;

class TimelineController extends Controller {

    public function __construct() {
    }

    public function index(){

    	$user_id = Auth::user()->id;
        $user_type = Auth::user()->user_type;
        $header_image_upload_popup = '';
        $header_image_upload_popup = (string) view('web.master_layout.header-image-upload-popup');
        return view('web.news-feed',compact('header_image_upload_popup','user_id','user_type'));
    }

    public function savePost(Request $request){
        
        $data = $request->all();

        $userObj = new Post;
        $userObj->description = $data['description'];
        $userObj->user_type = $data['user_type'];
        $userObj->user_id = $data['user_id'];
        $userObj->save();
    }

}