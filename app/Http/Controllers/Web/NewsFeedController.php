<?php

namespace App\Http\Controllers\Web;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Collective\Html\Eloquent\FormAccessible;

use App\Models\User;

use App\Models\Cms;

use App\Models\Notification;

use Illuminate\Support\Facades\Hash;

use App\Models\Language;

use App\Models\LanguageKeyword;

use Illuminate\Support\Facades\Session;

use App\Models\AppUser;

use App\Models\Question;

use App\Models\Setting;

use App\Models\Slider;

use Illuminate\Support\Facades\Mail;

use App\Models\Message;

use App\Library\webGeneral;

use App\Models\EmailTemplate;

use App\Models\Employment;

use App\Models\PostMedia;

use App\Models\PostLike;

use App\Models\PostReply;

use App\Models\PostComment;

use App\Models\Post;

use App\Models\Education;

use DateTime;

use DB;

use Html;

use File;

use Input;

use Validator;

use Redirect;

use View;

use Storage;

use Auth;

use Cache;



class NewsFeedController extends Controller {



    public function __construct() {

    }



    public function index(){



    	$user_id = Auth::user()->id;

        $user_type = Auth::user()->user_type;

        $post_view = '';

        $postList = Post::getList();



        if(count($postList) > 0){

            $postList = $postList->toArray();



            foreach ($postList as $key => $value) {

                $comment_view = '';

                 //                $comments = App\Post::find(1)->comments;

                $commentCount = Post::find($value['id'])->comments()->get()->count();

                $likeCount = Post::find($value['id'])->likes()->get()->count();

                $media = Post::find($value['id'])->media()->get();

                $commentList = PostComment::getList($value['id']);



                if(count($commentList) > 0){

                    foreach ($commentList as $key => $comment) {

                        $comment_view .= (string) view('web.post.comment_block',compact('comment'));

                    }

                    

                }



                if(count($media) > 0){

                    $post_view .= (string) view('web.post.no_media');

                }else{

                    $post_view .= (string) view('web.post.no_media',compact('value','commentCount','likeCount','commentList','comment_view')); 

                }



            //            $response['total'] = $campaignList->total();

            //            $response['perPage'] = $campaignList->perPage();

            //            $response['currentPage'] = $campaignList->currentPage();

            //            $response['lastPage'] = $campaignList->lastPage();

                            // echo $post_view;exit;



                           

                            // // foreach ($comments as $comment) {

                            // //     //comment logic

                            // // }

                 

                    





            }



        }



        $header_image_upload_popup = '';

        $header_image_upload_popup = (string) view('web.master_layout.header-image-upload-popup');

        return view('web.post.post_block',compact('header_image_upload_popup','user_id','user_type','post_view'));

    }



    public function savePost(Request $request){

        

        $data = $request->all();

        $commentCount = 0;

        $likeCount = 0;

        $comment_view = '';

        $userObj = new Post;

        $userObj->description = $data['description'];

        $userObj->user_type = $data['user_type'];

        $userObj->user_id = $data['user_id'];

        $userObj->save();

        

        $postData = Post::getSinglePost($userObj->id);



        $response['post_view'] = (string) view('web.post.no_media',['value' => $postData,'commentCount' => $commentCount,'likeCount' => $likeCount,'comment_view' => $comment_view ]);

        $response['status'] = 200;

        return response()->json($response);

        



    }



}