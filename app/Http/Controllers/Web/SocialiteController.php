<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\AuthenticateUser;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Route;
use App\Extensions\MongoSessionStore;
use Illuminate\Support\ServiceProvider;
use Illuminate\Session\Middleware\StartSession;
use Session;
use Socialite;
use Auth;
use DB;
use Validator;
use Redirect;
use View;
use Hash;
use App\Models\AppUser;
//for mail
use Illuminate\Support\Facades\Mail;
// use Mail;
// use App\Model\AdminRole;

class SocialiteController extends Controller {
    
    public function __construct()
    {
        $this->middleware('guest');

    }

    public function redirectToGoogle()
    { 
        return Socialite::driver('google')->redirect();
    }


    public function handleGoogleCallback(Request $request )
    {

        try {
           
            $usersocialite = Socialite::driver('google')->stateless()->user();
            //echo '<pre>';
            //print_r($usersocialite);
           return SocialiteController::socialiteCreateUser($usersocialite,'google');
                        
        } catch (Exception $e) {
            return redirect('/');
        }
    }

     public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {
        try {
            $usersocialite = Socialite::driver('facebook')->stateless()->user();
            //echo '<pre>';
            //print_r($usersocialite);
           return SocialiteController::socialiteCreateUser($usersocialite,'facebook');
        exit;
            
            
        } catch (Exception $e) {
            return redirect('/');
        }
    }

    

    public static function socialiteCreateUser($usersocialite = 0,$socialiteDriver = ''){

        $user = AppUser::where('email', $usersocialite->email)
                    //->where('is_verified', 1)->where($socialiteId, $usersocialite->id)->where('is_login', $is_login)
                    ->first();
        $random = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $token = md5(rand(111111111, 999999999));
                if(!empty($user)){
                    $AppUser  = AppUser::find($user->id);
                    $AppUser->updated_at = date("Y-m-d H:i:s");
                    if(isset($user->room) && $user->room == null){
                        $AppUser->room = $random;
                    }
                }
                else{
                    $AppUser = new AppUser;
                    $AppUser->room = $random;
                    $AppUser->created_at = date("Y-m-d H:i:s");
                }
                
                    if($socialiteDriver == 'facebook'){
                        $AppUser->facebook_id = $usersocialite->id;
                        $AppUser->login_type = 1;
                    }elseif ($socialiteDriver == 'google') {
                        $AppUser->google_id = $usersocialite->id;
                        $AppUser->login_type = 2;
                    }
               
                $AppUser->email = $usersocialite->email;
                $AppUser->username = $usersocialite->name;
                $AppUser->remember_token = $usersocialite->token;
                $AppUser->is_verified = 1;
                $AppUser->token = $token;
                $AppUser->is_login = 1;
                $AppUser->save();
                Auth::loginUsingId($AppUser->id);
                //return redirect('/home');
                
                if(!empty(Session::get('currentUrl'))){
                    return redirect(Session::get('currentUrl'));
                }else{
                return Redirect::back();    
                }
                
                // if(Request::is('dashboard')){
                //     return redirect('/');
                // }else{
                //     return Redirect::back();
                // }
               

            //$id =   $customer->insertGetId($userData);
            //Auth::loginUsingId($id);
                

    }

   }
