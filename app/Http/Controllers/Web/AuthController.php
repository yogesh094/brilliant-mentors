<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\AuthenticateUser;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Tests\Session\Flash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Auth;
use DB;
use Validator;
use Redirect;
use View;
use App\Models\User;
use App\Models\AppUser;
use App\Models\EmailTemplate;
use Hash;
use Socialite;
use Illuminate\Support\Facades\Mail;
use Cache;

// use App\Model\AdminRole;

class AuthController extends Controller {

    use AuthenticatesUsers;

    protected function validator(array $data) {
        return Validator::make($data, [
                    // 'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',

        ]);
    }

    public function __construct() {
        $this->middleware('auth', ['except' => 'logout']);
    }

    //return admin login view page
    public function index() {
        return View::make('Login.login');
    }

    //admin login
    public function loginAuth(Request $request) {

        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ];
        $message = [
            "email.required" => "Please enter email",
            "email.email" => "Valid email address required",
            "password.required" => "Please enter password",
            "password.min" => "Password should be minimum 6 character length"
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'error' => 401,
                        'message' => $validator->messages()->first(),
            ]);
            exit;
        }

        $user = AppUser::where('email', $request->get('email'))->first();
        if (!empty($user)) {
            $userverification = AppUser::where('email', $request->get('email'))->where("is_verified", 1)->first();
            if (empty($userverification)) {
                return Redirect::to('login')->with('error', 'Please complete your verification process.');
                exit;
            } else {

                $validCredentials = Hash::check($request->get('password'), $user->password);
                if ($validCredentials) {
                    $user = AppUser::where('email', $request->email)->first();
                    $random = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

                    // if (isset($user->room) && $user->room == null) {
                    //     $user->room = $random;
                    // }
                    //$user->login_type = 0;
                    $user->save();
                    $auth = Auth::attempt(
                                    array(
                                        'email' => $request->get('email'),
                                        'password' => $request->get('password'),
                    ));
                    if ($auth) {
                        return Redirect::route('profile');
                    }
                } else {
                    return Redirect::to('login')->with('error', 'Invalid Login Credentials.');
                    exit;
                }
            }
        } else {
            return Redirect::to('login')->with('error', 'Email Not registered with us.');
            exit;
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function appUserCreate(Request $request) {

        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:password',
          
        ];
        $message = [
            "email.required" => "Please enter email",
            "email.email" => "Valid email address required",
            "password.required" => "Please enter password",
            "password.min" => "Password should be minimum 6 character length",
            "confirm_password.required" => "Confirm password is required",
            "confirm_password.min" => "Confirm password should be minimum 6 character length",
            "confirm_password.same" => "Confirm password should be same as password",
            // "phone_number.required" => "Please enter phone number",
            // "first_name.required" => "Please enter first name",
            // "last_name.required" => "Please enter last name",
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'error' => 401,
                        'message' => $validator->messages()->first(),
            ]);
            exit;
        }

        $token = md5(rand(111111111, 999999999));

        $room = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

        $user = AppUser::where('email', $request->email)->first();
        if (!empty($user) && $user->is_verified == 1) {
            return response()->json([
                        'status' => false,
                        'error' => 401,
                        'message' => Session::get('message_email_already')]);
            exit;
        } else {



            if (!empty($user)) {
                $AppUser = AppUser::find($user->id);
                $AppUser->remember_token = $token;
            } else {
                $AppUser = new AppUser;
                $AppUser->remember_token = $token;
                //$AppUser->room = $room;
            }

            //selectedLang
            //$username = explode('@', $request->email);
            $AppUser->email = $request->email;
            //$AppUser->username = $username[0];
            $AppUser->password = Hash::make($request->password);
            $AppUser->phone_number = $request->phone_number;
            $AppUser->user_type = $request->user_type;
            $AppUser->first_name = $request->first_name;
            $AppUser->last_name = $request->last_name;
            $AppUser->gender = $request->gender;

            //currently for direct login
            $AppUser->is_verified = 1;

            // if (session::get('selectedLang') != '') {
            //     $AppUser->language_id = session::get('selectedLang');
            // }
            if ($AppUser->save()) {
                $user = AppUser::where('email', $request->email)->first();
                $token = $user->remember_token;
                // if (Session::get('selectedLang') == 2) {
                //     $emailTemplateObj = EmailTemplate::where('id', 5)->where('language_id', 2)->first();
                // } else {
                //     $emailTemplateObj = EmailTemplate::where('id', 4)->where('language_id', 1)->first();
                // }

                // $confirm_link = env('FRONT_URL') . '/account-activation/' . $user->token;

                // $tomail = $user->email;
                // $subject = $emailTemplateObj->subject;
                // $body = $emailTemplateObj->body;
                // $body = str_replace('[confirm_link]', $confirm_link, $body);

                // $data = array("name" =s> "Dear Friend", "confirm_link" => $confirm_link, 'body' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'FRONT_URL' => url(''));
                // $userEmail = new EmailVerify;

                // $userEmail->user_id = $user->uid;
                // $userEmail->email = $user->email;
                // $userEmail->created_at = date('Y-m-d h:i:s');
                // $userEmail->link = $confirm_link;

                // $res = Mail::send('emails.signup', ['data' => $data], function($message) use ($data) {
                //             $message->from($data['from'], $data['emailLabel']);
                //             $message->to($data['to'], $data['name']);
                //             $message->subject($data['subject']);
                //         });
                return Redirect::to('login')->with('success', 'You are successfully registered with us. Enjoy!!');
                return response()->json([
                            'status' => true,
                            'success' => 200,
                            'message' => Session::get('message_register_verification_message'),
                ]);
                exit;
            } else {

                return response()->json(['status' => false, 'error' => 401, 'message' => Session::get('message_went_wrong')]);
                exit;
            }
        }
    }

    protected function create(array $data) {

        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    public function activateRegisterAccount(Request $request) {

        $token = collect(request()->segments())->last();
        $user = AppUser::where("token", $token)->get()->first();
        if (!empty($user) && $user->is_verified == 0) {
            if (!empty($user)) {
                $AppUser = AppUser::find($user->id);
                $AppUser->status = 1;
                $AppUser->is_verified = 1;
                if ($AppUser->save()) {
                    return Redirect::to('/activateRegisterAccount')->with('message', 'Your account successfully activated.');
                } else {
                    return Redirect::to('/activateRegisterAccount')->with('message', 'Something went wrong please try again.');
                }
            }
        } else {
            return Redirect::to('/activateRegisterAccount')->with('message', 'Your account activation link was expired.');
        }
    }

    public function activateRegisterAccountView() {
        return view('web.user.accountConfirm');
    }

    public function getLogout() {
        //Auth::logout();
        Auth::guard('panel')->logout();
        Session::flush();
        return redirect('admin')->with('success', 'You are logged out.');
    }

    public function logoutPayer() {
        Auth::guard('payer')->logout();
        Session::flush();
        return redirect('payer/login')->with('success', 'You are logged out.');
    }

    public function adminLogout(Request $request) {
        Auth::logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route('login'));
    }

    public function logout(Request $request) {
        Auth::logout();
        //$request->session()->flush();
        return redirect('/');
    }

    public function profile(Request $request) {
        return view('web.profile');
    }

    public function storeProfile(Request $request) {
        if ($request->get('confirm_password') != '') {
            $rules = [
                //'current_password'  => 'min:6',
                //'new_password'      => 'min:6',
                'confirm_password' => 'same:new_password'
            ];

            $requestData = Input::all();

            $message = [
                "new_password.required" => "Password is required",
                "new_password.min" => "Password should be six characters long",
                "current_password.required" => "Password is required",
                "current_password.min" => "Password should be six characters long",
                "confirm_password.required" => "Confirm Password is required",
                "confirm_password.same" => "Confirm Password and New Password do not match.",
            ];

            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $message);

            if ($validator->fails()) {
                return Redirect::back()
                                ->withErrors($validator)
                                ->withInput();
            }
        }
        $userObj = AppUser::where('id', Auth::user()->id)->where('is_verified', 1)->first();
        if (count($userObj) > 0) {
            $userObj = AppUser::find($userObj->id);
            if ($request->get('username') != '') {
                $userObj->username = $request->get('username');
            }
            if ($request->file('profile_image') != '') {
                $file = $request->file('profile_image');
                $ImagesFileName = $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $getRealPath = $file->getRealPath();
                $getSize = $file->getSize();
                $getMimeType = $file->getMimeType();
                $without_ext = preg_replace('/\\.[^.\\s]{3,4}$/', '', $ImagesFileName);
                $ImagesFileName = str_slug(strtolower($without_ext)) . '-' . time() . '.' . $ext;
                $destinationPath = public_path() . "/uploads/profile/";
                $file->move($destinationPath, $ImagesFileName);

                $userObj->profile_image = $ImagesFileName;
                $userObj->profile_path = $destinationPath;
                $userObj->profile_url = url('/public') . "/uploads/profile/" . $ImagesFileName;
                //url('/public').$albumData->url."/".$albumData->name
            }
            if ($request->get('current_password') == "" && $request->get('new_password') == "") {
                if ($userObj->save()) {
                    return redirect('profile')->with('success', Session::get('message_profile_change'));
                    exit;
                } else {
                    return redirect('profile')->with('error', Session::get('message_went_wrong'));
                    exit;
                }
            }

            if ($request->get('current_password') != '') {
                $validCredentials = Hash::check($request->get('current_password'), $userObj->password);
                if ($validCredentials) {
                    if ($request->get('confirm_password') == '') {
                        return redirect('profile')->with('error', Session::get('message_confirm_password'));
                        exit;
                    }
                    if ($request->get('new_password') != '') {
                        $userObj->password = bcrypt($request->get('new_password'));
                        $userObj->save();
                        return redirect('profile')->with('success', Session::get('message_profile_change'));
                        exit;
                    } else {
                        return redirect('profile')->with('error', Session::get('message_enter_password'));
                        exit;
                    }
                } else {
                    return redirect('profile')->with('error', Session::get('message_invalid_pass'));
                    exit;
                }
            }
        } else {
            return redirect('profile')->with('error', Session::get('message_email_not_register'));
            exit;
        }
    }

    public function forgotPassword(Request $request) {

        $rules = [
            'email' => 'required|email'
        ];
        $message = [
            "email.email" => "Valid email address required",
            "email.required" => "Please enter email",
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return response()->json([
                        'status' => false,
                        'error' => 401,
                        'message' => $validator->messages()->first(),
            ]);
            exit;
        }

        $userObj = AppUser::where('email', $request->email)->first();
        if (count($userObj) > 0) {
            if (Session::get('selectedLang') == 2) {
                $emailTemplateObj = EmailTemplate::where('id', 2)->where('language_id', 2)->first();
            } else {
                $emailTemplateObj = EmailTemplate::where('id', 1)->where('language_id', 1)->first();
            }

            $reset_link = env('FRONT_URL') . '/resent-password/' . $userObj->token;
            $tomail = $request->get('email');
            $subject = $emailTemplateObj->subject;
            $body = $emailTemplateObj->body;
            $body = str_replace('[userName]', $userObj->username, $body);
            $body = str_replace('[reset_link]', $reset_link, $body);
            //$code = mt_rand(1000, 9999);


            $data = array("name" => $userObj->username, "reset_link" => $reset_link, 'body' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'), 'FRONT_URL' => url(''));

            $res = Mail::send('emails.forgotPass', ['data' => $data], function($message) use ($data) {
                        $message->from($data['from'], $data['emailLabel']);
                        $message->to($data['to'], $data['name']);
                        $message->subject($data['subject']);
                    });

            return response()->json([
                        'status' => true,
                        'success' => 200,
                        'message' => Session::get('message_forgot_password_valid'),
            ]);
            exit;
        } else {
            return response()->json(['status' => false, 'error' => 401, 'message' => Session::get('message_email_not_register')]);
        }
    }

    public function resentPasswors(Request $request) {
        $token = collect(request()->segments())->last();
        $user = AppUser::where("token", $token)->first();
        if (!empty($user)) {
            return View::make('web.user.resetPassword')->with("email", $user->email)->with("id", $user->id);
        } else {
            return Redirect::to('/404')->with('message', Session::get('message_went_wrong'));
        }
    }

    public function resentPasswordMsg() {
        return view('web.user.resetConfirm');
    }

    public function resetPasswordStore(Request $request) {
        $rules = [
            'current_password'      => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password'
        ];

        $requestData = Input::all();

        $message = [
            //"email.email" => "Invalid e-mail address",
            "current_password.required" => "Please provide Your current Password",
            "password.required" => "Password is required",
            "password.min" => "Password should be six characters long",
            "confirm_password.required" => "Confirm Password is required",
            "confirm_password.same" => "Confirm Password and New Password do not match.",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator)
                            ->withInput();
        }

        $user_id = $request->user_id;
        $userObj = AppUser::where('uid', $user_id)->first();
        $validCredentials = Hash::check($request->get('current_password'), $userObj->password);
        
        if ($validCredentials) {
           
            $userObj->password = bcrypt($request->get('password'));

            if ($userObj->save()) {
                return Redirect::to('change-password')->with('message', 'Your Password changed successfully.');
            } else {
                return Redirect::to('change-password')->with('message', 'Something went wrong in updating your current password.');
            }
        } else {
            return Redirect::to('change-password')->with('message', 'Your current Password was incorrect. Please try again');
        }
    }

}
