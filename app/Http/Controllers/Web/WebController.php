<?php
namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Cms;
use App\Models\Notification;
use Illuminate\Support\Facades\Hash;
use App\Models\Language;
use App\Models\LanguageKeyword;
use Illuminate\Support\Facades\Session;
use App\Models\AppUser;
use App\Models\Question;
use App\Models\Setting;
use App\Models\Slider;
use Illuminate\Support\Facades\Mail;
use App\Models\Message;
use App\Library\webGeneral;
use App\Models\EmailTemplate;
use App\Models\FAQ;
use DB;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Cache;

class WebController extends Controller {

    public function __construct() {


    }
    public function loginCheck(Request $request){
        $userName = 'admin';
        $password = 'admin';
        if(isset($request->email)&& isset($request->password) && $request->email == $userName && $request->password == $password){
            Session::put('webAccess',1);
            return redirect('/');
        }else{
            return redirect('/')->with('error','Invalid UserName & Password');
        }
    }
    public function index() {
        return view('web.landing');
    }

    public function termsCondition() {
        $pagesData = CMS::where('status', 1)->first();
        return view('web.terms', ['data' => $pagesData]);
    }

    public function setLanguages(Request $request){
       
        $languages = Language::where('id', $request->get('langId'))->first();
        session::put('languageName', $languages->name);
        session::put('languageCode', $languages->code);
        session::put('languageId', $languages->id);
        Session::put("selectedLang",$languages->id);

        if(Session::get('selectedLang') != ''){
            $allMultilanguage = LanguageKeyword::where("language_id",$languages->id)->get()->toArray();
           
            foreach ($allMultilanguage as $key => $value) {
                    Session::put($value['keyword'],$value['label']);
                }

            }
        
        $getMessage = Message::where('language_id',Session::get('selectedLang'))->get();
        foreach ($getMessage as $message) {
            Session::put('message_'.$message->keyword, $message->value);
        } 
           if(Auth::check()){
            
            $AppUser    = AppUser::find(Auth::user()->id);
            $AppUser->language_id = session::get('selectedLang');
            $AppUser->save();
           }
        return Redirect::back();
    }

    public function about_us() {
        Session::forget('textQuestion');
        return view('web.aboutus');
    }
    
    public function contact(Request $request){
        Session::forget('textQuestion');
        Session::forget('category_id');
        Session::forget('booking_date');
        Session::forget('start_time');
        Session::forget('description');
        $setting = Setting::where('slug','email')->first();
        $tomail  = $setting->value;
        $setting_address = Setting::where('slug','address')->first();
        $address  = $setting_address->value;
        return view('web.contactus',['tomail' => $tomail,'address' => $address]);
    }

    public function contactSendMail(Request $request){
        $rules = [ 
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'PhoneNo' => 'max:15',
            //'country' => 'required',
            //'state' => 'required',
            //'zip' => 'required'
        ];
        $message = [
            "firstName.required" => "Please enter first Name",
            "lastName.required" => "Please enter last Name",
            "email.required" => "Please enter email",
            "PhoneNo.numeric" => "Please enter numeric value",
            "PhoneNo.max" => "Phone number should be maximum 15 character length",
            //"country.required" => "Please enter country",
            //"state.required" => "Please enter state",
           //"zip.required" => "Please enter zip"
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        if(Session::get('selectedLang') == 2){
              $emailTemplateObj =  EmailTemplate::where('id',10)->where('language_id',2)->first();
            }else{
              $emailTemplateObj =  EmailTemplate::where('id',9)->where('language_id',1)->first();
        }
        $setting  = Setting::where('slug','email')->first();
        $tomail   = $setting->value;
        $subject  = $emailTemplateObj->subject;
        $body     = $emailTemplateObj->body;
        $body     =  str_replace('[firstName]',$request->firstName,$body);
        $body     =  str_replace('[lastName]',$request->lastName,$body);
        $body     =  str_replace('[email]',$request->email,$body);
        $body     =  str_replace('[PhoneNo]',$request->PhoneNo,$body);
        $body     =  str_replace('[message]',$request->message,$body);

        $data    = array('body'=>$body,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'FRONT_URL' => url(''),'name' => $request->firstName.' '.$request->lastName);
        
        $res =Mail::send('emails.contact',['data'=>$data],function($message) use ($data){
            $message->from($data['from'],$data['emailLabel']);
            $message->to($data['to'],$data['name']);
            $message->subject($data['subject']);
        });

        return redirect('contactus')->with('success',  Session::get('message_contact_us'));

    }

    public function pageNotFound(){
        Session::forget('textQuestion');
        Session::forget('category_id');
        Session::forget('booking_date');
        Session::forget('start_time');
        Session::forget('description');
        return View::make('web.404');
    }
    public function privacyPolicy(){
        Session::forget('textQuestion');
        Session::forget('category_id');
        Session::forget('booking_date');
        Session::forget('start_time');
        Session::forget('description');
        return view('web.privacyPolicy');
    }

    public function helpPage(){
        Session::forget('textQuestion');
        Session::forget('category_id');
        Session::forget('booking_date');
        Session::forget('start_time');
        Session::forget('description');
       $faqObj = FAQ::where('status',1)->where('language_id',Session::get('selectedLang'))->get();
        return view('web.help')->with('faqObj',$faqObj);
    }

  public function aboutusAppEnglish(){
    return view('web.app.aboutusAppEnglish');
  }
  public function aboutusAppArabic(){
    return view('web.app.aboutusAppArabic');
  }

  public function privacyAppEnglish(){
    return view('web.app.privacyAppEnglish');
  }

  public function privacyAppArabic(){
    return view('web.app.privacyAppArabic');
  }

  public function helpAppEnglish(){
    $faqObj = FAQ::where('status',1)->where('language_id',1)->get();
    return view('web.app.helpAppEnglish')->with('faqObj',$faqObj);
  }

  public function helpAppArabic(){
    $faqObj = FAQ::where('status',1)->where('language_id',2)->get();
    return view('web.app.helpAppArabic')->with('faqObj',$faqObj);;
  }

  public function contactAppEnglish(){
    return view('web.app.contactAppEnglish');
  }

  public function contactAppArabic(){
    return view('web.app.contactAppArabic');
  }

  public function contactAppSendMail(Request $request){
        $rules = [ 
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'PhoneNo' => 'max:15',
            //'country' => 'required',
            //'state' => 'required',
            //'zip' => 'required'
        ];
        $message = [
            "firstName.required" => "Please enter first Name",
            "lastName.required" => "Please enter last Name",
            "email.required" => "Please enter email",
            "PhoneNo.max" => "Phone number should be maximum 15 character length",
            //"country.required" => "Please enter country",
            //"state.required" => "Please enter state",
            //"zip.required" => "Please enter zip"
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        if($request->language_id == 2){
              $emailTemplateObj =  EmailTemplate::where('id',10)->where('language_id',2)->first();
            }else{
              $emailTemplateObj =  EmailTemplate::where('id',9)->where('language_id',1)->first();
        }
        $setting  = Setting::where('slug','email')->first();
        $tomail   = $setting->value;
        $subject  = $emailTemplateObj->subject;
        $body     = $emailTemplateObj->body;
        $body     =  str_replace('[firstName]',$request->firstName,$body);
        $body     =  str_replace('[lastName]',$request->lastName,$body);
        $body     =  str_replace('[email]',$request->email,$body);
        $body     =  str_replace('[PhoneNo]',$request->PhoneNo,$body);
        $body     =  str_replace('[message]',$request->message,$body);

        $data    = array('body'=>$body,"to"=>$tomail,"subject"=>$subject,"from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'),'FRONT_URL' => url(''),'name' => $request->firstName.' '.$request->lastName);
        
        $res =Mail::send('emails.contact',['data'=>$data],function($message) use ($data){
            $message->from($data['from'],$data['emailLabel']);
            $message->to($data['to'],$data['name']);
            $message->subject($data['subject']);
        });

        if($request->language_id == 1){
          return redirect('contactAppEnglish')->with('success', 'Your contact request have submitted successfully.');  
        }else{
            return redirect('contactAppArabic')->with('success', 'لقد تم تقديم طلب الاتصال الخاص بك بنجاح.');  
        }
        

    }

    public function resetPassword($token) {
        $userObj = User::where('token',$token)->where('is_verified',1)->first();
        if(count($userObj) > 0) {
            return view('Admin.user.resetPassword',['user'=>$userObj,'email' => $userObj->email,'token' => $token]);
        } else {
            //return Redirect::to('admin/login');
        }
    }

    public function savePassword(Request $request) {

        $rules = [
            'password'      => 'required|min:6',
            'conf_password' => 'required|same:password'
        ];
         $message = [
            "password.required"         => "Password is required",
            "conf_password.required"    => "Confirm Password is required",
            "password.min"              => "Password should be six characters long",
            "conf_password.same"        => "Confirmation Password should be like password",
        ];
        $validator = Validator::make(Input::all(), $rules,$message);
        // process the login

        if ($validator->fails()) {
            return Redirect::to('expert/confirm/'.$request->token)->withErrors($validator);
        }

        $userObj = User::where('token',$request->token)->first();
        if (count($userObj) > 0) {
            $userObj->is_verified   = 1;
            $userObj->status        = 1;
            $userObj->verified_at   = date('Y-m-d H:i:s');
            $userObj->password      = bcrypt($request->password);
            $userObj->save();
            Auth::guard('panel')->logout();
            Session::flush();
            return Redirect::to('panel/');
        }
    }


    public function saveResetPassword(Request $request) {
        
        $rules = [
            'current_password'      => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password'
        ];

        $requestData = Input::all();

        $message = [
            //"email.email" => "Invalid e-mail address",
            "current_password.required" => "Please provide Your current Password",
            "password.required" => "Password is required",
            "password.min" => "Password should be six characters long",
            "confirm_password.required" => "Confirm Password is required",
            "confirm_password.same" => "Confirm Password and New Password do not match.",
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules, $message);

        if ($validator->fails()) {

            return Redirect::back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $user_id = $request->user_id;
        $userObj = AppUser::where('id', $user_id)->first();
        $validCredentials = Hash::check($request->get('current_password'), $userObj->password);
        
        if ($validCredentials) {
           
            $userObj->password = bcrypt($request->get('password'));

            if ($userObj->save()) {
                return Redirect::to('change-password')->with('success', 'Your Password changed successfully.');
            } else {
                return Redirect::to('change-password')->with('error', 'Something went wrong in updating your current password.');
            }
        } else {
            return Redirect::to('change-password')->with('error', 'Your current Password was incorrect. Please try again');
        }

    }

    public function resetConfirm() {
        return view('web.user.resetConfirm');
    }


    public function userConfirm($token) {
        $userObj = User::where('token',$token)->first();

        if(count($userObj) > 0) {
             $userObj->is_verified = 1;
             $userObj->status = 1;
             $userObj->verified_at = date('Y-m-d H:i:s');
             $userObj->save();
            return view('Admin.user.setPassword',['token'=>$token]);
        }else{
            return View::make('Admin.error.404');
        }
    }

    public function userAccountConfirm() {
        return view('web.user.accountConfirm');
    }
}