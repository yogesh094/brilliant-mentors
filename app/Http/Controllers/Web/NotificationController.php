<?php
namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\Cms;
use App\Models\Notification;
use Illuminate\Support\Facades\Hash;
use App\Models\Language;
use App\Models\LanguageKeyword;
use Illuminate\Support\Facades\Session;
use App\Models\AppUser;
use App\Models\Question;
use App\Models\Category;
use App\Models\Setting;
use App\Models\Message;
use App\Models\Appointment;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Cache;
use DB;

class NotificationController extends Controller {

    public function __construct() {
     
     $languages      = Language::where('status',1)->get()->toArray();
        $languageArr    = [];
        foreach ($languages as $key => $lang) {
            $languageArr[$lang['id']] = $lang['name'];
        }
      Session::put("languageArr",$languageArr);
      $selectedLang = 1;
        if(Session::get('selectedLang') != ''){
             $selectedLang = Session::get('selectedLang');
        }
      Session::put("selectedLang",$selectedLang);
      $getMessage = Message::where('language_id',Session::get('selectedLang'))->get();
      foreach ($getMessage as $message) {
            Session::put('message_'.$message->keyword, $message->value);
        }
        Session::forget('textQuestion');
    }

    public function getAllNotification(Request $request){
      Session::forget('textQuestion');
        Session::forget('category_id');
        Session::forget('booking_date');
        Session::forget('start_time');
        Session::forget('description');
       $Notification = Notification::where('appuser_id',Auth::user()->id)->where('language_id',Session::get('selectedLang'))->latest('id')->paginate(20);

    //     $Notification = DB::table('notification')
    // ->select('notification.*','question.question')
    // ->join('question', 'question.id', '=', 'notification.question_id')
    // ->where('notification.appuser_id', Auth::user()->id)
    // ->where('notification.language_id', session::get('selectedLang'))
    // ->latest('id')->paginate(20);


       //->get()->toArray()
       $data = [];
       if(count($Notification) > 0){
        foreach ($Notification as $key => $notifications) {
          $data[$key]['id'] = $notifications->id;
          $data[$key]['type_id'] = $notifications->type_id;
          $data[$key]['question_id'] = $notifications->question_id;
          $data[$key]['message'] = $notifications->message;
          $data[$key]['created_at'] = date('Y-m-d H:i:s', strtotime($notifications->created_at));
          $description = "";
          $answer = "";
          if($notifications->type_id == 1){
            $questionObj = Question::where('id',$notifications->question_id)->first();
            if(!empty($questionObj)){
              $description = str_limit($questionObj->question,100);
              $answer = $questionObj->answer;

            }
          }else if($notifications->type_id == 0){
              $AppointmentObj = Appointment::where('id',$notifications->question_id)->first();
              if(!empty($AppointmentObj)){
                 $description = str_limit($AppointmentObj->description,200);
              }
          }
          $data[$key]['description'] = $description;
          $data[$key]['answer'] = $answer;
        }
       }
       
        return view('web.notification',["Notification" => $Notification,'data' => $data]);
    }

    public function getQuestionAnswer(Request $request,$id){
        Session::forget('textQuestion');
        Session::forget('category_id');
        Session::forget('booking_date');
        Session::forget('start_time');
        Session::forget('description');

      $Notification = Notification::find($id);
      if(!empty($Notification)){
        DB::table('question')->where('id', $Notification->question_id)->increment('is_popular');
      $questionObj = Question::where('id',$Notification->question_id)->where('language_id',Session::get('selectedLang'))->first();
      $getExpertName = '';
      if(count($questionObj) > 0){
        $getExpertName = User::where('id',$questionObj->expert_id)->pluck('username')->first();
        $Notification->is_read = 1;
        $Notification->save();
        return view('web.notification-view',['questionObj' => $questionObj,'getExpertName' => $getExpertName]);
      }else{
        return Redirect::to('/404');
      }
      }else{
        return Redirect::to('/404');
       }
    }
}
