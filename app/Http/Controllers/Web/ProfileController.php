<?php
namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Language;
use App\Models\LanguageKeyword;
use Illuminate\Support\Facades\Session;
use App\Models\AppUser;
use Illuminate\Support\Facades\Mail;
use App\Models\Message;
use App\Library\webGeneral;
use App\Models\EmailTemplate;
use App\Models\Country;
use App\Models\State;
use App\Models\FAQ;
use App\Models\GeoLocations;
use App\Models\Employment;
use App\Models\Education;
use App\Models\Stream;
use App\Models\SubStream;
use App\Models\UserStream;
use App\Models\PostMedia;
use App\Models\PostLike;
use App\Models\PostReply;
use App\Models\PostComment;
use App\Models\Post;
use DateTime;
use DB;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use Cache;

class ProfileController extends Controller {

    public function __construct() {
    }

    public function profileInformation() {
    	$user_id = Auth::user()->id;
        $userInfo = AppUser::where('id',$user_id)->first();
        $country = Country::get();
        $state = DB::table('geo_locations')->where('location_type', '=', 'STATE')->get()->all();
        $district = DB::table('geo_locations')->where('location_type', '=', 'DISTRICT')->get()->all();
        //$state = State::get();
        $profile_sidebar = '';
        $profile_sidebar = (string) view('web.master_layout.profileSidebar');
        if(Auth::user()->user_type == 1){
            return view('web.profile.profile_student',compact('userInfo','country','state','profile_sidebar'));
        }else{
            return view('web.profile.profile',compact('userInfo','country','state','profile_sidebar'));
        }
        
    }

    public function selectDistrict(Request $request)
    {
//        die('comes');
        $data = $request->all();
        $id = $data['id'];
        $selected = isset($data['selected']) ? $data['selected'] : 0;
        $results = DB::table('geo_locations')->where('parent_id', '=', $id)->get();
        return view('web.ajaxDistrict', compact('results','selected'));
    }

    public function selectSubStream(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $selected = isset($data['selected']) ? $data['selected'] : 0;
        $results = DB::table('sub_stream')->where('stream_id', '=', $id)->get();
        return $results;
    }

    public function saveProfile(Request $request){

        $dob = date('Y-m-d', strtotime($request->datetimepicker));
        $user_id = Auth::user()->id;
        $userObj = AppUser::find($user_id);
        $userObj->first_name = $request->first_name;
        $userObj->last_name = $request->last_name;
        $userObj->dob = $dob;
        $userObj->phone_number = $request->phone_number;
        $userObj->city = $request->city;
        $userObj->state = $request->state;
        $userObj->birth_place = $request->birth_place;
        $userObj->religion = $request->religion;
        $userObj->about_u = $request->about_u;
        $userObj->relationship = $request->relationship;
        $userObj->fb_link = $request->fb_link;
        $userObj->twitter_link = $request->twitter_link;
        $userObj->insta_link = $request->insta_link;
        $userObj->fb_link = $request->fb_link;
        $userObj->twitter_link = $request->twitter_link;
        $userObj->gender = $request->gender;
        $userObj->occupation = $request->occupation;

        if ($userObj->save()) {
            return redirect('profile')->with('success', 'Profile updated sucessfully');
            exit;
        } else {
            return redirect('profile')->with('error', 'Something went to wrong in updating profile. Plese try again');
            exit;
        }

    }

    public function changePassword() {
        $user_id = Auth::user()->id;
        $profile_sidebar = '';
        $profile_sidebar = (string) view('web.master_layout.profileSidebar');
        
        return view('web.profile.change-password',compact('user_id','profile_sidebar'));
    }

    public function education() {

        $employmentView = '';
        $educationView = '';
        $month = array(1=>'January',2=>'February',3=>'March',4=>'April',6=>'May',7=>'June',8=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');

        $user_id = Auth::user()->id;
        $employmentData = array();
        $employment = Employment::where('user_id',$user_id)->get();
        $employment_count = 0;
        if(count($employment) > 0){
            $employmentData = $employment->toArray();

            foreach ($employmentData as $key => $value) {

                $employmentView .= (string) view('web.profile.employement_block',['count' => $employment_count,'month' => $month,'employmentData' => $value]);
                $employment_count++;
            }
        }else{
            $employmentView .= (string) view('web.profile.employement_block',['count' => $employment_count,'month' => $month,'employmentData' => $employmentData]);
        }
        $educationData = array();
        $education = Education::where('user_id',$user_id)->get();
        $education_count = 0;
        if(count($education) > 0){
            $educationData = $education->toArray();

            foreach ($educationData as $key => $value) {

                $educationView .= (string) view('web.profile.education_block',['count' => $education_count,'month' => $month,'educationData' => $value]);
                $education_count++;
            }
        }else{
            $educationView .= (string) view('web.profile.education_block',['count' => $education_count,'month' => $month,'educationData' => $educationData]);
        }

        $profile_sidebar = '';
        $profile_sidebar = (string) view('web.master_layout.profileSidebar');
        if(Auth::user()->user_type == 1){
            return view('web.profile.education_student',compact('profile_sidebar','employmentData','month','employmentView','employment_count','educationView','education_count'));
        }else{
            return view('web.profile.education',compact('profile_sidebar','employmentData','month','employmentView','employment_count','educationView','education_count'));
        }
        
    }
    public function education_block(Request $request){

        $data = $request->all();
        $education_count = $data['education_count'];
        $month = array(1=>'January',2=>'February',3=>'March',4=>'April',6=>'May',7=>'June',8=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
        $response['content'] = (string) view('web.profile.education_block',['count' => $education_count,'month' => $month]);
        $response['education_count'] = $education_count + 1;
        
        return response()->json($response);
    }

    public function employment_block(Request $request){
        $data = $request->all();
        $month = array(1=>'January',2=>'February',3=>'March',4=>'April',6=>'May',7=>'June',8=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
        $employment_count = $data['employment_count'];
        $response['content'] = (string) view('web.profile.employement_block',['count' => $employment_count,'month' => $month]);
        $response['employment_count'] = $employment_count + 1;
        
        return response()->json($response);   
    }

    public function save_employment(Request $request){
        $data = $request->all();
        $user_id = Auth::user()->id;

        if(!empty($data['Employment'])){

            Employment::where('user_id',$user_id)->delete();
            foreach ($data['Employment'] as $key => $value) {

                if($value['position'] != ''){
                    $empObj = new Employment;
                
                    $empObj->start_month = $value['start_month'];
                    $empObj->start_year = $value['start_year'];
                    $empObj->end_month = $value['end_month'];
                    $empObj->end_year = $value['end_year'];
                    $empObj->user_id = $user_id;
                    $empObj->description = $value['description'];
                    $empObj->position = $value['position'];
                    $empObj->save();    
                }
                
            }
            $response['status'] = 200;
            $response['message'] = 'sucessfully submited';
        
            //return response()->json($response); 
        }else{
            $response['status'] = 401;
            $response['message'] = 'something wrong. please try again';
        }
        return response()->json($response);
    }

    public function save_education(Request $request){
        $data = $request->all();
        $user_id = Auth::user()->id;

        if(!empty($data['Education'])){

            Education::where('user_id',$user_id)->delete();
            foreach ($data['Education'] as $key => $value) {

                if($value['position'] != ''){
                    $empObj = new Education;
                
                    $empObj->start_month = $value['start_month'];
                    $empObj->start_year = $value['start_year'];
                    $empObj->end_month = $value['end_month'];
                    $empObj->end_year = $value['end_year'];
                    $empObj->user_id = $user_id;
                    $empObj->description = $value['description'];
                    $empObj->position = $value['position'];
                    $empObj->save();    
                }
                
            }
            $response['status'] = 200;
            $response['message'] = 'sucessfully submited';
        
            //return response()->json($response); 
        }else{
            $response['status'] = 401;
            $response['message'] = 'something wrong. please try again';
        }
        return response()->json($response);
    }

    public function hobbiesAndInterests(Request $request){

        $user_id = Auth::user()->id;
        $userData = AppUser::where('id',$user_id)->first();
        $stream = Stream::where('status',1)->get();
        $substream = [];
        $stream_selected = [];
        $sub_stream_selected = [];
        if(count($userData) > 0){

            $userStream = UserStream::where('user_id',$userData->id)->get();
           
            if(count($userStream) > 0){
                $st = [];
                $st_sub = [];
                foreach ($userStream as $key => $data) {
                   $st[$key]['stream_id'] = $data->stream_id;
                   $st_sub[$key]['sub_stream_id'] = $data->sub_stream_id;
                }
            }
            $stream = Stream::where('status',1)->whereNotIn('id',$st)->get();
            $stream_selected = Stream::where('status',1)->whereIn('id',$st)->get();
            $sub_stream = SubStream::where('status',1)->whereNotIn('id',$st_sub)->whereIn('stream_id',$st)->get();
            $sub_stream_selected = SubStream::where('status',1)->whereIn('id',$st_sub)->get();
        }
        $profile_sidebar = '';
        $profile_sidebar = (string) view('web.master_layout.profileSidebar');
        return view('web.profile.hobbies-interests',compact('profile_sidebar','userData','stream','stream_selected','sub_stream','sub_stream_selected'));
    }

    public function aboutProfile($id){

        $userData = AppUser::where('id',$id)->first();

        // if(!empty($userData->id)){
        //     $employment = Employment::where('user_id',$userData->id)->get();   

        //     foreach ($employment as $key => $value) {
               
        //     }
        // }
        $top_header_profile = '';
        $top_header_profile = (string) view('web.master_layout.top-header-profile',compact('userData'));
        return view('web.profile.profile-about',compact('top_header_profile','userData'));
    }

    public function saveInterest(Request $request){

        $rules['stream_id'] = 'required';
        $rules['sub_stream_id'] = 'required';
        $message = array(
            'stream_id.required'         => 'Stream is required',
            'sub_stream_id.required'     => 'Sub Stream is required',
        );
        $validator = Validator::make($request->all(), $rules, $message);
        if ($validator->fails()) {
            if ($request->id != null && $request->id > 0) {
                return Redirect::to('hobbies-interest/' . $request->id)->withInput()->withErrors($validator);
            } else {
                return Redirect::to('hobbies-interest/')->withErrors($validator)->withInput();
            }
        } else {

            $user_id = Auth::user()->id;

            $stream_ids = [];

            $getStreamAndSubStreamIds = SubStream::whereIn('id', $request->sub_stream_id)->whereIn('stream_id', $request->stream_id)->select('stream_id', 'id as sub_stream_id')->get();
            if(isset($request->stream_id)){
                foreach ($getStreamAndSubStreamIds as $key => $data) {
                    $values = [];
                    $values['user_id'] = $user_id;
                    $values['stream_id'] = $data->stream_id;
                    $values['sub_stream_id'] = $data->sub_stream_id;
                    DB::table('user_stream')->insert($values);
                }
                return redirect('hobbies-interest/')->with('success', 'Your data is submitted successfully.');
                exit;
            }else{
                return redirect('hobbies-interest/')->with('erro', 'Something went wrong. Data not submitted properly.');
                exit;
            }
        }
    }

    public function aboutProfileTimeline($id){

        $userData = AppUser::where('id',$id)->first();
        $user_id = Auth::user()->id;
        $user_type = Auth::user()->user_type;
        $post_view = '';
        $postList = Post::getUserList();
        if(count($postList) > 0){

            $postList = $postList->toArray();
            foreach ($postList as $key => $value) {

                $comment_view = '';
                $commentCount = Post::find($value['id'])->comments()->get()->count();
                $likeCount = Post::find($value['id'])->likes()->get()->count();
                $media = Post::find($value['id'])->media()->get();
                $commentList = PostComment::getList($value['id']);

                if(count($commentList) > 0){
                    foreach ($commentList as $key => $comment) {
                        $comment_view .= (string) view('web.post.comment_block',compact('comment'));
                    }
                }

                if(count($media) > 0){
                    $post_view .= (string) view('web.post.no_media');
                }else{
                    $post_view .= (string) view('web.post.no_media',compact('value','commentCount','likeCount','commentList','comment_view')); 
                }
            }
        }
        $top_header_profile = '';
        $top_header_profile = (string) view('web.master_layout.top-header-profile',compact('userData'));
        return view('web.profile.profile-timeline',compact('top_header_profile','userData','post_view','user_id','user_type'));
    }

    public function saveUserProfilePic(Request $request){

        // $photo_upload = $request->file('dataPic');
        // $filename = $_FILES['file']['name'];
        print_r($request->all());
        exit;
        // print_r($dataPic);
        if ($photo_upload) {
            $image = Image::make($photo_upload->getRealPath());
            $photo_name = time().'.'.$photo_upload->getClientOriginalExtension();
            $photo_name = md5(time()."_".$photo_name).strrchr($photo_name, '.');
            $destinationPath = upload_path() . "user_profile/";
            $image->save($destinationPath.$photo_name, 100);
        }

    }


}