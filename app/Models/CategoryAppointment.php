<?php
namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class CategoryAppointment extends Eloquent
{
	protected $table = 'category_appointment';
	public $primaryKey = 'id';	
}
