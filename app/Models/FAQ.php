<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class FAQ extends Eloquent
{
	protected $table = 'faq';
	public $primaryKey = 'id';
}
