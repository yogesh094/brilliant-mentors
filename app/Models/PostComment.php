<?php

/**
 * Created by Reliese Model.
 * Date: Sunday, 07 Apr 2019 03:41:32 PM.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Role
 * 
 * @property int $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $permissions
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class PostComment extends Authenticatable
{
	protected $table = 'post_comment';
	protected $primaryKey = 'id';

	public static function getList($post_id = 0)
    {
    	$pageLimit = 2;
        $post_data = Self::select("post_comment.comment","post_comment.post_id" ,"users.first_name", "post_comment.id","post_comment.commenter_id", "users.last_name", "post_comment.created_at")
                        ->join("users", "users.id", "=", "post_comment.commenter_id")
                        ->where("post_comment.post_id",$post_id)
                        ->orderBy("created_at", "DESC")
                        ->simplePaginate($pageLimit);

        return $post_data;                
    }
}
