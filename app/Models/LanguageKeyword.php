<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 06:07:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class LanguageKeyword
 * 
 * @property int $id
 * @property string $key_title
 * @property int $created_by
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deledted_at
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $language_multis
 *
 * @package App\Models
 */
class LanguageKeyword extends Eloquent
{
	protected $table = 'language_keyword';
	protected $primaryKey = 'id';
}
