<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 04 Apr 2018 10:19:25 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PermissionRole
 * 
 * @property int $permission_id
 * @property int $role_id
 *
 * @package App\Models
 */
class PermissionRole extends Eloquent {

    protected $table = 'permission_role';
    public $incrementing = false;
    public $timestamps = false;
    protected $casts = [
        'permission_id' => 'int',
        'role_id' => 'int'
    ];
    protected $fillable = [
        'permission_id',
        'role_id'
    ];

}
