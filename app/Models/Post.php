<?php

/**
 * Created by Reliese Model.
 * Date: Sunday, 07 Apr 2019 03:41:32 PM.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Model\PostMedia;
use App\Model\PostComment;
use App\Model\PostReply;
use App\Models\AppUser;
use Auth;

class Post extends Authenticatable
{
	protected $table = 'user_post';
	protected $primaryKey = 'id';

	public function comments()
    {
        return $this->hasMany('App\Models\PostComment');
    }
    public function likes()
    {
        return $this->hasMany('App\Models\PostLike');
    }
    public function media()
    {
        return $this->hasMany('App\Models\PostMedia');
    }
    public static function getList()
    {
        $post_data = Self::select("user_post.description", "users.first_name", "user_post.id", "users.last_name", "user_post.created_at","user_post.user_id")
                        ->join("users", "users.id", "=", "user_post.user_id")
                        ->orderBy("created_at", "DESC")->get();

        return $post_data;                
    }

    public static function getUserList()
    {
        $post_data = Self::select("user_post.description", "users.first_name", "user_post.id", "users.last_name", "user_post.created_at","user_post.user_id")
                        ->join("users", "users.id", "=", "user_post.user_id")
                        ->where('user_post.user_id',Auth::user()->id)
                        ->orderBy("created_at", "DESC")->get();

        return $post_data;                
    }

    public static function getSinglePost($post_id)
    {
        $post_data = Self::select("user_post.description", "users.first_name", "user_post.id", "users.last_name", "user_post.created_at","user_post.user_id")
                        ->join("users", "users.id", "=", "user_post.user_id")
                        ->where("user_post.id", $post_id)->first();

        return $post_data;                
    }
}
