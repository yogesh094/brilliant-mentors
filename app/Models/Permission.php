<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 04 Apr 2018 10:19:05 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Zizaco\Entrust\EntrustPermission;

/**
 * Class Permission
 * 
 * @property int $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property int $created_by
 * @property int $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Permission extends EntrustPermission {

    public $incrementing = false;
    protected $casts = [
        'id' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int'
    ];
    protected $fillable = [
        'id',
        'module',
        'name',
        'display_name',
        'description',
        'created_by',
        'updated_by'
    ];

    public static function getPermission() {

        $permission = ['roles' => [
                [
                    'name' => 'list',
                    'display_name' => 'Display Role Listing',
                    'description' => 'See only Listing Of Role'
                ],
                [
                    'name' => 'add',
                    'display_name' => 'Create Role',
                    'description' => 'Create New Role'
                ],
                [
                    'name' => 'edit',
                    'display_name' => 'Edit Role',
                    'description' => 'Edit Role'
                ],
                [
                    'name' => 'delete',
                    'display_name' => 'Delete Role',
                    'description' => 'Delete Role'
                ]
            ],
            'general_setting' => [
                [
                    'name' => 'show',
                    'display_name' => 'General Setting show',
                    'description' => 'General Setting Show only '
                ],
                [
                    'name' => 'store',
                    'display_name' => 'General Setting Add Edit',
                    'description' => 'Add Edit General Setting'
                ]
            ]
        ];
        return $permission;
    }

}
