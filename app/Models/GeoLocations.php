<?php

namespace App;

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;

// use Illuminate\Database\Eloquent\Model;

class GeoLocations extends Authenticatable {

	protected $table = 'geo_locations';
	protected $primaryKey = 'id';


    public static function getGeoLocationsById($id) {
        return self::query()->where('id', $id)->get()->first();
    }
}
