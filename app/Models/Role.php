<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 04 Apr 2018 10:19:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Zizaco\Entrust\EntrustRole;

/**
 * Class Role
 * 
 * @property int $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Role extends EntrustRole
{
	protected $fillable = [
		'name',
		'display_name',
		'description',
		'updated_at',
	];
}
