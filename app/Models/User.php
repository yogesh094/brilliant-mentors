<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 29 Dec 2017 06:07:32 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $token
 * @property int $user_role
 * @property int $is_super
 * @property string $phone_number
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \Carbon\Carbon $verified_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $contacts
 * @property \Illuminate\Database\Eloquent\Collection $languages
 * @property \Illuminate\Database\Eloquent\Collection $language_keywords
 * @property \Illuminate\Database\Eloquent\Collection $language_multis
 * @property \Illuminate\Database\Eloquent\Collection $roles
 * @property \Illuminate\Database\Eloquent\Collection $user_login_histories
 *
 * @package App\Models
 */
class User extends Authenticatable
{
	// use \Illuminate\Database\Eloquent\SoftDeletes;
	// use EntrustUserTrait;
	use EntrustUserTrait {
        can as entrustCan;
    }

    public function can($permission, $requireAll = false)
    {
        if ($this->is_super == 1) {
            return true;
        }
        return $this->entrustCan($permission, $requireAll);
    }

	protected $table = 'users';
	protected $primaryKey = 'id';
	protected $casts = [
		'user_role' => 'int',
		'is_super' => 'int',
		'status' => 'int'
	];

	protected $dates = [
		'verified_at'
	];

	protected $hidden = [
		'password',
		'token'
	];

	protected $fillable = [
		'first_name',
		'last_name',
		'username',
		'email',
		'password',
		'token',
		'user_role',
		'is_super',
		'phone_number',
		'status',
		'verified_at'
	];

	

	public function contacts()
	{
		return $this->hasMany(\App\Models\Contact::class, 'updated_by');
	}

	public function languages()
	{
		return $this->hasMany(\App\Models\Language::class, 'created_by');
	}

	public function language_keywords()
	{
		return $this->hasMany(\App\Models\LanguageKeyword::class, 'created_by');
	}

	public function language_multis()
	{
		return $this->hasMany(\App\Models\LanguageMulti::class, 'created_by');
	}

	public function roles()
	{
		return $this->belongsToMany(\App\Models\Role::class);
	}

	public function user_login_histories()
	{
		return $this->hasMany(\App\Models\UserLoginHistory::class);
	}
	//generate unique string for user token
    public static function generateToken($length=25) {
        $token          =   "";
        $codeAlphabet   =   "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet   .=  "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet   .=  "0123456789";
        $max            =   strlen($codeAlphabet); // edited

        for($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }
        return $token;
    }
}
