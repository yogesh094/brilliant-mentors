<?php
namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class EmailTemplate extends Eloquent
{
	protected $table = 'email_template';
	public $primaryKey = 'id';	
}
