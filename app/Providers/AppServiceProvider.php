<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Message;
use App\Models\Setting;
use Illuminate\Support\Facades\Session;
use Cache;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       
        $request = app('request');
        $language_id = $request->get('language_id') != null?$request->get('language_id'):1;
        $getMessage = Message::where('language_id',$language_id)->get();
        foreach ($getMessage as $message) {
            Cache::forever($message->keyword, $message->value);
        } 

        $getSetting = Setting::all();
        foreach ($getSetting as $setting) {
            Cache::forever($setting->slug, $setting->value);
        }  
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }
}
