<?php 
namespace App\Providers;

use Session;
use App\Extensions\RedisSessionStore;
use Illuminate\Support\ServiceProvider;

class SessionServiceProvider extends ServiceProvider {

    public function boot() {
        Session::extend('redis', function($app) {
            return new RedisSessionStore;
        });
    }

    public function register() {}
}
?>