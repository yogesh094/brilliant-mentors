<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Message;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider\SessionServiceProvider;
use App\Models\Language;
use Request;
use Cache;

class WebServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        //Cache::forever('selectedLang', 2);
        
        $languages      = Language::all()->toArray();
        $languageArr    = [];
        foreach ($languages as $key => $lang) {
            $languageArr[$lang['id']] = $lang['name'];
        }
      Session::put("languageArr",$languageArr);
      $selectedLang = 1;
        if(Session::get('selectedLang') != ''){
             $selectedLang = Session::get('selectedLang');
        }
      Session::put("selectedLang",$selectedLang);
      
    }

}
