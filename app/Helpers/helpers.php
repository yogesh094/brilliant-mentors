<?php

function pr($data) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function storeDateTime($date = '') {
    return $date != '' ? date('Y-m-d H:i:s', strtotime($date)) : date('Y-m-d H:i:s');
}

function noSignImage_url() {
    return asset('/admin/img/e4da87134d00eda959ad1a9783159310.png');
}

function noSignImage_path() {
    return public_path() . '/admin/img/e4da87134d00eda959ad1a9783159310.png';
}

function getDateTimeFormat($date = "") {
    if ($date != '') {
        return date("d-M-Y g:i a", strtotime($date));
    } else {
        return date("d-M-Y g:i a");
    }
}

function getDateTimeStampFormat($date = "") {
    if ($date != '') {
        return date("d-M-Y H:m:s", strtotime($date));
    } else {
        return date("d-M-Y H:m:s");
    }
}

function displayDateFormat($date = "") {
    if ($date != '') {
        return date("d-M-Y", strtotime($date));
    } else {
        return date("d-M-Y");
    }
}

function getDateFormat($date = "") {
    // return date("d-M-Y",  strtotime($date));
    if ($date != '') {
        return date("Y-m-d", strtotime($date));
    } else {
        return date("Y-m-d");
    }
}

function apiDisplayDateFormat($date = "") {
    return date("d-m-Y", strtotime($date));
}


function getCountryName($id) {
    if ($id != null) {
        $data = App\Models\Country::where(['id' => $id])->first();
        return $data->name;
    } else {
        return '';
    }
}

function getStateName($id) {
    if ($id != null) {
        $data = App\Models\GeoLocations::where(['id' => $id])->where('location_type','STATE')->first();
        return $data->name;
    } else {
        return '';
    }
}

function getCityName($id) {
    if ($id != null || $id != 0) {
        $data = App\Models\GeoLocations::where(['id' => $id])->where('location_type','DISTRICT')->first();
        if (isset($data->name)) {
            return $data->name;
        } else {
            return '';
        }
    } else {
        return '';
    }
}

function sendOTP() {
    $user = App\User::where(['email' => Input::get('email')])->first();
    $admin_role = App\Models\AdminRole::find($user->admin_role_id);
    if ($admin_role->required_otp == 1) {
        $digits = 4;
        $otp = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
        //dd($user);
        $userName = $user['name'];
        $tomail = $user['email'];
        $subject = 'Login OTP';
        $body = 'Donation System Login OTP is - <b>' . $otp . '</b>';
        $data = array("userName" => $userName, 'message' => $body, "to" => $tomail, "subject" => $subject, "from" => config('constants.fromMail'), "emailLabel" => config('constants.labelMail'));

        $res = Mail::send('emails.template', ['data' => $data], function($message) use ($data) {
                    $message->from($data['from'], $data['emailLabel']);
                    $message->to($data['to'], $data['userName']);
                    $message->subject($data['subject']);
                });
        sendSMS([$user->phone], strip_tags($body));
        $user->login_otp = $otp;
        $user->login_otp_expire = date('Y-m-d H:i:s', strtotime("+5 minutes"));
        $user->save();
        return true;
    } else {
        return false;
    }
}

// function sendSMS($numbers, $message){
//     // Account details
//     $apiKey = urlencode('7Ipxfw345Os-rzuSD0x9IOG4wOohEZXDESEs8qXSXT');
//     // Message details
//     //$numbers = array(918123456789, 918987654321);
//     $sender = urlencode('TXTLCL');
//     //$message = rawurlencode('This is your message');
//     $message = rawurlencode($message);
//     $numbers = implode(',', $numbers);
//     // Prepare data for POST request
//     $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
//     // Send the POST request with cURL
//     $ch = curl_init('https://api.textlocal.in/send/');
//     curl_setopt($ch, CURLOPT_POST, true);
//     curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     $response = curl_exec($ch);
//     curl_close($ch);
//     // Process your response here
//     return $response;
// }

function sendSMS($numbers, $message) {
    // Account details
    $user = urlencode('PiggyC');
    $apiKey = urlencode('500aed1921XX');

    // http://103.233.79.246//submitsms.jsp?user=PiggyC&key=500aed1921XX&mobile=+919881237461&message=test sms&senderid=INFOSM&accusage=1
    // Message details
    //$numbers = array(918123456789, 918987654321);
    $sender = urlencode('INFOSM');
    //$message = rawurlencode('This is your message');
    // $message = rawurlencode($message);
    // dd($message);
    // $numbers = $numbers;
    foreach ($numbers as $key => $value) {
        $number = "+91" . $value;
        // Prepare data for POST request
        $data = array('user' => $user, 'key' => $apiKey, 'mobile' => $number, "senderid" => $sender, "accusage" => 1, "message" => $message);
        // dd('http://103.233.79.246//submitsms.jsp?'.http_build_query($data));
        // Send the POST request with cURL
        $ch = curl_init('http://103.233.79.246//submitsms.jsp?' . http_build_query($data));
        curl_setopt($ch, CURLOPT_POST, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        // dd($response);
        curl_close($ch);
    }

    // Process your response here
    return $response;
}

function sendEmail() {
    //define the receiver of the email
    $to = 'rahul.botad@yahoo.com';
    //define the subject of the email
    $subject = 'Test email with attachment';
    //create a boundary string. It must be unique
    //so we use the MD5 algorithm to generate a random hash
    $random_hash = md5(date('r', time()));
    //define the headers we want passed. Note that they are separated with \r\n
    $headers = "From: webmaster@example.com\r\nReply-To: webmaster@example.com";
    //add boundary string and mime type specification
    $headers .= "\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-" . $random_hash . "\"";
    //read the atachment file contents into a string,
    //encode it with MIME base64,
    //and split it into smaller chunks
    $attachment = chunk_split(base64_encode(file_get_contents(public_path() . '/uploads/tmp/SAT00351.pdf')));
    //define the body of the message.
    ob_start(); //Turn on output buffering
    echo '--PHP-mixed-' . $random_hash;
    echo 'Content-Type: multipart/alternative; boundary="PHP-alt-' . $random_hash . '"';

    echo '--PHP-alt-' . $random_hash;
    echo 'Content-Type: text/plain; charset="iso-8859-1"';
    echo 'Content-Transfer-Encoding: 7bit';

    echo 'Hello World!!!';
    echo 'This is simple text email message.';

    echo '--PHP-alt-' . $random_hash;
    echo 'Content-Type: text/html; charset="iso-8859-1"';
    echo 'Content-Transfer-Encoding: 7bit';

    echo '<h2>Hello World!</h2>';
    echo '<p>This is something with <b>HTML</b> formatting.</p>';

    echo '--PHP-alt-' . $random_hash . '--';

    echo '--PHP-mixed-' . $random_hash;
    echo 'Content-Type: application/zip; name="attachment.zip" ';
    echo 'Content-Transfer-Encoding: base64 ';
    echo 'Content-Disposition: attachment ';
    echo $attachment;
    echo '--PHP-mixed-' . $random_hash . '--';

    //copy current buffer contents into $message variable and delete current output buffer
    $message = ob_get_clean();
    //send the email
    $mail_sent = @mail($to, $subject, $message, $headers);
    //if the message is sent successfully print "Mail sent". Otherwise print "Mail failed"
    echo $mail_sent ? "Mail sent" : "Mail failed";
}

function curlCall($qs, $wsUrl) {
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    return $o = curl_exec($c);
}


function pdfArray($receiptnumber) {
    $receiptnumber = str_replace('_', '/', $receiptnumber);
    $payment = App\Models\Payment::where(['receipt_number' => $receiptnumber, 'deleted_at' => null, 'status' => '1'])->first();

    if ($payment != null) {
        $returnArray['payment'] = $payment->toArray();
        $returnArray['merchant'] = App\Models\Merchant::find($payment['merchant_id'])->toArray();
        $merchantbankaccountapisetting = App\Models\MerchantBankAccountApiSetting::where('deleted_at', null)->where('merchant_id', $payment['merchant_id'])->first();
        $returnArray['pg_settings'] = json_decode($merchantbankaccountapisetting->pg_settings);
        // $returnArray['payment_log'] = App\Models\PaymentLog::where('pg_transaction_id',$payment['pg_transaction_id'])->first()->toArray();
        $returnArray['payment_log'] = App\Models\PaymentLog::find($payment['payment_log_id'])->toArray();
        if ($returnArray['merchant']['logo'] != '') {
            $uploadFolder = public_path('uploads/');
            $destination_path = $uploadFolder . 'merchant/' . $returnArray['merchant']['logo'];
            if (file_exists($destination_path)) {
                // $returnArray['merchant']['logo'] = "https://".$_SERVER ['SERVER_NAME']."/public/uploads/merchant/".$returnArray['merchant']['logo'];
                $returnArray['merchant']['logo'] = \URL::to('/') . "/uploads/merchant/" . $returnArray['merchant']['logo'];
                $returnArray['merchant']['logoPath'] = $destination_path;
            } else {
                $returnArray['merchant']['logo'] = noSignImage_url();
                $returnArray['merchant']['logoPath'] = noSignImage_path();
            }
        }
        if ($returnArray['merchant']['authorised_signatory'] != '') {
            $uploadFolder = public_path('uploads/');
            $destination_path = $uploadFolder . 'merchant/' . $returnArray['merchant']['authorised_signatory'];
            if (file_exists($destination_path)) {
                // $returnArray['merchant']['authorised_signatory'] = "https://".$_SERVER ['SERVER_NAME']."/public/uploads/merchant/".$returnArray['merchant']['authorised_signatory'];
                $returnArray['merchant']['authorised_signatory'] = \URL::to('/') . "/uploads/merchant/" . $returnArray['merchant']['authorised_signatory'];
                $returnArray['merchant']['authorised_signatoryPath'] = $destination_path;
            }
        } else {
            $returnArray['merchant']['authorised_signatory'] = noSignImage_url();
            $returnArray['merchant']['authorised_signatoryPath'] = noSignImage_path();
        }
        $coutry = getCountryName($returnArray['merchant']['country_id']);
        $state = getStateName($returnArray['merchant']['state_id']);
        $city = getCityName($returnArray['merchant']['city_id']);
        $returnArray['merchant']['address'] = $returnArray['merchant']['full_address'] . ", " . $city . ", " . $state . ", " . $coutry . " - " . $returnArray['merchant']['pincode'];
        $coutry = getCountryName($returnArray['payment']['country_id']);
        $state = getStateName($returnArray['payment']['state_id']);
        $city = getCityName($returnArray['payment']['city_id']);
        $returnArray['payment']['address'] = $returnArray['payment']['full_address'] . ", " . $city . ", " . $state . ", " . $coutry . " - " . $returnArray['payment']['pincode'];
        $returnArray['payment']['date'] = $returnArray['payment']['created_at'];
        $returnArray['payment']['amountWord'] = displaywords($returnArray['payment']['amount']);
        $returnArray['payment']['amount'] = number_format($returnArray['payment']['amount'], 2);
        $returnArray['payment']['rahebar_id'] = getRahebarOrder($returnArray['payment']['rahebar_id']);
        $returnArray['payment']['agent_types_id'] = getAgentsType($returnArray['payment']['agent_types_id']);
        // dd($returnArray);
        return $returnArray;
    } else {
        return false;
    }
}

function pdfOutput($ReceiptNumber, $pdfFileName = "pdf") {
    $returnArray = pdfArray($ReceiptNumber);
    if ($returnArray) {
        $pdf = PDF::loadView('pdf.' . $pdfFileName, compact('returnArray'));
        $pdf->setOptions(['log_output_file' => public_path() . '/uploads/tmp/log.htm', 'temp_dir' => public_path() . '/uploads/tmp']);
        return $pdf->output();
    }
}

function setupSMTP($merchant_id) {
    $merchantsmtp = App\Models\MerchantSmtp::where('deleted_at', null)->where('merchant_id', $merchant_id)->first();
    if ($merchantsmtp != null) {
        $config = array(
            'driver' => 'smtp',
            'host' => $merchantsmtp->host,
            'port' => $merchantsmtp->port,
            'encryption' => $merchantsmtp->encryption,
            'username' => $merchantsmtp->username,
            'password' => $merchantsmtp->password
        );
        Config::set('mail', $config);
        return $merchantsmtp;
    }
    return false;
}

function aashirvadPDF($receiptnumber) {
    $payment = App\Models\Payment::where('receipt_number', str_replace('_', '/', $receiptnumber))->first();
    $rahebar = App\Models\Rahebar::find($payment->rahebar_id);
    $ashirvadletter = App\Models\AshirvadLetter::where('merchant_id', $payment->merchant_id)->where('status', 1)->first();
    // file_put_contents( public_path().'/hindiunicode.ttf', base64_decode(file_get_contents(url('/hindiunicode'))));
    // return view('pdf.aashirvad',compact('ashirvadletter','rahebar'));
    $pdf = mPDF::loadView('pdf.aashirvad', compact('ashirvadletter', 'rahebar'));
    // return $pdf->stream('aashirvad.pdf');
    return $pdf;
}

function getMonth() {
    $result = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
    return $result;
}

function getYear() {
    $startdate = date("Y") - 2;
    $enddate = date("Y") + 5;
    $years = range($startdate, $enddate);
    return $years;
}

function time_ago($date) {
        if (empty($date)) {
            return "No date provided";
        }
        $periods = array("SECOND", "MINUTE", "HOUR", "DAY", "WEEK", "MONTH", "YEAR", "DECADE");
        $lengths = array("59", "59", "23", "6", "4.35", "12", "10");
        $now = time();
        $unix_date = strtotime($date);
            // check validity of date
        if (empty($unix_date)) {
            return "Bad date";
        }
            // is it future date or past date
        if ($now > $unix_date) {
            $difference = $now - $unix_date;
            $tense = "AGO";
        } else {
            $difference = $unix_date - $now;
            $tense = "FROM NOW";
        }
        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }
        $difference = round($difference);
        if ($difference != 1) {
            $periods[$j].= "S";
        }
        return "$difference $periods[$j] {$tense}";
    } 

?>