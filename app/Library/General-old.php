<?php
namespace App\Library;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUser;
use App\Models\UserRequest;
use App\Models\Notification;
use App\Models\Setting;
use Illuminate\Support\Facades\Hash;
use App\Library\General;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use DB;
use Mail;
class General {
    // function to generate random string
    public static function getRandomString($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;   
    }

    //get time array
    public static function getTimeData() {
        return  ["1" => "12:00 AM","2" => "12:30 AM",
                    "3" => "01:00 AM","4" => "01:30 AM",
                    "5" => "02:00 AM","6" => "02:30 AM",
                    "7" => "03:00 AM","8" => "03:30 AM",
                    "9" => "04:00 AM","10" => "04:30 AM",
                    "11" => "05:00 AM","12" => "05:30 AM",
                    "13" => "06:00 AM","14" => "06:30 AM",
                    "15" => "07:00 AM","16" => "07:30 AM",
                    "17" => "08:00 AM","18" => "08:30 AM",
                    "19" => "09:00 AM","20" => "09:30 AM",
                    "20" => "10:00 AM","21" => "10:30 AM",
                    "22" => "11:00 AM","23" => "11:30 AM",
                    "24" => "12:00 PM","25" => "12:30 PM",
                    "26" => "01:00 PM","27" => "01:30 PM",
                    "28" => "02:00 PM","29" => "02:30 PM",
                    "30" => "03:00 PM","31" => "03:30 PM",
                    "32" => "04:00 PM","33" => "04:30 PM",
                    "34" => "05:00 PM","35" => "05:30 PM",
                    "36" => "06:00 PM","37" => "06:30 PM",
                    "38" => "07:00 PM","39" => "07:30 PM",
                    "40" => "08:00 PM","41" => "08:30 PM",
                    "42" => "09:00 PM","43" => "09:30 PM",
                    "44" => "10:00 PM","45" => "10:30 PM",
                    "46" => "11:00 PM","47" => "11:30 PM"
                ];
    }

    //get reminder time array
    public static function reminderData() {
        return ["1" => "Minutes","2" => "Hours","3" => "Days","4" => "Weeks"];
    }

    //address type
    public static function addressTypeArr() {
        return ['1'=>'Office', '2' => 'Residential'];   
    }

    //importance arr
    public static function importanceArr() {
        return ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
    }

    //causion arr
    public static function causionArr() {
        return ['1' => 'Low' , '2' => 'Medium', '3' => 'High'];
    }

    //sensitivity arr
    public static function sensitivityArr() {
        return ['1' => 'Low' , '2' => 'Medium', '3' => 'High'];
    }

    public static function pushNotification($expertId,$userId,$message,$type,$question_id){


        // $userObj = User::where('id',$userId)->where('is_verified',1)->first();
        $userObj = AppUser::where('id',$userId)->first();

        if(count($userObj) > 0){
            $getdeviceID = $userObj->device_type;
            if($getdeviceID == 1){
                $setting = Setting::where('slug','firebug_key')->first();
                if(count($setting) > 0){
                    $apikey = $setting->value;
                    $registrationId = $userObj->device_id;
                    $sendNotification = General::androidPushNotification($apikey,$registrationId,$message,$type,$question_id);
                }
            }elseif($getdeviceID == 2){

                $setting = Setting::where('slug','sandbox_pem')->first();
                if(count($setting) > 0){
                    $iphoneData = array("aps" => array(
                                                'question_id' => $question_id,
                                                "NT" => $type,
                                                'alert' => $message,
                                                "badge" => '1',
                                                "sound" => "default"
                                                )
                                                // "NT" => $NT
                                            );
                    $path = public_path().$setting->value;

                    $deviceToken = $userObj->device_id;
                    $sendNotification = General::iosPushNotification('3abcbd633766e40072e1f84df6680b9cba90420a751fd7d3276b793f1de4841f',$iphoneData,$path);
                }
            }
        }
    }

    public static function androidPushNotification($apikey,$registrationId,$message,$type,$question_id){
        
        $registrationIds = array($registrationId);
        
        $msg = array
          (
        'body'  => $message,
        'question_id' => $question_id,
        'title' => 'Ilmpal',
        'icon'  => 'myicon',/*Default Icon*/
        'sound' => 'mySound'/*Default sound*/
        );

        $fields = array
        (
            'registration_ids'  => $registrationIds,
            'data'          => $msg,
            'NT'          => $type
        );
        // echo "<prE>";
        // print_r($registrationIds);
        // exit;
                 
        $headers = array
        (
            'Authorization: key='.$apikey,
            'Content-Type: application/json'
        );
         
        $ch = curl_init();
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        // echo $result;
        // exit;
    }

    public static function iosPushNotification($deviceToken, $body = array(),$path) {

        
        $passphrase = '1';

        $ctx = @stream_context_create();

       
        // For live
        @stream_context_set_option($ctx, 'ssl', 'local_cert',$path);
        
        @stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // for send box : ssl://gateway.sandbox.push.apple.com:2195
        // for live : ssl://gateway.push.apple.com:2195
        $fp = @stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        //if (!$fp)
            
            // exit("Failed to connect: $err $errstr" . PHP_EOL);
        if (!$fp){
            //echo phpinfo();
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        }
        $payload = json_encode($body);

        $msg = chr(0) . pack('n', 32) . pack('H*', '3abcbd633766e40072e1f84df6680b9cba90420a751fd7d3276b793f1de4841f') . pack('n', strlen($payload)) . $payload;

        $result = fwrite($fp, $msg, strlen($msg));

       	if (!$result){
           echo 'Message not delivered' . PHP_EOL;
      	}
        
        fclose($fp);
        return $result;
    }

}