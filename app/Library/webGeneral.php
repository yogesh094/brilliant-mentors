<?php
namespace App\Library;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Collective\Html\Eloquent\FormAccessible;
use App\Models\User;
use App\Models\AppUser;
use App\Models\UserRequest;
use App\Models\Notification;
use App\Models\Setting;
use App\Models\Question;
use App\Models\Appointment;
use Illuminate\Support\Facades\Hash;
use App\Library\webGeneral;
use DateTime;
use Html;
use File;
use Input;
use Validator;
use Redirect;
use View;
use Storage;
use Auth;
use DB;
use Mail;
use Session;
use DateTimeImmutable;
use DateInterval;
use DatePeriod;

class webGeneral {
    // function to generate random string
    

    //get time array
    
  public static function top_notification(){
    if(Auth::check()){
        $NotificationObj = Notification::where('appuser_id',Auth::user()->id)->where('is_read',0)->where('language_id',Session::get('selectedLang'))->orderBy('id', 'desc')->limit(5)->get();
        $data = [];
        foreach ($NotificationObj as $key => $notifications) {
          $data[$key]['id'] = $notifications->id;
          $data[$key]['type_id'] = $notifications->type_id;
          $data[$key]['question_id'] = $notifications->question_id;
          $data[$key]['message'] = $notifications->message;
          $data[$key]['created_at'] = date('Y-m-d H:i:s', strtotime($notifications->created_at));
          $description = "";
          $answer = "";
          if($notifications->type_id == 1){
            $questionObj = Question::where('id',$notifications->question_id)->first();
            if(!empty($questionObj)){
              $description = str_limit($questionObj->question,100);
              $answer = $questionObj->answer;

            }
          }else if($notifications->type_id == 0){
              $AppointmentObj = Appointment::where('id',$notifications->question_id)->first();
              if(!empty($AppointmentObj)){
                 $description = str_limit($AppointmentObj->description,200);
              }
          }
          $data[$key]['description'] = $description;
          $data[$key]['answer'] = $answer;
        }

        return $data;
    }
    return false;
  }
  public static function TimeAgo ($oldTime, $newTime) {

    $timeCalc = strtotime($newTime) - strtotime($oldTime);
    if ($timeCalc >= (60*60*24*30*12*2)){
    $timeCalc = intval($timeCalc/60/60/24/30/12) . " years ago";
    }else if ($timeCalc >= (60*60*24*30*12)){
        $timeCalc = intval($timeCalc/60/60/24/30/12) . " year ago";
    }else if ($timeCalc >= (60*60*24*30*2)){
        $timeCalc = intval($timeCalc/60/60/24/30) . " months ago";
    }else if ($timeCalc >= (60*60*24*30)){
        $timeCalc = intval($timeCalc/60/60/24/30) . " month ago";
    }else if ($timeCalc >= (60*60*24*2)){
        $timeCalc = intval($timeCalc/60/60/24) . " days ago";
    }else if ($timeCalc >= (60*60*24)){
        $timeCalc = " Yesterday";
    }else if ($timeCalc >= (60*60*2)){
        $timeCalc = intval($timeCalc/60/60) . " hours ago";
    }else if ($timeCalc >= (60*60)){
        $timeCalc = intval($timeCalc/60/60) . " hour ago";
    }else if ($timeCalc >= 60*2){
        $timeCalc = intval($timeCalc/60) . " minutes ago";
    }else if ($timeCalc >= 60){
        $timeCalc = intval($timeCalc/60) . " minute ago";
    }else if ($timeCalc > 0){
        $timeCalc .= " seconds ago";
    }
return $timeCalc;
}
public static function timeInterval($startTime, $endTime) {
  //strtotime($startTime)
  $Range = [];
        if($startTime < $endTime) {

            $slotTime = date("H:i", strtotime('+30 minutes', $startTime));
            $Range['time'] = date('H:i',$startTime)."-".$slotTime."<br>";
            // Resursive function call
            echo $Range['time'];
            webGeneral::timeInterval(strtotime($slotTime), $endTime);
        }
      
    }
public static function hoursRange($startTime = 0,$endTime = 0){
  
  $start = new DateTimeImmutable($startTime);
  $end = new DateTimeImmutable($endTime);
  $interval = new DateInterval('PT30M'); //30 minute interval
  $range = new DatePeriod($start, $interval, $end);
  $timeArr = [];
    foreach ($range as $time) {
      $key = $time->format('H:i')."-".$time->add($interval)->format('H:i');
      $value = $time->format('g:i A')."-".$time->add($interval)->format('g:i A');
      $timeArr[$key] = $value;
    //echo $time->format('H:i'), "-", $time->add($interval)->format('H:i');
}
  return $timeArr;
}
public static function hoursRangeOld($startTime = 0,$endTime = 0) {
    $lower = 0;
    $upper = 86400;
    $step = 3600;
    $format = '';
    $times = array();

    if ( empty( $format ) ) {
        $format1 = 'g:i';
        $format = 'g:i A';
    }

    foreach ( range( $lower, $upper, $step ) as $increment ) {
        $increment = gmdate( 'H:i', $increment );

        list( $hour, $minutes ) = explode( ':', $increment );
        list( $hourKey, $minutesKey ) = explode( ':', $increment );

        $date = new DateTime( $hour . ':' . $minutes );
        $date1 = new DateTime( $hour . ':' . ($minutes+30) );
        if($increment >= $startTime && $increment <= $endTime){
            $key = $increment .'-'.$hourKey.':'.($minutesKey+30);
            $times[(string) $key] = $date->format( $format1 ) .'-'. $date1->format( $format );
        }
        
    }

    return $times;
}

  public static function check48hours ($oldTime, $newTime) {
    $time48hours = strtotime($newTime) - strtotime($oldTime);
    $time48hours = intval($time48hours/60/60);
    return $time48hours;
  }
}