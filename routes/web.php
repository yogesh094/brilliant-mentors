<?php


Route::get('/', 'Web\WebController@index');

Route::get('/user-profile/{id}','Web\ProfileController@aboutProfile')->name('user-profile');
Route::any('user-profile/image/{id}','Web\ProfileController@saveUserProfilePic')->name('user-profile-image');
Route::get('/user-profile-timeline/{id}','Web\ProfileController@aboutProfileTimeline')->name('user-timeline');

Route::get('/news-feed', 'Web\NewsFeedController@index');
Route::get('/news-test', 'Web\TimelineController@index')->name('news-test');


//App page
Route::get('/aboutusAppEnglish', 'Web\WebController@aboutusAppEnglish');
Route::get('/aboutusAppArabic', 'Web\WebController@aboutusAppArabic');

Route::get('/privacyAppEnglish', 'Web\WebController@privacyAppEnglish');
Route::get('/privacyAppArabic', 'Web\WebController@privacyAppArabic');

Route::get('/helpAppEnglish', 'Web\WebController@helpAppEnglish');
Route::get('/helpAppArabic', 'Web\WebController@helpAppArabic');

Route::get('/contactAppEnglish', 'Web\WebController@contactAppEnglish');
Route::get('/contactAppArabic', 'Web\WebController@contactAppArabic');

Route::any('/contactAppSendMail', 'Web\WebController@contactAppSendMail');
//

Route::get('/404', 'Web\WebController@pageNotFound');
Route::get('/home', 'Web\WebController@index');
Route::get('/aboutus', 'Web\WebController@about_us');
Route::get('/resentPasswordMsg', 'Web\AuthController@resentPasswordMsg');

Route::get('/contactus', 'Web\WebController@contact');
Route::any('/forgotpassword', 'Web\AuthController@forgotPassword');
Route::get('/resent-password/{id}', 'Web\AuthController@resentPasswors');

Route::any('/registration', 'Web\AuthController@appUserCreate');
Route::any('/login', 'Web\WebController@index')->name('login');
Route::any('/logout', 'Web\AuthController@logout')->name('logout');
Route::any('/getlanguage', 'Web\WebController@setLanguages');
Route::any('/contactSendMail', 'Web\WebController@contactSendMail');
Route::get('/privacy-policy', 'Web\WebController@privacyPolicy');
Route::get('auth/facebook', 'Web\SocialiteController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Web\SocialiteController@handleFacebookCallback');

Route::get('auth/google', 'Web\SocialiteController@redirectToGoogle');
Route::get('auth/google/callback', 'Web\SocialiteController@handleGoogleCallback');
Route::any('/account-activation/{id}', 'Web\AuthController@activateRegisterAccount');
Route::get('/video-consultaion', 'Web\AppointmentController@videoConsultaion');

Route::any('/question/view/{id}', 'Web\QuestionController@viewQuestionAnswer');
Route::any('/question-answer/{id}', 'Web\QuestionController@getAnswer');

Route::get('activateRegisterAccount', ['as' => 'activateRegisterAccount', 'uses' => 'Web\AuthController@activateRegisterAccountView']);

Route::any('/storeprofile', 'Web\ProfileController@saveProfile')->name('storeprofile');
Route::any('/resetPasswordStore', 'Web\AuthController@resetPasswordStore')->name('resetpasswordstore');
Route::get('/question-search', 'Web\QuestionController@questionSearch');
Route::get('/getParentCategory', 'Web\QuestionController@getParentCategory');
Route::any('/questionSearchResult', 'Web\QuestionController@questionSearchResult');
Route::get('/ask-question', 'Web\QuestionController@askQuestion');

Route::get('/help', 'Web\WebController@helpPage');

Route::get('/suggestedQuestion', 'Web\QuestionController@suggestedQuestion');
//After User Login
Route::any('/checkTimeConsultaion', 'Web\AppointmentController@checkTimeConsultaion');
    Route::any('/loginCheck', 'Web\AuthController@loginAuth')->name('loginCheck');

Route::group(['prefix' => '', 'middleware' => 'auth:user'], function () {

    Route::get('/change-password','Web\ProfileController@changePassword')->name('change-password');

    Route::post('/save-reset-Password', 'Web\WebController@saveResetPassword');
    Route::any('/education-block', 'Web\ProfileController@education_block')->name('education-block');
    Route::any('/employment-block', 'Web\ProfileController@employment_block')->name('employment-block');
    Route::post('/employment-save', 'Web\ProfileController@save_employment')->name('employment-save');
    Route::post('/education-save', 'Web\ProfileController@save_education')->name('education-save');

    Route::get('/education','Web\ProfileController@education')->name('education');
    //Route::post('/save-post','Web\TimelineController@savePost')->name('save-post');
    Route::post('/save-post','Web\NewsFeedController@savePost')->name('save-post');
    Route::get('/hobbies-interest','Web\ProfileController@hobbiesAndInterests')->name('hobbies-interest');
    Route::any('/save-interest','Web\ProfileController@saveInterest')->name('save-interest');

    Route::get('/question', 'Web\QuestionController@getQuestion');

    Route::any('/storeQuestion', 'Web\QuestionController@storeQuestion');

    Route::any('/videoConsultaionStore', 'Web\AppointmentController@videoConsultaionStore');
    Route::get('/payment/{id}', 'Web\AppointmentController@payment');
    Route::any('/storePayment', 'Web\AppointmentController@storePayment');
    Route::get('/district-select','Web\ProfileController@selectDistrict')->name('selectDistrict');

    Route::post('/sub-stream-select','Web\ProfileController@selectSubStream')->name('selectSubStream');
    Route::get('/profile','Web\ProfileController@profileInformation')->name('profile');


    Route::any('/upload-profile','Web\ProfileController@uploadProfile')->name('uploadProfile');

    Route::any('/video-consultaion/reschedule/{id}', 'Web\AppointmentController@rescheduleConsultaion');

    Route::any('/video-consultaion/rescheduleStore', 'Web\AppointmentController@videoConsultaionReschedule');

    
    Route::any('/notification', 'Web\NotificationController@getAllNotification');
    Route::any('/notification/view/{id}', 'Web\NotificationController@getQuestionAnswer');

    Route::any('/appointmentList', 'Web\AppointmentController@yourAppointmentList');
    Route::any('/refundAppointment', 'Web\AppointmentController@refundAppointment');
    Route::any('/refund/{token}', 'Web\AppointmentController@refundByEmail');
    Route::any('/refunds', 'Web\AppointmentController@refundView');
    Route::any('/reschedule', 'Web\AppointmentController@rescheduleView');

    Route::any('/thank-you/{id}', 'Web\AppointmentController@thankYou');
    Route::any('/cancelAppointment', 'Web\AppointmentController@cancelAppointment');
    Route::any('/video-call', 'Web\AppointmentController@videoCall');
    Route::get('/video-consultaion/{id}', 'Web\AppointmentController@videoConsultaion');
});

Route::group(['prefix' => 'panel', 'middleware' => 'guest:panel'], function () {

    Route::get('/', ['as' => 'panel', 'uses' => 'Admin\Auth\AuthController@index']);

    //Admin Login
    Route::get('login', ['as' => 'login', 'uses' => 'Admin\Auth\AuthController@index']);
    Route::post('loginauth', 'Admin\Auth\AuthController@loginAuth');
});

Route::get('/user/reset-password/{token}', 'Web\WebController@resetPassword');
Route::post('/user/setPassword', 'Web\WebController@savePassword');
Route::get('/user/reset-password-confirm', 'Web\WebController@resetConfirm');
Route::get('/expert/confirm/{token}', 'Web\WebController@userConfirm');
Route::get('/user/account/confirm', 'Web\WebController@userAccountConfirm');


// Route::get('/terms', 'Web\WebController@termsCondition');
// Route::get('/expireRequest', 'Web\WebController@expireRequest');
// Route::get('/account-activation/{id}', 'Api\BasicController@changePasswordConfirm');


