<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// UserController Controller
Route::post('/register', 'Api\UserController@register');
Route::post('/login', 'Api\UserController@login');
Route::post('/forgotPassword', 'Api\UserController@forgotPassword');
Route::post('/verifycode', 'Api\UserController@verifycode');
Route::post('/resetPassword', 'Api\UserController@resetPassword');
Route::post('/changePassword', 'Api\GeneralController@changePassword');
Route::post('/getProfile', 'Api\GeneralController@getProfile');
Route::post('/getCategoryAppointment', 'Api\GeneralController@getCategoryAppointment');



// Question Controller
Route::post('/addQuestion', 'Api\QuestionController@addQuestion');
Route::post('/searchQuestion', 'Api\QuestionController@searchQuestion');
Route::post('/quesionSuggestion', 'Api\QuestionController@quesionSuggestion');
Route::post('/getAnswer', 'Api\QuestionController@getAnswer');
Route::post('/getLatestQuestionData', 'Api\QuestionController@getLatestQuestionData');
Route::post('/getNotification', 'Api\AppointmentController@getNotification');
Route::post('/readNotification', 'Api\AppointmentController@readNotification');
Route::post('/getQuestion', 'Api\QuestionController@getQuestion');


// GeneralController
Route::post('/getLanguage', 'Api\GeneralController@getLanguage');
Route::post('/setLanguage', 'Api\GeneralController@setLanguage');
Route::post('/getLanguageKeyword', 'Api\GeneralController@getLanguageKeyword');
Route::post('/getCategory', 'Api\GeneralController@getCategory');
Route::post('/getTopics', 'Api\GeneralController@getTopics');
Route::post('/logout', 'Api\UserController@logout');
Route::post('/getSlider', 'Api\GeneralController@getSlider');
Route::post('/setting', 'Api\GeneralController@setting');
Route::post('/getBookingTime', 'Api\GeneralController@getBookingTime');


//AppointmentController


Route::post('/bookAppointment', 'Api\AppointmentController@bookAppointment');
Route::post('/getAppointment', 'Api\AppointmentController@getAppointment');
Route::post('/paymetStore', 'Api\PaymentController@paymetStore');
Route::post('/refundPayment', 'Api\PaymentController@refundPayment');
Route::post('/cancelAppointment', 'Api\AppointmentController@cancelAppointment');


Route::post('/aboutUs', 'Api\UserController@aboutUs');
Route::post('/policy', 'Api\UserController@policy');
Route::post('/help', 'Api\UserController@help');


Route::post('/allPages', 'Api\PagesController@allPages');
Route::post('/TermCondition', 'Api\PagesController@TermCondition');

Route::get('/test', 'Api\PagesController@test');







