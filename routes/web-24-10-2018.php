<?php

//App page
Route::get('/aboutusAppEnglish', 'Web\WebController@aboutusAppEnglish');
Route::get('/aboutusAppArabic', 'Web\WebController@aboutusAppArabic');

Route::get('/privacyAppEnglish', 'Web\WebController@privacyAppEnglish');
Route::get('/privacyAppArabic', 'Web\WebController@privacyAppArabic');

Route::get('/helpAppEnglish', 'Web\WebController@helpAppEnglish');
Route::get('/helpAppArabic', 'Web\WebController@helpAppArabic');

Route::get('/contactAppEnglish', 'Web\WebController@contactAppEnglish');
Route::get('/contactAppArabic', 'Web\WebController@contactAppArabic');

Route::any('/contactAppSendMail', 'Web\WebController@contactAppSendMail');
//
Route::get('/', 'Web\WebController@index');
Route::get('/404', 'Web\WebController@pageNotFound');
Route::get('/home', 'Web\WebController@index');
Route::get('/aboutus', 'Web\WebController@about_us');
Route::get('/resentPasswordMsg', 'Web\AuthController@resentPasswordMsg');
Route::any('/loginCheck', 'Web\WebController@loginCheck');
Route::get('/contactus', 'Web\WebController@contact');
Route::any('/forgotpassword', 'Web\AuthController@forgotPassword');
Route::get('/resent-password/{id}', 'Web\AuthController@resentPasswors');
Route::any('/resentPasswordStore', 'Web\AuthController@resentPasswordStore');
Route::any('/registration', 'Web\AuthController@appUserCreate');
Route::any('/login', 'Web\AuthController@loginAuth');
Route::any('/logout', 'Web\AuthController@logout');
Route::any('/getlanguage', 'Web\WebController@setLanguages');
Route::any('/contactSendMail', 'Web\WebController@contactSendMail');
Route::get('/privacy-policy', 'Web\WebController@privacyPolicy');
Route::get('auth/facebook', 'Web\SocialiteController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Web\SocialiteController@handleFacebookCallback');

Route::get('auth/google', 'Web\SocialiteController@redirectToGoogle');
Route::get('auth/google/callback', 'Web\SocialiteController@handleGoogleCallback');
Route::any('/account-activation/{id}', 'Web\AuthController@activateRegisterAccount');
Route::get('/video-consultaion', 'Web\AppointmentController@videoConsultaion');

Route::any('/question/view/{id}', 'Web\QuestionController@viewQuestionAnswer');
Route::any('/question-answer/{id}', 'Web\QuestionController@getAnswer');

Route::get('activateRegisterAccount', ['as' => 'activateRegisterAccount', 'uses' => 'Web\AuthController@activateRegisterAccountView']);


Route::get('/question-search', 'Web\QuestionController@questionSearch');
Route::get('/getParentCategory', 'Web\QuestionController@getParentCategory');
Route::any('/questionSearchResult', 'Web\QuestionController@questionSearchResult');
Route::get('/ask-question', 'Web\QuestionController@askQuestion');

Route::get('/help', 'Web\WebController@helpPage');

Route::get('/suggestedQuestion', 'Web\QuestionController@suggestedQuestion');
//After User Login
Route::any('/checkTimeConsultaion', 'Web\AppointmentController@checkTimeConsultaion');
Route::group(['prefix' => '', 'middleware' => 'guest:user'], function () {
    Route::get('/question', 'Web\QuestionController@getQuestion');

    Route::any('/storeQuestion', 'Web\QuestionController@storeQuestion');

    Route::any('/videoConsultaionStore', 'Web\AppointmentController@videoConsultaionStore');
    Route::get('/payment/{id}', 'Web\AppointmentController@payment');
    Route::any('/storePayment', 'Web\AppointmentController@storePayment');

    Route::any('/video-consultaion/reschedule/{id}', 'Web\AppointmentController@rescheduleConsultaion');

    Route::any('/video-consultaion/rescheduleStore', 'Web\AppointmentController@videoConsultaionReschedule');

    Route::get('/profile', 'Web\AuthController@profile');
    Route::any('/storeProfile', 'Web\AuthController@storeProfile');
    Route::any('/notification', 'Web\NotificationController@getAllNotification');
    Route::any('/notification/view/{id}', 'Web\NotificationController@getQuestionAnswer');

    Route::any('/appointmentList', 'Web\AppointmentController@yourAppointmentList');
    Route::any('/refundAppointment', 'Web\AppointmentController@refundAppointment');
    Route::any('/refund/{token}', 'Web\AppointmentController@refundByEmail');
    Route::any('/refunds', 'Web\AppointmentController@refundView');
    Route::any('/reschedule', 'Web\AppointmentController@rescheduleView');

    Route::any('/thank-you/{id}', 'Web\AppointmentController@thankYou');
    Route::any('/cancelAppointment', 'Web\AppointmentController@cancelAppointment');
    Route::any('/video-call', 'Web\AppointmentController@videoCall');
    Route::get('/video-consultaion/{id}', 'Web\AppointmentController@videoConsultaion');
});

Route::group(['prefix' => 'panel', 'middleware' => 'guest:panel'], function () {

    Route::get('/', ['as' => 'panel', 'uses' => 'Admin\Auth\AuthController@index']);

    //Admin Login
    Route::get('login', ['as' => 'login', 'uses' => 'Admin\Auth\AuthController@index']);
    Route::post('loginauth', 'Admin\Auth\AuthController@loginAuth');
});

Route::get('/user/reset-password/{token}', 'Web\WebController@resetPassword');
Route::post('/user/setPassword', 'Web\WebController@savePassword');
Route::post('/user/save-reset-Password', 'Web\WebController@saveResetPassword');
Route::get('/user/reset-password-confirm', 'Web\WebController@resetConfirm');
Route::get('/expert/confirm/{token}', 'Web\WebController@userConfirm');
Route::get('/user/account/confirm', 'Web\WebController@userAccountConfirm');

Route::group(['prefix' => 'panel', 'middleware' => 'auth:panel'], function () {

    Route::any('/logout', 'Admin\Auth\AuthController@getLogout');
    Route::get('/dashboard', 'Admin\DashboardController@index');
    Route::get('/add', 'Admin\DashboardController@add');

    //////////////////////////////////User Management//////////////////////////////////
    Route::get('/user', 'Admin\UserController@index');
    Route::get('/user/create', 'Admin\UserController@create');
    Route::post('/user/store', 'Admin\UserController@store');
    Route::post('/user/destroy', 'Admin\UserController@destroy');
    Route::get('/user/edit/{id}', 'Admin\UserController@edit');
    Route::get('/confirm-pass', 'Admin\UserController@editConfirmPass');
    Route::any('/profile', 'Admin\UserController@viewProfile');
    Route::any('/profileStore', 'Admin\UserController@profileStore');
    Route::any('/change-password', 'Admin\UserController@viewChangePassword');
    Route::any('/changepassword', 'Admin\UserController@changePassword');
    Route::get('/user/reset-pass-link', 'Admin\UserController@sendResetPasswordLink');
    // Datatables
    Route::any('/user/array-data', 'Admin\UserController@arrayData');
    Route::any('/user/download/excel', 'Admin\UserController@download');
    //////////////////////////////////User Management//////////////////////////////////

    Route::get('/role', ['as' => 'role', 'uses' => 'Admin\RoleController@index']); //,'middleware' => ['permission:list']

    Route::get('/role/list', ['as' => '/role/list', 'uses' => 'Admin\RoleController@roleList']); //,'middleware' => ['permission:list']

    Route::get('/role/add', ['as' => '/role/add', 'uses' => 'Admin\RoleController@addEditRole']); //,'middleware' => ['permission:add']

    Route::get('/role/edit/{id}', ['as' => '/role/edit/{id}', 'uses' => 'Admin\RoleController@addEditRole']); //,'middleware' => ['permission:edit']

    Route::post('/role/store', ['as' => '/role/store', 'uses' => 'Admin\RoleController@storeRole']); //,'middleware' => ['permission:add']

    Route::get('/role/destroy/{id}', ['as' => '/role/destroy/{id}', 'uses' => 'Admin\RoleController@destroyRole']); //,'middleware' => ['permission:delete']

    Route::get('/general-setting', ['as' => '/general-setting', 'uses' => 'Admin\GeneralSettingController@index', 'middleware' => ['permission:view']]);

    Route::post('/general-setting/store', ['as' => '/general-setting/store', 'uses' => 'Admin\GeneralSettingController@store', 'middleware' => ['permission:create']]);


    Route::get('/appuser', ['as' => '/appuser', 'uses' => 'Admin\AppUserController@index', 'middleware' => ['permission:list']]);

    Route::get('/appuser/array-data', ['as' => '/appuser/array-data', 'uses' => 'Admin\AppUserController@arrayData', 'middleware' => ['permission:list']]);

    Route::get('/appuser/create', ['as' => '/appuser/create', 'uses' => 'Admin\AppUserController@create', 'middleware' => ['permission:create']]);

    Route::post('/appuser/store', ['as' => '/appuser/store', 'uses' => 'Admin\AppUserController@store', 'middleware' => ['permission:create']]);
    Route::post('/appuser/pageStoreNumber', ['as' => '/appuser/pageStoreNumber', 'uses' => 'Admin\AppUserController@pageStoreNumber', 'middleware' => ['permission:list']]);

    Route::any('/appuser/destroy/{id}', ['as' => '/appuser/destroy/{id}', 'uses' => 'Admin\AppUserController@destroy', 'middleware' => ['permission:delete']]);

    Route::get('/appuser/edit/{id}', ['as' => '/appuser/edit/{id}', 'uses' => 'Admin\AppUserController@edit', 'middleware' => ['permission:edit']]);
    Route::any('/appuser/updatemultirecode', 'Admin\AppUserController@updatemultirecode');

    // Route::get('/appuser', 'Admin\AppUserController@index');
    // Route::get('/appuser/create', 'Admin\AppUserController@create');
    // Route::post('/appuser/store', 'Admin\AppUserController@store');
    // Route::get('/appuser/destroy', 'Admin\AppUserController@destroy');
    // Route::get('/appuser/edit/{id}', 'Admin\AppUserController@edit');
    // Datatables
    // Route::any('/appuser/array-data', 'Admin\AppUserController@arrayData');
    // Route::get('/general-setting', 'Admin\GeneralSettingController@index');
    // Route::post('/general-setting/store', 'Admin\GeneralSettingController@store');
    ////Language //////
    Route::get('/language', ['as' => '/language', 'uses' => 'Admin\LanguageController@index', 'middleware' => ['permission:list']]);
    Route::any('/language/add', ['as' => '/language/add', 'uses' => 'Admin\LanguageController@add', 'middleware' => ['permission:add']]);
    Route::any('/language/array-data', 'Admin\LanguageController@arrayData');
    Route::any('/language/edit/{id}', ['as' => '/language/edit/{id}', 'uses' => 'Admin\LanguageController@edit', 'middleware' => ['permission:edit']]);
    Route::post('/language/store', 'Admin\LanguageController@store');
    Route::any('/language/delete/{id}', ['as' => '/language/delete/{id}', 'uses' => 'Admin\LanguageController@delete', 'middleware' => ['permission:delete']]);

    ////Language //////
    ////Language Keyword//////

    Route::get('/language/keyword', ['as' => '/language/keyword', 'uses' => 'Admin\LanguageKeywordController@index', 'middleware' => ['permission:keyword']]);
    Route::get('/language/keyword/add', 'Admin\LanguageKeywordController@add');
    Route::post('/language/keyword/store', 'Admin\LanguageKeywordController@store');
    Route::post('/language/keyword/multiStore', 'Admin\LanguageKeywordController@multiStore');
    Route::any('/language/keyword/getlangkeyword', 'Admin\LanguageKeywordController@getlangkeyword');
    Route::get('/language/keyword/delete/{id}', 'Admin\LanguageKeywordController@delete');
    //Route::get('/language/keyword/edit/{id}', 'Admin\LanguageKeywordController@edit');
    Route::any('/language/keyword/edit/{id}', 'Admin\LanguageKeywordController@edit');
    // Datatables
    Route::get('/language/keyword/array-data', 'Admin\LanguageKeywordController@arrayData');
    ////Language Keyword///////
    ////question //////
    Route::get('/question', 'Admin\QuestionController@index');
    Route::any('/question/add', 'Admin\QuestionController@add');
    Route::get('/question/add', ['as' => '/question/add', 'uses' => 'Admin\QuestionController@add', 'middleware' => ['permission:create']]);
    Route::any('/question/array-data', 'Admin\QuestionController@arrayData');
    Route::any('/question/edit/{id}', 'Admin\QuestionController@edit');
    Route::post('/question/store', 'Admin\QuestionController@store');
    Route::any('/question/delete/{id}', 'Admin\QuestionController@delete');
    Route::any('/question/pageStoreNumber', 'Admin\QuestionController@pageStoreNumber');
    Route::get('/question/getquestion', 'Admin\QuestionController@getquestion');
    Route::get('/question/getCategoryByLanguage', 'Admin\QuestionController@getCategoryByLanguage');
    Route::get('/question/getTopicByLanguage', 'Admin\QuestionController@getTopicByLanguage');
    Route::any('/question/Assignquestion', 'Admin\QuestionController@Assignquestion');
    Route::any('/question/updatemultirecode', 'Admin\QuestionController@updatemultirecode');
    Route::get('/question/getParentCategory', 'Admin\QuestionController@getParentCategory');
    ////question //////
    ///////// CMS  /////////////////	
    Route::get('/cms', ['as' => '/cms', 'uses' => 'Admin\CMSController@index', 'middleware' => ['permission:list']]);
    Route::get('/cms/create', ['as' => '/cms/create', 'uses' => 'Admin\CMSController@create', 'middleware' => ['permission:create']]);
    Route::any('/cms/edit/{id}', ['as' => '/cms/edit/{id}', 'uses' => 'Admin\CMSController@edit', 'middleware' => ['permission:edit']]);
    Route::get('/cms/destroy/{id}', ['as' => '/cms/destroy/{id}', 'uses' => 'Admin\CMSController@destroy', 'middleware' => ['permission:delete']]);
    Route::any('/cms/array-data', 'Admin\CMSController@arrayData');
    Route::any('/cms/store', 'Admin\CMSController@store');

    ///////// CMS  /////////////////
    ///////// EMAIL TEMPLATE  /////////////////	
    Route::get('/email-template', ['as' => '/email-template', 'uses' => 'Admin\EmailTemplateController@index', 'middleware' => ['permission:list']]);
    Route::get('/email-template/create', ['as' => '/email-template/create', 'uses' => 'Admin\EmailTemplateController@create', 'middleware' => ['permission:create']]);
    Route::any('/email-template/edit/{id}', ['as' => '/email-template/edit/{id}', 'uses' => 'Admin\EmailTemplateController@edit', 'middleware' => ['permission:edit']]);

    Route::any('/email-template/array-data', 'Admin\EmailTemplateController@arrayData');
    Route::any('/email-template/store', 'Admin\EmailTemplateController@store');
    Route::get('/email-template/destroy/{id}', 'Admin\EmailTemplateController@destroy');


    ///////// EMAIL TEMPLATE  /////////////////
    ///////// Slider  /////////////////	
    Route::get('/slider', ['as' => '/slider', 'uses' => 'Admin\SliderController@index', 'middleware' => ['permission:list']]);
    Route::get('/slider/create', ['as' => '/slider/create', 'uses' => 'Admin\SliderController@create', 'middleware' => ['permission:create']]);
    Route::any('/slider/edit/{id}', ['as' => '/slider/edit/{id}', 'uses' => 'Admin\SliderController@edit', 'middleware' => ['permission:edit']]);
    Route::get('/slider/destroy/{id}', ['as' => '/slider/destroy/{id}', 'uses' => 'Admin\SliderController@destroy', 'middleware' => ['permission:delete']]);
    Route::any('/slider/array-data', 'Admin\SliderController@arrayData');
    Route::any('/slider/store', 'Admin\SliderController@store');
    //Route::get('/slider/destroy/{id}', 'Admin\SliderController@destroy');
    ///////// Slider  /////////////////
    ///////// Order  /////////////////	
    Route::get('/orders', ['as' => '/orders', 'uses' => 'Admin\OrdersController@index', 'middleware' => ['permission:list']]);
    Route::get('/orders/delete/{id}', ['as' => '/orders/delete/{id}', 'uses' => 'Admin\OrdersController@delete', 'middleware' => ['permission:delete']]);
    Route::any('/orders/view/{id}', ['as' => '/orders/view/{id}', 'uses' => 'Admin\OrdersController@view', 'middleware' => ['permission:view']]);
    Route::any('/orders/array-data', 'Admin\OrdersController@arrayData');
    Route::any('/orders/updatemultirecode', 'Admin\OrdersController@updatemultirecode');
    ///////// Order  /////////////////
    ////Category //////
    Route::get('/category', ['as' => '/category', 'uses' => 'Admin\CategoryController@index', 'middleware' => ['permission:list']]);
    Route::get('/category/add', ['as' => '/category/add', 'uses' => 'Admin\CategoryController@add', 'middleware' => ['permission:create']]);
    Route::any('/category/edit/{id}', ['as' => '/category/edit/{id}', 'uses' => 'Admin\CategoryController@edit', 'middleware' => ['permission:edit']]);
    Route::any('/category/delete/{id}', ['as' => '/category/delete/{id}', 'uses' => 'Admin\CategoryController@delete', 'middleware' => ['permission:delete']]);
    Route::any('/category/array-data', 'Admin\CategoryController@arrayData');
    Route::post('/category/store', 'Admin\CategoryController@store');
    Route::any('/category/updatemultirecode', 'Admin\CategoryController@updatemultirecode');
    Route::any('/category/getParentCategory', 'Admin\CategoryController@getParentCategory');
    ////Category //////
////Category appoinment //////
    Route::get('/appointment/category', ['as' => '/appointment/category', 'uses' => 'Admin\CategoryAppointmentController@index', 'middleware' => ['permission:list']]);
    Route::get('/appointment/category/add', ['as' => '/appointment/category/add', 'uses' => 'Admin\CategoryAppointmentController@add', 'middleware' => ['permission:create']]);
    Route::any('/appointment/category/edit/{id}', ['as' => '/appointment/category/edit/{id}', 'uses' => 'Admin\CategoryAppointmentController@edit', 'middleware' => ['permission:edit']]);
    Route::get('/appointment/category/delete/{id}', ['as' => '/appointment/category/delete/{id}', 'uses' => 'Admin\CategoryAppointmentController@delete', 'middleware' => ['permission:delete']]);
    Route::any('/appointment/category/array-data', 'Admin\CategoryAppointmentController@arrayData');
    Route::post('/appointment/category/store', 'Admin\CategoryAppointmentController@store');
    Route::any('/appointment/category/updatemultirecode', 'Admin\CategoryAppointmentController@updatemultirecode');
    ////Category appoinment //////
    ////Topic//////
    Route::get('/topic', 'Admin\TopicController@index');
    Route::get('/topic/add', 'Admin\TopicController@add');
    Route::post('/topic/store', 'Admin\TopicController@store');
    Route::post('/topic/multiStore', 'Admin\TopicController@multiStore');
    Route::any('/topic/getlangkeyword', 'Admin\TopicController@getlangkeyword');
    Route::get('/topic/delete/{id}', 'Admin\TopicController@delete');
    Route::any('/topic/edit/{id}', 'Admin\TopicController@edit');
    // Datatables
    Route::get('/topic/array-data', 'Admin\TopicController@arrayData');
    ////Topic ///////
    // Appointment
    Route::get('/appointment', 'Admin\AppointmentController@index');
    Route::any('/appointment/add', 'Admin\AppointmentController@add');
    Route::any('/appointment/array-data', 'Admin\AppointmentController@arrayData');
    Route::any('/appointment/updatemultirecode', 'Admin\AppointmentController@updatemultirecode');
    // Route::any('/appointment/edit/{id}', 'Admin\AppointmentController@edit');

    Route::get('/appointment/edit/{id}', ['as' => '/appointment/edit/{id}', 'uses' => 'Admin\AppointmentController@edit', 'middleware' => ['permission:edit']]);

    Route::post('/appointment/store', 'Admin\AppointmentController@store');
    Route::any('/appointment/delete/{id}', 'Admin\AppointmentController@delete');
    Route::any('/appointment/cancel/{id}', 'Admin\AppointmentController@cancelAppointment');

    Route::get('/appointment/getAppoinment', 'Admin\AppointmentController@getAppoinment');
    Route::get('/appointment/getCategoryByLanguage', 'Admin\AppointmentController@getCategoryByLanguage');
    Route::get('/appointment/getTopicByLanguage', 'Admin\AppointmentController@getTopicByLanguage');
    Route::any('/appointment/assignAppoinment', 'Admin\AppointmentController@assignAppoinment');
    Route::any('/appointment/webrtc/{id}', 'Admin\AppointmentController@webrtc');
    Route::any('/appointment/logout', 'Admin\AppointmentController@webrtcLogout');
    Route::any('/appointment/updateAppoinmentStatus', 'Admin\AppointmentController@updateAppoinmentStatus');
    Route::any('/appointment/customeNotification', 'Admin\AppointmentController@customeNotificationIOS');


    /////////////////////FAQ////////////////////////
    Route::get('/faq', ['as' => '/faq', 'uses' => 'Admin\FAQController@index', 'middleware' => ['permission:list']]);
    Route::get('/faq/create', ['as' => '/faq/create', 'uses' => 'Admin\FAQController@create', 'middleware' => ['permission:create']]);
    Route::any('/faq/edit/{id}', ['as' => '/faq/edit/{id}', 'uses' => 'Admin\FAQController@edit', 'middleware' => ['permission:edit']]);
    Route::get('/faq/destroy/{id}', ['as' => '/faq/destroy/{id}', 'uses' => 'Admin\FAQController@destroy', 'middleware' => ['permission:delete']]);
    Route::any('/faq/array-data', 'Admin\FAQController@arrayData');
    Route::any('/faq/store', 'Admin\FAQController@store');
    ////////////////////////FAQ//////////////////////////
});

// Route::get('/terms', 'Web\WebController@termsCondition');
// Route::get('/expireRequest', 'Web\WebController@expireRequest');
// Route::get('/account-activation/{id}', 'Api\BasicController@changePasswordConfirm');


