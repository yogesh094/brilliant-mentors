<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = Permission::getPermission();

        Permission::truncate();
        
        foreach ($permission as $module => $moduleArr) {
            foreach ($moduleArr as $key => $value) {
                $value['module'] = $module;
                Permission::create($value);
            }
        }
    }
}
