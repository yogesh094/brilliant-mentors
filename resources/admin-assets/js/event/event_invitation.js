$(document).ready(function(){
      
    // tinymce text editor  
	// tinymce.init({ selector:'.textEditor' });

	$(document).on("click",".invitation-options", function(e) {
		var btnId 		= $(this).attr('id');
		var inviteText 	= "";
		inviteText 		= "{{ $"+btnId+" }}";
		$("#invitation_text").insertAtCaret(inviteText);
	}); 

	//function for find current position and put button text
	$.fn.extend({
		insertAtCaret: function(myValue){
	  		return this.each(function(i) {
	    		if (document.selection) {
			      	//For browsers like Internet Explorer
			      	this.focus();
			      	sel = document.selection.createRange();
			      	sel.text = myValue;
			      	this.focus();
			    }
	    		else if (this.selectionStart || this.selectionStart == '0') {
			      //For browsers like Firefox and Webkit based
			      var startPos = this.selectionStart;
			      var endPos = this.selectionEnd;
			      var scrollTop = this.scrollTop;
			      this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
			      this.focus();
			      this.selectionStart = startPos + myValue.length;
			      this.selectionEnd = startPos + myValue.length;
			      this.scrollTop = scrollTop;
	    		} else {
	      			this.value += myValue;
	      			this.focus();
	    		}
	 	 	})
		}
	});

	// check validation on form submit
	$("input[name=submit_invitation]").click(function(){
		var isError         = 0;
		//event subject
        if($.trim($('#email_subject').val()) == "") {
            $('body #err_email_subject').html("Email subject is required");
            isError = 1;
        } else {
            $('body #err_email_subject').html("");
        }

        //event content
        if($.trim($('#email_content').val()) == "") {
            $('body #err_email_content').html("Email content is required");
            isError = 1;
        } else {
            $('body #err_email_content').html("");
        }

        //thank you statement
        if($.trim($('#thank_you').val()) == "") {
            $('body #err_thank_you').html("Thank you statement is required");
            isError = 1;
        } else {
            $('body #err_thank_you').html("");
        }

        //signature
        if($.trim($('#signature').val()) == "") {
            $('body #err_signature').html("Signature is required");
            isError = 1;
        } else {
            $('body #err_signature').html("");
        }

        if(isError == 1) {
            return false;
        }
	});

	//event subject 
    $(document).on('keyup','#email_subject',function(){    
        if($.trim($(this).val()) == "") {
            $('body #err_email_subject').html("Email subject is required");
        } else {
            $('body #err_email_subject').html("");
        }
    });

    //event content 
    $(document).on('keyup','#email_content',function(){    
        if($.trim($(this).val()) == "") {
            $('body #err_email_content').html("Email content is required");
        } else {
            $('body #err_email_content').html("");
        }
    });

    //thank you 
    $(document).on('keyup','#thank_you',function(){    
        if($.trim($(this).val()) == "") {
            $('body #err_thank_you').html("Thank you statement is required");
        } else {
            $('body #err_thank_you').html("");
        }
    });

    //thank you 
    $(document).on('keyup','#signature',function(){    
        if($.trim($(this).val()) == "") {
            $('body #err_signature').html("Signature is required");
        } else {
            $('body #err_signature').html("");
        }
    });
	       
});