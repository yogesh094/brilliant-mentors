$(document).ready(function(){
    //radio button design
    // $('input.flat').iCheck({
    //     checkboxClass: 'icheckbox_square-blue',
    //     radioClass: 'iradio_square-blue'
    // });

    // $('.user-list-block:first-child').css("background-color", "yellow");
    // $('.user-list-block:first-child').trigger('click');
    $(".user-list-block:first-child").click();
    
    var userReceiverId  = $("#userReceiverId").val();
    var eventId         = $("#event_id").val();

    if(userReceiverId != "" && userReceiverId != 0 && eventId > 0) {
        loadChat(eventId, userReceiverId);
    }

    $(document).on('click','.user-list-block',function(){
        var receiverId  = 0; 
        receiverId      = $(this).attr('user'); 
        $("#msg_receiver_id").val(receiverId);

        $(".namecolor").each(function(){
            $(this).css('color','inherit');
        });

        $(".name_"+receiverId).css('color','green');

        var eventId         = $("#event_id").val();
        

        if($.trim(receiverId) != "") {
            loadChat(eventId, receiverId);
        }
    });

    //press enter button and send message
    $(document).on('click','.push-message',function(){
        
        var message     = $("#message-text").val();
        var eventId     = $("#event_id").val();
        
        var receiverId  = $("#msg_receiver_id").val();
        var senderId    = $("#msg_sender_id").val();

        if($.trim(message) != "") {
            pushChat(eventId ,message,receiverId , senderId );
        }
    });

    $(document).keypress(function(e) {
        if(e.which == 13) {
            var message     = $("#message-text").val();
            var eventId     = $("#event_id").val();
            
            var receiverId  = $("#msg_receiver_id").val();
            var senderId    = $("#msg_sender_id").val();

            if($.trim(message) != "") {
                pushChat(eventId ,message,receiverId , senderId );
            }
        }
    });

});



function pushChat(eventId = 0,message = "",receiverId = 0, senderId = 0) {
    $.ajax({
        type: "post",
        url: base_url+"/event/message/store",
        data: { 'event_id' : eventId, 'message' : message, 'receiver_id' : receiverId , 'sender_id' : senderId },
        success:function(data) {
            $("#user-chats").append(data);
            
            var elem = document.getElementById('chat-scroller');
            elem.scrollTop = elem.scrollHeight;
            // var div = $(".chat-scroller");
            // div.scrollTop(div.prop('scrollHeight'));
            $("#message-text").val("");
            // $("#messageBtnLoader").addClass("hide");
        },
        beforeSend: function() {
            $("body #messageBtnLoader").removeClass('hide');
            $("body #messageSendBtn").addClass("hide");
        },
        complete: function(){
            $("body #messageBtnLoader").addClass('hide');
            $("body #messageSendBtn").removeClass("hide");
        },
        error: function() {
            $("body #messageBtnLoader").addClass('hide');
            $("body #messageSendBtn").removeClass("hide");
            // $(this).attr("disabled", false);
        }
    });
}

function loadChat(eventId =0, receiverId = 0) {
    
    $.ajax({
        type: "get",
        url: base_url+"/event/get-message",
        data: { 'event_id' : eventId, 'receiver_id' : receiverId },
        success:function(data) {

            if(data != "") {
                $(".start-message").addClass("hide");
            } else {
                $(".start-message").removeClass("hide");
            }
            $("#user-chats").html(data);
            $("#message-text").removeAttr('disabled');
            $("#message-text").val("");
            var elem = document.getElementById('chat-scroller');
            elem.scrollTop = elem.scrollHeight;
            $("body .load-message-loader").addClass('hide');
            // var div = $(".chat-scroller");
            // div.scrollTop(div.prop('scrollHeight'));
        },
        beforeSend: function() {
            $("body #messageLoader_"+receiverId).removeClass('hide');
            $("body .load-message-loader").removeClass('hide');
        },
        complete: function() {
            $("body #messageLoader_"+receiverId).addClass('hide');
            $("body .load-message-loader").addClass('hide');
        },
        error: function() {
            $("body #messageLoader_"+receiverId).addClass('hide');
            $("body .load-message-loader").addClass('hide');
            // $(this).attr("disabled", false);
        }
    });
}
