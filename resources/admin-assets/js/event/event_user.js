$(document).ready(function() {

	var eventId 		= $("#event_id").val(); 
	$(document).on("click","body #user_search_btn",function(e){

		var seachText 	= $("#user_search").val();
		var data 		= [];
		if($.trim(seachText) != "") {
			data['seachText'] 	= seachText;
			data['eventId '] 	= eventId;
			loadSearchResult(data);
		} else {
            $("body #userResultSection").addClass("hide");
        }
	});

	$(document).on("keyup","body #user_search",function(e){
		var seachText 	= $(this).val();
		var data 		= [];
		if($.trim(seachText) != "") {
			data['seachText'] 	= seachText;
			data['eventId '] 	= eventId;
			loadSearchResult(data);
		} else {
            $("body #userResultSection").addClass("hide");
        }
	});

	//if user select check box then submit button appear
	$(document).on('ifChanged','.eventUserCheckBox',function(){

    	var countAllcheck 	= countcheck = 0;
    	var countAllcheck 	= $(".eventUserCheckBox").length;
    	if($(this).iCheck('update')[0].checked) {
    		var countcheck  = $(".eventUserCheckBox").filter(':checked').length;
    	} else {
    		var countcheck  = $(".eventUserCheckBox").filter(':checked').length;
    	}

    	if(countcheck == countAllcheck) {
        	$('.eventUserCheckBoxAll').iCheck('check');
        } else {
        	$('.eventUserCheckBoxAll').iCheck('uncheck');
        }

        if(countcheck > 0) {
        	$( ".event_user_submit" ).removeClass('hide');
        } else {
        	$( ".event_user_submit" ).addClass('hide');
        }
    });


    //select all result
    $(document).on('ifChanged', '.eventUserCheckBoxAll',function (event) { 
        
        var countAllcheck   = countcheck = 0;
        var countAllcheck   = $(".eventUserCheckBox").length;
        if($(this).iCheck('update')[0].checked) {
            var countcheck  = $(".eventUserCheckBox").filter(':checked').length;
        } else {
            var countcheck  = $(".eventUserCheckBox").filter(':checked').length;
        }
        
        if($(this).iCheck('update')[0].checked) {
            $('.eventUserCheckBox').iCheck('check');
        } else {
            if(countcheck == countAllcheck) {
                $('.eventUserCheckBox').iCheck('uncheck');
            }
        }
    });

	
});

function loadSearchResult(data = []) {
	var eventId 	= data['eventId'];
	var seachText 	= data['seachText'];

	$.ajax({
        type: "GET",
        url : base_url+"/event/"+eventId+"/event-user/search/"+seachText,
        data: {},
        success:function(data) {
        	$("body #userResultSection").removeClass("hide");
        	$("body #userListSection").html(data);
        	$('input.flat').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue'
            });
        	// $("#add_new_product").attr("disabled", false);
        },
        beforeSend: function(){
            $("#userSearchLoader").removeClass('hide');
        },
        complete: function(){
            $("#userSearchLoader").addClass('hide');
        },
        error: function() {
            $("#userSearchLoader").addClass('hide');
        }
    });
}