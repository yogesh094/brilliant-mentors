$(document).ready(function(){
    // contact address main contry dependency
    $("select.region").change(function(){
        var selRegion = $(".region option:selected").val();
        // console.log(selRegion);

        $.ajax({
            type: "GET",
            url: "{{ url('country') }}",
            data: { region : selRegion, '_token' : "{{ csrf_token() }}" } 
        }).done(function(data){
            $("#countryAutoLoad").html(data);
        });
    });

    //contact address main city auto load
    $(document).on("change", 'select.country', function(event) { 

        var selCountry = $(".country option:selected").val();
        // contact main city dropdown
        $.ajax({
            type: "GET",
            url: "{{ url('city') }}",
            data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
        }).done(function(data){
            $("#cityAutoLoad").html(data);
        });

        //contact main provience dropdown
        $.ajax({
            type: "GET",
            url: "{{ url('province') }}",
            data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
        }).done(function(data){
            $("#provinceAutoLoad").html(data);
        });

        //contact main district dropdown
        $.ajax({
            type: "GET",
            url: "{{ url('district') }}",
            data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
        }).done(function(data){
            $("#districtAutoLoad").html(data);
        });
    });
});
