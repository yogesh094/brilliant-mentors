$(document).ready(function(){
    // education accordian
    $("#accordionEducation").on("hidden.bs.collapse", function (e) {
        
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionEducation").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });

    // load dates from focus
    $('body').on('focus',".educationDate", function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,        
            singleClasses: "picker_2"
        }, function (start, end, label) {
            // console.log(start.toISOString(), end.toISOString(), label);
        });
    });

    //from date validation
    $(document).on('change',".educationDate", function(){
        var startDate   = $(this).val();
        var keyId       = $(this).attr('keyid');
        if(startDate != ""){
            $('#educationFromDateError_'+keyId).html("");
        }
    });

    
    //Add education fields - start
    $(document).on("click", '.add_education', function(event) {
        var keyId = $('.education_multi_fields').last().attr('data-id');
        var dataId = $('.education_multi_fields').last().attr('data');
        var educationFormIndex   = $('.education_multi_fields').last().attr('form-index');
        $("#educationLoader").removeClass('hide');
        $(this).attr("disabled", true);
        $.ajax({
            type: "GET",
            url: base_url+"/multiple/education",
            data: { 'keyId' : keyId,'dataId' : dataId, 'educationFormIndex':educationFormIndex,'_token' : "{{ csrf_token() }}" },
            success:function(data) {
                $("#education_multiple").append(data);
                $('.checkTrashEducation').removeClass('hide');
                $("#educationLoader").addClass('hide');
                $(this).attr("disabled", false);
            },
            beforeSend: function() {
                $("#educationLoader").removeClass('hide');
            },
            error: function() {
                $("#educationLoader").addClass('hide');
                $(this).attr("disabled", false);
            }

        });
        $(this).attr("disabled", false);
        // blank value disabled in dropdown
        $("select option[value='']").attr("disabled", true);
    });
    //Add education fields - end

    //remove contact education start
    $(document).on("click", '.trashEducation', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        if(answer==true) {
            $(".education_multi_fields").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/education/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    } else {
                        // console.log(response.status);
                        // console.log("fail");
                    }         
                    
                    //when remove all content is deleted from block
                    if( !$.trim( $('#education_multiple').html() ).length ) {    
                        $('.checkTrashEducation').addClass('hide');
                        $("#education .tagsinput .tag").remove();
                        $('#education .fr-element.fr-view').html("");
                        $('#education .userField').removeAttr('placeholder');
                    
                    } else {
                        $('.checkTrashEducation').removeClass('hide');
                    
                    }             
                },
                beforeSend: function() {
                    
                },
                error: function() {
                    
                }

            });

        } else {
            
            return false;
        }
    });

    // load validation on submit start
    $("input[name=submit_education]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var isError = 0;
        //start date validation 
        $(".educationDateData").each(function() {
            var keyId       = $(this).attr('keyId');
            var start_date  = $('#educationFromDate_'+keyId).val();
            var end_date    = $('#educationEndDate_'+keyId).val();
            
            if(start_date == "") {
                $('body #educationFromDateError_'+keyId).html('From date is required');
                isError     = 1;
            } else {
                $('body #educationFromDateError_'+keyId).html('');
            }

            if( start_date != "" && end_date != "") {
                var res     = CompareDate(start_date,end_date);
                if(!res) {
                    $('body #educationEndDateError_'+keyId).html('End date must be a greater than start date');
                    isError = 1;
                } else {
                    $('body #educationEndDateError_'+keyId).html('');
                }
            }
        });
        
        if(isError == 1) {
            $(".contact-education-error-block").show();
            return false;
        } else {
            $(".contact-education-error-block").hide();
        }
    });
    // load validation on submit end

    

    // $('input[name=education_certificate]').on('change',function(){
    //     if (this.files && this.files[0]) {
    //         var file = this.files[0];
    //         var fileType = file["type"];
    //         var ValidImageTypes = ["image/jpeg", "image/jpg", "image/png"];
    //         if ($.inArray(fileType, ValidImageTypes) < 0) {
    //             $('.educationCertificateError').html('select only jpeg,jpg,png image');
    //             return false;
    //         } else {
    //             $('.educationCertificateError').html('');
    //             displayClass = '.education_image';
    //             DisplayOnChangeFile(this.files,displayClass);
    //         }
    //     }
    // });
});