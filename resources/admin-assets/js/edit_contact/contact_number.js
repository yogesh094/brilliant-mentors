$(document).ready(function() {

	// accorion on multiple contact number 
	$("#accordionContact").on("hidden.bs.collapse", function (e) {
        
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionContact").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });

    //contact address
    $("input[name=submit-contact-number]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);
        
        // return false;
        var isError = 0;

        // Greeting intro 
        $(".contactNumberData").each(function(){
            var keyId           = $(this).attr('keyId');
            var contactNumber   = $('#contactNumberid_'+keyId).val();
            
            if(contactNumber == "") {
                $('body #contactNumberErr_'+keyId).html('Contact Number is required');
                isError     = 1;
            } else {
                $('body #contactNumberErr_'+keyId).html('');
            }
        });

        $(".countryCodeData").each(function(){
            var keyId     = $(this).attr('keyId');
            
            if($('#contact_country_code_'+keyId+' option:selected').val() == "") {
                $('body #err_contact_country_code_'+keyId).html('Country Code is required');
                isError     = 1;
            } else {
                $('body #err_contact_country_code_'+keyId).html('');
            }
        });

        if(isError == 1) {
            $(".contact-number-error-block").show();
            return false;
        } else {
            $(".contact-number-error-block").hide();
        }

    });

    // contact number 
    $(document).on('keypress','.contactNumberData',function(e){    
        var keyId       = $(this).attr('keyId');
        var contactNum  = $('#contactNumberid_'+keyId).val();

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
            $("#contactNumberErr_"+keyId).html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    // contact number 
    $(document).on('keypress','.contactNumberFaxData',function(e){    
        var keyId       = $(this).attr('keyId');
        var contactNum  = $('#contact_number_fax_'+keyId).val();

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
            $("#err_contactNumberFaxData_"+keyId).html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    //country code
    $(document).on('change','.countryCodeData',function(){    
        var keyId     = $(this).attr('keyId');
        
        if($('#contact_country_code_'+keyId+' option:selected').val() == "") {
            $('body #err_contact_country_code_'+keyId).html('Country Code is required');
        } else {
            $('body #err_contact_country_code_'+keyId).html('');
        }
    });

    // add more on contact number start
	$(document).on('click','.addBlockContactNumber',function(event) {

		var address_id = $('.contactNumberAjaxBlock').last().attr('data-id');
        var contactNumberFormIndex = $('.contactNumberAjaxBlock').last().attr('form-index');
        
        $("#contactNumberLoader").removeClass('hide');
        $(this).attr("disabled", true);
        $.ajax({
            type: "GET",
            url: base_url+"/contact/multiple-contact-number",
            data: { 'address_id' : address_id, 'contactNumberFormIndex':contactNumberFormIndex,'_token' : "{{ csrf_token() }}" },
            success:function(data) {
                $("#contactNumberMultipleBlock").append(data);
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $('.checkTrashContactNumber').removeClass('hide');
                $("#contactNumberLoader").addClass('hide');
                
                $(this).attr("disabled", false);
            },
            beforeSend: function(){
		     	$("#contactNumberLoader").removeClass('hide');
		   	},
		   	complete: function(){
		     	$("#contactNumberLoader").addClass('hide');
		   	},
            error: function() {
                $("#contactNumberLoader").addClass('hide');
                $(this).attr("disabled", false);
            }
        });
        $(this).attr("disabled", false);
	})
	// add more on contact number end

	//remove contact address start
    $(document).on("click", '.trashContactNumber', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        if(answer==true) {
            $(".contactNumberAjaxBlock").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/contact-number/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    }          

                    //when remove all content is deleted from block
                    if( !$.trim( $('#contactNumberMultipleBlock').html() ).length ) {    
                        $('.checkTrashContactNumber').addClass('hide');
                        $("#contactNumber .tagsinput .tag").remove();
                        $('#contactNumber .fr-element.fr-view').html("");
                        $('#contactNumber .userField').removeAttr('placeholder');
                    } else {
                        $('.checkTrashContactNumber').removeClass('hide');
                    }            
                },
                beforeSend: function() {
                    
                },
                error: function() {
                    
                }

            });

        } else {
            $(this).hide();
            return false;
        }
    });
    //remove contact address end
	
});
