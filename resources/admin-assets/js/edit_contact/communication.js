$(document).ready(function(){

    $("#accordionCommunication").on("hidden.bs.collapse", function (e) {
        
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionCommunication").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });

//contact COMMUNICATION js validation - start
    $("input[name=submit_communication]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var isError     = 0;
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  

        $(".communicationEmail").each(function(index) {
            var emailIndex  = index+1;
            var emailError  = ".communicationEmailError_"+emailIndex;
            var emailVal    = $("#communicationEmail_"+emailIndex).val();

            if(emailVal != ""){
                if(!emailVal.match(mailformat)) { 
                    $(emailError).html('Please enter valid email');
                    isError = 1;
                } else {
                    $(emailError).html('');
                }
            } else {
                $(emailError).html('Email is required');
                isError = 1;
            }
        });

        $(".communicationEmailCC").each(function(index) {
            var emailCCIndex   = index+1;
            var emailCCError   = ".communicationEmailBCCError_"+emailCCIndex;
            var emailCCVal     = $("#communicationEmailBCC_"+emailCCIndex).val();
            if(emailCCVal != ""){
                if(!emailCCVal.match(mailformat)) { 
                    $(emailCCError).html('Please enter valid email');
                    isError = 1;
                } else {
                    $(emailCCError).html('');
                }
            }
        });

        $(".communicationEmailBCC").each(function(index) {
            var emailBCCIndex   = index+1;
            var emailBCCError   = ".communicationEmailBCCError_"+emailBCCIndex;
            var emailBCCVal     = $("#communicationEmailBCC_"+emailBCCIndex).val();
            if(emailBCCVal != ""){
                if(!emailBCCVal.match(mailformat)) { 
                    $(emailBCCError).html('please enter valid email');
                    isError = 1;
                } else {
                    $(emailBCCError).html('');
                }
            }
        });

        if(isError == 1) {
            $(".contact-communication-error-block").show();
            return false;
        } else {
            $(".contact-communication-error-block").hide();
        }
    });

    //Add communication fields - start
    $(document).on("click", '.add_communication', function(event) {
        var keyId = $('.communication_multi_fields').last().attr('data-id');
        var dataId = $('.communication_multi_fields').last().attr('data');
         var communicationFormIndex = $('.communication_multi_fields').last().attr('form-index');
        $("#communicationLoader").removeClass('hide');
        $(this).attr("disabled", true);
        $.ajax({
            type: "GET",
            url: base_url+"/multiple/communication",
            data: { 
                'keyId' : keyId,'dataId' : dataId,
                '_token' : "{{ csrf_token() }}" ,
                 'communicationFormIndex':  communicationFormIndex
            },
            success:function(data) {
                $("#communication_multiple").append(data);
                $('.checkTrashCommunication').removeClass('hide');
                $("#communicationLoader").addClass('hide');
                $(this).attr("disabled", false);
            },
            beforeSend: function() {
                $("#communicationLoader").removeClass('hide');
            },
            error: function() {
                $("#communicationLoader").addClass('hide');
                $(this).attr("disabled", false);
            }

        });
        $(this).attr("disabled", false);
        // blank value disabled in dropdown
        $("select option[value='']").attr("disabled", true);
    });
    //Add communication fields - end

    //remove contact address start
    $(document).on("click", '.trashCommunication', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        if(answer==true) {
            $(".communication_multi_fields").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/communication/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    } else {
                        // console.log(response.status);
                        // console.log("fail");
                    }  

                    //when remove all content is deleted from block
                    if( !$.trim( $('#communication_multiple').html() ).length ) {    
                        $('.checkTrashCommunication').addClass('hide');
                        
                        $("#communication .tagsinput .tag").remove();
                        $('#communication .fr-element.fr-view').html("");
                        $('#communication .userField').removeAttr('placeholder');
                    } else {
                        $('.checkTrashCommunication').removeClass('hide');
                    }                    
                },
                beforeSend: function() {
                    
                },
                error: function() {
                    
                }

            });

        } else {
            
            return false;
        }
    });

//contact COMMUNICATION js validation - end
});

function getCommunicationEmail(index){
    var communicationEmail = $("#communicationEmail_"+index).val();
    var emailError  = ".communicationEmailError_"+index;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(communicationEmail != ""){
        if(!communicationEmail.match(mailformat)) {
            $(emailError).html('Please enter valid email');
        } else {
            $(emailError).html('');
        }
    }
}

function getCommunicationEmailCC(index){
    var communicationEmailCC = $("#communicationEmailCC_"+index).val();
    var emailCCError  = ".communicationEmailCCError_"+index;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(communicationEmailCC != ""){
        if(!communicationEmailCC.match(mailformat)) {
            $(emailCCError).html('Please enter valid email');
        } else {
            $(emailCCError).html('');
        }
    }
}

function getCommunicationEmailBCC(index){
    var communicationEmailBCC = $("#communicationEmailBCC_"+index).val();
    var emailBCCError  = ".communicationEmailBCCError_"+index;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(communicationEmailBCC != ""){
        if(!communicationEmailBCC.match(mailformat)) {
            $(emailBCCError).html('Please enter valid email');
        } else {
            $(emailBCCError).html('');
        }
    }
}