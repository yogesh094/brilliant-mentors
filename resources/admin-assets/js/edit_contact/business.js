$(document).ready(function(){
    // business accordion start
    $("#accordionContactBusiness").on("hidden.bs.collapse", function (e) {

        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionContactBusiness").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });
    // business accordion end

    //load start date
    $('body').on('focus',".business_startdate", function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            singleClasses: "picker_2"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });

    //load start date
    $(document).on('change',".business_startdate", function(){
        var startDate   = $(this).val();
        var keyId       = $(this).attr('keyid');
        if(startDate != ""){
            $('#businessStartDateError_'+keyId).html("");
        }
    });

    // validate on form submit start
    $("input[name=submit_business]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var isError         = 0;

        $(".businessStartDateData").each(function() {
            var keyId       = $(this).attr('keyId');
            var start_date  = $('#start_date_'+keyId).val();

            if(start_date == "") {
                $('body #businessStartDateError_'+keyId).html('Start date is required');
                isError     = 1;
            } else {
                $('body #businessStartDateError_'+keyId).html('');
            }
        });

        if(isError == 1) {
            $(".contact-business-error-block").show();
            return false;
        } else {
            $(".contact-business-error-block").hide();
        }
    });
    // validate on form submit end

    // business website validation
    $(".business_website").keyup(function(){
        CheckWebsiteVlidation($(this).val().trim(),'.businessWebsite');
    });


    // add more on political position start
    $(document).on('click','.addBlockBusiness',function(event) {

        var businessblock_id = $('.contactBusinessAjaxBlock').last().attr('data-id');
        var businessFormIndex = $('.contactBusinessAjaxBlock').last().attr('form-index');
        $(".businessLoader").removeClass('hide');
        // $(this).attr("disabled", true);
        $.ajax({
            type: "GET",
            url: base_url+"/contact/multiple-business",
            data: { 'businessblock_id' :  businessblock_id,
            'businessFormIndex' : businessFormIndex,
             '_token' : "{{ csrf_token() }}" },
            success:function(data) {
                $("#businessAddnewBlock").append(data);
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $('.checkTrashBusiness').removeClass('hide');
                $("#businessLoader").addClass('hide');
                $(this).attr("disabled", false);
            },
            beforeSend: function(){
                $("#businessLoader").removeClass('hide');
                $(this).attr("disabled", true);
            },
            complete: function(){
                $("#businessLoader").addClass('hide');
            },
            error: function() {
                $("#businessLoader").addClass('hide');
            }
        });
        $(this).attr("disabled", false);
    })
    // add more on political position end

    //remove contact address start
    $(document).on("click", '.trashBusiness', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        if(answer==true) {
            $(".contactBusinessAjaxBlock").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/business-block/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        console.log(response.status);
                    }
                    //when remove all content is deleted from block
                    if( !$.trim( $('#businessAddnewBlock').html() ).length ) {
                        $('.checkTrashBusiness').addClass('hide');

                        $("#business .tagsinput .tag").remove();
                        $('#business .fr-element.fr-view').html("");
                        $('#business .userField').removeAttr('placeholder');

                    } else {
                        $('.checkTrashBusiness').removeClass('hide');

                    }
                },
                beforeSend: function() {

                },
                error: function() {

                }

            });

        } else {
            $(this).hide();
            return false;
        }
    });
    //remove contact address end
});