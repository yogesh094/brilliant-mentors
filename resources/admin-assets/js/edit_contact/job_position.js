$(document).ready(function(){
    
    $("#accordionJobPosition").on("hidden.bs.collapse", function (e) {
        
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionJobPosition").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });

//contact JOB POSITION js validation - start
    $('body').on('focus',".jobposition_date", function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,        
            singleClasses: "picker_2"
        }, function (start, end, label) {
            // console.log(start.toISOString(), end.toISOString(), label);
        });
    });

    $("input[name=submit_job_position]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var JobPositionJobType  = $("#JobPositionJobType option:selected" ).val();
        var start_date          = $("input[name='start_date']").val();
        var end_date            = $("input[name='exit_date']").val();

        var isError     = 0;
        //job type validation start
        $(".jobposData").each(function(){
            // var keyId       = $(this).attr('keyId');
            var keyId     = $(this).attr('keyId');
            console.log(keyId);
            if($('#JobPositionJobType_'+keyId+' option:selected').val() == "") {
                $('body .err_JobPositionJobType_'+keyId).html('Select the job type');
                isError     = 1;
            } else {
                $('body .err_JobPositionJobType_'+keyId).html('');
            }
        });
        //job type validation end

        //job start and end date check start
        $(".jobposDateData").each(function(){

            var keyId       = $(this).attr('keyId');
            var start_date  = $("#start_date_"+keyId).val();
            var end_date    = $("#end_date_"+keyId).val();

            if( start_date != "" && end_date != "") {
                var res     = CompareDate(start_date,end_date);
                if(!res) {
                    $('body .err_JobPositionEndDate_'+keyId).html('Exit date must be a greater than start date');
                    isError = 1;
                } else {
                    $('body .err_JobPositionEndDate_'+keyId).html('');
                }
            }
        });
        //job start and end date check end

        if(isError == 1) {
            $(".contact-jobposition-error-block").show();
            return false;
        } else {
            $(".contact-jobposition-error-block").hide();
        }
        
    });
    
    //onchage job position type validation remove
    $(document).on('change','.jobposData',function(){
        var keyId     = $(this).attr('keyId');
        if($('#JobPositionJobType_'+keyId+' option:selected').val() == "") {
            $('body .err_JobPositionJobType_'+keyId).html('Select the job type');
            isError     = 1;
        } else {
            $('body .err_JobPositionJobType_'+keyId).html('');
        }
    });

    $(".jobposition_website").keyup(function(){
        CheckWebsiteVlidation($(this).val().trim(),'.jobpositionWebsite');
    });

    //Add job position fields - start
    $(document).on("click", '.add_jobPosition', function(event) {
        var keyId = $('.jobPosition_multi_fields').last().attr('data-id');
        var dataId = $('.jobPosition_multi_fields').last().attr('data');
        var jobpositionFormIndex   = $('.jobPosition_multi_fields').last().attr('form-index');
        $("#jobPositionLoader").removeClass('hide');
        $.ajax({
            type: "GET",
            url: base_url+"/multiple/job-position",
            data: { 'keyId' : keyId,'dataId' : dataId, 'jobpositionFormIndex':jobpositionFormIndex,'_token' : "{{ csrf_token() }}" },
            success:function(data) {
                $("#jobPosition_multiple").append(data);
                $('.checkTrashJobposition').removeClass('hide');
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $("#jobPositionLoader").addClass('hide');
                $(this).attr("disabled", false);
            },
            beforeSend: function() {
                $("#jobPositionLoader").removeClass('hide');
            },
            error: function() {
                $("#jobPositionLoader").addClass('hide');
                $(this).attr("disabled", false);
            }

        });
        $(this).attr("disabled", false);
        // blank value disabled in dropdown
        $("select option[value='']").attr("disabled", true);
    });
    //Add job position fields - end

    //remove job position start
    $(document).on("click", '.trashJobPosition', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        if(answer==true) {
            $(".jobPosition_multi_fields").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/job-position/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    } else {
                        // console.log(response.status);
                        // console.log("fail");
                    }      

                    //when remove all content is deleted from block
                    if( !$.trim( $('#jobPosition_multiple').html() ).length ) {    
                        $('.checkTrashJobposition').addClass('hide');
                        $("#jobposition .tagsinput .tag").remove();
                        $('#jobposition .fr-element.fr-view').html("");
                        $('#jobposition .userField').removeAttr('placeholder');
                        
                    } else {
                        $('.checkTrashJobposition').removeClass('hide');
                        
                    }                
                },
                beforeSend: function() {
                    
                },
                error: function() {
                    
                }

            });

        } else {
            
            return false;
        }
    });
    //remove job position end

});

function getJobPositions(index,keyId){
    var selCategory = $(".job_category_"+index+" option:selected").val();
    //Job Positions drop down
    $.ajax({
        type: "GET",
        url: base_url+'/job-category',
        data: { jobcategory : selCategory, 'keyId' : keyId,'_token' : "{{ csrf_token() }}" }
    }).done(function(data){
        $("#jobPositionAutoLoad_"+index).html(data);
    });
}