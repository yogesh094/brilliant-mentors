$(document).ready(function(){
    
    $(document).on('click',".add_guest_by_filter",function(){
        
        $("body .add_guest_div").hide();
        
        $('input[name=contact_search]').val('');
        $('#guest_attendee_result').html('');
        $('.guest_attendee_result_div').hide();

        $('input[name=contact_search_keyword]').val('');
        $('.search_keyword_result').html('');
        $('.add_guest_search_keyword_div').hide();

        $("#add_guest_by_filter_block").show();
    });

    //get data property of selected option
    $(document).on('change',"#data_property_title",function(){

    	// console.log($(this).val());
    	var selDataPropertyId 	= $(this).val();  	
    	$('#data_property_option_section').show();
    	// $('#data_property_option_section').html(selDataPropertyId);

    	//hide result block
    	$("#result_guest_by_filter_block").hide();

    	if(selDataPropertyId != "") {

	    	$.ajax({
	            type: "GET",
	            url: base_url+"/options/list/"+selDataPropertyId,
	            // data: { 'selDataPropertyId' : selDataPropertyId ,'_token' : "{{ csrf_token() }}" },
	            success:function(data) {

	                $("#data_property_option_section").html(data);
	                
	                $('input.flat').iCheck({
	                    checkboxClass: 'icheckbox_square-blue',
	                    radioClass: 'iradio_square-blue'
	                });
	                $("#data_property_select_option_loader").addClass('hide');
	            },
	            beforeSend: function() {
	                $("#data_property_select_option_loader").removeClass('hide');
	            },
	            error: function() {
	                $("#data_property_select_option_loader").addClass('hide');
	                $(this).attr("disabled", false);
	            }

	        });
    	}
    });

    //select all data property
    // data_property_option_section
    $(document).on('ifChanged', '.dataPropertySelectAll',function (event) { 

        var countAllcheck   = countcheck = 0;
        var countAllcheck   = $(".dataPropertyCheck").length;
        if($(this).iCheck('update')[0].checked) {
            var countcheck  = $(".dataPropertyCheck").filter(':checked').length;
        } else {
            var countcheck  = $(".dataPropertyCheck").filter(':checked').length;
        }

    	if($(this).iCheck('update')[0].checked) {
    		$('.dataPropertyCheck').iCheck('check');
    	} else {
            if(countcheck == countAllcheck) {
    		    $('.dataPropertyCheck').iCheck('uncheck');
            }
    	}
    });

    //select all result
    $(document).on('ifChanged', '.resultDataSelectAll',function (event) { 
        
        var countAllcheck   = countcheck = 0;
        var countAllcheck   = $(".resultGuestFilter").length;
        if($(this).iCheck('update')[0].checked) {
            var countcheck  = $(".resultGuestFilter").filter(':checked').length;
        } else {
            var countcheck  = $(".resultGuestFilter").filter(':checked').length;
        }
        
        if($(this).iCheck('update')[0].checked) {
            $('.resultGuestFilter').iCheck('check');
        } else {
            if(countcheck == countAllcheck) {
                $('.resultGuestFilter').iCheck('uncheck');
            }
        }
    });

    $(document).on('ifChanged','.dataPropertyCheck',function(){

    	var countAllcheck 	= countcheck = 0;
    	var countAllcheck 	= $(".dataPropertyCheck").length;
    	if($(this).iCheck('update')[0].checked) {
    		var countcheck  = $(".dataPropertyCheck").filter(':checked').length;
    	} else {
    		var countcheck  = $(".dataPropertyCheck").filter(':checked').length;
    	}

    	if(countcheck == countAllcheck) {
        	$('.dataPropertySelectAll').iCheck('check');
        } else {
        	$('.dataPropertySelectAll').iCheck('uncheck');
        }

        if(countcheck > 0) {
        	$( ".submit_filter_search" ).removeClass('hide');
        } else {
        	$( ".submit_filter_search" ).addClass('hide');
            $("#result_guest_by_filter_block").html("");
            $("#result_guest_by_filter_block").hide();
        }
    });

    //check is all check box checked or not.
    $(document).on('ifChanged','.resultGuestFilter',function(){	

    	var countAllcheck 	= countcheck = 0;
    	var countAllcheck 	= $(".resultGuestFilter").length;
    	if($(this).iCheck('update')[0].checked) {
    		var countcheck  = $(".resultGuestFilter").filter(':checked').length;
    	} else {
    		var countcheck  = $(".resultGuestFilter").filter(':checked').length;
    	}

        if(countcheck == countAllcheck) {
            $('.resultDataSelectAll').iCheck('check');
        } else {
            $('.resultDataSelectAll').iCheck('uncheck');
        }

        if(countcheck > 0) {
        	$( ".submit_filter_result" ).removeClass('hide');
        } else {
        	$( ".submit_filter_result" ).addClass('hide');
        }
    });

    //filter result
    $(document).on('click','.submit_filter_search',function(){

    	var selectedIds 	= [];
    	var parentId 		= 0;
    	$('.dataPropertyCheck').each(function(){
    		if(this.checked) {
    			selectedIds.push($(this).val());
    			parentId 	= $(this).attr("datapropertyid");
    		}
    	});
    	var eventId 			= $("body #eventId").val();
    	var dataPropertyTitle 	= $("#data_property_title :checked").text();
        var form_token          = $('body #form_token').val();
    	var baseUrl 			= $('body #baseUrl').val();
        $(this).attr('disabled',true);

    	if(selectedIds.length > 0) {

    		$.ajax({
	            type: "POST",
	            url: base_url+"/result/guest-by-filter",
	            data: { 'selectedIds' : selectedIds,'parentId' : parentId, 'dataPropertyTitle' : dataPropertyTitle , '_token' : form_token,'eventId': eventId },
	            success:function(data) {

	                $("#result_guest_by_filter_block").show();
	                $("#result_guest_by_filter_block").html(data);
	                
                    $('body .submit_filter_search').attr('disabled',false);
                    $('input.flat').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue'
                    });
                    $("#result_guest_filter_loader").addClass('hide');
                    
	            },
	            beforeSend: function() {
                    $("#result_guest_filter_loader").removeClass('hide');
	            },
	            error: function() {
	                $("#result_guest_filter_loader").addClass('hide');
	                $('body .submit_filter_search').attr('disabled',false);
	            }

	        });
    	}
    });

    $(document).on('blur','#guest_list_title', function(){
        var eventId         = $("body #eventId").val();
        var guestListTitle  = $("#guest_list_title").val();
        var guestListId     = $("#guest_list_id").val();
        var guestSlug       = $("#guest_slug").val();
        var che     = checkGroupNameExist(guestListTitle,eventId,guestListId);
        console.log(che);
    });

    // get submit filter result and store event attendee
    $(document).on('click','.submit_filter_result',function(){

        var isError         = 0;
        var resultArr       = [];
        var i               = 0;
        var form_token      = $('#form_token').val();
        var eventId         = $("body #eventId").val();
        var baseUrl         = $('body #baseUrl').val();
        var guestListTitle  = $("#guest_list_title").val();
        var adminiGroup     = $("#administrative_group").val();
        var deliveryRep     = $("#delivery_representative").val();
        var guestListId     = $("#guest_list_id").val();
        var guestSlug       = $("#guest_slug").val();


        if($.trim($('#guest_list_title').val()) == "") {
            $('body #err_guest_list_title').html("Event guest title is required");
            isError = 1;
        } else {
            $('body #err_guest_list_title').html("");
        }

        //administrative group
        if($('#administrative_group option:selected').val() == "") {
            $('body #err_administrative_group').html('Select administrative group');
            isError     = 1;
        } else {
            $('body #err_administrative_group').html('');
        }

        //delivery_representative group
        if($('#delivery_representative option:selected').val() == "") {
            $('body #err_delivery_representative').html('Select delivery representative');
            isError     = 1;
        } else {
            $('body #err_delivery_representative').html('');
        }

        var checkNameExist  = checkGroupNameExist(guestListTitle,eventId,guestListId);
        if(checkNameExist) {
            isError = 1;
        }

        if(isError == 1) {
            $('html, body #user-filter-group').animate({
                scrollTop: "100px"
            }, 800);
             
            return false;
        }

    	$('.resultGuestFilter').each(function(){
    		if ($(this).prop('checked')==true){ 	
    			resultArr.push({
				    "contactId"	    : $(this).attr('contactid'),
				    "eventId"	    : $(this).attr('eventId'),
				    "tableType"	    : $(this).attr('tableType'),
				    "tablerowid"    : $(this).attr('tablerowid'),
				    "parentId" 	    : $(this).attr('datapropertyid'),
				    "eventId"	    : $("body #eventId").val(),
                    "fullname"	    : $(this).val(),
				});
    		}                         
        });

        $(this).attr('disabled',true);
        if(resultArr.length > 0) {
			$.ajax({
		        type: "POST",
		        url: base_url+"/result/store-guest-filter",
		        data: { 'resultArr' : resultArr, '_token' : form_token,'eventId': eventId, 'guestListTitle' : guestListTitle, 'adminiGroup' : adminiGroup, 'deliveryRep' : deliveryRep, 'guestListId' : guestListId, 'guestSlug' : guestSlug },
		        success:function(data) {
                    var obj = jQuery.parseJSON(data);

		        	if(obj.isSave) {
		        		// location.reload();
                        window.location.href = baseUrl+"/event/"+eventId+"/guest/event-guest-list";
		        	} 
		            $('body .submit_filter_result').attr('disabled',false);
		            $("#submit_guest_filter_loader").addClass('hide');
		        },
		        beforeSend: function() {
		            $("#submit_guest_filter_loader").removeClass('hide');

		        },
		        error: function() {
		            $("#submit_guest_filter_loader").addClass('hide');
		            
		        }
		    });
		}

        // console.log(resultArr);
        return false;
    });

    //function for check guest title is exist or not
    function checkGroupNameExist(guestTitleList = "",eventId = 0, guestListId = 0) {

        var result = false;
        $.ajax({
            type: "GET",
            url: base_url+"/event/"+eventId+"/guest/check-title-exist/"+guestTitleList+"/"+guestListId,
            // data: { 'resultArr' : resultArr, '_token' : form_token,'eventId': eventId },
            async: false,
            success:function(data) {
                var obj = jQuery.parseJSON(data);
                var startCheck;
                
                if(obj.isExist) {
                    $('body #err_guest_list_title').html("Event guest title is already exist");
                    result = true;
                } else {
                    $('body #err_guest_list_title').html("");
                    result = false;
                }
            },
            beforeSend: function() {
                // $("#submit_guest_filter_loader").removeClass('hide');
            },
            error: function() {
                // $("#submit_guest_filter_loader").addClass('hide');
            }

        });

        return result;
    }

});

