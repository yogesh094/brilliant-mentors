$(document).ready(function(){
    // accorion on multiple records
    $("#accordionContactReferral").on("hidden.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionContactReferral").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });

     // add more on contact spoise start
    $(document).on('click','.addBlockReferral',function(event) {

        var data_id = $('.contactReferralAjaxBlock').last().attr('data-id');
        var referralFormIndex   = $('.contactReferralAjaxBlock').last().attr('form-index');
        $("#addReferralLoader").removeClass('hide');
        $(this).attr("disabled", true);
        $.ajax({
            type: "GET",
            url: base_url+"/contact/multiple-contact-referral",
            data: { 'address_id' : data_id, 'referralFormIndex':referralFormIndex,'_token' : "{{ csrf_token() }}" },
            success:function(data) {
                $("#referralAddnewBlock").append(data);
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $(".checkTrashReferral").removeClass('hide');
                
                $("#addReferralLoader").addClass('hide');
                $("select option[value='']").attr("disabled", true);
                $(this).attr("disabled", false);
            },
            beforeSend: function(){
                $("#addReferralLoader").removeClass('hide');
            },
            complete: function(){
                $("#addReferralLoader").addClass('hide');
            },
            error: function() {
                $("#addReferralLoader").addClass('hide');
                $(this).attr("disabled", false);
            }
        });
        $(this).attr("disabled", false);
    })
    // add more on contact number end

    //remove contact address start
    $(document).on("click", '.trashreferral', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        
        if(answer==true) {
            $(".contactReferralAjaxBlock").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/contact-referral/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    }
                    
                    if( !$.trim( $('#referralAddnewBlock').html() ).length ) {    
                        $('.checkTrashReferral').addClass('hide');
                        $("#referral .tagsinput .tag").remove();
                        $('#referral .fr-element.fr-view').html("");
                        $('#referral .userField').removeAttr('placeholder');
                    
                    } else {
                        $('.checkTrashReferral').removeClass('hide');
                    
                    } 
                },
                beforeSend: function() {

                },
                error: function() {

                }

            });

        } else {
            return false;
        }
    });
    //remove contact address end
    
//contact REFERRAL js validation - start
    // $('#referral_of_birth').daterangepicker({
    //     singleDatePicker: true,
    //     showDropdowns: true,        
    //     singleClasses: "picker_2"
    //     // singleClasses: "picker_1"
    // }, function (start, end, label) {
    //     console.log(start.toISOString(), end.toISOString(), label);
    // });

    $('body').on('focus',".ref_dateofbirth", function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,        
            singleClasses: "picker_2"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });
    
    $("input[name=submit_referral]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var isError         = 0;
        // contact key validation start
        $(".refrContactKey").each(function(){
            var keyId       = $(this).attr('keyId');
            var contactKey  = $('#refrContactKey_'+keyId).val();
            
            if(contactKey == "") {
                $('body #err_contact_ref_unique_key_'+keyId).html('Contact key is required');
                isError     = 1;
            } else {
                $('body #err_contact_ref_unique_key_'+keyId).html('');
            }
        });

        //suffix data validation
        $(".refrSuffix").each(function(){
            var keyId     = $(this).attr('keyId');
            var suffix  = $('#referral_suffix_values_drops_'+keyId).val();

            if(suffix == "") {
                $('body #referralSuffixError_'+keyId).html('Select suffix');
                isError     = 1;
            } else {
                $('body #referralSuffixError_'+keyId).html('');
            }

        });
       

        //title data validation
        $(".refrTitle").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#referralTitle_'+keyId+' option:selected').val() == "") {
                $('body #referralTitleError_'+keyId).html('Select title');
                isError     = 1;
            } else {
                $('body #referralTitleError_'+keyId).html('');
            }
        });

        //first name data validation
        $(".refrFirstName").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#refferalFirstName_'+keyId+' option:selected').val() == "") {
                $('body #refferalFirstNameError_'+keyId).html('Select first name');
                isError     = 1;
            } else {
                $('body #refferalFirstNameError_'+keyId).html('');
            }
        });

        //surname data validation
        $(".refrSurname").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#referralSurname_'+keyId+' option:selected').val() == "") {
                $('body #referralSurnameError_'+keyId).html('Select surname');
                isError     = 1;
            } else {
                $('body #referralSurnameError_'+keyId).html('');
            }
        });

        if(isError == 1) {
            $(".contact-referral-error-block").show();
            return false;
        } else {
            $(".contact-referral-error-block").hide();
        }
    });

    //suffix data validation
    $(document).on('change','.refrSuffix',function(){     
        var keyId     = $(this).attr('keyId');
        if($('#referralSuffix_'+keyId+' option:selected').val() == "") {
            $('body #referralSuffixError_'+keyId).html('Select suffix');
            isError     = 1;
        } else {
            $('body #referralSuffixError_'+keyId).html('');
        }
    });

    //title data validation
    $(document).on('change','.refrTitle',function(){     
        var keyId     = $(this).attr('keyId');
        if($('#referralTitle_'+keyId+' option:selected').val() == "") {
            $('body #referralTitleError_'+keyId).html('Select title');
            isError     = 1;
        } else {
            $('body #referralTitleError_'+keyId).html('');
        }
    });

    //first name data validation
    $(document).on('change','.refrFirstName',function(){  
        var keyId     = $(this).attr('keyId');
        if($('#refferalFirstName_'+keyId+' option:selected').val() == "") {
            $('body #refferalFirstNameError_'+keyId).html('Select first name');
            isError     = 1;
        } else {
            $('body #refferalFirstNameError_'+keyId).html('');
        }
    });

    //surname data validation
    $(document).on('change','.refrSurname',function(){        
        var keyId     = $(this).attr('keyId');
        if($('#referralSurname_'+keyId+' option:selected').val() == "") {
            $('body #referralSurnameError_'+keyId).html('Select surname');
            isError     = 1;
        } else {
            $('body #referralSurnameError_'+keyId).html('');
        }
    });

    // digit only for no. of dependent
    $(document).on('keypress','.referralNoOfDependent',function(e){    
        var keyId       = $(this).attr('keyId');
        var dependent   = $('#referral_no_of_dependents_'+keyId).val();

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
            $("body #err_referral_no_of_dependents_"+keyId).html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    // $('input[name=referral_photo]').on('change',function(){
    //     if (this.files && this.files[0]) {
    //         var file = this.files[0];
    //         var fileType = file["type"];
    //         var ValidImageTypes = ["image/jpeg", "image/jpg", "image/png"];
    //         if ($.inArray(fileType, ValidImageTypes) < 0) {
    //             $('.referralPhotoError').html('select only jpeg,jpg,png image');
    //             return false;
    //         } else {
    //             $('.referralPhotoError').html('');
    //             displayClass = '.referral_image';
    //             DisplayOnChangeFile(this.files,displayClass);
    //         }
    //     }
    // });
//contact REFERRAL js validation - end
});

function referralFullnameWriter(keyId = "") {

    if(keyId != "") {
        var fullname        = "";
        var titalValue      = $('#referralTitle_'+keyId+" option:selected").text();
        var surnameValue    = $('#referralSurname_'+keyId+" option:selected").text();
        var firstValue      = $('#refferalFirstName_'+keyId+" option:selected").text();
        var fatherValue     = $('#referral_father_name_'+keyId+" option:selected").text();
        fullname            = titalValue+' '+firstValue+' '+fatherValue+' '+surnameValue; 
        $('body #referral_fullname_'+keyId).val(fullname);
    }
}