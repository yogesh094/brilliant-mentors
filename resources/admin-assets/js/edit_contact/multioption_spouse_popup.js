$(document).ready(function(){

	// edit contact multiple option popup js start

    /* suffix start */
	$(document).on('focus','.contact_spouse_suffix_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_spouse_suffix_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_spouse_suffix_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_spouse_suffix_multi_block_'+keyId).removeClass('hide');

        var suffixValue     = $("body #spouse_suffix_values_drops_"+keyId).val();

        $.each(suffixValue.split(","), function(i,e){
            $("body .drops_block #spouseSuffix_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitSpouseDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #spouseSuffix_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitSpouseDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#spouseSuffix_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#spouseSuffix_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var suffixtext      = selMulti.join(", ");
        var suffixValue     = selMulValue.join(",");
        $("body #spouse_suffix_text_"+keyId).val(suffixtext);
        $("body #spouse_suffix_values_drops_"+keyId).val(suffixValue);
        
        $('body .contact_spouse_suffix_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* suffix end */

    /* language start */
    $(document).on('focus','.contact_spouse_languages_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_spouse_languages_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_spouse_languages_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_spouse_languages_multi_block_'+keyId).removeClass('hide');

        var langValue     = $("body #spouse_language_values_drops_"+keyId).val();

        $.each(langValue.split(","), function(i,e){
            $("body .drops_block #spouseLanguages_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitSpouseLanguageDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #spouseLanguages_"+keyId+" option[value='']").prop("selected", false);
    });

    $(document).on('click','.submitSpouseLanguageDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#spouseLanguages_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#spouseLanguages_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var langtext      = selMulti.join(", ");
        var langValue     = selMulValue.join(",");
        $("body #spouse_language_text_"+keyId).val(langtext);
        $("body #spouse_language_values_drops_"+keyId).val(langValue);
        
        $('body .contact_spouse_languages_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* language end */

    /* political party start */
    $(document).on('focus','.contact_spouse_political_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_spouse_political_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_spouse_political_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_spouse_political_multi_block_'+keyId).removeClass('hide');

        var langValue     = $("body #spouse_political_values_drops_"+keyId).val();

        $.each(langValue.split(","), function(i,e){
            $("body .drops_block #spousePoliticalParty_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitSpousePoliticalDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #spousePoliticalParty_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitSpousePoliticalDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#spousePoliticalParty_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#spousePoliticalParty_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var langtext      = selMulti.join(", ");
        var langValue     = selMulValue.join(",");
        $("body #spouse_political_text_"+keyId).val(langtext);
        $("body #spouse_political_values_drops_"+keyId).val(langValue);
        
        $('body .contact_spouse_political_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* political party end */

    /* administrative group start */
    $(document).on('focus','.contact_spouse_adminigp_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_spouse_adminigp_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_spouse_adminigp_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_spouse_adminigp_multi_block_'+keyId).removeClass('hide');

        var adminiValue     = $("body #spouse_adminigroup_values_drops_"+keyId).val();

        $.each(adminiValue.split(","), function(i,e){
            $("body .drops_block #spouseAdminiGroup_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitSpouseAdminigpDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #spouseAdminiGroup_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitSpouseAdminigpDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#spouseAdminiGroup_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#spouseAdminiGroup_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var adminiText      = selMulti.join(", ");
        var adminiValue     = selMulValue.join(",");
        $("body #spouse_administrative_group_"+keyId).val(adminiText);
        $("body #spouse_adminigroup_values_drops_"+keyId).val(adminiValue);
        
        $('body .contact_spouse_adminigp_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* administrative group end */

    /* charitable organization group start */
    $(document).on('focus','.contact_spouse_charityOrg_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_spouse_charityOrg_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_spouse_charityOrg_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_spouse_charityOrg_multi_block_'+keyId).removeClass('hide');

        var adminiValue     = $("body #spouse_charityOrg_values_drops_"+keyId).val();

        $.each(adminiValue.split(","), function(i,e){
            $("body .drops_block #spouseCharityOrg_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitSpouseCharityOrgDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #spouseCharityOrg_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitSpouseCharityOrgDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#spouseCharityOrg_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#spouseCharityOrg_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var charityText      = selMulti.join(", ");
        var charityValue     = selMulValue.join(",");
        $("body #spouse_charity_org_"+keyId).val(charityText);
        $("body #spouse_charityOrg_values_drops_"+keyId).val(charityValue);
        
        $('body .contact_spouse_charityOrg_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* charitable organization end */

    
    /* profession start */
    $(document).on('focus','.contact_spouse_Profession_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_spouse_profession_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_spouse_profession_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_spouse_profession_multi_block_'+keyId).removeClass('hide');

        var langValue     = $("body #spouse_profession_values_drops_"+keyId).val();


        $.each(langValue.split(","), function(i,e){
            $("body .drops_block #spouseProfession_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitSpouseProfessionDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #spouseProfession_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitSpouseProfessionDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#spouseProfession_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#spouseProfession_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var proftext      = selMulti.join(", ");
        var profValue     = selMulValue.join(",");
        $("body #spouse_profession_text_"+keyId).val(proftext);
        $("body #spouse_profession_values_drops_"+keyId).val(profValue);
        
        $('body .contact_spouse_profession_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* profession end */

    /* nationality start */
    $(document).on('focus','.contact_spouse_nationality_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_spouse_nationality_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_spouse_nationality_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_spouse_nationality_multi_block_'+keyId).removeClass('hide');

        var nationalityValue     = $("body #spouse_nationality_values_drops_"+keyId).val();

        $.each(nationalityValue.split(","), function(i,e){
            $("body .drops_block #spouseNationality_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitSpouseNatinalityDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #spouseNationality_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitSpouseNatinalityDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#spouseNationality_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#spouseNationality_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var nationalitytext      = selMulti.join(", ");
        var nationalityValue     = selMulValue.join(",");
        $("body #spouse_nationality_text_"+keyId).val(nationalitytext);
        $("body #spouse_nationality_values_drops_"+keyId).val(nationalityValue);
        
        $('body .contact_spouse_nationality_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* natinality end */

    /* sponsorship start */
    $(document).on('focus','.contact_spouse_sponsorship_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_spouse_sponsorship_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_spouse_sponsorship_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_spouse_sponsorship_multi_block_'+keyId).removeClass('hide');

        var sponsorValue     = $("body #spouse_sponsorship_values_drops_"+keyId).val();

        $.each(sponsorValue.split(","), function(i,e){
            $("body .drops_block #spouseSponsorship_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitSpouseSponsorDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #spouseSponsorship_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitSpouseSponsorDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#spouseSponsorship_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#spouseSponsorship_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var sponsortext      = selMulti.join(", ");
        var sponsorValue     = selMulValue.join(",");
        $("body #spouse_sponsorship_text_"+keyId).val(sponsortext);
        $("body #spouse_sponsorship_values_drops_"+keyId).val(sponsorValue);
        
        $('body .contact_spouse_sponsorship_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* sponsorship end */

    /* internal organization start */
    $(document).on('focus','.contact_spouse_internalOrg_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_spouse_internalOrg_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_spouse_internalOrg_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_spouse_internalOrg_multi_block_'+keyId).removeClass('hide');

        var nationalityValue     = $("body #spouse_internal_values_drops_"+keyId).val();

        $.each(nationalityValue.split(","), function(i,e){
            $("body .drops_block #SpouseInternalOrg_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitSpouseInternalOrgDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #SpouseInternalOrg_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitSpouseInternalOrgDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#SpouseInternalOrg_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#SpouseInternalOrg_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var nationalitytext      = selMulti.join(", ");
        var nationalityValue     = selMulValue.join(",");
        $("body #spouse_internal_text_"+keyId).val(nationalitytext);
        $("body #spouse_internal_values_drops_"+keyId).val(nationalityValue);
        
        $('body .contact_spouse_internalOrg_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* internal organization end */

	// edit contact multiple option popup js end
});