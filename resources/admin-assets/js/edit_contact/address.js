$(document).ready(function(){

    //accorion on multiple address 
    $("#accordion").on("hidden.bs.collapse", function (e) {
        
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordion").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });

//contact address add/edit js validation - start
    $("input[name=submit_contact_address]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var isError         = 0;
        //address type validation
        $(".contact_address_type").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#contact_address_type_'+keyId+' option:selected').val() == "") {
                $('body #err_contact_address_type_'+keyId).html('Select address type');
                isError    = 1;
            } else {
                $('body #err_contact_address_type_'+keyId).html('');
            }
        });
        
        //address region validation
        $(".contact_address_region").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#contact_address_region_id_'+keyId+' option:selected').val() == "") {
                $('body #err_contact_address_region_id_'+keyId).html('Select region');
                isError    = 1;
            } else {
                $('body #err_contact_address_region_id_'+keyId).html('');
            }
        });

        //address country 
        $(".contact_address_country").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#contact_address_country_id_'+keyId+' option:selected').val() == "") {
                $('body #err_contact_address_country_id_'+keyId).html('Select region');
                isError    = 1;
            } else {
                $('body #err_contact_address_country_id_'+keyId).html('');
            }
        });

        //postal code
        $(".contact_address_postal_code").each(function() {
            var keyId       = $(this).attr('keyId');
            var postalcode  = $('#contact_address_postal_code_'+keyId).val();
            
            if(postalcode == "") {
                $('body #err_contact_address_postal_code_'+keyId).html('Postal code is required');
                isError     = 1;
            } else {
                $('body #err_contact_address_postal_code_'+keyId).html('');
            }
        });
        
        //web site
        $(".contact_address_website").each(function(index) {
            var websiteIndex    = index+1;
            var websiteErrorId  = "#err_contact_address_website_"+websiteIndex;
            var websiteVal      = $("#contact_address_website_"+websiteIndex).val();
            var resWebsite      = CheckWebsiteVlidate(websiteVal,websiteErrorId);
            if(resWebsite == "true"){
                isError = 1;
            }
        });

        //delivery address
        $(".contact_address_delivery_address").each(function(index) {
            var keyId       = $(this).attr('keyId');

            if($('#contact_address_delivery_address_'+keyId+':checked').length<=0) {
                $('body #err_contact_address_delivery_address_'+keyId).html('Delivery address is required');
                isError     = 1;
            } else {
                $('body #err_contact_address_delivery_address_'+keyId).html('');
            }
        });
        
        if(isError == 1) {
            $(".contact-address-error-block").show();
            return false;
        } else {
            $(".contact-address-error-block").hide();
        }
    });

    $(".address_website").keyup(function(){
        CheckWebsiteVlidation($(this).val().trim(),'.addressWebsite');
    });

    $(".iCheck-helper").click(function(){
        $('#err_contact_address_delivery_address').html('');
    });

    //delivery address on change validation
    $(document).on('ifChanged', '.contact_address_delivery_address',function (event) {  
        $(".contact_address_delivery_address").each(function(index) {
            var keyId       = $(this).attr('keyId');

            if($('#contact_address_delivery_address_'+keyId+':checked').length<=0) {
                $('body #err_contact_address_delivery_address_'+keyId).html('Delivery address is required');
                isError     = 1;
            } else {
                $('body #err_contact_address_delivery_address_'+keyId).html('');
            }
        });
    });
    
    //address region validation
    $(document).on('change','.contact_address_region',function(){        
        var keyId     = $(this).attr('keyId');
        if($('#contact_address_region_id_'+keyId+' option:selected').val() == "") {
            $('body #err_contact_address_region_id_'+keyId).html('Select region');
            isError    = 1;
        } else {
            $('body #err_contact_address_region_id_'+keyId).html('');
        }
    });

    //address country 
    $(document).on('change','.contact_address_country',function(){            
        var keyId     = $(this).attr('keyId');
        if($('#contact_address_country_id_'+keyId+' option:selected').val() == "") {
            $('body #err_contact_address_country_id_'+keyId).html('Select region');
            isError    = 1;
        } else {
            $('body #err_contact_address_country_id_'+keyId).html('');
        }
    });

    //contact address postal code
    $(document).on('keyup','.contact_address_postal_code',function(){    
        var keyId       = $(this).attr('keyId');
        var postalcode  = $('#contact_address_postal_code_'+keyId).val();

        // if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        // //display error message
        //     $("#err_contact_address_postal_code_"+keyId).html("Digits Only").show().fadeOut("slow");
        //     return false;
        // }
        
        if(postalcode == "") {
            $('body #err_contact_address_postal_code_'+keyId).html('Postal code is required');
            isError     = 1;
        } else {
            $('body #err_contact_address_postal_code_'+keyId).html('');
        }
    });

    //contact address po box
    $(document).on('keypress','.addressPoBox',function(e){  
        var keyId       = $(this).attr('keyId');
        var postalcode  = $('#contact_address_postal_code_'+keyId).val();

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
            $("#err_contact_address_postal_code_"+keyId).html("Digits Only").show().fadeOut("slow");
            return false;
        }
    }); 

    //Add address fields - start
    $(document).on("click", '.add_address', function(event) {
        var address_id          = $('.address_multi_fields').last().attr('data-id');
        var addressFormIndex    = $('.address_multi_fields').last().attr('form-index');
        //if adress multi fields not exist
        if(isNaN(address_id)) {   
            address_id  = 1;
        }
        var lat   = 33.893476;
        var lng   = 35.501433;
        $("#addLoader").removeClass('hide');
        $.ajax({
            type: "GET",
            url: base_url+"/multiple/address",
            data: { 'address_id' : address_id, 'addressFormIndex':addressFormIndex,'lat':lat, 'lng':lng,'_token' : "{{ csrf_token() }}" },
            success:function(data) {
                
                $("#address_multiple").append(data);
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $("#addLoader").addClass('hide');
                $('.checkTrashAddress').removeClass('hide');
                var formId  = +(address_id)+1;

                var divId   = "googleMap"+formId;
                
                googleMap(divId,lat,lng,formId);
                $(this).attr("disabled", false);
            },
            beforeSend: function() {
                $("#addLoader").removeClass('hide');
            },
            error: function() {
                $("#addLoader").addClass('hide');
                $(this).attr("disabled", false);
            }
        });
        $(this).attr("disabled", false);
    });
    //Add address fields - end


    //remove contact address start
    $(document).on("click", '.trashAddress', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        
        if(answer==true) {
            $(".address_multi_fields").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/address/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    } else {
                        // console.log(response.status);
                        // console.log("fail");
                    }    

                    //when remove all content is deleted from block
                    if( !$.trim( $('#address_multiple').html() ).length ) {    
                        $('.checkTrashAddress').addClass('hide');
                        $("#address .tagsinput .tag").remove();
                        $('#address .fr-element.fr-view').html("");
                        $('#address .userField').removeAttr('placeholder');
                        
                    } else {
                        $('.checkTrashAddress').removeClass('hide');
                        
                    }                 
                },
                beforeSend: function() {
                    
                },
                error: function() {
                    
                }

            });

        } else {
            
            return false;
        }
    });
    //remove contact address end
});


function getTypeValue(index){
    var typeIndex = index+1;
    var typeVal = $("#contact_address_type_"+typeIndex+" option:selected").val();
    if(typeVal == ""){
        $('#err_contact_address_type_'+(index)).html("Select the address type");
    } else {
        $('#err_contact_address_type_'+(index)).html("");
    }
}

function getPostalCodeValue(index){
    var postalCodeVal = $("#contact_address_postal_code_"+index).val();
    if(postalCodeVal == ""){
        $('#err_contact_address_postal_code_'+index).html("Postalcode is required");
    } else {
        $('#err_contact_address_postal_code_'+index).html("");
    }
}

function getWebsiteValue(index){
    var websiteErrorId  = "#err_contact_address_website_"+index;
    var websiteVal      = $("#contact_address_website_"+index).val();
    CheckWebsiteVlidate(websiteVal,websiteErrorId);
}

/*function myMap() {
    var mapProp= {
        center:new google.maps.LatLng(33.9312457,35.0632803),
        zoom:5,
    };
    // var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}*/