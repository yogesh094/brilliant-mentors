$(document).ready(function(){

	// edit contact multiple option popup js start

    /* suffix start */
    	// open modal when focus on contact suffix field start 
    	$(document).on('focus','.contact_representative_suffix_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_suffix_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_suffix_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_suffix_multi_block_'+keyId).removeClass('hide');

            var suffixValue     = $("body #representative_suffix_values_drops_"+keyId).val();

            $.each(suffixValue.split(","), function(i,e){
                $("body .drops_block #contact_rep_suffix_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativeDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #contact_rep_suffix_"+keyId+" option[value='']").prop("selected", false);
        });
        // open modal when focus on contact suffix field end

        //set dropdown on contact suffix fields start
        $(document).on('click','.submitRepresentativeDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#contact_rep_suffix_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#contact_rep_suffix_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var suffixtext      = selMulti.join(", ");
            var suffixValue     = selMulValue.join(",");
            $("body #representative_suffix_text_"+keyId).val(suffixtext);
            $("body #representative_suffix_values_drops_"+keyId).val(suffixValue);
            
            $('body .contact_representative_suffix_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
            // $('#representative_mother_name').focus();
        });
        //set dropdown on contact suffix fields end
    /* suffix end */

    /* language start */
        // open modal when focus on contact language field start 
        $(document).on('focus','.contact_resentative_language_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_language_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_language_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_language_multi_block_'+keyId).removeClass('hide');

            var langValue     = $("body #representative_language_values_drops_"+keyId).val();

            $.each(langValue.split(","), function(i,e){
                $("body .drops_block #resentativeLanguages_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativeLanguageDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #resentativeLanguages_"+keyId+" option[value='']").prop("selected", false);
        });
        // open modal when focus on contact language field end

        //set dropdown on contact language fields start
        $(document).on('click','.submitRepresentativeLanguageDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#resentativeLanguages_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#resentativeLanguages_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var langtext      = selMulti.join(", ");
            var langValue     = selMulValue.join(",");
            $("body #representative_language_text_"+keyId).val(langtext);
            $("body #representative_language_values_drops_"+keyId).val(langValue);
            
            $('body .contact_representative_language_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
            // $('.representativeInternalOrganization').focus();
        });
        //set dropdown on contact language fields end
    /* language end */

    /* administrative group start */
        // open modal when focus on contact language field start 
        $(document).on('focus','.contact_representative_adminigroup_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_adminigroup_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_adminigroup_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_adminigroup_multi_block_'+keyId).removeClass('hide');

            var langValue     = $("body #representative_adminigroup_values_drops_"+keyId).val();

            $.each(langValue.split(","), function(i,e){
                $("body .drops_block #representativeAdminiGroup_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativeAdminigpDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #representativeAdminiGroup_"+keyId+" option[value='']").prop("selected", false);
        });
        // open modal when focus on contact language field end

        //set dropdown on contact language fields start
        $(document).on('click','.submitRepresentativeAdminigpDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#representativeAdminiGroup_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#representativeAdminiGroup_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var langtext      = selMulti.join(", ");
            var langValue     = selMulValue.join(",");
            $("body #representative_administrative_group_"+keyId).val(langtext);
            $("body #representative_adminigroup_values_drops_"+keyId).val(langValue);
            
            $('body .contact_representative_adminigroup_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
        });
        //set dropdown on contact language fields end
    /* administrative group end */

    
    /* profession start */
        // open modal when focus on contact language field start 
        $(document).on('focus','.contact_representative_profession_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_profession_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_profession_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_profession_multi_block_'+keyId).removeClass('hide');

            var langValue     = $("body #representative_profession_values_drops_"+keyId).val();

            $.each(langValue.split(","), function(i,e){
                $("body .drops_block #representativeProfession_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativeProfessionDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #representativeProfession_"+keyId+" option[value='']").prop("selected", false);
        });
        // open modal when focus on contact language field end

        //set dropdown on contact language fields start
        $(document).on('click','.submitRepresentativeProfessionDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#representativeProfession_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#representativeProfession_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var proftext      = selMulti.join(", ");
            var profValue     = selMulValue.join(",");
            $("body #representative_profession_text_"+keyId).val(proftext);
            $("body #representative_profession_values_drops_"+keyId).val(profValue);
            
            $('body .contact_representative_profession_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
            // $('#representative_of_birth').focus();
        });
        //set dropdown on contact language fields end
    /* profession end */

    /* nationality start */
        // open modal when focus on contact language field start 
        $(document).on('focus','.contact_representative_nationality_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_nationality_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_nationality_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_nationality_multi_block_'+keyId).removeClass('hide');

            var nationalityValue     = $("body #representative_nationality_values_drops_"+keyId).val();

            $.each(nationalityValue.split(","), function(i,e){
                $("body .drops_block #representativeNationality_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativeNatinalityDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #representativeNationality_"+keyId+" option[value='']").prop("selected", false);
        });
        // open modal when focus on contact language field end

        //set dropdown on contact language fields start
        $(document).on('click','.submitRepresentativeNatinalityDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#representativeNationality_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#representativeNationality_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var nationalitytext      = selMulti.join(", ");
            var nationalityValue     = selMulValue.join(",");
            $("body #representative_nationality_text_"+keyId).val(nationalitytext);
            $("body #representative_nationality_values_drops_"+keyId).val(nationalityValue);
            
            $('body .contact_representative_nationality_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
            // $('#representativeReligion').focus();
        });
        //set dropdown on contact language fields end
    /* natinality end */

    /* internal organization start */
        // open modal when focus on contact language field start 
        $(document).on('focus','.contact_representative_internalOrg_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_internalOrg_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_internalOrg_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_internalOrg_multi_block_'+keyId).removeClass('hide');

            var nationalityValue     = $("body #representative_internalOrg_values_drops_"+keyId).val();

            $.each(nationalityValue.split(","), function(i,e){
                $("body .drops_block #representativeInternalOrg_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativeInternalOrgDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #representativeInternalOrg_"+keyId+" option[value='']").prop("selected", false);
        });
        // open modal when focus on contact language field end

        //set dropdown on contact language fields start
        $(document).on('click','.submitRepresentativeInternalOrgDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#representativeInternalOrg_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#representativeInternalOrg_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var nationalitytext      = selMulti.join(", ");
            var nationalityValue     = selMulValue.join(",");
            $("body #representative_internalOrg_text_"+keyId).val(nationalitytext);
            $("body #representative_internalOrg_values_drops_"+keyId).val(nationalityValue);
            
            $('body .contact_representative_internalOrg_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
            // $('.representativePolitical').focus();
        });
        //set dropdown on contact language fields end
    /* internal organization end */

    /* sponsorship start */
        $(document).on('focus','.contact_representative_sponsorship_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_sponsorship_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_sponsorship_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_sponsorship_multi_block_'+keyId).removeClass('hide');

            var sponsorValue     = $("body #representative_sponsorship_values_drops_"+keyId).val();

            $.each(sponsorValue.split(","), function(i,e){
                $("body .drops_block #representativeSponsorship_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativeSponsorDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #representativeSponsorship_"+keyId+" option[value='']").prop("selected", false);
        });
        
        $(document).on('click','.submitRepresentativeSponsorDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#representativeSponsorship_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#representativeSponsorship_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var sponsortext      = selMulti.join(", ");
            var sponsorValue     = selMulValue.join(",");
            $("body #representative_sponsorship_text_"+keyId).val(sponsortext);
            $("body #representative_sponsorship_values_drops_"+keyId).val(sponsorValue);
            
            $('body .contact_representative_sponsorship_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
            // $("#bloodTypeRepresentative").focus();
        });
    /* sponsorship end */

    /* charitable organization group start */
        $(document).on('focus','.contact_representative_charityOrg_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_charityOrg_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_charityOrg_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_charityOrg_multi_block_'+keyId).removeClass('hide');

            var adminiValue     = $("body #representative_charityOrg_values_drops_"+keyId).val();

            $.each(adminiValue.split(","), function(i,e){
                $("body .drops_block #representativeCharityOrg_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativeCharityOrgDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #representativeCharityOrg_"+keyId+" option[value='']").prop("selected", false);
        });
        
        $(document).on('click','.submitRepresentativeCharityOrgDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#representativeCharityOrg_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#representativeCharityOrg_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var charityText      = selMulti.join(", ");
            var charityValue     = selMulValue.join(",");
            $("body #representative_charity_org_"+keyId).val(charityText);
            $("body #representative_charityOrg_values_drops_"+keyId).val(charityValue);
            
            $('body .contact_representative_charityOrg_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
            // $('.representativeLanguages').focus();
        });
    /* charitable organization end */

    /* political party start */
        $(document).on('focus','.contact_representative_political_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_political_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_political_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_political_multi_block_'+keyId).removeClass('hide');

            var langValue     = $("body #representative_political_values_drops_"+keyId).val();

            $.each(langValue.split(","), function(i,e){
                $("body .drops_block #representativePoliticalParty_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativePoliticalDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #representativePoliticalParty_"+keyId+" option[value='']").prop("selected", false);
        });
        
        $(document).on('click','.submitRepresentativePoliticalDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#representativePoliticalParty_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#representativePoliticalParty_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var langtext      = selMulti.join(", ");
            var langValue     = selMulValue.join(",");
            $("body #representative_political_text_"+keyId).val(langtext);
            $("body #representative_political_values_drops_"+keyId).val(langValue);
            
            $('body .contact_representative_political_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
            // $('.representativeNoOfDependent').focus();
        });
    /* political party end */

    /* Relationship start */
        $(document).on('focus','.contact_representative_relationship_section',function() {

            var keyId   = $(this).attr('keyId');

            $('body .drops_block').html($('.contact_representative_relationship_multi_block_'+keyId).html());
            $('body #MyContactLabel').html($('.contact_representative_relationship_multi_block_'+keyId+' label').html());
            
            $('body .drops_block .contact_representative_relationship_multi_block_'+keyId).removeClass('hide');

            var langValue     = $("body #representative_relationship_values_drops_"+keyId).val();

            $.each(langValue.split(","), function(i,e){
                $("body .drops_block #representativeRelationship_"+keyId+" option[value='"+e+"']").prop("selected", true);
            });

            $('body #multiple-modal-btn-section button').removeClass();
            $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitRepresentativeRelationshipDrops');
            $('body #multiple-modal-btn-section button').attr('keyId',keyId);

            $('#modal_drops').click();
            $("body .drops_block #representativeRelationship_"+keyId+" option[value='']").prop("selected", false);
        });
        
        $(document).on('click','.submitRepresentativeRelationshipDrops',function() {
            var keyId   = $(this).attr('keyId');

            var selMulti    = $.map($("#representativeRelationship_"+keyId+" option:selected"), function (el, i) {
                return $(el).text();
            });

            var selMulValue = $.map($("#representativeRelationship_"+keyId+" option:selected"), function (el, i) {
                return $(el).val();
            });

            var langtext      = selMulti.join(", ");
            var langValue     = selMulValue.join(",");
            $("body #representative_relationship_text_"+keyId).val(langtext);
            $("body #representative_relationship_values_drops_"+keyId).val(langValue);
            
            $('body .contact_representative_relationship_multi_block_'+keyId).html($('body .drops_block').html());
            
            $('#contact_drop_modal').modal('toggle');
            // $('.representativeCaution').focus();
        });
    /* political party end */

	// edit contact multiple option popup js end
});