$(document).ready(function(){

	// edit contact multiple option popup js start

	// open modal when focus on contact suffix field start 
	$(document).on('focus','.contact_suffix_section',function() {

        $('body .drops_block').html($('.contact_suffix_multi_block').html());
        $('body #MyContactLabel').html($('.contact_suffix_multi_block label').html());
        
        $('body .drops_block .contact_suffix_multi_block').removeClass('hide');

        var suffixValue     = $("body #suffix_values_drops").val();

        $.each(suffixValue.split(","), function(i,e){
            $("body .drops_block #contact_suffix option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitDrops');

        $('#modal_drops').click();
        $("body .drops_block #contact_suffix option[value='']").prop("selected", false);
    });
    // open modal when focus on contact suffix field end

    //set dropdown on contact suffix fields start
    $(document).on('focus','.submitDrops',function() {
        var selMulti    = $.map($(".drops_block #contact_suffix option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($(".drops_block #contact_suffix option:selected"), function (el, i) {
            return $(el).val();
        });

        var suffixtext      = selMulti.join(", ");
        var suffixValue     = selMulValue.join(",");
        $("body #suffix_text").val(suffixtext);
        $("body #suffix_values_drops").val(suffixValue);
        
        $('body .contact_suffix_multi_block').html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact suffix fields end

    // open modal when focus on contact language field start 
	$(document).on('focus','.contact_language_section',function() {

        $('body .drops_block').html($('.contact_language_multi_block').html());
        $('body #MyContactLabel').html($('.contact_language_multi_block label').html());
        
        $('body .drops_block .contact_language_multi_block').removeClass('hide');

        var langValue     = $("body #language_values_drops").val();

        $.each(langValue.split(","), function(i,e){
            $("body .drops_block #contact_languages option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitLanguagaeDrops');
        $('#modal_drops').click();
        $("body .drops_block #contact_languages option[value='']").prop("selected", false);
    });
    // open modal when focus on contact language field end

    //set dropdown on contact language fields start
    $(document).on('focus','.submitLanguagaeDrops',function() {
        var selMulti    = $.map($(".drops_block #contact_languages option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($(".drops_block #contact_languages option:selected"), function (el, i) {
            return $(el).val();
        });

        var dropText      = selMulti.join(", ");
        var dropValue     = selMulValue.join(",");
        $("body #language_text").val(dropText);
        $("body #language_values_drops").val(dropValue);
        
        $('body .contact_language_multi_block').html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact language fields end

    // open modal when focus on contact profession field start 
	$(document).on('focus','.contact_profession_section',function() {

        $('body .drops_block').html($('.contact_profession_multi_block').html());
        $('body #MyContactLabel').html($('.contact_profession_multi_block label').html());
        
        $('body .drops_block .contact_profession_multi_block').removeClass('hide');

        var dropValue     = $("body #profession_values_drops").val();

        $.each(dropValue.split(","), function(i,e){
            $("body .drops_block #contact_profession option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitProfessionDrops');
        
        $('#modal_drops').click();
        $("body .drops_block #contact_profession option[value='']").prop("selected", false);
    });
    // open modal when focus on contact profession field end

    //set dropdown on contact profession fields start
    $(document).on('focus','.submitProfessionDrops',function() {
        var selMulti    = $.map($(".drops_block #contact_profession option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($(".drops_block #contact_profession option:selected"), function (el, i) {
            return $(el).val();
        });

        var dropText      = selMulti.join(", ");
        var dropValue     = selMulValue.join(",");
        $("body #profession_text").val(dropText);
        $("body #profession_values_drops").val(dropValue);
        
        $('body .contact_profession_multi_block').html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact profession fields end

    // open modal when focus on contact profession field start 
	$(document).on('focus','.contact_nationality_section',function() {

        $('body .drops_block').html($('.contact_nationality_multi_block').html());
        $('body #MyContactLabel').html($('.contact_nationality_multi_block label').html());
        
        $('body .drops_block .contact_nationality_multi_block').removeClass('hide');

        var dropValue     = $("body #nationality_values_drops").val();

        $.each(dropValue.split(","), function(i,e){
            $("body .drops_block #contact_nationality option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitNationalityDrops');
        
        $('#modal_drops').click();
        $("body .drops_block #contact_nationality option[value='']").prop("selected", false);
    });
    // open modal when focus on contact profession field end

    //set dropdown on contact profession fields start
    $(document).on('focus','.submitNationalityDrops',function() {
        var selMulti    = $.map($(".drops_block #contact_nationality option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($(".drops_block #contact_nationality option:selected"), function (el, i) {
            return $(el).val();
        });

        var dropText      = selMulti.join(", ");
        var dropValue     = selMulValue.join(",");
        $("body #nationality_text").val(dropText);
        $("body #nationality_values_drops").val(dropValue);
        
        $('body .contact_nationality_multi_block').html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact profession fields end

	// edit contact multiple option popup js end
});