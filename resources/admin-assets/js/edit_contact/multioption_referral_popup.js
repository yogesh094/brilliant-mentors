$(document).ready(function(){

	// edit contact multiple option popup js start

    /* suffix start */
	$(document).on('focus','.contact_referral_suffix_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_suffix_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_suffix_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_suffix_multi_block_'+keyId).removeClass('hide');

        var suffixValue     = $("body #referral_suffix_values_drops_"+keyId).val();

        $.each(suffixValue.split(","), function(i,e){
            $("body .drops_block #referralSuffix_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralSuffix_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitReferralDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralSuffix_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralSuffix_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var suffixtext      = selMulti.join(", ");
        var suffixValue     = selMulValue.join(",");
        $("body #referral_suffix_text_"+keyId).val(suffixtext);
        $("body #referral_suffix_values_drops_"+keyId).val(suffixValue);
        
        $('body .contact_referral_suffix_multi_block_'+keyId).html($('body .drops_block').html());
        
        $("body #referral_suffix_values_drops_"+keyId).focus();
        $('#contact_drop_modal').modal('toggle');
    });
    /* suffix end */

    /* language start */
    $(document).on('focus','.contact_referral_languages_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_languages_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_languages_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_languages_multi_block_'+keyId).removeClass('hide');

        var langValue     = $("body #referral_languages_values_drops_"+keyId).val();

        $.each(langValue.split(","), function(i,e){
            $("body .drops_block #referralLanguages_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralLanguageDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralLanguages_"+keyId+" option[value='']").prop("selected", false);
    });

    $(document).on('click','.submitReferralLanguageDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralLanguages_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralLanguages_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var langtext      = selMulti.join(", ");
        var langValue     = selMulValue.join(",");
        $("body #referral_languages_text_"+keyId).val(langtext);
        $("body #referral_languages_values_drops_"+keyId).val(langValue);
        
        $('body .contact_referral_languages_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* language end */

    /* political party start */
    $(document).on('focus','.contact_referral_political_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_political_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_political_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_political_multi_block_'+keyId).removeClass('hide');

        var politicalValue     = $("body #referral_political_values_drops_"+keyId).val();

        $.each(politicalValue.split(","), function(i,e){
            $("body .drops_block #referralPoliticalParty_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralPoliticalDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralPoliticalParty_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitReferralPoliticalDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralPoliticalParty_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralPoliticalParty_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var politicaltext      = selMulti.join(", ");
        var politicalValue     = selMulValue.join(",");
        $("body #referral_political_text_"+keyId).val(politicaltext);
        $("body #referral_political_values_drops_"+keyId).val(politicalValue);
        
        $('body .contact_referral_political_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* political party end */

    /* relationship reference start */
    $(document).on('focus','.contact_referral_relation_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_relation_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_relation_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_relation_multi_block_'+keyId).removeClass('hide');

        var relationValue     = $("body #referral_relation_values_drops_"+keyId).val();

        $.each(relationValue.split(","), function(i,e){
            $("body .drops_block #referralRelationRef_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralRelationDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralRelationRef_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitReferralRelationDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralRelationRef_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralRelationRef_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var relationtext      = selMulti.join(", ");
        var relationValue     = selMulValue.join(",");
        $("body #referral_relation_text_"+keyId).val(relationtext);
        $("body #referral_relation_values_drops_"+keyId).val(relationValue);
        
        $('body .contact_referral_relation_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* relationship reference end */

    /* administrative group start */
    $(document).on('focus','.contact_referral_adminigp_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_adminigp_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_adminigp_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_adminigp_multi_block_'+keyId).removeClass('hide');

        var adminiValue     = $("body #referral_adminigroup_values_drops_"+keyId).val();

        $.each(adminiValue.split(","), function(i,e){
            $("body .drops_block #referralAdminiGroup_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralAdminigpDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralAdminiGroup_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitReferralAdminigpDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralAdminiGroup_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralAdminiGroup_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var adminiText      = selMulti.join(", ");
        var adminiValue     = selMulValue.join(",");
        $("body #referral_adminigroup_text_"+keyId).val(adminiText);
        $("body #referral_adminigroup_values_drops_"+keyId).val(adminiValue);
        
        $('body .contact_referral_adminigp_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* administrative group end */

    /* charitable organization group start */
    $(document).on('focus','.contact_referral_charityOrg_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_charityOrg_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_charityOrg_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_charityOrg_multi_block_'+keyId).removeClass('hide');

        var adminiValue     = $("body #referral_charityOrg_values_drops_"+keyId).val();

        $.each(adminiValue.split(","), function(i,e){
            $("body .drops_block #referralCharityOrg_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralCharityOrgDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralCharityOrg_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitReferralCharityOrgDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralCharityOrg_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralCharityOrg_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var charityText      = selMulti.join(", ");
        var charityValue     = selMulValue.join(",");
        $("body #referral_charityOrg_text_"+keyId).val(charityText);
        $("body #referral_charityOrg_values_drops_"+keyId).val(charityValue);
        
        $('body .contact_referral_charityOrg_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* charitable organization end */
    
    /* profession start */
    $(document).on('focus','.contact_referral_profession_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_profession_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_profession_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_profession_multi_block_'+keyId).removeClass('hide');

        var langValue     = $("body #referral_profession_values_drops_"+keyId).val();

        $.each(langValue.split(","), function(i,e){
            $("body .drops_block #referralProfession_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralProfessionDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralProfession_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitReferralProfessionDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralProfession_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralProfession_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var proftext      = selMulti.join(", ");
        var profValue     = selMulValue.join(",");
        $("body #referral_profession_text_"+keyId).val(proftext);
        $("body #referral_profession_values_drops_"+keyId).val(profValue);
        
        $('body .contact_referral_profession_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* profession end */

    /* nationality start */
    $(document).on('focus','.contact_referral_nationality_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_nationality_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_nationality_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_nationality_multi_block_'+keyId).removeClass('hide');

        var nationalityValue     = $("body #referral_nationality_values_drops_"+keyId).val();

        $.each(nationalityValue.split(","), function(i,e){
            $("body .drops_block #referralNationality_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralNatinalityDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralNationality_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitReferralNatinalityDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralNationality_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralNationality_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var nationalitytext      = selMulti.join(", ");
        var nationalityValue     = selMulValue.join(",");
        $("body #referral_nationality_text_"+keyId).val(nationalitytext);
        $("body #referral_nationality_values_drops_"+keyId).val(nationalityValue);
        
        $('body .contact_referral_nationality_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* natinality end */

    /* sponsorship start */
    $(document).on('focus','.contact_referral_sponsors_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_sponsors_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_sponsors_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_sponsors_multi_block_'+keyId).removeClass('hide');

        var sponsorValue     = $("body #referral_sponsors_values_drops_"+keyId).val();

        $.each(sponsorValue.split(","), function(i,e){
            $("body .drops_block #referralSponsors_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralSponsorDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralSponsors_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitReferralSponsorDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralSponsors_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralSponsors_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var sponsortext      = selMulti.join(", ");
        var sponsorValue     = selMulValue.join(",");
        $("body #referral_sponsors_text_"+keyId).val(sponsortext);
        $("body #referral_sponsors_values_drops_"+keyId).val(sponsorValue);
        
        $('body .contact_referral_sponsors_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* sponsorship end */

    /* internal organization start */
    $(document).on('focus','.contact_referral_internalOrg_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_referral_internalOrg_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_referral_internalOrg_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_referral_internalOrg_multi_block_'+keyId).removeClass('hide');

        var internalOrgValue     = $("body #referral_internalOrg_values_drops_"+keyId).val();
        $.each(internalOrgValue.split(","), function(i,e){
            $("body .drops_block #referralInternalOrgz_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitReferralInternalOrgDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #referralInternalOrgz_"+keyId+" option[value='']").prop("selected", false);
    });
    
    $(document).on('click','.submitReferralInternalOrgDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#referralInternalOrgz_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#referralInternalOrgz_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var internalOrgtext      = selMulti.join(", ");
        var internalOrgValue     = selMulValue.join(",");
        $("body #referral_internalOrg_text_"+keyId).val(internalOrgtext);
        $("body #referral_internalOrg_values_drops_"+keyId).val(internalOrgValue);
        
        $('body .contact_referral_internalOrg_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    /* internal organization end */

	// edit contact multiple option popup js end
});