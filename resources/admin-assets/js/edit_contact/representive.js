$(document).ready(function(){


    // accorion on multiple contact number 
    $("#accordionContactRepresentive").on("hidden.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionContactRepresentive").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });

    // add more on contact rep start
    $(document).on('click','.addBlockRepresentive',function(event) {

        var address_id = $('.contactRepresentiveAjaxBlock').last().attr('data-id');
        var representativeFormIndex   = $('.contactRepresentiveAjaxBlock').last().attr('form-index');
        $("#addRepresentiveLoader").removeClass('hide');
        $.ajax({
            type: "GET",
            url: base_url+"/contact/multiple-contact-representive",
            data: { 'address_id' : address_id, 'representativeFormIndex':representativeFormIndex,'_token' : "{{ csrf_token() }}" },
            success:function(data) {
                $("#representiveAddnewBlock").append(data);
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $('.checkTrashRepresentative').removeClass('hide');
                $("#addRepresentiveLoader").addClass('hide');
                $("select option[value='']").attr("disabled", true);
                $(this).attr("disabled", false);
            },
            beforeSend: function(){
                $("#addRepresentiveLoader").removeClass('hide');
            },
            complete: function(){
                $("#addRepresentiveLoader").addClass('hide');
            },
            error: function() {
                $("#addRepresentiveLoader").addClass('hide');
                $(this).attr("disabled", false);
            }
        });
        $(this).attr("disabled", false);
    })
    // add more on contact rep end

    //remove contact rep start
    $(document).on("click", '.trashRepresentive', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        console.log(trashId);
        if(answer==true) {
            $(".contactRepresentiveAjaxBlock").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/contact-representive/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    }
                    if( !$.trim( $('#representiveAddnewBlock').html() ).length ) {    
                        $('.checkTrashRepresentative').addClass('hide');
                        $("#representative .tagsinput .tag").remove();
                        $('#representative .fr-element.fr-view').html("");
                        $('#representative .userField').removeAttr('placeholder');
                    
                    } else {
                        $('.checkTrashRepresentative').removeClass('hide');
                    
                    } 
                },
                beforeSend: function() {

                },
                error: function() {

                }

            });

        } else {
            return false;
        }
    });
    //remove contact rep end
    
    // load date of birth date picker
    $('body').on('focus',".rep_dateofbirth", function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,        
            singleClasses: "picker_2"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });
    
    //validation on submit start 
    $("input[name=submit_contact_representive]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);
        
        var isError         = 0;
        // contact key validation start
        $(".repContactKeyData").each(function(){
            var keyId       = $(this).attr('keyId');
            var contactKey  = $('#contact_unique_key_'+keyId).val();
            
            if(contactKey == "") {
                $('body #err_contact_unique_key_'+keyId).html('Contact key is required');
                isError     = 1;
            } else {
                $('body #err_contact_unique_key_'+keyId).html('');
            }
        });

        $('.representative_image').each(function(){
            var keyId       = $(this).attr('keyid');
            var file        = this.files[0];
            var fileType    = file["type"];
            var ValidImageTypes = ["image/jpeg", "image/jpg", "image/png"];
            
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                $('.representativePhotoError_'+keyId).html('select only jpeg,jpg,png image');
                isError     = 1;
            } else {
                $('.representativePhotoError_'+keyId).html('');
            }
        });
        //suffix data validation
        $(".repSuffixData").each(function(){
            
            var keyId   = $(this).attr('keyId');
            var suffix  = $('#representative_suffix_values_drops_'+keyId).val();

            if(suffix == "") {
                $('body #err_contact_rep_suffix_'+keyId).html('Select suffix');
                isError     = 1;
            } else {
                $('body #err_contact_rep_suffix_'+keyId).html('');
            }

        });

        
        //title data validation
        $(".repTitleData").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#contact_rep_title_'+keyId+' option:selected').val() == "") {
                $('body #err_contact_rep_title_'+keyId).html('Select title');
                isError     = 1;
            } else {
                $('body #err_contact_rep_title_'+keyId).html('');
            }
        });

        //first name data validation
        $(".repFirstNameData").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#contact_rep_first_name_'+keyId+' option:selected').val() == "") {
                $('body #err_contact_rep_first_name_'+keyId).html('Select first name');
                isError     = 1;
            } else {
                $('body #err_contact_rep_first_name_'+keyId).html('');
            }
        });

        //surname data validation
        $(".resSurname").each(function(){
            var keyId     = $(this).attr('keyId');
            
            if($('#contact_rep_surname_'+keyId+' option:selected').val() == "") {
                $('body #err_contact_rep_surname_'+keyId).html('Select surname');
                console.log("if "+ keyId);
                isError     = 1;
            } else {
                console.log("else "+ keyId);
                $('body #err_contact_rep_surname_'+keyId).html('');
            }
        });

        if(isError == 1) {
            $(".contact-representive-error-block").show();
            return false;
        } else {
            $(".contact-representive-error-block").hide();
        }
    });
    //validation on submit end 

    //remove error when change title
    $(document).on('change','.repTitleData',function(){
        var keyId       = $(this).attr('keyId');
        if($('#contact_rep_title_'+keyId+' option:selected').val() == "") {
            $('body #err_contact_rep_title_'+keyId).html('Select the political position type');
            isError     = 1;
        } else {
            $('body #err_contact_rep_title_'+keyId).html('');
        }
    });

    //remove error when change first name
    $(document).on('change','.repFirstNameData',function(){
        var keyId       = $(this).attr('keyId');
        if($('#contact_rep_first_name_'+keyId+' option:selected').val() == "") {
            $('body #err_contact_rep_first_name_'+keyId).html('Select the political position type');
            isError     = 1;
        } else {
            $('body #err_contact_rep_first_name_'+keyId).html('');
        }
    });

    //remove error when change surname
    $(document).on('change','.resSurname',function(){
        var keyId       = $(this).attr('keyId');
        if($('#contact_rep_surname_'+keyId+' option:selected').val() == "") {
            $('body #err_contact_rep_surname_'+keyId).html('Select the political position type');
            isError     = 1;
        } else {
            $('body #err_contact_rep_surname_'+keyId).html('');
        }
    });

    //remove error when change suffix
    $(document).on('change','.repSuffixData',function(){
        var keyId       = $(this).attr('keyId');
        if($('#contact_rep_suffix_'+keyId+' option:selected').val() == "") {
            $('body #err_contact_rep_suffix_'+keyId).html('Select the political position type');
            isError     = 1;
        } else {
            $('body #err_contact_rep_suffix_'+keyId).html('');
        }
    });

    // digit only for no. of dependent
    $(document).on('keypress','.representativeNoOfDependent',function(e){    
        var keyId       = $(this).attr('keyId');
        var dependent   = $('#representativeNoOfDependents_'+keyId).val();

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
            $("body #err_representive_no_of_dep_"+keyId).html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

});


function displayRepresentativeImage(e){
    var keyId = $(e).attr('keyid');
    if (e.files[0]) {
        var file = e.files[0];
        var fileType = file["type"];
        console.log(fileType);
        var ValidImageTypes = ["image/jpeg", "image/jpg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            $('.representativePhotoError_'+keyId).html('select only jpeg,jpg,png image');
            return false;
        } else {
            $('.representativePhotoError_'+keyId).html('');
            displayClass = '.representative_image_'+keyId;
            DisplayOnChangeFile(e.files,displayClass);
        }
    }
}


function representativeFullnameWriter(keyId = "") {

    if(keyId != "") {
        var fullname        = "";
        var titalValue      = $('#contact_rep_title_'+keyId+" option:selected").text();
        var surnameValue    = $('#contact_rep_surname_'+keyId+" option:selected").text();
        var firstValue      = $('#contact_rep_first_name_'+keyId+" option:selected").text();
        var fatherValue     = $('#representative_father_name_'+keyId+" option:selected").text();
        fullname            = titalValue+' '+firstValue+' '+fatherValue+' '+surnameValue; 
        $('body #representative_fullname_'+keyId).val(fullname);
    }
}