$(document).ready(function(){
//contact edit js validation - start
    $('#date_of_birth').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,        
        singleClasses: "picker_2"
    }, function (start, end, label) {
        // console.log(start.toISOString(), end.toISOString(), label);
    });
    
    $("input[name=submit_add_contact]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var contact_title       = $("#contact_title option:selected" ).val();
        // var contact_suffix      = $("#contact_suffix option:selected" ).val();
        var contact_suffix      = $("#suffix_values_drops" ).val();
        var contact_first_name  = $("#contact_first_name option:selected" ).val();
        var contact_father_name = $("#contact_father_name option:selected" ).val();
        var contact_mother_name = $("#contact_mother_name option:selected" ).val();
        var contact_surname     = $("#contact_surname option:selected" ).val();
        var contact_unique_key  = $("input[name='contact_unique_key']").val();
        var date_of_birth       = $("input[name='date_of_birth']").val();
        
        var isError     = 0;
        if(contact_title == ""){
            $('#err_contact_title').html('Select the title');
            isError     = 1;
        } else {
            $('#err_contact_title').html('');
        }

        if(contact_suffix == undefined || contact_suffix == ""){
            $('#err_contact_suffix').html('Select the suffix');
            isError     = 1;
        } else {
            $('#err_contact_suffix').html('');
        }

        if(contact_first_name == ""){
            $('#err_contact_first_name').html('Select the first name');
            isError     = 1;
        } else {
            $('#err_contact_first_name').html('');
        }

        if(contact_father_name == ""){
            $('#err_contact_father_name').html('Select the father name');
            isError     = 1;
        } else {
            $('#err_contact_father_name').html('');
        }

        if(contact_mother_name == ""){
            $('#err_contact_mother_name').html('Select the mother name');
            isError     = 1;
        } else {
            $('#err_contact_mother_name').html('');
        }

        if(contact_surname == ""){
            $('#err_contact_surname').html('Select the surname');
            isError     = 1;
        } else {
            $('#err_contact_surname').html('');
        }

        if(contact_unique_key == ""){
            $('#err_contact_unique_key').html('Contact unique is required');
            isError     = 1;
        } else {
            $('#err_contact_unique_key').html('');
        }

        if(date_of_birth == ""){
            $('#err_date_of_birth').html('Select the date of birth');
            isError     = 1;
        } else {
            $('#err_date_of_birth').html('');
        }

        if(isError == 1) {
            $(".contact-error-block").show();
            return false;
        } else {
            $(".contact-error-block").hide();
        }

        var fileType = $('input[name=photo]')[0].files[0]['type'];
        var ValidImageTypes = ["image/jpeg", "image/jpg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            $('#err_contact_photo').html('select only jpeg,jpg,png image');
            return false;
        }
    });

    $("input[name='contact_unique_key']").keyup(function(){
        var contact_unique_key = $(this).val().trim();
        if(contact_unique_key == "") {            
            $('#err_contact_unique_key').html('Contact id is required');
        } else {
            $('#err_contact_unique_key').html('');
        }
    });

    $('#contact_suffix option').on('click',function(){
        var contact_suffix = $( "#contact_suffix option:selected" ).length;
        if(contact_suffix == 0) {
            $('#err_contact_suffix').html('Select the Suffix');
            return false;
        } else {
            $('#err_contact_suffix').html('');
        }
    });

    $('#contact_first_name').change(function(){
        var contact_first_name = $( "#contact_first_name option:selected" ).val();
        if(contact_first_name == "") {
            $('#err_contact_first_name').html('Select the first name');
            return false;
        }
        $('#err_contact_first_name').html('');
    });

    $('#contact_father_name').change(function(){
        var contact_father_name = $( "#contact_father_name option:selected" ).val();
        if(contact_father_name == "") {
            $('#err_contact_father_name').html('Select the father name');
            return false;
        }
        $('#err_contact_father_name').html('');
    });

    $('#contact_mother_name').change(function(){
        var contact_mother_name = $( "#contact_mother_name option:selected" ).val();
        if(contact_mother_name == "") {
            $('#err_contact_mother_name').html('Select the mother name');
            return false;
        }
        $('#err_contact_mother_name').html('');
    });

    $('#contact_surname').change(function(){
        var contact_surname = $( "#contact_surname option:selected" ).val();
        if(contact_surname == "") {
            $('#err_contact_surname').html('Select the Surname');
            return false;
        }
        $('#err_contact_surname').html('');
    });

    $('input[name=photo]').on('change',function(){
        if (this.files && this.files[0]) {
            var file = this.files[0];
            var fileType = file["type"];
            var ValidImageTypes = ["image/jpeg", "image/jpg", "image/png"];
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                $('#err_contact_photo').html('select only jpeg,jpg,png image');
                return false;
            } else {
                $('#err_contact_photo').html('');
                displayClass = '.contact_image';
                DisplayOnChangeFile(this.files,displayClass);
            }
        }
    });
//contact edit js validation - end
});