$(document).ready(function(){

    // political position accordion start
    $("#accordionContactPolitical").on("hidden.bs.collapse", function (e) {

        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionContactPolitical").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });
    // political position accordion end

    // load date start picker
    $('body').on('focus',".pp_startdate", function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            singleClasses: "picker_2"
        }, function (start, end, label) {
            // console.log(start.toISOString(), end.toISOString(), label);
        });
    });
    // load date start picker end

    // load date end picker
    $('body').on('focus',".pp_enddate", function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            singleClasses: "picker_2"
        }, function (start, end, label) {
            // console.log(start.toISOString(), end.toISOString(), label);
        });
    });
    // load date end picker end

    //contact political position js validation on submit
    $("input[name=submit_contact_political_pos]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var isError         = 0;
        //political position type held
        $(".poliPosTypeData").each(function(){
            // var keyId       = $(this).attr('keyId');
            var keyId     = $(this).attr('keyId');
            console.log(keyId);
            if($('#contact_poli_pos_type_'+keyId+' option:selected').val() == "") {
                $('body #err_contact_poli_pos_type_'+keyId).html('Select the political position type');
                isError     = 1;
            } else {
                $('body #err_contact_poli_pos_type_'+keyId).html('');
            }
        });

        //political position type held
        $(".poliPosHeldData").each(function(){
            // var keyId       = $(this).attr('keyId');
            var keyId     = $(this).attr('keyId');

            if($('#contact_poli_pos_held_'+keyId+' option:selected').val() == "") {
                $('body #err_contact_poli_pos_held_'+keyId).html('Select the political position type');
                isError     = 1;
            } else {
                $('body #err_contact_poli_pos_held_'+keyId).html('');
            }
        });

        //start date validation
        $(".poliPosStartDate").each(function(){
            var keyId       = $(this).attr('keyId');
            var start_date  = $('#political_posotion_start_date_'+keyId).val();
            var end_date    = $('#political_posotion_end_date_'+keyId).val();

            if(start_date == "") {
                $('body #err_political_posotion_start_date_'+keyId).html('Start date is required');
                isError     = 1;
            } else {
                $('body #err_political_posotion_start_date_'+keyId).html('');
            }

            if( start_date != "" && end_date != "") {
                var res     = CompareDate(start_date,end_date);
                if(!res) {
                    $('body #err_political_posotion_end_date_'+keyId).html('Exit date must be a greater than start date');
                    isError = 1;
                } else {
                    $('body #err_political_posotion_end_date_'+keyId).html('');
                }
            }
        });

        //start date validation
        $(document).on('change',".poliPosStartDate", function(){
            var startDate   = $(this).val();
            var keyId       = $(this).attr('keyId');
            if(startDate != ""){
                $('#err_political_posotion_start_date_'+keyId).html("");
            }
        });

        if(isError == 1) {
            $(".political_position_err_block").show();
            return false;
        } else {
            $(".political_position_err_block").hide();
        }
    });
    //contact political position js validation on submit end

    //remove error when change type
    $(document).on('change','.poliPosTypeData',function(){
        var keyId       = $(this).attr('keyId');
        if($('#contact_poli_pos_type_'+keyId+' option:selected').val() == "") {
            $('body #err_contact_poli_pos_type_'+keyId).html('Select the political position type');
            isError     = 1;
        } else {
            $('body #err_contact_poli_pos_type_'+keyId).html('');
        }
    });

    //remove error when change poli held
    $(document).on('change','.poliPosHeldData',function(){
        var keyId       = $(this).attr('keyId');
        if($('#contact_poli_pos_held_'+keyId+' option:selected').val() == "") {
            $('body #err_contact_poli_pos_held_'+keyId).html('Select the political position held');
            isError     = 1;
        } else {
            $('body #err_contact_poli_pos_held_'+keyId).html('');
        }
    });
    //remove error when change poli held end

    // add more on political position start
    $(document).on('click','.addBlockPolitical',function(event) {

        var politicalblock_id   = $('.contactPoliticalAjaxBlock').last().attr('data-id');
         var poliPosFormIndex   = $('.contactPoliticalAjaxBlock').last().attr('form-index');

        $.ajax({
            type: "GET",
            url: base_url+"/contact/multiple-political-positions",
            data: { 'politicalblock_id' : politicalblock_id,'poliPosFormIndex':poliPosFormIndex, '_token' : "{{ csrf_token() }}" },
            success:function(data) {

                $("#politicalAddnewBlock").append(data);
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $('.checkTrashPoliticalPos').removeClass('hide');
                $("#addPoliticalLoader").addClass('hide');


                $(this).attr("disabled", false);
            },
            beforeSend: function(){
                $("#addPoliticalLoader").removeClass('hide');
            },
            complete: function(){
                $("#addPoliticalLoader").addClass('hide');
            },
            error: function() {
                $("#addPoliticalLoader").addClass('hide');
                $(this).attr("disabled", false);
            }
        });
        $(this).attr("disabled", false);
    })
    // add more on political position end

    //remove contact address start
    $(document).on("click", '.trashPolitical', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        console.log(trashId);
        if(answer==true) {
            $(".contactPoliticalAjaxBlock").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/political-position/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    }
                    //when remove all content is deleted from block
                    if( !$.trim( $('#politicalAddnewBlock').html() ).length ) {
                        $('.checkTrashPoliticalPos').addClass('hide');
                        $("#politicalPositions .tagsinput .tag").remove();
                        $('#politicalPositions .fr-element.fr-view').html("");
                        $('#politicalPositions .userField').removeAttr('placeholder');
                    } else {
                        $('.checkTrashPoliticalPos').removeClass('hide');
                    }
                },
                beforeSend: function() {

                },
                error: function() {

                }

            });

        } else {
            return false;
        }
    });
    //remove contact address end
});