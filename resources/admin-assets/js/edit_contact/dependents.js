$(document).ready(function(){

    // dependent accorion on multiple records
    $("#accordionDependents").on("hidden.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionDependents").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });

     // add more on contact spoise start
    $(document).on('click','.addBlockDependents',function(event) {

        var data_id = $('.contactDependentsAjaxBlock').last().attr('data-id');
        var dependentsFormIndex   = $('.contactDependentsAjaxBlock').last().attr('form-index');
        $("#dependentsLoader").removeClass('hide');
        $.ajax({
            type: "GET",
            url: base_url+"/contact/multiple-contact-dependents",
            data: { 'address_id' : data_id, 'dependentsFormIndex':dependentsFormIndex,'_token' : "{{ csrf_token() }}" },
            success:function(data) {
           
                $("#dependentsAddnewBlock").append(data);
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $('.checkTrashDependents').removeClass('hide');
                $("#dependentsLoader").addClass('hide');
                $("select option[value='']").attr("disabled", true);
                $(this).attr("disabled", false);
            },
            beforeSend: function(){
                $("#dependentsLoader").removeClass('hide');
                $(this).attr("disabled", true);
            },
            complete: function(){
                $("#dependentsLoader").addClass('hide');
            },
            error: function() {
                $("#dependentsLoader").addClass('hide');
                $(this).attr("disabled", false);
            }
        });
        $(this).attr("disabled", false);
    })
    // add more on contact number end

    //remove contact address start
    $(document).on("click", '.trashDependents', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        console.log(trashId);
        if(answer==true) {
            $(".contactDependentsAjaxBlock").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/contact-dependents/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    }
                    //remove all from form if all deleted
                    if( !$.trim( $('#dependentsAddnewBlock').html() ).length ) {    
                        $('.checkTrashDependents').addClass('hide');
                        $("#dependents .tagsinput .tag").remove();
                        $('#dependents .fr-element.fr-view').html("");
                        $('#dependents .userField').removeAttr('placeholder');
                    
                    } else {
                        $('.checkTrashDependents').removeClass('hide');
                    
                    }
                },
                beforeSend: function() {

                },
                error: function() {

                }

            });

        } else {
            $(this).hide();
            return false;
        }
    });
    //remove contact address end

//contact DEPENDENTS js validation - start
    // $('#dependents_date_of_birth').daterangepicker({
    //     singleDatePicker: true,
    //     showDropdowns: true,        
    //     singleClasses: "picker_2"
    //     // singleClasses: "picker_1"
    // }, function (start, end, label) {
    //     console.log(start.toISOString(), end.toISOString(), label);
    // });

    $('body').on('focus',".dep_dateofbirth", function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,        
            singleClasses: "picker_2"
        }, function (start, end, label) {
            // console.log(start.toISOString(), end.toISOString(), label);
        });
    });

    
    $("input[name=submit_dependents]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var isError         = 0;
        // contact key validation start
        $(".dependentContactKeyData").each(function(){
            var keyId       = $(this).attr('keyId');
            var contactKey  = $('#dependentsContactKey_'+keyId).val();
            console.log("suffix :"+contactKey);
            if(contactKey == "") {
                $('body #err_contact_dep_unique_key_'+keyId).html('Contact key is required');
                isError     = 1;
            } else {
                $('body #err_contact_dep_unique_key_'+keyId).html('');
            }
        });

        //suffix data validation
        $(".dependentSuffixData").each(function(){
            var keyId   = $(this).attr('keyId');
            var suffix  = $('#dependent_suffix_values_drops_'+keyId).val();
            

            if(suffix == "" || suffix === undefined) {
                $('body #dependentsSuffixError_'+keyId).html('Select suffix');
                isError     = 1;
                console.log("isError if :"+isError);
            } else {
                $('body #dependentsSuffixError_'+keyId).html('');
                console.log("isError else :"+isError);
            }

        });

        //title data validation
        $(".dependentTitleData").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#dependentstitle_'+keyId+' option:selected').val() == "") {
                $('body #dependentsTitleError_'+keyId).html('Select title');
                isError     = 1;
            } else {
                $('body #dependentsTitleError_'+keyId).html('');
            }
        });

        //first name data validation
        $(".dependentFirstNameData").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#dependentsFirstName_'+keyId+' option:selected').val() == "") {
                $('body #dependentsFirstNameError_'+keyId).html('Select first name');
                isError     = 1;
            } else {
                $('body #dependentsFirstNameError_'+keyId).html('');
            }
        });

        //surname data validation
        $(".dependentSurnameData").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#dependentsSurname_'+keyId+' option:selected').val() == "") {
                $('body #dependentsSurnameError_'+keyId).html('Select surname');
                isError     = 1;
            } else {
                $('body #dependentsSurnameError_'+keyId).html('');
            }
        });

        if(isError == 1) {
            $(".contact-dependents-error-block").show();
            return false;
        } else {
            $(".contact-dependents-error-block").hide();
        }

    });

    //suffix data validation
    $(document).on('change','.dependentSuffixData',function(){        
        var keyId     = $(this).attr('keyId');
        if($('#dependentsSuffix_'+keyId+' option:selected').val() == "") {
            $('body #dependentsSuffixError_'+keyId).html('Select suffix');
            isError     = 1;
        } else {
            $('body #dependentsSuffixError_'+keyId).html('');
        }
    });

    //title data validation
    $(document).on('change','.dependentTitleData',function(){    
        var keyId     = $(this).attr('keyId');
        if($('#dependentstitle_'+keyId+' option:selected').val() == "") {
            $('body #dependentsTitleError_'+keyId).html('Select title');
            isError     = 1;
        } else {
            $('body #dependentsTitleError_'+keyId).html('');
        }
    });

    //first name data validation
    $(document).on('change','.dependentFirstNameData',function(){        
        var keyId     = $(this).attr('keyId');
        if($('#dependentsFirstName_'+keyId+' option:selected').val() == "") {
            $('body #dependentsFirstNameError_'+keyId).html('Select first name');
            isError     = 1;
        } else {
            $('body #dependentsFirstNameError_'+keyId).html('');
        }
    });

    //surname data validation
    $(document).on('change','.dependentSurnameData',function(){        
        var keyId     = $(this).attr('keyId');
        if($('#dependentsSurname_'+keyId+' option:selected').val() == "") {
            $('body #dependentsSurnameError_'+keyId).html('Select surname');
            isError     = 1;
        } else {
            $('body #dependentsSurnameError_'+keyId).html('');
        }
    });


    // digit only for no. of dependent
    $(document).on('keypress','.depNoOfDependents',function(e){    
        var keyId       = $(this).attr('keyId');
        var dependent   = $('#dependentNoOfDependents_'+keyId).val();

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
            $("body #err_dep_no_of_dependents_"+keyId).html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    // $('input[name=dependents_photo]').on('change',function(){
    //     if (this.files && this.files[0]) {
    //         var file = this.files[0];
    //         var fileType = file["type"];
    //         var ValidImageTypes = ["image/jpeg", "image/jpg", "image/png"];
    //         if ($.inArray(fileType, ValidImageTypes) < 0) {
    //             $('.dependentsPhotoError').html('select only jpeg,jpg,png image');
    //             return false;
    //         } else {
    //             $('.dependentsPhotoError').html('');
    //             displayClass = '.dependents_image';
    //             DisplayOnChangeFile(this.files,displayClass);
    //         }
    //     }
    // });
//contact DEPENDENTS js validation - end
});

function dependentFullnameWriter(keyId = "") {

    if(keyId != "") {
        var fullname        = "";
        var titalValue      = $('#dependentstitle_'+keyId+" option:selected").text();
        var surnameValue    = $('#dependentsSurname_'+keyId+" option:selected").text();
        var firstValue      = $('#dependentsFirstName_'+keyId+" option:selected").text();
        var fatherValue     = $('#dependents_father_name_'+keyId+" option:selected").text();
        fullname            = titalValue+' '+firstValue+' '+fatherValue+' '+surnameValue; 
        $('body #dependent_fullname_'+keyId).val(fullname);
    }
}

