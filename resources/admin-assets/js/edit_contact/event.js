$(document).ready(function(){
	
	// form submit
    $("input[name=submit_event]").click(function(){

        var isError         = 0;
        //event title
        if($.trim($('#event_title').val()) == "") {
            $('body #err_event_title').html("Event title is required");
            isError = 1;
        } else {
            $('body #err_event_title').html("");
        }

        //event type
        if($('#event_type option:selected').val() == "") {
            $('body #err_event_type').html('Select event type');
            isError     = 1;
        } else {
            $('body #err_event_type').html('');
        }
        var start_date  = $("#start_date").val();     
        var end_date    = $("#end_date").val();    

        //event start date
        if($.trim(start_date) == "") {
            $('body #err_event_start_date').html("Event start is required");
            isError = 1;
        } else {
            $('body #err_event_start_date').html("");
        }

        //event end date
        if($.trim(end_date) == "") {
            $('body #err_event_end_date').html("Event end is required");
            isError = 1;
        } else {
            $('body #err_event_end_date').html("");
        }

        if( start_date != "" && end_date != "") {
            var res     = CompareDate(start_date,end_date);
            if(!res) {
                $('body #err_event_end_date').html('End date must be a greater than start date or equal');
                isError = 1;
            } else {
                $('body #err_event_end_date').html('');
            }
        }

        if(isError == 1) {
            return false;
        }
    });


    // target budget
    $(document).on('keypress','#target_budget',function(e){    
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
            $("#err_target_budget").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    //event type
    $(document).on('change','#event_type',function(){        
        
        if($('#event_type option:selected').val() == "") {
            $('body #err_event_type').html('Select event type');
            isError     = 1;
        } else {
            $('body #err_event_type').html('');
        }
    });

    //event title 
    $(document).on('keypress','#event_title',function(){    
        if($.trim($(this).val()) == "") {
            $('body #err_event_title').html("Event title is required");
            isError = 1;
        } else {
            $('body #err_event_title').html("");
        }
    });

    //event end date 
    $(document).on('change','#end_date',function(){    
        var start_date  = $("#start_date").val();     
        var end_date    = $("#end_date").val(); 
        
        //event end date
        if($.trim(end_date) == "") {
            $('body #err_event_end_date').html("Event end is required");
            isError = 1;
        } else {
            $('body #err_event_end_date').html("");
        }

        if( start_date != "" && end_date != "") {
            var res     = CompareDate(start_date,end_date);
            if(!res) {
                $('body #err_event_end_date').html('End date must be a greater than start date or equal');
                isError = 1;
            } else {
                $('body #err_event_end_date').html('');
            }
        }

    });

    //compare date function start
    function CompareDate(startDate,endDate) {
        var stDate = new Date(startDate);
        var edDate = new Date(endDate);
        var timeDiff = edDate.getTime() - stDate.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if(diffDays < 0 ){
            return false;
        } else {
            return true;
        }
    }
    //compare date function end
    	
});