$(document).ready(function(){
    // accorion on multiple records
    $("#accordionContactSpouse").on("hidden.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionContactSpouse").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });

     // add more on contact spouse start
    $(document).on('click','.addBlockSpouse',function(event) {
        var data_id = $('.contactSpouseAjaxBlock').last().attr('data-id');
        var spouseFormIndex = $('.contactSpouseAjaxBlock').last().attr('form-index');
        
        $.ajax({
            type: "GET",
            url: base_url+"/contact/multiple-contact-spouse",
            data: { 'address_id' : data_id, 'spouseFormIndex':spouseFormIndex,'_token' : "{{ csrf_token() }}" },
            success:function(data) {
                // 
                $("#spouseAddnewBlock").append(data);
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
                $('.checkTrashSpouse').removeClass('hide');
                $("#addSpouseLoader").addClass('hide');
                $("select option[value='']").attr("disabled", true);

                $(this).attr("disabled", false);
            },
            beforeSend: function(){
                $("#addSpouseLoader").removeClass('hide');
                //$("#addSpouseLoader").addClass('show');
                $(this).attr("disabled", true);
            },
            complete: function(){
                //$("#addSpouseLoader").removeClass('show');
                $("#addSpouseLoader").addClass('hide');
                $(this).attr("disabled", false);
            },
            error: function() {
                // $(".addSpouseLoader").css('display','block');
                //$("#addSpouseLoader").removeClass('show');
                $("#addSpouseLoader").addClass('hide');
                 $(this).attr("disabled", false);
            }
        });
        
    })
    // add more on contact number end

    //remove contact address start
    $(document).on("click", '.trashSpouse', function(event) {
        var trashId = $(this).attr('trashId');
        var answer=confirm("Are you sure you want to delete this record?");
        console.log(trashId);
        if(answer==true) {
            $(".contactSpouseAjaxBlock").each(function(){
                if($(this).attr('data-id') == trashId) {
                    $(this).remove();
                }
            });

            $.ajax({
                type: "GET",
                url: base_url+"/contact-spouse/remove/"+trashId,
                async:false,
                data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        // console.log(response.status);
                        // console.log("success");
                    }
                    
                    //remove all from form if all deleted
                    if( !$.trim( $('#spouseAddnewBlock').html() ).length ) {    
                        $('.checkTrashSpouse').addClass('hide');
                        $("#spouse .tagsinput .tag").remove();
                        $('#spouse .fr-element.fr-view').html("");
                        $('#spouse .userField').removeAttr('placeholder');
                    
                    } else {
                        $('.checkTrashSpouse').removeClass('hide');
                    
                    } 
                },
                beforeSend: function() {

                },
                error: function() {

                }

            });

        } else {
            return false;
        }
    });
    //remove contact address end

    $('body').on('focus',".spouse_dateofbirth", function(){
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,        
            singleClasses: "picker_2"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });

    //spouse form submit validation
    $("input[name=submit_spouse]").click(function(){

        setTimeout(function() {
            $(".alert-danger").hide();
        }, 10000);

        var isError         = 0;
        // contact key validation start
        $(".spouseContactKey").each(function(){
            var keyId       = $(this).attr('keyId');
            var contactKey  = $('#spouse_contact_key_'+keyId).val();
            
            if(contactKey == "") {
                $('body #spouseContactKeyError_'+keyId).html('Contact key is required');
                isError     = 1;
            } else {
                $('body #spouseContactKeyError_'+keyId).html('');
            }
        });
        
        //suffix data validation
        $(".spouseSuffix").each(function(){
            var keyId   = $(this).attr('keyId');
            var suffix  = $('#spouse_suffix_values_drops_'+keyId).val();

            if(suffix == "") {
                $('body #spouseSuffixError_'+keyId).html('Select suffix');
                isError     = 1;
            } else {
                $('body #spouseSuffixError_'+keyId).html('');
            }
        });
        
        //title data validation
        $(".spouseTitle").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#spouseTitle_'+keyId+' option:selected').val() == "") {
                $('body #spouseTitleError_'+keyId).html('Select title');
                isError     = 1;
            } else {
                $('body #spouseTitleError_'+keyId).html('');
            }
        });
        
        //first name data validation
        $(".spouseFirstname").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#spouseFirstName_'+keyId+' option:selected').val() == "") {
                $('body #spouseFirstNameError_'+keyId).html('Select first name');
                isError     = 1;
            } else {
                $('body #spouseFirstNameError_'+keyId).html('');
            }
        });
        
        //surname data validation
        $(".spouseSurname").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#spouseSurname_'+keyId+' option:selected').val() == "") {
                $('body #spouseSurnameError_'+keyId).html('Select surname');
                isError     = 1;
            } else {
                $('body #spouseSurnameError_'+keyId).html('');
            }
        });
        
        if(isError == 1) {
            $(".contact-spouse-error-block").show();
            return false;
        } else {
            $(".contact-spouse-error-block").hide();
        }
    });
    //spouse form submit validation

    //suffix data validation
    $(document).on('change','.spouseSuffix',function(){        
        var keyId     = $(this).attr('keyId');
        
        if($('#spouseSuffix_'+keyId+' option:selected').val() == "") {
            $('body #spouseSuffixError_'+keyId).html('Select suffix');
            isError     = 1;
        } else {
            $('body #spouseSuffixError_'+keyId).html('');
        }
    });

    //title data validation
    $(document).on('change','.spouseTitle',function(){        
        var keyId     = $(this).attr('keyId');
        if($('#spouseTitle_'+keyId+' option:selected').val() == "") {
            $('body #spouseTitleError_'+keyId).html('Select title');
            isError     = 1;
        } else {
            $('body #spouseTitleError_'+keyId).html('');
        }
    });

    //first name data validation
    $(document).on('change','.spouseFirstname',function(){   
        var keyId     = $(this).attr('keyId');
        if($('#spouseFirstName_'+keyId+' option:selected').val() == "") {
            $('body #spouseFirstNameError_'+keyId).html('Select first name');
            isError     = 1;
        } else {
            $('body #spouseFirstNameError_'+keyId).html('');
        }
    });

    //surname data validation
    $(document).on('change','.spouseSurname',function(){   
        var keyId     = $(this).attr('keyId');
        if($('#spouseSurname_'+keyId+' option:selected').val() == "") {
            $('body #spouseSurnameError_'+keyId).html('Select surname');
            isError     = 1;
        } else {
            $('body #spouseSurnameError_'+keyId).html('');
        }
    });

    // digit only for no. of dependent
    $(document).on('keypress','.spouseNoOfdependent',function(e){    
        var keyId       = $(this).attr('keyId');
        var dependent   = $('#SpouseNoOfDependents_'+keyId).val();

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
            $("body #err_spouse_no_of_dep_"+keyId).html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

});

function spouseFullnameWriter(keyId) {

    if(keyId != "") {
        var fullname        = "";
        var titalValue      = $('#spouseTitle_'+keyId+" option:selected").text();
        var surnameValue    = $('#spouseSurname_'+keyId+" option:selected").text();
        var firstValue      = $('#spouseFirstName_'+keyId+" option:selected").text();
        var fatherValue     = $('#spouse_father_name_'+keyId+" option:selected").text();
        fullname            = titalValue+' '+firstValue+' '+fatherValue+' '+surnameValue; 
        $('body #spouse_fullname_'+keyId).val(fullname);
    }
}
