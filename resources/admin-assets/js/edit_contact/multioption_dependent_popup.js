$(document).ready(function(){

	// edit contact multiple option popup js start

    /* suffix start */
	// open modal when focus on contact suffix field start 
	$(document).on('focus','.contact_dependent_suffix_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_dependent_suffix_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_dependent_suffix_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_dependent_suffix_multi_block_'+keyId).removeClass('hide');

        var suffixValue     = $("body #dependent_suffix_values_drops_"+keyId).val();

        $.each(suffixValue.split(","), function(i,e){
            $("body .drops_block #dependentsSuffix_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitDependentDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #dependentsSuffix_"+keyId+" option[value='']").prop("selected", false);
    });
    // open modal when focus on contact suffix field end

    //set dropdown on contact suffix fields start
    $(document).on('click','.submitDependentDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#dependentsSuffix_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#dependentsSuffix_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var suffixtext      = selMulti.join(", ");
        var suffixValue     = selMulValue.join(",");
        $("body #suffix_text_"+keyId).val(suffixtext);
        $("body #dependent_suffix_values_drops_"+keyId).val(suffixValue);
        
        $('body .contact_dependent_suffix_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact suffix fields end
    /* suffix end */


    /* language start */
    // open modal when focus on contact language field start 
	$(document).on('focus','.contact_dependent_language_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_dependent_language_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_dependent_language_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_dependent_language_multi_block_'+keyId).removeClass('hide');

        var langValue     = $("body #dependent_language_values_drops_"+keyId).val();

        $.each(langValue.split(","), function(i,e){
            $("body .drops_block #dependentsLanguages_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitDependentLanguageDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #dependentsLanguages_"+keyId+" option[value='']").prop("selected", false);
    });
    // open modal when focus on contact language field end

    //set dropdown on contact language fields start
    $(document).on('click','.submitDependentLanguageDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#dependentsLanguages_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#dependentsLanguages_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var langtext      = selMulti.join(", ");
        var langValue     = selMulValue.join(",");
        $("body #language_text_"+keyId).val(langtext);
        $("body #dependent_language_values_drops_"+keyId).val(langValue);
        
        $('body .contact_dependent_language_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact language fields end
    /* language end */

    /* administrative group start */
    // open modal when focus on contact language field start 
    $(document).on('focus','.contact_dependent_adminigroup_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_dependent_adminigroup_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_dependent_adminigroup_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_dependent_adminigroup_multi_block_'+keyId).removeClass('hide');

        var langValue     = $("body #dependent_adminigroup_values_drops_"+keyId).val();

        $.each(langValue.split(","), function(i,e){
            $("body .drops_block #dependentsAdminiGroup_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitDependentAdminigpDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #dependentsAdminiGroup_"+keyId+" option[value='']").prop("selected", false);
    });
    // open modal when focus on contact language field end

    //set dropdown on contact language fields start
    $(document).on('click','.submitDependentAdminigpDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#dependentsAdminiGroup_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#dependentsAdminiGroup_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var langtext      = selMulti.join(", ");
        var langValue     = selMulValue.join(",");
        $("body #administrative_group_"+keyId).val(langtext);
        $("body #dependent_adminigroup_values_drops_"+keyId).val(langValue);
        
        $('body .contact_dependent_adminigroup_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact language fields end
    /* administrative group end */

    
    /* profession start */
    // open modal when focus on contact language field start 
    $(document).on('focus','.contact_dependent_profession_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_dependent_profession_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_dependent_profession_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_dependent_profession_multi_block_'+keyId).removeClass('hide');

        var langValue     = $("body #dependent_profession_values_drops_"+keyId).val();

        $.each(langValue.split(","), function(i,e){
            $("body .drops_block #dependentsProfession_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitDependentProfessionDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #dependentsProfession_"+keyId+" option[value='']").prop("selected", false);
    });
    // open modal when focus on contact language field end

    //set dropdown on contact language fields start
    $(document).on('click','.submitDependentProfessionDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#dependentsProfession_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#dependentsProfession_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var proftext      = selMulti.join(", ");
        var profValue     = selMulValue.join(",");
        $("body #profession_text_"+keyId).val(proftext);
        $("body #dependent_profession_values_drops_"+keyId).val(profValue);
        
        $('body .contact_dependent_profession_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact language fields end
    /* profession end */

    /* nationality start */
    // open modal when focus on contact language field start 
    $(document).on('focus','.contact_dependent_nationality_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_dependent_nationality_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_dependent_nationality_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_dependent_nationality_multi_block_'+keyId).removeClass('hide');

        var nationalityValue     = $("body #dependent_nationality_values_drops_"+keyId).val();

        $.each(nationalityValue.split(","), function(i,e){
            $("body .drops_block #dependentsNationality_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitDependentNatinalityDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #dependentsNationality_"+keyId+" option[value='']").prop("selected", false);
    });
    // open modal when focus on contact language field end

    //set dropdown on contact language fields start
    $(document).on('click','.submitDependentNatinalityDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#dependentsNationality_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#dependentsNationality_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var nationalitytext      = selMulti.join(", ");
        var nationalityValue     = selMulValue.join(",");
        $("body #nationality_text_"+keyId).val(nationalitytext);
        $("body #dependent_nationality_values_drops_"+keyId).val(nationalityValue);
        
        $('body .contact_dependent_nationality_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact language fields end
    /* natinality end */

    /* internal organization start */
    // open modal when focus on contact language field start 
    $(document).on('focus','.contact_dependent_internalOrg_section',function() {

        var keyId   = $(this).attr('keyId');

        $('body .drops_block').html($('.contact_dependent_internalOrg_multi_block_'+keyId).html());
        $('body #MyContactLabel').html($('.contact_dependent_internalOrg_multi_block_'+keyId+' label').html());
        
        $('body .drops_block .contact_dependent_internalOrg_multi_block_'+keyId).removeClass('hide');

        var nationalityValue     = $("body #dependent_internalOrg_values_drops_"+keyId).val();

        $.each(nationalityValue.split(","), function(i,e){
            $("body .drops_block #dependentsInternalOrg_"+keyId+" option[value='"+e+"']").prop("selected", true);
        });

        $('body #multiple-modal-btn-section button').removeClass();
        $('body #multiple-modal-btn-section button').addClass('btn btn-primary submitDependentInternalOrgDrops');
        $('body #multiple-modal-btn-section button').attr('keyId',keyId);

        $('#modal_drops').click();
        $("body .drops_block #dependentsInternalOrg_"+keyId+" option[value='']").prop("selected", false);
    });
    // open modal when focus on contact language field end

    //set dropdown on contact language fields start
    $(document).on('click','.submitDependentInternalOrgDrops',function() {
        var keyId   = $(this).attr('keyId');

        var selMulti    = $.map($("#dependentsInternalOrg_"+keyId+" option:selected"), function (el, i) {
            return $(el).text();
        });

        var selMulValue = $.map($("#dependentsInternalOrg_"+keyId+" option:selected"), function (el, i) {
            return $(el).val();
        });

        var nationalitytext      = selMulti.join(", ");
        var nationalityValue     = selMulValue.join(",");
        $("body #internalOrg_text_"+keyId).val(nationalitytext);
        $("body #dependent_internalOrg_values_drops_"+keyId).val(nationalityValue);
        
        $('body .contact_dependent_internalOrg_multi_block_'+keyId).html($('body .drops_block').html());
        
        $('#contact_drop_modal').modal('toggle');
    });
    //set dropdown on contact language fields end
    /* internal organization end */

	// edit contact multiple option popup js end
});