$(document).ready(function(){
    $("#accordionGreetings").on("hidden.bs.collapse", function (e) {
        
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-up")
            .addClass("fa-chevron-right");
    });
    $("#accordionGreetings").on("shown.bs.collapse", function (e) {
        $(e.target).closest(".panel")
            .find(".panel-heading i")
            .removeClass("fa-chevron-right")
            .addClass("fa-chevron-up");
    });
    
    //contact GREETINGS LETTER js validation - start
        $("input[name=submit_greetings_letter]").click(function(){

            setTimeout(function() {
                $(".alert-danger").hide();
            }, 10000);
            
            var isError     = 0;
           
            $(".GreetingTitleData").each(function(){
                var keyId     = $(this).attr('keyId');
                
                if($('#greetingTitle_'+keyId+' option:selected').val() == "") {
                    $('body #GreetingTitleError_'+keyId).html('Select the greeting title');
                    isError     = 1;
                } else {
                    $('body #GreetingTitleError_'+keyId).html('');
                }
            });

            // Greeting intro 
            $(".grettingIntroData").each(function(){
                var keyId           = $(this).attr('keyId');
                var greetingIntro   = $('#grettingIntro_'+keyId).val();
                
                if(greetingIntro == "") {
                    $('body #GreetingIntroError_'+keyId).html('Greeting intro is required');
                    isError     = 1;
                } else {
                    $('body #GreetingIntroError_'+keyId).html('');
                }
            });

            // Greeting ending
            $(".greetingEndingData").each(function(){
                var keyId           = $(this).attr('keyId');
                var greetingEnding  = $('#greetingEnding_'+keyId).val();
                
                if(greetingEnding == "") {
                    $('body #GreetingEndingError_'+keyId).html('Greeting intro is required');
                    isError     = 1;
                } else {
                    $('body #GreetingEndingError_'+keyId).html('');
                }
            });

            if(isError == 1) {
                $(".contact-greeting-error-block").show();
                return false;
            } else {
                $(".contact-greeting-error-block").hide();
            }
        });

        //greeting title
        $(document).on('change','.GreetingTitleData',function(){    
            var keyId     = $(this).attr('keyId');
            
            if($('#greetingTitle_'+keyId+' option:selected').val() == "") {
                $('body #GreetingTitleError_'+keyId).html('Select the greeting title');
                isError     = 1;
            } else {
                $('body #GreetingTitleError_'+keyId).html('');
            }
        });

        // Greeting intro 
        $(document).on('keypress','.grettingIntroData',function(){    
            var keyId           = $(this).attr('keyId');
            var greetingIntro   = $('#grettingIntro_'+keyId).val();
            
            if(greetingIntro == "") {
                $('body #GreetingIntroError_'+keyId).html('Greeting intro is required');
                isError     = 1;
            } else {
                $('body #GreetingIntroError_'+keyId).html('');
            }
        });

        // Greeting ending
        $(document).on('keypress','.greetingEndingData',function(){     
            var keyId           = $(this).attr('keyId');
            var greetingEnding  = $('#greetingEnding_'+keyId).val();
            
            if(greetingEnding == "") {
                $('body #GreetingEndingError_'+keyId).html('Greeting intro is required');
                isError     = 1;
            } else {
                $('body #GreetingEndingError_'+keyId).html('');
            }
        });  
    //contact GREETINGS LETTER js validation - end

    //Add greeting fields - start
        $(document).on("click", '.add_greetings', function(event) {
            var keyId       = $('.greetings_multi_fields').last().attr('data-id');
            var dataId      = $('.greetings_multi_fields').last().attr('data');
            var formIndex   = $('.greetings_multi_fields').last().attr('form-index');
            $("#greetingLoader").removeClass('hide');
            $(this).attr("disabled", true);
            $.ajax({
                type: "GET",
                url: base_url+"/multiple/greetings",
                data: { 'keyId' : keyId,'dataId' : dataId, 'formIndex':formIndex,'_token' : "{{ csrf_token() }}" },
                success:function(data) {
                    $('.checkTrashGreeting').removeClass('hide');
                    $("#greetings_multiple").append(data);
                    $("#greetingLoader").addClass('hide');
                    $(this).attr("disabled", false);

                    //when remove all content is deleted from block
                    if( !$.trim( $('#greetings_multiple').html() ).length ) {    
                        $('.checkTrashGreeting').addClass('hide');
                        console.log('yes empty');
                    } else {
                        $('.checkTrashGreeting').removeClass('hide');
                        console.log('not empty');
                    }                 
                },
                beforeSend: function() {
                    $("#greetingLoader").removeClass('hide');
                },
                error: function() {
                    $("#greetingLoader").addClass('hide');
                    $(this).attr("disabled", false);
                }

            });
            $(this).attr("disabled", false);
            // blank value disabled in dropdown
            $("select option[value='']").attr("disabled", true);
        });
    //Add greeting fields - end

    //remove greeting - start
        $(document).on("click", '.trashGreetings', function(event) {
            var trashId = $(this).attr('trashId');
            var answer=confirm("Are you sure you want to delete this record?");
            if(answer==true) {
                $(".greetings_multi_fields").each(function(){
                    if($(this).attr('data-id') == trashId) {
                        $(this).remove();
                    }
                });

                $.ajax({
                    type: "GET",
                    url: base_url+"/greetings/remove/"+trashId,
                    async:false,
                    data: { 'trashId' : trashId, '_token' : "{{ csrf_token() }}" },
                    success:function(data) {
                        response = jQuery.parseJSON(data);
                        if(response.status) {
                            // console.log(response.status);
                            // console.log("success");
                        } else {
                            // console.log(response.status);
                            // console.log("fail");
                        }   

                        if( !$.trim( $('#greetings_multiple').html() ).length ) {    
                            $('.checkTrashGreeting').addClass('hide');
                            $("#greetingsLetter .tagsinput .tag").remove();
                            $('#greetingsLetter .fr-element.fr-view').html("");
                            $('#greetingsLetter .userField').removeAttr('placeholder');
                        } else {
                            $('.checkTrashGreeting').removeClass('hide');
                        }                 
                    },
                    beforeSend: function() {
                        
                    },
                    error: function() {
                        
                    }

                });

            } else {
                
                return false;
            }
        });
    //remove greeting - end
});


function getTitleValue(index){
    var titleVal = $("#greetingTitle_"+index+" option:selected").val();
    if(titleVal == ""){
        $('.GreetingTitleError_'+index).html("Select the title");
    } else {
        $('.GreetingTitleError_'+index).html("");
    }
}

function getGreetingIntroValue(index){
    var grettingIntroVal = $("#grettingIntro_"+index).val();
    if(grettingIntroVal == ""){
        $('.GreetingIntroError_'+index).html("Greeting intro is required");
    } else {
        $('.GreetingIntroError_'+index).html("");
    }
}

function getGreetingEndingValue(index){
    var grettingEndingVal = $("#greetingEnding_"+index).val();
    if(grettingEndingVal == ""){
        $('.GreetingEndingError_'+index).html("Greeting ending is required");
    } else {
        $('.GreetingEndingError_'+index).html("");
    }
}
