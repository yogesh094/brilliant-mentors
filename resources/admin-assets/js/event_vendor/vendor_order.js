$(document).ready(function() {

	var orderId 	= $("#order_id").val();
	addNewProductRow(orderId);
	loadTotalAmount();

	$(document).on('click','#add_new_product',function(e){ 
		addNewProductRow();
	});
	// console.log("class " +$(".productItem").length);

	//product quantity validation
	$(document).on('keypress','.productQuantity',function(e){ 
		var keyId       = $(this).attr('keyId');
        var quantity  	= $('#quantity_'+keyId).val();

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
	});

	//product tax validation
	$(document).on('keypress','.productTax',function(e){ 
		var keyId       = $(this).attr('keyId');
        var tax  		= $('#tax_'+keyId).val();

        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
	    	return false;
	  	}
	});

	//product price validation
	$(document).on('keypress','.productPrice',function(e){ 
		var keyId       = $(this).attr('keyId');
        var price  		= $('#price_'+keyId).val();

        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
	    	return false;
	  	}
	});

	//order-discount validation
	$(document).on('keypress','#order-discount',function(e){ 
		
		if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
	    	return false;
	  	}
	});

	$(document).on('keyup','.productQuantity',function(e){ 
		var keyId       = $(this).attr('keyId');
		var quantity  	= 0;
		var price 		= 0;
		var tax 		= 0;
		var total 		= 0;
		quantity  		= parseInt($('#quantity_'+keyId).val());
		quantity  		= (isNaN(quantity)) ? 1 : quantity;
		
		if(quantity > 0) {
			price 		= parseFloat($('#price_'+keyId).val());
			price 		= (isNaN(price)) ? 0 : price;
			tax 		= parseFloat($('#tax_'+keyId).val());
			tax 		= (isNaN(tax)) ? 0 : tax;

			total 			= quantity * price;
			tax 			= price * tax / 100;
			total 			= (total + tax).toFixed(2);
			
		}
		$('#rowTotal_'+keyId).html("<b>"+total+"</b>");
		$('#totalhidden_'+keyId).val(total);
		loadSubTotal();
	});

	$(document).on('keyup','.productTax',function(e){ 
		var keyId       = $(this).attr('keyId');
		var quantity  	= 0;
		var price 		= 0;
		var tax 		= 0;
		var total 		= 0;
		tax  			= parseFloat($('#tax_'+keyId).val());
		tax 			= (isNaN(tax)) ? 0 : tax;

		price 		= parseFloat($('#price_'+keyId).val());
		price 		= (isNaN(price)) ? 0 : price;
		quantity 	= parseInt($('#quantity_'+keyId).val());
		quantity 	= (isNaN(quantity)) ? 1 : quantity;
		//set default quantity
		$('#quantity_'+keyId).val(quantity)
		
		total 			= quantity * price;
		tax 			= price * tax / 100;
		total 			= (total + tax).toFixed(2);
		
		$('#rowTotal_'+keyId).html("<b>"+total+"</b>");
		$('#totalhidden_'+keyId).val(total);
		loadSubTotal();
	});

	$(document).on('keyup','.productPrice',function(e){ 
		var keyId       = $(this).attr('keyId');
		var quantity  	= 0;
		var price 		= 0;
		var tax 		= 0;
		var total 		= 0;
		price  			= parseFloat($('#price_'+keyId).val());
		price 			= (isNaN(price)) ? 1 : price;
			
		quantity 		= parseInt($('#quantity_'+keyId).val());
		quantity 		= (isNaN(quantity)) ? 1 : quantity;
		//set default quantity
		$('#quantity_'+keyId).val(quantity)
		tax 			= parseFloat($('#tax_'+keyId).val());
		tax 			= (isNaN(tax)) ? 0 : tax;

		total 			= quantity * price;
		tax 			= price * tax / 100;
		total 			= (total + tax).toFixed(2);

		$('#rowTotal_'+keyId).html("<b>"+total+"</b>");
		$('#totalhidden_'+keyId).val(total);
		loadSubTotal();
	});

	//when product item change
	$(document).on('change','.productItem',function(e){ 
		var keyId 		= $(this).attr('keyId');
		// console.log("productItem :"+keyId);
		var productId 	= 0;
		productId 		= $(this).val();

		if(productId > 0) {

			// $("#add_new_product").attr("disabled", true);
			$.ajax({
		        type: "GET",
		        url: base_url+"/order/get-product-detail/"+productId,
		        data: {},
		        success:function(data) {
					var obj = $.parseJSON(data);
		        	$("#price_"+keyId).val(obj.actual_price);
		        	$("#actual_price_"+keyId).val(obj.actual_price);

		        	price  			= parseFloat($('#price_'+keyId).val());
					price 			= (isNaN(price)) ? 1 : price;
					quantity 		= parseFloat($('#quantity_'+keyId).val());
					quantity 		= (isNaN(quantity)) ? 1 : quantity;
					//set default quantity
					$('#quantity_'+keyId).val(quantity)
					tax 			= parseInt($('#tax_'+keyId).val());
					tax 			= (isNaN(tax)) ? 0 : tax;

					total 			= quantity * price;
					tax 			= price * tax / 100;
					total 			= (total + tax).toFixed(2);			

					$('#rowTotal_'+keyId).html("<b>"+total+"</b>");
					$('body #totalhidden_'+keyId).val(total);
					loadSubTotal();

		        },
		        beforeSend: function(){
		            // $("#addNewProductLoader").removeClass('hide');
		        },
		        complete: function(){
		            // $("#addNewProductLoader").addClass('hide');
		        },
		        error: function() {
		            // $("#addNewProductLoader").addClass('hide');
		            // $("#add_new_product").attr("disabled", false);
		        }
		    });
		} else {
			$('#price_'+keyId).val(0);
			$('#tax_'+keyId).val(0);
			$('#quantity_'+keyId).val(0);
			var total 	= 0;
			$('#rowTotal_'+keyId).html("<b>"+total+"</b>");
			$('body #totalhidden_'+keyId).val(total);
			loadSubTotal();
		}
	});

	// when product category change and product list generate
	$(document).on('change','.productCategory',function(e){ 
		
		var categoryId 		= ($(this).val() == "")?0:$(this).val();
		var keyId 			= $(this).attr("keyId");

		// $("#add_new_product").attr("disabled", true);
		$.ajax({
	        type: "GET",
	        url: base_url+"/order/category/get-products/"+categoryId,
	        data: {'keyId' : keyId},
	        success:function(data) {
				// var obj = $.parseJSON(data);
				// console.log(data);
				$("table .product_row_"+keyId).html(data);
	        },
	        beforeSend: function(){
	            $("#product_id_"+keyId).addClass('hide');
	            $("#productLoader_"+keyId).removeClass('hide');
	        },
	        complete: function(){
	            $("#product_id_"+keyId).removeClass('hide');
	            $("#productLoader_"+keyId).addClass('hide');
	        },
	        error: function() {
	            $("#product_id_"+keyId).removeClass('hide');
	            $("#productLoader_"+keyId).addClass('hide');
	            // $("#add_new_product").attr("disabled", false);
	        }
	    });

		if(categoryId <= 0) {
		    $('#actual_price_'+keyId).val("");
		    $('#price_'+keyId).val("");
			$('#tax_'+keyId).val("");
			$('#quantity_'+keyId).val("");
			var total 	= 0;
			$('#rowTotal_'+keyId).html("<b>"+total+"</b>");
			$('body #totalhidden_'+keyId).val(total);
			loadSubTotal();
		}
	});

	//when vendor category change than this will appear
	$(document).on('change','.vendorCategory',function(e){ 
		categoryId 		= ($(this).val() == "")?0:$(this).val();
		// console.log(categoryId);
		$("body .categorized-vendor").html();
		
		// categorized-vendor
		$.ajax({
	        type: "GET",
	        url: base_url+"/vendors/get-categorized-vendor/"+categoryId,
	        data: {},
	        success:function(data) {
				$("body .categorized-vendor").html(data);
	        },
	        beforeSend: function(){
	            // $("#product_id_"+keyId).addClass('hide');
	            // $("body .categorized-vendor").addClass('hide');
	            $("#categorizedVendorLoader").removeClass('hide');
	        },
	        complete: function(){
	            // $("#product_id_"+keyId).removeClass('hide');
	            // $("body .categorized-vendor").removeClass('hide');
	            $("#categorizedVendorLoader").addClass('hide');
	        },
	        error: function() {
	            // $("#product_id_"+keyId).removeClass('hide');
	            // $("body .categorized-vendor").removeClass('hide');
	            $("#categorizedVendorLoader").addClass('hide');
	            // $("#add_new_product").attr("disabled", false);
	        }
	    });
	});

	//apply discount on subtotal
	$(document).on('keyup','#order-discount',function(e){

		var discount 	= 0;
		discount 		= parseFloat($(this).val());
		discount 		= isNaN(discount)?0:discount;
		if (discount > 100) {
			e.preventDefault();     
           	$(this).val(100);
		}
		var discount  			= parseFloat($(this).val());
		discount 				= (isNaN(discount))?0:discount;
		var totalOrderAmount 	= parseFloat($('#subTotalProductText').val());
		totalOrderAmount 		= (isNaN(totalOrderAmount))?0:totalOrderAmount;
		var discountedAmount 	= 0;
		var grandTotal 			= 0;

		discountedAmount 		= parseFloat(totalOrderAmount * discount / 100).toFixed(2);
		
		$('#discounted-amount').html(discountedAmount);

		grandTotal 				= parseFloat(totalOrderAmount - discountedAmount).toFixed(2);
		grandTotal 				= (isNaN(grandTotal))?0:grandTotal;
		$("#grand_total").html(grandTotal);
		$('#totalProductText').val(grandTotal);
	});

	//remove product
	$(document).on('click','.removeProduct',function(e){

		var product_id 	= 0;
		product_id 		= parseInt($(this).attr('product_id'));
		var keyId 		= $(this).attr('keyId');
		// console.log("product_id :"+product_id);
		if (confirm('Are you sure you want to delete this product ?')) {
			if(product_id > 0) {

				// $("#add_new_product").attr("disabled", true);
				$.ajax({
			        type: "GET",
			        url: base_url+"/order/product/delete/"+product_id,
			        data: {},
			        success:function(data) {
						// console.log(data);
			        	var obj = $.parseJSON(data);
			        	// console.log("status del : "+obj.status);

			        	if(obj.status) {
			        		$('.rowid_'+keyId).remove();
			        	} 
			        	loadSubTotal();

			        },
			        beforeSend: function(){
			            // $("#addNewProductLoader").removeClass('hide');
			        },
			        complete: function(){
			            // $("#addNewProductLoader").addClass('hide');
			        },
			        error: function() {
			            // $("#addNewProductLoader").addClass('hide');
			            // $("#add_new_product").attr("disabled", false);
			        }
			    });
			} else {
				$('.rowid_'+keyId).remove();
			}
		}
	});

	//contact address
    $("input[name=submit_order]").click(function(){
        // return false;
        var isError = 0;
        var trCount = $('#new_product_item tr').length

        //vendor category
        if($('body #vendor_category_id option:selected').val() == "") {
            $('body #err_vendor_category_id').html('Select vendor category');
            isError     = 1;
        } else {
            $('body #err_vendor_category_id').html('');
        }

        //vendor
        if($('body #vendor_id option:selected').val() == "") {
            $('body #err_vendor_id').html('Select vendor');
            isError     = 1;
        } else {
            $('body #err_vendor_id').html('');
        }

        //currency_type
        if($('body #currency_type option:selected').val() == "") {
            $('body #err_currency_type').html('Select currency');
            isError     = 1;
        } else {
            $('body #err_currency_type').html('');
        }

        //order number
        // if($.trim($('#order_number').val()) == "") {
        //     $('body #err_order_number').html("Order number is required");
        //     isError = 1;
        // } else {
        //     $('body #err_order_number').html("");
        // }

        //order date
        // if($.trim($('#order_date').val()) == "") {
        //     $('body #err_order_date').html("Order date is required");
        //     isError = 1;
        // } else {
        //     $('body #err_order_date').html("");
        // }

        var productCount 	= 0;
        $(".productItem").each(function(){
            var keyId     = $(this).attr('keyId');
            if($('#product_id_'+keyId+' option:selected').val() != "") {
                productCount++; 	
            } 
        });
        // console.log("class total " +$(".productItem").length);

        if(productCount == 0) {
        	isError = 1;
        	$('body #product_item_err').html("Please select any one product");
        }

        if(isError == 1) {
            $(".event-order-error-block").removeClass('hide');
            return false;
        } else {
            $(".event-order-error-block").addClass('hide');
        }
    });

    //country code
    $(document).on('change','.productItem',function(){  

    	var productCount 	= $('body .productItem option:selected').size();
    	// console.log(" count :"+productCount);
        if(productCount <= 0) {
        	$('body #product_item_err').html("Please select any one product");
        } else {
        	$('body #product_item_err').html("");
        }
    });


    $(document).on('change','#vendor_category_id',function(){  
    	if($('body #vendor_category_id option:selected').val() == "") {
	        $('body #err_vendor_category_id').html('Select vendor category');
	        isError     = 1;
	    } else {
	        $('body #err_vendor_category_id').html('');
	    }
    });
    

    //vendor
    $(document).on('change','#vendor_id',function(){  
	    if($('body #vendor_id option:selected').val() == "") {
	        $('body #err_vendor_id').html('Select vendor');
	        isError     = 1;
	    } else {
	        $('body #err_vendor_id').html('');
	    }
    });

    //currency_type
    $(document).on('change','#currency_type',function(){  
	    if($('body #currency_type option:selected').val() == "") {
	        $('body #err_currency_type').html('Select currency');
	        isError     = 1;
	    } else {
	        $('body #err_currency_type').html('');
	    }
	});
	

});

function addNewProductRow(isEdit = 0) {
	var trCount 	= $('#new_product_item tr').length
	$("#add_new_product").attr("disabled", true);
	var orderId 	= 0;
	var eventId 	= $('#event_id').val();
	if(isEdit >= 1) {
		orderId 	= $("#order_id").val();
	}
	$.ajax({
        type: "GET",
        url : base_url+"/event/"+eventId+"/order/add-new-product-row",
        data: {'trCount' : trCount,'orderId' : orderId},
        success:function(data) {
        	$('#new_product_item tr:last').after(data);
        	$("#add_new_product").attr("disabled", false);
        	loadSubTotal();
        },
        beforeSend: function(){
            $("#addNewProductLoader").removeClass('hide');
        },
        complete: function(){
            $("#addNewProductLoader").addClass('hide');
        },
        error: function() {
            $("#addNewProductLoader").addClass('hide');
            $("#add_new_product").attr("disabled", false);
        }
    });
}

function loadSubTotal() {

	var subTotal 	= 0;
	var grandTotal 	= $('#totalProductText').val();
	$(".productSubtotalTotal").each(function(){
        var keyId     = $(this).attr('keyId');
        var total     = 0;
        var total     = parseFloat($('body #totalhidden_'+keyId).val());
        
        var total     = (isNaN(total)) ? 0 : total;
		subTotal 	+= total;
    });
    $('#subTotalProduct').html((subTotal).toFixed(2));
    $('#subTotalProductText').val((subTotal).toFixed(2));
    loadTotalAmount();
}

function loadTotalAmount() {
	
	var discount  			= parseFloat($("#order-discount").val());
	discount 				= (isNaN(discount))?0:discount;
	var totalOrderAmount 	= parseFloat($('#subTotalProductText').val());
	totalOrderAmount 		= (isNaN(totalOrderAmount))?0:totalOrderAmount;

	var discountedAmount 	= 0;
	var grandTotal 			= 0;

	// console.log("totalOrderAmount :"+totalOrderAmount + " -:-"+discount);
	discountedAmount 		= parseFloat(totalOrderAmount * discount / 100).toFixed(2);

	// console.log("discountedAmount :"+discountedAmount);
	$('#discounted-amount').html(discountedAmount);

	grandTotal 				= parseFloat(totalOrderAmount - discountedAmount).toFixed(2);
	// console.log("grandTotal :"+grandTotal);
	$('#totalProductText').val(grandTotal);
	// var grandTotal 	= $('#totalProductText').val();
	$("#grand_total").html(grandTotal);
}

