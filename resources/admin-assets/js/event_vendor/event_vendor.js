$(document).ready(function() {

	//contact address
    $("input[name=submit_event_vendor]").click(function(){
        // return false;
        var isError = 0;

        // company name
        var companyName     = $('#company_name').val().trim();    
        if (!companyName.trim()) {    
            $('body #err_company_name').html("Company name is required");
            isError     = 1;
        } else {
            $('body #err_company_name').html('');
        }

        // phone_number
        var phoneNumber     = $('#phone_number').val().trim();    
        if (!phoneNumber.trim()) {    
            $('body #err_phone_number').html("Phone number is required");
            isError     = 1;
        } else {
            $('body #err_phone_number').html('');

            // var phoneNumberLength   = $("#phone_number").val().trim().length;
            // console.log(phoneNumberLength);
            // if(phoneNumberLength > 10 || phoneNumberLength < 10) {
            //     $('body #err_phone_number').html("Phone number is 10 digit only");
            //     isError     = 1;
            //     console.log("if ");
            // } else {
            //     $('body #err_phone_number').html('');
            //     console.log("else ");
            // }
        }

        //country code
        if($('#country_code option:selected').val() == "") {
            $('#err_country_code').html('Country Code is required');
            isError     = 1;
        } else {
            $('#err_country_code').html('');
        }

        // business website validation
        var webUrl      = $('#web_url').val();  
        if (webUrl.trim()) {   
            if(CheckWebsiteVlidation($('#web_url').val().trim())) {
                $("#err_web_url").html('Enter valid web url');
                isError     = 1;
            } else {
                $("#err_web_url").html('');
            } 
        }
        
        if(isError == 1) {
            $(".event-vendor-error-block").removeClass('hide');
            return false;
        } else {
            $(".event-vendor-error-block").addClass('hide');
        }
    });

    // contact number 
    $(document).on('keypress','#phone_number',function(e){    
        var phoneNumber  = $(this).val();
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#err_phone_number").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    $("#web_url").keyup(function(){
        CheckWebsiteVlidation($(this).val().trim());
    });
    
});

function CheckWebsiteVlidation(websiteValue){
    var regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if(websiteValue != '' && websiteValue != null){
        if (!regexp.test(websiteValue)) {
            $("#err_web_url").html('Enter valid web url');
            return true;
        } else {
            $("#err_web_url").html('');
            return false;
        }
    } else {
        $("#err_web_url").html('');
        return false;
    }
}