$(document).ready(function(){
    setTimeout(function() {
        $(".alert-success").hide()
        // $(".alert-danger").hide()
    }, 10000);
    var regx = /^\d*(\.\d{0,500})?$/;///^\d*(\.\d{0,500})*(\.\d{0,500})?$/

    $("select option[value='']").attr("disabled", true);

    $('input[name=submit_event_product]').click(function(){
        var ventodId        = $('#eventVendorId option:selected').val();
        var categoryId      = $('#categoryId option:selected').val();
        var productTitle    = $('#productTitle').val();
        var originalPrice   = $('#originalPrice').val();
        var actualPrice     = $('#actualPrice').val();
        // var discount        = $('#discount').val();
        // var description     = $('#description').val();
        var isError = 0;
        
        //vendor category
        if($('#vendor_category_id option:selected').val() == "") {
            $('body #err_vendor_category_id').html('Select vendor category');
            isError     = 1;
        } else {
            $('body #err_vendor_category_id').html('');
        }

        if(ventodId == ""){
            $('#err_vendor_id').html("Please select the vendor.");
            isError = 1;
        }else {
            $('#err_vendor_id').html("");
        }

        if(categoryId == ""){
            $('#err_category_id').html("Please select the category.");
            isError = 1;
        }else {
            $('#err_category_id').html("");
        }

        if(productTitle == ""){
            $('#err_product_title').html("Product title is required.");
            isError = 1;
        }else {
            $('#err_product_title').html("");
        }

        if(originalPrice == ""){
            $('#err_original_price').html("Original price is required.");
            isError = 1;
        }else {
            if(!regx.test(originalPrice)){
                $('#err_original_price').html("Original price should be numeric.");
                isError = 1;
            } else {
                $('#err_original_price').html("");
            }
        }

        if(actualPrice == ""){
            $('#err_actual_price').html("Actual price is required.");
            isError = 1;
        }else {
            if(!regx.test(actualPrice)){
                $('#err_actual_price').html("Actual price should be numeric.");
                isError = 1;
            } else {
                $('#err_actual_price').html("");
            }
        }

        // if(discount == ""){
        //     $('#err_discount').html("Discount is required.");
        //     isError = 1;
        // }else {
        //     if(!regx.test(discount)){
        //         $('#err_discount').html("Discount should be numeric.");
        //         isError = 1;
        //     } else {
        //         $('#err_discount').html("");
        //     }
        // }

        if(isError == 1){
            $('.product-error-block').show();
            return false;
        }else {
            $('.product-error-block').hide();
        }
    });

    // when product category change and product list generate
    $(document).on('change','.vendorProductCategory',function(e){ 
        categoryId      = ($(this).val() == "")?0:$(this).val();
        $("body .categorized-product-vendor").html();
        
        // categorized-vendor
        $.ajax({
            type: "GET",
            url: base_url+"/product/get-categorized-vendor/"+categoryId,
            data: {},
            success:function(data) {
                $("body .categorized-product-vendor").html(data);
            },
            beforeSend: function(){
                // $("#product_id_"+keyId).addClass('hide');
                // $("body .categorized-vendor").addClass('hide');
                $("#categorizedVendorProductLoader").removeClass('hide');
            },
            complete: function(){
                // $("#product_id_"+keyId).removeClass('hide');
                // $("body .categorized-vendor").removeClass('hide');
                $("#categorizedVendorProductLoader").addClass('hide');
            },
            error: function() {
                // $("#product_id_"+keyId).removeClass('hide');
                // $("body .categorized-vendor").removeClass('hide');
                $("#categorizedVendorProductLoader").addClass('hide');
                // $("#add_new_product").attr("disabled", false);
            }
        });
    });

    // vendor category validation
    $(document).on('change','#vendor_category_id',function(){ 
        if($('#vendor_category_id option:selected').val() == "") {
            $('body #err_vendor_category_id').html('Select vendor category');
            isError     = 1;
        } else {
            $('body #err_vendor_category_id').html('');
        }
    });

    //vendor on change validation
    $(document).on('change','#eventVendorId',function(){    
        var ventodId = $('#eventVendorId option:selected').val();
        if(ventodId == "") {
            $('#err_vendor_id').html("Please select the vendor.");          
        } else {
            $('#err_vendor_id').html("");
        }
    });

    //category on change validation
    $(document).on('change','#categoryId',function(){    
        var categoryId = $('#categoryId option:selected').val();
        if(categoryId == "") {
            $('#err_category_id').html("Please select the category.");          
        } else {
            $('#err_category_id').html("");
        }
    });

    //product title on keyup validation
    $(document).on('keyup','#productTitle',function(){    
        var productTitle = $('#productTitle').val().trim();
        if(productTitle == "") {
            $('#productTitle').val("");
            $('#err_product_title').html("Product title is required.");
        } else {
            $('#err_product_title').html("");
        }
    });

    //original price on keyup validation
    $(document).on('keyup','#originalPrice',function(){    
        var originalPrice   = parseFloat($('#originalPrice').val().trim());
        originalPrice       = (isNaN(originalPrice)) ? 1 : originalPrice;
        var discount        = parseFloat($('#discount').val().trim());
        discount            = (isNaN(discount)) ? 0 : discount;
        var actualPrice     = discountedAmount  = 0;
        
        // var regx = /^\d*(\.\d{0,500})?$/;///^\d*(\.\d{0,500})*(\.\d{0,500})?$/
        if(originalPrice == "") {
            $('#originalPrice').val("");
            $('#err_original_price').html("Original price is required.");
        } else {
            discountedAmount= (originalPrice * discount / 100).toFixed(2);
            actualPrice     = (originalPrice - discountedAmount).toFixed(2);
            $("#actualPrice").val(actualPrice);
            $('#err_original_price').html("");
        }

        //Actual price on keyup validation
        var actualPrice = $('#actualPrice').val().trim();
        if(actualPrice == "") {
            $('#actualPrice').val("");
            $('#err_actual_price').html("Actual price is required.");
        } else {
            if(!regx.test(actualPrice)){
                $('#err_actual_price').html("Actual price should be numeric.");
                return false;
            }
            $('#err_actual_price').html("");
        }
    });

    //Actual price on keyup validation
    $(document).on('keyup','#actualPrice',function(){    
        var actualPrice = $('#actualPrice').val().trim();
        if(actualPrice == "") {
            $('#actualPrice').val("");
            $('#err_actual_price').html("Actual price is required.");
        } else {
            if(!regx.test(actualPrice)){
                $('#err_actual_price').html("Actual price should be numeric.");
                return false;
            }
            $('#err_actual_price').html("");
        }
    });

    //Discount on keyup validation
    $(document).on('keyup','#discount',function(e){    
        var discount        = parseFloat($('#discount').val().trim());
        discount            = (isNaN(discount)) ? 0 : discount;
        var originalPrice   = parseFloat($('#originalPrice').val().trim());
        originalPrice       = (isNaN(originalPrice)) ? 1 : originalPrice;
        var actualPrice     = discountedAmount  = 0;

        if (discount > 100) {
            e.preventDefault(); 
            $("#actualPrice").val('0.00');    
            $(this).val(100);
            return false;
        }

        if(discount == "") {
            $('#discount').val("");
            // $('#err_discount').html("Discount is required.");
        } else {
            discountedAmount= (originalPrice * discount / 100).toFixed(2);
            actualPrice     = (originalPrice - discountedAmount).toFixed(2);
            $("#actualPrice").val(actualPrice);
            
            $('#err_discount').html("");
        }
    });

    //Description on keyup validation
    $(document).on('keyup','#description',function(){    
        var description = $('#description').val().trim();
        if(description == "") {
            $('#description').val("");
            $('#err_description').html("Description is required.");
        } else {
            $('#err_description').html("");
        }
    });

    // add only digit
    $(document).on('keypress','#discount',function(e){ 
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            $("#err_discount").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    // add only digit
    $(document).on('keypress','#actualPrice',function(e){ 
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            $("#err_actual_price").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    // add only digit
    $(document).on('keypress','#originalPrice',function(e){ 
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            $("#err_original_price").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
});