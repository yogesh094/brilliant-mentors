@extends('web.master_layout.master')

@section('title')
    {{ \App\Models\CMS::where('status',1)->where('slug','thank-you')->where('language_id',Session()->get('selectedLang'))->first()->title }}
@endsection
@push('styles')
<style type="text/css">
  ul.dsply_grd.brdr_1px {
    list-style-type: none;
}
</style>
@endpush('styles')
@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>{{ \App\Models\CMS::where('status',1)->where('slug','thank-you')->where('language_id',Session()->get('selectedLang'))->first()->title }}</h1>
</div>
<div class="h-backimg text-center m-0">
  <div class="container">
    <div class="row">
      <div class="col-12"> </div>
    </div>
  </div>
</div>
<div class="container mb-4">
  <div class="row">
    <div class="col-12">
      <h2 class="title-h2">@if(!empty(Session::get('Booking Detail'))) {{ Session::get('Booking Detail') }} @else Booking Detail @endif</h2>
    </div>
    <div class="col-md-4 order-md-2 mb-4">
      <h4 class="h4"><strong>@if(!empty(Session::get('Description'))) {{ Session::get('Description') }} @else Description @endif</strong></h4>
      
      <p>{{ $data['description'] }}</p>
    </div>
    <div class="col-md-8 order-md-1">
      <ul class="dsply_grd brdr_1px">
              <li>
                <p class="pull-left">@if(!empty(Session::get('Date'))) {{ Session::get('Date') }} @else Date @endif</p>
                <p class="pull-right">{{ Carbon\Carbon::parse($data['booking_date'])->format('d/m/Y') }}</p>
              </li>
              <li>
                <p class="pull-left">@if(!empty(Session::get('Time'))) {{ Session::get('Time') }} @else Time @endif</p>
                <p class="pull-right">{{ Carbon\Carbon::parse($data['start_time'])->format('g:i A') }} to {{ Carbon\Carbon::parse($data['end_time'])->format('g:i A') }}</p>
              </li>
              <li>
                <p class="pull-left">@if(!empty(Session::get('Category'))) {{ Session::get('Category') }} @else Category @endif</p>
                <p class="pull-right">{{ $data['category'] }}</p>
              </li>
              <li>
                <p class="pull-left">@if(!empty(Session::get('Charges'))) {{ Session::get('Charges') }} @else Charges @endif</p>
                <p class="pull-right">{{ $data['currency'] }}{{ $data['charges'] }} </p>
              </li>
             
            </ul>

            <div style="text-align: center;">
              <a class="btn btn-success" href="{{ url('appointmentList') }}"> @if(!empty(Session::get('Your Consultation'))) {{ Session::get('Your Consultation') }} @else Your Consultation @endif </a>
            </div>
      
    </div>
  </div>
</div>
@endsection