<div class="col col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group label-floating">
									<label class="control-label">Title or Place</label>
									 <?php $position = isset($educationData['position'])?$educationData['position']:''; ?>
									<input class="form-control" name="Education[<?= $count ?>][position]" placeholder="" type="text" value=<?= $position?>>
								</div>
							</div>					
							<div class="col col-lg-3 col-md-3 col-sm-12 col-12">
			                <div class="form-group label-floating is-select">
			                  <label class="control-label">Select Started Month</label>
			                  <select class="form-control" name="Education[<?= $count ?>][start_month]">
			                  <?php $start_month = isset($educationData['start_month'])?$educationData['start_month']:0; 
			                  ?>
			                  @foreach($month as $key => $value) 
			                  		@if($start_month == $value)
			                  			<option value= <?= $key ?> Selected > <?= $value ?></option>
			                  		@else
			                  			<option value= <?= $key ?> > <?= $value ?></option>
			                  		@endif
			                  @endforeach
			                  </select>
			                </div>
			                </div>
			                <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
			                <div class="form-group label-floating is-select">
			                  <label class="control-label">Select Started Year</label>
			                  <select class="form-control" name="Education[<?= $count ?>][start_year]">
			                  <?php $start_year = isset($educationData['start_year'])?$educationData['start_year']:0; ?>
			                  <?php 
			                  	for ($i=1990; $i <= date("Y") ; $i++){
			                  	?>
			                  		@if($start_year == $i)
			                  			<option value= <?= $i ?> Selected > <?= $i ?></option>
			                  		@else
			                  			<option value= <?= $i ?> > <?= $i ?></option>
			                  		@endif
			                  	<?php
			                  }
			                  ?>
			                  </select>
			                </div>
			                </div>
			                <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
			                <div class="form-group label-floating is-select">
			                  <label class="control-label">Select Ended Month</label>
			                  <select class="form-control" name="Education[<?= $count ?>][end_month]">
			                  <?php $end_month = isset($educationData['end_month'])?$educationData['end_month']:0; ?>
			                  @foreach ($month as $key => $value) 
			                  		@if($end_month == $value)
			                  			<option value= <?= $key ?> Selected > <?= $value ?></option>
			                  		@else
			                  			<option value= <?= $key ?> > <?= $value ?></option>
			                  		@endif
			                  @endforeach
			                  </select>
			                </div>
			                </div>
			                <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
			                <div class="form-group label-floating is-select">
			                  <label class="control-label">Select Ended Year</label>
			                  <select class="form-control" name="Education[<?= $count ?>][end_year]">
			                  
			                  <?php 
			                  $end_year = isset($educationData['end_year'])?$educationData['end_year']:0;
			                  	for ($i=1990; $i <= date("Y") ; $i++){
			                  	?>
			                  		@if($end_year == $i)
			                  			<option value= <?= $i ?> Selected > <?= $i ?></option>
			                  		@else
			                  			<option value= <?= $i ?> > <?= $i ?></option>
			                  		@endif
			                  	<?php
			                  }
			                  ?>
			                  </select>
			                </div>
			                </div>
							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group label-floating">
								<?php $description = isset($educationData['description'])?$educationData['description']:''; ?>
									<label class="control-label">Description</label>
									<textarea class="form-control" placeholder="" name="Education[<?= $count ?>][description]" 
									value= "<?= $description ?>" ><?= $description ?></textarea>
								</div>
							</div>