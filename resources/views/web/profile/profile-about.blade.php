@extends('web.master_layout.master')



@section('title')

    @if(!empty(Session::get('Profile - About'))) {{ Session::get('Profile - About') }} @else Profile - About @endif

@endsection



@section('main-content')



<!-- Top Header-Profile -->



{!! $top_header_profile !!}



<!-- ... end Top Header-Profile -->



<div class="container">

  <div class="row">

    <div class="col col-xl-8 order-xl-2 col-lg-8 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">

      <div class="ui-block">

        <div class="ui-block-title">

          <h6 class="title">Hobbies and Interests</h6>

          <a href="#" class="more">

            <i class="fa fa-edit"></i>

          </a>

        </div>

        <div class="ui-block-content">

          <div class="row">

            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">



              

              <!-- W-Personal-Info -->

              
 <?php $user_id = isset($userData['id'])?$userData['id']:0; ?>
              <ul class="widget w-personal-info item-block">

                <li>

                  <span class="title">Hobbies:</span>

                  <span class="text">

                  <?php echo isset($userData['hobby'])?$userData['hobby']:''; ?>

                  </span>

                </li>

                <li>

                  <span class="title">Favourite TV Shows:</span>

                  <span class="text"><?php echo isset($userData['fav_tv'])?$userData['fav_tv']:''; ?></span>

                </li>

                <li>

                  <span class="title">Favourite Movies:</span>

                  <span class="text"><?php echo isset($userData['fav_movie'])?$userData['fav_movie']:''; ?></span>

                </li>

                <li>

                  <span class="title">Favourite Games:</span>

                  <span class="text"><?php echo isset($userData['fav_game'])?$userData['fav_game']:''; ?></span>

                </li>

              </ul>

              

              <!-- ... end W-Personal-Info -->

            </div>

            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">



              

              <!-- W-Personal-Info -->

              

              <ul class="widget w-personal-info item-block">

                <li>

                  <span class="title">Favourite Music Bands / Artists:</span>

                  <span class="text"><?php echo isset($userData['fav_music'])?$userData['fav_music']:''; ?></span>

                </li>

                <li>

                  <span class="title">Favourite Books:</span>

                  <span class="text"><?php echo isset($userData['fav_book'])?$userData['fav_book']:''; ?></span>

                </li>

                <li>

                  <span class="title">Favourite Writers:</span>

                  <span class="text"><?php echo isset($userData['fav_writer'])?$userData['fav_writer']:''; ?> </span>

                </li>

                <li>

                  <span class="title">Other Interests:</span>

                  <span class="text"><?php echo isset($userData['other_interest'])?$userData['other_interest']:''; ?></span>

                </li>

              </ul>

              

              <!-- ... end W-Personal-Info -->

            </div>

          </div>

        </div>

      </div>

      <div class="ui-block">

        <div class="ui-block-title">

          <h6 class="title">Education and Employement</h6>

          <a href="#" class="more">

            <i class="fa fa-edit"></i>

          </a>

        </div>

        <div class="ui-block-content">

          <div class="row">

            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">



              

              <!-- W-Personal-Info -->

              

              <ul class="widget w-personal-info item-block">

                <li>

                  <span class="title">The New College of Design</span>

                  <span class="date">2001 - 2006</span>

                  <span class="text">Breaking Good, RedDevil, People of Interest, The Running Dead, Found,  American Guy.</span>

                </li>

                <li>

                  <span class="title">Rembrandt Institute</span>

                  <span class="date">2008</span>

                  <span class="text">Five months Digital Illustration course. Professor: Leonardo Stagg.</span>

                </li>

                <li>

                  <span class="title">The Digital College </span>

                  <span class="date">2010</span>

                  <span class="text">6 months intensive Motion Graphics course. After Effects and Premire. Professor: Donatello Urtle. </span>

                </li>

              </ul>

              

              <!-- ... end W-Personal-Info -->



            </div>

            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">



              

              <!-- W-Personal-Info -->

              

              <ul class="widget w-personal-info item-block">

                <li>

                  <span class="title">Digital Design Intern</span>

                  <span class="date">2006-2008</span>

                  <span class="text">Digital Design Intern for the “Multimedz” agency. Was in charge of the communication with the clients.</span>

                </li>

                <li>

                  <span class="title">UI/UX Designer</span>

                  <span class="date">2008-2013</span>

                  <span class="text">UI/UX Designer for the “Daydreams” agency. </span>

                </li>

                <li>

                  <span class="title">Senior UI/UX Designer</span>

                  <span class="date">2013-Now</span>

                  <span class="text">Senior UI/UX Designer for the “Daydreams” agency. I’m in charge of a ten person group, overseeing all the proyects and talking to potential clients.</span>

                </li>

              </ul>

              

              <!-- ... end W-Personal-Info -->

            </div>

          </div>

        </div>

      </div>

    </div>



    <div class="col col-xl-4 order-xl-1 col-lg-4 order-lg-1 col-md-12 order-md-2 col-sm-12 col-12">

      <div class="ui-block">

        <div class="ui-block-title">

          <h6 class="title">Personal Info</h6>

          <a href="#" class="more">

            <i class="fa fa-edit"></i>

          </a>

        </div>

        <div class="ui-block-content">



          

          <!-- W-Personal-Info -->

          

          <ul class="widget w-personal-info">

            <li>

              <span class="title">About Me:</span>

              <span class="text">

              <?php echo isset($userData['about_u'])?$userData['about_u']:''; ?>

              </span>

            </li>

            <li>

              <span class="title">Birthday:</span>

              <span class="text">

              <?php 

                $date = isset($userData['dob'])?$userData['dob']:'';

                echo date('F d, Y',strtotime($date));

              ?>

              </span>

            </li>

            <li>

              <span class="title">Birthplace:</span>

              <span class="text"><?php isset($userData['birth_place'])?$userData['birth_place']:''; ?></span>

            </li>

            <li>

              <span class="title">Lives in:</span>

              <span class="text">

              <?php 

                $city = isset($userData['city'])?$userData['city']:'';

                $state = isset($userData['state'])?$userData['state']:'';

                echo getCityName($city).", ".getStateName($state);

              ?>

              </span>

            </li>

            <li>

              <span class="title">Occupation:</span>

              <span class="text">UI/UX Designer</span>

            </li>

            <li>

              <span class="title">Joined:</span>

              <span class="text">

                <?php

                  $date = isset($userData['created_at'])?$userData['created_at']:'';

                  echo date('F d, Y',strtotime($date));

                ?>

              </span>

            </li>

            <li>

              <span class="title">Gender:</span>

              <span class="text"><?php echo isset($userData['gender'])?$userData['gender']:'-'; ?></span>

            </li>

            <li>

              <span class="title">Status:</span>

              <span class="text"><?php echo isset($userData['relationship'])?$userData['relationship']:'-'; ?></span>

            </li>

            <li>

              <span class="title">Email:</span>

              <a href="#" class="text"><?php echo isset($userData['email'])?$userData['email']:'-'; ?></a>

            </li>

            <li>

              <span class="title">Website:</span>

              <a href="#" class="text"><?php echo isset($userData['website'])?$userData['website']:'-'; ?></a>

            </li>

            <li>

              <span class="title">Phone Number:</span>

              <span class="text"><?php echo isset($userData['phone_number'])?$userData['phone_number']:'-'; ?></span>

            </li>

          </ul>

          

          <!-- ... end W-Personal-Info -->

          <!-- W-Socials -->

          



          

          

          <!-- ... end W-Socials -->

        </div>

      </div>

    </div>

  </div>

</div>

@endsection

@push('scripts')

<script type="text/javascript">



  // $(document).ready(function(){

  //   var id = $("select[name=state]").val();

  //       var selected = $('#dist_selected').val();

  //       $.ajax({

  //           type: "GET",

  //           url: "{{route('selectDistrict')}}",

  //           data: {id: id,selected:selected},

  //           cache: false,

  //           success: function (data) {

  //               $('#district').html(data);

  //           }

  //       });

  //   });    

    function isNumberKey(evt)

    {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 32 && (charCode < 46 || charCode > 57) || (charCode == 32)) {

            alert("Please enter only digits");

            return false;

        }

        return true;

    }

$("input[name='phone_number']").on("keyup paste", function () {

            // Remove invalid chars from the input

            var input = this.value.replace(/[^0-9\(\)\s\-]/g, "");

            var inputlen = input.length;

            // Get just the numbers in the input

            var numbers = this.value.replace(/\D/g, '');

            var numberslen = numbers.length;

            // Value to store the masked input

            var newval = "";

            

            $(this).val(numbers.substring(0, 14));

        });



    function changeState(id) {

        $.ajax({

            type: "GET",

            url: "{{route('selectDistrict')}}",

            data: {id: id},

            cache: false,

            success: function (data) {

                $('#district').html(data);

            }

        });             

    }

</script>

@endpush('scripts')