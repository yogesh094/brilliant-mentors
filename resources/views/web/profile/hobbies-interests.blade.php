@extends('web.master_layout.master')



@section('title')

    @if(!empty(Session::get('Hobbies/Interests'))) {{ Session::get('Hobbies/Interests') }} @else Hobbies/Interests @endif

@endsection



@section('main-content')

<!-- Main Header Account -->

<div class="main-header">

  <div class="content-bg-wrap bg-account"></div>

  <div class="container">

    <div class="row">

      <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">

        <div class="main-header-content">

          <h1>Your Account Dashboard</h1>

          <p>Welcome to your account dashboard! Here you’ll find everything you need to change your profile

		  information, settings, read notifications and requests, view your latest messages, change your pasword and much

		  more! Also you can create or manage your own favourite page, have fun!</p>

        </div>

      </div>

    </div>

  </div>

  <img class="img-bottom" src="{{ asset('resources/web-assets/img/account-bottom.png') }}" alt="friends">

</div>



<!-- ... end Main Header Account -->





<!-- Your Account Personal Information -->



<div class="container">

	<div class="row">

		<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">

			<div class="ui-block">

				<div class="ui-block-title">

					<h6 class="title">Hobbies and Interests</h6>

				</div>
				@if (Session::has('error'))

			    <div class="alert alert-danger">{!! Session::get('error') !!}</div>

			    @endif

			    @if (Session::has('success'))

			    <div class="alert alert-success">{!! Session::get('success') !!}</div>

			    @endif

				<div class="ui-block-content">
					<!-- Form Hobbies and Interests -->
					<form role= "form" id="interest-form" method="POST" action="{{ url('/save-interest') }}">
						<div class="row">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<div class="col col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="control-group" id="stream">
	                                <label class="control-label">Stream<span class="error">*</span> :</label>
	                                <div class="controls">
	                                    <div class="subject-info-box-1">
	                                        <select multiple="multiple" name="stream_id_unselected[]" id='stream_id1' class="form-control">
	                                            @foreach($stream as $s)
							                      <option value="{{ $s['id'] }}">{{ $s['title'] }}</option>
							                    @endforeach
	                                        </select>
	                                    </div>
	                                    <div class="subject-info-arrows text-center">
	                                        <input type='button' id='stream_idbtnAllRight' value='>>' class="btn-default drg-button" />
	                                        <input type='button' id='stream_idbtnRight' value='>' class="btn-default drg-button" />
	                                        <input type='button' id='stream_idbtnLeft' value='<' class="btn-default drg-button" />
	                                        <input type='button' id='stream_idbtnAllLeft' value='<<' class="btn-default drg-button" />
	                                    </div>
	                                    <div class="subject-info-box-2">
	                                        <select multiple="multiple" name="stream_id[]" id='stream_id2' class="form-control" required>
	                                            @foreach($stream_selected as $s)
                                                  <option value="{{ $s['id'] }}" selected>{{ $s['title'] }}</option>
                                                @endforeach
	                                        </select>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="control-group" id="substream">
	                                <label class="control-label">Sub Stream<span class="error">* </span>:</label>
	                                <div class="controls">
	                                    <div class="subject-info-box-1">
	                                        <select multiple="multiple" name="sub_stream_id_unselected[]" id='sub_stream_id1' class="form-control">
	                                        @foreach($sub_stream as $s)
                                                  <option value="{{ $s['id'] }}">{{ $s['title'] }}</option>
                                            @endforeach
                                            </select>
	                                    </div>
	                                    <div class="subject-info-arrows text-center">
	                                        <input type='button' id='sub_stream_idbtnAllRight' value='>>' class="btn-default drg-button" />
	                                        <input type='button' id='sub_stream_idbtnRight' value='>' class="btn-default drg-button" />
	                                        <input type='button' id='sub_stream_idbtnLeft' value='<' class="btn-default drg-button" />
	                                        <input type='button' id='sub_stream_idbtnAllLeft' value='<<' class="btn-default drg-button" />
	                                    </div>
	                                    <div class="subject-info-box-2">
	                                        <select multiple="multiple" name="sub_stream_id[]" id='sub_stream_id2' class="form-control" required>
	                                        @foreach($sub_stream_selected as $s)
                                                  <option value="{{ $s['id'] }}" selected>{{ $s['title'] }}</option>
                                            @endforeach
                                            </select>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
				                <button class="btn btn-primary btn-lg full-width">Save all Changes</button>
				            </div>
						</div>
					</form>
					<!-- ... end Form Hobbies and Interests -->
				</div>
			</div>
		</div>

		<div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">

			<div class="ui-block">

		{!! $profile_sidebar !!}

			</div>

		</div>

	</div>

</div>



<!-- ... end Your Account Personal Information -->







@endsection

@push('scripts')
<script type="text/javascript">
(function () {
        $('#stream_idbtnRight').click(function (e) {

            var selectedOpts = $('#stream_id1 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }
            $('#stream_id2').append($(selectedOpts).clone());
            $('#stream_id2 > option').attr('selected', 'selected');
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#stream_idbtnAllRight').click(function (e) {
            var selectedOpts = $('#stream_id1 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#stream_id2').append($(selectedOpts).clone());
            $('#stream_id2 option').attr('selected', 'selected');
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#stream_idbtnLeft').click(function (e) {
            var selectedOpts = $('#stream_id2 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#stream_id1').append($(selectedOpts).clone());
            $('#stream_id1 option').removeAttr("selected");
            $(selectedOpts).remove();
            e.preventDefault();
            $('#stream_id2 > option').attr('selected', 'selected');
        });

        $('#stream_idbtnAllLeft').click(function (e) {
            var selectedOpts = $('#stream_id2 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#stream_id1').append($(selectedOpts).clone());
            $('#stream_id1 option').removeAttr("selected");
            $(selectedOpts).remove();
            e.preventDefault();
            $('#stream_id2 > option').attr('selected', 'selected');
        });
}(jQuery));

(function () {
        $('#sub_stream_idbtnRight').click(function (e) {

            var selectedOpts = $('#sub_stream_id1 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }
            $('#sub_stream_id2').append($(selectedOpts).clone());
            $('#sub_stream_id2 > option').attr('selected', 'selected');
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#sub_stream_idbtnAllRight').click(function (e) {
            var selectedOpts = $('#sub_stream_id1 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#sub_stream_id2').append($(selectedOpts).clone());
            $('#sub_stream_id2 option').attr('selected', 'selected');
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#sub_stream_idbtnLeft').click(function (e) {
            var selectedOpts = $('#sub_stream_id2 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#sub_stream_id1').append($(selectedOpts).clone());
            $('#sub_stream_id1 option').removeAttr("selected");
            $(selectedOpts).remove();
            e.preventDefault();
            $('#sub_stream_id2 > option').attr('selected', 'selected');
        });

        $('#sub_stream_idbtnAllLeft').click(function (e) {
            var selectedOpts = $('#sub_stream_id2 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#sub_stream_id1').append($(selectedOpts).clone());
            $('#sub_stream_id1 option').removeAttr("selected");
            $(selectedOpts).remove();
            e.preventDefault();
            $('#sub_stream_id2 > option').attr('selected', 'selected');
        });
}(jQuery));

$('#stream_id1').on('change', function () {
    var id = $(this).val();
    var token = $('input[name="_token"]').val();
    var streamArray = new Array();//storing the selected values inside an array
    $('#stream_id1 option:selected').each(function (i, selected) {
        streamArray[i] = $(selected).val();
    });

    if (streamArray) {
    	$.ajax({
            type: "POST",
            url: "{{route('selectSubStream')}}",
            data: {id: id, _token: token},
            cache: false,
            dataType: "json",
            success: function (data) {
                $('#sub_stream_id1').empty();
                $.each(data, function (key, value) {
                    $('#sub_stream_id1').append('<option value="' + value.id + '">' + value.title + '</option>');
                });
            }
        });
    } else {
        $('#sub_stream_id1').empty();
    }
});
</script>
@endpush('scripts')