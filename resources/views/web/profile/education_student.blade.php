@extends('web.master_layout.master')



@section('title')

    @if(!empty(Session::get('Education/Experience'))) {{ Session::get('Education/Experience') }} @else Education/Experience @endif

@endsection



@section('main-content')

<!-- Main Header Account -->

<div class="main-header">

  <div class="content-bg-wrap bg-account"></div>

  <div class="container">

    <div class="row">

      <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">

        <div class="main-header-content">

          <h1>Your Account Dashboard</h1>

          <p>Welcome to your account dashboard! Here you’ll find everything you need to change your profile

  information, settings, read notifications and requests, view your latest messages, change your pasword and much

  more! Also you can create or manage your own favourite page, have fun!</p>

        </div>

      </div>

    </div>

  </div>

  <img class="img-bottom" src="{{ asset('resources/web-assets/img/account-bottom.png') }}" alt="friends">

</div>



<!-- ... end Main Header Account -->





<!-- Your Account Personal Information -->



<div class="container">

	<div class="row">

		<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">

			<div class="ui-block">

				<div class="ui-block-title">

					<h6 class="title">Your Education History</h6>

				</div>
				@if (Session::has('error'))

	    <div class="alert alert-danger">{!! Session::get('error') !!}</div>

	    @endif

	    @if (Session::has('success'))

	    <div class="alert alert-success">{!! Session::get('success') !!}</div>

	    @endif

				<div class="ui-block-content">



					

					<!-- Education History Form -->

					

					<form role="form" method="POST" id="education-form">

						<div class="row education-block">

							

							{!! $educationView !!}

							

							</div>

							<input type="hidden" name="education_count" value=<?= $employment_count ?> >

							<meta type="hidden" name="csrf-token" content="{{csrf_token()}}">

							

							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">

							<a href="#" class="add-field" id="add-education">

									<svg class="olymp-plus-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-plus-icon"></use></svg>

									<span> + Add Education Field</span>

							</a>

							</div>

						<div class="row">

							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">

								<button class="btn btn-secondary btn-lg full-width">Cancel</button>

							</div>

							<div class="col col-lg-6 col-md-6 col-sm-12 col-12" id="save-education">

								<button class="btn btn-primary btn-lg full-width">Save all Changes</button>

							</div>

							</div>

					</form>

					

					<!-- ... end Education History Form -->

				</div>

			</div>

			<div class="ui-block">

				<div class="ui-block-title">

					<h6 class="title">Your Employement History</h6>

				</div>

				<div class="ui-block-content">



					

					<!-- Employement History Form -->

					

					<form role="form" method="POST" id="employment-form">

						<div class="row" id="employment-block">

						

							{!! $employmentView !!}

							</div>

						

							<input type="hidden" name="employment_count" id="employment_count" value= <?= $employment_count ?> >

							<meta type="hidden" name="csrf-token" content="{{csrf_token()}}">

							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">

							<a href="#" class="add-field" id="add-employment">

									<svg class="olymp-plus-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-plus-icon"></use></svg>

									<span> + Add Employement Field</span>

							</a>

							</div>

					

							<div class="row">

							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">

								<button class="btn btn-secondary btn-lg full-width">Cancel</button>

							</div>

							<div class="col col-lg-6 col-md-6 col-sm-12 col-12" id="save-employment">

								<button class="btn btn-primary btn-lg full-width">Save all Changes</button>

							</div>

							</div>

						

					</form>

					

					<!-- ... end Employement History Form -->

				</div>

			</div>

		</div>



		<div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">

			<div class="ui-block">

		{!! $profile_sidebar !!}

			</div>

		</div>

	</div>

</div>



<!-- ... end Your Account Personal Information -->







@endsection

@push('scripts')

<script type="text/javascript">



$(document).on('click','#add-education',function (e){

        e.preventDefault();

        var education_count = $('#education_count').val();

			$.ajax({

            type: "GET",

            url: "{{route('education-block')}}",

            data: {education_count: education_count},

            cache: false,

            success: function (data) {

               $(".education-block").append(data.content);

               $('#education_count').val(data.education_count);

            }

        });         

    });



$(document).on('click','#add-employment',function (e){

        e.preventDefault();

        var employment_count = $('#employment_count').val();

			$.ajax({

            type: "GET",

            url: "{{route('employment-block')}}",

            data: {employment_count: employment_count},

            cache: false,

            success: function (data) {

               $("#employment-block").append(data.content);

               $('#employment_count').val(data.employment_count);

            }

        });         

    });





$(document).on('click','#save-employment',function (e){

        e.preventDefault();

        var token = $('meta[name="csrf-token"]').attr('content');

			$.ajax({

            type: "POST",

            dataType: "json",

            url: "{{route('employment-save')}}",

            data: {

	            "_method": 'POST',

	            "_token": token,

	            "data": $("#employment-form").serialize(),

        	},

            cache: false,

            success: function (data) {

            	alert(data.status)

            	if(data.status == 200){

            		$( '#employment-form' ).each(function(){this.reset();});

            		$("#employment-form")[0].reset();	

            	}

            	

              

            }

        });         

    });

$(document).on('click','#save-education',function (e){

        e.preventDefault();

        var token = $('meta[name="csrf-token"]').attr('content');

			$.ajax({

            type: "POST",

            dataType: "json",

            url: "{{route('education-save')}}",

            data: {

	            "_method": 'POST',

	            "_token": token,

	            "data": $("#education-form").serialize(),

        	},

            cache: false,

            success: function (data) {

            	alert(data.status)

            	if(data.status == 200){

            		$( '#education-form' ).each(function(){this.reset();});

            		$("#education-form")[0].reset();	

            	}

            }

        });         

    });



</script>

@endpush('scripts')