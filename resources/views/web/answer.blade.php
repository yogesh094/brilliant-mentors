@extends('web.master_layout.master')

@section('title')
   @if(!empty(Session::get('Answer'))) {{ Session::get('Answer') }} @else Answer @endif
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>@if(!empty(Session::get('Answer'))) {{ Session::get('Answer') }} @else Answer @endif</h1>
</div>
<div class="h-backimg text-center">
    <div class="container">
      <div class="row">
        <div class="col-12">
        &nbsp;
        </div>
      </div>
    </div>
  </div>
<div class="answer-sec">
  <div class="container">
      <div class="row">
          <div class="col-12">
            <span class="ansTitle">@if(!empty(Session::get('Question'))) {{ Session::get('Question') }} @else Question @endif:</span><br>
                <h2>{{ $questionObj->question }}</h2>
                <hr class="borderLine">
                <span class="ansTitle">@if(!empty(Session::get('Answer'))) {{ Session::get('Answer') }} @else Answer @endif:</span><br>
<!--                <small><i>@if(!empty(Session::get('By'))) {{ Session::get('By') }} @else By @endif: {{ $getExpertName }}</i></small><br>
                <small><i>@if(!empty(Session::get('day'))) {{ Session::get('day') }} @else day @endif: {{ Carbon\Carbon::parse($questionObj->updated_at)->format('F d,Y (g:i A)') }}</i></small>-->
<!--               <p></p>
               <br>-->
                <p>{!! $questionObj->answer !!}</p> 
            </div>
          <div class="col-12 text-center mb-4">
              
            </div>
        </div>
    </div>
</div>
@endsection