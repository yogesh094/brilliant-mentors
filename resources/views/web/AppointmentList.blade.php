@extends('web.master_layout.master')

@section('title')
@if(!empty(Session::get('Your Consultation'))) {{ Session::get('Your Consultation') }} @else Your Consultation @endif
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
    <h1>@if(!empty(Session::get('Your Consultation'))) {{ Session::get('Your Consultation') }} @else Your Consultation @endif</h1>
</div>
<div class="h-backimg m-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!--
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Category</li>
                    <li class="breadcrumb-item">Topic</li>
                    <li class="breadcrumb-item">Concept</li>
                </ol>-->
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        @if(count($dataArr) > 0)
        <div class="col-12" style="margin-bottom: 12px;">
            <div class="book">
                <h4 class="m-0">@if(!empty(Session::get('Booked Consultation'))) {{ Session::get('Booked Consultation') }} @else Booked Consultation @endif</h4>
            </div>
        </div>
        <div class="ajaxerrorreg col-12"></div>
        @foreach($dataArr as $key => $list)
        <div class="card notifi col-12">
            <div class="card-body  pd_0px">
                <div class="row">
                    <div class="col-5 clr"> 
                        <small><i><strong>@if(!empty(Session::get('With'))) {{ Session::get('With') }} @else With @endif:</strong>
                                {{ $list['expert_name'] }}
                            </i></small>
                        <br>
                        <small><strong>@if(!empty(Session::get('Date and Time'))) {{ Session::get('Date and Time') }} @else Date and Time @endif:</strong> 
                            {{ Carbon\Carbon::parse($list['booking_date'])->format('F d,Y') }} ({{ Carbon\Carbon::parse($list['start_time'])->format('g:i A') }} - {{ Carbon\Carbon::parse($list['end_time'])->format('g:i A') }})</small>
                        <br>
                        <small><i><strong>@if(!empty(Session::get('Category'))) {{ Session::get('Category') }} @else Category @endif:</strong>
                                {{ $list['category_name'] }}
                            </i></small><br>
                        <small><i><strong>@if(!empty(Session::get('Description'))) {{ Session::get('Description') }} @else Description @endif:</strong>
                                {{ $list['description'] }}
                            </i></small>
                          <!-- <p class="m-0">{{ $list['description'] }}</p> -->
                    </div>
                    <div class="col-4 clr brdr_2px">
                        @if($list['cancel_type'] == 1)
                        <small><i>{{ Cache::get('reschedule-payment') }}</i></small>
                        <br>

                        @else
                        <small><i><strong>@if(!empty(Session::get('Charge Id'))) {{ Session::get('Charge Id') }} @else Charge Id @endif:</strong> {{ $list['charge_id'] }}</i></small>
                        <br>
                        <small><i><strong>@if(!empty(Session::get('Transaction Id'))) {{ Session::get('Transaction Id') }} @else Transaction Id @endif:</strong> {{ $list['balance_transaction'] }}</i></small>
                        <br>
                        <small><i><strong>@if(!empty(Session::get('Amount'))) {{ Session::get('Amount') }} @else Amount @endif:</strong> {{ Cache::get('currency') }}{{ $list['amount'] }}</i></small>
                        <br>
                        <small><i><strong>@if(!empty(Session::get('Brand'))) {{ Session::get('Brand') }} @else Brand @endif:</strong> {{ $list['brand'] }}</i></small>
                        <br>
                        <small><strong>@if(!empty(Session::get('Payment mode'))) {{ Session::get('Payment mode') }} @else Payment mode @endif:</strong> {{ $list['funding'] }}</small>
                        @endif
                    </div>
                    <div class="col-3 clr"> 
                        @if($list['cancel_type'] != 1)
                        <small><i><strong>@if(!empty(Session::get('Payment Status'))) {{ Session::get('Payment Status') }} @else Payment Status @endif:</strong> {{ $list['payment_status'] }}</i></small><br>
                        @endif
                        @if($list['payment_type'] != 2)
                        <small><strong>@if(!empty(Session::get('Status'))) {{ Session::get('Status') }} @else Status @endif:</strong> {{ $list['statusName'] }}</small>
                        @else
                        your refund is successfully,please check your account
                        @endif




                        <div class="col-12 mgtp_20px">
                            <div class="row">
                                <div class="col-12">
                                    @if($list['payment_status'] != 'succeeded' && $list['status'] != 3)
                                    <a href="{{ url('payment/') }}/{{ $list['id'] }}" class="btn btn-primary blue">Pay Now</a>
                                    @endif
                                    @if($list['status'] == 2)
                                    @if(!empty(Session::get('appointment_complete'))) {{ Session::get('appointment_complete') }} @else Your appointment is complete. @endif

                                    @else
                                    @if($list['status'] != 3 && $list['status'] != 4 && $list['video-time'] != 1)
                                    <a href="javascript:void(0)" class="btn btn-primary blue cancelAppointment" id="{{ $list['id'] }}">@if(!empty(Session::get('Cancel'))) {{ Session::get('Cancel') }} @else Cancel @endif</a>
                                    @endif
                                    @if($list['check48hours'] <= 48)
                                    <br>
                                    <!-- <small><i>{{ Cache::get('refund') }}</i></small> -->
                                    @else
                                    @if($list['cancel_type'] == '' && $list['status'] != 3)
                                    <a href="{{ url('video-consultaion/reschedule') }}/{{ $list['id'] }}" class="btn btn-primary blue">@if(!empty(Session::get('Reschedule'))) {{ Session::get('Reschedule') }} @else Reschedule @endif</a>

                                    @if($list['status'] == 4 && $list['payment_type'] != 2)
                                    <a href="javascript:void(0)" class="btn btn-primary blue RefundAppointment" id="{{ $list['id'] }}">@if(!empty(Session::get('Refund'))) {{ Session::get('Refund') }} @else Refund @endif</a>
                                    @endif
                                    @endif
                                    @endif
                                    @endif
                                    @if($list['status'] == 3)
                                    <small><i> @if(!empty(Session::get('Appointment cancel'))) {{ Session::get('Appointment cancel') }} @else Appointment cancel @endif</i></small>
                                    @endif
                                    <img src="{{ asset('resources/web-assets/images/loading.gif') }}" class="loading{{ $list['id'] }}" style="display: none;">
                                </div>
                                <br>
                                @if($list['video'] == 1 && $list['video-time'] == 1) 
                                <div class="col-6">
                                    <a href="{{ url('video-call') }}" class="btn btn-success">Video call</a> </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @else

        <div class="col-12" style="text-align: center;">
            <img src="{{ asset('resources/web-assets/images/blank_img.png') }}">
            <h2 >
                @if(!empty(Session::get('Your Consultation list is empty!'))) {{ Session::get('Your Consultation list is empty!') }} @else Your Consultation list is empty! @endif
            </h2>
        </div>

        @endif

        <nav aria-label="Page navigation example">
            {{ $Appointment->links() }}
        </nav>

    </div>
</div>
@endsection