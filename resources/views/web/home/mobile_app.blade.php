<div class="app-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                {!! \App\Models\CMS::where('status',1)->where('slug','mobile_app')->where('language_id',Session()->get('selectedLang'))->first()->description !!}
            </div>
            <div class="col-md-5 text-right app-dtl"> 
                <a href="#"><img src="{{ asset('resources/web-assets/images/app_store.png') }}" class="img-fluid" alt="App Store"></a> 
                <a href="#" class="mgl-15"><img src="{{ asset('resources/web-assets/images/google_play.png') }}" class="img-fluid" alt="Google Play"></a> </div>
        </div>
    </div>
</div>
<div class="f-dtl-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-3"> <img src="{{ asset('resources/web-assets/images/white_logo.png') }}" class="img-fluid" alt="Islamic Religious"> </div>
            <div class="col-md-9">
                {!! \App\Models\CMS::where('status',1)->where('slug','footer')->where('language_id',Session()->get('selectedLang'))->first()->description !!}
                
            </div>
        </div>
    </div>
</div>