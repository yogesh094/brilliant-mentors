<div class="adv-sec">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mgb-1p25">
                <h1 class="title-h1">
               @if(!empty(Session::get('Question'))) {{ Session::get('Question') }} @else Question @endif
            </h1>
                <img src="{{ asset('resources/web-assets/images/sep_img.png') }}" alt="HeaderSeparator" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4" style="padding-bottom: 15px;"> 
              @if (count($advertisement) > 0)
              <div id="jssor_1" >
                 <div data-u="slides" style="cursor:default;position:relative;width:328px;height:230px;overflow:hidden;">
                @foreach($advertisement as $key => $advertisements)
                <div>
                    <img class="slides-image" src="{{ url('public') }}{{ $advertisements['image_path'] }}" alt="{{ $advertisements['name'] }}" title="{{ $advertisements['title'] }}">
                </div>
                @endforeach
            </div>
            </div>
              @else
                <img src="{{ asset('resources/web-assets/images/book_img.png') }}" class="img-fluid" style="height: 400px;" />
              @endif
            </div>
            <div class="col-md-8">
                <div id="Testimonial" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <!-- {!! \App\Models\CMS::where('status',1)->where('slug','testimonial')->where('language_id',Session()->get('selectedLang'))->first()->description !!} question/view/250-->
                        @if(count($getData) > 0)
                        @foreach($getData as $key => $data)
                         <div class="carousel-item @if($key == 0)active @endif" style="min-height: 200px;">
                            <h4 class="h4"><a href="{{ url('question/view/') }}/{{ $data->id }}" >{{ str_limit($data->question,100) }}</a></h4>
                            <hr>

                        <p>{!! str_limit($data->answer,400) !!}</p>
                        </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="clearfix"></div>
                    @if(count($getData) > 0)
                    <ol class="carousel-indicators">
                        @foreach($getData as $key => $data)
                        <li data-target="#Testimonial" data-slide-to="{{ $key }}" class="@if($key == 0) active @endif"></li>
                        @endforeach
                    </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>