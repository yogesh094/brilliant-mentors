<div class="h-backimg text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="title-h1">
                    {{ \App\Models\CMS::where('status',1)->where('slug','about_us')->where('language_id',Session()->get('selectedLang'))->first()->title }}
                </h1>
                <img src="{{ asset('resources/web-assets/images/sep_img.png') }}" alt="HeaderSeparator" /> </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4"> <img src="{{ asset('resources/web-assets/images/book_img.png') }}" class="img-fluid" /> </div>
        <div class="col-md-8">
            {!! str_limit(\App\Models\CMS::where('status',1)->where('slug','about_us')->where('language_id',Session()->get('selectedLang'))->first()->description,500) !!}
            <p><a class="btn btn-success" href="{{ url('aboutus') }}">@if(!empty(Session::get('Read More'))) {{ Session::get('Read More') }} @else Read More @endif</a></p>
        </div>
    </div>
</div>