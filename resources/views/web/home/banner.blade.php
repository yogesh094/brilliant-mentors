<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel"> 
    <div class="carousel-inner">
        @if (count($Slider) > 0)
        @foreach($Slider as $key => $Sliders)
            <div class="carousel-item @if($key == 0)active @endif"> 
            <img class="first-slide img-fluid" src="{{ url('public') }}{{ $Sliders['image_path'] }}" alt="{{ $Sliders['name'] }}" title="{{ $Sliders['title'] }}">
            <div class="container">
                <div class="carousel-caption">
                    {!! $Sliders['description'] !!}
                    <!-- <a class="btn btn-success btn-lg" href="#" role="button">@if(!empty(Session::get('about_us'))) {{ Session::get('about_us') }} @else About us @endif</a> --> </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i> <span class="sr-only">Next</span> </a> </div>