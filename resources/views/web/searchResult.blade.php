<div id="accordion">
      @if(count($Data) > 0)
                @foreach($Data as $key => $questions)
                @if($questions->answer != '')
                  <div class="card">
                    <div class="card-header" id="heading{{ $key }}">
                      <h5 class="mb-0 question">
                        <button class="btn btn-link @if($key 
                        != 0) collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                         {{ $questions->question }}
                        </button>
                         <a href="javascript:void(0);" class="plusIcon @if($key == 0) @else collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                          <!-- <i class="fa fa-plus" aria-hidden="true"></i> -->
                        </a>
                      </h5>
                    </div>
                
                    <div id="collapse{{ $key }}" class="collapse @if($key == 0)show @endif" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
                      <div class="card-body">
<!--                        @if(isset($questions->expert_id) && $questions->expert_id > 0)
                        <small><i>By: {{ \App\Models\User::where('id',$questions->expert_id)->first()->username }}</i></small><br>
                <small><i>day: {{ Carbon\Carbon::parse($questions->updated_at)->format('F d,Y (g:i A)') }}</i></small>
                <br>
                @endif-->
                         @if(strlen(strip_tags($questions->answer)) > 400)
                        {!! str_limit($questions->answer,400) !!}
                        <br>
                        <div class="read-more-question">
                          <a href="{{ url('question-answer') }}/{{ $questions->id }}">@if(!empty(Session::get('Read More'))) {{ Session::get('Read More') }} @else Read More @endif</a>
                        </div>
                      @else
                        {!! $questions->answer !!}
                      @endif
                      </div>
                    </div>
                  </div>
                  @endif
                  @endforeach
                  @else
                <div class="notfound" style="text-align: center;">
                  <img src="{{ asset('resources/web-assets/images/blank_img.png') }}">
                  <h2 > @if(!empty(Session::get('Question not found'))) {{ Session::get('Question not found') }} @else Question not found @endif</h2>
                </div>
                  @endif
</div>