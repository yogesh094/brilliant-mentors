@extends('web.master_layout.master')

@section('title')
    @if(!empty(Session::get('Notification'))) {{ Session::get('Notification') }} @else Notification @endif
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>@if(!empty(Session::get('Notification'))) {{ Session::get('Notification') }} @else Notification @endif</h1>
</div>
<div class="h-backimg m-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!--<ol class="breadcrumb">
                    <li class="breadcrumb-item">Category</li>
                    <li class="breadcrumb-item">Topic</li>
                    <li class="breadcrumb-item">Concept</li>
                </ol>-->
            </div>
        </div>
    </div>
</div>
<div class="container  mb-3">
    <div class="row">
      <div class="col-12">
        @if(count($data) > 0)
        @foreach($data as $key => $notifications)
        <div class="card notifi">
            <div class="card-body">
              <div class="row">
              <!-- <div class="col">
                <i class="fa fa-user-circle-o" aria-hidden="true"></i>
              </div> -->
              <div class="col-8">
                <p class="m-0">
                  <a href="@if($notifications['type_id'] == 1 && $notifications['answer'] != ''){{ url('/notification/view') }}/{{ $notifications['id'] }}@elseif ($notifications['type_id'] == 0) {{ url('/appointmentList') }} @else javascript:void(0) @endif" class="notificationTitle">{{ $notifications['message'] }}</a>
                  <br>
                
                 <small><i>
                  {{ $notifications['description'] }}
                </i></small>
                 
               </p>
              </div>
              <div class="col text-right">
                @if($notifications['type_id'] == 1 && $notifications['answer'] != '')
                <a href="@if($notifications['type_id'] == 1){{ url('/notification/view') }}/{{ $notifications['id'] }}@else {{ url('/appointmentList') }} @endif"> 
                          @if(!empty(Session::get('view_notification'))) {{ Session::get('view_notification') }} @else View Notification @endif</a>
                @elseif($notifications['type_id'] == 0)
                <a href="{{ url('/appointmentList') }}"> 
                   @if(!empty(Session::get('view_notification'))) {{ Session::get('view_notification') }} @else View Notification @endif
                 </a>
                @endif
                <div class="clearfix"></div>
                <small>{{ webGeneral::TimeAgo($notifications['created_at'],Carbon\Carbon::now()) }}</small>
              </div>
            </div>
            </div>
        </div>
        @endforeach
        @else
        <div style="text-align: center;">
         <img src="{{ asset('resources/web-assets/images/blank_img.png') }}">
        <h2>
        @if(!empty(Session::get('No Notification Yet'))) {{ Session::get('No Notification Yet') }} @else No Notification Yet @endif</h2>
        </div>
        @endif
        <nav aria-label="Page navigation example">
        {{ $Notification->links() }}
        </nav>
      </div>
    </div>
  </div>
@endsection