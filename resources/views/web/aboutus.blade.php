@extends('web.master_layout.master')

@section('title')
    {{ \App\Models\CMS::where('status',1)->where('slug','about_us')->where('language_id',Session()->get('selectedLang'))->first()->title }}
@endsection

@section('main-content')
<div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ $errors->first() }}.
                <br/>
            </div>
        @endif
        </div>
<?php //exit;?>
   <div class="sub-header">
        <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
        <h1>
        {{ \App\Models\CMS::where('status',1)->where('slug','about_us')->where('language_id',Session()->get('selectedLang'))->first()->title }}
    </h1>
    </div>
    
    <div class="h-backimg text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                &nbsp;
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                {!! \App\Models\CMS::where('status',1)->where('slug','about_us')->where('language_id',Session()->get('selectedLang'))->first()->description !!}
                
            </div>
            <div class="col-md-4"> <img src="{{ asset('resources/web-assets/images/Quran.jpg') }}" class="img-fluid"> </div>
        </div>
    </div>
    <div class="adv-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-4"> <img class="img-fluid" src="{{ asset('resources/web-assets/images/Rewriting-the-Quran.jpg') }}" alt="Quran"> </div>
                <div class="col-md-4">
                    <h2 class="title-h2"> 
                        {{ \App\Models\CMS::where('status',1)->where('slug','our_mission')->where('language_id',Session()->get('selectedLang'))->first()->title }}
                    </h2>
                    <p>
                       
                       {!! str_limit(\App\Models\CMS::where('status',1)->where('slug','our_mission')->where('language_id',Session()->get('selectedLang'))->first()->description,500) !!}
<!-- 
                        {!! \App\Models\CMS::where('status',1)->where('slug','our_mission')->where('language_id',Session()->get('selectedLang'))->first()->description !!} -->
                    </p>
                </div>
                <div class="col-md-4">
                    <h2 class="title-h2">
                        {{ \App\Models\CMS::where('status',1)->where('slug','our_vision')->where('language_id',Session()->get('selectedLang'))->first()->title }}
                    </h2>
                    {!! str_limit(\App\Models\CMS::where('status',1)->where('slug','our_vision')->where('language_id',Session()->get('selectedLang'))->first()->description,500) !!}
                    <!-- {!! \App\Models\CMS::where('status',1)->where('slug','our_vision')->where('language_id',Session()->get('selectedLang'))->first()->description !!} -->
                </div>
            </div>
        </div>
    </div>
    @include('web.home.mobile_app')
@endsection