@extends('web.master_layout.master')

@section('title')
    @if(!empty(Session::get('Question'))) {{ Session::get('Question') }} @else Question @endif
@endsection

@section('main-content')
<div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ $errors->first() }}.
                <br/>
            </div>
        @endif
        </div>
<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>@if(!empty(Session::get('Question'))) {{ Session::get('Question') }} @else Question @endif</h1>
</div>
<div class="h-backimg m-0">
    <div class="container" >
        <div class="row">
            <div class="col-12">
                <!--<ol class="breadcrumb">
                    <li class="breadcrumb-item">Category</li>
                    <li class="breadcrumb-item">Topic</li>
                    <li class="breadcrumb-item">Concept</li>
                </ol>-->
            </div>
        </div>
    </div>
</div>
<div class="question-sec mb-5">
    <div class="container" style="min-height:50vh;">
        <div class="row">
            <div class="col-12">
                <div id="accordion">

                @if(count($question) > 0)
                @foreach($question as $key => $questions)
                  @if($questions->answer != '')
                    <div class="card">
                    <div class="card-header" id="heading{{ $key }}">
                      <h5 class="mb-0 question">
                        <button class="btn btn-link @if($key 
                        != 0) collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                         {{ $questions->question }}
                        </button>
                        
                        <a href="javascript:void(0);" class="plusIcon @if($key == 0) @else collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                          <!-- <i class="fa fa-plus" aria-hidden="true"></i> -->
                        </a>
                        
                      </h5>
                    </div>
                
                    <div id="collapse{{ $key }}" class="collapse @if($key == 0)show @endif" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
                      <div class="card-body">
<!--                @if(isset($questions->expert_id) && $questions->expert_id > 0)
                        <small><i>By: {{ \App\Models\User::where('id',$questions->expert_id)->first()->username }}</i></small><br>
                <small><i>day: {{ Carbon\Carbon::parse($questions->updated_at)->format('F d,Y (g:i A)') }}</i></small>
                <br>
                @endif-->
                        {!! $questions->answer !!}
                      </div>
                    </div>
                  </div>
                  @else
                  <div class="card">
                    <div class="card-header" id="heading{{ $key }}">
                      <h5 class="mb-0 question">
                        <button class="btn btn-link @if($key 
                        != 0) collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                         {{ $questions->question }}
                        </button>
                        
                        <a href="javascript:void(0);" class="plusIcon" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                          <!-- <i class="fa fa-plus" aria-hidden="true"></i> -->
                        </a>
                        
                      </h5>
                    </div>
                
                   
                  </div>
                  @endif
                  @endforeach
                  @else
                  <div style="text-align: center;">
          <img src="{{ asset('resources/web-assets/images/blank_img.png') }}">
        <h2 >
        @if(!empty(Session::get('Your question list is empty!!'))) {{ Session::get('Your question list is empty!!') }} @else Your question list is empty!! @endif
      </h2>
    </div>
                  @endif
                  <nav aria-label="Page navigation example">
          {{ $question->links() }}
        </nav>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection