<div class="modal fade c-popup" id="Login" tabindex="-1" role="dialog" aria-labelledby="signinLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="signinLabel">@if(!empty(Session::get('sign_in'))) {{ Session::get('sign_in') }} @else SIGN IN @endif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row social-btn">
                        <div class="col-12 text-center">
                            <h3 class="pop-stitle">&nbsp;
                            <!-- @if(!empty(Session::get('sign_in_with'))) {{ Session::get('sign_in_with') }} @else Sign in with @endif -->
                        </h3>
                        </div>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4 col-md-6" style="margin-bottom: 10px;">
                        <a href="{{url('auth/facebook') }}" class="btn btn-primary btn-lg btn-block"><i class="fa fa-facebook" aria-hidden="true"></i> @if(!empty(Session::get('facebook'))) {{ Session::get('facebook') }} @else Facebook @endif </a>
                        </div>
                        
                        <div class="col-lg-4 col-md-6">
                            <a href="{{ url('auth/google') }}" class="btn btn-danger btn-lg btn-block"><i class="fa fa-google-plus" aria-hidden="true"></i> @if(!empty(Session::get('google'))) {{ Session::get('google') }} @else Google @endif </a>
                        </div>
                   <div class="col-md-2">      </div>
                        <div class="col-12">
                            <div class="seprator">
                                <h2 class="background"><span>@if(!empty(Session::get('or'))) {{ Session::get('or') }} @else OR @endif</span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row signin-bx">
                        <div class="col-1">&nbsp;</div>
                        <div class="col-10 text-center">
                            <div id="ajaxerrorlogin"></div>
                        <form role="form" class="form-horizontal">
                                <div class="form-group">
                                    <input type="email" name="email" class="pop_fils form-control" id="email" placeholder="@if(!empty(Session::get('email'))) {{ Session::get('email') }} @else Email @endif" />
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="pop_fils form-control" id="password" placeholder="@if(!empty(Session::get('password'))) {{ Session::get('password') }} @else Password @endif" />
                                </div>
                                <button type="submit" class="btn btn-lg btn-success btn-block mb-3" id="login">@if(!empty(Session::get('sign_in'))) {{ Session::get('sign_in') }} @else SIGN IN @endif</button>
                                <a href="#forgotpass" class="f-pass" data-toggle="modal" data-target="#forgotpass" data-dismiss="modal">@if(!empty(Session::get('forgot_password'))) {{ Session::get('forgot_password') }} @else Forgot Password? @endif</a>
                            </form>
                        </div>
                        <div class="col-1">&nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <p class="m-0 text-center">
                    @if(!empty(Session::get('do_not_have_an_account'))) {{ Session::get('do_not_have_an_account') }} @else Don’t have an account? @endif <a href="#Register" data-toggle="modal" data-target="#Register" data-dismiss="modal"> @if(!empty(Session::get('sign_up_now'))) {{ Session::get('sign_up_now') }} @else Sign up now @endif</a>
                </p>
            </div>
        </div>
    </div>
</div>