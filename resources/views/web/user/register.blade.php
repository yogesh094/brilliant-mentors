<div class="modal fade c-popup" id="Register" tabindex="-1" role="dialog" aria-labelledby="signupLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="signupLabel">@if(!empty(Session::get('sign_up'))) {{ Session::get('sign_up') }} @else SIGN UP @endif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row social-btn">
                        <div class="col-12 text-center">
                        <h3 class="pop-stitle">
                            <!-- @if(!empty(Session::get('sign_up_with'))) {{ Session::get('sign_up_with') }} @else Sign up with @endif -->
                        </h3>
                        </div>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-4 col-md-6" style="margin-bottom: 10px;">
                        <a href="{{url('auth/facebook') }}" class="btn btn-primary btn-lg btn-block"><i class="fa fa-facebook" aria-hidden="true"></i> @if(!empty(Session::get('facebook'))) {{ Session::get('facebook') }} @else Facebook @endif</a>
                        </div>
                        
                        <div class="col-lg-4 col-md-6">
                            <a href="{{ url('auth/google') }}" class="btn btn-danger btn-lg btn-block"><i class="fa fa-google-plus" aria-hidden="true"></i> @if(!empty(Session::get('google'))) {{ Session::get('google') }} @else Google @endif</a>
                        </div>
                        <div class="col-md-2">      </div>
                        <div class="col-12">
                            <div class="seprator" style="margin:1.5rem 0rem;">
                                <h2 class="background"><span>@if(!empty(Session::get('or'))) {{ Session::get('or') }} @else OR @endif</span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row signin-bx">
                        <div class="col-1">&nbsp;</div>
                        <div class="col-10 text-center">
                            <div id="ajaxerrorreg"></div>
                        <form class="form-horizontal" id="newUserRegister">
                             {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control form-control-lg" id="email1" placeholder="@if(!empty(Session::get('email'))) {{ Session::get('email') }} @else Email @endif" />
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="@if(!empty(Session::get('password'))) {{ Session::get('password') }} @else Password @endif" />
                                </div>
                                <div class="form-group">
                                    <input type="password" name="conf_password" class="form-control form-control-lg" id="confPassword" placeholder="@if(!empty(Session::get('confirm_password'))) {{ Session::get('confirm_password') }} @else Confirm Password @endif" />
                                </div>
                                <button type="submit" class="btn btn-lg btn-success btn-block mb-3" id="signup">@if(!empty(Session::get('sign_up'))) {{ Session::get('sign_up') }} @else Sign up @endif</button>
                            </form>
                        </div>
                        <div class="col-1">&nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <p class="m-0 text-center">
                    @if(!empty(Session::get('already_have_an _account'))) {{ Session::get('already_have_an _account') }} @else Already have an account? @endif  <a href="#Login" data-toggle="modal" data-target="#Login" data-dismiss="modal"> @if(!empty(Session::get('sign_in_now'))) {{ Session::get('sign_in_now') }} @else Sign in now @endif </a>
                    <img src="{{ asset('resources/web-assets/images/loading.gif') }}" class="loading" style="display: none;">
                </p>
            </div>
        </div>
    </div>
</div>
