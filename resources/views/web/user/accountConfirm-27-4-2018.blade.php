<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php config('constants.site_name');?> - Account Verified</title><meta charset="UTF-8" />
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/icon" href="./images/favicon.ico"/>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('resources/admin-assets/css/bootstrap.min.css') }}" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('resources/admin-assets/css/font-awesome/css/font-awesome.min.css') }}" />
        <!-- NProgress -->
        <link href="{{ asset('resources/admin-assets/css/nprogress/nprogress.css') }}" rel="stylesheet">
        <!-- Animate.css -->
        <link href="{{ asset('resources/admin-assets/css/animate.css/animate.min.css') }}" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="{{ asset('resources/admin-assets/css/custom.min.css') }}" rel="stylesheet">

        <link href="{{ asset('resources/admin-assets/css/mycustom.css') }}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            
            i,h2 {
                color: #009688;
            }
            h5 {
                color: #fefefe;
                font-weight: 400;
            }
            .btn-success {
                margin-bottom: 50px;
            }
        </style>
    </head>

    <body class="login">

            <div class="row login_wrapper">


                <!-- login form start -->
                <div class="login_form">

                    <section class="login_content">
                        <h1>
                            <img  width="60%;" src="{{ url('img/logo.png') }}">
                        </h1>
                        <i class="fa fa-check-circle fa-4x"></i>
                        <h2>
                            Congrulations !!
                        </h2>
                        <h5>
                            Your acount has been set up successfully.<br/>
                            Now you can login to our system.
                        </h5>
                        <a href="{{ url('/') }}" class="btn btn-round btn-success">
                            Login
                        </a>
                         <div>

                            <p style="color: #999;">Powered By Religion Consulting.</p>
                            <p style="color: #999;">© <?= date('Y') ?> All Rights Reserved.</p>
                        </div>
                    </section>
                </div>
            </div>
    </body>
</html>