<div class="modal fade c-popup" id="forgotpass" tabindex="-1" role="dialog" aria-labelledby="signinLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="signinLabel">@if(!empty(Session::get('forgot_password_web'))) {{ Session::get('forgot_password_web') }} @else Forgot Password @endif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    
                    <div class="row signin-bx">
                        <div class="col-1">&nbsp;</div>
                        <div class="col-10 text-center" style="padding-top: 15px;">
                            <div id="ajaxerrorforgot"></div>
                        <form role="form" class="form-horizontal">
                                <div class="form-group">
                                    <input type="email" name="email" class="pop_fils form-control" id="email_forgot" placeholder="@if(!empty(Session::get('email'))) {{ Session::get('email') }} @else Email @endif" />
                                </div>
                                
                                <button type="submit" class="btn btn-lg btn-success btn-block mb-3" id="forgotpassword">@if(!empty(Session::get('forgot_password_web'))) {{ Session::get('forgot_password_web') }} @else Forgot Password @endif</button>
                                
                            </form>
                        </div>
                        <div class="col-1">&nbsp;</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <p class="m-0 text-center">
                    @if(!empty(Session::get('do_not_have_an_account'))) {{ Session::get('do_not_have_an_account') }} @else Don’t have an account? @endif
                     <a href="#Register" data-toggle="modal" data-target="#Register" data-dismiss="modal"> @if(!empty(Session::get('sign_up_now'))) {{ Session::get('sign_up_now') }} @else Sign up now @endif</a>
                </p> -->
            </div>
        </div>
    </div>
</div>