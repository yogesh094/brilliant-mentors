@extends('web.master_layout.master')

@section('title')
   Reset Password
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>@if(!empty(Session::get('resetpassword'))) {{ Session::get('resetpassword') }} @else Reset Password @endif</h1>
</div>
<div class="h-backimg text-center m-0">
  <div class="container">
    <div class="row">
      <div class="col-12"> </div>
    </div>
  </div>
</div>
<div class="container mb-3">
  <div class="row">
    <div class="col-12">
      <h2 class="title-h2">@if(!empty(Session::get('contact_title'))) {{ Session::get('contact_title') }} @else We'd love to here from you! @endif</h2>
    </div>
    
    <div class="col-md-6 order-md-1">
      <div class="col-md-12 col-sm-12 col-xs-12">
       
          @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
        @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
        </div>
        {{ Form::open(array('url' => 'resentPasswordStore' ,'name' => 'frmresentPasswordStore','id' => '')) }}
        <input type="hidden" name="email" value="{{ $email }}">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="password">@if(!empty(Session::get('New Password'))) {{ Session::get('New Password') }} @else New Password @endif</label>
            <input class="form-control" id="newPassword" type="password" name="password">
            <span class="text-danger" >{{ $errors->first('password') }}</span>
            
          </div>
          
        </div>
        <div class="row">
        <div class="col-md-6 mb-3">
          <label for="confirm_password">@if(!empty(Session::get('Confirm Password'))) {{ Session::get('Confirm Password') }} @else Confirm Password @endif </label>
          <input class="form-control" id="ConfirmPassword"  type="password" name="confirm_password">
          <span class="text-danger" >{{ $errors->first('confirm_password') }}</span>
          
        </div>
       </div>
        <input type="submit" name="submit" value="@if(!empty(Session::get('pl_submit_que'))) {{ Session::get('pl_submit_que') }} @else Submit @endif" class="btn btn-success">
      {{ Form::close() }} 
    </div>
  </div>
</div>
@endsection