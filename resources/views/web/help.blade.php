@extends('web.master_layout.master')

@section('title')
   @if(!empty(Session::get('Help'))) {{ Session::get('Help') }} @else Help @endif
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>@if(!empty(Session::get('Help'))) {{ Session::get('Help') }} @else Help @endif</h1>
</div>
<div class="h-backimg m-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!--<ol class="breadcrumb">
                    <li class="breadcrumb-item">Category</li>
                    <li class="breadcrumb-item">Topic</li>
                    <li class="breadcrumb-item">Concept</li>
                </ol>-->
            </div>
        </div>
    </div>
</div>
<div class="question-sec mb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div id="accordion">

                @if(count($faqObj) > 0)
                @foreach($faqObj as $key => $data)
                  <div class="card">
                    <div class="card-header" id="heading{{ $key }}">
                      <h5 class="mb-0 question">
                        <button class="btn btn-link @if($key 
                        != 0) collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                         {{ $data->question }}
                        </button>
                        
                        <a href="javascript:void(0);" class="plusIcon @if($key == 0) @else collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                          <!-- <i class="fa fa-plus" aria-hidden="true"></i> -->
                        </a>
                        
                      </h5>
                    </div>
                
                    <div id="collapse{{ $key }}" class="collapse @if($key == 0)show @endif" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
                      <div class="card-body">
                        {!! $data->answer !!}
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @else
                  <div style="text-align: center;">
          <img src="{{ asset('resources/web-assets/images/blank_img.png') }}">
       
    </div>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection