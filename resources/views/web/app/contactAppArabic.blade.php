@extends('web.app.master_layout.master')

@section('styles')
<link href="{{ asset('resources/web-assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('resources/web-assets/css/rtc.css') }}" rel="stylesheet">
<style type="text/css">
    .changealign{float: left;}
</style>
<link href="{{ asset('resources/web-assets/css/fonts.css') }}" rel="stylesheet">

<link href="{{ asset('resources/web-assets/css/font-awesome.min.css') }} " rel="stylesheet">
<link href="{{ asset('resources/web-assets/css/custom.css') }} " rel="stylesheet">
@show
@stack('styles')

@section('title')
   اتصل بنا -  ديني إسلامي  
@endsection

@section('main-content')

   <div class="sub-header">
        <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
        <h1>
        اتصل بنا
    </h1>
    </div>
    
    <div class="h-backimg text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                &nbsp;
                </div>
            </div>
        </div>
    </div>
    
    <div class="container mb-3">
  <div class="row">
    <div class="col-12">
      <h2 class="title-h2" style="float: right;">نحب هنا منك!</h2>
    </div>
    
    <div class="col-md-8 order-md-1">
      <div class="col-md-12 col-sm-12 col-xs-12">
       
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
        @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
        </div>
        {{ Form::open(array('url' => 'contactAppSendMail' ,'name' => 'frmAskQuestion','id' => '')) }}
        <input type="hidden" name="language_id" value="2">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="firstName"> الاسم الاول </label>
            <input class="form-control" id="firstName" type="text" name="firstName" value="{{ Request::old('firstName') }}">
            <span class="text-danger" >{{ $errors->first('firstName') }}</span>
            
          </div>
          <div class="col-md-6 mb-3">
            <label for="lastName"> الكنية </label>
            <input class="form-control" id="lastName" type="text" name="lastName" value="{{ Request::old('lastName') }}">
            <span class="text-danger" >{{ $errors->first('lastName') }}</span>
            
          </div>
        </div>
        <div class="mb-3">
          <label for="email"> البريد الإلكتروني   </label>
          <input class="form-control" id="email" placeholder="you@example.com" type="email" name="email" value="{{ Request::old('email') }}">
          <span class="text-danger" >{{ $errors->first('email') }}</span>
          
        </div>
        <div class="row">
          <div class="col-md-5 mb-3">
            <label for="PhoneNo"> لا يوجد هاتف </label>
            <input class="form-control PhoneNo" id="PhoneNo"  type="text" name="PhoneNo" value="{{ Request::old('PhoneNo') }}">
            <span class="text-danger" >{{ $errors->first('PhoneNo') }}</span>
            
          </div>
          
        </div>
        <div class="mb-3">
          <label for="Message"> يرجى إدخال تفاصيل استفسارك هنا </label>
          <textarea class="form-control contact" id="Message" rows="5" cols="4" name="message">{{ Request::old('message') }}</textarea>
        </div>
        <input type="submit" name="submit" value="إرسال" class="btn btn-success">
      {{ Form::close() }} 
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('resources/web-assets/js/jquery.inputmask.bundle.js') }}"></script>
<script type="text/javascript">
  $(".PhoneNo").inputmask({ 
  mask: "999999999999999",
  placeholder: ""
});
</script>
@endpush('scripts')