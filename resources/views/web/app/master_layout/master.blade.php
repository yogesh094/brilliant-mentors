<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="#">
        <title>
            @section('title') 
            @show
        </title>
        
        @section('style')
            
        @show
        @stack('styles')
        
        @section('header_section')
            @include('web.app.master_layout.header')
        @show
        @stack('header')

    </head>

    <body>
        @section('main-content')  
        @show

        @section('footer_section')
            <!-- @include('web.app.master_layout.footer') -->
        @show

        @section('script')
        @if (Request::is('video-call'))
        @else
            @include('web.master_layout.script')
            <script type="text/javascript">
                var base_url    = "{{ url('') }}";
                var public_path = "{{ public_path('') }}";
                var selectedLang = "{{ Session::get('selectedLang') }}";
            </script>
        @endif
        @show
        @stack('scripts')
    </body>
</html>