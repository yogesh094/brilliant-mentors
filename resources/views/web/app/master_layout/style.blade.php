<link href="{{ asset('resources/web-assets/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Custom styles for this template -->
<!-- <link href="{{ asset('resources/web-assets/css/carousel.css') }}" rel="stylesheet"> -->

@if(session::get('selectedLang') !== null &&session::get('selectedLang') == 1)
<link href="{{ asset('resources/web-assets/css/style.css') }}" rel="stylesheet">
@elseif(session::get('selectedLang') !== null &&session::get('selectedLang') == 2)
<link href="{{ asset('resources/web-assets/css/rtc.css') }}" rel="stylesheet">
<style type="text/css">
	.changealign{float: left;}
</style>
@else
<link href="{{ asset('resources/web-assets/css/style.css') }}" rel="stylesheet">
@endif


<link href="{{ asset('resources/web-assets/css/fonts.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('resources/web-assets/css/responsive.css') }}" rel="stylesheet"> -->
<link href="{{ asset('resources/web-assets/css/font-awesome.min.css') }} " rel="stylesheet">
<link href="{{ asset('resources/web-assets/css/custom.css') }} " rel="stylesheet">