@extends('web.app.master_layout.master')

@section('styles')
<link href="{{ asset('resources/web-assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('resources/web-assets/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('resources/web-assets/css/fonts.css') }}" rel="stylesheet">

<link href="{{ asset('resources/web-assets/css/font-awesome.min.css') }} " rel="stylesheet">
<link href="{{ asset('resources/web-assets/css/custom.css') }} " rel="stylesheet">
@show
@stack('styles')

@section('title')
    {{ \App\Models\CMS::where('status',1)->where('slug','Privacy-policy')->where('language_id',1)->first()->title }}
@endsection

@section('main-content')

   <div class="sub-header">
        <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
        <h1>
        {{ \App\Models\CMS::where('status',1)->where('slug','Privacy-policy')->where('language_id',1)->first()->title }}
    </h1>
    </div>
    
    <div class="h-backimg text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                &nbsp;
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! \App\Models\CMS::where('status',1)->where('slug','Privacy-policy')->where('language_id',1)->first()->description !!}
                
            </div>
           
        </div>
    </div>

@endsection