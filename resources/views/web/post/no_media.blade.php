<?php 
$description = isset($value['description'])?$value['description']:'';
$first_name = isset($value['first_name'])?$value['first_name']:''; 
$last_name = isset($value['last_name'])?$value['last_name']:''; 
$post_owner = $first_name.' '.$last_name;
 if(isset($value['owner_type']) && $value['owner_type'] == 0) {
		$owner_type = 'Tutor';
	}else {
		$owner_type = 'Student';
	} 
$post_time = isset($value['created_at'])?time_ago($value['created_at']):''; 
$profile_url = URL::to('/user-profile/')."/".$value['user_id'];
?>

<article class="hentry post">
   <div class="post__author author vcard inline-items">
      <img src="{{ asset('resources/web-assets/img/avatar10-sm.jpg') }}" alt="author">
      <div class="author-date">
         <a class="h6 post__author-name fn" href="<?php echo $profile_url; ?>"><?= $post_owner ?></a>
         <div class="post__date">
            <?= $owner_type ?>
         </div>
         <div class="post__date">
            <time class="published" datetime="2004-07-24T18:18">
            <?= $post_time ?>
            </time>
         </div>
      </div>
      <div class="more">
         <svg class="olymp-three-dots-icon">
            <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>
         </svg>
         <ul class="more-dropdown">
            <li>
               <a href="#">Edit Post</a>
            </li>
            <li>
               <a href="#">Delete Post</a>
            </li>
            <!-- <li>
               <a href="#">Turn Off Notifications</a>
               </li>
               <li>
               <a href="#">Select as Featured</a>
               </li> -->
         </ul>
      </div>
   </div>
   <p> <?= $description ?>
   </p>
   <div class="post-additional-info inline-items">
      <a href="#" class="post-add-icon inline-items">
         <svg class="olymp-heart-icon">
            <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use>
         </svg>
         <span>{{ $likeCount }}</span>
      </a>
      <!-- <div class="names-people-likes">
         <a href="#">You</a>, <a href="#">Elaine</a> and
         <br>22 more liked this
         </div> -->
      <div class="comments-shared">
         <a href="#" class="post-add-icon inline-items">
            <svg class="olymp-speech-balloon-icon">
               <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon') }}"></use>
            </svg>
            <span>{{ $commentCount }}</span>
         </a>
         <a href="#" class="post-add-icon inline-items">
            <svg class="olymp-share-icon">
               <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-share-icon') }}"></use>
            </svg>
            <span>24</span>
         </a>
      </div>
   </div>
   <div class="control-block-button post-control-button">
      <a href="#" class="btn btn-control">
         <svg class="olymp-like-post-icon">
            <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-like-post-icon') }}"></use>
         </svg>
      </a>
      <a href="#" class="btn btn-control">
         <svg class="olymp-comments-post-icon">
            <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon') }}"></use>
         </svg>
      </a>
      <a href="#" class="btn btn-control">
         <svg class="olymp-share-icon">
            <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-share-icon') }}"></use>
         </svg>
      </a>
   </div>
</article>
<ul class="comments-list">
   {!! $comment_view !!}

</ul>
<form class="comment-form inline-items">
               
                  <div class="post__author author vcard inline-items">
                     <img src="{{ asset('resources/web-assets/img/author-page.jpg') }}" alt="author">
               
                     <div class="form-group with-icon-right is-empty">
                        <textarea class="form-control" placeholder=""></textarea>
                        <div class="add-options-message">
                           <a href="#" class="options-message" data-toggle="modal" data-target="#update-header-photo">
                              <svg class="olymp-camera-icon">
                                 <use xlink:href="http://localhost/social/resources/web-assets/svg-icons/sprites/icons.svg#olymp-camera-icon">
                                    <svg id="olymp-camera-icon" viewBox="0 0 43 32" width="100%" height="100%">
                                       <title>camera-icon</title>
                                       <path d="M21.333 10.667c-3.927 0-7.111 3.182-7.111 7.111 0 3.927 3.184 7.111 7.111 7.111s7.111-3.184 7.111-7.111c0-3.929-3.184-7.111-7.111-7.111zM21.333 21.337c-1.963 0-3.556-1.593-3.556-3.556s1.593-3.556 3.556-3.556 3.556 1.593 3.556 3.556-1.593 3.556-3.556 3.556zM35.556 3.556h-3.556c0-1.964-1.593-3.556-3.556-3.556h-14.222c-1.963 0-3.556 1.591-3.556 3.556h-3.556c-3.927 0-7.111 3.184-7.111 7.111v14.222c0 3.929 3.184 7.111 7.111 7.111 0 0 6.924 0 15.89 0h0.11v-3.556h-16c-1.963 0-3.556-1.593-3.556-3.556v-14.222c0-1.963 1.593-3.556 3.556-3.556h7.111v-3.556h14.222v3.556h7.111c1.963 0 3.556 1.593 3.556 3.556v14.222c0 1.963-1.593 3.556-3.556 3.556h-1.778v3.556c1.122 0 1.778 0 1.778 0 3.927 0 7.111-3.182 7.111-7.111v-14.222c0-3.927-3.184-7.111-7.111-7.111zM26.667 32h3.556v-3.556h-3.556v3.556z"></path>
                                    </svg>
                                 </use>
                              </svg>
                           </a>
                        </div>
                     <span class="material-input"></span></div>
                  </div>
               
                  <button class="btn btn-md-2 btn-primary">Post Comment</button>
               
                  <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Cancel</button>
               
               </form>



