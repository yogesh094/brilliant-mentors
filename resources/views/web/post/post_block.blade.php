@extends('web.master_layout.master')



@section('title')

    @if(!empty(Session::get('Profile - Timeline'))) {{ Session::get('Profile - Timeline') }} @else Profile - Timeline @endif

@endsection



@section('main-content')







<!-- ... end Top Header-Profile -->

<div class="container">

   <div class="row">

      <!-- Main Content -->

      <main class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">

         <div class="ui-block">

            <!-- News Feed Form  -->

            <div class="news-feed-form">

               <!-- Nav tabs -->

               <ul class="nav nav-tabs" role="tablist">

                  <li class="nav-item">

                     <a class="nav-link active inline-items" data-toggle="tab" href="#home-1" role="tab" aria-expanded="true">

                        <svg class="olymp-status-icon">

                           <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-status-icon') }}">

                              <svg id="olymp-status-icon" viewBox="0 0 36 32" width="100%" height="100%">

                                 <title>status-icon</title>

                                 <path d="M32-0.002h-28.444c-1.963 0-3.556 1.593-3.556 3.557v21.332h3.554v0.004h28.444v-3.557h-28.443v-17.778h28.444v24.889h-24.889v3.556h24.889c1.963 0 3.556-1.593 3.556-3.556v-24.889c0-1.964-1.593-3.557-3.556-3.557zM0 32h3.556v-3.556h-3.556v3.556zM7.109 7.111v3.557h10.667v-3.557h-10.667zM7.109 17.778h21.333v-3.556h-21.333v3.556z"></path>

                              </svg>

                           </use>

                        </svg>

                        <span>Status</span>

                     </a>

                  </li>

                  <li class="nav-item">

                     <a class="nav-link inline-items" data-toggle="tab" href="#profile-1" role="tab" aria-expanded="false">

                        <svg class="olymp-multimedia-icon">

                           <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-multimedia-icon') }}">

                              <svg id="olymp-multimedia-icon" viewBox="0 0 32 32" width="100%" height="100%">

                                 <title>multimedia-icon</title>

                                 <path d="M28.8 0.002h-22.4v3.2h22.4v5.41l-11.57 5.786-7.579-8.496-6.451 5.166v-4.666h-3.2v22.4c0 1.766 1.434 3.198 3.2 3.198h25.6c1.766 0 3.2-1.432 3.2-3.198v-25.602c0-1.766-1.434-3.198-3.2-3.198zM3.2 15.168l6.203-4.963 7.872 8.995h-14.075v-4.032zM28.8 28.802h-25.6v-6.402h25.6v6.402zM28.8 19.2h-7.285l-2.078-2.33 9.363-4.682v7.011zM3.2 0.002h-3.2v3.2h3.2v-3.2zM11.2 24h-3.2v3.2h3.2v-3.2zM17.6 24h-3.2v3.2h3.2v-3.2zM24 24h-3.2v3.2h3.2v-3.2z"></path>

                              </svg>

                           </use>

                        </svg>

                        <span>Multimedia</span>

                     </a>

                  </li>

                  <li class="nav-item">

                     <a class="nav-link inline-items" data-toggle="tab" href="#blog" role="tab" aria-expanded="false">

                        <svg class="olymp-blog-icon">

                           <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-blog-icon') }}">

                              <svg id="olymp-blog-icon" viewBox="0 0 29 32" width="100%" height="100%">

                                 <title>blog-icon</title>

                                 <path d="M25.6 0h-22.4c-1.766 0-3.2 1.434-3.2 3.2v28.8h22.4v-6.4h6.4v-3.2h-9.6v6.4h-16v-25.6h22.4v9.6h3.2v-9.6c0-1.766-1.434-3.2-3.2-3.2zM22.4 6.4h-16v3.2h16v-3.2zM22.4 12.798h-16v3.202h16v-3.202zM6.4 22.4h9.6v-3.2h-9.6v3.2zM25.6 19.2h3.2v-3.2h-3.2v3.2z"></path>

                              </svg>

                           </use>

                        </svg>

                        <span>Blog Post</span>

                     </a>

                  </li>

               </ul>

               <!-- Tab panes -->

               <div class="tab-content">

                  <div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">

                     <form class="form content" role="form" method="POST" id="save-post-form">

                        <div class="author-thumb">

                           <img src="{{ asset('resources/web-assets/img/author-page.jpg') }}" alt="author">

                        </div>

                        <meta type="hidden" name="csrf-token" content="{{csrf_token()}}">

                        <input type="hidden" name="user_id" value = "{{ $user_id }}" >

                        <input type="hidden" name="user_type" value = "{{ $user_type }}" >

                        <div class="form-group with-icon label-floating is-empty">

                           <label class="control-label">Share what you are thinking here...</label>

                           <textarea class="form-control" id="description" name="description" placeholder=""></textarea>

                        </div>

                        <div class="add-options-message">

                           <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">

                              <svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo">

                                 <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-camera-icon') }}"></use>

                              </svg>

                           </a>

                           <!-- <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">

                              <svg class="olymp-computer-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-computer-icon') }}"></use></svg>

                              </a> -->

                           <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">

                              <svg class="olymp-small-pin-icon">

                                 <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-small-pin-icon">

                                    <svg id="olymp-small-pin-icon" viewBox="0 0 25 32" width="100%" height="100%">

                                       <title>small-pin-icon</title>

                                       <path d="M12.444 7.111c-2.946 0-5.333 2.389-5.333 5.333 0 2.946 2.388 5.333 5.333 5.333s5.333-2.386 5.333-5.333c0-2.944-2.388-5.333-5.333-5.333zM12.444 14.222c-0.981 0-1.778-0.796-1.778-1.778s0.796-1.778 1.778-1.778 1.778 0.796 1.778 1.778-0.796 1.778-1.778 1.778z"></path>

                                       <path d="M12.444 0c-4.823 0-8.996 2.891-11.061 7.111h4.194c1.632-2.151 4.087-3.556 6.868-3.556 4.901 0 8.889 4.277 8.889 9.534 0 7.237-6.46 13.865-8.876 15.204-1.838-1.054-6.148-5.36-8.011-10.516h-3.73c2.263 7.817 9.374 14.222 11.728 14.222 2.811 0 12.444-8.951 12.444-18.91 0-7.228-5.573-13.090-12.444-13.090z"></path>

                                       <path d="M0 10.667h3.556v3.556h-3.556v-3.556z"></path>

                                    </svg>

                                 </use>

                              </svg>

                           </a>

                           <button class="btn btn-primary btn-md-2" id="save-post" >Post Status</button>

                           <button   class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>

                        </div>

                     </form>

                  </div>

                  <div class="tab-pane" id="profile-1" role="tabpanel" aria-expanded="true">

                     <form>

                        <div class="author-thumb">

                           <img src="<?php asset('resources/web-assets/img/author-page.jpg') ?>" alt="author">

                        </div>

                        <div class="form-group with-icon label-floating is-empty">

                           <label class="control-label">Share what you are thinking here...</label>

                           <textarea class="form-control" placeholder=""  ></textarea>

                        </div>

                        <div class="add-options-message">

                           <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">

                              <svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo">

                                 <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-camera-icon') }}"></use>

                              </svg>

                           </a>

                           <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">

                              <svg class="olymp-computer-icon">

                                 <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-computer-icon') }}"></use>

                              </svg>

                           </a>

                           <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">

                              <svg class="olymp-small-pin-icon">

                                 <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-small-pin-icon') }}"></use>

                              </svg>

                           </a>

                           <button class="btn btn-primary btn-md-2">Post Status</button>

                           <button   class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>

                        </div>

                     </form>

                  </div>

                  <div class="tab-pane" id="blog" role="tabpanel" aria-expanded="true">

                     <form>

                        <div class="author-thumb">

                           <img src="{{ asset('resources/web-assets/img/author-page.jpg') }}" alt="author">

                        </div>

                        <div class="form-group with-icon label-floating is-empty">

                           <label class="control-label">Share what you are thinking here...</label>

                           <textarea class="form-control" placeholder=""  ></textarea>

                        </div>

                        <div class="add-options-message">

                           <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">

                              <svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo">

                                 <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-camera-icon') }}"></use>

                              </svg>

                           </a>

                           <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">

                              <svg class="olymp-computer-icon">

                                 <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-computer-icon') }}"></use>

                              </svg>

                           </a>

                           <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">

                              <svg class="olymp-small-pin-icon">

                                 <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-small-pin-icon') }}"></use>

                              </svg>

                           </a>

                           <button class="btn btn-primary btn-md-2">Post Status</button>

                           <button   class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>

                        </div>

                     </form>

                  </div>

               </div>

            </div>

            <!-- ... end News Feed Form  -->			

         </div>

         <div id="newsfeed-items-grid">

            <div class="ui-block new-post">

               {!! $post_view !!}

            </div>

         </div>

         <a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid">

            <svg class="olymp-three-dots-icon">

               <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>

            </svg>

         </a>

      </main>

      <!-- ... end Main Content -->

      <!-- Left Sidebar -->
      <?php 
      $username = Auth::user()->first_name." ".Auth::user()->last_name;
      $occupation = Auth::user()->occupation;
      $profile_url = url("user-profile-timeline/")."/".Auth::user()->id; ?>
      <aside class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">

         <div class="ui-block">

            <!-- W-Weather -->

            <div class="widget w-build-fav">

               <div class="widget-thumb profile">

                  <img src="{{ asset('resources/web-assets/img/author-main1.jpg') }}" alt="notebook">

               </div>

               <div class="content">

                  <span>{{ $occupation }}</span>

                  <a href="{{ $profile_url }}" class="h4 title">{{ $username }}</a>

                  <!-- <p><a href="#" class="bold">Click here</a> to start now and get your own fav page up and running! </p> -->

               </div>

            </div>

            <!-- W-Weather -->

         </div>

         <div class="ui-block">

            <div class="ui-block-title">

               <h6 class="title">Activity Feed</h6>

               <a href="#" class="more">

                  <svg class="olymp-three-dots-icon">

                     <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>

                  </svg>

               </a>

            </div>

            <!-- W-Activity-Feed -->

            <ul class="widget w-activity-feed notification-list">

               <li>

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar49-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Marina Polson</a> commented on Jason Mark’s <a href="#" class="notification-link">photo.</a>.

                     <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 mins ago</time></span>

                  </div>

               </li>

               <li>

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar9-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Jake Parker </a> liked Nicholas Grissom’s <a href="#" class="notification-link">status update.</a>.

                     <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">5 mins ago</time></span>

                  </div>

               </li>

               <li>

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar50-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Mary Jane Stark </a> added 20 new photos to her <a href="#" class="notification-link">gallery album.</a>.

                     <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">12 mins ago</time></span>

                  </div>

               </li>

               <li>

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar51-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Nicholas Grissom </a> updated his profile <a href="#" class="notification-link">photo</a>.

                     <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>

                  </div>

               </li>

               <li>

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar48-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Marina Valentine </a> commented on Chris Greyson’s <a href="#" class="notification-link">status update</a>.

                     <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>

                  </div>

               </li>

               <li>

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar52-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Green Goo Rock </a> posted a <a href="#" class="notification-link">status update</a>.

                     <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>

                  </div>

               </li>

               <li>

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar10-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Elaine Dreyfuss  </a> liked your <a href="#" class="notification-link">blog post</a>.

                     <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 hours ago</time></span>

                  </div>

               </li>

               <li>

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar10-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Elaine Dreyfuss  </a> commented on your <a href="#" class="notification-link">blog post</a>.

                     <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 hours ago</time></span>

                  </div>

               </li>

               <li>

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar53-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Bruce Peterson </a> changed his <a href="#" class="notification-link">profile picture</a>.

                     <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">15 hours ago</time></span>

                  </div>

               </li>

            </ul>

            <!-- .. end W-Activity-Feed -->

         </div>

      </aside>

      <!-- ... end Left Sidebar -->

      <!-- Right Sidebar -->

      <aside class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">

         <div class="ui-block">

            <div class="ui-block-title">

               <h6 class="title">Students Suggestions</h6>

               <a href="#" class="more">

                  <svg class="olymp-three-dots-icon">

                     <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>

                  </svg>

               </a>

            </div>

            <!-- W-Action -->

            <ul class="widget w-friend-pages-added notification-list friend-requests">

               <li class="inline-items">

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar38-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Francine Smith</a>

                     <span class="chat-message-item">8 Friends in Common</span>

                  </div>

                  <span class="notification-icon">

                     <a href="#" class="accept-request">

                        <span class="icon-add without-text">

                           <svg class="olymp-happy-face-icon">

                              <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use>

                           </svg>

                        </span>

                     </a>

                  </span>

               </li>

               <li class="inline-items">

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar39-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Hugh Wilson</a>

                     <span class="chat-message-item">6 Friends in Common</span>

                  </div>

                  <span class="notification-icon">

                     <a href="#" class="accept-request">

                        <span class="icon-add without-text">

                           <svg class="olymp-happy-face-icon">

                              <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use>

                           </svg>

                        </span>

                     </a>

                  </span>

               </li>

               <li class="inline-items">

                  <div class="author-thumb">

                     <img src="{{ asset('resources/web-assets/img/avatar40-sm.jpg') }}" alt="author">

                  </div>

                  <div class="notification-event">

                     <a href="#" class="h6 notification-friend">Karen Masters</a>

                     <span class="chat-message-item">6 Friends in Common</span>

                  </div>

                  <span class="notification-icon">

                     <a href="#" class="accept-request">

                        <span class="icon-add without-text">

                           <svg class="olymp-happy-face-icon">

                              <use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use>

                           </svg>

                        </span>

                     </a>

                  </span>

               </li>

            </ul>

            <!-- ... end W-Action -->

         </div>

      </aside>

      <!-- ... end Right Sidebar -->

   </div>

</div>

<!-- Popup -->



{!! $header_image_upload_popup !!}



<!-- Popup -->



@endsection

@push('scripts')

<script type="text/javascript">

$.ajaxSetup({

  headers: {

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

  }

});

$(document).on('click','#save-post',function (e){

        e.preventDefault();

        var token = $('meta[name="csrf-token"]').attr('content');

			$.ajax({

            type: "POST",

            dataType: "json",

            url: "{{route('save-post')}}",

        	data: $("#save-post-form").serialize(),

            cache: false,

            success: function (data) {

            	if(data.status == 200){

            		$(".new-post").prepend(data.post_view);

            		$('#description').val('');



            	}

            	

              

            }

        });         

    });



	$(document).on('click','#upload_link',function (e){

		    e.preventDefault();

		    $("#upload:hidden").trigger('click');

		});





    // $('.media_upload').on("change", function (evt) {

    //     var error = null;



    //     var errorCount = 0;

    //     var restaurantId = $('#user_id').val();

    //     var token = $('#token').val();



    //  //   var numFiles = $("input:file", this)[0].files.length;

    //     var numFiles = $("#photo_upload").get(0).files.length;



    //     parseInt(numFiles)+parseInt(photo_count);

    //     var photo_count = $("#photo_count").val();



    //     var photo_count =parseInt(numFiles)+parseInt(photo_count);



    //     if(parseInt(photo_count) <= 12){



    //         var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");





    //         if (regex.test(this.value.toLowerCase())) {

    //             var reader = new FileReader();



    //                 var formData = new FormData($("#happyhour")[0]);

    //                 $.ajax({

    //                     url: baseUrl + "/happyhour/saveImage/"+business_id+'/'+restaurantId,

    //                     type: 'POST',

    //                     data: formData,

    //                     processData: false,

    //                     contentType: false,

    //                     dataType: 'json',

    //                     success: function (data)

    //                     {

    //                         $(".pre_media_image_happy").html('');

    //                         var imageHtml = '<div class="col-md-12 col-sm-12 col-xs-12" style="position:relative;margin-top:10px;margin-left:8%;width:100% !important">';

    //                         for(i = 0; i < (data.imagePath).length; i++) { 



    //                             parts = data.imagePath[i].split("/"),

    //                             last_part = parts[parts.length-1];



    //                             image_id = last_part.split(".")[0];



    //                             //image_id = image_id.substring(0, image_id.indexOf(".") - 1);



    //                             imageHtml += '<div class="col-md-3 happy_div" id="image_'+image_id+'">';

    //                             imageHtml += '<span class="phto_area_ph_v"><image class="happyhourImage phto_area_happy_photo img-responsive phto_area_img_ph_v" src="'+data.imagePath[i]+'"></span>';

    //                             imageHtml += '<span style="position:absolute;" class="close_bbl-image" data-imageName= "'+last_part+'"></span>';

    //                             imageHtml += '</div>'; 



    //                         }

    //                         $('#description').focus();

    //                         imageHtml += '</div>';

    //                         $(".pre_media_image_happy").html(imageHtml); 

    //                         $("#photo_count").val(parseInt((data.imagePath).length)); 



    //                         $('.pic_counter').html(parseInt((data.imagePath).length));



    //                     },

    //                     error: function (data)

    //                     {

    //                         console.log(data);

    //                     }

    //                 });

                            

    //             }else {

    //             $('.media_upload').val();

    //             alert("Please select a valid image (.png, .jpeg, .jpg).");

    //             return false;

    //         }



    //     }else{



    //             alert("Max limit 12.");

    //             return false;

    //     }

    // });





    // $(document).on('click','.close_bbl-image',function (e){

    //     e.preventDefault();



    //     var business_id = $('#business_id').val();

    //     var restaurantId = $('#rest_id').val();

    //     var happyId = $('#happy_uid').val();

        

    //     var baseUrl = $('#base_url').val();

    //     var token = $('#token').val();



    //     var image_name =$(this).attr('data-imageName');

        

    //     $.ajax({

    //         url: baseUrl + "/happyhour/removeImage/"+business_id+'/'+restaurantId,

    //         dataType: "json",

    //         type: "POST",

    //         data:{'image_name': image_name ,'happyId' : happyId ,_token: token},

    //         success: function (data) {

    //             if (data.status == '200') {



    //                 image_id = data.image_name.split(".")[0];



    //                 var photo_count = $("#photo_count").val();



    //                 var photo_count =parseInt(photo_count)- 1;

    //                 $("#photo_count").val(parseInt(photo_count));

                     

    //                 $('.pic_counter').html(parseInt(photo_count));

    //                 $('#image_'+image_id).remove();



    //             }else {

    //                 alert('Something went wrong, please refresh page and try again');

    //             }

    //         }

    //     });

    // });





</script>

@endpush('scripts')