@extends('web.master_layout.master')

@section('title')
    @if(!empty(Session::get('Hobbies/Interests'))) {{ Session::get('Hobbies/Interests') }} @else Hobbies/Interests @endif
@endsection

@section('main-content')
<!-- Main Header Account -->
<div class="main-header">
  <div class="content-bg-wrap bg-account"></div>
  <div class="container">
    <div class="row">
      <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
        <div class="main-header-content">
          <h1>Your Account Dashboard</h1>
          <p>Welcome to your account dashboard! Here you’ll find everything you need to change your profile
  information, settings, read notifications and requests, view your latest messages, change your pasword and much
  more! Also you can create or manage your own favourite page, have fun!</p>
        </div>
      </div>
    </div>
  </div>
  <img class="img-bottom" src="{{ asset('resources/web-assets/img/account-bottom.png') }}" alt="friends">
</div>

<!-- ... end Main Header Account -->


<!-- Your Account Personal Information -->

<div class="container">
	<div class="row">
		<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Hobbies and Interests</h6>
				</div>
				<div class="ui-block-content">

					
					<!-- Form Hobbies and Interests -->
					
					<form>
						<div class="row">
					
							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">
								<div class="form-group label-floating">
									<label class="control-label">Hobbies</label>
									<textarea class="form-control" placeholder=""  >I like to ride the bike to work, swimming, and working out. I also like reading design magazines, go to museums, and binge watching a good tv show while it’s raining outside.
													</textarea>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Favourite TV Shows</label>
									<textarea class="form-control" placeholder=""  >Breaking Good, RedDevil, People of Interest, The Running Dead, Found,  American Guy.</textarea>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Favourite Movies</label>
									<textarea class="form-control" placeholder=""  >Idiocratic, The Scarred Wizard and the Fire Crown,  Crime Squad, Ferrum Man. </textarea>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Favourite Games</label>
									<textarea class="form-control" placeholder=""  >The First of Us, Assassin’s Squad, Dark Assylum, NMAK16, Last Cause 4, Grand Snatch Auto. </textarea>
								</div>
					
								<button class="btn btn-secondary btn-lg full-width">Cancel</button>
							</div>
					
							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">
								<div class="form-group label-floating">
									<label class="control-label">Favourite Music Bands / Artists</label>
									<textarea class="form-control" placeholder=""  >Iron Maid, DC/AC, Megablow, The Ill, Kung Fighters, System of a Revenge.</textarea>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Favourite Books</label>
									<textarea class="form-control" placeholder=""  >The Crime of the Century, Egiptian Mythology 101, The Scarred Wizard, Lord of the Wings, Amongst Gods, The Oracle, A Tale of Air and Water.</textarea>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Favourite Writers</label>
									<textarea class="form-control" placeholder=""  >Martin T. Georgeston, Jhonathan R. Token, Ivana Rowle, Alexandria Platt, Marcus Roth. </textarea>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Other Interests</label>
									<textarea class="form-control" placeholder=""  >Swimming, Surfing, Scuba Diving, Anime, Photography, Tattoos, Street Art.</textarea>
								</div>
					
								<button class="btn btn-primary btn-lg full-width">Save all Changes</button>
							</div>
					
						</div>
					</form>
					
					<!-- ... end Form Hobbies and Interests -->

				</div>
			</div>
		</div>

		<div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">
			<div class="ui-block">
		{!! $profile_sidebar !!}
			</div>
		</div>
	</div>
</div>

<!-- ... end Your Account Personal Information -->



@endsection
@push('scripts')
<script type="text/javascript">

$(document).on('click','#add-education',function (e){
        e.preventDefault();

			$.ajax({
            type: "GET",
            url: "{{route('education-block')}}",
            cache: false,
            success: function (data) {
               $(".education-block").append(data.content);
            }
        });         
    });

  $(document).ready(function(){
    var id = $("select[name=state]").val();
        var selected = $('#dist_selected').val();
        $.ajax({
            type: "GET",
            url: "{{route('selectDistrict')}}",
            data: {id: id,selected:selected},
            cache: false,
            success: function (data) {
                $('#district').html(data);
            }
        });
    });    
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 32 && (charCode < 46 || charCode > 57) || (charCode == 32)) {
            alert("Please enter only digits");
            return false;
        }
        return true;
    }

$("input[name='phone_number']").on("keyup paste", function () {
            // Remove invalid chars from the input
            var input = this.value.replace(/[^0-9\(\)\s\-]/g, "");
            var inputlen = input.length;
            // Get just the numbers in the input
            var numbers = this.value.replace(/\D/g, '');
            var numberslen = numbers.length;
            // Value to store the masked input
            var newval = "";
            
            $(this).val(numbers.substring(0, 14));
        });

    function changeState(id) {
        $.ajax({
            type: "GET",
            url: "{{route('selectDistrict')}}",
            data: {id: id},
            cache: false,
            success: function (data) {
                $('#district').html(data);
            }
        });             
    }
</script>
@endpush('scripts')