@extends('web.master_layout.master')

@section('title')
    @if(!empty(Session::get('pl_ask_question'))) {{ Session::get('pl_ask_question') }} @else Ask a Question @endif
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>@if(!empty(Session::get('ask_now'))) {{ Session::get('ask_now') }} @else Ask Now @endif</h1>
</div>
<div class="h-backimg text-center m-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                </div>
            </div>
        </div>
    </div>
    <div class="question-sec
366">
 <div class="container">

        <div class="row">
            <div class="col-12">
                <h2 class="title-h2">{{ \App\Models\CMS::where('status',1)->where('slug','ask_question')->where('language_id',Session()->get('selectedLang'))->first()->title }}</h2>
                <p>{!! \App\Models\CMS::where('status',1)->where('slug','ask_question')->where('language_id',Session()->get('selectedLang'))->first()->description !!}</p>

      <div class="col-md-12 col-sm-12 col-xs-12" id="ajaxerror">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ $errors->first() }}.
                <br/>
            </div>
        @endif
        @if (Session::has('message'))
          <div class="alert alert-info">{!! Session::get('message') !!}</div>
          @endif
          @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
        @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
        </div>

          <!-- {{ Form::open(array('url' => 'storeQuestion' ,'name' => 'frmAskQuestion','id' => '')) }} -->
            <div class="form-group type-question">
            <label for="question">@if(!empty(Session::get('type_question'))) {{ Session::get('type_question') }} @else Type your Question @endif</label>
            
            <textarea class="form-control contact" id="question" name="question" rows="5" cols="4">{{ Session::get('textQuestion') }}</textarea>
            
          </div>
          <button name="submit" class="btn btn-success" onclick="suggestedQuestion();" id="btnHide">
            @if(!empty(Session::get('submit_question'))) {{ Session::get('submit_question') }} @else Submit Your Question @endif
          </button>
          <!-- <input type="Submit" name="Submit" value="@if(!empty(Session::get('submit_question'))) {{ Session::get('submit_question') }} @else Submit Your Question @endif" class="btn btn-success"> -->
              <!--  {{ Form::close() }} -->
              <hr>
            </div>
          <div id="showquestion"></div>

            <div class="col-12 mt-3">
        <!-- <nav aria-label="Page navigation example">
          <ul class="pagination">
          <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
            </a>
          </li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
            </a>
          </li>
          </ul>
        </nav> -->
      </div>
        </div>
    </div>
  </div>
@endsection
@push('scripts')
<script type="text/javascript">
 $('.alert').fadeTo( 3000, 0 );
</script>
@endpush('scripts')
