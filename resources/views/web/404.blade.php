@extends('web.master_layout.master')

@section('title')
    404
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>404</h1>
</div>
<div class="h-backimg text-center m-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                </div>
            </div>
        </div>
    </div>
 <div class="container mb-3">

        <div class="row">
            <div class="col-12">
                

      <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ $errors->first() }}.
                <br/>
            </div>
        @endif
        @if (Session::has('message'))
          <div class="alert alert-info">{!! Session::get('message') !!}</div>
          @endif
          @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
        @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
        </div>

            <h1 class="h1404">Page Not Found</h1>
            </div>
        </div>
    </div>
@endsection