@extends('web.master_layout.master')

@section('title')
    @if(!empty(Session::get('Profile - Timeline'))) {{ Session::get('Profile - Timeline') }} @else Profile - Timeline @endif
@endsection

@section('main-content')



<!-- ... end Top Header-Profile -->
<div class="container">
	<div class="row">

		<!-- Main Content -->

		<main class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">

			<div class="ui-block">
				
				<!-- News Feed Form  -->
				
				<div class="news-feed-form">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active inline-items" data-toggle="tab" href="#home-1" role="tab" aria-expanded="true">
				
								<svg class="olymp-status-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-status-icon') }}">
									<svg id="olymp-status-icon" viewBox="0 0 36 32" width="100%" height="100%">
									<title>status-icon</title>
									<path d="M32-0.002h-28.444c-1.963 0-3.556 1.593-3.556 3.557v21.332h3.554v0.004h28.444v-3.557h-28.443v-17.778h28.444v24.889h-24.889v3.556h24.889c1.963 0 3.556-1.593 3.556-3.556v-24.889c0-1.964-1.593-3.557-3.556-3.557zM0 32h3.556v-3.556h-3.556v3.556zM7.109 7.111v3.557h10.667v-3.557h-10.667zM7.109 17.778h21.333v-3.556h-21.333v3.556z"></path>
									</svg>
								</use></svg>
				
								<span>Status</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link inline-items" data-toggle="tab" href="#profile-1" role="tab" aria-expanded="false">
				
								<svg class="olymp-multimedia-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-multimedia-icon') }}">
									<svg id="olymp-multimedia-icon" viewBox="0 0 32 32" width="100%" height="100%">
									<title>multimedia-icon</title>
									<path d="M28.8 0.002h-22.4v3.2h22.4v5.41l-11.57 5.786-7.579-8.496-6.451 5.166v-4.666h-3.2v22.4c0 1.766 1.434 3.198 3.2 3.198h25.6c1.766 0 3.2-1.432 3.2-3.198v-25.602c0-1.766-1.434-3.198-3.2-3.198zM3.2 15.168l6.203-4.963 7.872 8.995h-14.075v-4.032zM28.8 28.802h-25.6v-6.402h25.6v6.402zM28.8 19.2h-7.285l-2.078-2.33 9.363-4.682v7.011zM3.2 0.002h-3.2v3.2h3.2v-3.2zM11.2 24h-3.2v3.2h3.2v-3.2zM17.6 24h-3.2v3.2h3.2v-3.2zM24 24h-3.2v3.2h3.2v-3.2z"></path>
									</svg>
								</use></svg>
				
								<span>Multimedia</span>
							</a>
						</li>
				
						<li class="nav-item">
							<a class="nav-link inline-items" data-toggle="tab" href="#blog" role="tab" aria-expanded="false">
								<svg class="olymp-blog-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-blog-icon') }}">
									<svg id="olymp-blog-icon" viewBox="0 0 29 32" width="100%" height="100%">
									<title>blog-icon</title>
									<path d="M25.6 0h-22.4c-1.766 0-3.2 1.434-3.2 3.2v28.8h22.4v-6.4h6.4v-3.2h-9.6v6.4h-16v-25.6h22.4v9.6h3.2v-9.6c0-1.766-1.434-3.2-3.2-3.2zM22.4 6.4h-16v3.2h16v-3.2zM22.4 12.798h-16v3.202h16v-3.202zM6.4 22.4h9.6v-3.2h-9.6v3.2zM25.6 19.2h3.2v-3.2h-3.2v3.2z"></path>
									</svg>
								</use></svg>
				
								<span>Blog Post</span>
							</a>
						</li>
					</ul>
				
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">
							 <form class="form content" role="form" method="POST" id="save-post-form">
								<div class="author-thumb">
									<img src="{{ asset('resources/web-assets/img/author-page.jpg') }}" alt="author">
								</div>
								<meta type="hidden" name="csrf-token" content="{{csrf_token()}}">
								<input type="hidden" name="user_id" value = "{{ $user_id }}" >
								<input type="hidden" name="user_type" value = "{{ $user_type }}" >
								
								<div class="form-group with-icon label-floating is-empty">
									<label class="control-label">Share what you are thinking here...</label>
									<textarea class="form-control" name="description" placeholder=""></textarea>
								</div>
								<div class="add-options-message">
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">
										<svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-camera-icon') }}"></use></svg>
									</a>
									<!-- <a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
										<svg class="olymp-computer-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-computer-icon') }}"></use></svg>
									</a> -->
				
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">
										<svg class="olymp-small-pin-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-small-pin-icon">
											<svg id="olymp-small-pin-icon" viewBox="0 0 25 32" width="100%" height="100%">
											<title>small-pin-icon</title>
											<path d="M12.444 7.111c-2.946 0-5.333 2.389-5.333 5.333 0 2.946 2.388 5.333 5.333 5.333s5.333-2.386 5.333-5.333c0-2.944-2.388-5.333-5.333-5.333zM12.444 14.222c-0.981 0-1.778-0.796-1.778-1.778s0.796-1.778 1.778-1.778 1.778 0.796 1.778 1.778-0.796 1.778-1.778 1.778z"></path>
											<path d="M12.444 0c-4.823 0-8.996 2.891-11.061 7.111h4.194c1.632-2.151 4.087-3.556 6.868-3.556 4.901 0 8.889 4.277 8.889 9.534 0 7.237-6.46 13.865-8.876 15.204-1.838-1.054-6.148-5.36-8.011-10.516h-3.73c2.263 7.817 9.374 14.222 11.728 14.222 2.811 0 12.444-8.951 12.444-18.91 0-7.228-5.573-13.090-12.444-13.090z"></path>
											<path d="M0 10.667h3.556v3.556h-3.556v-3.556z"></path>
											</svg>
										</use></svg>
									</a>
				
									<button class="btn btn-primary btn-md-2" id="save-post" >Post Status</button>
									<button   class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>
				
								</div>
				
							</form>
						</div>
				
						<div class="tab-pane" id="profile-1" role="tabpanel" aria-expanded="true">
							<form>
								<div class="author-thumb">
								<img src="<?php asset('resources/web-assets/img/author-page.jpg') ?>" alt="author">
								</div>
								<div class="form-group with-icon label-floating is-empty">
									<label class="control-label">Share what you are thinking here...</label>
									<textarea class="form-control" placeholder=""  ></textarea>
								</div>
								<div class="add-options-message">
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">
										<svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-camera-icon') }}"></use></svg>
									</a>
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
										<svg class="olymp-computer-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-computer-icon') }}"></use></svg>
									</a>
				
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">
										<svg class="olymp-small-pin-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-small-pin-icon') }}"></use></svg>
									</a>
				
									<button class="btn btn-primary btn-md-2">Post Status</button>
									<button   class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>
				
								</div>
				
							</form>
						</div>
				
						<div class="tab-pane" id="blog" role="tabpanel" aria-expanded="true">
							<form>
								<div class="author-thumb">
									<img src="{{ asset('resources/web-assets/img/author-page.jpg') }}" alt="author">
								</div>
								<div class="form-group with-icon label-floating is-empty">
									<label class="control-label">Share what you are thinking here...</label>
									<textarea class="form-control" placeholder=""  ></textarea>
								</div>
								<div class="add-options-message">
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">
										<svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-camera-icon') }}"></use></svg>
									</a>
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
										<svg class="olymp-computer-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-computer-icon') }}"></use></svg>
									</a>
				
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">
										<svg class="olymp-small-pin-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-small-pin-icon') }}"></use></svg>
									</a>
				
									<button class="btn btn-primary btn-md-2">Post Status</button>
									<button   class="btn btn-md-2 btn-border-think btn-transparent c-grey">Preview</button>
				
								</div>
				
							</form>
						</div>
					</div>
				</div>
				
				<!-- ... end News Feed Form  -->			</div>

			<div id="newsfeed-items-grid">

				<div class="ui-block">
					
					<article class="hentry post video">
					
						<div class="post__author author vcard inline-items">
							<img src="{{ asset('resources/web-assets/img/avatar7-sm.jpg') }}" alt="author">
					
							<div class="author-date">
								<a class="h6 post__author-name fn" href="#">Marina Valentine</a> shared a <a href="#">link</a>
								<div class="post__date">
									<time class="published" datetime="2004-07-24T18:18">
										March 4 at 2:05pm
									</time>
								</div>
							</div>
					
							<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg>
								<ul class="more-dropdown">
									<li>
										<a href="#">Edit Post</a>
									</li>
									<li>
										<a href="#">Delete Post</a>
									</li>
									<!-- <li>
										<a href="#">Turn Off Notifications</a>
									</li>
									<li>
										<a href="#">Select as Featured</a>
									</li> -->
								</ul>
							</div>
					
						</div>
					
						<p>Hey <a href="#">Cindi</a>, you should really check out this new song by Iron Maid. The next time they come to the city we should totally go!</p>
					
						<div class="post-video">
							<div class="video-thumb">
								<img src="{{ asset('resources/web-assets/img/video-youtube1.jpg') }}" alt="photo">
								<a href="https://youtube.com/watch?v=excVFQ2TWig" class="play-video">
									<svg class="olymp-play-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-play-icon') }}"></use></svg>
								</a>
							</div>
					
							<div class="video-content">
								<a href="#" class="h4 title">Iron Maid - ChillGroves</a>
								<p>Lorem ipsum dolor sit amet, consectetur ipisicing elit, sed do eiusmod tempor incididunt
									ut labore et dolore magna aliqua...
								</p>
								<a href="#" class="link-site">YOUTUBE.COM</a>
							</div>
						</div>
					
						<div class="post-additional-info inline-items">
					
							<a href="#" class="post-add-icon inline-items">
								<svg class="olymp-heart-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-heart-icon') }}">
									<svg id="olymp-heart-icon" viewBox="0 0 36 32" width="100%" height="100%">
									<title>heart-icon</title>
									<path d="M23.111 21.333h3.556v3.556h-3.556v-3.556z"></path>
									<path d="M32.512 2.997c-2.014-2.011-4.263-3.006-7.006-3.006-2.62 0-5.545 2.089-7.728 4.304-2.254-2.217-5.086-4.295-7.797-4.295-2.652 0-4.99 0.793-6.937 2.738-4.057 4.043-4.057 10.599 0 14.647 1.157 1.157 12.402 13.657 12.402 13.657 0.64 0.638 1.481 0.958 2.32 0.958s1.678-0.32 2.318-0.958l1.863-2.012-2.523-2.507-1.655 1.787c-2.078-2.311-11.095-12.324-12.213-13.442-1.291-1.285-2-2.994-2-4.811 0-1.813 0.709-3.518 2-4.804 1.177-1.175 2.54-1.698 4.425-1.698 0.464 0 2.215 0.236 5.303 3.273l2.533 2.492 2.492-2.532c2.208-2.242 4.201-3.244 5.196-3.244 1.769 0 3.113 0.588 4.496 1.97 1.289 1.284 1.998 2.99 1.998 4.804 0 1.815-0.709 3.522-1.966 4.775-0.087 0.085-0.098 0.094-1.9 2.041l-0.156 0.167 2.523 2.51 0.24-0.26c0 0 1.742-1.881 1.774-1.911 4.055-4.043 4.055-10.603-0.002-14.644z"></path>
									</svg>
								</use></svg>
								<span>18</span>
							</a>
					
							<ul class="friends-harmonic">
								<li>
									<a href="#">
										<img src="{{ asset('resources/web-assets/img/friend-harmonic9.jpg') }}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('resources/web-assets/img/friend-harmonic10.jpg') }}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('resources/web-assets/img/friend-harmonic7.jpg') }}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('resources/web-assets/img/friend-harmonic8.jpg') }}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('resources/web-assets/img/friend-harmonic11.jpg') }}" alt="friend">
									</a>
								</li>
							</ul>
					
							<div class="names-people-likes">
								<a href="#">Jenny</a>, <a href="#">Robert</a> and
								<br>18 more liked this
							</div>
					
							<div class="comments-shared">
								<a href="#" class="post-add-icon inline-items">
									<svg class="olymp-speech-balloon-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon') }}">
										<svg id="olymp-speech-balloon-icon" viewBox="0 0 35 32" width="100%" height="100%">
										<title>speech-balloon-icon</title>
										<path d="M32 0h-28.8c-1.768 0-3.2 1.434-3.2 3.2v16c0 1.766 1.434 3.2 3.2 3.2 0 0 0.6 0 1.6 0v-3.2h-1.6v-16h28.8v19.2c1.766 0 3.2-1.434 3.2-3.2v-16c0-1.766-1.434-3.2-3.2-3.2zM20.8 22.4h1.6v-3.2h-1.6v3.2zM25.6 22.4h3.2v-3.2h-3.2v3.2zM27.2 6.4h-20.8v3.2h20.8v-3.2zM6.4 16h12.8v-3.2h-12.8v3.2z"></path>
										<path d="M17.6 19.2l-9.6 6.626v-6.626h-3.2v12.8l12.8-9.6h3.2v-3.2z"></path>
										</svg>
									</use></svg>
					
									<span>0</span>
								</a>
					
								<a href="#" class="post-add-icon inline-items">
									<svg class="olymp-share-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-share-icon') }}"></use></svg>
					
									<span>16</span>
								</a>
							</div>
					
					
						</div>
					
						<div class="control-block-button post-control-button">
					
							<a href="#" class="btn btn-control">
								<svg class="olymp-like-post-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-like-post-icon') }}">
									<svg id="olymp-like-post-icon" viewBox="0 0 37 32" width="100%" height="100%">
										<title>like-post-icon</title>
										<path d="M33.449 2.994c-2.066-2.009-4.377-3.006-7.191-3.006-2.69 0-5.691 2.089-7.934 4.306-2.311-2.217-5.221-4.295-8.002-4.295-2.722 0-5.122 0.793-7.12 2.736-4.165 4.043-4.165 10.601 0 14.649 1.189 1.154 12.729 13.655 12.729 13.655 0.656 0.64 1.52 0.96 2.379 0.96 0.862 0 1.721-0.32 2.382-0.96l1.726-1.815-3.237-3.234-0.857 0.905c-3.122-3.381-10.862-11.744-11.936-12.789-1.122-1.090-1.739-2.528-1.737-4.050 0-1.52 0.617-2.955 1.739-4.046 1.024-0.997 2.238-1.44 3.931-1.44 0.777 0 2.512 0.791 4.839 3.022l3.214 3.081 3.163-3.131c2.215-2.19 4.037-2.985 4.718-2.985 1.573 0 2.77 0.512 4.007 1.714 1.12 1.088 1.737 2.523 1.737 4.046s-0.617 2.958-1.669 3.982c-0.13 0.121-0.135 0.123-2.037 2.123l-0.187 0.199 3.23 3.237 0.046-0.048 0.242-0.258c0 0 1.79-1.881 1.824-1.911 4.162-4.043 4.162-10.603 0-14.647z"></path>
										<path d="M22.889 20.578h4.571v4.571h-4.571v-4.571z"></path>
									</svg>
								</use></svg>
							</a>
					
							<a href="#" class="btn btn-control">
								<svg class="olymp-comments-post-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-comments-post-icon') }}">
									<svg id="olymp-comments-post-icon" viewBox="0 0 36 32" width="100%" height="100%">
									<title>comments-post-icon</title>
									<path d="M32 0h-28c-2.21 0-4 1.792-4 4v18c0 2.208 1.792 4 4 4 0 0 0.75 0 2 0v-4h-2v-18h28v22c2.208 0 4-1.792 4-4v-18c0-2.208-1.792-4-4-4zM18 26h2v-4h-2v4zM24 26h4v-4h-4v4zM8 12h20v-4h-20v4zM8 18h12v-4h-12v4z"></path>
									<path d="M18 22l-8 4.282v-4.282h-4v10l12-6v-4z"></path>
									</svg>
								</use></svg>
							</a>
					
							<a href="#" class="btn btn-control">
								<svg class="olymp-share-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-share-icon') }}">
									<svg id="olymp-share-icon" viewBox="0 0 40 32" width="100%" height="100%">
									<title>share-icon</title>
									<path d="M11.168 16.788v3.046h9.132v3.048h-9.132v3.072l-6.095-4.584 6.095-4.582zM14.216 10.683l-14.216 10.685 14.216 10.688v-6.129h9.132v-9.144h-9.132v-6.1z"></path>
									<path d="M15.739 6.095h3.048v3.048h-3.048v-3.048z"></path>
									<path d="M35.854 8.25l-10.973-8.25v6.1h-3.048v3.049h6.095v-3.046l6.095 4.582-6.095 4.582v-3.072h-12.18v3.048h9.132v6.129l14.214-10.686z"></path>
									</svg>
								</use></svg>
							</a>
					
						</div>
					
					</article>
				</div>


			</div>

			<a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid"><svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg></a>

		</main>

		<!-- ... end Main Content -->


		<!-- Left Sidebar -->
		<?php $profile_url = url("user-profile-timeline/")."/".Auth::user()->id; ?>
		<aside class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">
			<div class="ui-block">
				
				<!-- W-Weather -->
				
				<div class="widget w-build-fav">
				
					<div class="widget-thumb profile">
						<img src="{{ asset('resources/web-assets/img/author-main1.jpg') }}" alt="notebook">
					</div>
				
					<div class="content">
						<span>Project Manager1</span>
						<a href="{{ $profile_url }}" class="h4 title">Yogesh Khandelwal</a>
						<!-- <p><a href="#" class="bold">Click here</a> to start now and get your own fav page up and running! </p> -->
					</div>
				</div>
				
				<!-- W-Weather -->
			</div>

			<div class="ui-block">

				<div class="ui-block-title">
					<h6 class="title">Activity Feed</h6>
					<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg></a>
				</div>

				
				<!-- W-Activity-Feed -->
				
				<ul class="widget w-activity-feed notification-list">
					<li>
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar49-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Marina Polson</a> commented on Jason Mark’s <a href="#" class="notification-link">photo.</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 mins ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar9-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Jake Parker </a> liked Nicholas Grissom’s <a href="#" class="notification-link">status update.</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">5 mins ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar50-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Mary Jane Stark </a> added 20 new photos to her <a href="#" class="notification-link">gallery album.</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">12 mins ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar51-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Nicholas Grissom </a> updated his profile <a href="#" class="notification-link">photo</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>
						</div>
					</li>
					<li>
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar48-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Marina Valentine </a> commented on Chris Greyson’s <a href="#" class="notification-link">status update</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar52-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Green Goo Rock </a> posted a <a href="#" class="notification-link">status update</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">1 hour ago</time></span>
						</div>
					</li>
					<li>
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar10-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Elaine Dreyfuss  </a> liked your <a href="#" class="notification-link">blog post</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 hours ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar10-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Elaine Dreyfuss  </a> commented on your <a href="#" class="notification-link">blog post</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">2 hours ago</time></span>
						</div>
					</li>
				
					<li>
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar53-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Bruce Peterson </a> changed his <a href="#" class="notification-link">profile picture</a>.
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">15 hours ago</time></span>
						</div>
					</li>
				
				</ul>
				
				<!-- .. end W-Activity-Feed -->
			</div>		
		</aside>

		<!-- ... end Left Sidebar -->


		<!-- Right Sidebar -->

		<aside class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">

			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Students Suggestions</h6>
					<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg></a>
				</div>

				
				
				<!-- W-Action -->
				
				<ul class="widget w-friend-pages-added notification-list friend-requests">
					<li class="inline-items">
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar38-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Francine Smith</a>
							<span class="chat-message-item">8 Friends in Common</span>
						</div>
						<span class="notification-icon">
							<a href="#" class="accept-request">
								<span class="icon-add without-text">
									<svg class="olymp-happy-face-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>
								</span>
							</a>
						</span>
					</li>
				
					<li class="inline-items">
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar39-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Hugh Wilson</a>
							<span class="chat-message-item">6 Friends in Common</span>
						</div>
						<span class="notification-icon">
							<a href="#" class="accept-request">
								<span class="icon-add without-text">
									<svg class="olymp-happy-face-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>
								</span>
							</a>
						</span>
					</li>
				
					<li class="inline-items">
						<div class="author-thumb">
							<img src="{{ asset('resources/web-assets/img/avatar40-sm.jpg') }}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">Karen Masters</a>
							<span class="chat-message-item">6 Friends in Common</span>
						</div>
						<span class="notification-icon">
							<a href="#" class="accept-request">
								<span class="icon-add without-text">
									<svg class="olymp-happy-face-icon"><use xlink:href="{{ asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>
								</span>
							</a>
						</span>
					</li>
				
				</ul>
				
				<!-- ... end W-Action -->
			</div>

		</aside>

		<!-- ... end Right Sidebar -->

	</div>
</div>

<!-- Popup -->

{!! $header_image_upload_popup !!}

<!-- Popup -->

@endsection
@push('scripts')
<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$(document).on('click','#save-post',function (e){
        e.preventDefault();
        var token = $('meta[name="csrf-token"]').attr('content');
			$.ajax({
            type: "POST",
            dataType: "json",
            url: "{{route('save-post')}}",
        	data: $("#save-post-form").serialize(),
            cache: false,
            success: function (data) {
            	if(data.status == 200){
            		$('#success-model').modal('show');
            	}
            	
              
            }
        });         
    });

	$(document).on('click','#upload_link',function (e){
		    e.preventDefault();
		    $("#upload:hidden").trigger('click');
		});


    // $('.media_upload').on("change", function (evt) {
    //     var error = null;

    //     var errorCount = 0;
    //     var restaurantId = $('#user_id').val();
    //     var token = $('#token').val();

    //  //   var numFiles = $("input:file", this)[0].files.length;
    //     var numFiles = $("#photo_upload").get(0).files.length;

    //     parseInt(numFiles)+parseInt(photo_count);
    //     var photo_count = $("#photo_count").val();

    //     var photo_count =parseInt(numFiles)+parseInt(photo_count);

    //     if(parseInt(photo_count) <= 12){

    //         var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");


    //         if (regex.test(this.value.toLowerCase())) {
    //             var reader = new FileReader();

    //                 var formData = new FormData($("#happyhour")[0]);
    //                 $.ajax({
    //                     url: baseUrl + "/happyhour/saveImage/"+business_id+'/'+restaurantId,
    //                     type: 'POST',
    //                     data: formData,
    //                     processData: false,
    //                     contentType: false,
    //                     dataType: 'json',
    //                     success: function (data)
    //                     {
    //                         $(".pre_media_image_happy").html('');
    //                         var imageHtml = '<div class="col-md-12 col-sm-12 col-xs-12" style="position:relative;margin-top:10px;margin-left:8%;width:100% !important">';
    //                         for(i = 0; i < (data.imagePath).length; i++) { 

    //                             parts = data.imagePath[i].split("/"),
    //                             last_part = parts[parts.length-1];

    //                             image_id = last_part.split(".")[0];

    //                             //image_id = image_id.substring(0, image_id.indexOf(".") - 1);

    //                             imageHtml += '<div class="col-md-3 happy_div" id="image_'+image_id+'">';
    //                             imageHtml += '<span class="phto_area_ph_v"><image class="happyhourImage phto_area_happy_photo img-responsive phto_area_img_ph_v" src="'+data.imagePath[i]+'"></span>';
    //                             imageHtml += '<span style="position:absolute;" class="close_bbl-image" data-imageName= "'+last_part+'"></span>';
    //                             imageHtml += '</div>'; 

    //                         }
    //                         $('#description').focus();
    //                         imageHtml += '</div>';
    //                         $(".pre_media_image_happy").html(imageHtml); 
    //                         $("#photo_count").val(parseInt((data.imagePath).length)); 

    //                         $('.pic_counter').html(parseInt((data.imagePath).length));

    //                     },
    //                     error: function (data)
    //                     {
    //                         console.log(data);
    //                     }
    //                 });
                            
    //             }else {
    //             $('.media_upload').val();
    //             alert("Please select a valid image (.png, .jpeg, .jpg).");
    //             return false;
    //         }

    //     }else{

    //             alert("Max limit 12.");
    //             return false;
    //     }
    // });


    // $(document).on('click','.close_bbl-image',function (e){
    //     e.preventDefault();

    //     var business_id = $('#business_id').val();
    //     var restaurantId = $('#rest_id').val();
    //     var happyId = $('#happy_uid').val();
        
    //     var baseUrl = $('#base_url').val();
    //     var token = $('#token').val();

    //     var image_name =$(this).attr('data-imageName');
        
    //     $.ajax({
    //         url: baseUrl + "/happyhour/removeImage/"+business_id+'/'+restaurantId,
    //         dataType: "json",
    //         type: "POST",
    //         data:{'image_name': image_name ,'happyId' : happyId ,_token: token},
    //         success: function (data) {
    //             if (data.status == '200') {

    //                 image_id = data.image_name.split(".")[0];

    //                 var photo_count = $("#photo_count").val();

    //                 var photo_count =parseInt(photo_count)- 1;
    //                 $("#photo_count").val(parseInt(photo_count));
                     
    //                 $('.pic_counter').html(parseInt(photo_count));
    //                 $('#image_'+image_id).remove();

    //             }else {
    //                 alert('Something went wrong, please refresh page and try again');
    //             }
    //         }
    //     });
    // });


</script>
@endpush('scripts')