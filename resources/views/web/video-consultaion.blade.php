@extends('web.master_layout.master')

@section('title')
@if(!empty(Session::get('Video Consultation'))) {{ Session::get('Video Consultation') }} @else Video Consultation @endif
@endsection

@section('main-content')

<div class="sub-header"> 
  @if($videoConsultaionBanner != '')
  <img src="{{ url('public/uploads/slider') }}/{{ $videoConsultaionBanner->value }}" class="img-fluid w-100" alt="Banner">
  @else
  <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  @endif
  
  <h1>@if(!empty(Session::get('Video Consultation'))) {{ Session::get('Video Consultation') }} @else Video Consultation @endif</h1>
</div>
<div class="h-backimg text-center m-0">
  <div class="container">
    <div class="row">
      <div class="col-12">
      </div>
    </div>
  </div>
</div>
<div class="container mb-3">
  <div class="row">
    <div class="col-12">
      <h2 class="title-h2">{{ \App\Models\CMS::where('status',1)->where('slug','video-consultaion')->where('language_id',Session()->get('selectedLang'))->first()->title }}</h2>
      <p>{!! \App\Models\CMS::where('status',1)->where('slug','video-consultaion')->where('language_id',Session()->get('selectedLang'))->first()->description !!}</p>
    </div>
    <div class="col-md-12 order-md-1">
      <div class="col-md-12 col-sm-12 col-xs-12">

        @if (Session::has('message'))
        <div class="alert alert-info">{!! Session::get('message') !!}</div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger">{!! Session::get('error') !!}</div>
        @endif
        @if (Session::has('success'))
        <div class="alert alert-success">{!! Session::get('success') !!}</div>
        @endif
      </div>
      {{ Form::open(array('url' => 'videoConsultaionStore' ,'name' => 'frmBookAppointment','id' => '')) }}
      <input type="hidden" name="category_id" value="@if(Request::old('category_id') != ''){{ Request::old('category_id') }}@else{{ Session::get('category_id') }}@endif" id="categoryId">
      
      <div class="col-md-12 mb-3 catagory">
        <label for="category">@if(!empty(Session::get('pl_select_category'))) {{ Session::get('pl_select_category') }} @else Select Category @endif</label>
        <div class="row">
          @if(count($Category) > 0)
          @foreach($Category as $key => $Categorys)
          <div class="col-md-3">
            <div class="card card-1 min_height @if(Request::old('category_id') == $Categorys->id || Session::get('category_id') == $Categorys->id)select1 @endif" id="category{{ $Categorys->id }}" onclick="selectCategory('{{ $Categorys->id }}');">
              <h2>{{ $Categorys->category }}<br>{{ Cache::get('currency') }}{{ $Categorys->charges }}</h2>

            </div>
          </div>
          @endforeach
          @endif

        </div>
        <span class="text-danger" >{{ $errors->first('category_id') }}</span>
      </div>
      <div class="row">
        <div class="col-md-6 mb-3">
          <label for="selectdate">@if(!empty(Session::get('Select Date'))) {{ Session::get('Select Date') }} @else Select Date @endif</label>
          <select class="custom-select d-block w-100" name="booking_date" id="booking_date">

            @foreach($date['date'] as $key => $dates)
            <option value="{{ $dates }}" @if(Session::get('booking_date') == $dates) selected="" @endif>{{ $dates }}</option>
            @endforeach
          </select>

          <!-- <input class="form-control" id="selectdate" placeholder="" value="" type="text" name="booking_date"> -->
        </div>
        <div class="col-md-6 mb-3">
          <label for="startTime">@if(!empty(Session::get('Select Time'))) {{ Session::get('Select Time') }} @else Select Time @endif</label>

          <select class="custom-select d-block w-100" id="startTime" name="start_time">

            @if(count($getTime) > 0)
            @foreach($getTime as $key => $value)
            <option value="{{ $key }}" @if(Session::get('startTime') == $key) selected="" @endif>{{ $value }}</option>
            @endforeach
            @endif
          </select>
        </div>

      </div>

      <div class="mb-3">
        <label for="Message">@if(!empty(Session::get('app_message'))) {{ Session::get('app_message') }} @else Please enter details of your query here @endif</label>
        <textarea class="form-control contact" id="Message" rows="5" cols="4" name="description">@if($errors->has('description')){{ Request::old('description') }}@endif {{ Session::get('description') }}</textarea>
        <span class="text-danger" >{{ $errors->first('description') }}</span>
      </div>
      <div class="mb-3">
        <label for="Message">
          <input type="checkbox" name="term_condition" id="term_condition" value="1" required @if(Session::get('term_condition') == 1) checked="checked" @endif >@if(!empty(Session::get('Term-Condition'))) {{ Session::get('Term-Condition') }} @else Term &amp; Condition @endif</label>
          <br>
          <span class="text-danger" >{{ $errors->first('term_condition') }}</span>
          
        </div>

        <button class="btn btn-success" type="submit">@if(!empty(Session::get('book_appointment'))) {{ Session::get('book_appointment') }} @else Book Your Appointment @endif</button>
        {{ Form::close() }} 
      </div>
    </div>
  </div>




  <div class="modal fade c-popup" id="termConditionModel" tabindex="-1" role="dialog" aria-labelledby="termConditionModelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="termConditionModelLabel">{{ \App\Models\CMS::where('status',1)->where('slug','term-condition')->where('language_id',Session()->get('selectedLang'))->first()->title }}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        </div>
        <div class="modal-body">
          <div class="container">
            {!! \App\Models\CMS::where('status',1)->where('slug','term-condition')->where('language_id',Session()->get('selectedLang'))->first()->description !!}
          </div>
        </div>
        <div class="modal-footer">

        </div>
      </div>
    </div>
  </div>
  @endsection
  @push('styles')
  <link href="{{ asset('resources/web-assets/css/jquery-ui.css') }}" rel="stylesheet">
  <link href="{{ asset('resources/web-assets/css/jquery.timepicker.min.css') }}" rel="stylesheet">
  @endpush('styles')
  @push('scripts')
  <script src="{{ asset('resources/web-assets/js/jquery-ui.js') }}"></script>
  <script src="{{ asset('resources/web-assets/js/jquery.timepicker.min.js') }}"></script>
  <script type="text/javascript">

    $("#term_condition").click(function(){
      if($("#term_condition").prop("checked") == true){
        $("#termConditionModel").modal('show');
      }else{
        $("#termConditionModel").modal('hide');
      }
    });


    var booking_date = $("#booking_date").val();
    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: $("#urlBase").val()+"/checkTimeConsultaion",
      data: {
        bookingDate: booking_date,
      },
      success: function (data) {
                //alert(data);
                $('#startTime').html(data);
              },
              error: function (result) {
               // alert("Error chk");
             }
           });
    $('#booking_date').change(function() {
     var booking_date = this.value;
     $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: $("#urlBase").val()+"/checkTimeConsultaion",
      data: {
        bookingDate: booking_date,
      },
      success: function (data) {
                //alert(data);
                $('#startTime').html(data);
              },
              error: function (result) {
               // alert("Error chk");
             }
           });
   })
    $( function() {
    ///$( "#selectdate" ).datepicker('setDate', 'today');

    var currentDate = new Date();
    $('#selectdate').datepicker({
      inline: true,
      showOtherMonths: true,
      dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      dateFormat: 'dd/mm/yy'
    });
    $("#selectdate").datepicker("setDate", currentDate);
  });

</script>
@endpush('scripts')