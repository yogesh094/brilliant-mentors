@extends('web.master_layout.master')

@section('title')
   @if(!empty(Session::get('Question'))) {{ Session::get('Question') }} @else Question @endif
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>@if(!empty(Session::get('pl_search'))) {{ Session::get('pl_search') }} @else Search @endif</h1>
</div>
<div class="h-backimg text-center m-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                </div>
            </div>
        </div>
    </div>
    <div class="question-sec
366">
  <div class="container">
      <div class="row">
      <div class="col-12 mb-3">
          <div class="form-row align-items-center" style="margin-left: 3px;">
            <div class="col-sm-2 my-1">
            <label class="" for="category">@if(!empty(Session::get('Select Search Filter'))) {{ Session::get('Select Search Filter') }} @else Select Search Filter @endif</label>
          </div>
          
          <div class="col-sm-2 my-1">
            <label class="sr-only" for="category">@if(!empty(Session::get('pl_select_category'))) {{ Session::get('pl_select_category') }} @else Category @endif</label>
            <select id="category" class="form-control">
            <option value="">@if(!empty(Session::get('pl_select_category'))) {{ Session::get('pl_select_category') }} @else Select Category @endif</option>
            <option value="">@if(!empty(Session::get('all'))) {{ Session::get('all') }} @else All @endif</option>
            @if(count($Category) > 0)
            @foreach($Category as $key => $value)
            <option value="{{ $value['id'] }}">{{ $value['category'] }}</option>
            @endforeach
            @endif
            </select>
          </div>
               <div class="col-sm-2 my-1">
            <label class="sr-only" for="SubCategory">@if(!empty(Session::get('SubCategory'))) {{ Session::get('SubCategory') }} @else Sub Category @endif</label>
            <select id="SubCategory" class="form-control" onchange="showSearchResult()">
            <option value="0">@if(!empty(Session::get('SubCategory'))) {{ Session::get('SubCategory') }} @else Sub Category @endif</option>
            <!-- <option value="0">@if(!empty(Session::get('None'))) {{ Session::get('None') }} @else None @endif</option> -->
            </select>
          </div>
          <div class="col-sm-2 my-1">
            <label class="sr-only" for="topic"></label>
            <select id="searchType" class="form-control" onchange="showSearchResult()">
            <option value="">@if(!empty(Session::get('Select Type'))) {{ Session::get('Select Type') }} @else Select Type @endif</option>
            <option value="">@if(!empty(Session::get('all'))) {{ Session::get('all') }} @else All @endif</option>
            <option value="0">@if(!empty(Session::get('Latest'))) {{ Session::get('Latest') }} @else Latest @endif</option>
            <option value="1">@if(!empty(Session::get('Popular'))) {{ Session::get('Popular') }} @else Popular @endif</option>
            <option value="2">@if(!empty(Session::get('Seasonal'))) {{ Session::get('Seasonal') }} @else Seasonal @endif</option>
            </select>
          </div>
          <div class="col-sm-3 my-1">
            
  <input type="search" class="form-control" id="Searchbytext" placeholder="@if(!empty(Session::get('pl_search_by_text'))) {{ Session::get('pl_search_by_text') }} @else Search By Text @endif">
  <span id="searchclear" style="display: none;">X</span>

            <!-- <label class="sr-only" for="Searchbytext">@if(!empty(Session::get('pl_search_by_text'))) {{ Session::get('pl_search_by_text') }} @else Search By Question @endif</label>
            <input type="text" class="form-control" id="Searchbytext" placeholder="@if(!empty(Session::get('pl_search_by_text'))) {{ Session::get('pl_search_by_text') }} @else Search By Text @endif"><button class="close-icon" type="reset" ></button> -->
          </div>
          <div class="col-sm-1 my-1">
            <button type="submit" class="btn btn-primary blue" onclick="showSearchResult();">@if(!empty(Session::get('pl_search_result'))) {{ Session::get('pl_search_result') }} @else Search @endif</button>
          </div>
           <!-- <div class="col-sm-1 my-1">
            <button type="submit" class="btn btn-primary blue changealign" onClick="window.location.reload()">@if(!empty(Session::get('Cancel'))) {{ Session::get('Cancel') }} @else Cancel @endif</button>
            <img src="{{ asset('resources/web-assets/images/cancel.jpeg') }}" onClick="window.location.reload()" style="width: 35px;cursor: pointer;" class="changealign">
          </div> -->
          </div>
      </div>
        <div class="col-12">
          <div id="showSearchResult">
            <div id="accordion">
      @if(count($question) > 0)
                @foreach($question as $key => $questions)
                @if($questions->answer != '')
                  <div class="card">
                    <div class="card-header" id="heading{{ $key }}">
                      <h5 class="mb-0 question">
                        <button class="btn btn-link @if($key 
                        != 0) collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                         {{ $questions->question }}
                        </button>
                        <a href="javascript:void(0);" class="plusIcon @if($key == 0) @else collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                          <!-- <i class="fa fa-plus" aria-hidden="true"></i> -->
                        </a>
                      </h5>
                    </div>
                
                    <div id="collapse{{ $key }}" class="collapse @if($key == 0)show @endif" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
                      <div class="card-body">
<!--                        <small><i>By: {{ \App\Models\User::where('id',$questions->expert_id)->first()->username }}</i></small><br>
                <small><i>day: {{ Carbon\Carbon::parse($questions->updated_at)->format('F d,Y (g:i A)') }}</i></small>
                <br>-->
                      @if(strlen(strip_tags($questions->answer)) > 400)
                        {!! str_limit($questions->answer,400) !!}
                        <br>
                        <div class="read-more-question">
                          <a href="{{ url('question-answer') }}/{{ $questions->id }}">@if(!empty(Session::get('Read More'))) {{ Session::get('Read More') }} @else Read More @endif</a>
                        </div>
                      @else
                        {!! $questions->answer !!}
                      @endif
                      </div>

                    </div>
                  </div>
                  @endif
                  @endforeach
                  @else
                  <div style="text-align: center;">
                    <img src="{{ asset('resources/web-assets/images/blank_img.png') }}">
                  <h2 >
        @if(!empty(Session::get('Your question list is empty!!'))) {{ Session::get('Your question list is empty!!') }} @else Your question list is empty!! @endif
      </h2>
                </div>
                  @endif
          </div>
          <div class="col-12 mt-3">
        <nav aria-label="Page navigation example">
        {{ $question->links() }}
        </nav>
      </div>
          </div>
        </div>
      <div class="col-12 mt-3">
      </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script type="text/javascript">
  $(document).ready(function(){
    $('#category').on('change', function (e) {
            var id = $(this).find(":selected").val();
            $.ajax({
                type : 'get',
                url:  "{{ url('/getParentCategory') }}",
                data :  'id='+ id, //Pass $idSub Category
                success : function(response){
                    var $select = $('#SubCategory');
                    $select.find('option').remove();
                    $select.append('<option value="0">Sub Category</option>');
                    $select.append('<option value="0">All</option>');
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value['id'] + '>' + value['category'] + '</option>'); // return empty
                    });
                showSearchResult();
                }
            }); 
         });
});
</script>
@endpush('scripts')