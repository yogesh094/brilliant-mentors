<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('resources/web-assets/images/fav-icon.png') }}">
        <title>
           Video call
        </title>
         @section('style')

            <link href="{{ asset('resources/web-assets/css/bootstrap.min.css') }}" rel="stylesheet">
            @if(!empty(Session::get('selectedLang')) &&Session::get('selectedLang') == 1)
<link href="{{ asset('resources/web-assets/css/style.css') }}" rel="stylesheet">
@elseif(!empty(Session::get('selectedLang')) &&Session::get('selectedLang') == 2)
<link href="{{ asset('resources/web-assets/css/rtc.css') }}" rel="stylesheet">
@else
<link href="{{ asset('resources/web-assets/css/style.css') }}" rel="stylesheet">
@endif
            <link href="{{ asset('resources/web-assets/css/fonts.css') }}" rel="stylesheet">
            <link href="{{ asset('resources/web-assets/css/font-awesome.min.css') }} " rel="stylesheet">
            <link href="{{ asset('resources/web-assets/css/custom.css') }} " rel="stylesheet">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://cdn.rawgit.com/una/CSSgram/master/source/css/cssgram.css">
            <link rel="stylesheet" href="{{ asset('resources/admin-assets/webrtc/styles.css') }}">
<style type="text/css">
    body {
        font-family:'gt_walsheim_proregular' !important;
    }
    .dropdown-menu{
        top:25%;
    }
    .navbar-divider, .navbar-nav .nav-item+.nav-item, .navbar-nav .nav-link+.nav-link{
        margin-left: 0px;
    }
    .navbar-light .navbar-nav .nav-link{
        color: rgba(0,0,0,.5);
    }
.modal-header .close{
    margin-top:-50px;
}
.c-popup .modal-dialog{
    max-width: 35%;
}
</style>
        @show
        @stack('styles')
        </head>
    <body>
        @section('header_section')
            @include('web.master_layout.header')
        @show
        @stack('header')
        @section('main-content')  
        @show

        @section('script')
        
        @show
        @stack('scripts')
    </body>
</html>