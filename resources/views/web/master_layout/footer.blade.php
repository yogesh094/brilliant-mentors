<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <ul class="f-link">
                    <li><a href="{{ url('/home') }}">@if(!empty(Session::get('Home'))) {{ Session::get('Home') }} @else Home @endif</a></li>
                    <li><a href="{{ url('aboutus') }}">@if(!empty(Session::get('about_us'))) {{ Session::get('about_us') }} @else About us @endif</a></li>
                    <li><a href="{{ url('question-search') }}">@if(!empty(Session::get('Question'))) {{ Session::get('Question') }} @else Question @endif</a></li>
                    <li><a href="{{ url('ask-question') }}">@if(!empty(Session::get('ask_now'))) {{ Session::get('ask_now') }} @else Ask Now @endif</a></li>
                    <li><a href="{{ url('video-consultaion') }}">@if(!empty(Session::get('Video Consultation'))) {{ Session::get('Video Consultation') }} @else Video Consultation @endif</a></li>
                    <li><a href="{{ url('contactus') }}">@if(!empty(Session::get('web_contact_us'))) {{ Session::get('web_contact_us') }} @else Contact @endif</a></li>
                     <li><a href="{{ url('privacy-policy') }}">@if(!empty(Session::get('privacy-policy'))) {{ Session::get('privacy-policy') }} @else Privacy policy @endif</a></li>
                     <li><a href="{{ url('help') }}">@if(!empty(Session::get('Help'))) {{ Session::get('Help') }} @else Help @endif</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-2">
                <ul class="f-social">
                    <li><a href="{{ Cache::get('facebook') }}"><img src="{{ asset('resources/web-assets/images/fb.png') }}" class="img-fluid" alt="Facebook"></a></li>
                    <li><a href="{{ Cache::get('twitter') }}"><img src="{{ asset('resources/web-assets/images/twtr.png') }}" class="img-fluid" alt="Twitter"></a></li>
                    <li><a href="{{ Cache::get('google') }}"><img src="{{ asset('resources/web-assets/images/ggl+.png') }}" class="img-fluid" alt="Google"></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12"> <span class="copyright">&copy;  @if(!empty(Session::get('footer_text'))) {{ Session::get('footer_text') }} @else Copyright islamhub.com 2018 All rights reserved. @endif </span> </div>
        </div>
    </div>
</div>
<input type="hidden" value="{{ url('/') }}" id="urlBase" name="urlBase">
@push('scripts')
    @if (Session::has('checklogin'))
    @if(Session::get('checklogin') == 'login')
    <script type="text/javascript">
    $(document).ready(function(){
     $("#Login").modal('show');
    });

</script>
    @endif
    @endif
@endpush('scripts')