<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <link rel="icon" href="{{ asset('resources/web-assets/images/fav-icon.png') }}">
        <title>
            @section('title') 
            @show
            - @if(!empty(Session::get('Brilliant Mentors'))) {{ Session::get('Brilliant Mentors') }} @else Brilliant Mentors @endif
        </title>
        
        @section('style')
            @include('web.master_layout.style')
        @show
        @stack('styles')
    </head>

    <body class="landing-page">
        @if(!(Route::getCurrentRoute()->uri() == '/' || Route::getCurrentRoute()->uri() == 'login'))
        @section('header')
            @include('web.master_layout.header')
        @show
        @stack('header')
        @endif

        @section('main-content')  
        @show

        @section('script')
         @include('web.master_layout.script')
        @show
        @stack('scripts')
    </body>
</html>