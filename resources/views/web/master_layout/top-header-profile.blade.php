<div class="container">
  <div class="row">
    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="ui-block">
        <div class="top-header">
          <div class="top-header-thumb">
            <img src="<?php echo asset('resources/web-assets/img/top-header1.jpg') ?>" width="100%" height="auto" alt="User Profile Photo">
          </div>
          <div class="profile-section">
            <div class="row">
              <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
                <ul class="profile-menu">
                  <li>
                    <?php 
                    $profile_url = url("user-profile-timeline/")."/".Auth::user()->id;
                    $user_profile_url = url("user-profile/")."/".Auth::user()->id;

                    ?>
                    <a href="{{ $profile_url }}" class="{{ (Request::segment('1') == 'user-profile-timeline') ? 'active' : '' }}">Timeline</a>
                  </li>
                  <li>
                    <a href="{{ $user_profile_url }}" class="{{ (Request::segment('1') == 'user-profile') ? 'active' : '' }}">About</a>
                  </li>
                  <li>
                    <a href="#">My Networks</a>
                  </li>
                </ul>
              </div>
              <div class="col col-lg-5 ml-auto col-md-5 col-sm-12 col-12">
                <ul class="profile-menu">
                  <li>
                    <a href="#">Photos</a>
                  </li>
                  <!-- <li>
                    <a href="#">Videos</a>
                  </li> -->
                  <!-- <li>
                    <div class="more">
                      <svg class="olymp-three-dots-icon"><use xlink:href="<?php echo asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon') ?>"></use></svg>
                      <ul class="more-dropdown more-with-triangle">
                        <li>
                          <a href="#">Report Profile</a>
                        </li>
                        <li>
                          <a href="#">Block Profile</a>
                        </li>
                      </ul>
                    </div>
                  </li> -->
                </ul>
              </div>
            </div>
            
            <div class="control-block-button">
              <a href="#" class="btn btn-control bg-blue">
                <svg class="olymp-happy-face-icon"><use xlink:href="<?php echo asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-happy-face-icon') ?>"></use></svg>
              </a>

              <a href="#" class="btn btn-control bg-purple">
                <svg class="olymp-chat---messages-icon"><use xlink:href="<?php echo asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-chat---messages-icon') ?>"></use></svg>
              </a>
              @if($userData["id"] == Auth::user()->id)
              <div class="btn btn-control bg-primary more">
                <svg class="olymp-settings-icon"><use xlink:href="<?php echo asset('resources/web-assets/svg-icons/sprites/icons.svg#olymp-settings-icon') ?>"></use></svg>

                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                  <li>
                    <a href="#" class="options-message" id="upload-profile">Upload Profile Photo</a>
                    <input type="file" name="profile-photo" id="profile-photo" style="display: none;">
                  </li>
                  <li>
                    <a href="#" class="options-message" id="upload-profile-bg">Upload Timeline Photo</a>
                    <input type="file" name="profile-bg-photo" id="profile-bg-photo" style="display: none;">
                  </li>
                  <!--  <li>
                    <a href="29-YourAccount-AccountSettings.html">Account Settings</a>
                  </li> -->
                </ul>
              </div>
              @endif
            </div>
            
          </div>
          <div class="top-header-author">
            <a href="#" class="author-thumb">
              <img src="<?php echo asset('resources/web-assets/img/author-main1.jpg') ?>" width="100%" height="auto" alt="author">
            </a>
            <div class="author-content">
               <?php $first_name = isset($userData['first_name'])?$userData['first_name']:''; 
                    $last_name = isset($userData['last_name'])?$userData['last_name']:'';
               ?>
              <a href="#" class="h4 author-name"><?= $first_name ?> <?= $last_name ?></a>
              <div class="country">{{ Auth::user()->occupation }}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>