

<!-- JS Scripts -->

<script src="{{ asset('resources/web-assets/js/jquery-3.2.1.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/jquery.appear.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/jquery.mousewheel.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/perfect-scrollbar.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/jquery.matchHeight.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/svgxuse.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/imagesloaded.pkgd.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/Headroom.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/velocity.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/ScrollMagic.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/jquery.waypoints.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/jquery.countTo.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/popper.min.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/material.min.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/bootstrap-select.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/smooth-scroll.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/selectize.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/swiper.jquery.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/moment.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/daterangepicker.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/simplecalendar.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/fullcalendar.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/isotope.pkgd.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/ajax-pagination.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/Chart.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/chartjs-plugin-deferred.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/circle-progress.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/loader.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/run-chart.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/jquery.magnific-popup.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/jquery.gifplayer.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/mediaelement-and-player.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/mediaelement-playlist-plugin.min.js') }}"></script>



<script src="{{ asset('resources/web-assets/js/base-init.js') }}"></script>

<script defer src="{{ asset('resources/web-assets/fonts/fontawesome-all.js') }}"></script>



<script src="{{ asset('resources/web-assets/Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

<script src="{{ asset('resources/web-assets/js/custom.js') }}"></script>
