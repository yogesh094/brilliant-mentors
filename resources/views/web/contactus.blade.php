@extends('web.master_layout.master')

@section('title')
    @if(!empty(Session::get('web_contact_us'))) {{ Session::get('web_contact_us') }} @else Contact Us @endif
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
  <h1>@if(!empty(Session::get('menu_contact'))) {{ Session::get('menu_contact') }} @else Contact @endif</h1>
</div>
<div class="h-backimg text-center m-0">
  <div class="container">
    <div class="row">
      <div class="col-12"> </div>
    </div>
  </div>
</div>
<div class="container mb-3">
  <div class="row">
    <div class="col-12">
      <h2 class="title-h2">@if(!empty(Session::get('contact_title'))) {{ Session::get('contact_title') }} @else We'd love to here from you! @endif</h2>
    </div>
    <div class="col-md-4 order-md-2 mb-4">
      <address>
            {!! $address !!}
        </address>
        <address>
        <strong>Email</strong><br>
        <a href="mailto:{{ $tomail }}">{{ $tomail }}</a>
      </address>
    </div>
    <div class="col-md-8 order-md-1">
      <div class="col-md-12 col-sm-12 col-xs-12">
        @if (Session::has('message'))
          <div class="alert alert-info">{!! Session::get('message') !!}</div>
          @endif
          @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
        @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
        </div>
        {{ Form::open(array('url' => 'contactSendMail' ,'name' => 'frmAskQuestion','id' => '')) }}
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="firstName">@if(!empty(Session::get('first_name'))) {{ Session::get('first_name') }} @else First name @endif</label>
            <input class="form-control" id="firstName" type="text" name="firstName" value="{{ Request::old('firstName') }}">
            <span class="text-danger" >{{ $errors->first('firstName') }}</span>
            
          </div>
          <div class="col-md-6 mb-3">
            <label for="lastName">@if(!empty(Session::get('last_name'))) {{ Session::get('last_name') }} @else Last name @endif</label>
            <input class="form-control" id="lastName" type="text" name="lastName" value="{{ Request::old('lastName') }}">
            <span class="text-danger" >{{ $errors->first('lastName') }}</span>
            
          </div>
        </div>
        <div class="mb-3">
          <label for="email">@if(!empty(Session::get('email'))) {{ Session::get('email') }} @else Email @endif </label>
          <input class="form-control" id="email" placeholder="you@example.com" type="email" name="email" value="{{ Request::old('email') }}">
          <span class="text-danger" >{{ $errors->first('email') }}</span>
          
        </div>
        <div class="row">
          <div class="col-md-5 mb-3">
            <label for="country">@if(!empty(Session::get('Phone No'))) {{ Session::get('Phone No') }} @else Phone No @endif</label>
            <!-- <select class="custom-select d-block w-100" id="country" required>
              <option value="">Choose...</option>
              <option>United States</option>
            </select> -->
            <input class="form-control PhoneNo" id="PhoneNo"  type="text" name="PhoneNo" value="{{ Request::old('PhoneNo') }}">
            <span class="text-danger" >{{ $errors->first('PhoneNo') }}</span>
            
          </div>
        </div>
        <div class="mb-3">
          <label for="Message">@if(!empty(Session::get('app_message'))) {{ Session::get('app_message') }} @else Message @endif</label>
          <textarea class="form-control contact" id="Message" rows="5" cols="4" name="message">{{ Request::old('message') }}</textarea>
        </div>
        <input type="submit" name="submit" value="@if(!empty(Session::get('send'))) {{ Session::get('send') }} @else Send @endif" class="btn btn-success">
      {{ Form::close() }} 
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('resources/web-assets/js/jquery.inputmask.bundle.js') }}"></script>
<script type="text/javascript">
  $(".PhoneNo").inputmask({ 
  mask: "999999999999999",
  placeholder: ""
});
</script>
@endpush('scripts')
