
<div class="col-12">
  
        <h2 class="title-h2">@if(!empty(Session::get('pl_suggested_ans'))) {{ Session::get('pl_suggested_ans') }} @else Suggested Answers @endif</h2>
        <p>@if(!empty(Session::get('suggested_description'))) {{ Session::get('suggested_description') }} @else Following are suggested answers that may relate to your question. @endif</p>

<div id="accordion">
      @if(count($question) > 0)
                @foreach($question as $key => $questions)
                @if($questions->answer != '')
                  <div class="card">
                    <div class="card-header" id="heading{{ $key }}">
                      <h5 class="mb-0 question">
                        <button class="btn btn-link @if($key 
                        != 0) collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                         {{ $questions->question }}
                        </button>
                        <a href="javascript:void(0);" class="plusIcon @if($key == 0) @else collapsed @endif" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="@if($key == 0)true @else false @endif" aria-controls="collapse{{ $key }}">
                          <!-- <i class="fa fa-plus" aria-hidden="true"></i> -->
                        </a>
                      </h5>
                    </div>
                
                    <div id="collapse{{ $key }}" class="collapse @if($key == 0)show @endif" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
                      <div class="card-body">
<!--                        @if(isset($questions->expert_id) && $questions->expert_id > 0)
                        <small><i>By: {{ \App\Models\User::where('id',$questions->expert_id)->first()->username }}</i></small><br>
                <small><i>day: {{ Carbon\Carbon::parse($questions->updated_at)->format('F d,Y (g:i A)') }}</i></small>
                <br>
                @endif-->
                          @if(strlen(strip_tags($questions->answer)) > 400)

                        {!! str_limit($questions->answer,400) !!}
                         <br>
                        <div class="read-more-question">
                          <a href="{{ url('question-answer') }}/{{ $questions->id }}">@if(!empty(Session::get('Read More'))) {{ Session::get('Read More') }} @else Read More @endif</a>
                        </div>
                      @else
                        {!! $questions->answer !!}
                      @endif
                      </div>
                    </div>
                  </div>
                  @endif
                  @endforeach

                  @else
                <div class="notfound" style="text-align: center;">
                  @if(!empty(Session::get('suggested_recode'))) {{ Session::get('suggested_recode') }} @else Not Suggested Answers @endif
                </div>

                  @endif
   <hr>
{{ Form::open(array('url' => 'storeQuestion' ,'name' => 'frmAskQuestion','id' => '')) }}
<input type="hidden" name="question" value="{{ $newQuestion }}">
<input type="Submit" name="Submit" value="@if(!empty(Session::get('submit_question'))) {{ Session::get('submit_question') }} @else Submit Your Question @endif" class="btn btn-success">

{{ Form::close() }}
 
</div>
</div>