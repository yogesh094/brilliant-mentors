@extends('web.master_layout.master')

@section('title')
    {{ \App\Models\CMS::where('status',1)->where('slug','Privacy-policy')->where('language_id',Session()->get('selectedLang'))->first()->title }}
@endsection

@section('main-content')
   <div class="sub-header">
        <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
        <h1>
        {{ \App\Models\CMS::where('status',1)->where('slug','Privacy-policy')->where('language_id',Session()->get('selectedLang'))->first()->title }}
    </h1>
    </div>
    
    <div class="h-backimg text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                &nbsp;
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! \App\Models\CMS::where('status',1)->where('slug','Privacy-policy')->where('language_id',Session()->get('selectedLang'))->first()->description !!}
                
            </div>
           
        </div>
    </div>
    
    @include('web.home.mobile_app')
@endsection