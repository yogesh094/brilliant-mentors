@extends('web.master_layout.master')

@section('title')
@if(!empty(Session::get('Payment'))) {{ Session::get('Payment') }} @else Payment @endif
@endsection

@section('main-content')

<div class="sub-header"> <img src="{{ asset('resources/web-assets/images/banner-2.JPG') }}" class="img-fluid w-100" alt="Banner">
    <h1>@if(!empty(Session::get('Payment'))) {{ Session::get('Payment') }} @else Payment @endif </h1>
</div>
<div class="h-backimg text-center m-0">
    <div class="container">
        <div class="row">
            <div class="col-12"> </div>
        </div>
    </div>
</div>
<div class="container mb-4">
    <div class="row">
        <div class="col-12">
            <h2 class="title-h2">@if(!empty(Session::get('Payment'))) {{ Session::get('Payment') }} @else Payment @endif</h2>
        </div>
        <div class="col-md-4 order-md-2 mb-4">
            <h4 class="h4"><strong>{{ \App\Models\CMS::where('status',1)->where('slug','payment')->where('language_id',Session()->get('selectedLang'))->first()->title }}</strong></h4>
            <!-- <p>{!! \App\Models\CMS::where('status',1)->where('slug','payment')->where('language_id',Session()->get('selectedLang'))->first()->description !!}</p> -->

            <b>@if(!empty(Session::get('Category'))) {{ Session::get('Category') }} @else Category @endif :</b>
            {{ $getCategory }}
            <br>
            <b> @if(!empty(Session::get('Booking Price'))) {{ Session::get('Booking Price') }} @else Booking Price @endif : </b>
            {{ Cache::get('currency') }}{{ $getcharges }}
            <br>
            <b> @if(!empty(Session::get('Date'))) {{ Session::get('Date') }} @else Date @endif :</b>
            {{ Carbon\Carbon::parse($Appointment['booking_date'])->format('d/m/Y') }}
            <br>
            <b>@if(!empty(Session::get('Time'))) {{ Session::get('Time') }} @else Time @endif :</b>
            {{ Carbon\Carbon::parse($Appointment['start_time'])->format('g:i A') }} to {{ Carbon\Carbon::parse($Appointment['end_time'])->format('g:i A') }}
            <br>
            <b>@if(!empty(Session::get('Description'))) {{ Session::get('Description') }} @else Description @endif :</b>
            {{ $Appointment['description'] }}
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if (Session::has('error'))
            <div class="alert alert-danger">{!! Session::get('error') !!}</div>
            @endif
            @if (Session::has('success'))
            <div class="alert alert-success">{!! Session::get('success') !!}</div>
            @endif
        </div>
        <div class="col-md-8 order-md-1">

            <div class="card" style="margin-bottom: 15px;">
                <div class="card-body card-icon">
                    <i class="fa fa-cc-visa" aria-hidden="true"></i>
                    <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
                    <i class="fa fa-cc-amex" aria-hidden="true"></i>
                    <i class="fa fa-cc-paypal" aria-hidden="true"></i>
                    <i class="fa fa-cc-discover" aria-hidden="true"></i>
                </div>
            </div>

            {{ Form::open(array('url' => '/storePayment' ,'name' => 'frmpayment','id' => 'storePayment')) }}
            <input type="hidden" name="appointment_id" value="{{ $appoinment_id }}">
            <input type="hidden" name="language_id" value="{{ $Appointment->language_id }}">
            <input type="hidden" name="category_id" value="{{ $Appointment->category_id }}">
            <input type="hidden" name="booking_date" value="{{ $Appointment->booking_date }}">
            <input type="hidden" name="start_time" value="{{ $Appointment->start_time }}">
            <input type="hidden" name="end_time" value="{{ $Appointment->end_time }}">
            <input type="hidden" name="description" value="{{ $Appointment->description }}">
            <input type="hidden" name="amount" value="{{ $getcharges }}">
            <input type="hidden" name="status" value="{{ $Appointment->status }}">
            <!-- <div class="d-block my-3">
              <div class="custom-control custom-radio pull-left">
                <input id="credit" name="paymentMethod" class="custom-control-input" checked="" required type="radio">
                <label class="custom-control-label" for="credit">@if(!empty(Session::get('Credit Card'))) {{ Session::get('Credit Card') }} @else Credit card @endif</label>
              </div>
              <div class="custom-control custom-radio pull-left ml-3">
                <input id="debit" name="paymentMethod" class="custom-control-input" required type="radio">
                <label class="custom-control-label" for="debit">@if(!empty(Session::get('Debit Card'))) {{ Session::get('Debit Card') }} @else Debit card @endif</label>
              </div>
              
              <div class="clearfix"></div>
            </div>
            -->
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="cc-name">@if(!empty(Session::get('Name on Card*'))) {{ Session::get('Name on Card*') }} @else Name on Card* @endif</label>
                    <input class="form-control cardName" id="cc-name" type="text" name="cardName" value="{{ old('cardName') }} {{ Session::get('cardName') }}">
                    <small class="text-muted"> @if(!empty(Session::get('card_display'))) {{ Session::get('card_display') }} @else Full name as displayed on card @endif </small><br>
                    <span class="text-danger errorcard" >{{ $errors->first('cardName') }}</span>

                </div>
                <div class="col-md-6 mb-3">
                    <label for="cc-number">@if(!empty(Session::get('Card Number*'))) {{ Session::get('Card Number*') }} @else Card Number* @endif</label>
                    <input class="form-control cardNumber" id="cc-number" type="text" name="cardNumber" value="{{ old('cardNumber') }}">
                    <span class="text-danger" >{{ $errors->first('cardNumber') }}</span>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="cc-expiration">@if(!empty(Session::get('Expiry Date*'))) {{ Session::get('Expiry Date*') }} @else Expiry Date* @endif @if(!empty(Session::get('mm/yy*'))) {{ Session::get('mm/yy*') }} @else mm/yy @endif</label>
                    <div class="mb-3">

<!-- <input class="form-control" id="cc-expiration" placeholder="" required type="text"> -->
<!--  <input type="text" size="2" class="form-control paymant_month" name="cardExpiryMonth" value="{{ Request::old('cardExpiryMonth') }} {{ Session::get('cardExpiryMonth') }}"/>  -->
                        <select name="cardExpiryMonth" class="form-control paymant_month">
                            @foreach($month as $key => $months)
                            <option value="{{ $months }}" @if(old('cardExpiryMonth') == $months) selected="selected" @endif>{{ $months }}</option>
                            @endforeach
                        </select>
                        <span class="paymant_span"> / </span>
                        <!-- <input type="text" size="4" class="form-control payment_year" name="cardExpiryYear" value="{{ Request::old('cardExpiryYear') }} {{ Session::get('cardExpiryYear') }}"/> -->
                        <select name="cardExpiryYear" class="form-control payment_year">
                            @foreach($years as $key => $year)
                            <option value="{{ $year }}" @if(old('cardExpiryYear') == $year) selected="selected" @endif>{{ $year }}</option>
                            @endforeach
                        </select>

   <!--  <span class="text-danger errorcard" >{{ $errors->first('cardExpiryMonth') }}</span><br>
    <span class="text-danger errorcard" >{{ $errors->first('cardExpiryYear') }}</span> -->
                    </div>

                </div>
                <div class="col-md-3 mb-3">
                    <label for="cc-cvv">@if(!empty(Session::get('Cvv*'))) {{ Session::get('Cvv*') }} @else Cvv* @endif</label>
                    <input class="form-control cardCVC" id="cc-cvv" type="text" name="cardCVC" value="{{ old('cardCVC') }}">
                    <span class="text-danger" >{{ $errors->first('cardCVC') }}</span>

                </div>
            </div>
            <hr class="mb-4">

            <input type="submit" class="btn btn-success btn-lg btn-block" name="submit" value="@if(!empty(Session::get('Continue_checkout'))) {{ Session::get('Continue_checkout') }} @else Continue to checkout @endif" />
            {{ Form::close() }} 
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('resources/web-assets/js/jquery.inputmask.bundle.js') }}"></script>
<script type="text/javascript">
$(".cardNumber").inputmask({
mask: "9999 9999 9999 9999",
placeholder: ""
});
//   $(".paymant_month").inputmask({ 
//   mask: "99",
//   placeholder: ""
//   });
//   $(".payment_year").inputmask({ 
//   mask: "9999",
//   placeholder: ""
// });
$(".cardCVC").inputmask({
mask: "999",
placeholder: ""
});
$(".cardName").keypress(function (event) {
var inputValue = event.charCode;

if (inputValue == 95 || inputValue == 94) {
    event.preventDefault();
}
if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
    event.preventDefault();
}
});
// $('.cardName').keypress(function (e) {
//     var regex = new RegExp("^[a-zA-Z]+$");
//     var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
//     if (regex.test(str)) {
//       return true;
//     }
//     else
//     {
//     e.preventDefault();
//     //$('.errorcard').show();
//     //$('.errorcard').text('Please Enter Alphabate');
//     return false;
//     }
//   });
</script>
@endpush('scripts')
