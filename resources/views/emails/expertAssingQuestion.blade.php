<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $data['subject']; ?></title>

</head>

<body>
<table align="center" cellpadding="0" cellspacing="0" style="max-width:800px; width:100%;" >
  <tr>
    <td style="border-top: 4px solid #017bce;">
    <tr style="background-image: url(<?=$data['FRONT_URL']?>/resources/web-assets/images/head-back.png); background-repeat: repeat-x;">

    <td align="left" style="padding: 20px"><img src="<?=$data['FRONT_URL']?>/resources/web-assets/images/logo.png"></td>
    </tr>
    </td>
  
    </tr>
  
  <tr>
    <td height="40px"> </td>
  </tr>
  
  <tr style="padding: 10px; font-family: 'gt_walsheim_promedium';">
    <td style="padding: 20px">
      <?php echo $data['body']; ?>
     </td>
  </tr>
  
  <tr>
    <td height="80px"> </td>
    
  </tr>
  <tr style="background-color:#00406b; text-align: left;">
    <td style="padding: 20px; font-family: 'gt_walsheim_promedium'; color: #FFF; font-size: 15px;" > &copy; Copyright <a style="color: #FFF;">Islamhub.com</a> <?php echo " ".date('Y');?> All rights reserved. </td>
  </tr>  
</table>
</body>
</html>