
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Religion</title>
  <style type="text/css" rel="stylesheet" media="all">
    /* Base ------------------------------ */
    /**:not(br):not(tr):not(html) {
      font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
    }*/
    body {
      width: 100% !important;
      height: 100%;
      margin: 0;
      line-height: 1.4;
       color: #74787E;
      /*-webkit-text-size-adjust: none;*/
    }
    a {
      color: #3869D4;
    }
    /* Layout ------------------------------ */
    .email-wrapper {
      width: 100%;
      margin: 0;
      padding: 0;
      background-color: #1c9dd8;
    }
    .email-content {
      width: 50%;
      margin: 0;
      padding: 0;
    }
    /* Masthead ----------------------- */
    .email-masthead , #email-masthead {
      padding: 25px 0;
      text-align: center;
    }
    .email-masthead_logo, #email-masthead .email-masthead_logo {
      max-width: 400px;
      border: 0;
    }
    .email-masthead_name {
      font-size: 16px;
      font-weight: bold;
      color: #bbbfc3;
      text-decoration: none;
      text-shadow: 0 1px 0 white;
    }
    /* Body ------------------------------ */
    .email-body {
      width: 100%;
      margin: 0;
      padding: 0;
      border-top: 1px solid #EDEFF2;
      border-bottom: 1px solid #EDEFF2;
      background-color: #FFF;
      border-radius: 10px;
    }
    .email-body_inner {
      width: 570px;
      margin: 0 auto;
      padding: 0;
    }
    .email-footer {
      width: 570px;
      margin: 0 auto;
      padding: 0;
      text-align: center;
    }
    
    .body-action {
      width: 100%;
      margin: 30px auto;
      padding: 0;
      text-align: center;
    }
    .body-sub {
      width: 100%;
      margin-top: 25px;
      padding-top: 25px;
      border-top: 1px solid #EDEFF2;
    }
    .content-cell {
      padding: 25px;
    }
    .align-right {
      text-align: right;
    }
    /* Type ------------------------------ */
    #email-wrapper h1 {
      margin-top: 0;
      color: #2F3133;
      font-size: 19px;
      font-weight: bold;
      text-align: left;
    }
    #email-wrapper h2 {
      margin-top: 0;
      color: #2F3133;
      font-size: 16px;
      font-weight: bold;
      text-align: left;
    }
    #email-wrapper h3 {
      margin-top: 0;
      color: #2F3133;
      font-size: 14px;
      font-weight: bold;
      text-align: left;
    }
    #email-wrapper p , .content-cell p {
      margin-top: 0;color: #74787E;font-size: 16px;line-height: 1.5em;text-align: left;
    }
    #email-wrapper p.sub {
      font-size: 12px;
    }
    #email-wrapper p.center {
      text-align: center;
    }
    .email-footer p {
      color: #FFF;
    }
    /* Buttons ------------------------------ */
    
    .button {
      display: inline-block;
      width: 200px;
      background-color: #3869D4;
      border-radius: 3px;
      color: #ffffff;
      font-size: 15px;
      /*line-height: 45px;*/
      text-align: center;
      text-decoration: none;
      /*-webkit-text-size-adjust: none;*/
      /*mso-hide: all;*/
    }
    #buttonId {
      display: inline-block;
      width: 200px;
      background-color: #3869D4;
      border-radius: 3px;
      color: #ffffff;
      font-size: 15px;
      /*line-height: 45px;*/
      text-align: center;
      text-decoration: none;
      /*-webkit-text-size-adjust: none;*/
      /*mso-hide: all;*/
    }
    .button--green {
      background-color: #22BC66;
    }
    .button--red {
      background-color: #dc4d2f;
    }
    .button--blue {
      background-color: #3869D4;
    }
    #buttonId.button--green {
      background-color: #22BC66;
    }
    #buttonId.button--red {
      background-color: #dc4d2f;
    }
    #buttonId.button--blue {
      background-color: #3869D4;
    }
    /*Media Queries ------------------------------ */
    @media only screen and (max-width: 600px) {
      .email-body_inner,
      .email-footer {
        width: 100% !important;
      }
    }
    @media only screen and (max-width: 500px) {
      .button {
        width: 100% !important;
      }
    }
  </style>
</head>
<body>
  <table id="email-wrapper" class="email-wrapper" style="" width="50%" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center">
        <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
          <!-- Logo -->
          <tr>
            <td class="email-masthead" id="email-masthead" align="center" style="color:#fefefe;font-size:35px;font-weight:900;">
              Religion
            </td>
          </tr>
          <!-- Email Body -->
          <tr>
            <td class="email-body" style="background-color: #FFF;border-radius-10px;" width="50%">
              <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                <!-- Body content -->
                <tr>
                  <td class="content-cell">
                    <h1 style="margin-top: 0;color: #2F3133;font-size: 19px;font-weight: bold;text-align: left;">Hi <?php echo $data['name']; ?>,</h1>
                    <p style="margin-top: 0;color: #000;font-size: 16px;line-height: 1.5em;text-align: left;font-weight:bold;">It looks like you requested a new password.</p>
                    <p style="margin-top: 0;color: #000;font-size: 16px;line-height: 1.5em;text-align: left;font-weight:bold;">If that sounds right, you can enter new password by Verification Code.</p>
                    <p style="margin-top: 0;color: #000;font-size: 16px;line-height: 1.5em;text-align: left;font-weight:bold;">Verification Code:</p>
                    <p style="color: #fefefe;background-color: #009688;padding: 5px;border-radius: 3px;text-decoration:none;">
                     <?php echo $data['code']; ?>
                    </p>
                    <p style="margin-top: 20px;color: #000;font-size: 16px;line-height: 1.5em;text-align: left;font-weight:bold;">
                      Regards,<br>
                      
                    <!-- Sub copy -->

                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table class="email-footer"  align="center" width="570" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-cell">
                    <p class="sub center" style="color:#ffffff;">&copy; Copyright Religion <?php echo " ".date('Y');?> All rights reserved.</p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>