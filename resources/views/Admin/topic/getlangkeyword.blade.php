@if(count($Topic) > 0)
{!! Form::open(array('url' => ('/panel/topic/multiStore'),'method'=>'POST', 'files'=>true)) !!}
        <input type="hidden" name="langId" value="">
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
        	<h4>List for <b>Topic</b></h4>
        </div>
        <div class="form-group col-md-12 col-sm-12 col-xs-12 entity_multi_data">
            <div class="row">
                <div class=" col-md-3 col-sm-3 col-xs-12 childRow_18">
                   <b> Topic Name</b>
                </div>
                 <div class=" col-md-3 col-sm-3 col-xs-12 childRow_18">
                   <b> Charges</b>
                </div>
            </div>
        </div>
               
                @foreach($Topic as $key => $TopicKey)
                <div class="form-group col-md-12 col-sm-12 col-xs-12 entity_multi_data">
                                	<div class="row">
                                	<div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
                                		<input class="form-control" id="topic" name="topic[{{ $TopicKey->id }}][{{ $TopicKey->language_id }}]" type="text" placeholder="Topic" value="{{ $TopicKey->topic }}">
                                		<span class="text-danger">{{ $errors->first('topic') }}</span>
                                	</div>
                                    
                                    <div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
                                    <input class="form-control" id="charges" name="charges[{{ $TopicKey->id }}][{{ $TopicKey->language_id }}]" type="text" placeholder="Charges" value="{{ $TopicKey->charges }}">
                                    <span class="text-danger">{{ $errors->first('charges') }}</span>
                                  </div>

                                    <span class="text-danger defaultTextError"></span>
                                    <div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
                                    
                <span class="trashRow trashRow_{{ $TopicKey->id }} remove" trashid="{{ $TopicKey->id }}" dataid="{{ $TopicKey->id }}">
                    <i class="fa fa-trash" style="font-size: 18px;padding-top: 2%"></i>
                </span>
            
                                </div>
                           </div>
                                  
                            
                 
                </div>
@endforeach

<div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
  {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
</div>
{!! Form::close() !!} 
@endif