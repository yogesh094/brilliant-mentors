	@extends('Admin.master_layout.master')

@section('title', 'Add Topic')

@section('breadcum')
     / Add Topic
@endsection

@section('content')
 <div class="show_message">
 	<div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
 </div>

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                     <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul> 
                </div>
            @endif
            <!-- if there are creation errors, they will show here -->
            <span class="errormessage"></span>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
                
                <div class="box-body">
                	{!! Form::open(array('url' => ('/panel/topic/store'),'method'=>'POST', 'files'=>true)) !!}
                	<div class="form-group">
                        {!! Form::label('Language', 'Language') !!}
                        
                        <select name="language_id" class="form-control" required="" id="language_id">
                            <option value="">Select Language</option>
                            @if(count($languages) > 0)
                            @foreach($languages as $key => $lang)
                            <option value="{{ $lang['id'] }}" @if(isset($langId) && $langId > 0 && $langId == $lang['id'])selected=""@endif>{{ $lang['name'] }}</option>
                            @endforeach

                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('language_id') }}</span>
                    </div>
                    <?php 
                                if(isset($langId) && !empty($langId) && $langId > 0) {
                                    $divBlock = "display: block";
                                } else {
                                    $divBlock = "display: none";
                                }
                            ?>
                    <div id="hideshowkey" class="add_txt_block" style="{{ $divBlock }}">
                         <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <h4 class="add_txt_label">Create new <b>Topic</b></h4>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 entity_multi_data">
                                	<div class="row">
                                	<div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
                                		<input class="form-control" id="topic" name="topic" type="text" placeholder="Topic Name">
                                		<span class="text-danger">{{ $errors->first('topic') }}</span>
                                	</div>
                                    
                                    <div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
                                    <input class="form-control" id="charges" name="charges" type="text" placeholder="Charges">
                                    <span class="text-danger">{{ $errors->first('charges') }}</span>
                                  </div>

                                    <span class="text-danger defaultTextError"></span>
                                    <div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
                                    {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                                </div>
                           </div>
                                  
                          {!! Form::close() !!}  
                 
                </div>
		
            </div>
</div>
<div id="add_multi_lang_txt_box">
	 <?php 
        if(isset($langId) && !empty($langId) && $langId > 0) {
        $divBlock = "display: block";
         } else {
         $divBlock = "display: none";
         }?>
         <div style="{{ $divBlock }}">
         	@include('Admin.topic.getlangkeyword')
         </div>
</div>
              
            </div>
            
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection
@push('scripts')
<script type="text/javascript">
	//Display add text on select title - start
        $(document).on("change", 'select#language_id', function(event) {
            var langText   = $('select#language_id option:selected').text();
            var langId     = $('select#language_id option:selected').val();
            // var divClass    = 0;
            
            if(langId != ""){
                $('.add_txt_label').html("Create new Label for <b>"+langText+"</b>");
                $('.add_txt_block').show();
            }

            /*if($('.entity_multi_data').length){
                divClass = 1;
            }*/

            $.ajax({
                type: "GET",
                url: base_url+"/panel/topic/getlangkeyword",
                async:false,
                data: { 'langId' : langId,'_token' : "{{ csrf_token() }}" },
                success:function(data) {
                	//alert(data);
                    $("#add_multi_lang_txt_box").html(data);
                    $('.submit').show();
                }
            });
        });
    //Display add text on select title - end
	//Delete multi language fields - start
        $(document).on("click", '.trashRow', function(event) {
            var rowId  = $(this).attr('trashId');
            var dataId = $(this).attr('dataId');
            // console.log(dataId);
            var answer=confirm("Are you sure you want to delete this record?");
            if(answer==true) {
                if(dataId != undefined){
                    $.ajax({
                        type: "GET",
                        url: base_url+"/panel/topic/delete/"+rowId,
                        async:false,
                        data: { 'id' : rowId, '_token' : "{{ csrf_token() }}" },
                        success:function(data) {
                        	
                            if(data == "true"){
                                $('.trashRow_'+rowId).parent().parent().parent().remove();
                                $('.childRow_'+rowId).remove();
                                $('.show_message').html("<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>Keyword removed successfully.</div>");
                                setTimeout(function() {
                                    $(".alert-success").hide()
                                }, 10000);
                            } else {
                                alert("This property can not be deleted now.");
                            }
                        }
                    });
                }
            } else {
                return false;
            }
        });
    //Delete multi language fields - end
</script>
@endpush