<div role="tabpanel" class="tab-pane fade" id="spouse" >
    {{-- \Session::get('submit_spouse')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Spouse </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#spouse') }}"> Edit <i class="fa fa-external-link"></i></a>
        
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
                <!-- <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul> -->

            </div>
        @endif
        <!-- if there are creation errors, they will show here -->
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">

        {!! Form::open(array('url' => ('contact/spouse-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($contact->id)?$contact->id:'') !!}   

            {!! Form::hidden('contact_id', isset($contact->id)?$contact->id:'') !!}

            <!-- left -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- First Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name </label>

                    <!-- title -->
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <?php 
                        $spouseTitleArr         = ['1' => 'Mr.' , '2' => 'Mrs.' , '3' => 'Miss.'];
                        $spouseTitleText        = "";
                        if(isset($spouseContact->title)) {
                            $spouseTitleText    = (isset($spouseTitleArr[$spouseContact->title]))?$spouseTitleArr[$spouseContact->title]:"";    
                        }
                        ?> 
                        {!! Form::text('spouse_title','',['class'=>'form-control', 'placeholder'=>$spouseTitleText,'readonly' => 'readonly']) !!}
                        
                        <span class="text-danger">{{ $errors->first('spouse_title') }}</span>
                    </div>

                    <!-- First Name -->
                    <div class="col-md-6 col-sm-6 col-xs-9">

                        <?php 
                        $spouseFirstNameArr     = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => '    Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'];
                        $spouseFirstNameText    = "";
                        if(isset($spouseContact->first_name)) {
                            $spouseFirstNameText    = (isset($spouseFirstNameArr[$spouseContact->first_name]))?$spouseFirstNameArr[$spouseContact->first_name]:"";    
                        }
                        ?> 
                        {!! Form::text('spouse_first_name','',['class'=>'form-control', 'placeholder'=>$spouseFirstNameText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('spouse_first_name') }}</span>
                    </div>
                </div>

                <!-- suffix -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Suffix </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $suffixArr      = ['1' => 'CLU' , '2' => 'D.D.S.', '3' => 'D.V.M.', '4' => 'Esq.', '5' => 'LL.D.', '6' => 'M.D.', '7' => 'O.S.B.', '8' => 'Ph.D.', '9' => 'R.G.S', '10' => 'S.H.C.J.'];

                        $suffixStr      = "";
                        if(isset($spouseContact->suffix)) {

                            if($spouseContact->suffix != "") {
                                foreach (explode(',',$spouseContact->suffix) as $key => $suffix) {
                                    $suffixStr   .= $suffixArr[$suffix].", ";
                                }   
                                $suffixStr   = ($suffixStr != "")?rtrim($suffixStr," ,"):"";
                            }
                        }
                        ?>

                        {!! Form::textarea('spouse_suffix','',['id'=>'spouse_suffix','class'=>'form-control', 'placeholder'=>$suffixStr,'readonly' => 'readonly','rows'=>2]) !!}
                        
                        <span class="text-danger">{{ $errors->first('spouse_suffix') }}</span>
                    </div>
                </div>

                <!-- Father Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Father Name  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $FatherNameArr      = ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '6' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'];

                        $fatherNameText     = "";
                        if(isset($spouseContact->father_name)) {
                            $fatherNameText = (isset($FatherNameArr[$spouseContact->father_name]))?$FatherNameArr[$spouseContact->father_name]:"";    
                        }
                        
                        ?>    
                        {!! Form::text('spouse_father_name','',['class'=>'form-control', 'placeholder'=>$fatherNameText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('spouse_father_name') }}</span>
                    </div>
                </div>

                <!-- Mother Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mother Name  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $motherNameArr      = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ];

                        $motherNameText     = "";
                        if(isset($spouseContact->mother_name)) {
                            $motherNameText   = (isset($motherNameArr[$spouseContact->mother_name]))?$motherNameArr[$spouseContact->mother_name]:"";    
                        }
                        
                        ?>    
                        {!! Form::text('spouse_mother_name','',['class'=>'form-control', 'placeholder'=>$motherNameText,'readonly' => 'readonly']) !!}
                        <span class="text-danger">{{ $errors->first('spouse_mother_name') }}</span>
                    </div>
                </div>

                <!-- Surname -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Surname  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $surnameArr      = ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'];

                        $surnameText     = "";
                        if(isset($spouseContact->surname)) {
                            $surnameText   = (isset($surnameArr[$spouseContact->surname]))?$surnameArr[$spouseContact->surname]:"";    
                        }
                        
                        ?>    
                        {!! Form::text('spouse_surname','',['class'=>'form-control', 'placeholder'=>$surnameText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('spouse_surname') }}</span>
                    </div>
                </div>

                <!-- DOB -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $dobText        = isset($spouseContact->date_of_birth)?date("m-d-Y",strtotime($spouseContact->date_of_birth)):'';
                        ?>
                        {!! Form::text('spouse_date_of_birth','',['class'=>'form-control', 'placeholder'=>$dobText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('spouse_date_of_birth') }}</span>
                    </div>
                </div>

                <!-- blood type -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Type  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $bloodTypeText      = "";    
                        if(isset($spouseContact->blood_type)) {
                            $bloodTypeText  = isset($bloodType[$spouseContact->blood_type])?$bloodType[$spouseContact->blood_type]:"";
                        }
                        ?> 
                        {!! Form::text('spouse_blood_type','',['class'=>'form-control', 'placeholder'=>$bloodTypeText,'readonly' => 'readonly']) !!}
                        <span class="text-danger">{{ $errors->first('spouse_blood_type') }}</span>
                    </div>
                </div>

                <!-- No of Dependents -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    No Of Dependents  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $noOfdependencyText     = isset($spouseContact->number_of_dependents)?$spouseContact->number_of_dependents:'';
                        ?>
                        {!! Form::text('spouse_number_of_dependents','',['class'=>'form-control', 'placeholder'=>$noOfdependencyText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('spouse_number_of_dependents') }}</span>
                    </div>
                </div>

                <!-- languages -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Languages </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $langStr      = "";
                        if(isset($spouseContact->languages)) {
                            if($spouseContact->languages != "") {
                                foreach (explode(',',$spouseContact->languages) as $key => $lang) {
                                    $langStr   .= $languages[$lang].", ";
                                }   
                                $langStr   = ($langStr != "")?rtrim($langStr," ,"):"";
                            }
                        }
                        ?>
                        {!! Form::textarea('spouse_languages[]','',['class'=>'form-control', 'placeholder'=>$langStr,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('spouse_languages') }}</span>
                    </div>
                </div>

                <!-- importance -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $importanceArr  = ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
                        $impoetanceStr  = "";
                        if(isset($spouseContact->importance)) {
                            $impoetanceStr  = isset($importanceArr[$spouseContact->importance])?$importanceArr[$spouseContact->importance]:"";
                        }
                        ?>

                        {!! Form::text('spouse_importance','',['class'=>'form-control', 'placeholder'=>$impoetanceStr,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('spouse_importance') }}</span>
                    </div>
                </div>

                <!-- sensitivity -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $sensitivityArr     = [1=>'sensitivity text1',2=>'sensitivity text2',3=>'sensitivity text3'];
                        $sensitivityStr     = "";
                        if(isset($spouseContact->sensitivity)) {
                            $sensitivityStr = isset($sensitivityArr[$spouseContact->sensitivity])?$sensitivityArr[$spouseContact->sensitivity]:"";
                        }
                        ?>

                        {!! Form::text('spouse_sensitivity','',['class'=>'form-control', 'placeholder'=>$sensitivityStr,'readonly' => 'readonly','rows'=>2]) !!}

                        
                        <span class="text-danger">{{ $errors->first('spouse_sensitivity') }}</span>
                    </div>
                </div>

                <!-- Charitable Organizations -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Charitable Organizations </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $charityArr     = ['1' => 'YSR FOUNDATION ' , '2' => 'THE COCONADA CHAMBER OF COMMERCE', '3' => 'RAVINDRANATH MEDICAL FOUNDATION'];
                        $charityStr     = "";
                        if(isset($spouseContact->charitable_organizations)) {
                            if($spouseContact->charitable_organizations != "") {
                                foreach (explode(',',$spouseContact->charitable_organizations) as $key => $charity) {
                                    $charityStr   .= $charityArr[$charity].", ";
                                }   
                                $charityStr   = ($charityStr != "")?rtrim($charityStr," ,"):"";
                            }
                        }
                        
                        ?>
                        {!! Form::textarea('spouse_charitable_organizations[]','',['class'=>'form-control', 'placeholder'=>$charityStr,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('spouse_charitable_organizations') }}</span>
                    </div>
                </div>

                <!-- Internal Organizations -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Internal Organizations </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $intOrgArr     = ['1' => 'Makhzoumi Foundation' , '2' => 'Beirutiyat', '3' => 'Makhzoumi Health Care', '4' => 'Makhzoumi Development'];
                        $intOrgStr     = "";
                        if(isset($spouseContact->internal_organizations)) {

                            if($spouseContact->internal_organizations != "") {
                                foreach (explode(',',$spouseContact->internal_organizations) as $key => $intOrg) {
                                    $intOrgStr   .= $intOrgArr[$intOrg].", ";
                                }   
                                $intOrgStr   = ($intOrgStr != "")?rtrim($intOrgStr," ,"):"";
                            }
                        }
                        ?>
                        {!! Form::textarea('spouse_internal_organizations[]','',['class'=>'form-control', 'placeholder'=>$intOrgStr,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('spouse_internal_organizations') }}</span>
                    </div>
                </div>

                <!-- Status -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Status  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <label>
                            <?php
                            $statusYes      = false;
                            if(isset($spouseContact->status)) {
                                $statusYes     = $spouseContact->status == 1?true:false;
                            }
                            ?>
                            {!! Form::radio('spouse_status', '1', $statusYes, ['class' => 'flat','disabled' => 'disabled' ]) !!}  Active
                        </label>
                        <label>
                            <?php
                            $statusNo      = false;
                            if(isset($spouseContact->status)) {
                                $statusNo      = $spouseContact->status == 0?true:false;
                            }
                            ?>
                            {!! Form::radio('spouse_status', '0', $statusNo, ['class' => 'flat', 'disabled' => 'disabled']) !!}  Inactive
                        </label>
                    </div>
                </div>

                <!-- Keywords -->
                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label"> Keywords  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $keywordText     = isset($spouseContact->keywords)?$spouseContact->keywords:'';
                        ?>
                        {!! Form::textarea('spouse_keywords','',['class'=>'form-control', 'placeholder'=>$keywordText,'readonly' => 'readonly','rows' => 2]) !!}
                        <span class="text-danger">{{ $errors->first('spouse_keywords') }}</span>
                    </div>
                </div> -->
            </div>

            <!-- right -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- photo box -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-right">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3 my-photo-box-view">
                        <div style="text-align: left;float:none;">
                            <?php
                            if(isset($spouseContact->photo) && $spouseContact->photo != "") {
                            ?>
                                <img src="{{ $imageUrl.$spouseContact->photo }}" class="contact_spouse_photo img-thumbnail" >
                            <?php
                            } else {
                            ?>
                                <img src="{{ url('/uploads/contact/avatar.png') }}" class="contact_spouse_photo img-thumbnail" >
                            <?php 
                            }
                            ?>
                            <input type="hidden" name="old_contact_spouse_image" value="{{ isset($spouseContact->photo)?$spouseContact->photo:'' }}">
                        </div>
                    </div>
                </div>

                <!-- nick name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $nicknameText     = isset($spouseContact->nickname)?$spouseContact->nickname:'';
                        ?>
                        {!! Form::text('spouse_nick_name','',['class'=>'form-control', 'placeholder'=>$nicknameText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('spouse_nick_name') }}</span>
                    </div>
                </div>

                <!-- birth place -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Birth Place </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $birthPlaceText     = "";    
                        if(isset($spouseContact->birth_place)) {
                            $birthPlaceText = isset($nationality[$spouseContact->birth_place])?$nationality[$spouseContact->birth_place]:"";
                        }
                        
                        ?> 

                        {!! Form::text('spouse_birth_place','',['class'=>'form-control', 'placeholder'=>$birthPlaceText,'readonly' => 'readonly']) !!}
                        <span class="text-danger">{{ $errors->first('spouse_birth_place') }}</span>
                    </div>
                </div>

                <!-- gender -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Gender  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $genderText     = "";    
                        if(isset($spouseContact->gender)) {
                            $genderText = isset($gender[$spouseContact->gender])?$gender[$spouseContact->gender]:"";
                        }
                        
                        ?> 

                        {!! Form::text('spouse_gender','',['class'=>'form-control', 'placeholder'=>$genderText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('spouse_gender') }}</span>
                    </div>
                </div>

                <!-- nationality -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nationality  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $nationalityStr         = "";  
                        if(isset($spouseContact->nationality)) {

                            if($spouseContact->nationality != "") {
                                foreach (explode(',',$spouseContact->nationality) as $key => $nation) {
                                    $nationalityStr   .= $nationality[$nation].", ";
                                }   
                                $nationalityStr     = ($nationalityStr != "")?rtrim($nationalityStr," ,"):"";
                            }
                        }
                        
                        ?> 

                        {!! Form::textarea('spouse_nationality[]','',['class'=>'form-control', 'placeholder'=>$nationalityStr,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('spouse_nationality') }}</span>
                    </div>
                </div>

                <!-- profession -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Profession </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $professionArr      = ['1' => 'Profession 1' , '2' => 'Profession 2' , '3' => 'Profession 3', '4' => 'Profession 4', '5' => 'Profession 5'];
                        $professionStr       = "";  
                        if(isset($spouseContact->profession)) {

                            if($spouseContact->profession != "") {
                                foreach (explode(',',$spouseContact->profession) as $key => $prof) {
                                    $professionStr   .= $professionArr[$prof].", ";
                                }   
                                $professionStr     = ($professionStr != "")?rtrim($professionStr," ,"):"";
                            }
                        }   
                        
                        ?> 

                        {!! Form::textarea('spouse_profession[]','',['class'=>'form-control', 'placeholder'=>$professionStr,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('spouse_profession') }}</span>
                    </div>
                </div>

                <!-- caution -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $causionArr     = ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'];
                        $cautionText    = "";    
                        if(isset($spouseContact->caution)) {
                            $cautionText = isset($causionArr[$spouseContact->caution])?$causionArr[$spouseContact->caution]:"";
                        }
                        
                        ?> 

                        {!! Form::text('spouse_caution','',['class'=>'form-control', 'placeholder'=>$cautionText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('spouse_caution') }}</span>
                    </div>
                </div>

                <!-- Sponsorships -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sponsorships  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $sponsorshipArr     = ['1' => 'JPMorgan' , '2' => 'Microsoft', '3' => 'Deloitte', '4' => 'PepsiCo', '5' => 'DHL Express'];
                        $sponsorText        = ""; 
                        if(isset($spouseContact->sponsorships)) {   
                            if($spouseContact->sponsorships != "") {
                                foreach (explode(',',$spouseContact->sponsorships) as $key => $spons) {
                                    $sponsorText   .= $sponsorshipArr[$spons].", ";
                                }   
                                $sponsorText     = ($sponsorText != "")?rtrim($sponsorText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('spouse_sponsorships','',['class'=>'form-control', 'placeholder'=>$sponsorText,'readonly' => 'readonly','rows' => 2]) !!}
                        <span class="text-danger">{{ $errors->first('spouse_sponsorships') }}</span>
                    </div>
                </div>

                <!-- Political Party -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Party </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $politicalPartyArr  = ['1' => 'Movement of the future' , '2' => 'Kataeb Party', '3' => 'Hezbollah', '4' => 'Free Patriotic Movement'];
                        $politicalText      = "";    
                        if(isset($spouseContact->political_party)) {   
                            if($spouseContact->political_party != "") {
                                foreach (explode(',',$spouseContact->political_party) as $key => $polity) {
                                    $politicalText   .= $politicalPartyArr[$polity].", ";
                                }   
                                $politicalText  = ($politicalText != "")?rtrim($politicalText," ,"):"";
                            }
                        }
                        
                        ?> 

                        {!! Form::textarea('spouse_political_party[]','',['class'=>'form-control', 'placeholder'=>$politicalText,'readonly' => 'readonly','rows' => 2]) !!}

                        
                        <span class="text-danger">{{ $errors->first('spouse_political_party') }}</span>
                    </div>
                </div>

                <!-- Religion -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Religion </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $religionArr        = ['1' => 'Christian', '2' => 'Muslim'];
                        $religionText       = "";    
                        if(isset($spouseContact->religion_id)) {
                            $religionText   = isset($religionArr[$spouseContact->religion_id])?$religionArr[$spouseContact->religion_id]:"";
                        }
                        ?> 

                        {!! Form::text('spouse_religion','',['class'=>'form-control', 'placeholder'=>$religionText,'readonly' => 'readonly']) !!}
                        <span class="text-danger">{{ $errors->first('spouse_religion') }}</span>
                    </div>
                </div>

                <!-- Administrative Group -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Administrative Group </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $administrativeArr  = ['1' => 'Business Group' , '2' => 'Volunteer Group', '3' => 'Administration Group', '4' => 'Management Group'];
                        $administrativeText = "";    
                        if(isset($spouseContact->administrative_group)) {
                            if($spouseContact->administrative_group != "") {
                                foreach (explode(',',$spouseContact->administrative_group) as $key => $admingp) {
                                    $administrativeText   .= $administrativeArr[$admingp].", ";
                                }   
                                $administrativeText  = ($administrativeText != "")?rtrim($administrativeText," ,"):"";
                            }
                        }
                        
                        ?> 

                        {!! Form::textarea('spouse_administrative_group[]','',['class'=>'form-control', 'placeholder'=>$administrativeText,'readonly' => 'readonly','rows' => 2]) !!}

                        
                        <span class="text-danger">{{ $errors->first('spouse_administrative_group') }}</span>
                    </div>
                </div>

                <!-- Notes -->
                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $noteText       = isset($spouseContact->notes)?$spouseContact->notes:'';
                        ?>
                        {!! Form::textarea('spouse_notes','',['class'=>'form-control', 'placeholder'=>$noteText,'readonly' => 'readonly','rows' => 2]) !!}
                        <span class="text-danger">{{ $errors->first('spouse_notes') }}</span>
                    </div>
                </div> -->
            </div>

            <!-- keyword and notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <!-- Keywords -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        {!! Form::text('keywords',isset($spouseContact->keywords)?$spouseContact->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                    </div>
                </div>
                
                <!-- Notes -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php 
                        $notes     = isset($spouseContact->notes)?$spouseContact->notes:'';
                        ?>
                        {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                    </div>
                </div>
            </div>

            <!-- left -->
            <!-- created section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Created by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($spouseContact->user_createby['first_name'])?$spouseContact->user_createby['first_name']:'';
                        $lastName   = isset($spouseContact->user_createby['last_name'])?$spouseContact->user_createby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Created on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($spouseContact->created_at)?date_format($spouseContact->created_at, 'j M Y g:i A'):'';
                        ?>

                        {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- right -->
            <!-- updated section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Updated by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($spouseContact->user_updateby['first_name'])?$spouseContact->user_updateby['first_name']:'';
                        $lastName   = isset($spouseContact->user_updateby['last_name'])?$spouseContact->user_updateby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Updated on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($spouseContact->updated_at)?date_format($spouseContact->updated_at, 'j M Y g:i A'):'';
                        ?>
                        {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- submit section -->
            <?php 
            $i = 0;
            if($i == 1) {
            ?>
                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_spouse')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                    </div>
                </div>
            <?php 
            }
            ?>
        {!! Form::close() !!}
        </div>
    </div>
</div>