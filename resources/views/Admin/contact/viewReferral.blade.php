<div role="tabpanel" class="tab-pane fade" id="referral" >
    {{-- \Session::get('submit_referral')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Referral </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#referral') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/referral-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($contact->id)?$contact->id:'') !!}   

            {!! Form::hidden('contact_id', isset($referralContact->contact_id)?$referralContact->contact_id:'') !!}

            <!-- left -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- First Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name  </label>

                    <!-- title -->
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <?php 
                        $titleArr     = ['1' => 'Mr.' , '2' => 'Mrs.' , '3' => 'Miss.'];
                        $titleText    = "";
                        if(isset($referralContact->title)) {
                            $titleText    = (isset($titleArr[$referralContact->title]))?$titleArr[$referralContact->title]:"";    
                        }
                        ?> 
                        {!! Form::text('referral_title','',['class'=>'form-control', 'placeholder'=>$titleText,'readonly' => 'readonly']) !!}

                        
                        <span class="text-danger">{{ $errors->first('referral_title') }}</span>
                    </div>

                    <!-- First Name -->
                    <div class="col-md-6 col-sm-6 col-xs-9">
                        <?php
                        $firstArr     = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => '    Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'];
                        $firstText    = "";
                        if(isset($referralContact->first_name)) {
                            $firstText    = (isset($firstArr[$referralContact->first_name]))?$firstArr[$referralContact->first_name]:"";    
                        }
                        ?> 
                        {!! Form::text('referral_first_name','',['class'=>'form-control', 'placeholder'=>$firstText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_first_name') }}</span>
                    </div>
                </div>

                <!-- suffix -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Suffix  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $suffixArr      = ['1' => 'CLU' , '2' => 'D.D.S.', '3' => 'D.V.M.', '4' => 'Esq.', '5' => 'LL.D.', '6' => 'M.D.', '7' => 'O.S.B.', '8' => 'Ph.D.', '9' => 'R.G.S', '10' => 'S.H.C.J.'];

                        $suffixStr      = "";
                        if(isset($referralContact->suffix)){

                            if($referralContact->suffix != "") {
                                foreach (explode(',',$referralContact->suffix) as $key => $suffix) {
                                    $suffixStr   .= $suffixArr[$suffix].", ";
                                }   
                                $suffixStr   = ($suffixStr != "")?rtrim($suffixStr," ,"):"";
                            }
                        }
                        ?>

                        {!! Form::textarea('referral_suffix','',['class'=>'form-control', 'placeholder'=>$suffixStr,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_suffix') }}</span>
                    </div>
                </div>

                <!-- Father Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Father Name</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $fatherArr     = ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '12' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'];
                        $fatherText    = "";
                        if(isset($referralContact->father_name)) {
                            $fatherText    = (isset($fatherArr[$referralContact->father_name]))?$fatherArr[$referralContact->father_name]:"";    
                        }
                        ?> 
                        {!! Form::text('referral_father_name','',['class'=>'form-control', 'placeholder'=>$fatherText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_father_name') }}</span>
                    </div>
                </div>

                <!-- Mother Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mother Name</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $motherArr     = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ];
                        $motherText    = "";
                        if(isset($referralContact->mother_name)) {
                            $motherText    = (isset($motherArr[$referralContact->mother_name]))?$motherArr[$referralContact->mother_name]:"";    
                        }
                        ?> 
                        {!! Form::text('referral_mother_name','',['class'=>'form-control', 'placeholder'=>$motherText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_mother_name') }}</span>
                    </div>
                </div>

                <!-- Surname -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Surname  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $surnameArr     = ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'];
                        $surnameText    = "";
                        if(isset($referralContact->surname)) {
                            $surnameText    = (isset($surnameArr[$referralContact->surname]))?$surnameArr[$referralContact->surname]:"";    
                        }
                        ?> 
                        {!! Form::text('referral_surname','',['class'=>'form-control', 'placeholder'=>$surnameText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_surname') }}</span>
                    </div>
                </div>

                <!-- DOB -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $dob     = isset($referralContact->date_of_birth) ?date("m-d-Y", strtotime($referralContact->date_of_birth)):'';
                        ?>
                        {!! Form::text('referral_date_of_birth','',['class'=>'form-control', 'placeholder'=>$dob,'readonly' => 'readonly']) !!}
                        
                        <span class="text-danger">{{ $errors->first('referral_date_of_birth') }}</span>
                    </div>
                </div>

                <!-- blood type -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Type</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $bloodTypeText    = "";
                        if(isset($referralContact->blood_type)) {
                            $bloodTypeText    = (isset($bloodType[$referralContact->blood_type]))?$bloodType[$referralContact->blood_type]:"";    
                        }
                        ?> 
                        {!! Form::text('referral_blood_type','',['class'=>'form-control', 'placeholder'=>$bloodTypeText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_blood_type') }}</span>
                    </div>
                </div>

                <!-- No of Dependents -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    No Of Dependents</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $numofdep     = isset($referralContact->number_of_dependents)?$referralContact->number_of_dependents:'';
                        ?>
                        {!! Form::text('referral_number_of_dependents','',['class'=>'form-control', 'placeholder'=>$numofdep,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_number_of_dependents') }}</span>
                    </div>
                </div>

                <!-- languages -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Languages</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $langStr      = "";
                        if(isset($referralContact->languages)) {
                            if($referralContact->languages != "") {
                                foreach (explode(',',$referralContact->languages) as $key => $lang) {
                                    $langStr   .= $languages[$lang].", ";
                                }   
                                $langStr   = ($langStr != "")?rtrim($langStr," ,"):"";
                            }
                        }
                        ?>

                        {!! Form::textarea('referral_languages','',['class'=>'form-control', 'placeholder'=>$langStr,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_languages') }}</span>
                    </div>
                </div>

                <!-- importance -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $imporArr       = ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
                        $imporText    = "";
                        if(isset($referralContact->importance)) {
                            $imporText    = (isset($imporArr[$referralContact->importance]))?$imporArr[$referralContact->importance]:"";    
                        }
                        ?> 
                        {!! Form::text('referral_importance','',['class'=>'form-control', 'placeholder'=>$imporText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_importance') }}</span>
                    </div>
                </div>

                <!-- sensitivity -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $senseArr       = [1=>'sensitivity text1',2=>'sensitivity text2',3=>'sensitivity text3'];
                        $senseText    = "";
                        if(isset($referralContact->sensitivity)) {
                            $senseText    = (isset($senseArr[$referralContact->sensitivity]))?$senseArr[$referralContact->sensitivity]:"";    
                        }
                        ?> 
                        {!! Form::text('referral_sensitivity','',['class'=>'form-control', 'placeholder'=>$senseText,'readonly' => 'readonly']) !!}


                        <span class="text-danger">{{ $errors->first('referral_sensitivity') }}</span>
                    </div>
                </div>

                <!-- Private -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Private  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <label>
                            <?php
                                $privateYes = $privateNo = false; 
                                if(isset($referralContact->status)) {
                                    $privateYes = ($referralContact->private == 1?true:false);
                                    $privateNo  = ($referralContact->private == 0?true:false);
                                }
                            ?>
                            {!! Form::radio('referral_private', '1', $privateYes, ['class' => 'flat','disabled' => 'disabled' ]) !!}  Yes
                        </label>
                        <label>
                            {!! Form::radio('referral_private', '0', $privateNo, ['class' => 'flat','disabled' => 'disabled']) !!}  No
                        </label>
                    </div>
                </div>

                <!-- Sponsorships -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sponsorships</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $sponseArr      = ['1' => 'JPMorgan' , '2' => 'Microsoft', '3' => 'Deloitte', '4' => 'PepsiCo', '5' => 'DHL Express'];
                        $sponseText     = "";
                        if(isset($referralContact->sponsorships)) {

                            if($referralContact->sponsorships != "") {
                                foreach (explode(',',$referralContact->sponsorships) as $key => $sponsor) {
                                    $sponseText   .= $sponseArr[$sponsor].", ";
                                }   
                                $sponseText   = ($sponseText != "")?rtrim($sponseText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('referral_sponsorships','',['class'=>'form-control', 'placeholder'=>$sponseText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_sponsorships') }}</span>
                    </div>
                </div>

                <!-- Political Party -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Party </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $politicalArr      = ['1' => 'Movement of the future' , '2' => 'Kataeb Party', '3' => 'Hezbollah', '4' => 'Free Patriotic Movement'];
                        $politicalText     = "";
                        if(isset($referralContact->political_party)) {

                            if($referralContact->political_party != "") {
                                foreach (explode(',',$referralContact->political_party) as $key => $political) {
                                    $politicalText   .= $politicalArr[$political].", ";
                                }   
                                $politicalText   = ($politicalText != "")?rtrim($politicalText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('referral_political_party[]','',['class'=>'form-control', 'placeholder'=>$politicalText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_political_party') }}</span>
                    </div>
                </div>

                <!-- Religion -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Religion </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $religionArr       = ['1' => 'Christian', '2' => 'Muslim'];
                        $senseText    = "";
                        if(isset($referralContact->religion_id)) {
                            $senseText    = (isset($religionArr[$referralContact->religion_id]))?$religionArr[$referralContact->religion_id]:"";    
                        }
                        ?> 
                        {!! Form::text('referral_sensitivity','',['class'=>'form-control', 'placeholder'=>$senseText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_religion') }}</span>
                    </div>
                </div>

                <!-- Administrative Group -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Administrative Group </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $adminGpArr     =  ['1' => 'Business Group' , '2' => 'Volunteer Group', '3' => 'Administration Group', '4' => 'Management Group'];
                        $adminText      = "";
                        if(isset($referralContact->administrative_group)) {

                            if($referralContact->administrative_group != "") {
                                foreach (explode(',',$referralContact->administrative_group) as $key => $lang) {
                                    $adminText   .= $adminGpArr[$lang].", ";
                                }   
                                $adminText   = ($adminText != "")?rtrim($adminText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('referral_administrative_group[]','',['class'=>'form-control', 'placeholder'=>$adminText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_administrative_group') }}</span>
                    </div>
                </div>

                <!-- Notes -->
                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $notes     = isset($referralContact->notes)?$referralContact->notes:'';
                        ?>
                        {!! Form::textarea('referral_notes','',['class'=>'form-control', 'placeholder'=>$notes,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_notes') }}</span>
                    </div>
                </div> -->
            </div>

            <!-- rigth -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">

                <!-- photo box -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-right">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3 my-photo-box-view">
                        <div style="text-align: left;float:none;">
                            <?php
                            if(isset($referralContact->photo) && $referralContact->photo != "") {
                            ?>
                                <img src="{{ $imageUrl.$referralContact->photo }}" class="contact_spouse_photo img-thumbnail" >
                            <?php
                            } else {
                            ?>
                                <img src="{{ url('/uploads/contact/avatar.png') }}" class="contact_spouse_photo img-thumbnail" >
                            <?php 
                            }
                            ?>
                            <input type="hidden" name="old_contact_referral_image" value="{{ isset($referralContact->photo)?$referralContact->photo:'' }}">
                        </div>
                    </div>
                </div>

                <!-- nick name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $nickname       = isset($referralContact->nickname)?$referralContact->nickname:'';
                        ?>
                        {!! Form::text('referral_nick_name','',['class'=>'form-control', 'placeholder'=>$nickname,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_nick_name') }}</span>
                    </div>
                </div>

                <!-- birth place -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Birth Place</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $birthplaceText          = "";
                        if(isset($referralContact->birth_place)) {
                            $birthplaceText   = (isset($nationality[$referralContact->birth_place]))?$nationality[$referralContact->birth_place]:"";
                        }
                        ?> 
                        {!! Form::text('referral_birth_place','',['class'=>'form-control', 'placeholder'=>$birthplaceText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_birth_place') }}</span>
                    </div>
                </div>

                <!-- gender -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Gender</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $genderText          = "";
                        if(isset($referralContact->gender)) {
                            $genderText   = (isset($gender[$referralContact->gender]))?$gender[$referralContact->gender]:"";
                        }
                        ?> 
                        {!! Form::text('referral_gender','',['class'=>'form-control', 'placeholder'=>$genderText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_gender') }}</span>
                    </div>
                </div>

                <!-- nationality -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nationality</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $nationText      = "";
                        if(isset($referralContact->nationality)) {

                            if($referralContact->nationality != "") {
                                foreach (explode(',',$referralContact->nationality) as $key => $nation) {
                                    $nationText   .= $nationality[$nation].", ";
                                }   
                                $nationText   = ($nationText != "")?rtrim($nationText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('referral_nationality[]','',['class'=>'form-control', 'placeholder'=>$nationText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_nationality') }}</span>
                    </div>
                </div>

                <!-- profession -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Profession </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $profArr        = ['1' => 'Profession 1' , '2' => 'Profession 2' , '3' => 'Profession 3', '4' => 'Profession 4', '5' => 'Profession 5'];
                        $profText     = "";
                        if(isset($referralContact->profession)) {

                            if($referralContact->profession != "") {
                                foreach (explode(',',$referralContact->profession) as $key => $prof) {
                                    $profText   .= isset($profArr[$prof])?$profArr[$prof].", ":"";
                                }   
                                $profText   = ($profText != "")?rtrim($profText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('referral_profession[]','',['class'=>'form-control', 'placeholder'=>$profText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_profession') }}</span>
                    </div>
                </div>

                <!-- caution -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $causionArr         = ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'];           
                        $causionText        = "";
                        if(isset($referralContact->caution)) {
                            $causionText   = (isset($causionArr[$referralContact->caution]))?$causionArr[$referralContact->caution]:"";
                        }
                        ?> 
                        {!! Form::text('referral_caution','',['class'=>'form-control', 'placeholder'=>$causionText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('referral_caution') }}</span>
                    </div>
                </div>

                <!-- Business Owner -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Business Owner</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $businessOwnYes = $businessOwnNo = false;
                        if(isset($referralContact->business_owner)) {
                            $businessOwnYes     = $referralContact->business_owner == 1?true:false;

                            $businessOwnNo      = $referralContact->business_owner == 0?true:false;
                        ?>
                        <label>
                            {!! Form::radio('business_owner', '1',$businessOwnYes,['class' => 'flat','disabled' => 'disabled']) !!}
                        </label> 
                        <label>
                            {!! Form::radio('business_owner', '0',$businessOwnNo,['class' => 'flat','disabled' => 'disabled']) !!}  No
                        </label>   
                        <?php
                        }
                        ?>

                        <span class="text-danger">{{ $errors->first('referral_business_owner') }}</span>
                    </div>
                </div>

                <!-- Charitable Organizations -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Charitable Organizations </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $charityArr        = ['1' => 'YSR FOUNDATION ' , '2' => 'THE COCONADA CHAMBER OF COMMERCE', '3' => 'RAVINDRANATH MEDICAL FOUNDATION'];
                        $charityText     = "";
                        if(isset($referralContact->charitable_organizations)) {

                            if($referralContact->charitable_organizations != "") {
                                foreach (explode(',',$referralContact->charitable_organizations) as $key => $charity) {
                                    $charityText   .= $charityArr[$charity].", ";
                                }   
                                $charityText   = ($charityText != "")?rtrim($charityText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('referral_charitable_organizations[]','',['class'=>'form-control', 'placeholder'=>$charityText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_charitable_organizations') }}</span>
                    </div>
                </div>
                <!-- Relationship Preference -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Relationship Preference </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                         <?php
                        $RelArr        = ['1' => 'Relationship Preference 1' , '2' => 'Relationship Preference 2', '3' => 'Relationship Preference 3', '4' => 'Relationship Preference 4'];
                        $relText     = "";
                        if(isset($referralContact->relationship_preference)) {

                            if($referralContact->relationship_preference != "") {
                                foreach (explode(',',$referralContact->relationship_preference) as $key => $rel) {
                                    $relText   .= $RelArr[$rel].", ";
                                }   
                                $relText   = ($relText != "")?rtrim($relText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('referral_relationship_preference[]','',['class'=>'form-control', 'placeholder'=>$relText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_relationship_preference') }}</span>
                    </div>
                </div>

                <!-- Internal Organizations -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Internal Organizations </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $intOrgArr        = ['1' => 'Makhzoumi Foundation' , '2' => 'Beirutiyat', '3' => 'Makhzoumi Health Care', '4' => 'Makhzoumi Development'];
                        $intOrgText     = "";
                        if(isset($referralContact->internal_organizations)) {

                            if($referralContact->internal_organizations != "") {
                                foreach (explode(',',$referralContact->internal_organizations) as $key => $rel) {
                                    $intOrgText   .= $intOrgArr[$rel].", ";
                                }   
                                $intOrgText   = ($intOrgText != "")?rtrim($intOrgText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('referral_internal_organizations[]','',['class'=>'form-control', 'placeholder'=>$intOrgText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_internal_organizations') }}</span>
                    </div>
                </div>

                <!-- Status -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Status  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <label>
                            <?php
                                $statusYes = $statusNo = false; 
                                if(isset($referralContact->status)) {
                                    $statusYes = ($referralContact->status == 1?true:false);
                                    $statusNo  = ($referralContact->status == 0?true:false);
                                }
                            ?>
                            {!! Form::radio('referral_status', '1', $statusYes, ['class' => 'flat','disabled' => 'disabled' ]) !!}  Active
                        </label>
                        <label>
                            {!! Form::radio('referral_status', '0', $statusNo, ['class' => 'flat','disabled' => 'disabled']) !!}  Inactive
                        </label>
                    </div>
                </div>

                <!-- Keywords -->
                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label"> Keywords  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $keyText      = isset($referralContact->notes)?$referralContact->notes:'';
                        ?>
                        {!! Form::textarea('referral_keywords','',['class'=>'form-control', 'placeholder'=>$keyText,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('referral_keywords') }}</span>
                    </div>
                </div> -->
            </div>

            <!-- keyword and notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <!-- Keywords -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        {!! Form::text('keywords',isset($referralContact->keywords)?$referralContact->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                    </div>
                </div>
                
                <!-- Notes -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php 
                        $notes     = isset($referralContact->notes)?$referralContact->notes:'';
                        ?>
                        {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                    </div>
                </div>
            </div>

            <!-- left -->
            <!-- created section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Created by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($referralContact->user_createby['first_name'])?$referralContact->user_createby['first_name']:'';
                        $lastName   = isset($referralContact->user_createby['last_name'])?$referralContact->user_createby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Created on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($referralContact->created_at)?date_format($referralContact->created_at, 'j M Y g:i A'):'';
                        ?>

                        {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- right -->
            <!-- updated section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Updated by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($referralContact->user_updateby['first_name'])?$referralContact->user_updateby['first_name']:'';
                        $lastName   = isset($referralContact->user_updateby['last_name'])?$referralContact->user_updateby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Updated on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($referralContact->updated_at)?date_format($referralContact->updated_at, 'j M Y g:i A'):'';
                        ?>
                        {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- submit section -->
            <?php
            $i = 0;
            if($i == 1) {
            ?>    
                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_referral')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                    </div>
                </div>
            <?php 
            }
            ?>
        {!! Form::close() !!}
        </div>
    </div>
</div>