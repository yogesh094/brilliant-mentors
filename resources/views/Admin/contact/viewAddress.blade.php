<div role="tabpanel" class="tab-pane fade" id="address"  >
    <div class="x_title">
        <h4>Address </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#address') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <!-- if there are creation errors, they will show here -->
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
            {!! Form::open(array('url' => ('contact/contactAddressStore'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!} 

                {!! Form::hidden('id',isset($addressContact->id)?$addressContact->id:"") !!}
                {!! Form::hidden('contactId', $contactId) !!}
                <!-- left side  -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <!-- type -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Type  </label>

                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $addrType       = ['1'=>'Office', '2' => 'Residential'];

                            $addrTypeText   = "";
                            if(isset($addressContact->type)) {
                                $addrTypeText   = (isset($addrType[$addressContact->type]))?$addrType[$addressContact->type]:"";    
                            }
                            ?>    
                            {!! Form::text('type','',['class'=>'form-control', 'placeholder'=>$addrTypeText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- country -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Country</label>

                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $countryText        = "";    
                            if(isset($addressContact->country_id)) {
                                $countryText    = isset($nationality[$addressContact->country_id])?$nationality[$addressContact->country_id]:"";
                            }
                            ?>
                            {!! Form::text('country_id','',['class'=>'form-control', 'placeholder'=>$countryText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- province -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Province </label>
                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $provinceText       = "";
                            if(isset($addressContact->province_id)){
                                $provinceText   = isset($province[$addressContact->province_id])?$province[$addressContact->province_id]:"";    
                            }
                            ?>
                            {!! Form::text('province_id','',['class'=>'form-control', 'placeholder'=>$provinceText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- city_id -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">City  </label>

                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $cityText       = "";
                            if(isset($addressContact->city_id)) {
                                $cityText   = isset($city[$addressContact->city_id])?$city[$addressContact->city_id]:"";    
                            }
                            
                            ?> 

                            {!! Form::text('city_id','',['class'=>'form-control', 'placeholder'=>$cityText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- po_box -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Po Box  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $poboxText      = isset($addressContact->po_box)?$addressContact->po_box:"";
                            ?> 
                            {!! Form::text('po_box','',['class'=>'form-control', 'placeholder'=>$poboxText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- address_line1 -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Address Line 1  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $addrLine1      = isset($addressContact->address_line1)?$addressContact->address_line1:"";
                            ?> 
                            {!! Form::text('address_line1','',['class'=>'form-control', 'placeholder'=>$addrLine1,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- website -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Website  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $websiteText    = isset($addressContact->website)?$addressContact->website:"";
                            ?> 
                            {!! Form::text('website','',['class'=>'form-control', 'placeholder'=>$websiteText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- caution -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Caution </label>

                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php 
                            $caustionArr    = ['1' => 'Low' , '2' => 'Medium', '3' => 'High'];
                            $causionText        = "";
                            if(isset($addressContact->caution)) {
                                $causionText    = isset($caustionArr[$addressContact->caution])?$caustionArr[$addressContact->caution]:"";
                            }
                            
                            ?>
                            {!! Form::text('caution','',['class'=>'form-control', 'placeholder'=>$causionText,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>
                </div>

                <!-- right side  -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    

                    <!-- region_id -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Region </label>
                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $regionText         = "";
                            if(isset($addressContact->region_id)) {
                                $regionText     = isset($region[$addressContact->region_id])?$region[$addressContact->region_id]:"";    
                            }
                            ?> 
                            {!! Form::text('region_id','',['class'=>'form-control', 'placeholder'=>$regionText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- district -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">District </label>

                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            
                            <?php
                            $districtText       = "";
                            if(isset($addressContact->district_id)) {
                                $districtText   = isset($district[$addressContact->district_id])?$district[$addressContact->district_id]:"";    
                            }
                            
                            ?> 

                            {!! Form::text('district_id','',['class'=>'form-control', 'placeholder'=>$districtText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>
                    
                    <!-- postal code -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Postalcode </label>
                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $postcodeText   = isset($addressContact->postal_code)?$addressContact->postal_code:"";
                            ?> 
                            {!! Form::text('postal_code','',['class'=>'form-control', 'placeholder'=>$postcodeText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- importance -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Importance </label>
                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php 
                                $imporArr   = ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
                                $importanceText         = "";
                                if(isset($addressContact->importance)) {
                                    $importanceText     = isset($imporArr[$addressContact->importance])?$imporArr[$addressContact->importance]:"";    
                                }
                                
                            ?>
                            {!! Form::text('importance','',['class'=>'form-control', 'placeholder'=>$importanceText,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- sensitivity -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Sensitivity  </label>

                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php 
                            $senseArr   = ['1' => 'Low' , '2' => 'Medium', '3' => 'High'];
                            $sensitivityText        = "";
                            if(isset($addressContact->sensitivity)) {
                                $sensitivityText    = isset($senseArr[$addressContact->sensitivity])?$senseArr[$addressContact->sensitivity]:"";    
                            }
                            
                            ?>
                            {!! Form::text('sensitivity','',['class'=>'form-control', 'placeholder'=>$sensitivityText,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>

                    <!-- address_line2 -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 ">Address Line 2  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <?php
                            $addrLine2      = isset($addressContact->address_line2)?$addressContact->address_line2:"";
                            ?> 
                            {!! Form::text('address_line2','',['class'=>'form-control', 'placeholder'=>$addrLine2,'readonly' => 'readonly']) !!}
                        </div>
                    </div>

                    <!-- delivery_address -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label ">
                        Delivery Address  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12 ">
                            <label>
                                <?php
                                $deliveryAddrYes        = false;
                                if(isset($addressContact->delivery_address)) {
                                    $deliveryAddrYes    = $addressContact->delivery_address == 1?true:false;    
                                }
                                
                                ?>
                                {!! Form::radio('delivery_address', '1',$deliveryAddrYes,['class' => 'flat','disabled' => 'disabled']) !!}  Yes
                            </label>
                            <label>
                                <?php
                                $deliveryAddrNo         = false;
                                if(isset($addressContact->delivery_address)) {
                                    $deliveryAddrNo     = $addressContact->delivery_address == 0?true:false;    
                                }
                                
                                ?>
                                {!! Form::radio('delivery_address', '0',$deliveryAddrNo,['class' => 'flat','disabled' => 'disabled']) !!}  No
                            </label>
                            
                        </div>
                    </div>
                </div>

                <!-- keyword and notes -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <!-- Keywords -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            {!! Form::text('keywords',isset($addressContact->keywords)?$addressContact->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                        </div>
                    </div>
                    
                    <!-- Notes -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <?php 
                            $notes     = isset($addressContact->notes)?$addressContact->notes:'';
                            ?>
                            {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                        </div>
                    </div>
                </div>

                <!-- left -->
                <!-- created section -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <!-- Created by -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Created by  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $firstName  = isset($addressContact->user_createby['first_name'])?$addressContact->user_createby['first_name']:'';
                            $lastName   = isset($addressContact->user_createby['last_name'])?$addressContact->user_createby['last_name']:'';
                            $createdBy  = $firstName.' '.$lastName;
                            ?>
                            {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>

                    <!-- Created on -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Created on  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $createdAt   = isset($addressContact->created_at)?date_format($addressContact->created_at, 'j M Y g:i A'):'';
                            ?>

                            {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>
                </div>

                <!-- right -->
                <!-- updated section -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <!-- Updated by -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Updated by  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $firstName  = isset($addressContact->user_updateby['first_name'])?$addressContact->user_updateby['first_name']:'';
                            $lastName   = isset($addressContact->user_updateby['last_name'])?$addressContact->user_updateby['last_name']:'';
                            $createdBy  = $firstName.' '.$lastName;
                            ?>
                            {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>

                    <!-- Updated on -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Updated on  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $createdAt   = isset($addressContact->updated_at)?date_format($addressContact->updated_at, 'j M Y g:i A'):'';
                            ?>
                            {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>
                </div>

                <!-- submit section -->
                <?php 
                $i = 0;
                if($i == 1) {
                ?>
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                            {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}

                            <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                        </div>
                    </div>
                <?php 
                }   
                ?>

            {!! Form::close() !!}
        </div>
    </div>
</div>