<div role="tabpanel" class="tab-pane fade" id="greetingsLetter" >
    {{-- \Session::get('submit_greetings_letter')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Greetings Letter </h4>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if (Session::has('educationSuccess'))
            <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('educationSuccess') !!}</div>
        @endif

        @if (Session::has('greetingSuccess'))
            <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('greetingSuccess') !!}</div>
        @endif

        <div class="alert alert-danger contact-greetingsLetter-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your input.
        </div>

        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/greetings-letter-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($greetingsContact->id)?$greetingsContact->id:"") !!}
            {!! Form::hidden('contact_id', $contact->id) !!}

            {!! Form::hidden('contact_political_position_id', isset($politicalPositionContact->id)?$politicalPositionContact->id:'') !!}
            {!! Form::hidden('contact_dependents_id', isset($dependentsContact->id)?$dependentsContact->id:'') !!}
            {!! Form::hidden('contact_spouse_id', isset($spouseContact->id)?$spouseContact->id:'') !!}
            {!! Form::hidden('contact_referral_id', isset($referralContact->id)?$referralContact->id:'') !!}
            {!! Form::hidden('contact_representative_id', isset($representativeContact->id)?$representativeContact->id:'') !!}
        <!-- left -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Title -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Title <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('greeting_title', ['1' => 'Contact','2'=>'Business','3'=>'Spouse','4'=>'Dependents','5'=>'Referral','6'=>'Representative'], isset($greetingsContact->title) ? $greetingsContact->title:'', ['placeholder' => 'Select Title', 'class' => 'form-control', 'id' =>'greetingTitle']) }}
                    <span class="text-danger GreetingTitleError">{{ $errors->first('greeting_title') }}</span>
                </div>
            </div>

            <!-- sensitivity -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('greeting_sensitivity', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],isset($greetingsContact->sensitivity)?$greetingsContact->sensitivity:'', ['placeholder' => 'Select Sensitivity','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('greeting_sensitivity') }}</span>
                </div>
            </div>

            <!-- Greeting Intro -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Greeting Intro <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::textarea('greeting_intro',isset($greetingsContact->grettings_intro) ? $greetingsContact->grettings_intro:'', ['placeholder' => 'Greeting Intro','class' => 'form-control','id' =>'grettingIntro','size' => '2x2']) }}
                    <span class="text-danger GreetingIntroError">{{ $errors->first('greeting_intro') }}</span>
                </div>
            </div>
        </div>

        <!-- right -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- importance -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('greeting_importance', ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'], isset($greetingsContact->importance)?$greetingsContact->importance:'', ['placeholder' => 'Select Importance','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('greeting_importance') }}</span>
                </div>
            </div>

            <!-- caution -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('greeting_caution', ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'],isset($greetingsContact->caution)?$greetingsContact->caution:'',['placeholder' => 'Select Caution','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('greeting_caution') }}</span>
                </div>
            </div>

            <!-- Grettings Ending -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Grettings Ending <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::textarea('greeting_ending',isset($greetingsContact->grettings_ending) ? $greetingsContact->grettings_ending:'', ['placeholder' => 'Greeting Ending','class' => 'form-control','id' => 'greetingEnding','size' => '2x2']) }}
                    <span class="text-danger GreetingEndingError">{{ $errors->first('greeting_ending') }}</span>
                </div>
            </div>
        </div>

        <!-- keywords -->
        <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    keywords
                </label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('greeting_keywords',isset($greetingsContact->keywords)?$greetingsContact->keywords:'',['id'=>'greetingsKeywords','class'=>'form-control tagInput', 'placeholder'=>'Type keyword']) !!}
                    <span class="text-danger">{{ $errors->first('greeting_keywords') }}</span>
                </div>
            </div>
        </div>

        <!-- notes -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
               <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    Notes
                </label>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::textarea('greeting_notes', isset($greetingsContact->notes)?$greetingsContact->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'Notes','size' => '3x3']) !!}
                <span class="text-danger">{{ $errors->first('greeting_notes') }}</span>
                </div>
            </div>
        </div>

        <!-- left -->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($greetingsContact->user_createby['first_name'])?$greetingsContact->user_createby['first_name']:'';
                    $lastName   = isset($greetingsContact->user_createby['last_name'])?$greetingsContact->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($greetingsContact->created_at)?date_format($greetingsContact->created_at, 'jS M Y g:iA'):'';
                    ?>

                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- right -->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($greetingsContact->user_updateby['first_name'])?$greetingsContact->user_updateby['first_name']:'';
                    $lastName   = isset($greetingsContact->user_updateby['last_name'])?$greetingsContact->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($greetingsContact->updated_at)?date_format($greetingsContact->updated_at, 'jS M Y g:iA'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- submit section -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit','name'=>'submit_greetings_letter')) !!}

                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>