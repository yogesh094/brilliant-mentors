<div role="tabpanel" class="tab-pane fade" id="contactNumber" >
    {{-- \Session::get('submit_contact_number')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Contact Number </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#contactNumber') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/contact-number-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($contact->id)?$contact->id:'') !!}   

            {!! Form::hidden('contact_id', isset($contactNumber->contact_id)?$contactNumber->contact_id:'') !!}
            
            {!! Form::hidden('political_position_id', isset($politicalPositionContact->id)?$politicalPositionContact->id:'') !!}
            {!! Form::hidden('contact_dependents_id', isset($dependentsContact->id)?$dependentsContact->id:'') !!}
            {!! Form::hidden('contact_spouse_id', isset($spouseContact->id)?$spouseContact->id:'') !!}
            {!! Form::hidden('contact_referral_id', isset($referralContact->id)?$referralContact->id:'') !!}
            {!! Form::hidden('contact_representative_id', isset($representativeContact->id)?$representativeContact->id:'') !!}

            <!-- left -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">

                <!-- Contact Number -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number</label>
                    <!-- Country Code -->
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <?php
                        $contryCode     = "";
                        if(isset($contactNumber->country_code)) {
                            $contryCode = (isset($countryCode[$contactNumber->country_code]))?$countryCode[$contactNumber->country_code]:"";    
                        }
                        ?> 
                        {!! Form::text('contact_country_code','',['class'=>'form-control', 'placeholder'=>$contryCode,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('contact_country_code') }}</span>
                    </div>

                    <!-- Phone number -->
                    <div class="col-md-6 col-sm-6 col-xs-9">
                        <?php 
                        $number     = isset($contactNumber->number)?$contactNumber->number:'';
                        ?>
                        {!! Form::text('contact_phone_number','',['class'=>'form-control', 'placeholder'=>$number,'readonly' => 'readonly']) !!}
                        <span class="text-danger">{{ $errors->first('contact_phone_number') }}</span>
                    </div>
                </div>

                <!-- Category -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $catArr         = ['1' => 'Contact Category1' , '2' => 'Contact Category2', '3' => 'Contact Category3','4' => 'Contact Category4'];
                        $category     = "";
                        if(isset($contactNumber->category)) {
                            $category = (isset($catArr[$contactNumber->category]))?$catArr[$contactNumber->category]:"";    
                        }
                        ?> 
                        {!! Form::text('contact_country_code','',['class'=>'form-control', 'placeholder'=>$category,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('contact_category') }}</span>
                    </div>
                </div>

                <!-- importance -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $imporArr       = ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
                        $imporText    = "";
                        if(isset($contactNumber->importance)) {
                            $imporText    = (isset($imporArr[$contactNumber->importance]))?$imporArr[$contactNumber->importance]:"";    
                        }
                        ?> 
                        {!! Form::text('contact_importance','',['class'=>'form-control', 'placeholder'=>$imporText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('contact_importance') }}</span>
                    </div>
                </div>

                <!-- sensitivity -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $senseArr       = [1=>'sensitivity text1',2=>'sensitivity text2',3=>'sensitivity text3'];
                        $senseText    = "";
                        if(isset($contactNumber->sensitivity)) {
                            $senseText    = (isset($senseArr[$contactNumber->sensitivity]))?$senseArr[$contactNumber->sensitivity]:"";    
                        }
                        ?> 
                        {!! Form::text('contact_sensitivity','',['class'=>'form-control', 'placeholder'=>$senseText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('contact_sensitivity') }}</span>
                    </div>
                </div>

                <!-- Keywords -->
                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Keywords</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $keyword      = isset($contactNumber->keywords)?$contactNumber->keywords:"";
                        ?>
                        {!! Form::textarea('contact_keywords','',['class'=>'form-control', 'placeholder'=>$keyword,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('contact_keywords') }}</span>
                    </div>
                </div> -->
            </div>

            <!-- right -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">

                <!-- Phone Number Type -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Phone Number Type</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $numberArr         = ['1' => 'Business','2'=>'Mobile','3'=>'Home'];
                        $phonetype     = "";
                        if(isset($contactNumber->phone_type)) {
                            $phonetype = (isset($numberArr[$contactNumber->phone_type]))?$numberArr[$contactNumber->phone_type]:"";    
                        }
                        ?> 
                        {!! Form::text('contact_number_type','',['class'=>'form-control', 'placeholder'=>$phonetype,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('contact_number_type') }}</span>
                    </div>
                </div>

                <!-- Fax -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $fax      = isset($contactNumber->fax_number)?$contactNumber->fax_number:"";
                        ?>
                        {!! Form::text('contact_fax','',['class'=>'form-control', 'placeholder'=>$fax,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('contact_fax') }}</span>
                    </div>
                </div>

                <!-- caution -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $causionArr     = ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'];
                        $cautionText    = "";    
                        if(isset($contactNumber->caution)) {
                            $cautionText = isset($causionArr[$contactNumber->caution])?$causionArr[$contactNumber->caution]:"";
                        }
                        
                        ?> 

                        {!! Form::text('contact_caution','',['class'=>'form-control', 'placeholder'=>$cautionText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('contact_caution') }}</span>
                    </div>
                </div>

                <!-- Notes -->
                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $notes      = isset($contactNumber->notes)?$contactNumber->notes:"";
                        ?>
                        {!! Form::textarea('contact_notes','',['class'=>'form-control', 'placeholder'=>$notes,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('contact_notes') }}</span>
                    </div>
                </div> -->
            </div>

            <!-- keyword and notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <!-- Keywords -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        {!! Form::text('keywords',isset($contactNumber->keywords)?$contactNumber->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                    </div>
                </div>
                
                <!-- Notes -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php 
                        $notes     = isset($contactNumber->notes)?$contactNumber->notes:'';
                        ?>
                        {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                    </div>
                </div>
            </div>

            <!-- left -->
            <!-- created section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Created by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($contactNumber->user_createby['first_name'])?$contactNumber->user_createby['first_name']:'';
                        $lastName   = isset($contactNumber->user_createby['last_name'])?$contactNumber->user_createby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Created on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($contactNumber->created_at)?date_format($contactNumber->created_at, 'j M Y g:i A'):'';
                        ?>

                        {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- right -->
            <!-- updated section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Updated by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($contactNumber->user_updateby['first_name'])?$contactNumber->user_updateby['first_name']:'';
                        $lastName   = isset($contactNumber->user_updateby['last_name'])?$contactNumber->user_updateby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Updated on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($contactNumber->updated_at)?date_format($contactNumber->updated_at, 'j M Y g:i A'):'';
                        ?>
                        {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- submit section -->
            <?php 
            $i = 0;
            if($i == 1) {
            ?>
                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_contact_number')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                    </div>
                </div>
            <?php 
            }
            ?>
        {!! Form::close() !!}
        </div>
    </div>
</div>