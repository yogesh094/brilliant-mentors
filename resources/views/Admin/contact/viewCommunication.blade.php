<div role="tabpanel" class="tab-pane fade" id="communication" >
    {{-- \Session::get('submit_communication')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Communication </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#communication') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/communication-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($contact->id)?$contact->id:'') !!}
            {!! Form::hidden('contact_id', isset($communicationContact->contact_id)?$communicationContact->contact_id:'') !!}
            {!! Form::hidden('contact_political_position_id', isset($politicalPositionContact->id)?$politicalPositionContact->id:'') !!}
            {!! Form::hidden('contact_dependents_id', isset($dependentsContact->id)?$dependentsContact->id:'') !!}
            {!! Form::hidden('contact_spouse_id', isset($spouseContact->id)?$spouseContact->id:'') !!}
            {!! Form::hidden('contact_referral_id', isset($referralContact->id)?$referralContact->id:'') !!}
            {!! Form::hidden('contact_representative_id', isset($representativeContact->id)?$representativeContact->id:'') !!}

            <!-- left -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                
                <!-- Email -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $email     = isset($communicationContact->email)?$communicationContact->email:'';
                        ?>
                        {!! Form::text('spouse_nick_name','',['class'=>'form-control', 'placeholder'=>$email,'readonly' => 'readonly']) !!}
                        <span class="text-danger">{{ $errors->first('communication_email') }}</span>
                    </div>
                </div>

                <!-- Category -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $catArr         = ['1' => 'Contact Category1' , '2' => 'Contact Category2', '3' => 'Contact Category3','4' => 'Contact Category4'];
                        $category     = "";
                        if(isset($communicationContact->category)) {
                            $category = (isset($catArr[$communicationContact->category]))?$catArr[$communicationContact->category]:"";    
                        }
                        ?> 
                        {!! Form::text('communication_category','',['class'=>'form-control', 'placeholder'=>$category,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_category') }}</span>
                    </div>
                </div>

                <!-- Email BCC -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Email BCC</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $emailBcc     = isset($communicationContact->email_bcc)?$communicationContact->email_bcc:'';
                        ?>
                        {!! Form::text('communication_email_bcc','',['class'=>'form-control', 'placeholder'=>$emailBcc,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_email_bcc') }}</span>
                    </div>
                </div>

                <!-- Facebook -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Facebook</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $facebook     = isset($communicationContact->facebook)?$communicationContact->facebook:'';
                        ?>
                        {!! Form::text('communication_facebook','',['class'=>'form-control', 'placeholder'=>$facebook,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_facebook') }}</span>
                    </div>
                </div>

                <!-- Instagram -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Instagram</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $instagram     =isset($communicationContact->instagram)?$communicationContact->instagram:'';
                        ?>
                        {!! Form::text('communication_instagram','',['class'=>'form-control', 'placeholder'=>$instagram,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_instagram') }}</span>
                    </div>
                </div>

                <!-- caution -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $causionArr     = ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'];
                        $cautionText    = "";    
                        if(isset($communicationContact->caution)) {
                            $cautionText = isset($causionArr[$communicationContact->caution])?$causionArr[$communicationContact->caution]:"";
                        }
                        
                        ?> 

                        {!! Form::text('communication_caution','',['class'=>'form-control', 'placeholder'=>$cautionText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_caution') }}</span>
                    </div>
                </div>

                <!-- Notes -->
                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $noteText       = isset($communicationContact->notes)?$communicationContact->notes:'';
                        ?>
                        {!! Form::textarea('communication_notes','',['class'=>'form-control', 'placeholder'=>$noteText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('communication_notes') }}</span>
                    </div>
                </div> -->
            </div>
           
            <!-- right -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">

                <!-- Email Type -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Type</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $emailTypeArr         = ['1' => 'Contact Category1' , '2' => 'Contact Category2', '3' => 'Contact Category3','4' => 'Contact Category4'];
                        $emailType     = "";
                        if(isset($communicationContact->email_type)) {
                            $emailType = (isset($emailTypeArr[$communicationContact->email_type]))?$emailTypeArr[$communicationContact->email_type]:"";    
                        }
                        ?> 
                        {!! Form::text('communication_email_type','',['class'=>'form-control', 'placeholder'=>$emailType,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_email_type') }}</span>
                    </div>
                </div>


                <!-- Email CC -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Email CC</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $emailCc     = isset($communicationContact->email_cc)?$communicationContact->email_cc:'';
                        ?>
                        {!! Form::text('communication_email_bcc','',['class'=>'form-control', 'placeholder'=>$emailCc,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_email_cc') }}</span>
                    </div>
                </div>

                <!-- Skype -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Skype</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $skype     = isset($communicationContact->skype)?$communicationContact->skype:'';
                        ?>
                        {!! Form::text('communication_skype','',['class'=>'form-control', 'placeholder'=>$skype,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_skype') }}</span>
                    </div>
                </div>

                <!-- Twitter -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Twitter</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $twitter     = isset($communicationContact->twitter)?$communicationContact->twitter:'';
                        ?>
                        {!! Form::text('communication_twitter','',['class'=>'form-control', 'placeholder'=>$twitter,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_twitter') }}</span>
                    </div>
                </div>

                <!-- importance -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $imporArr           = ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
                        $imporText          = "";
                        if(isset($communicationContact->importance)) {
                            $imporText   = (isset($imporArr[$communicationContact->importance]))?$imporArr[$communicationContact->importance]:"";
                        }
                        ?> 
                        {!! Form::text('communication_importance','',['class'=>'form-control', 'placeholder'=>$imporText,'readonly' => 'readonly']) !!}


                        <span class="text-danger">{{ $errors->first('communication_importance') }}</span>
                    </div>
                </div>

                <!-- sensitivity -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $senseArr           = [1=>'sensitivity text1',2=>'sensitivity text2',3=>'sensitivity text3'];
                        $senseText          = "";
                        if(isset($communicationContact->sensitivity)) {
                            $senseText   = (isset($senseArr[$communicationContact->sensitivity]))?$senseArr[$communicationContact->sensitivity]:"";
                        }
                        ?> 
                        {!! Form::text('communication_sensitivity','',['class'=>'form-control', 'placeholder'=>$senseText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('communication_sensitivity') }}</span>
                    </div>
                </div>
            </div>

            <!-- keyword and notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <!-- Keywords -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        {!! Form::text('keywords',isset($communicationContact->keywords)?$communicationContact->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                    </div>
                </div>
                
                <!-- Notes -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php 
                        $notes     = isset($communicationContact->notes)?$communicationContact->notes:'';
                        ?>
                        {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                    </div>
                </div>
            </div>

            <!-- left -->
            <!-- created section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Created by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($communicationContact->user_createby['first_name'])?$communicationContact->user_createby['first_name']:'';
                        $lastName   = isset($communicationContact->user_createby['last_name'])?$communicationContact->user_createby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Created on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($communicationContact->created_at)?date_format($communicationContact->created_at, 'j M Y g:i A'):'';
                        ?>

                        {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- right -->
            <!-- updated section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Updated by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($communicationContact->user_updateby['first_name'])?$communicationContact->user_updateby['first_name']:'';
                        $lastName   = isset($communicationContact->user_updateby['last_name'])?$communicationContact->user_updateby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Updated on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($communicationContact->updated_at)?date_format($communicationContact->updated_at, 'j M Y g:i A'):'';
                        ?>
                        {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- <div class="ln_solid"></div> -->

            <!-- submit section -->
            <?php 
            $i = 0;
            if($i == 1) {
            ?>
                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_communication')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                    </div>
                </div>
            <?php 
            }
            ?>
        {!! Form::close() !!}
        </div>
    </div>
</div>