<div role="tabpanel" class="tab-pane fade" id="education" >
    {{-- \Session::get('submit_education')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Education </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#education') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/education-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($contact->id)?$contact->id:'') !!}
            {!! Form::hidden('contact_id', isset($educationContact->contact_id)?$educationContact->contact_id:'') !!}
            {!! Form::hidden('contact_political_position_id', isset($politicalPositionContact->id)?$politicalPositionContact->id:'') !!}
            {!! Form::hidden('contact_dependents_id', isset($dependentsContact->id)?$dependentsContact->id:'') !!}
            {!! Form::hidden('contact_spouse_id', isset($spouseContact->id)?$spouseContact->id:'') !!}
            {!! Form::hidden('contact_referral_id', isset($referralContact->id)?$referralContact->id:'') !!}
            {!! Form::hidden('contact_representative_id', isset($representativeContact->id)?$representativeContact->id:'') !!}
        <!-- left -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">

            <!-- Qualification -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Qualification</label>
                <div class="col-md-9 col-sm-9 col-xs-12">

                    <?php
                    $qualificationArr         = ['1' => 'BE','2'=>'MCA'];
                    $qualification     = "";
                    if(isset($educationContact->qualification)) {
                        $qualification = (isset($emailTypeArr[$educationContact->qualification]))?$emailTypeArr[$educationContact->qualification]:"";    
                    }
                    ?> 
                    {!! Form::text('education_qualification','',['class'=>'form-control', 'placeholder'=>$qualification,'readonly' => 'readonly']) !!}

                    <span class="text-danger">{{ $errors->first('education_qualification') }}</span>
                </div>
            </div>

            <!-- Institute -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Institute</label>
                <div class="col-md-9 col-sm-9 col-xs-12">

                    <?php
                    $instituteArr         = ['1' => 'GUJ','2'=>'GTU'];
                    $institute     = "";
                    if(isset($educationContact->institute)) {
                        $institute = (isset($instituteArr[$educationContact->institute]))?$instituteArr[$educationContact->institute]:"";    
                    }
                    ?> 
                    {!! Form::text('education_institute','',['class'=>'form-control', 'placeholder'=>$institute,'readonly' => 'readonly']) !!}

                    <span class="text-danger">{{ $errors->first('education_institute') }}</span>
                </div>
            </div>

            <!-- Achivement -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Achivement</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php
                    $achievementArr         = ['1' => 'Education Achivement1' , '2' => 'Education Achivement2', '3' => 'Education Achivement3','4' => 'Education Achivement4'];
                    $achievement     = "";
                    if(isset($educationContact->achivement)) {
                        $achievement = (isset($achievementArr[$educationContact->achivement]))?$achievementArr[$educationContact->achivement]:"";    
                    }
                    ?> 
                    {!! Form::text('education_achivement','',['class'=>'form-control', 'placeholder'=>$achievement,'readonly' => 'readonly']) !!}

                    <span class="text-danger">{{ $errors->first('education_achivement') }}</span>
                </div>
            </div>

            <!-- sensitivity -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                <div class="col-md-9 col-sm-9 col-xs-12">

                    <?php
                    $senseArr       = [1=>'sensitivity text1',2=>'sensitivity text2',3=>'sensitivity text3'];
                    $senseText    = "";
                    if(isset($educationContact->sensitivity)) {
                        $senseText    = (isset($senseArr[$educationContact->sensitivity]))?$senseArr[$educationContact->sensitivity]:"";    
                    }
                    ?> 
                    {!! Form::text('education_senitivity','',['class'=>'form-control', 'placeholder'=>$senseText,'readonly' => 'readonly']) !!}

                    <span class="text-danger">{{ $errors->first('education_senitivity') }}</span>
                </div>
            </div>

            <!-- Date From -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date From </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php 
                    $dateFrom     = isset($educationContact->from_date)?date('m-d-Y',strtotime($educationContact->from_date)):'';
                    ?>
                    {!! Form::text('education_from_date','',['class'=>'form-control', 'placeholder'=>$dateFrom,'readonly' => 'readonly']) !!}

                    <span class="text-danger">{{ $errors->first('education_from_date') }}</span>
                </div>
            </div>

            <!-- Date To -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date To</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php 
                    $dateTo     = isset($educationContact->end_date)?date('m-d-Y',strtotime($educationContact->end_date)):'';
                    ?>
                    {!! Form::text('education_end_date','',['class'=>'form-control', 'placeholder'=>$dateTo,'readonly' => 'readonly']) !!}

                    <span class="text-danger">{{ $errors->first('education_end_date') }}</span>
                </div>
            </div>

            <!-- Certificate -->
            <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Certificate</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::File('education_certificate',['class'=>'form-control', 'placeholder'=>'Certificate']) !!}
                    <span class="text-danger">{{ $errors->first('education_certificate') }}</span>
                </div>
            </div> -->
        </div>

        <!-- right -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- photo box -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3 my-photo-box-view">
                    <div style="text-align: left;float:none;">
                        <?php
                        if(isset($educationContact->certificate) && $educationContact->certificate != "") {
                        ?>
                            <img src="{{ $imageUrl.$educationContact->certificate }}" class="contact_spouse_photo img-thumbnail" >
                        <?php
                        } else {
                        ?>
                            <img src="{{ url('/uploads/contact//certificate.png') }}" class="contact_spouse_photo img-thumbnail">
                        <?php 
                        }
                        ?>
                        <input type="hidden" name="old_contact_education_image" value="{{ isset($educationContact->certificate)?$educationContact->certificate:'' }}">
                    </div>
                </div>
            </div>

            <!-- Notes -->
            <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php 
                    $notes     = isset($educationContact->notes)?$educationContact->notes:'';
                    ?>
                    {!! Form::textarea('education_notes','',['class'=>'form-control', 'placeholder'=>$notes,'readonly' => 'readonly','rows' => 2]) !!}

                    <span class="text-danger">{{ $errors->first('education_notes') }}</span>
                </div>
            </div> -->

            <!-- Keywords -->
            <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Keywords</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php 
                    $keywords     = isset($educationContact->keywords)?$educationContact->keywords:'';
                    ?>
                    {!! Form::textarea('education_keywords','',['class'=>'form-control', 'placeholder'=>$keywords,'readonly' => 'readonly','rows' => 2]) !!}

                    <span class="text-danger">{{ $errors->first('education_keywords') }}</span>
                </div>
            </div> -->
        </div>

        <!-- keyword and notes -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <!-- Keywords -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                <div class="col-md-12 col-sm-12 col-xs-12">

                    {!! Form::text('keywords',isset($communicationContact->keywords)?$communicationContact->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                </div>
            </div>
            
            <!-- Notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <?php 
                    $notes     = isset($communicationContact->notes)?$communicationContact->notes:'';
                    ?>
                    {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                </div>
            </div>
        </div>

        <!-- left -->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($educationContact->user_createby['first_name'])?$educationContact->user_createby['first_name']:'';
                    $lastName   = isset($educationContact->user_createby['last_name'])?$educationContact->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($educationContact->created_at)?date_format($educationContact->created_at, 'j M Y g:i A'):'';
                    ?>

                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- right -->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($educationContact->user_updateby['first_name'])?$educationContact->user_updateby['first_name']:'';
                    $lastName   = isset($educationContact->user_updateby['last_name'])?$educationContact->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($educationContact->updated_at)?date_format($educationContact->updated_at, 'j M Y g:i A'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- submit section -->
        <?php 
        $i = 0;
        if($i == 1) {
        ?>
            <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_education')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        <?php 
        }
        ?>
        {!! Form::close() !!}
        </div>
    </div>
</div>