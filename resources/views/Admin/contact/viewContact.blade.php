@extends('Admin.master_layout.master')

@section('title', 'View Contact')

@section('breadcum')
     / View Contact
@endsection

@push('styles')

<link href="{{ asset('resources/admin-assets/css/daterangepicker.css') }}" rel="stylesheet">
<link href="{{ asset('resources/admin-assets/css/iCheck/skins/square/blue.css') }}" rel="stylesheet">

<link href="{{ asset('resources/admin-assets/css/editor/font-awesome.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
<link href="{{ asset('resources/admin-assets/css/editor/froala_editor.pkgd.min.css') }}" rel="stylesheet">
<link href="{{ asset('resources/admin-assets/css/editor/froala_style.min.css') }}" rel="stylesheet">
<link href="{{ asset('resources/admin-assets/css/tags_input/jquery.tagsinput.css') }}" rel="stylesheet">

<style type="text/css">
    .tagsinput .tag [data-role="remove"] { display: none; }
    .tagsinput div { display: none; }
    span.tag { background:#eee;color: #555; }
    div.fr-box.fr-basic.fr-top > div:last-child {
        display: none;
    }
</style>
@endpush
@section('content')

<div class="row">
    <div class="page-title">
        <div class="title_left">
            <h3>{{ $contact->full_name }}</h3>
        </div>
    </div>

    <div class="x_panel">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <div class="col-xs-2">
                <ul id="myTab" class="nav nav-tabs tabs-left" role="tablist">
                    <li role="presentation" class="active"><a href="#addContact" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Contact</a>
                    </li>
                    <li role="presentation" class=""><a href="#address" role="tab" id="profile-tab" data-toggle="tab" >Address</a>
                    </li>

                    <li role="presentation" class=""><a href="#jobposition" role="tab" id="profile-tab" data-toggle="tab" >Job Position</a>{{--  \Session::get('submit_job_position')? 'active':'' --}}
                    </li>
                    <li role="presentation" class=""><a href="#spouse" role="tab" id="profile-tab" data-toggle="tab" >Spouse</a>{{--  \Session::get('submit_spouse')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#business" role="tab" id="business-tab" data-toggle="tab" >Business</a>{{--  \Session::get('submit_business')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#representative" role="tab" id="representative-tab" data-toggle="tab" >Representative</a>{{--  \Session::get('submit_representative')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#referral" role="tab" id="referral-tab" data-toggle="tab" >Referral</a>{{--  \Session::get('submit_referral')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#politicalPositions" role="tab" id="political-positions-tab" data-toggle="tab" >Political Positions</a>{{--  \Session::get('submit_political_positions')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#dependents" role="tab" id="profile-tab" data-toggle="tab" >Dependents</a>{{--  \Session::get('submit_dependents')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#contactNumber" role="tab" id="contact-number-tab" data-toggle="tab" >Contact Number</a>{{--  \Session::get('submit_contact_number')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#communication" role="tab" id="communication-tab" data-toggle="tab" >Communication</a>{{--  \Session::get('submit_communication')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#education" role="tab" id="education-tab" data-toggle="tab" >Education</a>{{--  \Session::get('submit_education')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#greetingsLetter" role="tab" id="greetings-tab" data-toggle="tab" >Greetings Letter</a>
                    </li>{{--  \Session::get('submit_greetings_letter')? 'active':'' --}}
                </ul>
            </div>
            <div class="col-xs-10">
                <div id="myTabContent" class="tab-content">
                    <!-- Contact section start -->
                    @include('Admin.contact.viewContacts')
                    <!-- Contact section end -->

                    <!-- Address section start -->
                    @include('Admin.contact.viewAddress')
                    <!-- Address section end -->

                    <!-- Job position start -->
                    @include('Admin.contact.viewJobPosition')
                    <!-- Job position end -->

                    <!-- Spouse section start -->
                    @include('Admin.contact.viewSpouse')
                    <!-- Spouse section end -->

                    <!-- Bussiness section start -->
                    @include('Admin.contact.viewBussiness')
                    <!-- Bussiness section end -->

                    <!-- 6. Representative section start -->
                    @include('Admin.contact.viewRepresentative')
                    <!-- Representative section end -->

                    <!-- 7. Referral section start -->
                    @include('Admin.contact.viewReferral')
                    <!-- Referral section end -->

                    <!-- 8. Political Positions start -->
                    @include('Admin.contact.viewPoliticalPositions')
                    <!-- Political Positions end -->

                    <!-- 9. Dependents section start -->
                    @include('Admin.contact.viewDependents')
                    <!-- Dependents section end -->

                    <!-- 10. Contact Number start -->
                    @include('Admin.contact.viewContactNumber')
                    <!-- Contact Number end -->

                    <!-- 11. Communication start -->
                    @include('Admin.contact.viewCommunication')
                    <!-- Communication end -->

                    <!-- 12. Education start -->
                    @include('Admin.contact.viewEducation')
                    <!-- Education end -->

                    <!-- 13. Greetings Letter start -->
                    @include('Admin.contact.viewGreetingsLetter')
                    <!-- Greetings Letter end -->

                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('resources/admin-assets/js/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/daterangepicker.js') }}"></script>
<script src="{{ asset('resources/admin-assets/css/iCheck/icheck.min.js') }}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script src="{{ asset('resources/admin-assets/js/editor/froala_editor.pkgd.min.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/tags_input/jquery.tagsinput.js') }}">
</script>

<script type="application/javascript">

$(document).ready(function(){
    //if #tabname is select then it will do selected
    if (location.href.indexOf("#") != -1) {

        var tabSelect = location.href.split("#");
        console.log("yes =>"+tabSelect[1]);
        console.log("yes");
        $('#myTab a[href="#'+tabSelect[1]+'"]').tab('show');
    } 

    

    $("#first_name").keyup(function(){

    });
    $('#date_of_birth').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_1"
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

    $('#spouse_date_of_birth').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_1"
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
    
    //contact address js dependency - start
        // contact address main contry dependency
        $("select.region").change(function(){
            var selRegion = $(".region option:selected").val();
            // console.log(selRegion);

            $.ajax({
                type: "GET",
                url: "{{ url('country') }}",
                data: { region : selRegion, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#countryAutoLoad").html(data);
            });
        });

        //contact address main city auto load
        $(document).on("change", 'select.country', function(event) { 

            var selCountry = $(".country option:selected").val();
            // contact main city dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('city') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#cityAutoLoad").html(data);
            });

            //contact main provience dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('province') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#provinceAutoLoad").html(data);
            });

            //contact main district dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('district') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#districtAutoLoad").html(data);
            });
        });
    //contact address js dependency - end

    //contact jobposition js dependency - start
        // jobposition main contry dependency
        $("select.jobregion").change(function(){
            var selRegion = $(".jobregion option:selected").val();
            var classname = "jobcountry";
            console.log("region "+selRegion);

            $.ajax({
                type: "GET",
                url: "{{ url('country') }}",
                data: { 'region' : selRegion, 'classname' : classname,'_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#jobcountryAutoLoad").html(data);
            });
        });

        //job position country dropdown change
        $(document).on("change", 'select.jobcountry', function(event) { 

            var selCountry = $(".jobcountry option:selected").val();
            // contact main city dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('city') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#jobCityAutoLoad").html(data);
            });

            //contact main provience dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('province') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#jobProvinceAutoLoad").html(data);
            });

            //contact main district dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('district') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#jobDistrictAutoLoad").html(data);
            });
        });
    //contact jobposition js dependency - end

    //contact business js dependency - start
        // business main contry dependency
        $("select.businessregion").change(function(){
            var selRegion = $(".businessregion option:selected").val();
            var classname = "businesscountry";
            $.ajax({
                type: "GET",
                url: "{{ url('country') }}",
                data: { 'region' : selRegion, 'classname' : classname,'_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#businesscountryAutoLoad").html(data);
            });
        });

        //job business country dropdown change
        $(document).on("change", 'select.businesscountry', function(event) { 

            var selCountry = $(".businesscountry option:selected").val();
            // contact main city dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('city') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#businessCityAutoLoad").html(data);
            });

            //contact main provience dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('province') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#businessProvinceAutoLoad").html(data);
            });

            //contact main district dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('district') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#businessDistrictAutoLoad").html(data);
            });
        });
    //contact business js dependency - end

    // prahlad code - start
        $('#date_of_birth').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#start_date').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#start_at').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#end_date').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        // $(function() { 
        //     $('textarea').froalaEditor();
        // });

        $(function() { 
            $('.textEditor').froalaEditor({
                width:'100%',
                height: 200,
                toolbarButtons: ['bold', 'italic', 'underline','strikeThrough', 'subscript', 'superscript','align'],
                contenteditable:false,

            });

            $(".textEditor").froalaEditor("edit.off");



            $('.tagInput').tagsInput({
                width: 'auto'
            });
        });

        $(".textEditor").froalaEditor("edit.off");

        $(document).on("change", 'select.job_category', function(event) { 

            var selCategory = $(".job_category option:selected").val();
            //Job Positions drop down
            $.ajax({
                type: "GET",
                url: "{{ url('job-category') }}",
                data: { jobcategory : selCategory, '_token' : "{{ csrf_token() }}" } 
            }).done(function(data){
                $("#jocPositionAutoLoad").html(data);
            });
        });
    //prahlad code - end

});

</script>


@endpush
