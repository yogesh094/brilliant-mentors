<div role="tabpanel" class="tab-pane fade" id="representative" >
    {{-- \Session::get('submit_representative')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Representative </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#representative') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/representative-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($contact->id)?$contact->id:'') !!}   

            {!! Form::hidden('contact_id', isset($representativeContact->contact_id) ? $representativeContact->contact_id : '') !!}

            <!-- left -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">

                <!-- First Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name  </label>

                    <!-- title -->
                    <div class="col-md-3 col-sm-3 col-xs-3">

                        <?php 
                        $titleArr     = ['1' => 'Mr.' , '2' => 'Mrs.' , '3' => 'Miss.'];
                        $titleText    = "";
                        if(isset($representativeContact->title)) {
                            $titleText    = (isset($titleArr[$representativeContact->title]))?$titleArr[$representativeContact->title]:"";    
                        }
                        ?> 
                        {!! Form::text('representative_title','',['class'=>'form-control', 'placeholder'=>$titleText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('representative_title') }}</span>
                    </div>

                    <!-- First Name -->
                    <div class="col-md-6 col-sm-6 col-xs-9">

                        <?php 
                        $repreFirstNameArr     = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => '    Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'];
                        $repreFirstNameText    = "";
                        if(isset($representativeContact->first_name)) {
                            $repreFirstNameText    = (isset($repreFirstNameArr[$representativeContact->first_name]))?$repreFirstNameArr[$representativeContact->first_name]:"";    
                        }
                        ?> 
                        {!! Form::text('representative_first_name','',['class'=>'form-control', 'placeholder'=>$repreFirstNameText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('representative_first_name') }}</span>
                    </div>
                </div>

                <!-- suffix -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Suffix  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $suffixArr      = ['1' => 'CLU' , '2' => 'D.D.S.', '3' => 'D.V.M.', '4' => 'Esq.', '5' => 'LL.D.', '6' => 'M.D.', '7' => 'O.S.B.', '8' => 'Ph.D.', '9' => 'R.G.S', '10' => 'S.H.C.J.'];

                        $suffixStr      = "";
                        if(isset($representativeContact->suffix)) {

                            if($representativeContact->suffix != "") {
                                foreach (explode(',',$representativeContact->suffix) as $key => $suffix) {
                                    $suffixStr   .= $suffixArr[$suffix].", ";
                                }   
                                $suffixStr   = ($suffixStr != "")?rtrim($suffixStr," ,"):"";
                            }
                        }
                        ?>

                        {!! Form::textarea('representative_suffix','',['class'=>'form-control', 'placeholder'=>$suffixStr,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('representative_suffix') }}</span>
                    </div>
                </div>

                <!-- Father Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Father Name</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $faterNameArr     = ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '6' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'];
                        $repreFatherNameText    = "";
                        if(isset($representativeContact->father_name)) {
                            $repreFatherNameText    = (isset($faterNameArr[$representativeContact->father_name]))?$faterNameArr[$representativeContact->father_name]:"";    
                        }
                        ?> 
                        {!! Form::text('representative_father_name','',['class'=>'form-control', 'placeholder'=>$repreFatherNameText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('representative_father_name') }}</span>
                    </div>
                </div>

                <!-- Mother Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mother Name</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">


                        <?php 
                        $motherNameArr     = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ];
                        $repreMotherNameText    = "";
                        if(isset($representativeContact->mother_name)) {
                            $repreMotherNameText    = (isset($motherNameArr[$representativeContact->mother_name]))?$motherNameArr[$representativeContact->mother_name]:"";    
                        }
                        ?> 
                        {!! Form::text('representative_mother_name','',['class'=>'form-control', 'placeholder'=>$repreMotherNameText,'readonly' => 'readonly']) !!}
                        
                        <span class="text-danger">{{ $errors->first('representative_mother_name') }}</span>
                    </div>
                </div>

                <!-- Surname -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Surname  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $surnameArr     = ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'];
                        $repreSurnameText       = "";
                        if(isset($representativeContact->surname)) {
                            $repreSurnameText   = (isset($surnameArr[$representativeContact->surname]))?$surnameArr[$representativeContact->surname]:"";
                        }
                        ?> 
                        {!! Form::text('representative_surname','',['class'=>'form-control', 'placeholder'=>$repreSurnameText,'readonly' => 'readonly']) !!}
                        
                        <span class="text-danger">{{ $errors->first('representative_surname') }}</span>
                    </div>
                </div>

                <!-- DOB -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $dobText     = isset($representativeContact->date_of_birth)?date("m-d-Y",strtotime($representativeContact->date_of_birth)):'';
                        ?>
                        {!! Form::text('representative_date_of_birth','',['class'=>'form-control', 'placeholder'=>$dobText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('representative_date_of_birth') }}</span>
                    </div>
                </div>

                <!-- blood type -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Type</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $bloodtypeText       = "";
                        if(isset($representativeContact->blood_type)) {
                            $bloodtypeText   = (isset($bloodType[$representativeContact->blood_type]))?$bloodType[$representativeContact->blood_type]:"";
                        }
                        ?> 
                        {!! Form::text('representative_blood_type','',['class'=>'form-control', 'placeholder'=>$bloodtypeText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('representative_blood_type') }}</span>
                    </div>
                </div>

                <!-- No of Dependents -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">No Of Dependents</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $nodText     = isset($representativeContact->number_of_dependents)?$representativeContact->number_of_dependents:'';
                        ?>
                        {!! Form::text('representative_number_of_dependents','',['class'=>'form-control', 'placeholder'=>$nodText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('representative_number_of_dependents') }}</span>
                    </div>
                </div>

                <!-- languages -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Languages</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $langText     = "";
                        if(isset($representativeContact->languages)){

                            if($representativeContact->languages != "") {
                                foreach (explode(',',$representativeContact->languages) as $key => $lang) {
                                    $langText   .= $languages[$lang].", ";
                                }   
                                $langText   = ($langText != "")?rtrim($langText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('representative_languages[]','',['class'=>'form-control', 'placeholder'=>$langText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('representative_languages') }}</span>
                    </div>
                </div>

                <!-- importance -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $imporArr           = ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
                        $imporText          = "";
                        if(isset($representativeContact->importance)) {
                            $imporText   = (isset($imporArr[$representativeContact->importance]))?$imporArr[$representativeContact->importance]:"";
                        }
                        ?> 
                        {!! Form::text('representative_importance','',['class'=>'form-control', 'placeholder'=>$imporText,'readonly' => 'readonly']) !!}
                        
                        <span class="text-danger">{{ $errors->first('representative_importance') }}</span>
                    </div>
                </div>

                <!-- sensitivity -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $senseArr           = [1=>'sensitivity text1',2=>'sensitivity text2',3=>'sensitivity text3'];
                        $senseText          = "";
                        if(isset($representativeContact->sensitivity)) {
                            $senseText   = (isset($senseArr[$representativeContact->sensitivity]))?$senseArr[$representativeContact->sensitivity]:"";
                        }
                        ?> 
                        {!! Form::text('representative_sensitivity','',['class'=>'form-control', 'placeholder'=>$senseText,'readonly' => 'readonly']) !!}
                        
                        <span class="text-danger">{{ $errors->first('representative_sensitivity') }}</span>
                    </div>
                </div>

                <!-- Private -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Private  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <label>
                            <?php
                                $privateYes = $privateNo = false; 
                                if(isset($representativeContact->status)) {
                                    $privateYes = ($representativeContact->private == 1?true:false);
                                    $privateNo  = ($representativeContact->private == 0?true:false);
                                }
                            ?>
                            {!! Form::radio('representative_private', '1', $privateYes, ['class' => 'flat', 'disabled' => 'disabled' ]) !!}  Yes
                        </label>
                        <label>
                            {!! Form::radio('representative_private', '0', $privateNo, ['class' => 'flat', 'disabled' => 'disabled']) !!}  No
                        </label>
                    </div>
                </div>

                <!-- Sponsorships -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sponsorships</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $sponseArr      = ['1' => 'JPMorgan' , '2' => 'Microsoft', '3' => 'Deloitte', '4' => 'PepsiCo', '5' => 'DHL Express'];
                        $sponseText     = "";
                        if(isset($representativeContact->sponsorships)) {

                            if($representativeContact->sponsorships != "") {
                                foreach (explode(',',$representativeContact->sponsorships) as $key => $sponsor) {
                                    $sponseText   .= $sponseArr[$sponsor].", ";
                                }   
                                $sponseText   = ($sponseText != "")?rtrim($sponseText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('representative_sponsorships','',['class'=>'form-control', 'placeholder'=>$sponseText,'readonly' => 'readonly','rows' => 2]) !!}

                        
                        <span class="text-danger">{{ $errors->first('representative_sponsorships') }}</span>
                    </div>
                </div>

                <!-- Political Party -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Party </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $politicalArr      = ['1' => 'Movement of the future' , '2' => 'Kataeb Party', '3' => 'Hezbollah', '4' => 'Free Patriotic Movement'];
                        $politicalText     = "";
                        if(isset($representativeContact->political_party)) {

                            if($representativeContact->political_party != "") {
                                foreach (explode(',',$representativeContact->political_party) as $key => $political) {
                                    $politicalText   .= $politicalArr[$political].", ";
                                }   
                                $politicalText   = ($politicalText != "")?rtrim($politicalText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('representative_political_party','',['class'=>'form-control', 'placeholder'=>$politicalText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('representative_political_party') }}</span>
                    </div>
                </div>

                <!-- Religion -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Religion </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $regArr           = [1=>'sensitivity text1',2=>'sensitivity text2',3=>'sensitivity text3'];
                        $regText          = "";
                        if(isset($representativeContact->religion_id)) {
                            $regText   = (isset($regArr[$representativeContact->religion_id]))?$regArr[$representativeContact->religion_id]:"";
                        }
                        ?> 
                        {!! Form::text('representative_sensitivity','',['class'=>'form-control', 'placeholder'=>$regText,'readonly' => 'readonly']) !!}
                        
                        <span class="text-danger">{{ $errors->first('representative_religion') }}</span>
                    </div>
                </div>

                <!-- Administrative Group -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Administrative Group </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $adminGpArr     =  ['1' => 'Business Group' , '2' => 'Volunteer Group', '3' => 'Administration Group', '4' => 'Management Group'];
                        $adminText      = "";
                        if(isset($representativeContact->administrative_group)) {

                            if($representativeContact->administrative_group != "") {
                                foreach (explode(',',$representativeContact->administrative_group) as $key => $lang) {
                                    if($lang > 0) {
                                        $adminText   .= $adminGpArr[$lang].", ";
                                    }
                                }   
                                $adminText   = ($adminText != "")?rtrim($adminText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('representative_administrative_group[]','',['class'=>'form-control', 'placeholder'=>$adminText,'readonly' => 'readonly','rows' => 2]) !!}

                        
                        <span class="text-danger">{{ $errors->first('representative_administrative_group') }}</span>
                    </div>
                </div>
            </div>

            <!-- right -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- photo box -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-right">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3 my-photo-box-view">
                        <div style="text-align: left;float:none;">
                            <?php
                            if(isset($representativeContact->photo) && $representativeContact->photo != "") {
                            ?>
                                <img src="{{ $imageUrl.$representativeContact->photo }}" class="contact_spouse_photo img-thumbnail" >
                            <?php
                            } else {
                            ?>
                                <img src="{{ url('/uploads/contact/avatar.png') }}" class="contact_spouse_photo img-thumbnail" >
                            <?php 
                            }
                            ?>
                            <input type="hidden" name="old_contact_representative_image" value="{{ isset($representativeContact->photo)?$representativeContact->photo:'' }}">
                        </div>
                    </div>
                </div>

                <!-- nick name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $nickname       = isset($representativeContact->nickname)?$representativeContact->nickname:'';
                        ?>
                        {!! Form::text('representative_nick_name','',['class'=>'form-control', 'placeholder'=>$nickname,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('representative_nick_name') }}</span>
                    </div>
                </div>

                <!-- birth place -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Birth Place</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $birthplaceText          = "";
                        if(isset($representativeContact->birth_place)) {
                            $birthplaceText   = (isset($nationality[$representativeContact->birth_place]))?$nationality[$representativeContact->birth_place]:"";
                        }
                        ?> 
                        {!! Form::text('representative_birth_place','',['class'=>'form-control', 'placeholder'=>$birthplaceText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('representative_birth_place') }}</span>
                    </div>
                </div>

                <!-- gender -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Gender</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $genderText          = "";
                        if(isset($representativeContact->gender)) {
                            $genderText   = (isset($gender[$representativeContact->gender]))?$gender[$representativeContact->gender]:"";
                        }
                        ?> 
                        {!! Form::text('representative_gender','',['class'=>'form-control', 'placeholder'=>$genderText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('representative_gender') }}</span>
                    </div>
                </div>

                <!-- nationality -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nationality</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $nationText      = "";
                        if(isset($representativeContact->nationality)) {

                            if($representativeContact->nationality != "") {
                                foreach (explode(',',$representativeContact->nationality) as $key => $nation) {
                                    $nationText   .= $nationality[$nation].", ";
                                }   
                                $nationText   = ($nationText != "")?rtrim($nationText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('representative_administrative_group[]','',['class'=>'form-control', 'placeholder'=>$nationText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('representative_nationality') }}</span>
                    </div>
                </div>

                <!-- profession -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Profession </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $profArr        = ['1' => 'Profession 1' , '2' => 'Profession 2' , '3' => 'Profession 3', '4' => 'Profession 4', '5' => 'Profession 5'];
                        $profText     = "";
                        if(isset($representativeContact->profession)) {

                            if($representativeContact->profession != "") {
                                foreach (explode(',',$representativeContact->profession) as $key => $prof) {
                                    $profText   .= isset($profArr[$prof])?$profArr[$prof].", ":"";
                                }   
                                $profText   = ($profText != "")?rtrim($profText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('representative_profession[]','',['class'=>'form-control', 'placeholder'=>$profText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('representative_profession') }}</span>
                    </div>
                </div>

                <!-- caution -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $causionArr         = ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'];           
                        $causionText        = "";
                        if(isset($representativeContact->caution)) {
                            $causionText   = (isset($causionArr[$representativeContact->caution]))?$causionArr[$representativeContact->caution]:"";
                        }
                        ?> 
                        {!! Form::text('representative_caution','',['class'=>'form-control', 'placeholder'=>$causionText,'readonly' => 'readonly']) !!}
                        
                        <span class="text-danger">{{ $errors->first('representative_caution') }}</span>
                    </div>
                </div>

                <!-- Business Owner -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Business Owner</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $businessOwnYes = $businessOwnNo = false;
                        if(isset($representativeContact->business_owner)) {
                            $businessOwnYes     = $representativeContact->business_owner == 1?true:false;

                            $businessOwnNo      = $representativeContact->business_owner == 0?true:false;
                        ?>
                        <label>
                            {!! Form::radio('business_owner', '1',$businessOwnYes,['class' => 'flat','disabled' => 'disabled']) !!}
                        </label> 
                        <label>
                            {!! Form::radio('business_owner', '0',$businessOwnNo,['class' => 'flat','disabled' => 'disabled']) !!}  No
                        </label>   
                        <?php
                        }
                        ?>
                        
                        <span class="text-danger">{{ $errors->first('representative_business_owner') }}</span>
                    </div>
                </div>

                <!-- Charitable Organizations -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Charitable Organizations </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $charityArr        = ['1' => 'YSR FOUNDATION ' , '2' => 'THE COCONADA CHAMBER OF COMMERCE', '3' => 'RAVINDRANATH MEDICAL FOUNDATION'];
                        $charityText     = "";
                        if(isset($representativeContact->charitable_organizations)) {

                            if($representativeContact->charitable_organizations != "") {
                                foreach (explode(',',$representativeContact->charitable_organizations) as $key => $charity) {
                                    $charityText   .= $charityArr[$charity].", ";
                                }   
                                $charityText   = ($charityText != "")?rtrim($charityText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('representative_charitable_organizations[]','',['class'=>'form-control', 'placeholder'=>$charityText,'readonly' => 'readonly','rows' => 2]) !!}


                        
                        <span class="text-danger">{{ $errors->first('representative_charitable_organizations') }}</span>
                    </div>
                </div>

                <!-- Relationship Preference -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Relationship Preference </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $RelArr        = ['1' => 'Relationship Preference 1' , '2' => 'Relationship Preference 2', '3' => 'Relationship Preference 3', '4' => 'Relationship Preference 4'];
                        $relText     = "";
                        if(isset($representativeContact->relationship_preference)) {

                            if($representativeContact->relationship_preference != "") {
                                foreach (explode(',',$representativeContact->relationship_preference) as $key => $rel) {
                                    $relText   .= $RelArr[$rel].", ";
                                }   
                                $relText   = ($relText != "")?rtrim($relText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('representative_relationship_preference[]','',['class'=>'form-control', 'placeholder'=>$relText,'readonly' => 'readonly','rows' => 2]) !!}
                        
                        <span class="text-danger">{{ $errors->first('representative_relationship_preference') }}</span>
                    </div>
                </div>

                <!-- Internal Organizations -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Internal Organizations </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $intOrgArr        = ['1' => 'Makhzoumi Foundation' , '2' => 'Beirutiyat', '3' => 'Makhzoumi Health Care', '4' => 'Makhzoumi Development'];
                        $intOrgText     = "";
                        if(isset($representativeContact->internal_organizations)) {

                            if($representativeContact->internal_organizations != "") {
                                foreach (explode(',',$representativeContact->internal_organizations) as $key => $rel) {
                                    $intOrgText   .= $intOrgArr[$rel].", ";
                                }   
                                $intOrgText   = ($intOrgText != "")?rtrim($intOrgText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('representative_internal_organizations[]','',['class'=>'form-control', 'placeholder'=>$intOrgText,'readonly' => 'readonly','rows' => 2]) !!}


                        
                        <span class="text-danger">{{ $errors->first('representative_internal_organizations') }}</span>
                    </div>
                </div>

                <!-- Status -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Status  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <label>
                            <?php
                                $statusYes = $statusNo = false; 
                                if(isset($representativeContact->status)) {
                                    $statusYes = ($representativeContact->status == 1?true:false);
                                    $statusNo  = ($representativeContact->status == 0?true:false);
                                }
                            ?>
                            {!! Form::radio('representative_status', '1', $statusYes, ['class' => 'flat','disabled' => 'disabled' ]) !!}  Active
                        </label>
                        <label>
                            {!! Form::radio('representative_status', '0', $statusNo, ['class' => 'flat','disabled' => 'disabled']) !!}  Inactive
                        </label>
                    </div>
                </div>
            </div>

            <!-- keyword and notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <!-- Keywords -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        {!! Form::text('keywords',isset($representativeContact->keywords)?$representativeContact->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                    </div>
                </div>
                
                <!-- Notes -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php 
                        $notes     = isset($representativeContact->notes)?$representativeContact->notes:'';
                        ?>
                        {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                    </div>
                </div>
            </div>

            <!-- left -->
            <!-- created section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Created by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($representativeContact->user_createby['first_name'])?$representativeContact->user_createby['first_name']:'';
                        $lastName   = isset($representativeContact->user_createby['last_name'])?$representativeContact->user_createby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Created on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($representativeContact->created_at)?date_format($representativeContact->created_at, 'j M Y g:i A'):'';
                        ?>

                        {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- right -->
            <!-- updated section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Updated by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($representativeContact->user_updateby['first_name'])?$representativeContact->user_updateby['first_name']:'';
                        $lastName   = isset($representativeContact->user_updateby['last_name'])?$representativeContact->user_updateby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Updated on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($representativeContact->updated_at)?date_format($representativeContact->updated_at, 'j M Y g:i A'):'';
                        ?>
                        {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- submit section -->
            <?php
            $i = 0;
            if($i == 1) {
            ?>
                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_representative')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                    </div>
                </div>
            <?php 
            }
            ?>
        {!! Form::close() !!}
        </div>
    </div>
</div>