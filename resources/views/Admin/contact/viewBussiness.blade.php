<div role="tabpanel" class="tab-pane fade" id="business" >
    {{-- \Session::get('submit_business')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Bussiness </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#business') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
            {!! Form::open(array('url' => ('contact/business-store'), 'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}
                
            {!! Form::hidden('contact_id',isset($contact->id)?$contact->id:'') !!}    

            <!-- left -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Job type -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">BusinessType</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $businessType       = ['1'=>'Owner','2'=>'Employed','3'=>'Freelancer','4'=>'Politician'];

                        $businessTypeText   = "";
                        if(isset($businessDetail->business_type)) {
                            $businessTypeText   = (isset($businessType[$businessDetail->business_type]))?$businessType[$businessDetail->business_type]:"";    
                        }
                        ?>    
                        {!! Form::text('type','',['class'=>'form-control', 'placeholder'=>$businessTypeText,'readonly' => 'readonly']) !!}
                        <span class="text-danger">{{ $errors->first('business_type') }}</span>
                    </div>
                </div>

                <!-- Description -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $businessDesc   = isset($businessDetail->description)?$businessDetail->description:'';
                        ?>
                        {!! Form::text('description','',['class'=>'form-control', 'placeholder'=>$businessDesc,'readonly' => 'readonly']) !!}
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    </div>
                </div>

                <!-- Oranization -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Organization </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $organizationArr    = ["1"=>"Organization 1","2"=>"Organization 2","3"=>"Organization 3"];
                        $organizationText       = "";
                        if(isset($businessDetail->organization)) {
                            $organizationText   = (isset($organizationArr[$businessDetail->organization]))?$organizationArr[$businessDetail->organization]:"";    
                        }
                        ?>
                        {!! Form::text('organization','',['class'=>'form-control', 'placeholder'=>$organizationText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('organization') }}</span>
                    </div>
                </div>

                <!-- Number Of Employees -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">No Of Emp</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $noOfEmpArr         = [10,20,30,40,50];
                        $noOfEmpText        = "";
                        if(isset($businessDetail->number_of_employees)) {
                            $noOfEmpText   = (isset($noOfEmpArr[$businessDetail->number_of_employees]))?$noOfEmpArr[$businessDetail->number_of_employees]:"";    
                        }
                        ?>
                        {!! Form::text('number_of_employees','',['class'=>'form-control', 'placeholder'=>$noOfEmpText,'readonly' => 'readonly']) !!}

                        
                    <span class="text-danger">{{ $errors->first('number_of_employees') }}</span>
                    </div>
                </div>

                <!-- Start Date -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $startText   = isset($businessDetail->start_at)?date('m-d-Y',strtotime($businessDetail->start_at)):'';
                        ?> 
                        {!! Form::text('start_at','',['class'=>'form-control', 'placeholder'=>$startText,'readonly' => 'readonly']) !!}
                        
                        <span class="text-danger">{{ $errors->first('start_at') }}</span>
                    </div>
                </div>

                <!-- Region -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Region </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $regionText       = "";
                        if(isset($businessDetail->region_id)) {
                            $regionText   = (isset($region[$businessDetail->region_id]))?$region[$businessDetail->region_id]:"";    
                        }
                        ?>
                        {!! Form::text('region_id','',['class'=>'form-control', 'placeholder'=>$regionText,'readonly' => 'readonly']) !!}

                    <span class="text-danger">{{ $errors->first('region_id') }}</span>
                    </div>
                </div>

                <!-- Country -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $nationalitypText       = "";
                        if(isset($businessDetail->country_id)) {
                            $nationalitypText   = (isset($nationality[$businessDetail->country_id]))?$nationality[$businessDetail->country_id]:"";    
                        }
                        ?>
                        {!! Form::text('country_id','',['class'=>'form-control', 'placeholder'=>$nationalitypText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('country_id') }}</span>
                    </div>
                </div>

                <!-- Province -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Province </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php
                        $provinceText       = "";
                        if(isset($businessDetail->province_id)) {
                            $provinceText   = (isset($businessProvince[$businessDetail->province_id]))?$businessProvince[$businessDetail->province_id]:"";    
                        }
                        ?>
                        {!! Form::text('region_id','',['class'=>'form-control', 'placeholder'=>$provinceText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('province_id') }}</span>
                    </div>
                </div>
            </div> 

            <!-- right -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- District -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    District </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $destrictText       = "";
                        if(isset($businessDetail->district_id)) {
                            $destrictText   = (isset($businessDistrict[$businessDetail->district_id]))?$businessDistrict[$businessDetail->district_id]:"";    
                        }
                        ?>
                        {!! Form::text('district_id','',['class'=>'form-control', 'placeholder'=>$destrictText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('district_id') }}</span>
                    </div>
                </div>

                <!-- City -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $cityText       = "";
                        if(isset($businessDetail->city_id)) {
                            $cityText   = (isset($businessCity[$businessDetail->city_id]))?$businessCity[$businessDetail->city_id]:"";    
                        }
                        ?>
                        {!! Form::text('city_id','',['class'=>'form-control', 'placeholder'=>$cityText,'readonly' => 'readonly']) !!}

                        
                        <span class="text-danger">{{ $errors->first('city_id') }}</span>
                    </div>
                </div>

                <!-- Postal Code -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Postalcode </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $businessPostcode   = isset($businessDetail->postal_code)? $businessDetail->postal_code:'';
                        ?>
                        {!! Form::text('postal_code','',['class'=>'form-control', 'placeholder'=>$businessPostcode,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('postal_code') }}</span>
                    </div>
                </div>

                <!-- PO Box -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">PO Box </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $poboxText      = isset($businessDetail->po_box)?$businessDetail->po_box:'';
                        ?>
                        {!! Form::text('po_box','',['class'=>'form-control', 'placeholder'=>$poboxText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('po_box') }}</span>
                    </div>
                </div>

                <!-- Address Line 1 -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 1 </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $businessAdd1   = isset($businessDetail->address_line_1)?$businessDetail->address_line_1:'';
                        ?>
                        {!! Form::text('address_line_1','',['class'=>'form-control', 'placeholder'=>$businessAdd1,'readonly' => 'readonly']) !!}
                        <span class="text-danger">{{ $errors->first('address_line_1') }}</span>
                    </div>
                </div>

                <!-- Address Line 2 -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 2 </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $businessAdd2   = isset($businessDetail->address_line_2)?$businessDetail->address_line_2:'';
                        ?>
                        {!! Form::text('address_line_2','',['class'=>'form-control', 'placeholder'=>$businessAdd2,'readonly' => 'readonly']) !!}

                        <!-- {!! Form::text('address_line_2',isset($businessDetail->address_line_2)?$businessDetail->address_line_2:'',['id' => 'addressline2','class'=>'form-control', 'placeholder'=>'Address Line 2']) !!} -->
                        <span class="text-danger">{{ $errors->first('address_line_2') }}</span>
                    </div>
                </div>

                <!-- Website -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Website </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $websiteText      = isset($businessDetail->website)?$businessDetail->website:'';
                        ?>
                        {!! Form::text('website','',['class'=>'form-control', 'placeholder'=>$websiteText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('website') }}</span>
                    </div>
                </div>
            </div> 

            <!-- keyword and notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <!-- Keywords -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::text('Keywords',isset($businessDetail->Keywords)?$businessDetail->Keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                    </div>
                </div>
                
                <!-- Notes -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php 
                        $notes     = isset($businessDetail->notes)?$businessDetail->notes:'';
                        ?>
                        {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                    </div>
                </div>
            </div>

            <!-- left -->
            <!-- created section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Created by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($businessDetail->user_createby['first_name'])?$businessDetail->user_createby['first_name']:'';
                        $lastName   = isset($businessDetail->user_createby['last_name'])?$businessDetail->user_createby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Created on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($businessDetail->created_at)?date_format($businessDetail->created_at, 'j M Y g:i A'):'';
                        ?>

                        {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- right -->
            <!-- updated section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Updated by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($businessDetail->user_updateby['first_name'])?$businessDetail->user_updateby['first_name']:'';
                        $lastName   = isset($businessDetail->user_updateby['last_name'])?$businessDetail->user_updateby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Updated on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($businessDetail->updated_at)?date_format($businessDetail->updated_at, 'j M Y g:i A'):'';
                        ?>
                        {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- submit section -->
            <?php 
            $i = 0;
            if($i == 1) {
            ?>
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit','name'=>'submit_business')) !!}
                    </div>
                </div>
            <?php 
            }
            ?>
            {!! Form::close() !!}
        </div>
    </div>
</div>