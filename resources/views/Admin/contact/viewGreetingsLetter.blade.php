<div role="tabpanel" class="tab-pane fade" id="greetingsLetter" >
    {{-- \Session::get('submit_greetings_letter')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Greetings Letter </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#greetingsLetter') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/greetings-letter-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($contact->id)?$contact->id:'') !!}
            {!! Form::hidden('contact_id', isset($greetingsContact->contact_id)?$greetingsContact->contact_id:'') !!}
            {!! Form::hidden('contact_political_position_id', isset($politicalPositionContact->id)?$politicalPositionContact->id:'') !!}
            {!! Form::hidden('contact_dependents_id', isset($dependentsContact->id)?$dependentsContact->id:'') !!}
            {!! Form::hidden('contact_spouse_id', isset($spouseContact->id)?$spouseContact->id:'') !!}
            {!! Form::hidden('contact_referral_id', isset($referralContact->id)?$referralContact->id:'') !!}
            {!! Form::hidden('contact_representative_id', isset($representativeContact->id)?$representativeContact->id:'') !!}
        <!-- left -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">

            <!-- Title -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php 
                    $greetTitleArr         = ['1' => 'Contact','2'=>'Business','3'=>'Spouse','4'=>'Dependents','5'=>'Referral','6'=>'Representative'];
                    $greetTitleText        = "";
                    if(isset($greetingsContact->title)) {
                        $greetTitleText    = (isset($greetTitleArr[$greetingsContact->title]))?$greetTitleArr[$greetingsContact->title]:"";    
                    }
                    ?> 
                    {!! Form::text('greeting_title','',['class'=>'form-control', 'placeholder'=>$greetTitleText,'readonly' => 'readonly']) !!}

                    <span class="text-danger">{{ $errors->first('greeting_title') }}</span>
                </div>
            </div>

            <!-- importance -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php 
                    $greetImpArr         = ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
                    $greeting_importance        = "";
                    if(isset($greetingsContact->importance)) {
                        $greeting_importance    = (isset($greetImpArr[$greetingsContact->importance]))?$greetImpArr[$greetingsContact->importance]:"";    
                    }
                    ?> 
                    {!! Form::text('greeting_importance','',['class'=>'form-control', 'placeholder'=>$greeting_importance,'readonly' => 'readonly']) !!}
                    
                    <span class="text-danger">{{ $errors->first('greeting_importance') }}</span>
                </div>
            </div>

            <!-- Greeting Intro -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Greeting Intro</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php 
                    $greetIntroArr         = ['1' => 'Hi','2'=>'Hello','3'=>'Good morning','4'=>'Good afternoon','5'=>'Good evening'];
                    $greetIntroText        = "";
                    if(isset($greetingsContact->grettings_intro)) {
                        $greetIntroText    = isset($greetingsContact->grettings_intro)?$greetingsContact->grettings_intro:"";
                    }
                    ?> 
                    {!! Form::textarea('greeting_intro','',['class'=>'form-control', 'placeholder'=>$greetIntroText,'readonly' => 'readonly','rows'=>2]) !!}

                    <span class="text-danger">{{ $errors->first('greeting_intro') }}</span>
                </div>
            </div>
        </div>

        <!-- right -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">

            <!-- caution -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php 
                    $greetRiskArr         = ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'];
                    $greetingCaution        = "";
                    if(isset($greetingsContact->caution)) {
                        $greetingCaution    = (isset($greetRiskArr[$greetingsContact->caution]))?$greetRiskArr[$greetingsContact->caution]:"";    
                    }
                    ?> 
                    {!! Form::text('greeting_caution','',['class'=>'form-control', 'placeholder'=> $greetingCaution,'readonly' => 'readonly']) !!}

                    <span class="text-danger">{{ $errors->first('greeting_caution') }}</span>
                </div>
            </div>

            <!-- sensitivity -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php
                    $sensitivityArr     = [1=>'sensitivity text1',2=>'sensitivity text2',3=>'sensitivity text3'];
                    $sensitivityStr     = "";
                    if(isset($greetingsContact->sensitivity)) {
                        $sensitivityStr = isset($sensitivityArr[$greetingsContact->sensitivity])?$sensitivityArr[$greetingsContact->sensitivity]:"";
                    }
                    ?>

                    {!! Form::text('greeting_sensitivity','',['class'=>'form-control', 'placeholder'=>$sensitivityStr,'readonly' => 'readonly','rows'=>2]) !!}

                    <span class="text-danger">{{ $errors->first('greeting_sensitivity') }}</span>
                </div>
            </div>

            <!-- Grettings Ending -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Grettings Ending</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php 
                    $greetEndArr         = ['1' => 'Cheers' , '2' => 'Take care', '3' => 'Good night','4' => 'See you, Bye','5'=>'See you tomorrow','6'=>'Good-bye'];
                    $grettingsEnding        = "";
                    if(isset($greetingsContact->grettings_ending)) {
                        $grettingsEnding    = isset($greetingsContact->grettings_ending)?$greetingsContact->grettings_ending:"";    
                    }
                    ?> 
                    {!! Form::textarea('greeting_ending','',['class'=>'form-control', 'placeholder'=>$grettingsEnding,'readonly' => 'readonly','rows'=>2]) !!}

                    
                    <span class="text-danger">{{ $errors->first('greeting_ending') }}</span>
                </div>
            </div>
        </div>

        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <!-- Notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <?php 
                    $notes     = isset($greetingsContact->notes)?$greetingsContact->notes:'';
                    ?>
                    {!! Form::textarea('greeting_notes','',['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                    <span class="text-danger">{{ $errors->first('greeting_notes') }}</span>
                </div>
            </div>

            <!-- Keywords -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                <div class="col-md-12 col-sm-12 col-xs-12">

                    {!! Form::text('greeting_keywords',isset($greetingsContact->keywords)?$greetingsContact->keywords:'',['id'=>'greetingsKeywords','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                    <?php 
                    // $keywordText     = isset($greetingsContact->keywords)?$greetingsContact->keywords:'';
                    ?>
                    <span class="text-danger">{{ $errors->first('greeting_keywords') }}</span>
                </div>
            </div>
        </div>

        <!-- left -->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($greetingsContact->user_createby['first_name'])?$greetingsContact->user_createby['first_name']:'';
                    $lastName   = isset($greetingsContact->user_createby['last_name'])?$greetingsContact->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($greetingsContact->created_at)?date_format($greetingsContact->created_at, 'j M Y g:i A'):'';
                    ?>

                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- right -->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($greetingsContact->user_updateby['first_name'])?$greetingsContact->user_updateby['first_name']:'';
                    $lastName   = isset($greetingsContact->user_updateby['last_name'])?$greetingsContact->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($greetingsContact->updated_at)?date_format($greetingsContact->updated_at, 'j M Y g:i A'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- submit section -->
        <?php 
        $i = 0;
        if($i == 1) {
        ?>
            <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                    {!! Form::submit('Submit',array('class'=>'btn btn-primary submit', 'name'=>'submit_greetings_letter')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        <?php 
        }
        ?>
        {!! Form::close() !!}
        </div>
    </div>
</div>