@extends('Admin.master_layout.master')

@section('title', 'Edit Contact')

@section('breadcum')
     / Edit Contact
@endsection

@push('styles')

<link href="{{ asset('resources/admin-assets/css/daterangepicker.css') }}" rel="stylesheet">
<link href="{{ asset('resources/admin-assets/css/iCheck/skins/square/blue.css') }}" rel="stylesheet">

<link href="{{ asset('resources/admin-assets/css/editor/font-awesome.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
<link href="{{ asset('resources/admin-assets/css/editor/froala_editor.pkgd.min.css') }}" rel="stylesheet">
<link href="{{ asset('resources/admin-assets/css/editor/froala_style.min.css') }}" rel="stylesheet">
<link href="{{ asset('resources/admin-assets/css/tags_input/jquery.tagsinput.css') }}" rel="stylesheet">
<style type="text/css">
    .tagsinput input, .tagsinput div {
        width:  100% !important;
    }
</style>
@endpush
@section('content')

<div class="row">
    <div class="page-title">
        <div class="title_left">
            <h3>{{ $contact->full_name }}</h3>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="x_panel">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <div class="col-xs-2">
                <ul id="myTab" class="nav nav-tabs tabs-left" role="tablist">
                    <li role="presentation" class="active"><a href="#addContact" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Contact</a>
                    </li>
                    <li role="presentation" class=""><a href="#address" role="tab" id="profile-tab" data-toggle="tab" >Address</a>{{--  \Session::get('submit_address')? 'active':'' --}}
                    </li>
                    <li role="presentation" class=""><a href="#jobposition" role="tab" id="profile-tab" data-toggle="tab" >Job Position</a>{{--  \Session::get('submit_job_position')? 'active':'' --}}
                    </li>
                    <li role="presentation" class=""><a href="#contactNumber" role="tab" id="contact-number-tab" data-toggle="tab" >Contact Number</a>{{--  \Session::get('submit_contact_number')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#communication" role="tab" id="communication-tab" data-toggle="tab" >Communication</a>{{--  \Session::get('submit_communication')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#business" role="tab" id="business-tab" data-toggle="tab" >Business</a>{{--  \Session::get('submit_business')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#politicalPositions" role="tab" id="political-positions-tab" data-toggle="tab" >Political Positions</a>{{--  \Session::get('submit_political_positions')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#representative" role="tab" id="representative-tab" data-toggle="tab" >Representative</a>{{--  \Session::get('submit_representative')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#referral" role="tab" id="referral-tab" data-toggle="tab" >Referral</a>{{--  \Session::get('submit_referral')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#spouse" role="tab" id="profile-tab" data-toggle="tab" >Spouse</a>{{--  \Session::get('submit_spouse')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#dependents" role="tab" id="profile-tab" data-toggle="tab" >Dependents</a>{{--  \Session::get('submit_dependents')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#education" role="tab" id="education-tab" data-toggle="tab" >Education</a>{{--  \Session::get('submit_education')? 'active':'' --}}
                    <li role="presentation" class=""><a href="#greetingsLetter" role="tab" id="greetings-tab" data-toggle="tab" >Greetings Letter</a>
                    </li>{{--  \Session::get('submit_greetings_letter')? 'active':'' --}}
                </ul>
            </div>
            <div class="col-xs-10">
                <div id="myTabContent" class="tab-content">
                    <!-- 1. Contact section start -->
                    @include('Admin.contact.contact')
                    <!-- Contact section end -->

                    <!-- 2. Address section start -->
                    @include('Admin.contact.address')
                    <!-- Address section end -->

                    <!-- 3. Job position start -->
                    @include('Admin.contact.jobPosition')
                    <!-- Job position end -->

                    <!-- 4. Spouse section start -->
                    @include('Admin.contact.spouse')
                    <!-- Spouse section end -->

                    <!-- 5. Business section start -->
                    @include('Admin.contact.business')
                    <!-- Business section end -->

                    <!-- 6. Representative section start -->
                    @include('Admin.contact.representative')
                    <!-- Representative section end -->

                    <!-- 7. Referral section start -->
                    @include('Admin.contact.referral')
                    <!-- Referral section end -->

                    <!-- 8. Political Positions start -->
                    @include('Admin.contact.politicalPositions')
                    <!-- Political Positions end -->

                    <!-- 9. Dependents section start -->
                    @include('Admin.contact.dependents')
                    <!-- Dependents section end -->

                    <!-- 10. Contact Number start -->
                    @include('Admin.contact.contactNumber')
                    <!-- Contact Number end -->

                    <!-- 11. Communication start -->
                    @include('Admin.contact.communication')
                    <!-- Communication end -->

                    <!-- 12. Education start -->
                    @include('Admin.contact.education')
                    <!-- Education end -->

                    <!-- 13. Greetings Letter start -->
                    @include('Admin.contact.greetingsLetter')
                    <!-- Greetings Letter end -->

                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('resources/admin-assets/js/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/daterangepicker.js') }}"></script>
<script src="{{ asset('resources/admin-assets/css/iCheck/icheck.min.js') }}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script src="{{ asset('resources/admin-assets/js/editor/froala_editor.pkgd.min.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/tags_input/jquery.tagsinput.js') }}">
</script>

<script type="application/javascript">
$(document).ready(function(){

    setTimeout(function() {
        $(".alert-success").hide()
    }, 10000);

    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);

    //if #tabname is select then it will do selected
    if (location.href.indexOf("#") != -1) {
        var tabSelect = location.href.split("#");
        $('#myTab a[href="#'+tabSelect[1]+'"]').tab('show');
    }
    

    //contact address js dependency - start
        // contact address main contry dependency
        /*$(document).on('change','select.region',function(){
            var selRegion = $(".region option:selected").val();
            var idname      = "contact_address_country_id";
            // console.log(selRegion);

            $.ajax({
                type: "GET",
                url: "{{ url('country') }}",
                data: { region : selRegion, '_token' : "{{ csrf_token() }}",'idname' :idname } 
            }).done(function(data){
                $("#countryAutoLoad").html(data);
            });
            $("select option[value='']").attr("disabled", true);
        });*/

        //contact address main city auto load
        /*$(document).on("change", 'select.country', function(event) {

            var selCountry = $(".country option:selected").val();
            // contact main city dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('city') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#cityAutoLoad").html(data);
            });

            //contact main provience dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('province') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#provinceAutoLoad").html(data);
            });

            //contact main district dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('district') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#districtAutoLoad").html(data);
            });

            $("select option[value='']").attr("disabled", true);
        });*/
    //contact address js dependency - end

    //contact jobposition js dependency - start
        // jobposition main contry dependency
        $("select.jobregion").change(function(){
            var selRegion = $(".jobregion option:selected").val();
            var classname = "jobcountry";
            console.log("region "+selRegion);

            $.ajax({
                type: "GET",
                url: "{{ url('country') }}",
                data: { 'region' : selRegion, 'classname' : classname,'_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#jobcountryAutoLoad").html(data);
            });
            $("select option[value='']").attr("disabled", true);
        });

        //job position country dropdown change
        $(document).on("change", 'select.jobcountry', function(event) {

            var selCountry = $(".jobcountry option:selected").val();
            // contact main city dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('city') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#jobCityAutoLoad").html(data);
            });

            //contact main provience dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('province') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#jobProvinceAutoLoad").html(data);
            });

            //contact main district dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('district') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#jobDistrictAutoLoad").html(data);
            });
            $("select option[value='']").attr("disabled", true);
        });
    //contact jobposition js dependency - end

    //contact business js dependency - start
        // business main contry dependency
        $("select.businessregion").change(function(){
            var selRegion = $(".businessregion option:selected").val();
            var classname = "businesscountry";
            $.ajax({
                type: "GET",
                url: "{{ url('country') }}",
                data: { 'region' : selRegion, 'classname' : classname,'_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#businesscountryAutoLoad").html(data);
            });
            $("select option[value='']").attr("disabled", true);
        });

        //job business country dropdown change
        $(document).on("change", 'select.businesscountry', function(event) {

            var selCountry = $(".businesscountry option:selected").val();
            // contact main city dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('city') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#businessCityAutoLoad").html(data);
            });

            //contact main provience dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('province') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#businessProvinceAutoLoad").html(data);
            });

            //contact main district dropdown
            $.ajax({
                type: "GET",
                url: "{{ url('district') }}",
                data: { country : selCountry, '_token' : "{{ csrf_token() }}" }
            }).done(function(data){
                $("#businessDistrictAutoLoad").html(data);
            });
            $("select option[value='']").attr("disabled", true);
        });
    //contact business js dependency - end

    //text editor js - start
        $(function() {
            $('.textEditor').froalaEditor({
                width:'100%',
                height: 200,
                toolbarButtons: ['bold', 'italic', 'underline','strikeThrough', 'subscript', 'superscript','align']
            });

            $('.tagInput').tagsInput({
                width: 'auto' ,
                defaultText : "Type keyword and once done press tab to save the keyword"
            });
        });
    //text editor js - end
    
});
//common functions start
    function CompareDate(startDate,endDate) {
        var dateOne = new Date(startDate); //Year, Month, Date
        var dateTwo = new Date(endDate); //Year, Month, Date
        if (dateOne > dateTwo) {
            return "true";
        } else {
            return "false";
        }
    }

    function DisplayOnChangeFile(files,displayClass){
        if (files && files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(displayClass).attr('src', e.target.result);
            }
            reader.readAsDataURL(files[0]);
        }
    }

    function CheckWebsiteVlidation(websiteValue,websiteId){
        var regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if(websiteValue != '' && websiteValue != null){
            if (!regexp.test(websiteValue)) {
                $(websiteClass).html('Enter valid website');
                return "true";
            } else {
                $(websiteClass).html('');
            }
        } else {
            $(websiteClass).html('');
        }
    }

    function CheckWebsiteVlidate(websiteValue,websiteIndex){
        var regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if(websiteValue != '' && websiteValue != null){
            if (!regexp.test(websiteValue)) {
                $(websiteIndex).html('Enter valid website');
                return "true";
            } else {
                $(websiteIndex).html('');
            }
        } else {
            $(websiteIndex).html('');
        }
    }
//common functions

//contact address js dependency - start
function changeRegion(currentDiv){
    var address_id = $(currentDiv).attr('data-id');
    
    var selRegion = $(".region_"+address_id+" option:selected").val();
    var idname      = "contact_address_country_id_"+address_id;
    // console.log(selRegion);
    if(selRegion != ""){
        $('#err_contact_address_region_id_'+address_id).html('');
    }

    $.ajax({
        type: "GET",
        url: "{{ url('country') }}",
        data: { region : selRegion, 'idname' :idname, 'address_id':address_id,'_token' : "{{ csrf_token() }}" } 
    }).done(function(data){
        $(".countryAutoLoad_"+address_id).html(data);
    });
    $("select option[value='']").attr("disabled", true);
}

function changeCountry(currentDiv){
    var address_id = $(currentDiv).attr('data-id');
    var selCountry = $(".country_"+address_id+" option:selected").val();
    console.log(address_id);
    console.log(selCountry);
    if(selCountry != ""){
        $('#err_contact_address_country_id_'+address_id).html("");
    }
    // contact main city dropdown
    $.ajax({
        type: "GET",
        url: "{{ url('city') }}",
        data: { country : selCountry, 'address_id':address_id, '_token' : "{{ csrf_token() }}" }
    }).done(function(data){
        $(".cityAutoLoad_"+address_id).html(data);
    });

    //contact main provience dropdown
    $.ajax({
        type: "GET",
        url: "{{ url('province') }}",
        data: { country : selCountry, 'address_id':address_id, '_token' : "{{ csrf_token() }}" }
    }).done(function(data){
        $(".provinceAutoLoad_"+address_id).html(data);
    });

    //contact main district dropdown
    $.ajax({
        type: "GET",
        url: "{{ url('district') }}",
        data: { country : selCountry, 'address_id':address_id, '_token' : "{{ csrf_token() }}" }
    }).done(function(data){
        $(".districtAutoLoad_"+address_id).html(data);
    });

    $("select option[value='']").attr("disabled", true);
}

//contact address js dependency - end
</script>

<script src="{{ asset('resources/admin-assets/js/edit_contact/contact.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/address.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/job_position.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/spouse.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/business.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/representive.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/referral.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/political_positions.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/dependents.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/communication.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/education.js') }}">
</script>
<script src="{{ asset('resources/admin-assets/js/edit_contact/greeting_letter.js') }}">
</script>
@endpush