<div role="tabpanel" class="tab-pane fade" id="jobposition" >
    {{-- \Session::get('submit_job_position')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Job Position </h4>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if (Session::has('addressSuccess'))
            <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('addressSuccess') !!}</div>
        @endif

        <div class="alert alert-danger contact-jobposition-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your input.
        </div>

        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <!-- if there are creation errors, they will show here -->
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/job-position/store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

        {!! Form::hidden('id', isset($jobpositionDetail->id)?$jobpositionDetail->id:"") !!}
        {!! Form::hidden('contact_id', $contact->id) !!}

        <!-- left -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Job type -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Type <span class="required">*</span> </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('job_type', ['1'=>'Owner','2'=>'Employed','3'=>'Freelancer','4'=>'Politician',],isset($jobpositionDetail->job_type)?:'$jobpositionDetail->job_type',['placeholder' => 'Select Job Type','class' => 'form-control','id'=>'JobPositionJobType']) }}
                    <span class="text-danger JobPositionJobType">{{ $errors->first('job_type') }}</span>
                </div>
            </div>

            <!-- Job Category -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Category</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('job_category', $jobCategory,isset($jobpositionDetail->job_category)?$jobpositionDetail->job_category:'',['placeholder' => 'Select Job Category','class' => 'form-control job_category']) }}
                    <span class="text-danger">{{ $errors->first('job_category') }}</span>
                </div>
            </div>

            <!-- Start Date -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('start_date',isset($jobpositionDetail->start_date)?date('m-d-Y',strtotime($jobpositionDetail->start_date)):'',['id'=>'start_date','class'=>'form-control', 'placeholder'=>'Start Date']) !!}
                    <span class="text-danger">{{ $errors->first('start_date') }}</span>
                </div>
            </div>

            <!-- LinkedIn text -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">LinkedIn</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('linkedin',isset($jobpositionDetail->linkedin)?$jobpositionDetail->linkedin:'',['id' => 'linkedin','class'=>'form-control', 'placeholder'=>'Linkedin text']) !!}
                    <span class="text-danger">{{ $errors->first('linkedin') }}</span>
                </div>
            </div>

            <!-- Country -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <span id="jobcountryAutoLoad">
                    <?php
                        $countryDropdown    = "";
                        $countryDropdown    .= "<select class='form-control country' name='country_id'>";
                        $countryDropdown    .= "<option value=''>Select Country</option>";
                        if(count($nations) > 0) {
                            foreach ($nations as $key => $country) {
                                $selected       = "";
                                if((isset($jobpositionDetail->country_id)?$jobpositionDetail->country_id:'') == ($country->country_code) ) {
                                    $selected   = "selected";
                                }

                                $countryDropdown    .= "<option value='$country->country_code' $selected >$country->country</option>";
                            }
                        }
                        $countryDropdown    .= "</select>";
                        echo $countryDropdown;
                    ?>
                    </span>
                    <span class="text-danger">{{ $errors->first('country_id') }}</span>
                </div>
            </div>

            <!-- District -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                District</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <span id="jobDistrictAutoLoad">
                    {{ Form::select('district_id', $jobDistrict,isset($jobpositionDetail->district_id)?$jobpositionDetail->district_id:'',['placeholder' => 'Select District','class' => 'form-control']) }}
                    </span>
                    <span class="text-danger">{{ $errors->first('district_id') }}</span>
                </div>
            </div>

            <!-- Postal Code -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Postal Code</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('postal_code',isset($jobpositionDetail->postal_code)?$jobpositionDetail->postal_code:'',['id' => 'postalcode','class'=>'form-control', 'placeholder'=>'Postal Code']) !!}
                    <span class="text-danger">{{ $errors->first('postal_code') }}</span>
                </div>
            </div>

            <!-- Address Line 1 -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 1</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('address_line_1',isset($jobpositionDetail->address_line_1)?$jobpositionDetail->address_line_1:'',['id' => 'addressline1','class'=>'form-control', 'placeholder'=>'Address Line 1']) !!}
                    <span class="text-danger">{{ $errors->first('address_line_1') }}</span>
                </div>
            </div>

            <!-- Website -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Website</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('website',isset($jobpositionDetail->website)?$jobpositionDetail->website:'',['id'=>'website','class'=>'form-control jobposition_website', 'placeholder'=>'Website']) !!}
                    <span class="text-danger jobpositionWebsite">{{ $errors->first('website') }}</span>
                </div>
            </div>
        </div>

        <!-- right -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Oranization -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Organization</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::select('oranization',['1'=>'organization text1','2'=>'organization text2','3'=>'organization text3','4'=>'organization text4'],isset($jobpositionDetail->oranization)?$jobpositionDetail->oranization:'',['class'=>'form-control', 'placeholder'=>'Select Organization']) !!}
                    <span class="text-danger">{{ $errors->first('oranization') }}</span>
                </div>
            </div>

            <!-- Job Positions -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Positions</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <span id='jocPositionAutoLoad'>
                    {{ Form::select('job_positions', $jobpositions,isset($jobpositionDetail->job_positions)?$jobpositionDetail->job_positions:'',['placeholder' => 'Select Job Positions','class' => 'form-control']) }}
                    </span>
                    <span class="text-danger">{{ $errors->first('job_positions') }}</span>
                </div>
            </div>

            <!-- Exit Date -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Exit Date</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('exit_date',isset($jobpositionDetail->exit_date)?date('m-d-Y',strtotime($jobpositionDetail->exit_date)):'',['id'=>'end_date','class'=>'form-control', 'placeholder'=>'Exit Date']) !!}
                    <span class="text-danger JobPositionEndDate">{{ $errors->first('exit_date') }}</span>
                </div>
            </div>


            <!-- Region -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Region</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('region_id', $region,isset($jobpositionDetail->region_id)?$jobpositionDetail->region_id:'',['placeholder' => 'Select region','class' => 'form-control jobregion']) }}
                <span class="text-danger">{{ $errors->first('region_id') }}</span>
                </div>
            </div>

            <!-- Province -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Province</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <span id="jobProvinceAutoLoad">
                    {{ Form::select('province_id', $jobProvince,isset($jobpositionDetail->province_id)?$jobpositionDetail->province_id:'',['placeholder' => 'Select Province','class' => 'form-control province']) }}
                    </span>
                    <span class="text-danger">{{ $errors->first('province_id') }}</span>
                </div>
            </div>

            <!-- City -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <span id="jobCityAutoLoad">
                    {{ Form::select('city_id', $jobCity,isset($jobpositionDetail->city_id)?$jobpositionDetail->city_id:'',['placeholder' => 'Select City','class' => 'form-control']) }}
                    </span>
                    <span class="text-danger">{{ $errors->first('city_id') }}</span>
                </div>
            </div>

            <!-- PO Box -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">PO Box </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('po_box',isset($jobpositionDetail->po_box)?$jobpositionDetail->po_box:'',['id' => 'pobox','class'=>'form-control', 'placeholder'=>'Po Box']) !!}
                    <span class="text-danger">{{ $errors->first('po_box') }}</span>
                </div>
            </div>

            <!-- Address Line 2 -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 2</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('address_line_2',isset($jobpositionDetail->address_line_2)?$jobpositionDetail->address_line_2:'',['id' => 'addressline2','class'=>'form-control', 'placeholder'=>'Address Line 2']) !!}
                    <span class="text-danger">{{ $errors->first('address_line_2') }}</span>
                </div>
            </div>

            <!-- Status -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                <div class="col-md-9 col-sm-9 col-xs-12" width="24px" height="24px">
                    <label>
                        <?php
                            $statusOn = true;
                            $statusOff = false;
                            if(isset($jobpositionDetail->status)) {
                                $statusOn     = ($jobpositionDetail->status == 1?true:false);
                                $statusOff      = ($jobpositionDetail->status == 0?true:false);
                            }
                        ?>
                        {!! Form::radio('status', '1',$statusOn ,['class' => 'flat' ]) !!}  Active
                    </label>
                    <label>
                        {!! Form::radio('status','0',$statusOff,['class' => 'flat']) !!}  Inactive
                    </label>
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                </div>
            </div>
        </div>

        <!-- keywords -->
        <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">keywords</label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('keywords',isset($jobpositionDetail->keywords)?$jobpositionDetail->keywords:'',['id'=>'keywords','class'=>'tagInput form-control', 'placeholder'=>'Type keyword']) !!}
                    <span class="text-danger">{{ $errors->first('keywords') }}</span>
                </div>
            </div>
        </div>

        <!-- notes -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
               <label class="col-md-12 col-sm-12 col-xs-12 control-label">Notes</label>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::textarea('notes', isset($jobpositionDetail->notes)?$jobpositionDetail->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'notes','size' => '3x3', 'id'=>'textEditor']) !!}
                <span class="text-danger">{{ $errors->first('notes') }}</span>
                </div>
            </div>
        </div>

        <!-- left start-->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($jobpositionDetail->user_createby['first_name'])?$jobpositionDetail->user_createby['first_name']:'';
                    $lastName   = isset($jobpositionDetail->user_createby['last_name'])?$jobpositionDetail->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($jobpositionDetail->created_at)?date_format($jobpositionDetail->created_at, 'jS M Y g:iA'):'';
                    ?>
                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                </div>
            </div>
        </div>
        <!-- left end-->

        <!-- right start-->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($jobpositionDetail->user_updateby['first_name'])?$jobpositionDetail->user_updateby['first_name']:'';
                    $lastName   = isset($jobpositionDetail->user_updateby['last_name'])?$jobpositionDetail->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($jobpositionDetail->updated_at)?date_format($jobpositionDetail->updated_at, 'jS M Y g:iA'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                </div>
            </div>
        </div>
        <!-- right end-->

        <!-- submit section -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit','name'=>'submit_job_position')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>