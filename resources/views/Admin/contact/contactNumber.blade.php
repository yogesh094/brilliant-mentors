<div role="tabpanel" class="tab-pane fade" id="contactNumber" >
    {{-- \Session::get('submit_contact_number')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Contact Number </h4>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if (Session::has('dependentSuccess'))
            <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('dependentSuccess') !!}</div>
        @endif

        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/contact-number-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($contactNumber->id)?$contactNumber->id:"") !!}
            {!! Form::hidden('contact_id', $contact->id) !!}

            {!! Form::hidden('political_position_id', isset($politicalPositionContact->id)?$politicalPositionContact->id:'') !!}
            {!! Form::hidden('contact_dependents_id', isset($dependentsContact->id)?$dependentsContact->id:'') !!}
            {!! Form::hidden('contact_spouse_id', isset($spouseContact->id)?$spouseContact->id:'') !!}
            {!! Form::hidden('contact_referral_id', isset($referralContact->id)?$referralContact->id:'') !!}
            {!! Form::hidden('contact_representative_id', isset($representativeContact->id)?$representativeContact->id:'') !!}

        <!-- left -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Contact Number -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number</label>
                <!-- Country Code -->
                <div class="col-md-3 col-sm-3 col-xs-3">
                    {{ Form::select('contact_country_code', $countryCode,isset($contactNumber->country_code)?$contactNumber->country_code:'',['id' => 'title','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('contact_country_code') }}</span>
                </div>

                <!-- Phone number -->
                <div class="col-md-6 col-sm-6 col-xs-9">
                    {{ Form::text('contact_phone_number',isset($contactNumber->number) ? $contactNumber->number:'', ['placeholder' => 'Phone Number','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('contact_phone_number') }}</span>
                </div>
            </div>

            <!-- Phone Number Type -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Phone Number Type</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('contact_number_type', ['1' => 'Business','2'=>'Mobile','3'=>'Home'], isset($contactNumber->phone_type) ? $contactNumber->phone_type:'', ['placeholder' => 'Select Phone Number Type','class' => 'form-control','id' =>'contact_number_type']) }}
                    <span class="text-danger">{{ $errors->first('contact_number_type') }}</span>
                </div>
            </div>

            <!-- Category -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('contact_category', ['1' => 'Contact Category1' , '2' => 'Contact Category2', '3' => 'Contact Category3','4' => 'Contact Category4'] ,isset($contactNumber->category)?$contactNumber->category:'', ['placeholder' => 'Select Category','class' => 'form-control','id' => 'contact_category']) }}
                    <span class="text-danger">{{ $errors->first('contact_category') }}</span>
                </div>
            </div>
        </div>

        <!-- right -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Fax -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('contact_fax',isset($contactNumber->fax_number)?$contactNumber->fax_number:'',['class'=>'form-control', 'placeholder'=>'Fax']) !!}
                    <span class="text-danger">{{ $errors->first('contact_fax') }}</span>
                </div>
            </div>

            <!-- importance -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('contact_importance', ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'], isset($contactNumber->importance)?$contactNumber->importance:'', ['placeholder' => 'Select Importance','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('contact_importance') }}</span>
                </div>
            </div>

            <!-- caution -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('contact_caution', ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'],isset($contactNumber->caution)?$contactNumber->caution:'',['placeholder' => 'Select Caution','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('contact_caution') }}</span>
                </div>
            </div>

            <!-- sensitivity -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('contact_sensitivity', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],isset($contactNumber->sensitivity)?$contactNumber->sensitivity:'', ['placeholder' => 'Select Sensitivity','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('contact_sensitivity') }}</span>
                </div>
            </div>
        </div>

        <!-- keywords -->
        <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    keywords
                </label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('contact_keywords',isset($contactNumber->keywords)?$contactNumber->keywords:'',['id'=>'keywords','class'=>'form-control tagInput', 'placeholder'=>'Type keyword']) !!}
                    <span class="text-danger">{{ $errors->first('contact_keywords') }}</span>
                </div>
            </div>
        </div>

        <!-- notes -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    Notes
                </label>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::textarea('contact_notes', isset($contactNumber->notes)?$contactNumber->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'notes','size' => '3x3']) !!}
                <span class="text-danger">{{ $errors->first('contact_notes') }}</span>
                </div>
            </div>
        </div>

        <!-- left -->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($contactNumber->user_createby['first_name'])?$contactNumber->user_createby['first_name']:'';
                    $lastName   = isset($contactNumber->user_createby['last_name'])?$contactNumber->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($contactNumber->created_at)?date_format($contactNumber->created_at, 'jS M Y g:iA'):'';
                    ?>

                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- right -->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($contactNumber->user_updateby['first_name'])?$contactNumber->user_updateby['first_name']:'';
                    $lastName   = isset($contactNumber->user_updateby['last_name'])?$contactNumber->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($contactNumber->updated_at)?date_format($contactNumber->updated_at, 'jS M Y g:iA'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>
        
        <!-- submit section -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}

                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>