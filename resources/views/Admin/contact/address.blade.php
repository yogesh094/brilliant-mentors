<div role="tabpanel" class="tab-pane fade" id="address"  >
{{-- \Session::get('submit_address')? 'active fade in':'' --}}

{!! Form::open(array('url' => ('contact/contactAddressStore'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

{!! Form::hidden('contact_id', $contact->id) !!}
    
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if (Session::has('successContact'))
            <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('successContact') !!}</div>
        @endif

        <div class="alert alert-danger contact-address-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your input.
        </div>
        
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <!-- if there are creation errors, they will show here -->
        <span class="errormessage"></span>
    </div>

    <div id="address_multiple">
        <div class="address_multi_fields" data-id="1">
            <div class="x_title">
                <h4>Address</h4>
                <div class="clearfix"></div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::hidden('id[]', isset($addressContact->id)?$addressContact->id:"") !!}
                <div class=" box-primary">

                    <!-- delivery address -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="col-md-2 col-sm-2 col-xs-12 control-label">Delivery Address<span class="required">*</span> </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                                $delAddrYes  = $delAddrNo   = false;
                                if(isset($addressContact->delivery_address)) {
                                    $delAddrYes     = ($addressContact->delivery_address == 1?true:false);
                                    $delAddrNo      = ($addressContact->delivery_address == 0?true:false);
                                }
                            ?>
                            <label class="delivery_address">
                                {!! Form::radio('delivery_address[]', '1',$delAddrYes,['class' => 'flat contact_address_delivery_address','id'=>'contact_address_delivery_address_1']) !!}  Yes
                            </label>
                            <label class="delivery_address">
                                {!! Form::radio('delivery_address[]', '0',$delAddrNo,['class' => 'flat contact_address_delivery_address','id'=>'contact_address_delivery_address_1']) !!}  No
                            </label>
                            <span class="text-danger" id="err_contact_address_delivery_address_1">{{ $errors->first('delivery_address') }}</span>
                        </div>
                    </div>

                    <!-- left -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        
                        <!-- type -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Type <span class="required">*</span> </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('type[]', ['1'=>'Office', '2' => 'Residential'],isset($addressContact->type)?$addressContact->type:'',['placeholder' => 'Select Type','class' => 'form-control contact_address_type','id' => 'contact_address_type_1','onchange'=>"getTypeValue(1)"]) }}
                                <span class="text-danger" id="err_contact_address_type_1">{{ $errors->first('type') }}</span>
                            </div>
                        </div>

                        <!-- country -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Country <span class="required">*</span> </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <span id='countryAutoLoad' class="countryAutoLoad_1">
                                    <?php
                                        $countryDropdown    = "";
                                        $countryDropdown    .= "<select class='form-control country_1 contact_address_country' name='country_id[]' id='contact_address_country_id_1' data-id='1' onchange='changeCountry(this)'>";
                                        $countryDropdown    .= "<option value=''>Select Country</option>";
                                        if(count($nations) > 0) {
                                            foreach ($nations as $key => $country) {
                                                // echo $addressContact->country_id;die;
                                                $selected       = "";
                                                if((isset($addressContact->country_id)?$addressContact->country_id:'') == ($country->country_code) ) {
                                                    $selected   = "selected";
                                                }

                                                $countryDropdown    .= "<option value='$country->country_code' $selected >$country->country</option>";
                                            }
                                        }
                                        $countryDropdown    .= "</select>";
                                        echo $countryDropdown;
                                    ?>
                                </span>
                                <span class="text-danger" id="err_contact_address_country_id_1">{{ $errors->first('country_id') }}</span>
                            </div>
                        </div>

                        <!-- province -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Province </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <span id='provinceAutoLoad' class="provinceAutoLoad_1">
                                    {{ Form::select('province_id[]', $province,isset($addressContact->province_id)?$addressContact->province_id:'',['placeholder' => 'Select Province','class' => 'form-control province']) }}
                                </span>
                                <span class="text-danger">{{ $errors->first('province_id') }}</span>
                            </div>
                        </div>

                        <!-- city -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <span id='cityAutoLoad' class="cityAutoLoad_1">
                                    {{ Form::select('city_id[]', $city,(isset($addressContact->city_id)?$addressContact->city_id:''),['placeholder' => 'Select City','class' => 'form-control']) }}
                                </span>
                                <span class="text-danger">{{ $errors->first('city_id') }}</span>
                            </div>
                        </div>

                        <!-- address line1 -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 1  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('address_line1[]',isset($addressContact->address_line1)?$addressContact->address_line1:'',['id' => 'address_line1','class'=>'form-control', 'placeholder'=>'Address Line 1']) !!}
                                <span class="text-danger">{{ $errors->first('address_line1') }}</span>
                            </div>
                        </div>

                        <!-- website -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Website  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('website[]',isset($addressContact->website)?$addressContact->website:'',['class'=>'form-control contact_address_website', 'placeholder'=>'Postal Website','id'=>'contact_address_website_1','onkeyup'=>'getWebsiteValue(1)']) !!}
                                <span class="text-danger" id="err_contact_address_website_1">{{ $errors->first('website') }}</span>
                            </div>
                        </div>

                        <!-- importance -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('importance[]', ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'],isset($addressContact->importance)?$addressContact->importance:'',['placeholder' => 'Select Importance','class' => 'form-control']) }}
                                <span class="text-danger">{{ $errors->first('importance') }}</span>
                            </div>
                        </div>
                    </div>
                    <!-- left -->

                    <!-- right -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">

                        <!-- region_id -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Region <span class="required">*</span> </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('region_id[]', $region,isset($addressContact->region_id)?$addressContact->region_id:'',['placeholder' => 'Select region','id' => 'contact_address_region_id_1', 'class' => 'form-control region_1 contact_address_region', 'data-id'=>'1', 'onchange'=>'changeRegion(this)']) }}
                                <span class="text-danger" id="err_contact_address_region_id_1">{{ $errors->first('region_id') }}</span>
                            </div>
                        </div>

                        <!-- district -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">District </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <span id='districtAutoLoad' class="districtAutoLoad_1">
                                    {{ Form::select('district_id[]', $district,isset($addressContact->district_id)?$addressContact->district_id:'',['placeholder' => 'Select District','class' => 'form-control']) }}
                                </span>
                                <span class="text-danger">{{ $errors->first('district_id') }}</span>
                            </div>
                        </div>

                        <!-- postal code -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Postalcode<span class="required">*</span> </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('postal_code[]',isset($addressContact->postal_code)?$addressContact->postal_code:'',['id' => 'contact_address_postal_code_1','class'=>'form-control contact_address_postal_code', 'placeholder'=>'Postal Code', 'onkeyup'=>'getPostalCodeValue(1)']) !!}
                                <span class="text-danger" id="err_contact_address_postal_code_1">{{ $errors->first('postal_code') }}</span>
                            </div>
                        </div>

                        <!-- po box -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Po Box  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('po_box[]',isset($addressContact->po_box)?$addressContact->po_box:'',['id' => 'po_box','class'=>'form-control', 'placeholder'=>'Po Box']) !!}
                                <span class="text-danger">{{ $errors->first('po_box') }}</span>
                            </div>
                        </div>

                        <!-- address line2 -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 2  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('address_line2[]',isset($addressContact->address_line2)?$addressContact->address_line2:'',['id' => 'address_line2','class'=>'form-control', 'placeholder'=>'Address Line 2']) !!}
                                <span class="text-danger">{{ $errors->first('address_line2') }}</span>
                            </div>
                        </div>

                        <!-- sensitivity -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('sensitivity[]', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],isset($addressContact->sensitivity)?$addressContact->sensitivity:null,['placeholder' => 'Select Sensitivity','class' => 'form-control']) }}
                                <span class="text-danger">{{ $errors->first('sensitivity') }}</span>
                            </div>
                        </div>

                        <!-- caution -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('caution[]', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],isset($addressContact->caution)?$addressContact->caution:null,['placeholder' => 'Select Caution','class' => 'form-control']) }}
                                <span class="text-danger">{{ $errors->first('caution') }}</span>
                            </div>
                        </div>
                    </div>
                    <!-- right -->
                </div>
            </div>
        </div>
    </div>

    <!-- submit section -->
    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right" style="margin-left: -5px">
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::button('Add', array('class' => 'btn btn-primary add_address')) !!}
            </div>
        </div>
    </div>

    <!-- keywords -->
    <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
         <label class="col-md-12 col-sm-12 col-xs-12 control-label">
            keywords
        </label>
        <div class="col-md-12 col-sm-12 col-xs-12">
            {!! Form::text('keywords',isset($addressContact->keywords)?$addressContact->keywords:'',['class'=>'tagInput form-control tagInput', 'placeholder'=>'Type keyword']) !!}
            <span class="text-danger">{{ $errors->first('keywords') }}</span>
        </div>
        </div>
    </div>

    <!-- notes -->
    <div class="form-group col-md-12 col-sm-12 col-xs-12">
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
           <label class="col-md-12 col-sm-12 col-xs-12 control-label">Notes</label>
             <div class="col-md-12 col-sm-12 col-xs-12">
            {!! Form::textarea('notes',isset($addressContact->notes)?$addressContact->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'Notes', 'rows' => 3]) !!}
            <span class="text-danger">{{ $errors->first('notes') }}</span>
            </div>
        </div>
    </div>

    <!-- left -->
    <!-- created section -->
    <div class="form-group col-md-6 col-sm-6 col-xs-12">
        <!-- Created by -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Created by  </label>
            <div class="col-md-9 col-sm-9 col-xs-12" >
                <?php 
                $firstName  = isset($addressContact->user_createby['first_name'])?$addressContact->user_createby['first_name']:'';
                $lastName   = isset($addressContact->user_createby['last_name'])?$addressContact->user_createby['last_name']:'';
                $createdBy  = $firstName.' '.$lastName;
                ?>
                {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
            </div>
        </div>

        <!-- Created on -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
            Created on  </label>
            <div class="col-md-9 col-sm-9 col-xs-12" >
                <?php 
                $createdAt   = isset($addressContact->created_at)?date_format($addressContact->created_at, 'jS M Y g:iA'):'';
                ?>

                {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                
            </div>
        </div>
    </div>

    <!-- right -->
    <!-- updated section -->
    <div class="form-group col-md-6 col-sm-6 col-xs-12">
        <!-- Updated by -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Updated by  </label>
            <div class="col-md-9 col-sm-9 col-xs-12" >
                <?php 
                $firstName  = isset($addressContact->user_updateby['first_name'])?$addressContact->user_updateby['first_name']:'';
                $lastName   = isset($addressContact->user_updateby['last_name'])?$addressContact->user_updateby['last_name']:'';
                $createdBy  = $firstName.' '.$lastName;
                ?>
                {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
            </div>
        </div>

        <!-- Updated on -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
            Updated on  </label>
            <div class="col-md-9 col-sm-9 col-xs-12" >
                <?php 
                $createdAt   = isset($addressContact->updated_at)?date_format($addressContact->updated_at, 'jS M Y g:iA'):'';
                ?>
                {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
            </div>
        </div>
    </div>

    <!-- submit section -->
    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::submit('Submit', array('class' => 'btn btn-primary submit','name'=>'submit_contact_address')) !!}
                <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>