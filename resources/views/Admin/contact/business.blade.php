<div role="tabpanel" class="tab-pane fade" id="business" >
    {{-- \Session::get('submit_business')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Business </h4>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if (Session::has('spouseSuccess'))
            <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('spouseSuccess') !!}</div>
        @endif

        <div class="alert alert-danger contact-business-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your input.
        </div>

        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
            {!! Form::open(array('url' => ('contact/business-store'), 'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id', isset($businessDetail->id)?$businessDetail->id:"") !!}
            {!! Form::hidden('contact_id', $contact->id) !!}
        <!-- left -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Job type -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">BusinessType</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('business_type', ['1'=>'Owner','2'=>'Employed','3'=>'Freelancer','4'=>'Politician',],isset($businessDetail->business_type)?$businessDetail->business_type:'',['placeholder' => 'Select Business Type','class' => 'form-control']) }}

                    <span class="text-danger">{{ $errors->first('business_type') }}</span>
                </div>
            </div>

            <!-- Description -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('description',isset($businessDetail->description)?$businessDetail->description:'',['class'=>'form-control', 'placeholder'=>'Description']) !!}
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                </div>
            </div>

            <!-- Number Of Employees -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Number Of Employees</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('number_of_employees', [10,20,30,40,50], isset($businessDetail->number_of_employees)?$businessDetail->number_of_employees:'',['placeholder' => 'Select number of employees','class' => 'form-control']) }}
                <span class="text-danger">{{ $errors->first('number_of_employees') }}</span>
                </div>
            </div>

            <!-- Country -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <span id="businesscountryAutoLoad">
                    <?php
                        $countryDropdown    = "";
                        $countryDropdown    .= "<select class='form-control businesscountry' name='country_id'>";
                        $countryDropdown    .= "<option value=''>Select Country</option>";
                        if(count($nations) > 0) {
                            foreach ($nations as $key => $country) {
                                $selected       = "";
                                if((isset($businessDetail->country_id)?$businessDetail->country_id:'') == ($country->country_code) ) {
                                    $selected   = "selected";
                                }

                                $countryDropdown    .= "<option value='$country->country_code' $selected >$country->country</option>";
                            }
                        }
                        $countryDropdown    .= "</select>";
                        echo $countryDropdown;
                    ?>
                    </span>
                    <span class="text-danger">{{ $errors->first('country_id') }}</span>
                </div>
            </div>

            <!-- District -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">District</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <span id="businessDistrictAutoLoad">
                    {{ Form::select('district_id', $businessDistrict,isset($businessDetail->district_id)?$businessDetail->district_id:'',['placeholder' => 'Select District','class' => 'form-control']) }}
                    </span>
                    <span class="text-danger">{{ $errors->first('district_id') }}</span>
                </div>
            </div>

            <!-- Postal Code -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Postalcode</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('postal_code', isset($businessDetail->postal_code)? $businessDetail->postal_code:'',['id' => 'postalcode','class'=>'form-control', 'placeholder'=>'Postal Code']) !!}
                    <span class="text-danger">{{ $errors->first('postal_code') }}</span>
                </div>
            </div>

            <!-- Address Line 1 -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 1</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('address_line_1',isset($businessDetail->address_line_1)?$businessDetail->address_line_1:'',['id' => 'addressline1','class'=>'form-control', 'placeholder'=>'Address Line 1']) !!}
                    <span class="text-danger">{{ $errors->first('address_line_1') }}</span>
                </div>
            </div>
        </div>

        <!-- right -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Oranization -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Organization</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::select('organization', ['1'=>'organization text1','2'=>'organization text2','3'=>'organization text3','4'=>'organization text4'],isset($businessDetail->organization)?$businessDetail->organization:'',['class'=>'form-control', 'placeholder'=>'Select Organization']) !!}
                    <span class="text-danger">{{ $errors->first('organization') }}</span>
                </div>
            </div>

            <!-- Start Date -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date  <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('start_at', isset($businessDetail->start_at)?date('m-d-Y',strtotime($businessDetail->start_at)):'',['id'=>'start_at','class'=>'form-control', 'placeholder'=>'Start Date']) !!}
                    <span class="text-danger businessStartDate">{{ $errors->first('start_at') }}</span>
                </div>
            </div>

            <!-- Region -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Region</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('region_id', $region,isset($businessDetail->region_id)?$businessDetail->region_id:'',['placeholder' => 'Select region','class' => 'form-control businessregion']) }}
                <span class="text-danger">{{ $errors->first('region_id') }}</span>
                </div>
            </div>

            <!-- Province -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Province</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <span id="businessProvinceAutoLoad">
                    {{ Form::select('province_id', $businessProvince,isset($businessDetail->province_id)?$businessDetail->province_id:'',['placeholder' => 'Select Province','class' => 'form-control businessprovince']) }}
                    </span>
                    <span class="text-danger">{{ $errors->first('province_id') }}</span>
                </div>
            </div>

            <!-- City -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <span id="businessCityAutoLoad">
                    {{ Form::select('city_id', $businessCity,isset($businessDetail->city_id)?$businessDetail->city_id:'',['placeholder' => 'Select City','class' => 'form-control']) }}
                    </span>
                    <span class="text-danger">{{ $errors->first('city_id') }}</span>
                </div>
            </div>

            <!-- PO Box -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">PO Box </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('po_box',isset($businessDetail->po_box)?$businessDetail->po_box:'',['id' => 'pobox','class'=>'form-control', 'placeholder'=>'Po Box']) !!}
                    <span class="text-danger">{{ $errors->first('po_box') }}</span>
                </div>
            </div>

            <!-- Address Line 2 -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 2</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('address_line_2',isset($businessDetail->address_line_2)?$businessDetail->address_line_2:'',['id' => 'addressline2','class'=>'form-control', 'placeholder'=>'Address Line 2']) !!}
                    <span class="text-danger">{{ $errors->first('address_line_2') }}</span>
                </div>
            </div>

            <!-- Website -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Website </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('website',isset($businessDetail->website)?$businessDetail->website:'',['id'=>'website','class'=>'form-control business_website', 'placeholder'=>'Website']) !!}
                    <span class="text-danger businessWebsite">{{ $errors->first('website') }}</span>
                </div>
            </div>
        </div>

        <!-- keywords -->
        <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    keywords
                </label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('Keywords',isset($businessDetail->Keywords)?$businessDetail->Keywords:'',['id'=>'Keywords','class'=>'form-control tagInput', 'placeholder'=>'Type keyword']) !!}
                    <span class="text-danger">{{ $errors->first('Keywords') }}</span>
                </div>
            </div>
        </div>

        <!-- notes -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
               <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    Notes
                </label>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::textarea('notes', isset($businessDetail->notes)?$businessDetail->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'notes','size' => '3x3']) !!}
                <span class="text-danger">{{ $errors->first('notes') }}</span>
                </div>
            </div>
        </div>

        <!-- left -->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($businessDetail->user_createby['first_name'])?$businessDetail->user_createby['first_name']:'';
                    $lastName   = isset($businessDetail->user_createby['last_name'])?$businessDetail->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($businessDetail->created_at)?date_format($businessDetail->created_at, 'jS M Y g:iA'):'';
                    ?>

                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- right -->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($businessDetail->user_updateby['first_name'])?$businessDetail->user_updateby['first_name']:'';
                    $lastName   = isset($businessDetail->user_updateby['last_name'])?$businessDetail->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($businessDetail->updated_at)?date_format($businessDetail->updated_at, 'jS M Y g:iA'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- submit section -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_business')) !!}

                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        </div>

        {!! Form::close() !!}
        </div>
    </div>
</div>