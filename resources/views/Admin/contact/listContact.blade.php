@extends('Admin.master_layout.master')

@section('title', 'Contact List')

@section('breadcum')
     / Contact List 
@endsection

@section('content')
<div class="">
    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>contact Info</h4>
                    <a href="{{ url('contact/add') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add Contact
                    </a>
                     <a class="btn btn-sm btn-xs btn-warning pull-right">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?php
                    /*
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap w100proc">
                        <thead>
                            <tr>
                                <th width=''>#</th>
                                <th width=''>Name</th>
                                <th width=''>Nickname</th>
                                <th width=''>Birth Place</th>
                                <th width=''>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (!empty($contactList)) {
                                    $i = 0;
                                    foreach ($contactList as $contact) {
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?= $i ?></td>
                                            <td>
                                                {{ $contact->full_name }}
                                            </td>

                                            <td>
                                                {{ $contact->nick_name }}
                                            </td>    

                                            <td>
                                                {{ $contact->birth_place }}
                                            </td>  

                                            <td>
                                                <a href="<?= url('contact/edit/' . $contact->id); ?>" class="btn btn-success btn-sm" title='Edit'><i class="fa fa-pencil"></i></a>
                                                <a href="<?= url('contact/delete/' . $contact->id); ?>" class="btn btn-danger btn-sm" onclick="if (confirm('Do you want to delete?')) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }" title='Delete'><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                    */
                    ?>
                     <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <!-- <th>Title</th> -->
                            <th>Name</th>
                            <th>Nick Name</th>
                             <th>Gender</th>
                             <th>Phone</th>
                             <th>Email</th>
                            <th>City</th>
                            <th>Status</th>
                            
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('resources/admin-assets/js/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/dataTables.bootstrap.min.js') }}"></script>
         <script src="{{ asset('resources/admin-assets/js/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
         <script src="{{ asset('resources/admin-assets/js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
         <script src="{{ asset('resources/admin-assets/js/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/responsive.bootstrap.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/dataTables.scroller.min.js') }}"></script>
<script>
    $(function () {
        $('#data-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: "{{ url('/contact/array-data') }}",
            columns: [
                //{data: 'id', orderable: true, searchable: true },
                // {data: 'title',orderable: false, searchable: false},
                {data: 'full_name',orderable: true, searchable: true},
                {data: 'nick_name',orderable: true, searchable: true},
                {data: 'gender',orderable: false, searchable: false},
                {data: 'phone',orderable: false, searchable: false},
                {data: 'email',orderable: false, searchable: false},
                {data: 'city',orderable: false, searchable: false},
                {data: 'status', orderable: true},
               // {data: 'created_at', searchable: false },
               // {data: 'updated_at', searchable: false },
                //{data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                    next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
    });
</script>
@endpush('scripts')