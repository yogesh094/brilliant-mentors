<div role="tabpanel" class="tab-pane fade" id="dependents" >
    {{-- \Session::get('submit_dependents')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Dependents </h4>
        <div class="clearfix"></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if (Session::has('politicalSuccess'))
            <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('politicalSuccess') !!}</div>
        @endif

        <div class="alert alert-danger contact-dependents-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your input.
        </div>

        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <!-- if there are creation errors, they will show here -->
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/dependents-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($dependentsContact->id)?$dependentsContact->id:"") !!}
            {!! Form::hidden('contact_id', $contact->id) !!}

        <!-- left -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">

            <!-- First Name -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name <span class="required">*</span> </label>

                <!-- title -->
                <div class="col-md-3 col-sm-3 col-xs-3">
                    {{ Form::select('dependents_title', ['1' => 'Mr.' , '2' => 'Mrs.' , '3' => 'Miss.'],isset($dependentsContact->title)?$dependentsContact->title:'',['id' => 'dependentstitle','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('dependents_title') }}</span>
                </div>

                <!-- First Name -->
                <div class="col-md-6 col-sm-6 col-xs-9">
                    {{ Form::select('dependents_first_name', ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => '    Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'],isset($dependentsContact->first_name)?$dependentsContact->first_name:'',['placeholder' => 'Select First Name','class' => 'form-control','id'=>'dependentsFirstName']) }}
                    <span class="text-danger dependentsFirstNameError">{{ $errors->first('dependents_first_name') }}</span>
                </div>
            </div>

            <!-- suffix -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Suffix <span class="required">*</span> </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::select('dependents_suffix[]',['1' => 'CLU' , '2' => 'D.D.S.', '3' => 'D.V.M.', '4' => 'Esq.', '5' => 'LL.D.', '6' => 'M.D.', '7' => 'O.S.B.', '8' => 'Ph.D.', '9' => 'R.G.S', '10' => 'S.H.C.J.'],isset($dependentsContact->suffix)?explode(',',$dependentsContact->suffix):'',['class'=>'form-control selectheight', 'placeholder'=>'Select Suffix','multiple' => 'multiple','id'=>'dependentsSuffix']) !!}
                    <span class="text-danger dependentsSuffixError">{{ $errors->first('dependents_suffix') }}</span>
                </div>
            </div>

            <!-- Father Name -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Father Name</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_father_name', ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '6' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'],isset($dependentsContact->father_name)?$dependentsContact->father_name:'',['placeholder' => 'Select Father Name','class' => 'form-control','id' =>'dependents_father_name']) }}

                    <span class="text-danger">{{ $errors->first('dependents_father_name') }}</span>
                </div>
            </div>

            <!-- Mother Name -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Mother Name</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_mother_name', ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ] ,isset($dependentsContact->mother_name)?$dependentsContact->mother_name:'', ['placeholder' => 'Select Mother Name','class' => 'form-control','id' => 'dependents_mother_name']) }}
                    <span class="text-danger">{{ $errors->first('dependents_mother_name') }}</span>
                </div>
            </div>

            <!-- Surname -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Surname <span class="required">*</span> </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_surname', ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'],isset($dependentsContact->surname)?$dependentsContact->surname:'',['placeholder' => 'Select Surname','class' => 'form-control','id' => 'dependentsSurname']) }}
                    <span class="text-danger dependentsSurnameError">{{ $errors->first('dependents_surname') }}</span>
                </div>
            </div>

            <!-- nick name -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('dependents_nick_name',isset($dependentsContact->nickname)?$dependentsContact->nickname:'',['class'=>'form-control', 'placeholder'=>'Nick Name']) !!}
                    <span class="text-danger">{{ $errors->first('dependents_nick_name') }}</span>
                </div>
            </div>

            <!-- DOB -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('dependents_date_of_birth',isset($dependentsContact->date_of_birth)?date("m-d-Y",strtotime($dependentsContact->date_of_birth)):'',['id'=>'dependents_date_of_birth','class'=>'form-control', 'placeholder'=>'Date of Birth']) !!}
                    <span class="text-danger">{{ $errors->first('dependents_date_of_birth') }}</span>
                </div>
            </div>

            <!-- birth place -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Birth Place</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::select('dependents_birth_place',['1'=>'Akkar','2'=>'Akoura','3'=>'Antelias','4'=>'Ashrafieh'],isset($dependentsContact->birth_place)?$dependentsContact->birth_place:'',['class'=>'form-control', 'placeholder'=>'Birth Place']) !!}
                    <span class="text-danger">{{ $errors->first('dependents_birth_place') }}</span>
                </div>
            </div>

            <!-- blood type -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Type</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_blood_type',$bloodType,isset($dependentsContact->blood_type)?$dependentsContact->blood_type:'',['placeholder' => 'Select Blood Type','class' => 'form-control']) }}

                    <span class="text-danger">{{ $errors->first('dependents_blood_type') }}</span>
                </div>
            </div>

            <!-- gender -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Gender</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_gender', $gender,isset($dependentsContact->gender)?$dependentsContact->gender:'',['placeholder' => 'Select Gender','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('dependents_gender') }}</span>
                </div>
            </div>

            <!-- No of Dependents -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                No Of Dependents</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::text('dependents_number_of_dependents', isset($dependentsContact->number_of_dependents)?$dependentsContact->number_of_dependents:'',['placeholder' => 'Number Of Dependents','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('dependents_number_of_dependents') }}</span>
                </div>
            </div>

            <!-- nationality -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nationality</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_nationality[]', $nationality,isset($dependentsContact->nationality)?explode(',',$dependentsContact->nationality):'',['placeholder' => 'Select Nationality','class' => 'form-control selectheight','multiple' => 'multiple']) }}
                    <span class="text-danger">{{ $errors->first('dependents_nationality') }}</span>
                </div>
            </div>

            <!-- languages -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Languages</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_languages[]', $languages,isset($dependentsContact->languages)?explode(',',$dependentsContact->languages):'',['placeholder' => 'Select Language','class' => 'form-control selectheight','multiple' => 'multiple']) }}
                    <span class="text-danger">{{ $errors->first('dependents_languages') }}</span>
                </div>
            </div>
        </div>

        <!-- right -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">

            <!-- photo box -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3 my-photo-box">
                    <div style="text-align: left;float:none;">
                        <?php
                        if(isset($dependentsContact->photo) && $dependentsContact->photo != "") {
                        ?>
                            <img src="{{ $imageUrl.$dependentsContact->photo }}" class="img-thumbnail contact_spouse_photo dependents_image" >
                        <?php
                        } else {
                        ?>
                            <img src="{{ url('/uploads/contact/avatar.png') }}" class="img-thumbnail contact_spouse_photo dependents_image" >
                        <?php
                        }
                        ?>
                        <input type="hidden" name="old_contact_dependents_image" value="{{ isset($dependentsContact->photo)?$dependentsContact->photo:'' }}">
                    </div>
                </div>
            </div>

            <!-- Photo -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Photo</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::File('dependents_photo',['class'=>'form-control', 'accept'=>'.jpeg,.jpg,.png', 'placeholder'=>'Photo']) !!}
                    <span class="text-danger dependentsPhotoError">{{ $errors->first('dependents_photo') }}</span>
                </div>
            </div>

            <!-- profession -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Profession </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_profession[]', ['1' => 'Profession 1' , '2' => 'Profession 2' , '3' => 'Profession 3', '4' => 'Profession 4', '5' => 'Profession 5'],isset($dependentsContact->profession)?explode(',',$dependentsContact->profession):'',['placeholder' => 'Select Profession','class' => 'form-control selectheight', 'multiple' => 'multiple']) }}
                    <span class="text-danger">{{ $errors->first('dependents_profession') }}</span>
                </div>
            </div>

            <!-- importance -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_importance', ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'], isset($dependentsContact->importance)?$dependentsContact->importance:'', ['placeholder' => 'Select Importance','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('dependents_importance') }}</span>
                </div>
            </div>

            <!-- caution -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_caution', ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'],isset($dependentsContact->caution)?$dependentsContact->caution:'',['placeholder' => 'Select Caution','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('dependents_caution') }}</span>
                </div>
            </div>

            <!-- sensitivity -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_sensitivity', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],isset($dependentsContact->sensitivity)?$dependentsContact->sensitivity:'', ['placeholder' => 'Select Sensitivity','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('dependents_sensitivity') }}</span>
                </div>
            </div>

            <!-- Religion -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Religion </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_religion', ['1' => 'Christian', '2' => 'Muslim'],isset($dependentsContact->religion_id)?$dependentsContact->religion_id:'' ,['placeholder' => 'Select Religion','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('dependents_religion') }}</span>
                </div>
            </div>

            <!-- Internal Organizations -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Internal Organizations </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_internal_organizations[]', ['1' => 'Makhzoumi Foundation' , '2' => 'Beirutiyat', '3' => 'Makhzoumi Health Care', '4' => 'Makhzoumi Development'],isset($dependentsContact->internal_organizations)?explode(',',$dependentsContact->internal_organizations):'',['placeholder' => 'Select Internal Organizations','class' => 'form-control selectheight', 'multiple'=>'multiple']) }}
                    <span class="text-danger">{{ $errors->first('dependents_internal_organizations') }}</span>
                </div>
            </div>

            <!-- Administrative Group -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Administrative Group </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('dependents_administrative_group[]', ['1' => 'Business Group' , '2' => 'Volunteer Group', '3' => 'Administration Group', '4' => 'Management Group'],isset($dependentsContact->administrative_group)?explode(',',$dependentsContact->administrative_group):'',['placeholder' => 'Select Administrative Group','class' => 'form-control selectheight', 'multiple'=>'multiple']) }}
                    <span class="text-danger">{{ $errors->first('dependents_administrative_group') }}</span>
                </div>
            </div>

            <!-- Status -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Status  </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <label>
                        <?php
                            $statusYes  = true;
                            $statusNo   = false;
                            if(isset($dependentsContact->status)) {
                                $statusYes = ($dependentsContact->status == 1?true:false);
                                $statusNo  = ($dependentsContact->status == 0?true:false);
                            }
                        ?>
                        {!! Form::radio('dependents_status', '1', $statusYes, ['class' => 'flat' ]) !!}  Active
                    </label>
                    <label>
                        {!! Form::radio('dependents_status', '0', $statusNo, ['class' => 'flat']) !!}  Inactive
                    </label>
                </div>
            </div>
        </div>

        <!-- keywords -->
        <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    keywords
                </label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('dependents_keywords',isset($dependentsContact->keywords)?$dependentsContact->keywords:'',['id'=>'keywords','class'=>'tagInput form-control', 'placeholder'=>'Type keyword']) !!}
                    <span class="text-danger">{{ $errors->first('dependents_keywords') }}</span>
                </div>
            </div>
        </div>

        <!-- notes -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    Notes
                </label>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::textarea('dependents_notes', isset($dependentsContact->notes)?$dependentsContact->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'notes','size' => '3x3']) !!}
                <span class="text-danger">{{ $errors->first('dependents_notes') }}</span>
                </div>
            </div>
        </div>

        <!-- left -->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($dependentsContact->user_createby['first_name'])?$dependentsContact->user_createby['first_name']:'';
                    $lastName   = isset($dependentsContact->user_createby['last_name'])?$dependentsContact->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($dependentsContact->created_at)?date_format($dependentsContact->created_at, 'jS M Y g:iA'):'';
                    ?>

                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- right -->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($dependentsContact->user_updateby['first_name'])?$dependentsContact->user_updateby['first_name']:'';
                    $lastName   = isset($dependentsContact->user_updateby['last_name'])?$dependentsContact->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($dependentsContact->updated_at)?date_format($dependentsContact->updated_at, 'jS M Y g:iA'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- submit section -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_dependents')) !!}

                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>