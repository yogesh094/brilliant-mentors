<div role="tabpanel" class="tab-pane fade active in" id="addContact" >
    <div class="x_title">
        <h4>Contact </h4>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            @if (Session::has('successContact'))
                <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('successContact') !!}</div>
            @endif
            @if (Session::has('errorContact'))
                <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('errorContact') !!}</div>
            @endif
            <div class="alert alert-danger contact-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
            </div>
            @if(count($errors))
                <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                </div>
            @endif
            <!-- if there are creation errors, they will show here -->
            <span class="errormessage"></span>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
            {!! Form::open(array('url' => ('contact/store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',$contact->id) !!}
            <!-- left -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- private -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">

                    <!-- private -->
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Private  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <label>
                            <?php
                            $privateYes     = $contact->private == 1?true:false;
                            ?>
                            {!! Form::radio('private', '1',$privateYes,['class' => 'flat']) !!}  Yes
                        </label>
                        <label>
                            <?php
                            $privateNo      = $contact->private == 0?true:false;
                            ?>
                            {!! Form::radio('private', '0',$privateNo,['class' => 'flat']) !!}  No
                        </label>
                    </div>

                </div>

                <!-- Business Owner -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Business Owner  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                                $businessOwnYes = $businessOwnNo = false;
                                if(isset($contact->business_owner)) {
                                    $businessOwnYes = ($contact->business_owner == 1?true:false);
                                    $businessOwnNo  = ($contact->business_owner == 0?true:false);
                                }
                            ?>
                            <label>
                                {!! Form::radio('business_owner', '1',$businessOwnYes,['class' => 'flat']) !!}  Yes
                            </label>

                            <label>
                                {!! Form::radio('business_owner', '0',$businessOwnNo,['class' => 'flat']) !!}  No
                            </label>
                        </div>

                    <div class="col-md-4 col-sm-4 col-xs-6">

                    </div>
                </div>

                <!-- Contact Unique Key -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact ID<span class="required">*</span> </label>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::text('contact_unique_key',$contact->contact_unique_key,['class'=>'form-control', 'placeholder'=>'Contact Unique Key']) !!}
                        <span class="text-danger"  id="err_contact_unique_key" >{{ $errors->first('contact_unique_key') }}</span>
                    </div>

                    <div class="col-md-5 col-sm-4 col-xs-6">
                        {!! Form::text('system_unique_key',$contact->system_unique_key,['class'=>'form-control', 'placeholder'=>'Contact Unique Key','disabled' => 'disabled' ]) !!}
                        <span class="text-danger">{{ $errors->first('system_unique_key') }}</span>
                    </div>
                </div>

                <!-- Title -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Title<span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('title', ['1' => 'Mr.' , '2' => 'Mrs.' , '3' => 'Miss.'],$contact->title,['id' => 'title','class' => 'form-control', 'id' => 'contact_title']) }}
                        <span class="text-danger" id=
                    "err_contact_title">{{ $errors->first('title') }}</span>
                    </div>
                </div>

                <!-- suffix -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-6">Suffix<span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('suffix[]', ['1' => 'CLU' , '2' => 'D.D.S.', '3' => 'D.V.M.', '4' => 'Esq.', '5' => 'LL.D.', '6' => 'M.D.', '7' => 'O.S.B.', '8' => 'Ph.D.', '9' => 'R.G.S', '10' => 'S.H.C.J.'],explode(',',$contact->suffix),['placeholder' => 'Select Suffix','class' => 'form-control selectheight' , 'id'=>'contact_suffix' ,'multiple' => 
                        'multiple']) }}
                        <span class="text-danger" id='err_contact_suffix' >{{ $errors->first('suffix') }}</span>
                    </div>
                </div>

                <!-- First Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name<span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('first_name', ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => '    Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'],$contact->first_name,['placeholder' => 'Select First Name', 'id'=>'contact_first_name','class' => 'form-control']) }}
                        <span class="text-danger" id="err_contact_first_name" >{{ $errors->first('first_name') }}</span>
                    </div>
                </div>

                <!-- Father Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Father Name<span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('father_name', ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '6' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'],$contact->father_name,['placeholder' => 'Select Father Name', 'id'=>'contact_father_name','class' => 'form-control']) }}
                        <span class="text-danger" id="err_contact_father_name">{{ $errors->first('father_name') }}</span>
                    </div>
                </div>

                <!-- Mother Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mother Name<span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('mother_name', ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ] ,$contact->mother_name, ['placeholder' => 'Select Mother Name','id'=>'contact_mother_name','class' => 'form-control']) }}
                        <span class="text-danger" id="err_contact_mother_name">{{ $errors->first('mother_name') }}</span>
                    </div>
                </div>

                <!-- Surname -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Surname<span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('surname', ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'],$contact->surname,['placeholder' => 'Select Surname','class' => 'form-control','id'=>'contact_surname']) }}
                        <span class="text-danger" id="err_contact_surname">{{ $errors->first('surname') }}</span>
                    </div>
                </div>

                <!-- DOB -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth<span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {!! Form::text('date_of_birth',date("m-d-Y",strtotime($contact->date_of_birth)),['id'=>'date_of_birth','class'=>'form-control', 'placeholder'=>'Date of Birth']) !!}
                        <span class="text-danger" id="err_date_of_birth">{{ $errors->first('date_of_birth') }}</span>
                    </div>
                </div>

                <!-- blood type -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Type  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('blood_type', $bloodType,$contact->blood_type,['placeholder' => 'Select Blood Type','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('blood_type') }}</span>
                    </div>
                </div>

                <!-- nationality -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nationality  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('nationality[]', $nationality,explode(',',$contact->nationality),['placeholder' => 'Select Nationality','class' => 'form-control selectheight', 'multiple' => 'multiple']) }}
                        <span class="text-danger">{{ $errors->first('nationality') }}</span>
                    </div>
                </div>

                <!-- languages -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Languages  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('languages[]', $languages,explode(',',$contact->languages),['placeholder' => 'Select Languages','class' => 'form-control selectheight', 'multiple' => 'multiple']) }}
                        <span class="text-danger">{{ $errors->first('languages') }}</span>
                    </div>
                </div>

                <!-- sponsorships -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sponsorships </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('sponsorships', ['1' => 'JPMorgan' , '2' => 'Microsoft', '3' => 'Deloitte', '4' => 'PepsiCo', '5' => 'DHL Express'],$contact->sponsorships,['placeholder' => 'Select Sponsorships','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('sponsorships') }}</span>
                    </div>
                </div>

                <!-- political_party -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Party </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('political_party', ['1' => 'Movement of the future' , '2' => 'Kataeb Party', '3' => 'Hezbollah', '4' => 'Free Patriotic Movement'],$contact->political_party,['placeholder' => 'Select Political party','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('political_party') }}</span>
                    </div>
                </div>

                <!-- charitable_organizations -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Charitable Organizations </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('charitable_organizations', ['1' => 'YSR FOUNDATION ' , '2' => 'THE COCONADA CHAMBER OF COMMERCE', '3' => 'RAVINDRANATH MEDICAL FOUNDATION'],$contact->charitable_organizations,['placeholder' => 'Select Charitable Organizations','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('charitable_organizations') }}</span>
                    </div>
                </div>
            </div>
            <!-- left -->

            <!-- right -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- photo box -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-right">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3 my-photo-box">
                        <div style="text-align: left;float:none;">
                            <?php
                            if(isset($contact->photo) && $contact->photo != "") {
                            ?>
                                <img src="{{ $imageUrl.$contact->photo }}" class="img-thumbnail contact_spouse_photo contact_image" >
                            <?php
                            } else {
                            ?>
                                <img src="{{ url('/uploads/contact/avatar.png') }}" class="img-thumbnail contact_spouse_photo contact_image" >
                            <?php
                            }
                            ?>
                            <input type="hidden" name="old_contact_image" value="{{ isset($contact->photo)?$contact->photo:'' }}">
                        </div>
                    </div>
                </div>

                <!-- photo upload -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Photo </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {!! Form::File('photo',['class'=>'form-control', 'accept'=>'.jpeg,.jpg,.png', 'placeholder'=>'Photo']) !!}
                        <span class="text-danger" id="err_contact_photo">{{ $errors->first('photo') }}</span>
                    </div>
                </div>

                <!-- nick name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {!! Form::text('nick_name',$contact->nick_name,['class'=>'form-control', 'placeholder'=>'Nick Name']) !!}
                        <span class="text-danger">{{ $errors->first('nick_name') }}</span>
                    </div>
                </div>

                <!-- birth place -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Birth Place </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $countryDropdown    = "";
                        $countryDropdown    .= "<select class='form-control' name='birth_place'>";
                        $countryDropdown    .= "<option value=''>Select Birth Place </option>";
                        if(count($nations) > 0) {
                            foreach ($nations as $key => $country) {
                                $selected   =  "";
                                if($contact->birth_place == $country->country_code ) {
                                    $selected   = "selected";
                                }
                                $countryDropdown    .= "<option value='$country->country_code' $selected >$country->country</option>";
                            }
                        }
                        $countryDropdown    .= "</select>";
                        echo $countryDropdown;
                        ?>
                        <span class="text-danger">{{ $errors->first('birth_place') }}</span>
                    </div>
                </div>

                <!-- gender -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Gender  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('gender', $gender,$contact->gender,['placeholder' => 'Select Gender','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('gender') }}</span>
                    </div>
                </div>

                <!-- marital_status -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Marital Status  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('marital_status', $maritalStatus,$contact->marital_status,['placeholder' => 'Select Marital Status','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('marital_status') }}</span>

                    </div>
                </div>

                <!-- religion -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Religion </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('religion', ['1' => 'Christian', '2' => 'Muslim'],$contact->religion,['placeholder' => 'Select Religion','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('religion') }}</span>
                    </div>
                </div>

                <!-- profession -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Profession </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('profession[]', ['1' => 'Profession 1' , '2' => 'Profession 2' , '3' => 'Profession 3', '4' => 'Profession 4', '5' => 'Profession 5'],explode(',',$contact->profession),['placeholder' => 'Select Profession','class' => 'form-control selectheight', 'multiple' => 'multiple']) }}
                        <span class="text-danger">{{ $errors->first('profession') }}</span>
                    </div>
                </div>

                <!-- importance -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('importance', ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'],$contact->importance,['placeholder' => 'Select Importance','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('importance') }}</span>
                    </div>
                </div>

                <!-- sensitivity -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('sensitivity', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],$contact->sensitivity,['placeholder' => 'Select Sensitivity','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('sensitivity') }}</span>
                    </div>
                </div>

                <!-- caution -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('caution', ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'],$contact->caution,['placeholder' => 'Select Caution','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('caution') }}</span>
                    </div>
                </div>

                <!-- internal_organizations -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Internal Organizations </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('internal_organizations', ['1' => 'Makhzoumi Foundation' , '2' => 'Beirutiyat', '3' => 'Makhzoumi Health Care', '4' => 'Makhzoumi Development'],$contact->internal_organizations,['placeholder' => 'Select Internal Organizations','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('internal_organizations') }}</span>
                    </div>
                </div>

                <!-- administrative_group -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Administrative Group </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('administrative_group', ['1' => 'Business Group' , '2' => 'Volunteer Group', '3' => 'Administration Group', '4' => 'Management Group'],$contact->administrative_group,['placeholder' => 'Select Administrative Group','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('administrative_group') }}</span>
                    </div>
                </div>

                <!-- relationship_preference -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Relationship Preference </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        {{ Form::select('relationship_preference', ['1' => 'Customers' , '2' => 'Employees', '3' => 'Lender & Investors', '4' => 'Competitors', '5' => 'Suppliers'],$contact->relationship_preference,['placeholder' => 'Select Relationship Preference','class' => 'form-control']) }}
                        <span class="text-danger">{{ $errors->first('relationship_preference') }}</span>
                    </div>
                </div>

                <!-- status -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Status  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" style="margin-top: 5px">
                        <label>
                            <?php
                            $statusOn   = $contact->status == 1?true:false;
                            ?>
                            {!! Form::radio('status', '1',$statusOn,['class' => 'flat']) !!}  Active
                        </label>
                        <label>
                            <?php
                            $statusOff  = $contact->status == 0?true:false;
                            ?>
                            {!! Form::radio('status', '0',$statusOff,['class' => 'flat']) !!}  Inactive
                        </label>
                    </div>
                </div>
            </div>
            <!-- right -->

            <!-- keywords -->
            <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                 <label class="col-md-12 col-sm-12 col-xs-12 control-label">keywords</label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('keywords',isset($contact->keywords)?$contact->keywords:'', ['class'=>'tagInput form-control tagInput', 'placeholder'=>'Type keyword']) !!}
                    <span class="text-danger">{{ $errors->first('keywords') }}</span>
                </div>
                </div>
            </div>

            <!-- notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                   <label class="col-md-12 col-sm-12 col-xs-12 control-label">Notes</label>
                     <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::textarea('notes',isset($contact->notes)?$contact->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'Notes', 'rows' => 3]) !!}
                    <span class="text-danger">{{ $errors->first('notes') }}</span>
                    </div>
                </div>
            </div>

            <!-- left -->
            <!-- created section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Created by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Created by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($contact->user_createby['first_name'])?$contact->user_createby['first_name']:'';
                        $lastName   = isset($contact->user_createby['last_name'])?$contact->user_createby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    </div>
                </div>

                <!-- Created on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Created on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($contact->created_at)?date_format($contact->created_at, 'jS M Y g:iA'):'';
                        ?>
                        {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    </div>
                </div>
            </div>
            <!-- left -->

            <!-- right -->
            <!-- updated section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Updated by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Updated by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($contact->user_updateby['first_name'])?$contact->user_updateby['first_name']:'';
                        $lastName   = isset($contact->user_updateby['last_name'])?$contact->user_updateby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    </div>
                </div>

                <!-- Updated on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Updated on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($contact->updated_at)?date_format($contact->updated_at, 'jS M Y g:iA'):'';
                        ?>
                        {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>
            <!-- right -->

            <!-- submit section -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit','name'=>'submit_add_contact')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>