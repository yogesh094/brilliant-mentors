<div role="tabpanel" class="tab-pane fade in" id="politicalPositions" >
    <div class="x_title">{{-- \Session::get('submit_political_positions')? 'active fade in':'' --}}
        <h4>Political Positions </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#politicalPositions') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                </div>
            <!-- if there are creation errors, they will show here -->
            @endif
            <span class="errormessage"></span>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
                {!! Form::open(array('url' => ('contact/political-positions-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

                    {!! Form::hidden('id',isset($contact->id)?$contact->id:'') !!}

                    {!! Form::hidden('contact_id',isset($politicalPositionContact->contact_id)?$politicalPositionContact->contact_id:'') !!}

                    <!-- left -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <!-- Political Position type -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Position Type </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <?php
                                $positionArr      = ['1' => 'Secretary of Commerce', '2'=>'Deputy Secretary', '3'=>'General Counsel'];
                                $positionText       = "";
                                if(isset($politicalPositionContact->type)) {
                                    $positionText   = (isset($positionArr[$politicalPositionContact->type]))?$positionArr[$politicalPositionContact->type]:"";    
                                }
                                ?> 
                                {!! Form::text('political_posotion_type','',['class'=>'form-control', 'placeholder'=>$positionText,'readonly' => 'readonly']) !!}
                                <span class="text-danger">{{ $errors->first('political_posotion_type') }}</span>
                            </div>
                        </div>

                        <!-- Description -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php 
                                $description     = isset($politicalPositionContact->description)?$politicalPositionContact->description:'';
                                ?>
                                {!! Form::text('political_posotion_description','',['class'=>'form-control', 'placeholder'=>$description,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('political_posotion_description') }}</span>
                            </div>
                        </div>

                        <!-- Political Party -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Party </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $politicalPosiArr      = ['1' => 'AAP' , '2' => 'BJP', '3' => 'Congress', '4' => 'BASPA'];
                                $politicalPosiText       = "";
                                if(isset($politicalPositionContact->political_party)) {
                                    $politicalPosiText   = (isset($politicalPosiArr[$politicalPositionContact->political_party]))?$politicalPosiArr[$politicalPositionContact->political_party]:"";    
                                }
                                ?> 
                                {!! Form::text('political_posotion_party','',['class'=>'form-control', 'placeholder'=>$politicalPosiText,'readonly' => 'readonly']) !!}
                                <span class="text-danger">{{ $errors->first('political_posotion_party') }}</span>
                            </div>
                        </div>

                        <!-- Exit Date -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Exit Date</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php 
                                $endAt   = isset($politicalPositionContact->end_at)?date('m-d-Y',strtotime($politicalPositionContact->end_at)):'';
                                ?>
                                {!! Form::text('political_posotion_exit_date','',['class'=>'form-control', 'placeholder'=>$endAt,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('political_posotion_exit_date') }}</span>
                            </div>
                        </div>

                        <!-- Keywords -->
                        <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label"> Keywords  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php 
                                $keyword     = isset($politicalPositionContact->keywords)?$politicalPositionContact->keywords:'';
                                ?>
                                {!! Form::textarea('political_posotion_keywords','',['class'=>'form-control', 'placeholder'=>$keyword,'readonly' => 'readonly','rows'=>2]) !!}

                                <span class="text-danger">{{ $errors->first('political_posotion_keywords') }}</span>
                            </div>
                        </div> -->
                    </div>

                    <!-- right -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <!-- Political Position Held -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Position Held </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $postalHeadArr      = ['1'=>'Economic positions','2'=>'Social issues','3'=>'Legal issues','4'=>'Foreign policy positions',];
                                $postalText       = "";
                                if(isset($politicalPositionContact->political_posotion)) {
                                    $postalText   = (isset($postalHeadArr[$politicalPositionContact->political_posotion]))?$postalHeadArr[$politicalPositionContact->political_posotion]:"";    
                                }
                                ?> 
                                {!! Form::text('political_posotion_held','',['class'=>'form-control', 'placeholder'=>$postalText,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('political_posotion_held') }}</span>
                            </div>
                        </div>

                        <!-- Greeting -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Greeting</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $greetingArr      = ['1'=>'Test Greeting msg1','2'=>'Test Greeting msg2','3'=>'Test Greeting msg3','4'=>'Test Greeting msg4'];
                                $greetingText       = "";
                                if(isset($politicalPositionContact->greeting)) {
                                    $greetingText   = (isset($greetingArr[$politicalPositionContact->greeting]))?$greetingArr[$politicalPositionContact->greeting]:"";    
                                }
                                ?> 
                                {!! Form::text('political_posotion_greeting','',['class'=>'form-control', 'placeholder'=>$greetingText,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('political_posotion_greeting') }}</span>
                            </div>
                        </div>

                        <!-- Start Date -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php 
                                $startAt   = isset($politicalPositionContact->start_at)?date('m-d-Y',strtotime($businessDetail->start_at)):'';
                                ?>
                                {!! Form::text('political_posotion_start_date','',['class'=>'form-control', 'placeholder'=>$startAt,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('political_posotion_start_date') }}</span>
                            </div>
                        </div>

                        <!-- Notes -->
                        <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php 
                                $notes     = isset($politicalPositionContact->notes)?$politicalPositionContact->notes:'';
                                ?>
                                {!! Form::textarea('political_posotion_notes','',['class'=>'form-control', 'placeholder'=>$notes,'readonly' => 'readonly','rows'=>2]) !!}

                                <span class="text-danger">{{ $errors->first('political_posotion_notes') }}</span>
                            </div>
                        </div> -->

                    </div>

                    <!-- keyword and notes -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <!-- Keywords -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                {!! Form::text('keywords',isset($politicalPositionContact->keywords)?$politicalPositionContact->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                            </div>
                        </div>
                        
                        <!-- Notes -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <?php 
                                $notes     = isset($politicalPositionContact->notes)?$politicalPositionContact->notes:'';
                                ?>
                                {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                            </div>
                        </div>
                    </div>

                    <!-- left -->
                    <!-- created section -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <!-- Created by -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Created by  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" >
                                <?php 
                                $firstName  = isset($politicalPositionContact->user_createby['first_name'])?$politicalPositionContact->user_createby['first_name']:'';
                                $lastName   = isset($politicalPositionContact->user_createby['last_name'])?$politicalPositionContact->user_createby['last_name']:'';
                                $createdBy  = $firstName.' '.$lastName;
                                ?>
                                {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                                
                            </div>
                        </div>

                        <!-- Created on -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Created on  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" >
                                <?php 
                                $createdAt   = isset($politicalPositionContact->created_at)?date_format($politicalPositionContact->created_at, 'j M Y g:i A'):'';
                                ?>

                                {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                                
                            </div>
                        </div>
                    </div>

                    <!-- right -->
                    <!-- updated section -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <!-- Updated by -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Updated by  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" >
                                <?php 
                                $firstName  = isset($politicalPositionContact->user_updateby['first_name'])?$politicalPositionContact->user_updateby['first_name']:'';
                                $lastName   = isset($politicalPositionContact->user_updateby['last_name'])?$politicalPositionContact->user_updateby['last_name']:'';
                                $createdBy  = $firstName.' '.$lastName;
                                ?>
                                {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                                
                            </div>
                        </div>

                        <!-- Updated on -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Updated on  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" >
                                <?php 
                                $createdAt   = isset($politicalPositionContact->updated_at)?date_format($politicalPositionContact->updated_at, 'j M Y g:i A'):'';
                                ?>
                                {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                                
                            </div>
                        </div>
                    </div>

                    <!-- submit section -->
                    <?php
                    $i = 0;
                    if($i == 1) {
                    ?>
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                                {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_political_positions')) !!}
                                <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>