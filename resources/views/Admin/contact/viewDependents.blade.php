<div role="tabpanel" class="tab-pane fade" id="dependents" >
    {{-- \Session::get('submit_dependents')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Dependents </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#dependents') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <!-- if there are creation errors, they will show here -->
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/dependents-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($contact->id)?$contact->id:'') !!}   

            {!! Form::hidden('contact_id', isset($dependentsContact->contact_id)?$dependentsContact->contact_id:'') !!}

            <!-- left -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- First Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name  </label>

                    <!-- title -->
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <?php 
                        $titleArr     = ['1' => 'Mr.' , '2' => 'Mrs.' , '3' => 'Miss.'];
                        $titleText    = "";
                        if(isset($dependentsContact->title)) {
                            $titleText    = (isset($titleArr[$dependentsContact->title]))?$titleArr[$dependentsContact->title]:"";    
                        }
                        ?> 
                        {!! Form::text('dependents_title','',['class'=>'form-control', 'placeholder'=>$titleText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_title') }}</span>
                    </div>

                    <!-- First Name -->
                    <div class="col-md-6 col-sm-6 col-xs-9">
                        <?php
                        $firstArr     = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => 'Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'];
                        $firstText    = "";
                        if(isset($dependentsContact->first_name)) {
                            $firstText    = (isset($firstArr[$dependentsContact->first_name]))?$firstArr[$dependentsContact->first_name]:"";    
                        }
                        ?> 
                        {!! Form::text('dependents_first_name','',['class'=>'form-control', 'placeholder'=>$firstText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_first_name') }}</span>
                    </div>
                </div>

                <!-- suffix -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Suffix  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $suffixArr      = ['1' => 'CLU' , '2' => 'D.D.S.', '3' => 'D.V.M.', '4' => 'Esq.', '5' => 'LL.D.', '6' => 'M.D.', '7' => 'O.S.B.', '8' => 'Ph.D.', '9' => 'R.G.S', '10' => 'S.H.C.J.'];

                        $suffixStr      = "";
                        if(isset($dependentsContact->suffix)) {

                            if($dependentsContact->suffix != "") {
                                foreach (explode(',',$dependentsContact->suffix) as $key => $suffix) {
                                    $suffixStr   .= $suffixArr[$suffix].", ";
                                }   
                                $suffixStr   = ($suffixStr != "")?rtrim($suffixStr," ,"):"";
                            }
                        }
                        ?>

                        {!! Form::textarea('dependents_suffix','',['class'=>'form-control', 'placeholder'=>$suffixStr,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('dependents_suffix') }}</span>
                    </div>
                </div>

                <!-- Father Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Father Name</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $fatherArr     = ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '12' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'];
                        $fatherText    = "";
                        if(isset($dependentsContact->father_name)) {
                            $fatherText    = (isset($fatherArr[$dependentsContact->father_name]))?$fatherArr[$dependentsContact->father_name]:"";    
                        }
                        ?> 
                        {!! Form::text('dependents_father_name','',['class'=>'form-control', 'placeholder'=>$fatherText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_father_name') }}</span>
                    </div>
                </div>

                <!-- Mother Name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Mother Name</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $motherArr     = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ];
                        $motherText    = "";
                        if(isset($dependentsContact->mother_name)) {
                            $motherText    = (isset($motherArr[$dependentsContact->mother_name]))?$motherArr[$dependentsContact->mother_name]:"";    
                        }
                        ?> 
                        {!! Form::text('dependents_mother_name','',['class'=>'form-control', 'placeholder'=>$motherText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_mother_name') }}</span>
                    </div>
                </div>

                <!-- Surname -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Surname  </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $surnameArr     = ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'];
                        $surnameText    = "";
                        if(isset($dependentsContact->surname)) {
                            $surnameText    = (isset($surnameArr[$dependentsContact->surname]))?$surnameArr[$dependentsContact->surname]:"";    
                        }
                        ?> 
                        {!! Form::text('dependents_surname','',['class'=>'form-control', 'placeholder'=>$surnameText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_surname') }}</span>
                    </div>
                </div>

                <!-- DOB -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <?php 
                        $dob     = isset($dependentsContact->date_of_birth) ?date("m-d-Y", strtotime($dependentsContact->date_of_birth)):'';
                        ?>
                        {!! Form::text('dependents_date_of_birth','',['class'=>'form-control', 'placeholder'=>$dob,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_date_of_birth') }}</span>
                    </div>
                </div>

                <!-- blood type -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Type</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $bloodTypeText    = "";
                        if(isset($dependentsContact->blood_type)) {
                            $bloodTypeText    = (isset($bloodType[$dependentsContact->blood_type]))?$bloodType[$dependentsContact->blood_type]:"";    
                        }
                        ?> 
                        {!! Form::text('dependents_blood_type','',['class'=>'form-control', 'placeholder'=>$bloodTypeText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_blood_type') }}</span>
                    </div>
                </div>

                <!-- No of Dependents -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    No Of Dependents</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $numofdep     = isset($dependentsContact->number_of_dependents)?$dependentsContact->number_of_dependents:'';
                        ?>
                        {!! Form::text('dependents_number_of_dependents','',['class'=>'form-control', 'placeholder'=>$numofdep,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_number_of_dependents') }}</span>
                    </div>
                </div>

                <!-- languages -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Languages</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $langStr      = "";
                        if(isset($dependentsContact->languages)) {

                            if($dependentsContact->languages != "") {
                                foreach (explode(',',$dependentsContact->languages) as $key => $lang) {
                                    $langStr   .= $languages[$lang].", ";
                                }   
                                $langStr   = ($langStr != "")?rtrim($langStr," ,"):"";
                            }
                        }
                        ?>

                        {!! Form::textarea('dependents_languages[]','',['class'=>'form-control', 'placeholder'=>$langStr,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('dependents_languages') }}</span>
                    </div>
                </div>

                <!-- importance -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $imporArr       = ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
                        $imporText    = "";
                        if(isset($dependentsContact->importance)) {
                            $imporText    = (isset($imporArr[$dependentsContact->importance]))?$imporArr[$dependentsContact->importance]:"";    
                        }
                        ?> 
                        {!! Form::text('dependents_importance','',['class'=>'form-control', 'placeholder'=>$imporText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_importance') }}</span>
                    </div>
                </div>

                <!-- sensitivity -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $senseArr       = [1=>'sensitivity text1',2=>'sensitivity text2',3=>'sensitivity text3'];
                        $senseText    = "";
                        if(isset($dependentsContact->sensitivity)) {
                            $senseText    = (isset($senseArr[$dependentsContact->sensitivity]))?$senseArr[$dependentsContact->sensitivity]:"";    
                        }
                        ?> 
                        {!! Form::text('dependents_sensitivity','',['class'=>'form-control', 'placeholder'=>$senseText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_sensitivity') }}</span>
                    </div>
                </div>

                <!-- Religion -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Religion </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $religionArr       = ['1' => 'Christian', '2' => 'Muslim'];
                        $senseText    = "";
                        if(isset($dependentsContact->religion_id)) {
                            $senseText    = (isset($religionArr[$dependentsContact->religion_id]))?$religionArr[$dependentsContact->religion_id]:"";    
                        }
                        ?> 
                        {!! Form::text('dependents_religion','',['class'=>'form-control', 'placeholder'=>$senseText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_religion') }}</span>
                    </div>
                </div>

                <!-- Administrative Group -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Administrative Group </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $adminGpArr     =  ['1' => 'Business Group' , '2' => 'Volunteer Group', '3' => 'Administration Group', '4' => 'Management Group'];
                        $adminText      = "";
                        if(isset($dependentsContact->administrative_group)) {

                            if($dependentsContact->administrative_group != "") {
                                foreach (explode(',',$dependentsContact->administrative_group) as $key => $lang) {
                                    $adminText   .= $adminGpArr[$lang].", ";
                                }   
                                $adminText   = ($adminText != "")?rtrim($adminText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('dependents_administrative_group[]','',['class'=>'form-control', 'placeholder'=>$adminText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('dependents_administrative_group') }}</span>
                    </div>
                </div>

                <!-- Notes -->
                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $notes     = isset($dependentsContact->notes)?$dependentsContact->notes:'';
                        ?>
                        {!! Form::textarea('dependents_notes','',['class'=>'form-control', 'placeholder'=>$notes,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('dependents_notes') }}</span>
                    </div>
                </div> -->
            </div>

            <!-- right -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                
                <!-- photo box -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-right">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3 my-photo-box-view">
                        <div style="text-align: left;float:none;">
                            <?php
                            if(isset($dependentsContact->photo) && $dependentsContact->photo != "") {
                            ?>
                                <img src="{{ $imageUrl.$dependentsContact->photo }}" class="contact_spouse_photo img-thumbnail" >
                            <?php
                            } else {
                            ?>
                                <img src="{{ url('/uploads/contact/avatar.png') }}" class="contact_spouse_photo img-thumbnail" >
                            <?php 
                            }
                            ?>
                            <input type="hidden" name="old_contact_dependents_image" value="{{ isset($dependentsContact->photo)?$dependentsContact->photo:'' }}">
                        </div>
                    </div>
                </div>

                <!-- nick name -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $nickname       = isset($dependentsContact->nickname)?$dependentsContact->nickname:'';
                        ?>
                        {!! Form::text('dependents_nick_name','',['class'=>'form-control', 'placeholder'=>$nickname,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_nick_name') }}</span>
                    </div>
                </div>

                <!-- birth place -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Birth Place</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $birthplaceText          = "";
                        if(isset($dependentsContact->birth_place)) {
                            $birthplaceText   = (isset($nationality[$dependentsContact->birth_place]))?$nationality[$dependentsContact->birth_place]:"";
                        }
                        ?> 
                        {!! Form::text('dependents_birth_place','',['class'=>'form-control', 'placeholder'=>$birthplaceText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_birth_place') }}</span>
                    </div>
                </div>

                <!-- gender -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Gender</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $genderText          = "";
                        if(isset($dependentsContact->gender)) {
                            $genderText   = (isset($gender[$dependentsContact->gender]))?$gender[$dependentsContact->gender]:"";
                        }
                        ?> 
                        {!! Form::text('dependents_gender','',['class'=>'form-control', 'placeholder'=>$genderText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_gender') }}</span>
                    </div>
                </div>

                <!-- nationality -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nationality</label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $nationText      = "";
                        if(isset($dependentsContact->nationality)) {

                            if($dependentsContact->nationality != "") {
                                foreach (explode(',',$dependentsContact->nationality) as $key => $nation) {
                                    $nationText   .= $nationality[$nation].", ";
                                }   
                                $nationText   = ($nationText != "")?rtrim($nationText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('dependents_nationality[]','',['class'=>'form-control', 'placeholder'=>$nationText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('dependents_nationality') }}</span>
                    </div>
                </div>

                <!-- profession -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Profession </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $profArr        = ['1' => 'Profession 1' , '2' => 'Profession 2' , '3' => 'Profession 3', '4' => 'Profession 4', '5' => 'Profession 5'];
                        $profText     = "";
                        if(isset($dependentsContact->profession)) {

                            if($dependentsContact->profession != "") {
                                foreach (explode(',',$dependentsContact->profession) as $key => $prof) {
                                    $profText   .= isset($profArr[$prof])?$profArr[$prof].", ":"";
                                }   
                                $profText   = ($profText != "")?rtrim($profText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('dependents_profession[]','',['class'=>'form-control', 'placeholder'=>$profText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('dependents_profession') }}</span>
                    </div>
                </div>

                <!-- caution -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $causionArr         = ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'];           
                        $causionText        = "";
                        if(isset($dependentsContact->caution)) {
                            $causionText   = (isset($causionArr[$dependentsContact->caution]))?$causionArr[$dependentsContact->caution]:"";
                        }
                        ?> 
                        {!! Form::text('dependents_caution','',['class'=>'form-control', 'placeholder'=>$causionText,'readonly' => 'readonly']) !!}

                        <span class="text-danger">{{ $errors->first('dependents_caution') }}</span>
                    </div>
                </div>

                <!-- Internal Organizations -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Internal Organizations </label>

                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php
                        $intOrgArr        = ['1' => 'Makhzoumi Foundation' , '2' => 'Beirutiyat', '3' => 'Makhzoumi Health Care', '4' => 'Makhzoumi Development'];
                        $intOrgText     = "";
                        if(isset($dependentsContact->internal_organizations)) {

                            if($dependentsContact->internal_organizations != "") {
                                foreach (explode(',',$dependentsContact->internal_organizations) as $key => $rel) {
                                    $intOrgText   .= $intOrgArr[$rel].", ";
                                }   
                                $intOrgText   = ($intOrgText != "")?rtrim($intOrgText," ,"):"";
                            }
                        }
                        ?> 
                        {!! Form::textarea('dependents_internal_organizations[]','',['class'=>'form-control', 'placeholder'=>$intOrgText,'readonly' => 'readonly','rows' => 2]) !!}

                        <span class="text-danger">{{ $errors->first('dependents_internal_organizations') }}</span>
                    </div>
                </div>

                <!-- Status -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Status  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <label>
                            <?php
                                $statusYes = $statusNo = false; 
                                if(isset($dependentsContact->status)) {
                                    $statusYes = ($dependentsContact->status == 1?true:false);
                                    $statusNo  = ($dependentsContact->status == 0?true:false);
                                }
                            ?>
                            {!! Form::radio('dependents_status', '1', $statusYes, ['class' => 'flat','disabled' => 'disabled' ]) !!}  Active
                        </label>
                        <label>
                            {!! Form::radio('dependents_status', '0', $statusNo, ['class' => 'flat','disabled' => 'disabled']) !!}  Inactive
                        </label>
                    </div>
                </div>

                <!-- Keywords -->
                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label"> Keywords  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php 
                        $keywordText      = isset($dependentsContact->notes)?$dependentsContact->notes:'';
                        ?>
                        {!! Form::textarea('dependents_keywords','',['class'=>'form-control', 'placeholder'=>$keywordText,'readonly' => 'readonly','rows'=>2]) !!}

                        <span class="text-danger">{{ $errors->first('dependents_keywords') }}</span>
                    </div>
                </div> -->
            </div>

            <!-- keyword and notes -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <!-- Keywords -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        {!! Form::text('keywords',isset($dependentsContact->keywords)?$dependentsContact->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                    </div>
                </div>
                
                <!-- Notes -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php 
                        $notes     = isset($dependentsContact->notes)?$dependentsContact->notes:'';
                        ?>
                        {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                    </div>
                </div>
            </div>

            <!-- left -->
            <!-- created section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Created by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($dependentsContact->user_createby['first_name'])?$dependentsContact->user_createby['first_name']:'';
                        $lastName   = isset($dependentsContact->user_createby['last_name'])?$dependentsContact->user_createby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Created on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Created on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($dependentsContact->created_at)?date_format($dependentsContact->created_at, 'j M Y g:i A'):'';
                        ?>

                        {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- right -->
            <!-- updated section -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <!-- Updated by -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated by  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $firstName  = isset($dependentsContact->user_updateby['first_name'])?$dependentsContact->user_updateby['first_name']:'';
                        $lastName   = isset($dependentsContact->user_updateby['last_name'])?$dependentsContact->user_updateby['last_name']:'';
                        $createdBy  = $firstName.' '.$lastName;
                        ?>
                        {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>

                <!-- Updated on -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                    Updated on  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12" >
                        <?php 
                        $createdAt   = isset($dependentsContact->updated_at)?date_format($dependentsContact->updated_at, 'j M Y g:i A'):'';
                        ?>
                        {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                        
                    </div>
                </div>
            </div>

            <!-- submit section -->
            <?php 
            $i = 0;
            if($i == 1) {
            ?>
                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_dependents')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                    </div>
                </div>
            <?php 
            }
            ?>
        {!! Form::close() !!}
        </div>
    </div>
</div>