<div role="tabpanel" class="tab-pane fade" id="communication" >
    {!! Form::open(array('url' => ('contact/communication-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}
    {{-- \Session::get('submit_communication')? 'active fade in':'' --}}

        {!! Form::hidden('id',isset($communicationContact->id)?$communicationContact->id:"") !!}
        {!! Form::hidden('contact_id', $contact->id) !!}

        {!! Form::hidden('contact_political_position_id', isset($politicalPositionContact->id)?$politicalPositionContact->id:'') !!}
        {!! Form::hidden('contact_dependents_id', isset($dependentsContact->id)?$dependentsContact->id:'') !!}
        {!! Form::hidden('contact_spouse_id', isset($spouseContact->id)?$spouseContact->id:'') !!}
        {!! Form::hidden('contact_referral_id', isset($referralContact->id)?$referralContact->id:'') !!}
        {!! Form::hidden('contact_representative_id', isset($representativeContact->id)?$representativeContact->id:'') !!}
        
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            @if (Session::has('contactNumberSuccess'))
                <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('contactNumberSuccess') !!}</div>
            @endif

            <div class="alert alert-danger contact-communication-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
            </div>

            @if(count($errors))
                <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                </div>
            @endif
            <span class="errormessage"></span>
        </div>

        <div class="communication-multiple">
            <div class="x_title">
                <h4>Communication</h4>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class=" box-primary">

                    <!-- left -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">

                        <!-- Email -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::text('communication_email',isset($communicationContact->email)?$communicationContact->email:'',['id' => 'communicationEmail','class' => 'form-control']) }}
                                <span class="text-danger communicationEmailError">{{ $errors->first('communication_email') }}</span>
                            </div>
                        </div>

                        <!-- Email Type -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Type</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('communication_email_type', ['1' => 'Business','2'=>'Private'], isset($communicationContact->email_type) ? $communicationContact->email_type:'', ['placeholder' => 'Select Email Type','class' => 'form-control','id' =>'communicationEmailType']) }}
                                <span class="text-danger communicationEmailTypeError">{{ $errors->first('communication_email_type') }}</span>
                            </div>
                        </div>

                        <!-- Category -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('communication_category', ['1' => 'Communication Category1' , '2' => 'Communication Category2', '3' => 'Communication Category3','4' => 'Communication Category4'] ,isset($communicationContact->category)?$communicationContact->category:'', ['placeholder' => 'Select Category','class' => 'form-control','id' => 'communicationCategory']) }}
                                <span class="text-danger">{{ $errors->first('communication_category') }}</span>
                            </div>
                        </div>

                        <!-- Email CC -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email CC</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('communication_email_cc',isset($communicationContact->email_cc)?$communicationContact->email_cc:'',['class'=>'form-control', 'placeholder'=>'Email CC','id'=>'communicationEmailCC']) !!}
                                <span class="text-danger communicationEmailCCError">{{ $errors->first('communication_email_cc') }}</span>
                            </div>
                        </div>

                        <!-- Email BCC -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email BCC</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('communication_email_bcc',isset($communicationContact->email_bcc)?$communicationContact->email_bcc:'',['class'=>'form-control', 'placeholder'=>'Email BCC','id'=>'communicationEmailBCC']) !!}
                                <span class="text-danger communicationEmailBCCError">{{ $errors->first('communication_email_bcc') }}</span>
                            </div>
                        </div>

                        <!-- Skype -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Skype</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('communication_skype',isset($communicationContact->skype)?$communicationContact->skype:'',['class'=>'form-control', 'placeholder'=>'Skype']) !!}
                                <span class="text-danger">{{ $errors->first('communication_skype') }}</span>
                            </div>
                        </div>
                    </div>

                    <!-- right -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">

                        <!-- Facebook -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Facebook</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('communication_facebook',isset($communicationContact->facebook)?$communicationContact->facebook:'',['class'=>'form-control', 'placeholder'=>'Facebook']) !!}
                                <span class="text-danger">{{ $errors->first('communication_facebook') }}</span>
                            </div>
                        </div>

                        <!-- Twitter -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Twitter</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('communication_twitter',isset($communicationContact->twitter)?$communicationContact->twitter:'',['class'=>'form-control', 'placeholder'=>'Twitter']) !!}
                                <span class="text-danger">{{ $errors->first('communication_twitter') }}</span>
                            </div>
                        </div>

                        <!-- Instagram -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Instagram</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::text('communication_instagram',isset($communicationContact->instagram)?$communicationContact->instagram:'',['class'=>'form-control', 'placeholder'=>'Instagram']) !!}
                                <span class="text-danger">{{ $errors->first('communication_instagram') }}</span>
                            </div>
                        </div>

                        <!-- importance -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('communication_importance', ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'], isset($communicationContact->importance)?$communicationContact->importance:'', ['placeholder' => 'Select Importance','class' => 'form-control']) }}
                                <span class="text-danger">{{ $errors->first('communication_importance') }}</span>
                            </div>
                        </div>

                        <!-- caution -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('communication_caution', ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'],isset($communicationContact->caution)?$communicationContact->caution:'',['placeholder' => 'Select Caution','class' => 'form-control']) }}
                                <span class="text-danger">{{ $errors->first('communication_caution') }}</span>
                            </div>
                        </div>

                        <!-- sensitivity -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {{ Form::select('communication_sensitivity', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],isset($communicationContact->sensitivity)?$communicationContact->sensitivity:'', ['placeholder' => 'Select Sensitivity','class' => 'form-control']) }}
                                <span class="text-danger">{{ $errors->first('communication_sensitivity') }}</span>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <!-- keywords -->
        <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    keywords
                </label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('communication_keywords',isset($communicationContact->keywords)?$communicationContact->keywords:'',['id'=>'communicationKeywords','class'=>'form-control tagInput', 'placeholder'=>'Type keyword']) !!}
                    <span class="text-danger">{{ $errors->first('communication_keywords') }}</span>
                </div>
            </div>
        </div>

        <!-- notes -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    Notes
                </label>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::textarea('communication_notes', isset($communicationContact->notes)?$communicationContact->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'Notes','size' => '3x3']) !!}
                <span class="text-danger">{{ $errors->first('communication_notes') }}</span>
                </div>
            </div>
        </div>

        <!-- left -->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($communicationContact->user_createby['first_name'])?$communicationContact->user_createby['first_name']:'';
                    $lastName   = isset($communicationContact->user_createby['last_name'])?$communicationContact->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($communicationContact->created_at)?date_format($communicationContact->created_at, 'jS M Y g:iA'):'';
                    ?>

                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- right -->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($communicationContact->user_updateby['first_name'])?$communicationContact->user_updateby['first_name']:'';
                    $lastName   = isset($communicationContact->user_updateby['last_name'])?$communicationContact->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($communicationContact->updated_at)?date_format($communicationContact->updated_at, 'jS M Y g:iA'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- submit section -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name'=>'submit_communication')) !!}

                    {{-- {!! Form::button('Add', array('class' => 'btn btn-primary add_communucation')) !!} --}}

                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>