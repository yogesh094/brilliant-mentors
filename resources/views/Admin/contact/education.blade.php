<div role="tabpanel" class="tab-pane fade" id="education" >
    {{-- \Session::get('submit_education')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Education </h4>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">

        @if (Session::has('communicationSuccess'))
            <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('communicationSuccess') !!}</div>
        @endif

        <div class="alert alert-danger contact-education-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your input.
        </div>

        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/education-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

            {!! Form::hidden('id',isset($educationContact->id)?$educationContact->id:"") !!}
            {!! Form::hidden('contact_id', $contact->id) !!}

            {!! Form::hidden('contact_political_position_id', isset($politicalPositionContact->id)?$politicalPositionContact->id:'') !!}
            {!! Form::hidden('contact_dependents_id', isset($dependentsContact->id)?$dependentsContact->id:'') !!}
            {!! Form::hidden('contact_spouse_id', isset($spouseContact->id)?$spouseContact->id:'') !!}
            {!! Form::hidden('contact_referral_id', isset($referralContact->id)?$referralContact->id:'') !!}
            {!! Form::hidden('contact_representative_id', isset($representativeContact->id)?$representativeContact->id:'') !!}
        <!-- left -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">

            <!-- Qualification -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Qualification</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('education_qualification', ['1' => 'BE','2'=>'MCA'], isset($educationContact->qualification) ? $educationContact->qualification:'', ['placeholder' => 'Select Qualification', 'class' => 'form-control', 'id' =>'educationQualification']) }}
                    <span class="text-danger">{{ $errors->first('education_qualification') }}</span>
                </div>
            </div>

            <!-- Institute -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Institute</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('education_institute', ['1' => 'GUJ','2'=>'GTU'], isset($educationContact->institute) ? $educationContact->institute:'', ['placeholder' => 'Select Institute','class' => 'form-control','id' =>'educationInstitute']) }}
                    <span class="text-danger">{{ $errors->first('education_institute') }}</span>
                </div>
            </div>

            <!-- Achivement -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Achivement</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('education_achivement', ['1' => 'Education Achivement1' , '2' => 'Education Achivement2', '3' => 'Education Achivement3','4' => 'Education Achivement4'] ,isset($educationContact->achivement)?$educationContact->achivement:'', ['placeholder' => 'Select Achivement','class' => 'form-control','id' => 'educationAchivement']) }}
                    <span class="text-danger">{{ $errors->first('education_achivement') }}</span>
                </div>
            </div>

            <!-- sensitivity -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('education_sensitivity', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],isset($educationContact->sensitivity)?$educationContact->sensitivity:'', ['placeholder' => 'Select Sensitivity','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('education_sensitivity') }}</span>
                </div>
            </div>

            <!-- Date From -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date From <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('education_from_date',isset($educationContact->from_date)?date('m-d-Y',strtotime($educationContact->from_date)):'',['id'=>'educationFromDate','class'=>'form-control', 'placeholder'=>'From Date']) !!}
                    <span class="text-danger">{{ $errors->first('education_from_date') }}</span>
                </div>
            </div>

            <!-- Date To -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date To</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('education_end_date',isset($educationContact->end_date)?date('m-d-Y',strtotime($educationContact->end_date)):'',['id'=>'educationEndDate','class'=>'form-control', 'placeholder'=>'End Date']) !!}
                    <span class="text-danger educationEndDate">{{ $errors->first('education_end_date') }}</span>
                </div>
            </div>
        </div>

        <!-- right -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- photo box -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3 my-photo-box">
                    <div style="text-align: left;float:none;">
                        <?php
                        if(isset($educationContact->certificate) && $educationContact->certificate != "") {
                        ?>
                            <img src="{{ $imageUrl.$educationContact->certificate }}" class="img-thumbnail contact_spouse_photo education_image" >
                        <?php
                        } else {
                        ?>
                            <img src="{{ url('/uploads/contact/certificate.png') }}" class="img-thumbnail contact_spouse_photo education_image">
                        <?php
                        }
                        ?>
                        <input type="hidden" name="old_contact_education_image" value="{{ isset($educationContact->certificate)?$educationContact->certificate:'' }}">
                    </div>
                </div>
            </div>

            <!-- Certificate -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Certificate</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::File('education_certificate',['class'=>'form-control certificate', 'accept'=>'.jpeg,.jpg,.png', 'placeholder'=>'Certificate']) !!}
                    <span class="text-danger educationCertificateError">{{ $errors->first('education_certificate') }}</span>
                </div>
            </div>
        </div>

        <!-- keywords -->
        <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    keywords
                </label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('education_keywords',isset($educationContact->keywords)?$educationContact->keywords:'',['id'=>'educationKeywords','class'=>'form-control tagInput', 'placeholder'=>'Type keyword']) !!}
                    <span class="text-danger">{{ $errors->first('education_keywords') }}</span>
                </div>
            </div>
        </div>

        <!-- notes -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
               <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    Notes
                </label>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::textarea('education_notes', isset($educationContact->notes)?$educationContact->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'Notes','size' => '3x3']) !!}
                <span class="text-danger">{{ $errors->first('education_notes') }}</span>
                </div>
            </div>
        </div>

        <!-- left -->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($educationContact->user_createby['first_name'])?$educationContact->user_createby['first_name']:'';
                    $lastName   = isset($educationContact->user_createby['last_name'])?$educationContact->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($educationContact->created_at)?date_format($educationContact->created_at, 'jS M Y g:iA'):'';
                    ?>

                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- right -->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($educationContact->user_updateby['first_name'])?$educationContact->user_updateby['first_name']:'';
                    $lastName   = isset($educationContact->user_updateby['last_name'])?$educationContact->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($educationContact->updated_at)?date_format($educationContact->updated_at, 'jS M Y g:iA'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- submit section -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit','name'=>'submit_education')) !!}

                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>