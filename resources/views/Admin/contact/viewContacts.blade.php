<div role="tabpanel" class="tab-pane fade active in" id="addContact" >
    <div class="x_title">
        <h4>Contact </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#addContact') }}"> Edit <i class="fa fa-external-link"></i></a>
        <!-- <button type="button" class="btn btn-info btn-xs pull-right"  data-placement="top" data-original-title="Edit" aria-describedby="tooltip417825" data-target="#contactAddressModal" data-toggle="modal" ><i class="fa fa-pencil"></i></button> -->
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <!-- <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul> -->
                </div>
            @endif
            <!-- if there are creation errors, they will show here -->
            <span class="errormessage"></span>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
            {!! Form::open(array('url' => ('contact/store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}
            {!! Form::hidden('id',$contact->id) !!}
                <!-- //left -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <!-- private & bussiness ownwer & contact unique key-->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <!-- private -->
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Private  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php
                            $privateYes = $privateNo = false;
                            if (isset($contact->private)) {
                                $privateYes     = $contact->private == 1?true:false;

                                $privateNo      = $contact->private == 0?true:false;
                            }
                            ?>
                            <label>
                                {!! Form::radio('private', '1',$privateYes,['class' => 'flat','disabled' => 'disabled']) !!}  Yes
                            </label>
                            <label>
                                {!! Form::radio('private', '0',$privateNo,['class' => 'flat','disabled' => 'disabled']) !!}  No
                            </label>
                        </div>
                    </div>

                    <!-- Business owner -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Business owner</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php   
                            $businessOwnYes = $businessOwnNo = false;
                            if (isset($contact->business_owner)) {
                                $businessOwnYes     = $contact->business_owner == 1?true:false;
                                $businessOwnNo      = $contact->business_owner == 0?true:false;
                            }
                            ?>
                            <label>
                                {!! Form::radio('business_owner', '1',$businessOwnYes,['class' => 'flat','disabled' => 'disabled']) !!}  Yes
                            </label>

                            <label>
                                {!! Form::radio('business_owner', '0',$businessOwnNo,['class' => 'flat','disabled' => 'disabled']) !!}  No
                            </label>
                        </div>
                    </div>

                    <!-- Contact Unique Key -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact ID</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            {!! Form::text('contact_unique_key','',['class'=>'form-control', 'placeholder'=>isset($contact->contact_unique_key)?$contact->contact_unique_key:'','readonly' => 'readonly']) !!}
                            <span class="text-danger">{{ $errors->first('contact_unique_key') }}</span>
                        </div>

                        <div class="col-md-5 col-sm-5 col-xs-12">
                            {!! Form::text('system_unique_key','',['class'=>'form-control', 'placeholder'=>isset($contact->system_unique_key)?$contact->system_unique_key:'','readonly' => 'readonly']) !!}
                            <span class="text-danger">{{ $errors->first('system_unique_key') }}</span>
                        </div>

                    </div>

                    <!-- Title  -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Title </label>

                        <!-- title -->
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $titleArr      = ['1' => 'Mr.' , '2' => 'Mrs.' , '3' => 'Miss.'];
                            $titleText        = "";
                            if(isset($contact->title)) {
                                $titleText    = (isset($titleArr[$contact->title]))?$titleArr[$contact->title]:"";    
                            }
                            ?>
                            {!! Form::text('title','',['id'=>'title','class'=>'form-control', 'placeholder'=>$titleText,'readonly' => 'readonly']) !!}
                            <span class="text-danger">{{ $errors->first('title') }}</span>
                        </div>
                    </div>

                    <!-- suffix -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Suffix </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $suffixArr      = ['1' => 'CLU' , '2' => 'D.D.S.', '3' => 'D.V.M.', '4' => 'Esq.', '5' => 'LL.D.', '6' => 'M.D.', '7' => 'O.S.B.', '8' => 'Ph.D.', '9' => 'R.G.S', '10' => 'S.H.C.J.'];

                            $suffixStr      = "";
                            if($contact->suffix != "") {
                                foreach (explode(',',$contact->suffix) as $key => $suffix) {
                                    $suffixStr   .= $suffixArr[$suffix].", ";
                                }    
                                $suffixStr   = rtrim($suffixStr," ,");
                            }
                            ?>
                            {!! Form::textarea('suffix','',['id'=>'suffix','class'=>'form-control', 'placeholder'=>$suffixStr,'readonly' => 'readonly','rows'=>2]) !!}

                            <span class="text-danger">{{ $errors->first('suffix') }}</span>
                            
                        </div>
                    </div>

                    <!-- First Name -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $firstArr     = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => 'Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'];
                            $firstNameText    = "";
                            if(isset($contact->first_name)) {
                                $firstNameText    = (isset($firstArr[$contact->first_name]))?$firstArr[$contact->first_name]:"";    
                            }

                            ?>
                            {!! Form::text('first_name','',['id'=>'first_name','class'=>'form-control', 'placeholder'=>$firstNameText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                        </div>
                    </div>

                    <!-- Father Name -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Father Name </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $FatherNameArr      = ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '6' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'];

                            $fatherNameText     = "";
                            if(isset($contact->father_name)) {
                                $fatherNameText = (isset($FatherNameArr[$contact->father_name]))?$FatherNameArr[$contact->father_name]:""; 
                            }
                            ?>
                            {!! Form::text('father_name','',['id'=>'father_name','class'=>'form-control', 'placeholder'=>$fatherNameText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('father_name') }}</span>
                        </div>
                    </div>

                    <!-- Mother Name -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mother Name </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $motherNameArr      = ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ];

                            $motherNameText     = "";
                            if(isset($contact->mother_name)) {
                                $motherNameText   = (isset($motherNameArr[$contact->mother_name]))?$motherNameArr[$contact->mother_name]:"";    
                            }

                            ?>
                            {!! Form::text('mother_name','',['id'=>'mother_name','class'=>'form-control', 'placeholder'=>$motherNameText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('mother_name') }}</span>
                        </div>
                    </div>

                    <!-- Surname -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Surname </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $surnameArr      = ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'];

                            $surnameText     = "";
                            if(isset($contact->surname)) {
                                $surnameText   = (isset($surnameArr[$contact->surname]))?$surnameArr[$contact->surname]:"";    
                            }

                            ?>
                            {!! Form::text('surname','',['id'=>'surname','class'=>'form-control', 'placeholder'=>$surnameText,'readonly' => 'readonly']) !!}

                            
                            <span class="text-danger">{{ $errors->first('surname') }}</span>
                        </div>
                    </div>

                    <!-- DOB -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $dobText        = isset($contact->date_of_birth)?date("m-d-Y",strtotime($contact->date_of_birth)):'';
                            ?>

                            {!! Form::text('date_of_birth','',['id'=>'','class'=>'form-control', 'placeholder'=>$dobText,'readonly' => 'readonly']) !!}
                            
                            <span class="text-danger">{{ $errors->first('date_of_birth') }}</span>

                        </div>
                    </div>

                    <!-- blood type -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Type  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                            $bloodTypeText      = "";    
                            if(isset($contact->blood_type)) {
                                $bloodTypeText  = isset($bloodType[$contact->blood_type])?$bloodType[$contact->blood_type]:"";
                            }
                            ?>

                            {!! Form::text('blood_type','',['id'=>'blood_type','class'=>'form-control', 'placeholder'=> $bloodTypeText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('blood_type') }}</span>
                        </div>
                    </div>

                    <!-- nationality -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nationality  </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            
                            $nationalityStr     = "";
                            if($contact->nationality != "") {
                                    foreach (explode(',', $contact->nationality) as $key => $nation) {
                                        $nationalityStr     .= isset($nationality[$nation])?$nationality[$nation].", ":"";
                                    }    
                                    $nationalityStr   = rtrim($nationalityStr," ,");
                                }
                            ?>

                            {!! Form::textarea('nationality[]','',['id'=>'nationality','class'=>'form-control', 'placeholder'=>"$nationalityStr",'readonly' => 'readonly','rows'=>2]) !!}

                            <span class="text-danger">{{ $errors->first('nationality') }}</span>
                        </div>
                    </div>

                    <!-- languages -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Languages  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                                $selectedLang       = "";
                                if($contact->languages != ""){
                                    foreach (explode(',',$contact->languages) as $key => $lang) {
                                        $selectedLang   .= $languages[$lang].", ";
                                    }    
                                    $selectedLang   = rtrim($selectedLang," ,");
                                }
                            ?>
                            {!! Form::textarea('languages[]','',['id'=>'languages','class'=>'form-control', 'placeholder'=>"$selectedLang",'readonly' => 'readonly','rows'=>2]) !!}

                            <span class="text-danger">{{ $errors->first('languages') }}</span>
                        </div>
                    </div>

                    <!-- sponsorships -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Sponsorships </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                            $sponsorshipsArr    = ['1' => 'JPMorgan' , '2' => 'Microsoft', '3' => 'Deloitte', '4' => 'PepsiCo', '5' => 'DHL Express'];
                            $sponsorshipsText   = isset($sponsorshipsArr[$contact->sponsorships])?$sponsorshipsArr[$contact->sponsorships]:"";
                            ?>
                            {!! Form::text('sponsorships','',['id'=>'sponsorships','class'=>'form-control', 'placeholder'=>$sponsorshipsText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('sponsorships') }}</span>
                        </div>
                    </div>

                    <!-- political_party -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Party </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                            $AdministrativeGpArr    = ['1' => 'Movement of the future' , '2' => 'Kataeb Party', '3' => 'Hezbollah', '4' => 'Free Patriotic Movement'];
                            $adminstativeGpText     = isset($AdministrativeGpArr[$contact->political_party])?$AdministrativeGpArr[$contact->political_party]:"";
                            ?>
                            {!! Form::text('political_party','',['id'=>'political_party','class'=>'form-control', 'placeholder'=>$adminstativeGpText,'readonly' => 'readonly']) !!}
                            <span class="text-danger">{{ $errors->first('political_party') }}</span>
                        </div>
                    </div>

                    <!-- charitable_organizations -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Charitable Organizations </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                            $charityArr     = ['1' => 'YSR FOUNDATION ' , '2' => 'THE COCONADA CHAMBER OF COMMERCE', '3' => 'RAVINDRANATH MEDICAL FOUNDATION'];
                            $charityText    = isset($charityArr[$contact->charitable_organizations])?$charityArr[$contact->charitable_organizations]:"";
                            ?>
                            {!! Form::text('charitable_organizations','',['id'=>'charitable_organizations','class'=>'form-control', 'placeholder'=>$charityText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('charitable_organizations') }}</span>
                        </div>
                    </div> 
                </div>

                <!-- right -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <!-- photo box -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        
                         <div class="col-md-5 col-sm-5 col-xs-12 col-md-offset-3 col-sm-offset-3 my-photo-box-view">
                            <div style="text-align: left;float:none;">
                                <?php
                                if(isset($contact->photo) && $contact->photo != "") {
                                ?>
                                    <img src="{{ $imageUrl.$contact->photo }}" class="contact_photo img-thumbnail" >
                                <?php
                                } else {
                                ?>
                                    <img src="{{ url('/uploads/contact/avatar.png') }}" class="contact_photo img-thumbnail" >
                                <?php 
                                }
                                ?>
                                <input type="hidden" name="old_contact_image" value="{{ isset($contact->photo)?$contact->photo:'' }}">
                            </div>
                        </div>
                         <div class="col-md-4 col-sm-4 col-xs-12">
                            <div style="float:right;">
                                <?php
                                if(isset($contact->qr_code) && $contact->qr_code != "") {
                                ?>

                                    <img src="http://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{ $contact->qr_code }}" class="img-responsive" >
                                <?php 
                                }
                                ?>
                                <input type="hidden" name="old_contact_image" value="{{ isset($contact->photo)?$contact->photo:'' }}">
                            </div>
                        </div>
                    </div>

                    <!-- nick name -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name  </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {!! Form::text('nick_name','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$contact->nick_name,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('nick_name') }}</span>
                        </div>
                    </div>

                    <!-- birth place -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Birth Place </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $birthPlace     = isset($nationality[$contact->birth_place])?$nationality[$contact->birth_place]:"";
                            ?>
                            {!! Form::text('birth_place','',['id'=>'birth_place','class'=>'form-control', 'placeholder'=>$birthPlace,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('birth_place') }}</span>
                        </div>
                    </div>

                    <!-- gender -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Gender  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                            $genderStr  = isset($gender[$contact->gender])?$gender[$contact->gender]:""
                            ?>
                            {!! Form::text('gender','',['id'=>'gender','class'=>'form-control', 'placeholder'=>$genderStr,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('gender') }}</span>
                        </div>
                    </div>

                    <!-- marital_status -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Marital Status  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $maritalStr     = isset($maritalStatus[$contact->marital_status])?$maritalStatus[$contact->marital_status]:"";
                            ?>
                            {!! Form::text('marital_status','',['id'=>'marital_status','class'=>'form-control', 'placeholder'=>$maritalStr,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('marital_status') }}</span>

                        </div>
                    </div>

                    <!-- religion -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Religion </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $religionArr    = ['1' => 'Christian', '2' => 'Muslim'];
                            $regionText     = isset($religionArr[$contact->religion])?$religionArr[$contact->religion]:"";
                            ?>
                            {!! Form::text('religion','',['id'=>'religion','class'=>'form-control', 'placeholder'=>$regionText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('religion') }}</span>
                        </div>
                    </div>

                    <!-- profession -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Profession </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $professionArr  = ['1' => 'Profession 1' , '2' => 'Profession 2' , '3' => 'Profession 3', '4' => 'Profession 4', '5' => 'Profession 5'];
                            $professionStr      = "";
                            if($contact->profession != "") {
                                    foreach (explode(',', $contact->profession) as $key => $prof) {
                                        $professionStr      .= isset($professionArr[$prof])?$professionArr[$prof].", ":"";
                                    }    
                                    $professionStr   = rtrim($professionStr," ,");
                                }
                            ?>
                            {!! Form::textarea('profession[]','',['id'=>'profession','class'=>'form-control', 'placeholder'=>"$professionStr",'readonly' => 'readonly','rows'=>2]) !!}

                            <span class="text-danger">{{ $errors->first('profession') }}</span>
                        </div>
                    </div>

                    <!-- importance -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $importanceArr  = ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'];
                            $importanceText = isset($importanceArr[$contact->importance])?$importanceArr[$contact->importance]:"";    
                            ?>

                            {!! Form::text('importance','',['id'=>'importance','class'=>'form-control', 'placeholder'=>$importanceText,'readonly' => 'readonly']) !!}
                            <span class="text-danger">{{ $errors->first('importance') }}</span>
                        </div>
                    </div>

                    <!-- sensitivity -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity  </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $sensitivityArr     = ['1' => 'Low' , '2' => 'Medium', '3' => 'High'];
                            $sensitivityText    = isset($sensitivityArr[$contact->sensitivity])?$sensitivityArr[$contact->sensitivity]:"";
                            ?>
                            {!! Form::text('sensitivity','',['id'=>'sensitivity','class'=>'form-control', 'placeholder'=>$sensitivityText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('sensitivity') }}</span>
                        </div>
                    </div>

                    <!-- caution -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution  </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php 
                            $cautionArr     = ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'];
                            $cautionText    = isset($cautionArr[$contact->caution])?$cautionArr[$contact->caution]:"";
                            ?>

                            {!! Form::text('caution','',['id'=>'caution','class'=>'form-control', 'placeholder'=>$cautionText,'readonly' => 'readonly']) !!}
                            <span class="text-danger">{{ $errors->first('caution') }}</span>
                        </div>
                    </div>

                    <!-- internal_organizations -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Internal Organizations </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                            $internalOrg        = ['1' => 'Makhzoumi Foundation' , '2' => 'Beirutiyat', '3' => 'Makhzoumi Health Care', '4' => 'Makhzoumi Development'];
                            $internalOrgText    = isset($internalOrg[$contact->internal_organizations])?$internalOrg[$contact->internal_organizations]:"";
                            ?>
                            {!! Form::text('internal_organizations','',['id'=>'internal_organizations','class'=>'form-control', 'placeholder'=>$internalOrgText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('internal_organizations') }}</span>
                        </div>
                    </div>

                    <!-- administrative_group -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Administrative Group </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                            $administrativeGpArr    = ['1' => 'Business Group' , '2' => 'Volunteer Group', '3' => 'Administration Group', '4' => 'Management Group'];
                            $adminstrativeGpText    = isset($administrativeGpArr[$contact->administrative_group])?$administrativeGpArr[$contact->administrative_group]:"";
                            ?>
                            {!! Form::text('administrative_group','',['id'=>'administrative_group','class'=>'form-control', 'placeholder'=>$adminstrativeGpText,'readonly' => 'readonly']) !!}
                            <span class="text-danger">{{ $errors->first('administrative_group') }}</span>
                        </div>
                    </div>
                    
                    <!-- relationship_preference -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Relationship Preference </label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                            $relationPreArr     = ['1' => 'Customers' , '2' => 'Employees', '3' => 'Lender & Investors', '4' => 'Competitors', '5' => 'Suppliers'];
                            $relationPreText    = isset($relationPreArr[$contact->relationship_preference])?$relationPreArr[$contact->relationship_preference]:"";
                            ?>
                            {!! Form::text('relationship_preference','',['id'=>'relationship_preference','class'=>'form-control', 'placeholder'=>$relationPreText,'readonly' => 'readonly']) !!}

                            <span class="text-danger">{{ $errors->first('relationship_preference') }}</span>
                        </div>
                    </div>

                    <!-- status -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Status  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" style="margin-top: 5px">
                            <label>
                                <?php
                                $statusOn   = $contact->status == 1?true:false;
                                ?>
                                {!! Form::radio('status', '1',$statusOn,['class' => 'flat','disabled' => 'disabled']) !!}  Active
                            </label>

                            <label>
                                <?php
                                $statusOff  = $contact->status == 0?true:false;
                                ?>
                                {!! Form::radio('status', '0',$statusOff,['class' => 'flat','disabled' => 'disabled']) !!}  Inactive
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <!-- Keywords -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            {!! Form::text('keywords',isset($contact->keywords)?$contact->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                            <span class="text-danger">{{ $errors->first('greeting_keywords') }}</span>
                        </div>
                    </div>

                    <!-- Notes -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <?php 
                            $notes     = isset($contact->notes)?$contact->notes:'';
                            ?>
                            {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                            <span class="text-danger">{{ $errors->first('greeting_notes') }}</span>
                        </div>
                    </div>
                </div>

                <!-- left -->
                <!-- created section -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <!-- Created by -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Created by  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $firstName  = isset($contact->user_createby['first_name'])?$contact->user_createby['first_name']:'';
                            $lastName   = isset($contact->user_createby['last_name'])?$contact->user_createby['last_name']:'';
                            $createdBy  = $firstName.' '.$lastName;
                            ?>
                            {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>

                    <!-- Created on -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Created on  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $createdAt   = isset($contact->created_at)?date_format($contact->created_at, 'j M Y g:i A'):'';
                            ?>

                            {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>
                </div>

                <!-- right -->
                <!-- updated section -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <!-- Updated by -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Updated by  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $firstName  = isset($contact->user_updateby['first_name'])?$contact->user_updateby['first_name']:'';
                            $lastName   = isset($contact->user_updateby['last_name'])?$contact->user_updateby['last_name']:'';
                            $createdBy  = $firstName.' '.$lastName;
                            ?>
                            {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>

                    <!-- Updated on -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Updated on  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $createdAt   = isset($contact->updated_at)?date_format($contact->updated_at, 'j M Y g:i A'):'';
                            ?>
                            {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>
                </div>
                
                <!-- submit section -->
                <?php
                $i  = 0;
                if($i != 0) {
                ?>
                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                    </div>
                </div>
                <?php 
                }
                ?>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>