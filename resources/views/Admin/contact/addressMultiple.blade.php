<div class="address_multi_fields" data-id="{{ $address_id }}">
    <div class="x_title">
        <h4>Address</h4>
        <div class="clearfix"></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <input type="hidden" name="id[]" value="">
        <div class=" box-primary">

            <!-- delivery address -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-2 col-sm-2 col-xs-12 control-label">Delivery Address<span class="required">*</span> </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <label class="delivery_address">
                        <input type="radio" name="newaddress[{{  $address_id }}]['delivery_address']" value="1" class='flat'>  Yes
                    </label>
                    <label class="delivery_address">
                        <input type="radio" name="newaddress[{{  $address_id }}]['delivery_address']" value="0" class='flat'>  No
                    </label>
                    <span class="text-danger" id="err_contact_address_delivery_address_{{ $address_id }}"></span>
                </div>
            </div>

            <!-- left -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">

                <!-- type -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Type <span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control contact_address_type" id ="contact_address_type_{{ $address_id }}" name="newaddress[{{  $address_id }}]['type']" onchange="getTypeValue({{ $address_id }})">
                            <option disabled="disabled" selected="selected" value="">Select Type</option>
                            <option value="1">Office</option>
                            <option value="2">Residential</option>
                        </select>
                        <span class="text-danger" id="err_contact_address_type_{{ $address_id }}"></span>
                    </div>
                </div>

                <!-- country -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Country <span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <span id='countryAutoLoad' class="countryAutoLoad_{{ $address_id }}">
                            <select class="form-control country_{{ $address_id }} contact_address_country" id ="contact_address_country_id_{{ $address_id }}" name="newaddress[{{  $address_id }}]['country_id']" data-id="1" onchange="changeCountry(this)">
                                <option selected="selected" disabled="disabled" value="">Select Country</option>
                            </select>
                        </span>
                        <span class="text-danger" id="err_contact_address_country_id_{{ $address_id }}"></span>
                    </div>
                </div>

                <!-- province -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Province </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <span id='provinceAutoLoad' class="provinceAutoLoad_{{ $address_id }}">
                            <select class="form-control" name="newaddress[{{  $address_id }}]['province_id']">
                                <option>Select Province</option>
                            </select>
                        </span>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <!-- city -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">City  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <span id='cityAutoLoad' class="cityAutoLoad_{{ $address_id }}">
                            <select class="form-control" name="newaddress[{{  $address_id }}]['city_id']">
                                <option>Select City</option>
                            </select>
                        </span>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <!-- address line1 -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 1  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="newaddress[{{ $address_id }}]['address_line1']" class='form-control' id='address_line1' placeholder='Address Line 1'>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <!-- website -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Website  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="newaddress[{{ $address_id }}]['website']" class='form-control contact_address_website' id='contact_address_website_{{ $address_id }}' placeholder='Postal Website' onkeyup='getWebsiteValue({{ $address_id }})'>
                        <span class="text-danger" id="err_contact_address_website_{{ $address_id }}"></span>
                    </div>
                </div>

                <!-- importance -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" name="newaddress[{{ $address_id }}]['importance']">
                            <option>Select Importance</option>
                            <option value="1">First</option>
                            <option value="2">Second</option>
                            <option value="3">Third</option>
                            <option value="4">Fourth</option>
                            <option value="5">Fifth</option>
                        </select>
                        <span class="text-danger"></span>
                    </div>
                </div>
            </div>
            <!-- left -->

            <!-- right -->
            <div class="form-group col-md-6 col-sm-6 col-xs-12">

                <!-- region_id -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Region <span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control region_{{ $address_id }} contact_address_region" name="newaddress[{{ $address_id }}]['region_id']" id='contact_address_region_id_{{ $address_id }}' data-id="{{ $address_id }}" onchange="changeRegion(this)">
                                <option disabled="disabled" value="" selected="selected">Select region</option>
                                @foreach($region as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                        </select>
                        <span class="text-danger" id="err_contact_address_region_id_{{ $address_id }}"></span>
                    </div>
                </div>

                <!-- district -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">District </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <span id='districtAutoLoad' class="districtAutoLoad_{{ $address_id }}">
                            <select class="form-control" name="newaddress[{{ $address_id }}]['district_id']">
                                <option>Select District</option>
                            </select>
                        </span>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <!-- postal code -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Postalcode<span class="required">*</span> </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="newaddress[{{ $address_id }}]['postal_code']" class='form-control' id='contact_address_postal_code_{{ $address_id }}' placeholder='Postal Code' onkeyup="getPostalCodeValue({{ $address_id }})">
                        <span class="text-danger" id="err_contact_address_postal_code_{{ $address_id }}"></span>
                    </div>
                </div>

                <!-- po box -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Po Box  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="newaddress[{{ $address_id }}]['po_box']" class='form-control' id='po_box' placeholder='Po Box'>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <!-- address line2 -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 2  </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" name="newaddress[{{ $address_id }}]['address_line2']" class='form-control' id='address_line2' placeholder='Address Line 2'>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <!-- sensitivity -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" name="newaddress[{{ $address_id }}]['sensitivity']">
                            <option>Select Sensitivity</option>
                            <option value="1">Low</option>
                            <option value="2">Medium</option>
                            <option value="3">High</option>
                        </select>
                        <span class="text-danger"></span>
                    </div>
                </div>

                <!-- caution -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" name="newaddress[{{ $address_id }}]['caution']">
                            <option>Select Caution</option>
                            <option value="1">Low</option>
                            <option value="2">Medium</option>
                            <option value="3">High</option>
                        </select>
                        <span class="text-danger"></span>
                    </div>
                </div>
            </div>
            <!-- right -->
        </div>
    </div>
</div>
