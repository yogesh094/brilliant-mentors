<div role="tabpanel" class="tab-pane fade" id="referral" >
    {{-- \Session::get('submit_referral')? 'active fade in':'' --}}
    <div class="x_title">
        <h4>Referral </h4>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        @if (Session::has('representativeSuccess'))
            <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('representativeSuccess') !!}</div>
        @endif

        <div class="alert alert-danger contact-referral-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>Whoops!</strong> There were some problems with your input.
        </div>
        @if(count($errors))
            <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
        @endif
        <span class="errormessage"></span>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class=" box-primary">
        {!! Form::open(array('url' => ('contact/referral-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

        {!! Form::hidden('id', isset($referralContact->id)?$referralContact->id:"") !!}
        {!! Form::hidden('contact_id', $contact->id) !!}
        <!-- left -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">

            <!-- First Name -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name <span class="required">*</span> </label>

                <!-- title -->
                <div class="col-md-3 col-sm-3 col-xs-3">
                    {{ Form::select('referral_title', ['1' => 'Mr.' , '2' => 'Mrs.' , '3' => 'Miss.'],isset($referralContact->title)?$referralContact->title:'',['id' => 'title','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('referral_title') }}</span>
                </div>

                <!-- First Name -->
                <div class="col-md-6 col-sm-6 col-xs-9">
                    {{ Form::select('referral_first_name', ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => '    Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'],isset($referralContact->first_name)?$referralContact->first_name:'',['placeholder' => 'Select First Name','class' => 'form-control','id'=>'refferalFirstName']) }}
                    <span class="text-danger refferalFirstNameError">{{ $errors->first('referral_first_name') }}</span>
                </div>
            </div>

            <!-- suffix -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Suffix <span class="required">*</span> </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::select('referral_suffix[]',['1' => 'CLU' , '2' => 'D.D.S.', '3' => 'D.V.M.', '4' => 'Esq.', '5' => 'LL.D.', '6' => 'M.D.', '7' => 'O.S.B.', '8' => 'Ph.D.', '9' => 'R.G.S', '10' => 'S.H.C.J.'],isset($referralContact->suffix)?explode(',',$referralContact->suffix):'',['class'=>'form-control selectheight', 'placeholder'=>'Select Suffix','multiple' => 'multiple','id'=>'referralSuffix']) !!}
                    <span class="text-danger referralSuffixError">{{ $errors->first('referral_suffix') }}</span>
                </div>
            </div>

            <!-- Father Name -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Father Name</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_father_name', ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '6' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'],isset($referralContact->father_name)?$referralContact->father_name:'',['placeholder' => 'Select Father Name','class' => 'form-control','id' =>'referral_father_name']) }}

                    <span class="text-danger">{{ $errors->first('referral_father_name') }}</span>
                </div>
            </div>

            <!-- Mother Name -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Mother Name</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_mother_name', ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ] ,isset($referralContact->mother_name)?$referralContact->mother_name:'', ['placeholder' => 'Select Mother Name','class' => 'form-control','id' => 'referral_mother_name']) }}
                    <span class="text-danger">{{ $errors->first('referral_mother_name') }}</span>
                </div>
            </div>

            <!-- Surname -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Surname <span class="required">*</span> </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_surname', ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'],isset($referralContact->surname)?$referralContact->surname:'',['placeholder' => 'Select Surname','class' => 'form-control','id' => 'referral_surname','id'=>'referralSurname']) }}
                    <span class="text-danger referralSurnameError">{{ $errors->first('referral_surname') }}</span>
                </div>
            </div>

            <!-- nick name -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('referral_nick_name',isset($referralContact->nickname)?$referralContact->nickname:'',['class'=>'form-control', 'placeholder'=>'Nick Name']) !!}
                    <span class="text-danger">{{ $errors->first('referral_nick_name') }}</span>
                </div>
            </div>

            <!-- DOB -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('referral_date_of_birth',isset($referralContact->date_of_birth) ?date("m-d-Y", strtotime($referralContact->date_of_birth)):'',['id'=>'referral_of_birth','class'=>'form-control', 'placeholder'=>'Date of Birth']) !!}
                    <span class="text-danger">{{ $errors->first('referral_date_of_birth') }}</span>
                </div>
            </div>

            <!-- birth place -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Birth Place</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::select('referral_birth_place',['1'=>'Akkar','2'=>'Akoura','3'=>'Antelias','4'=>'Ashrafieh',],isset($referralContact->birth_place)?$referralContact->birth_place:'',['class'=>'form-control', 'placeholder'=>'Birth Place']) !!}
                    <span class="text-danger">{{ $errors->first('referral_birth_place') }}</span>
                </div>
            </div>

            <!-- blood type -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Type</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_blood_type',$bloodType,isset($referralContact->blood_type)?$referralContact->blood_type:'',['placeholder' => 'Select Blood Type','class' => 'form-control']) }}

                    <span class="text-danger">{{ $errors->first('referral_blood_type') }}</span>
                </div>
            </div>

            <!-- gender -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Gender</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_gender', $gender,isset($referralContact->gender)?$referralContact->gender:'',['placeholder' => 'Select Gender','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('referral_gender') }}</span>
                </div>
            </div>

            <!-- No of Dependents -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                No Of Dependents</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::text('referral_number_of_dependents', isset($referralContact->number_of_dependents)?$referralContact->number_of_dependents:'',['placeholder' => 'Number Of Dependents','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('referral_number_of_dependents') }}</span>
                </div>
            </div>

            <!-- nationality -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nationality</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_nationality[]', $nationality,isset($referralContact->nationality)?explode(',',$referralContact->nationality):'',['placeholder' => 'Select Nationality','class' => 'form-control selectheight','multiple' => 'multiple']) }}
                    <span class="text-danger">{{ $errors->first('referral_nationality') }}</span>
                </div>
            </div>

            <!-- languages -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Languages</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_languages[]', $languages,isset($referralContact->languages)?explode(',',$referralContact->languages):'',['placeholder' => 'Select Language','class' => 'form-control selectheight','multiple' => 'multiple']) }}
                    <span class="text-danger">{{ $errors->first('referral_languages') }}</span>
                </div>
            </div>

            <!-- profession -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Profession </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_profession[]', ['1' => 'Profession 1' , '2' => 'Profession 2' , '3' => 'Profession 3', '4' => 'Profession 4', '5' => 'Profession 5'],isset($referralContact->profession)?explode(',',$referralContact->profession):'',['placeholder' => 'Select Profession','class' => 'form-control selectheight', 'multiple' => 'multiple']) }}
                    <span class="text-danger">{{ $errors->first('referral_profession') }}</span>
                </div>
            </div>

            <!-- importance -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_importance', ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'], isset($referralContact->importance)?$referralContact->importance:'', ['placeholder' => 'Select Importance','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('referral_importance') }}</span>
                </div>
            </div>

            <!-- caution -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_caution', ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'],isset($referralContact->caution)?$referralContact->caution:'',['placeholder' => 'Select Caution','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('referral_caution') }}</span>
                </div>
            </div>

            <!-- sensitivity -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_sensitivity', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],isset($referralContact->sensitivity)?$referralContact->sensitivity:'', ['placeholder' => 'Select Sensitivity','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('referral_sensitivity') }}</span>
                </div>
            </div>
        </div>

        <!-- right -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">

            <!-- photo box -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 pull-right">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3">
                    <div style="text-align: left;float:none;">
                        <?php
                        if(isset($referralContact->photo) && $referralContact->photo != "") {
                        ?>
                            <img src="{{ $imageUrl.$referralContact->photo }}" class="img-thumbnail contact_spouse_photo referral_image" >
                        <?php
                        } else {
                        ?>
                            <img src="{{ url('/uploads/contact/avatar.png') }}" class="img-thumbnail contact_spouse_photo referral_image" >
                        <?php
                        }
                        ?>
                        <input type="hidden" name="old_contact_referral_image" value="{{ isset($referralContact->photo)?$referralContact->photo:'' }}">
                    </div>
                </div>
            </div>

            <!-- Photo -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Photo</label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::File('referral_photo',['class'=>'form-control', 'accept'=>'.jpeg,.jpg,.png', 'placeholder'=>'Photo']) !!}
                    <span class="text-danger referralPhotoError">{{ $errors->first('referral_photo') }}</span>
                </div>
            </div>

            <!-- Private -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Private  </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <label>
                        <?php
                            $privateYes = $privateNo = false;
                            if(isset($referralContact->status)) {
                                $privateYes = ($referralContact->private == 1?true:false);
                                $privateNo  = ($referralContact->private == 0?true:false);
                            }
                        ?>
                        {!! Form::radio('referral_private', '1', $privateYes, ['class' => 'flat' ]) !!}  Yes
                    </label>
                    <label>
                        {!! Form::radio('referral_private', '0', $privateNo, ['class' => 'flat']) !!}  No
                    </label>
                </div>
            </div>

            <!-- Business Owner -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Business Owner</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php
                        $businessOwnYes = $businessOwnNo = false;
                        if(isset($referralContact->business_owner)) {
                            $businessOwnYes = ($referralContact->business_owner == 1?true:false);
                            $businessOwnNo  = ($referralContact->business_owner == 0?true:false);
                        }
                    ?>
                    <label>
                        {!! Form::radio('referral_business_owner', '1',$businessOwnYes,['class' => 'flat']) !!}  Yes
                    </label>

                    <label>
                        {!! Form::radio('referral_business_owner', '0',$businessOwnNo,['class' => 'flat']) !!}  No
                    </label>
                    <span class="text-danger">{{ $errors->first('referral_business_owner') }}</span>
                </div>
            </div>

            <!-- Sponsorships -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sponsorships</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_sponsorships[]', ['1' => 'JPMorgan' , '2' => 'Microsoft', '3' => 'Deloitte', '4' => 'PepsiCo', '5' => 'DHL Express'],isset($referralContact->sponsorships)?explode(',',$referralContact->sponsorships):'',['placeholder' => 'Select Sponsorships','class' => 'form-control selectheight','multiple'=>'multiple']) }}
                    <span class="text-danger">{{ $errors->first('referral_sponsorships') }}</span>
                </div>
            </div>

            <!-- Charitable Organizations -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Charitable Organizations </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_charitable_organizations[]', ['1' => 'YSR FOUNDATION ' , '2' => 'THE COCONADA CHAMBER OF COMMERCE', '3' => 'RAVINDRANATH MEDICAL FOUNDATION'],isset($referralContact->charitable_organizations)?explode(',',$referralContact->charitable_organizations):'',['placeholder' => 'Select Charitable Organizations','class' => 'form-control selectheight', 'multiple'=>'multiple']) }}
                    <span class="text-danger">{{ $errors->first('referral_charitable_organizations') }}</span>
                </div>
            </div>

            <!-- Political Party -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Party </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_political_party[]', ['1' => 'Movement of the future' , '2' => 'Kataeb Party', '3' => 'Hezbollah', '4' => 'Free Patriotic Movement'],isset($referralContact->political_party)?explode(',',$referralContact->political_party):'',['placeholder' => 'Select Political party','class' => 'form-control selectheight', 'multiple'=>'multiple']) }}
                    <span class="text-danger">{{ $errors->first('referral_political_party') }}</span>
                </div>
            </div>

            <!-- Relationship Preference -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Relationship Preference </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_relationship_preference[]', ['1' => 'Relationship Preference 1' , '2' => 'Relationship Preference 2', '3' => 'Relationship Preference 3', '4' => 'Relationship Preference 4'],isset($referralContact->relationship_preference)?explode(',',$referralContact->relationship_preference):'',['placeholder' => 'Select Relationship Preference','class' => 'form-control selectheight', 'multiple'=>'multiple']) }}
                    <span class="text-danger">{{ $errors->first('referral_relationship_preference') }}</span>
                </div>
            </div>

            <!-- Religion -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Religion </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_religion', ['1' => 'Christian', '2' => 'Muslim'], isset($referralContact->religion_id)? $referralContact->religion_id:'' ,['placeholder' => 'Select Religion','class' => 'form-control']) }}
                    <span class="text-danger">{{ $errors->first('referral_religion') }}</span>
                </div>
            </div>

            <!-- Internal Organizations -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Internal Organizations </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_internal_organizations[]', ['1' => 'Makhzoumi Foundation' , '2' => 'Beirutiyat', '3' => 'Makhzoumi Health Care', '4' => 'Makhzoumi Development'],isset($referralContact->internal_organizations)?explode(',',$referralContact->internal_organizations):'',['placeholder' => 'Select Internal Organizations','class' => 'form-control selectheight', 'multiple'=>'multiple']) }}
                    <span class="text-danger">{{ $errors->first('referral_internal_organizations') }}</span>
                </div>
            </div>

            <!-- Administrative Group -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Administrative Group </label>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    {{ Form::select('referral_administrative_group[]', ['1' => 'Business Group' , '2' => 'Volunteer Group', '3' => 'Administration Group', '4' => 'Management Group'],isset($referralContact->administrative_group)?explode(',',$referralContact->administrative_group):'',['placeholder' => 'Select Administrative Group','class' => 'form-control selectheight', 'multiple'=>'multiple']) }}
                    <span class="text-danger">{{ $errors->first('referral_administrative_group') }}</span>
                </div>
            </div>

            <!-- Status -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Status  </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <label>
                        <?php
                            $statusYes  = true;
                            $statusNo   = false;
                            if(isset($referralContact->status)) {
                                $statusYes = ($referralContact->status == 1?true:false);
                                $statusNo  = ($referralContact->status == 0?true:false);
                            }
                        ?>
                        {!! Form::radio('referral_status', '1', $statusYes, ['class' => 'flat' ]) !!}  Active
                    </label>
                    <label>
                        {!! Form::radio('referral_status', '0', $statusNo, ['class' => 'flat']) !!}  Inactive
                    </label>
                </div>
            </div>
        </div>

        <!-- keywords -->
        <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    keywords
                </label>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('referral_keywords',isset($referralContact->keywords)?$referralContact->keywords:'',['id'=>'keywords','class'=>'form-control tagInput', 'placeholder'=>'Type keyword']) !!}
                    <span class="text-danger">{{ $errors->first('referral_keywords') }}</span>
                </div>
            </div>
        </div>

        <!-- notes -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                    Notes
                </label>
                 <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::textarea('referral_notes', isset($referralContact->notes)?$referralContact->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'notes','size' => '3x3']) !!}
                <span class="text-danger">{{ $errors->first('referral_notes') }}</span>
                </div>
            </div>
        </div>

        <!-- left -->
        <!-- created section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Created by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($referralContact->user_createby['first_name'])?$referralContact->user_createby['first_name']:'';
                    $lastName   = isset($referralContact->user_createby['last_name'])?$referralContact->user_createby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Created on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Created on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($referralContact->created_at)?date_format($referralContact->created_at, 'jS M Y g:iA'):'';
                    ?>

                    {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- right -->
        <!-- updated section -->
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <!-- Updated by -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated by  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $firstName  = isset($referralContact->user_updateby['first_name'])?$referralContact->user_updateby['first_name']:'';
                    $lastName   = isset($referralContact->user_updateby['last_name'])?$referralContact->user_updateby['last_name']:'';
                    $createdBy  = $firstName.' '.$lastName;
                    ?>
                    {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>

            <!-- Updated on -->
            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                Updated on  </label>
                <div class="col-md-9 col-sm-9 col-xs-12" >
                    <?php 
                    $createdAt   = isset($referralContact->updated_at)?date_format($referralContact->updated_at, 'jS M Y g:iA'):'';
                    ?>
                    {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                    
                </div>
            </div>
        </div>

        <!-- <div class="ln_solid"></div> -->

        <!-- submit section -->
        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit','name'=>'submit_referral')) !!}

                    <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
</div>