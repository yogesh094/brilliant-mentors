@extends('Admin.master_layout.master')

@section('title', 'Add Contact')

@section('breadcum')
     / Add Contact
@endsection

@push('styles')

<link href="{{ asset('resources/admin-assets/css/daterangepicker.css') }}" rel="stylesheet">
<link href="{{ asset('resources/admin-assets/css/iCheck/skins/square/blue.css') }}" rel="stylesheet">
@endpush
@section('content')

<div class="row">
    <div class="x_panel">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <div class="col-xs-2">
                <ul id="myTab" class="nav nav-tabs tabs-left" role="tablist">
                    <li role="presentation" class="active"><a href="#addContact" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Contact</a>
                    </li>
                    <li role="presentation" class="disabled" ><a href="#communication" role="tab" id="profile-tab" >Address</a>
                    </li>
                    <li role="presentation" class="disabled"><a href="#jobposition" role="tab" id="profile-tab" data-toggle="tab" >Job Position</a>{{--  \Session::get('submit_job_position')? 'active':'' --}}
                    </li>
                    <li role="presentation" class="disabled"><a href="#spouse" role="tab" id="profile-tab" data-toggle="tab" >Spouse</a>
                    <li role="presentation" class="disabled"><a href="#business" role="tab" id="business-tab" data-toggle="tab" >Business</a>
                    <li role="presentation" class="disabled"><a href="#representative" role="tab" id="representative-tab" data-toggle="tab" >Representative</a>
                  <li role="presentation" class="disabled"><a href="#referral" role="tab" id="referral-tab" data-toggle="tab" >Referral</a>
                    <li role="presentation" class="disabled"><a href="#political-positions" role="tab" id="political-positions-tab" data-toggle="tab" >Political Positions</a>
                    <li role="presentation" class="disabled"><a href="#contact-number" role="tab" id="contact-number-tab" data-toggle="tab" >Contact Number</a>
                    <li role="presentation" class="disabled"><a href="#communication" role="tab" id="communication-tab" data-toggle="tab" >Communication</a>
                    <li role="presentation" class="disabled"><a href="#education" role="tab" id="education-tab" data-toggle="tab" >Education</a>
                    <li role="presentation" class="disabled"><a href="#greetingsLetter" role="tab" id="greetings-tab" data-toggle="tab" >Greetings Letter</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-10">
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="addContact" >
                        <div class="x_title">
                            <h4>Contact </h4>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                            @if (Session::has('successContact'))
                                <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('successContact') !!}</div>
                            @endif
                            @if (Session::has('errorContact'))
                                <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('errorContact') !!}</div>
                            @endif
                            <div class="alert alert-danger contact-error-block alert-dismissable" style="display: none;"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                    <strong>Whoops!</strong> There were some problems with your input.
                                </div>
                            @if(count($errors))
                                <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                    <strong>Whoops!</strong> There were some problems with your input.
                                </div>
                            @endif
                            <!-- if there are creation errors, they will show here -->
                            <span class="errormessage"></span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class=" box-primary">

                                {!! Form::open(array('url' => ('contact/store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}    

                                    <!-- left -->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <!-- private -->
                                         <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                                            <!-- private -->
                                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                            Private  </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <label>
                                                    {!! Form::radio('private', '1','',['class' => 'flat']) !!}  Yes
                                                </label>
                                                <label>
                                                    {!! Form::radio('private', '0','',['class' => 'flat']) !!}  No
                                                </label>
                                            </div>
                                        </div>

                                        <!-- Business Owner -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                            Business Owner  </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <label>
                                                    {!! Form::radio('business_owner', '1','',['class' => 'flat']) !!}  Yes
                                                </label>

                                                <label>
                                                    {!! Form::radio('business_owner', '0','',['class' => 'flat']) !!}  No
                                                </label>
                                            </div>
                                        </div>

                                        <!-- Contact Unique Key -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact ID <span class="required">*</span> </label>
                                            <div class="col-md-4 col-sm-4 col-xs-6">
                                                {!! Form::text('contact_unique_key','',['class'=>'form-control', 'placeholder'=>'Contact ID','id' => 'contact_unique_key']) !!}
                                                <span class="text-danger" id="err_contact_unique_key">{{ $errors->first('contact_unique_key') }}</span>
                                            </div>

                                            <div class="col-md-5 col-sm-5 col-xs-6">
                                                {!! Form::text('system_unique_key',$systemUniqueKey,['class'=>'form-control', 'placeholder'=>'System ID','readonly' => 'readonly']) !!}
                                                <span class="text-danger">{{ $errors->first('system_unique_key') }}</span>
                                            </div>
                                        </div>

                                        <!-- Title -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Title <span class="required">*</span> </label>

                                            <!-- title -->
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('title', ['1' => 'Mr.' , '2' => 'Mrs.' , '3' => 'Miss.'],null,['id' => 'contact_title','class' => 'form-control','id' => 'contact_title']) }}
                                                <span class="text-danger" id=
                                                "err_contact_title">{{ $errors->first('title') }}</span>
                                            </div>
                                        </div>

                                        <!-- suffix -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Suffix <span class="required">*</span> </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('suffix[]', ['1' => 'CLU' , '2' => 'D.D.S.', '3' => 'D.V.M.', '4' => 'Esq.', '5' => 'LL.D.', '6' => 'M.D.', '7' => 'O.S.B.', '8' => 'Ph.D.', '9' => 'R.G.S', '10' => 'S.H.C.J.'],'',['placeholder' => 'Select Suffix','class' => 'form-control selectheight', 'id'=>'contact_suffix' ,'multiple' => 
                                                'multiple']) }}
                                                <span class="text-danger" id='err_contact_suffix' >{{ $errors->first('suffix') }}</span>
                                                
                                            </div>
                                        </div>

                                        <!-- First Name -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name<span class="required">*</span> </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                
                                                {{ Form::select('first_name', ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross', '4' => '    Sara', '5' => 'Shay', '6' => 'Sofi', '7' => 'Jeshan', '8' => 'Janet', '9' => 'Ruhi', '10' => 'Tianna'],'',['placeholder' => 'Select First Name', 'id'=>'contact_first_name', 'class' => 'form-control']) }}
                                                <span class="text-danger" id="err_contact_first_name">{{ $errors->first('first_name') }}</span>
                                            </div>
                                        </div>

                                        <!-- Father Name -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Father Name <span class="required">*</span> </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">

                                                {{ Form::select('father_name', ['1' => 'Abagnale' , '2' => 'Aaron', '3' => 'Abelson', '4' => 'Hega', '5' => 'huuey', '6' => 'Mike', '7' => 'Maaiz', '8' => 'Sham', '9' => 'Sawin', '10' => 'Sunny'],'',['placeholder' => 'Select Father Name','class' => 'form-control', 'id'=>'contact_father_name']) }}
                                                <span class="text-danger" id="err_contact_father_name">{{ $errors->first('father_name') }}</span>
                                            </div>
                                        </div>

                                        <!-- Mother Name -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Mother Name <span class="required">*</span> </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('mother_name', ['1' => 'Vinkal' , '2' => 'Meiwa', '3' => 'Ross','4' => 'Maddy','5' => 'Harmony','6' => 'Harriot','7' => 'Shimeka','8' => 'Shirenna','9' => 'Madaya','10' => 'Maddie' ] ,'', ['placeholder' => 'Select Mother Name','class' => 'form-control','id'=>'contact_mother_name']) }}
                                                <span class="text-danger" id="err_contact_mother_name">{{ $errors->first('mother_name') }}</span>
                                            </div>
                                        </div>

                                        <!-- Surname -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Surname <span class="required">*</span> </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('surname', ['1' => 'Hank','2' => 'Frank','3' => 'Edward','4' => 'Reuben','5' => 'Hal','6' => 'James','7' => 'Creighton','8' => 'Jane', '9' => 'Henry', '10' => 'Samuel'],'',['placeholder' => 'Select Surname','class' => 'form-control','id'=>'contact_surname']) }}
                                                <span class="text-danger" id="err_contact_surname">{{ $errors->first('surname') }}</span>
                                            </div>
                                        </div>

                                        <!-- DOB -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth <span class="required">*</span> </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {!! Form::text('date_of_birth', date("m/d/Y",strtotime("-18 years")),['id'=>'date_of_birth','class'=>'form-control', 'placeholder'=>'Date of Birth']) !!}
                                                <span class="text-danger" id="err_date_of_birth">{{ $errors->first('date_of_birth') }}</span>
                                            </div>
                                        </div>

                                        <!-- blood type -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Blood Type  </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('blood_type', $bloodType,null,['placeholder' => 'Select Blood Type','class' => 'form-control']) }}
                                                
                                                <span class="text-danger">{{ $errors->first('blood_type') }}</span>
                                            </div>
                                        </div>

                                        <!-- nationality -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nationality  </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('nationality[]', $nationality,null,['placeholder' => 'Select Nationality','class' => 'form-control selectheight', 'multiple' => 'multiple']) }}

                                                <span class="text-danger">{{ $errors->first('nationality') }}</span>
                                            </div>
                                        </div>

                                        <!-- languages -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Languages  </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('languages[]', $languages,null,['placeholder' => 'Select Languages','class' => 'form-control selectheight', 'multiple' => 'multiple']) }}
                                                
                                                <span class="text-danger">{{ $errors->first('languages') }}</span>
                                            </div>
                                        </div>

                                        <!-- sponsorships -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sponsorships </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">

                                                {{ Form::select('sponsorships', ['1' => 'JPMorgan' , '2' => 'Microsoft', '3' => 'Deloitte', '4' => 'PepsiCo', '5' => 'DHL Express'],null,['placeholder' => 'Select Sponsorships','class' => 'form-control']) }}
                                                <span class="text-danger">{{ $errors->first('sponsorships') }}</span>
                                            </div>
                                        </div>

                                        <!-- political_party -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Party </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('political_party', ['1' => 'Movement of the future' , '2' => 'Kataeb Party', '3' => 'Hezbollah', '4' => 'Free Patriotic Movement'],null,['placeholder' => 'Select Political party','class' => 'form-control']) }}
                                                <span class="text-danger">{{ $errors->first('political_party') }}</span>
                                            </div>
                                        </div>

                                        <!-- charitable_organizations -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Charitable Organizations </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('charitable_organizations', ['1' => 'YSR FOUNDATION ' , '2' => 'THE COCONADA CHAMBER OF COMMERCE', '3' => 'RAVINDRANATH MEDICAL FOUNDATION'],null,['placeholder' => 'Select Charitable Organizations','class' => 'form-control']) }}
                                                <span class="text-danger">{{ $errors->first('charitable_organizations') }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- right -->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <!-- photo box -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3 col-sm-offset-3">
                                                <div style="text-align: left;float:none;">
                                                    <img src="{{ url('/uploads/contact/avatar.png') }}" class="img-thumbnail contact_spouse_photo contact_image" style="height: 130px;">
                                                </div>
                                            </div>
                                        </div>

                                        <!-- nick name -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name  </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {!! Form::text('nick_name',"",['class'=>'form-control', 'placeholder'=>'Nick Name']) !!}
                                                <span class="text-danger">{{ $errors->first('nick_name') }}</span>
                                            </div>
                                        </div>

                                        <!-- birth place -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Birth Place </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <?php
                                                $countryDropdown    = "";
                                                $countryDropdown    .= "<select class='form-control' name='birth_place'>";
                                                $countryDropdown    .= "<option value=''>Select Birth Place</option>";
                                                if(count($nations) > 0) {
                                                    foreach ($nations as $key => $country) {
                                                        
                                                        $countryDropdown    .= "<option value='$country->country_code'>$country->country</option>";

                                                    }
                                                }
                                                
                                                $countryDropdown    .= "</select>";
                                                echo $countryDropdown;
                                                ?>
                                                <span class="text-danger">{{ $errors->first('birth_place') }}</span>
                                            </div>
                                        </div>

                                        <!-- gender -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                            Gender  </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('gender', $gender,null,['placeholder' => 'Select Gender','class' => 'form-control']) }}
                                                <span class="text-danger">{{ $errors->first('gender') }}</span>
                                            </div>
                                        </div>

                                        <!-- marital_status -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                            Marital Status   </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('marital_status', $maritalStatus,null,['placeholder' => 'Select Marital Status','class' => 'form-control']) }}
                                                <span class="text-danger">{{ $errors->first('marital_status') }}</span>
                                                
                                            </div>
                                        </div>

                                        <!-- religion -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Religion </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('religion', ['1' => 'Christian', '2' => 'Muslim'],null,['placeholder' => 'Select Religion','class' => 'form-control']) }}

                                                <span class="text-danger">{{ $errors->first('religion') }}</span>
                                            </div>
                                        </div> 

                                        <!-- photo upload -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Photo </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {!! Form::File('photo',['class'=>'form-control', 'placeholder'=>'Photo', 'accept'=>'.jpeg,.jpg,.png']) !!}
                                                <span class="text-danger" id="err_contact_photo">{{ $errors->first('photo') }}</span>
                                            </div>
                                        </div>

                                        <!-- profession -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Profession </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">

                                                {{ Form::select('profession[]', ['1' => 'Profession 1' , '2' => 'Profession 2' , '3' => 'Profession 3', '4' => 'Profession 4', '5' => 'Profession 5'],null,['placeholder' => 'Select Profession','class' => 'form-control selectheight', 'multiple' => 'multiple']) }}
                                                
                                                <span class="text-danger">{{ $errors->first('profession') }}</span>
                                            </div>
                                        </div>

                                        <!-- importance -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Importance </label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">

                                                {{ Form::select('importance', ['1' => 'First' , '2' => 'Second' , '3' => 'Third', '4' => 'Fourth', '5' => 'Fifth'],null,['placeholder' => 'Select Importance','class' => 'form-control']) }}
                                                <span class="text-danger">{{ $errors->first('importance') }}</span>
                                            </div>
                                        </div>

                                        <!-- sensitivity -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sensitivity  </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('sensitivity', ['1' => 'Low' , '2' => 'Medium', '3' => 'High'],null,['placeholder' => 'Select Sensitivity','class' => 'form-control']) }}
                                                <span class="text-danger">{{ $errors->first('sensitivity') }}</span>
                                            </div>
                                        </div>

                                        <!-- caution -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Caution  </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('caution', ['1' => 'Causion' , '2' => 'No Causion' , '3' => 'Risky', '4' => 'More Risk', '5' => 'No More Rsk'],null,['placeholder' => 'Select Caution','class' => 'form-control']) }}
                                                <span class="text-danger">{{ $errors->first('caution') }}</span>
                                            </div>
                                        </div>

                                        <!-- internal_organizations -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Internal Organizations </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('internal_organizations', ['1' => 'Makhzoumi Foundation' , '2' => 'Beirutiyat', '3' => 'Makhzoumi Health Care', '4' => 'Makhzoumi Development'],null,['placeholder' => 'Select Internal Organizations','class' => 'form-control']) }}
                                                <span class="text-danger">{{ $errors->first('internal_organizations') }}</span>
                                            </div>
                                        </div>

                                        <!-- administrative_group -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Administrative Group </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                
                                                {{ Form::select('administrative_group', ['1' => 'Business Group' , '2' => 'Volunteer Group', '3' => 'Administration Group', '4' => 'Management Group'],null,['placeholder' => 'Select Administrative Group','class' => 'form-control']) }}

                                                <span class="text-danger">{{ $errors->first('administrative_group') }}</span>
                                            </div>
                                        </div>

                                        <!-- relationship_preference -->
                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Relationship Preference </label>

                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                {{ Form::select('relationship_preference', ['1' => 'Customers' , '2' => 'Employees', '3' => 'Lender & Investors', '4' => 'Competitors', '5' => 'Suppliers'],null,['placeholder' => 'Select Relationship Preference','class' => 'form-control']) }}
                                               
                                                <span class="text-danger">{{ $errors->first('relationship_preference') }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- submit section -->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                                            {!! Form::submit('Submit', array('class' => 'btn btn-primary submit','name'=>'submit_add_contact')) !!}

                                            <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>

                                            
                                        </div>
                                    </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="communication" >
                        <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
                            booth letterpress, commodo enim craft beer mlkshk aliquip</p>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection


@push('scripts')
<script src="{{ asset('resources/admin-assets/js/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/daterangepicker.js') }}"></script>
<script src="{{ asset('resources/admin-assets/css/iCheck/icheck.min.js') }}"></script>
<script type="application/javascript">

$(document).ready(function(){

    // blank value disabled in dropdown
    $("select option[value='']").attr("disabled", true);
    
    $('#date_of_birth').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,        
        singleClasses: "picker_2"
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

    //contact add js validation - start
        $("input[name=submit_add_contact]").click(function(){
            var contact_title       = $("#contact_title option:selected" ).val();
            var contact_suffix      = $("#contact_suffix option:selected" ).val();
            var contact_first_name  = $("#contact_first_name option:selected" ).val();
            var contact_father_name = $("#contact_father_name option:selected" ).val();
            var contact_mother_name = $("#contact_mother_name option:selected" ).val();
            var contact_surname     = $("#contact_surname option:selected" ).val();
            var contact_unique_key  = $("input[name='contact_unique_key']").val();
            var date_of_birth       = $("input[name='date_of_birth']").val();

            var isError     = 0;
            if(contact_title == ""){
                $('#err_contact_title').html('Title is required');
                isError     = 1;
            } else {
                $('#err_contact_title').html('');
            }

            if(contact_suffix == undefined || contact_suffix == ""){
                $('#err_contact_suffix').html('Suffix is required');
                isError     = 1;
            } else {
                $('#err_contact_suffix').html('');
            }

            if(contact_first_name == ""){
                $('#err_contact_first_name').html('First name  is required');
                isError     = 1;
            } else {
                $('#err_contact_first_name').html('');
            }

            if(contact_father_name == ""){
                $('#err_contact_father_name').html('Father name  is required');
                isError     = 1;
            } else {
                $('#err_contact_father_name').html('');
            }

            if(contact_mother_name == ""){
                $('#err_contact_mother_name').html('Mother name  is required');
                isError     = 1;
            } else {
                $('#err_contact_mother_name').html('');
            }

            if(contact_surname == ""){
                $('#err_contact_surname').html('Surname is required');
                isError     = 1;
            } else {
                $('#err_contact_surname').html('');
            }

            if(contact_unique_key == ""){
                $('#err_contact_unique_key').html('Contact unique is required');
                isError     = 1;
            } else {
                $('#err_contact_unique_key').html('');
            }

            if(date_of_birth == ""){
                $('#err_date_of_birth').html('Date of birth is required');
                isError     = 1;
            } else {
                $('#err_date_of_birth').html('');
            }

            if(isError == 1) {
                $(".contact-error-block").show();
                return false;
            } else {
                $(".contact-error-block").hide();
            }

            var fileType = $('input[name=photo]')[0].files[0]['type'];
            var ValidImageTypes = ["image/jpeg", "image/jpg", "image/png"];
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                $('#err_contact_photo').html('select only jpeg,jpg,png image');
                return false;
            }
        });

        $("input[name='contact_unique_key']").keyup(function(){
            var contact_unique_key = $(this).val().trim();
            if(contact_unique_key == "") {            
                $('#err_contact_unique_key').html('Contact id is required');
            } else {
                $('#err_contact_unique_key').html('');
            }
        });

        $('#contact_suffix option').on('click',function(){
            var contact_suffix = $( "#contact_suffix option:selected" ).length;
            if(contact_suffix == 0) {
                $('#err_contact_suffix').html('Select the Suffix');
                return false;
            } else {
                $('#err_contact_suffix').html('');
            }
        });

        $('#contact_first_name').change(function(){
            var contact_first_name = $( "#contact_first_name option:selected" ).val();
            if(contact_first_name == "") {
                $('#err_contact_first_name').html('Select the first name');
                return false;
            }
            $('#err_contact_first_name').html('');
        });

        $('#contact_father_name').change(function(){
            var contact_father_name = $( "#contact_father_name option:selected" ).val();
            if(contact_father_name == "") {
                $('#err_contact_father_name').html('Select the father name');
                return false;
            }
            $('#err_contact_father_name').html('');
        });

        $('#contact_mother_name').change(function(){
            var contact_mother_name = $( "#contact_mother_name option:selected" ).val();
            if(contact_mother_name == "") {
                $('#err_contact_mother_name').html('Select the mother name');
                return false;
            }
            $('#err_contact_mother_name').html('');
        });

        $('#contact_surname').change(function(){
            var contact_surname = $( "#contact_surname option:selected" ).val();
            if(contact_surname == "") {
                $('#err_contact_surname').html('Select the Surname');
                return false;
            }
            $('#err_contact_surname').html('');
        });

        $('input[name=photo]').on('change',function(){
            if (this.files && this.files[0]) {
                var file = this.files[0];
                var fileType = file["type"];
                var ValidImageTypes = ["image/jpeg", "image/jpg", "image/png"];
                if ($.inArray(fileType, ValidImageTypes) < 0) {
                    $('#err_contact_photo').html('select only jpeg,jpg,png image');
                    return false;
                } else {
                    $('#err_contact_photo').html('');
                    displayClass = '.contact_image';
                    DisplayOnChangeFile(this.files,displayClass);
                }
            }
        });
    //contact add js validation - end
});

function DisplayOnChangeFile(files,displayClass){
            if (files && files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(displayClass).attr('src', e.target.result);
                }
                reader.readAsDataURL(files[0]);
            }
        }
</script>
@endpush
