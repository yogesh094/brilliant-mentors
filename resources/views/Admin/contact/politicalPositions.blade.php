<div role="tabpanel" class="tab-pane fade in" id="politicalPositions" >
    <div class="x_title">{{-- \Session::get('submit_political_positions')? 'active fade in':'' --}}
        <h4>Political Positions </h4>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if (Session::has('referralSuccess'))
                <div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>{!! Session::get('referralSuccess') !!}</div>
            @endif
            <div class="alert alert-danger political_position_err_block" style="display: none;">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
            </div>
            @if(count($errors))
                <div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                </div>
            <!-- if there are creation errors, they will show here -->
            @endif
            <span class="errormessage"></span>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
                {!! Form::open(array('url' => ('contact/political-positions-store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!}

                {!! Form::hidden('id',isset($politicalPositionContact->id)?$politicalPositionContact->id:"") !!}
                {!! Form::hidden('contact_id', $contact->id) !!}
                <!-- left -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                    <!-- Political Position type -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Position Type <span class="required">*</span> </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::select('political_posotion_type', ['1' => 'Secretary of Commerce', '2'=>'Deputy Secretary', '3'=>'General Counsel'] , isset($politicalPositionContact->type) ?$politicalPositionContact->type: '', ['placeholder' => 'Select Political Position Type','class' => 'form-control', 'id' => 'contact_poli_pos_type']) }}
                            <span class="text-danger" id="err_contact_poli_pos_type">{{ $errors->first('political_posotion_type') }}</span>
                        </div>
                    </div>

                    <!-- Political Party -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Party </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::select('political_posotion_party', ['1' => 'AAP' , '2' => 'BJP', '3' => 'Congress', '4' => 'BASPA'], isset($politicalPositionContact->political_party) ? $politicalPositionContact->political_party : '',['placeholder' => 'Select Political party','class' => 'form-control']) }}
                            <span class="text-danger">{{ $errors->first('political_posotion_party') }}</span>
                        </div>
                    </div>

                    <!-- Description -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {!! Form::text('political_posotion_description',isset($politicalPositionContact->description) ? $politicalPositionContact->description:'', ['class'=>'form-control', 'placeholder' => 'Description']) !!}
                            <span class="text-danger">{{ $errors->first('political_posotion_description') }}</span>
                        </div>
                    </div>

                    <!-- Start Date -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date  <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {!! Form::text('political_posotion_start_date',isset($politicalPositionContact->start_at)?date('m-d-Y',strtotime($politicalPositionContact->start_at)):'',['id'=>'political_posotion_start_date','class'=>'form-control', 'placeholder'=>'Start Date']) !!}
                            <span class="text-danger" id='err_political_posotion_start_date'>{{ $errors->first('political_posotion_start_date') }}</span>
                        </div>
                    </div>
                </div>


                <!-- right -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                    <!-- Political Position Held -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Political Position Held <span class="required">*</span> </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::select('political_posotion_held', ['1'=>'Economic positions','2'=>'Social issues','3'=>'Legal issues','4'=>'Foreign policy positions',],isset($politicalPositionContact->political_posotion)?$politicalPositionContact->political_posotion:'', ['placeholder' => 'Select Political Position Held','class' => 'form-control', 'id' => 'contact_poli_pos_held']) }}
                            
                            <span class="text-danger" id="err_contact_poli_pos_held">{{ $errors->first('political_posotion_held') }}</span>
                        </div>
                    </div>

                    <!-- Greeting -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Greeting</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {{ Form::select('political_posotion_greeting', ['1'=>'Test Greeting msg1','2'=>'Test Greeting msg2','3'=>'Test Greeting msg3','4'=>'Test Greeting msg4',],isset($politicalPositionContact->greeting) ? $politicalPositionContact->greeting: '', ['placeholder' => 'Select greeting','class' => 'form-control']) }}
                            <span class="text-danger">{{ $errors->first('political_posotion_greeting') }}</span>
                        </div>
                    </div>

                    <!-- Exit Date -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Exit Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {!! Form::text('political_posotion_exit_date',isset($politicalPositionContact->end_at)?date('m-d-Y',strtotime($politicalPositionContact->end_at)):'',['id'=>'political_posotion_end_date','class'=>'form-control', 'placeholder'=>'Exit Date']) !!}
                            <span class="text-danger" id="err_political_posotion_end_date">{{ $errors->first('political_posotion_exit_date') }}</span>
                        </div>
                    </div>
                </div>

                <!-- keywords -->
                <div class="form-group form-group col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                            keywords
                        </label>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            {!! Form::text('political_posotion_keywords',isset($politicalPositionContact->keywords) ? $politicalPositionContact->keywords:'',['id'=>'keywords','class'=>'form-control tagInput', 'placeholder'=>'Type keyword']) !!}
                            <span class="text-danger">{{ $errors->first('political_posotion_keywords') }}</span>
                        </div>
                    </div>
                </div>

                <!-- notes -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                       <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                            Notes
                        </label>
                         <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::textarea('political_posotion_notes', isset($politicalPositionContact->notes)?$politicalPositionContact->notes:'', ['class'=>'form-control textEditor', 'placeholder'=>'notes','size' => '3x3']) !!}
                        <span class="text-danger">{{ $errors->first('political_posotion_notes') }}</span>
                        </div>
                    </div>
                </div>

                <!-- left -->
                <!-- created section -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <!-- Created by -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Created by  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $firstName  = isset($politicalPositionContact->user_createby['first_name'])?$politicalPositionContact->user_createby['first_name']:'';
                            $lastName   = isset($politicalPositionContact->user_createby['last_name'])?$politicalPositionContact->user_createby['last_name']:'';
                            $createdBy  = $firstName.' '.$lastName;
                            ?>
                            {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>

                    <!-- Created on -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Created on  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $createdAt   = isset($politicalPositionContact->created_at)?date_format($politicalPositionContact->created_at, 'jS M Y g:iA'):'';
                            ?>

                            {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>
                </div>

                <!-- right -->
                <!-- updated section -->
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <!-- Updated by -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Updated by  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $firstName  = isset($politicalPositionContact->user_updateby['first_name'])?$politicalPositionContact->user_updateby['first_name']:'';
                            $lastName   = isset($politicalPositionContact->user_updateby['last_name'])?$politicalPositionContact->user_updateby['last_name']:'';
                            $createdBy  = $firstName.' '.$lastName;
                            ?>
                            {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>

                    <!-- Updated on -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                        Updated on  </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" >
                            <?php 
                            $createdAt   = isset($politicalPositionContact->updated_at)?date_format($politicalPositionContact->updated_at, 'jS M Y g:iA'):'';
                            ?>
                            {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                            
                        </div>
                    </div>
                </div>

                <!-- <div class="ln_solid"></div> -->

                <!-- submit section -->
                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            {!! Form::submit('Submit', array('class' => 'btn btn-primary submit', 'name' => 'submit_contact_political_pos')) !!}

                            <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>