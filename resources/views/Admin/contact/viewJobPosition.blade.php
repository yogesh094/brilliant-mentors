<div role="tabpanel" class="tab-pane fade in" id="jobposition" >
    <div class="x_title">{{-- \Session::get('submit_job_position')? 'active fade in':'' --}}
        <h4>Job Position </h4>
        <a type="button" class="btn btn-warning btn-xs pull-right"  href="{{ url('/contact/edit/'.$contact->id.'#jobposition') }}"> Edit <i class="fa fa-external-link"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <!-- <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul> -->

                </div>
            <!-- if there are creation errors, they will show here -->
            @endif
            <span class="errormessage"></span>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
                {!! Form::open(array('url' => ('contact/job-position/store'),'method'=>'POST', 'files'=>true,'class' => "form-horizontal form-label-left" )) !!} 

                    {!! Form::hidden('contact_id',isset($contact->id)?$contact->id:'') !!}   

                    <!-- left -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <!-- Job type -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Type </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php 
                                $jobTypeArr     = ['1'=>'Owner','2'=>'Employed','3'=>'Freelancer','4'=>'Politician'];
                                $jobTypeText        = "";
                                if(isset($jobpositionDetail->job_type)) {
                                    $jobTypeText   = (isset($jobTypeArr[$jobpositionDetail->job_type]))?$jobTypeArr[$jobpositionDetail->job_type]:"";    
                                }
                                ?>    
                                {!! Form::text('type','',['class'=>'form-control', 'placeholder'=>$jobTypeText,'readonly' => 'readonly']) !!}
                                
                                <span class="text-danger">{{ $errors->first('job_type') }}</span>
                            </div>
                        </div>

                        <!-- Job Category -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Category</label>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php 
                                $jobCategoryText    = "";
                                if(isset($jobpositionDetail->job_category)) {

                                    $jobCategoryText    = isset($jobCategory[$jobpositionDetail->job_category])?$jobCategory[$jobpositionDetail->job_category]:"";
                                }
                                ?>
                                {!! Form::text('job_category','',['class'=>'form-control', 'placeholder'=>$jobCategoryText,'readonly' => 'readonly']) !!}
                                
                                <span class="text-danger">{{ $errors->first('job_category') }}</span>
                            </div>
                        </div>

                        <!-- Start Date -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $jobstartDateText   = isset($jobpositionDetail->start_date)?date('m-d-Y',strtotime($jobpositionDetail->start_date)):'';
                                ?> 

                                {!! Form::text('start_date','',['class'=>'form-control', 'placeholder'=>$jobstartDateText,'readonly' => 'readonly']) !!}
                                
                                <span class="text-danger">{{ $errors->first('start_date') }}</span>
                            </div>
                        </div>

                        <!-- LinkedIn text -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">LinkedIn</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php 
                                $linkedInText   = isset($jobpositionDetail->linkedin)?$jobpositionDetail->linkedin:'';
                                ?>
                                {!! Form::text('linkedin','',['class'=>'form-control', 'placeholder'=>$linkedInText,'readonly' => 'readonly']) !!}
                                <span class="text-danger">{{ $errors->first('linkedin') }}</span>
                            </div>
                        </div>

                        <!-- Country -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $countryText        = "";    
                                if(isset($jobpositionDetail->country_id)) {
                                    $countryText    = isset($nationality[$jobpositionDetail->country_id])?$nationality[$jobpositionDetail->country_id]:"";
                                }
                                ?> 

                                {!! Form::text('country_id','',['class'=>'form-control', 'placeholder'=>$countryText,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('country_id') }}</span>
                            </div>
                        </div>

                        <!-- District -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            District</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $districtText        = "";    
                                if(isset($jobpositionDetail->district_id)) {
                                    $districtText    = isset($jobDistrict[$jobpositionDetail->district_id])?$jobDistrict[$jobpositionDetail->district_id]:"";
                                }
                                ?> 

                                {!! Form::text('district_id','',['class'=>'form-control', 'placeholder'=>$districtText,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('district_id') }}</span>
                            </div>
                        </div>

                        <!-- Postal Code -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Postalcode</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $districtText        = isset($jobpositionDetail->postal_code)?$jobpositionDetail->postal_code:'';    
                                ?> 

                                {!! Form::text('postal_code','',['class'=>'form-control', 'placeholder'=>$districtText,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('postal_code') }}</span>
                            </div>
                        </div>

                        <!-- Address Line 1 -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 1</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $addressLine1   = isset($jobpositionDetail->address_line_1)?$jobpositionDetail->address_line_1:'';    
                                ?> 

                                {!! Form::text('address_line_1','',['class'=>'form-control', 'placeholder'=>$addressLine1,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('address_line_1') }}</span>
                            </div>
                        </div>

                        <!-- Website -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Website</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $websiteText   = isset($jobpositionDetail->website)?$jobpositionDetail->website:'';    
                                ?> 

                                {!! Form::text('website','',['class'=>'form-control', 'placeholder'=>$websiteText,'readonly' => 'readonly']) !!}
                                <span class="text-danger">{{ $errors->first('website') }}</span>
                            </div>
                        </div>

                    </div>

                    <!-- right -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        
                        <!-- Oranization -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Organization</label>

                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $oranizationText   = isset($jobpositionDetail->oranization)?$jobpositionDetail->oranization:'';    
                                ?> 

                                {!! Form::text('oranization','',['class'=>'form-control', 'placeholder'=>$oranizationText,'readonly' => 'readonly']) !!}
                                
                                <span class="text-danger">{{ $errors->first('oranization') }}</span>
                            </div>
                        </div>

                        <!-- Job Positions -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Positions</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $jobPositionsText        = "";    
                                if(isset($jobpositionDetail->job_positions)) {
                                    $jobPositionsText    = isset($jobpositions[$jobpositionDetail->job_positions])?$jobpositions[$jobpositionDetail->job_positions]:"";
                                }
                                ?> 

                                {!! Form::text('job_positions','',['class'=>'form-control', 'placeholder'=>$jobPositionsText,'readonly' => 'readonly']) !!}

                                
                                <span class="text-danger">{{ $errors->first('job_positions') }}</span>
                            </div>
                        </div>

                        <!-- Exit Date -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Exit Date</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php 
                                $exitDateText   = isset($jobpositionDetail->exit_date)?date('m-d-Y',strtotime($jobpositionDetail->exit_date)):'';
                                ?>
                                {!! Form::text('exit_date','',['class'=>'form-control', 'placeholder'=>$exitDateText,'readonly' => 'readonly']) !!}
                                <span class="text-danger">{{ $errors->first('exit_date') }}</span>
                            </div>
                        </div>

                        <!-- Region -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Region</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $regionText        = "";    
                                if(isset($jobpositionDetail->region_id)) {
                                    $regionText    = isset($region[$jobpositionDetail->region_id])?$region[$jobpositionDetail->region_id]:"";
                                }
                                ?> 

                                {!! Form::text('region_id','',['class'=>'form-control', 'placeholder'=>$regionText,'readonly' => 'readonly']) !!}

                                
                            <span class="text-danger">{{ $errors->first('region_id') }}</span>
                            </div>
                        </div>

                        <!-- Province -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Province</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $jobProvinceText        = "";    
                                if(isset($jobpositionDetail->province_id)) {
                                    $jobProvinceText    = isset($jobProvince[$jobpositionDetail->province_id])?$jobProvince[$jobpositionDetail->province_id]:"";
                                }
                                ?> 

                                {!! Form::text('province_id','',['class'=>'form-control', 'placeholder'=>$jobProvinceText,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('province_id') }}</span>
                            </div>
                        </div>

                        <!-- City -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $cityText        = "";    
                                if(isset($jobpositionDetail->city_id)) {
                                    $jobProvinceText    = isset($jobCity[$jobpositionDetail->city_id])?$jobCity[$jobpositionDetail->city_id]:"";
                                }
                                ?> 

                                {!! Form::text('city_id','',['class'=>'form-control', 'placeholder'=>$cityText,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('city_id') }}</span>
                            </div>
                        </div>

                        <!-- PO Box -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">PO Box </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $poboxText   = isset($jobpositionDetail->po_box)?$jobpositionDetail->po_box:'';    
                                ?> 

                                {!! Form::text('po_box','',['class'=>'form-control', 'placeholder'=>$poboxText,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('po_box') }}</span>
                            </div>
                        </div>

                        <!-- Address Line 2 -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Address Line 2</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $addressLine2Text   = isset($jobpositionDetail->address_line_2)?$jobpositionDetail->address_line_2:'';    
                                ?> 

                                {!! Form::text('address_line_2','',['class'=>'form-control', 'placeholder'=>$addressLine2Text,'readonly' => 'readonly']) !!}

                                <span class="text-danger">{{ $errors->first('address_line_2') }}</span>
                            </div>
                        </div>

                        <!-- Status -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>

                            <div class="col-md-9 col-sm-9 col-xs-12" width="24px" height="24px">
                                <label>
                                    <?php
                                        $statusOn  = $statusOff   = false; 
                                        if(isset($jobpositionDetail->status)) {
                                            $statusOn     = ($jobpositionDetail->status == 1?true:false);
                                            $statusOff      = ($jobpositionDetail->status == 0?true:false);
                                        }
                                    ?>
                                    {!! Form::radio('status', '1',$statusOn ,['class' => 'flat','disabled' => 'disabled' ]) !!}  Active 
                                </label>
                                <label>
                                    {!! Form::radio('status','0',$statusOff,['class' => 'flat','disabled' => 'disabled']) !!}  Inactive 
                                </label>
                                <span class="text-danger">{{ $errors->first('status') }}</span>
                            </div>
                        </div>
                    </div>

                    <!-- keyword and notes -->
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                        <!-- Keywords -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="col-md-12 col-sm-12 col-xs-12 control-label">Keywords</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                {!! Form::text('keywords',isset($jobpositionDetail->keywords)?$jobpositionDetail->keywords:'',['id'=>'','class'=>'form-control tagInput', 'placeholder'=>'Keywords']) !!}

                            </div>
                        </div>
                        
                        <!-- Notes -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <?php 
                                $notes     = isset($jobpositionDetail->notes)?$jobpositionDetail->notes:'';
                                ?>
                                {!! Form::textarea('notes',$notes,['class'=>'form-control textEditor non-editable', 'placeholder'=>$notes,'contenteditable' => 'false','readonly' => 'readonly','rows' => 2]) !!}

                            </div>
                        </div>
                    </div>

                    <!-- left -->
                    <!-- created section -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <!-- Created by -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Created by  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" >
                                <?php 
                                $firstName  = isset($jobpositionDetail->user_createby['first_name'])?$jobpositionDetail->user_createby['first_name']:'';
                                $lastName   = isset($jobpositionDetail->user_createby['last_name'])?$jobpositionDetail->user_createby['last_name']:'';
                                $createdBy  = $firstName.' '.$lastName;
                                ?>
                                {!! Form::text('created_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                                
                            </div>
                        </div>

                        <!-- Created on -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Created on  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" >
                                <?php 
                                $createdAt   = isset($jobpositionDetail->created_at)?date_format($jobpositionDetail->created_at, 'j M Y g:i A'):'';
                                ?>

                                {!! Form::text('created_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                                
                            </div>
                        </div>
                    </div>

                    <!-- right -->
                    <!-- updated section -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <!-- Updated by -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Updated by  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" >
                                <?php 
                                $firstName  = isset($jobpositionDetail->user_updateby['first_name'])?$jobpositionDetail->user_updateby['first_name']:'';
                                $lastName   = isset($jobpositionDetail->user_updateby['last_name'])?$jobpositionDetail->user_updateby['last_name']:'';
                                $createdBy  = $firstName.' '.$lastName;
                                ?>
                                {!! Form::text('updated_by','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdBy,'readonly' => 'readonly']) !!}
                                
                            </div>
                        </div>

                        <!-- Updated on -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                            Updated on  </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" >
                                <?php 
                                $createdAt   = isset($jobpositionDetail->updated_at)?date_format($jobpositionDetail->updated_at, 'j M Y g:i A'):'';
                                ?>
                                {!! Form::text('updated_at','',['id'=>'nick_name','class'=>'form-control', 'placeholder'=>$createdAt,'readonly' => 'readonly']) !!}
                                
                            </div>
                        </div>
                    </div>

                    <!-- submit section -->
                    <?php 
                    $i = 0;
                    if($i == 1) {
                    ?>
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 text-left">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                                {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}

                                <a class="btn btn-default btn-close" href="{{ URL::to('/contact') }}">Cancel</a>
                            </div>
                        </div>
                    <?php 
                    }
                    ?>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>