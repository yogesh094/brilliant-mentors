@extends('Admin.master_layout.master')

@section('title', 'CMS List')

@section('breadcum')
     / CMS List 
@endsection

@section('content')
<div class="">
    <?php
    /*
     <div class="page-title">
        <div class="title_left">
            <h3>Users</h3>
            <p>You can manage users here.</p>
            </p>
        </div>
    </div>
    */
    ?>

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>CMS</h4>
                    <a href="{{ url('panel/cms/create') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add CMS
                    </a>
                    <!--
                <a href="{{ url('services/download/excel') }}" class="btn btn-sm btn-xs btn-warning pull-right">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export
                    </a>
                -->
                     
                     <div class="clearfix"></div>
                </div>
                <table id="cms-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <?php
                        /*
                        <th>#</th>
                        */
                        ?>
                        <th>ID</th>
                        <th>title</th>
                        <th>slug</th>
                        <th>Status</th>
                        <th>Action</th>
                       

                        <?php
                        /*
                        <th>Created On</th>
                        <th>Updated On</th>
                        <th>Action</th>
                        */
                        ?>
                    </tr>
                    </thead>
                </table>

                
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">
    
    $(document).on("click", '.delete-btn', function(event) { 
    var id = $(this).attr('data-id');
    $('#select-delete').val(id);
});
$('#delete-submit').click(function(){
    var id = $('#select-delete').val();

    var password = $(".delete-password").val();
    if (password == "") {
        $('#deletepasswordError').html('please enter password');
        return false;
    }
    $.ajax({
        url: "{{ url('panel/cms/destroy') }}",
        type: 'GET',
        async: false,
        data: {'password':password,'id':id},
        success: function (data) {
            if(data == "true") {
                $('.delete-close').click();
                $('.delete-'+id).parent().parent().remove();
                $(document).ready(function(){
                    $.notify({
                        message: 'User deleted successfully.'
                    },{
                        type: 'success'
                    });
                });
            }else{
              $('#deletepasswordError').html('please enter valid password');
              return false;
            }
        },
        error: function () {
            $('#deletepasswordError').html("something went wrong please try later");
            return false;
        }
    });
});
 
 
         $('#cms-table').DataTable({
            serverSide: true,
            processing: true,
            stateSave: true,
            ajax: "{{ url('/panel/cms/array-data') }}",
            columns: [
                {data: 'id',orderable: true},
                {data: 'title',orderable: true, searchable: true},
                {data: 'slug',orderable: true, searchable: true},
                {data: 'status', orderable: true},
                {data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                     next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
   
</script>
@endpush('scripts')
