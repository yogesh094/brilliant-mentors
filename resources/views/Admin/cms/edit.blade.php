@extends('Admin.master_layout.master')

@section('title', 'Edit CMS')

@section('breadcum')
     / <a href="{{ url('panel/cms') }}">CMS List</a> / Edit CMS 
@endsection

@section('content')

<!-- @foreach ($errors->all() as $error)
<p class="alert alert-warning">{{ $error }}</p>
@endforeach
<span class="errormessage"></span> -->
<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your request.
                    <br/>
                </div>
            @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit CMS</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/cms/store'),'method'=>'POST', 'id'=>'signup','files'=>true)) !!}
                <?php   
                    if(Auth::user()->user_role != 1){
                        $disabled = 'disabled';
                        $hide ='hide';
                    }else{
                        $disabled = ''; 
                        $hide='';
                    }
                ?>
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $data->id }} " >
                    <div class="form-group">
                        <div class="form-group">
                        {!! Form::label('Language', 'Language') !!}
                        <input type="hidden" name="language_id" value="{{ $data->language_id }}">
                        <select name="language_id" class="form-control" required="" id="language_id" disabled="">
                            <option value="">Select Language</option>
                            @if(count($languages) > 0)
                            @foreach($languages as $key => $lang)
                            <option value="{{ $lang['id'] }}" @if(isset($data->language_id) && $data->language_id == $lang['id'])selected=""@endif>{{ $lang['name'] }}</option>
                            @endforeach

                            @endif
                        </select>
                        <span class="text-danger">{{ $errors->first('language_id') }}</span>
                    </div>
                        <div class="form-group">
                            {!! Form::label('Title', 'Title : ',array('class'=>'control-label')) !!}

                         <input type="text" value="{{ $data->title }}" name="title" class="form-control" />

                            <span class="text-danger" >{{ $errors->first('title') }}</span>
                        </div>

                        <div class="form-group ">
                            {!! Form::label('Slug', 'Slug : ',array('class'=>'control-label')) !!}
                            
                            <input type="text" value="{{ $data->slug }}" name="slug" class="form-control" readonly="" />
                            <span class="text-danger" >{{ $errors->first('slug') }}</span>
                        </div>

                        <div class="form-group ">
                            {!! Form::label('Description ', 'Description  : ',array('class'=>'control-label')) !!}
                             <textarea name="description" class="form-control" id="summary-ckeditor" rows="15">{{ $data->description }}</textarea>
                           
                            <span class="text-danger" >{{ $errors->first('description') }}</span>
                        </div>


                        <div class="form-group">
                            {!! Form::label('status', 'Status:')   !!}
                             
                            @if ($data->status == 1)
                            
                            {!! Form::radio('status', '0','',['class' => 'flat']) !!} InActive
                            {!! Form::radio('status', '1',['checked' => 'checked'],['class' => 'flat']) !!} Active

                            @else

                            {!! Form::radio('status', '0',['checked' => 'checked'],['class' => 'flat']) !!} InActive
                            {!! Form::radio('status', '1','',['class' => 'flat']) !!} Active

                            @endif

                            
                        </div>

                
                    </div>
                    <div class="box-footer">
                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}

                        <a class="btn btn-default btn-close" href="{{ URL::to('/panel/cms') }}">Cancel</a>
                    </div>
                {!! Form::close() !!}
                </div>
                
                
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    //CKEDITOR.replace( 'summary-ckeditor' );
    CKEDITOR.replace('summary-ckeditor', {
   allowedContent:true,
});
</script>
    <script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
    
@endpush('scripts')