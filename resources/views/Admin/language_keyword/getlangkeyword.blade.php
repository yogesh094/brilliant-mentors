@if(count($LanguageKeyword) > 0)
{!! Form::open(array('url' => ('/panel/language/keyword/multiStore'),'method'=>'POST', 'files'=>true)) !!}
        <input type="hidden" name="langId" value="">
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
        	<h4>List for <b>Language Keyword</b></h4>
        </div>
        <div class="form-group col-md-12 col-sm-12 col-xs-12 entity_multi_data">
            <div class="row">
                <div class=" col-md-3 col-sm-3 col-xs-12 childRow_18">
                   <b> Keyword</b>
                </div>
                 <div class=" col-md-3 col-sm-3 col-xs-12 childRow_18">
                   <b> Label</b>
                </div>
            </div>
        </div>
               
                @foreach($LanguageKeyword as $key => $LanguageKey)
                <div class="form-group col-md-12 col-sm-12 col-xs-12 entity_multi_data">
                                	<div class="row">
                                	<div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
                                		<input class="form-control" id="keyword" name="keyword[{{ $LanguageKey->id }}][{{ $LanguageKey->language_id }}]" type="text" placeholder="Keyword" value="{{ $LanguageKey->keyword }}">
                                		<span class="text-danger">{{ $errors->first('keyword') }}</span>
                                	</div>
                                    
                                    <div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
                                    <input class="form-control" id="label" name="label[{{ $LanguageKey->id }}][{{ $LanguageKey->language_id }}]" type="text" placeholder="Label" value="{{ $LanguageKey->label }}">
                                    <span class="text-danger">{{ $errors->first('label') }}</span>
                                  </div>

                                    <span class="text-danger defaultTextError"></span>
                                    <div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
                                    
                <span class="trashRow trashRow_{{ $LanguageKey->id }} remove" trashid="{{ $LanguageKey->id }}" dataid="{{ $LanguageKey->id }}">
                    <i class="fa fa-trash" style="font-size: 18px;padding-top: 2%"></i>
                </span>
            
                                </div>
                           </div>
                                  
                            
                 
                </div>
@endforeach

<div class="form-group col-md-3 col-sm-3 col-xs-12 childRow_18">
  {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
</div>
{!! Form::close() !!} 
@endif