@extends('Admin.master_layout.master')

@section('title', 'Language Keyword List')

@section('breadcum')
     / Language Keyword List 
@endsection

@section('content')
<div class="">

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Language Keyword</h4>
                    <a  href="{{ url('/panel/language/keyword/add') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        {{ trans('Add Language Keyword') }}
                    </a>
                     
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                   <table id="data-table" class="table table-bordered">
                        <thead>
                        <tr>
                         <?php
                         /*
                            <th>#</th>
                        */
                        ?>
                            
                            <th>Language</th>
                            <th>Keyword</th>
                            <th>Label</th>
                            <th>Status</th>
                            <th>Action</th>
                        <?php /*    <th>Created On</th>
                            <th>Updated On</th>
                            */
                        ?>
                        <?php
                        /*
                            <th>Action</th>
                        */
                        ?>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script>
    $(function () {
        $('#data-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: "{{ url('/panel/language/keyword/array-data') }}",
            columns: [
                //{data: 'id', orderable: true, searchable: true },
                
                {data: 'language_id',orderable: true, searchable: true},
                {data: 'keyword',orderable: true, searchable: true},
                {data: 'label',orderable: true, searchable: true},
                {data: 'status', orderable: true},
                {data: 'action', orderable: true},
               // {data: 'created_at', searchable: false },
               // {data: 'updated_at', searchable: false },
              //  {data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                    next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
    });
</script>
@endpush('scripts')
