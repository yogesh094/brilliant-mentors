@extends('Admin.master_layout.master')

@section('title', 'Change Password')

@section('breadcum')
     / Change Password
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Change Password</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/changepassword'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        <div class="form-group <?php  echo $errors->first('current_password') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Current Password', 'Current Password',array('class'=>'control-label')) !!}
                            {{ Form::password('current_password',array('class' => 'form-control', 'id' => 'current_password')) }}
                            <span class="text-danger" >{{ $errors->first('current_password') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('new_password') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('New Password', 'New Password',array('class'=>'control-label')) !!}
                            {{ Form::password('new_password', array('class' => 'form-control', 'id' => 'new_password')) }}
                            <span class="text-danger" >{{ $errors->first('new_password') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('confirm_password') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Confirm Password', 'Confirm Password',array('class'=>'control-label')) !!}
                            {{ Form::password('confirm_password', array('class' => 'form-control', 'id' => 'username')) }}
                            <span class="text-danger" >{{ $errors->first('confirm_password') }}</span>
                        </div>

                        <div class="box-footer">
                            {!! Form::submit('Save', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/panel') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection