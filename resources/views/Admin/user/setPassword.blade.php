<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Religion</title><meta charset="UTF-8" />
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/icon" href="./images/favicon.ico"/>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('resources/admin-assets/css/bootstrap.min.css') }}" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('resources/admin-assets/css/font-awesome/css/font-awesome.min.css') }}" />
        <!-- NProgress -->
        <link href="{{ asset('resources/admin-assets/css/nprogress/nprogress.css') }}" rel="stylesheet">
        <!-- Animate.css -->
        <link href="{{ asset('resources/admin-assets/css/animate.css/animate.min.css') }}" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="{{ asset('resources/admin-assets/css/custom.min.css') }}" rel="stylesheet">

        <link href="{{ asset('resources/admin-assets/css/mycustom.css') }}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .login_content form input[type=password] {
                margin-bottom: 10px;
            }
            .text-danger {
                float: left;
                margin-top: -8px;
            }
            .login {
                background: url("{{ url('beirutiyat-lebanon-login.jpg') }}") no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;
                -o-background-size: cover;
                height: 100%;
            }
        </style>
    </head>

    <body class="login">




        <div class="row login_wrapper">


            <!-- login form start -->
            <div class="login_form">

                <section class="login_content">
                    <h1>
                        <img  width="60%;" src="{{ url('img/logo.png') }}">
                    </h1>

                    <form method="post" action="{{ url('user/setPassword') }}">

                       <!--  <div class="alert alert-success">
                            Your account has been verified
                        </div> -->
                        @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
                        <p>
                            Set Your password to complete your account registration
                        </p>
                        <input type="hidden" name="token" value="{{ $token }}">
                        {{ csrf_field() }}
                        <div class="row ">
                            <input type="password" class="form-control" placeholder="Password" name="password" required="required" />
                            <span class="text-danger" >{{ $errors->first('password') }}</span>
                        </div>
                        <div class="row">
                            <input type="password" class="form-control" placeholder="Confirm Password" name="conf_password" required="required"
                                   />
                            <span class="text-danger" >{{ $errors->first('conf_password') }}</span>
                        </div>
                        <div class="row text-left">
                            <button type="submit" class="btn btn-default submit">Set Password</button>
                            <!-- <a class="reset_pass to_forgotPassword" href="#forgotPassword">Lost password?</a> -->
                        </div>

                        <div class="clearfix"></div>


                    </form>
                    <div>
                        <p style="color: #fff;font-weight: 700;">Powered By Religion Consulting.</p>
                        <p style="color: #fff;font-weight: 700;">© <?= date('Y') ?> All Rights Reserved.</p>
                    </div>
                </section>
            </div>
            <!-- login form end -->

        </div>

<script type="text/javascript" src="{{ asset('resources/admin-assets/js/jquery.min.js') }}"></script>
<script type="text/javascript">
    $('input[type=password]').keypress(function( e ) {
    if(e.which === 32) 
        return false;
});
</script>
    </body>
</html>