<!-- @push('scripts') -->
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<script type="text/javascript">
    $('.submit').click(function(){
        var url         = "<?=url('/public')?>"
        var first_name      = $('input[name="first_name"]').val().trim();
        var last_name       = $('input[name="last_name"]').val().trim();       
        var phone1          = $('input[name="phone1"]').val().trim();
        var phone2          = $('input[name="phone2"]').val().trim();
        var email           = $('input[name="email"]').val().trim();
        var password        = $('input[name="password"]').val().trim();
        var conf_password   = $('input[name="conf_password"]').val().trim();
        
        if(first_name == ""){
            $('#fnameError').html('please enter first name');
            return false;
        }

        if(last_name == ""){
            $('#lnameError').html('please enter last name');
            return false;
        }

        var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;  
        if(phone1.length > 0) {
          if(!phone1.match(phoneno)) {  
              $('#phone1Error').html('please enter valid phone number');
              return false;  
          }
        }

        if(phone2.length > 0) {
          if(!phone2.match(phoneno)) {  
              $('#phone1Error').html('please enter valid phone number');
              return false;  
          }
        }

        if(email == ""){
            $('#emailError').html('please enter email');
            return false;
        }

        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
        if(!email.match(mailformat)) { 
          $('#emailError').html('please enter valid email');
          return false;
        }

        if(password == ""){
            $('#passwordError').html('please enter password');
            return false;
        }

        if(password.length < 6){
            $('#passwordError').html('Password should be more than 6 character');
            return false;
        }

        if(conf_password == ""){
            $('#confPasswordError').html('please enter confirm password');
            return false;
        }

        if(password != conf_password){
            $('#confPasswordError').html('please enter same as password');
            return false;
        }


        /*var image = $('input[name="image"]').val();
        if(image == ""){
            $('#imageError').html('please upload solution file!!');
            return false;
        }*/
    });

    $("input[name=first_name]").keyup(function(){
        var first_name = $(this).val().trim();
        if(first_name == "") {
            $('#fnameError').html('please enter first name');
            $(this).val('');
            return false;
        }
        $('#fnameError').html('');
    });

    $("input[name=last_name]").keyup(function(){
        var last_name = $(this).val().trim();
        if(last_name == "") {
            $('#lnameError').html('please enter last name');
            $(this).val('');
            return false;
        }
        $('#lnameError').html('');
    });

    $("input[name=phone1]").keyup(function(){
        var phone1 = $(this).val().trim();
        var numbers = /^[0-9]+$/;  
        if(phone1.length == 0) {
          $('#phone1Error').html('');
          return false;
        }
        if(!phone1.match(numbers))  {  
          $('#phone1Error').html('please enter valid phone number');
          return false;
        }

        if(phone1.length > 12) {
          $('#phone1Error').html('please enter valid phone number');
          return false;
        }
        $('#phone1Error').html('');
    });

    $("input[name=phone2]").keyup(function(){
        var phone2 = $(this).val().trim();
        var numbers = /^[0-9]+$/;  
        if(phone2.length == 0) {
          $('#phone2Error').html('');
          return false;
        }
        if(!phone2.match(numbers))  {  
          $('#phone2Error').html('please enter valid phone number');
          return false;
        }

        if(phone2.length > 12) {
          $('#phone2Error').html('please enter valid phone number');
          return false;
        }
        $('#phone2Error').html('');
    });

    $("input[name=email]").keyup(function(){
        var email = $(this).val().trim();
        if(email == "") {
            $('#emailError').html('please enter email');
            $(this).val('');
            return false;
        }
        $('#emailError').html('');
    });

    $("input[name=password]").keyup(function(){
        var password = $(this).val().trim();
        if(password == "") {
            $('#passwordError').html('please enter password');
            $(this).val('');
            return false;
        }
        $('#passwordError').html('');
    });

    $("input[name=conf_password]").keyup(function(){
        var conf_password = $(this).val().trim();
        if(conf_password == "") {
            $('#confPasswordError').html('please enter confirm password');
            $(this).val('');
            return false;
        }
        $('#confPasswordError').html('');
    });

    /*$('input[name="chellenge_image"]').on('change',function(){
        $('#ChellengeimageError').html('');
        if (this.files && this.files[0]) {
            var tmppath = URL.createObjectURL(this.files[0]);
            $('.add-image').html("<img class='add-image' style='max-height: 10%;max-width: 10%;' src='"+tmppath+"'>");
        }
    });
    $('input[name="image"]').on('change',function(){
        $('#imageError').html('');
    });*/
</script>
<!-- @endpush -->