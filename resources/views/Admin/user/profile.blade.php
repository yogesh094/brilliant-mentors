@extends('Admin.master_layout.master')

@section('title', 'Profile')

@section('breadcum')
     / Profile 
@endsection

@section('content')

<!-- @foreach ($errors->all() as $error)
<p class="alert alert-warning">{{ $error }}</p>
@endforeach
<span class="errormessage"></span> -->
<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
             @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your request.
                    <br/>
                </div>
            @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Profile</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/profileStore'),'method'=>'POST','files'=>true)) !!}
                <div class="x_content">
                   
                    <div class="form-group">

                        <div class="form-group <?php  echo $errors->first('first_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('First Name', 'First Name',array('class'=>'control-label')) !!}
                            {{ Form::text('first_name', Auth::User()->first_name,array('class' => 'form-control', 'id' => 'first_name')) }}
                            <span class="text-danger" >{{ $errors->first('first_name') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('last_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Last Name', 'Last Name',array('class'=>'control-label')) !!}
                            {{ Form::text('last_name', Auth::User()->last_name, array('class' => 'form-control', 'id' => 'last_name')) }}
                            <span class="text-danger" >{{ $errors->first('last_name') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('username') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('User Name', 'User Name',array('class'=>'control-label')) !!}
                            {{ Form::text('username', Auth::User()->username, array('class' => 'form-control', 'id' => 'username')) }}
                            <span class="text-danger" >{{ $errors->first('username') }}</span>
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                            {{ Form::text('email', Auth::User()->email, array('class' => 'form-control', 'id' => 'email','readonly' => true)) }}
                        </div>

                        <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Phone', 'Phone',array('class'=>'control-label')) !!}
                            {{ Form::text('phone_number', Auth::User()->phone_number, array('class' => 'form-control', 'id' => 'phone')) }}
                            <span class="text-danger" >{{ $errors->first('phone_number') }}</span>
                        </div>

                    </div>
                    <div class="box-footer">
                        {!! Form::submit('Save', array('class' => 'btn btn-primary edit-submit')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/panel') }}">Cancel</a>
                    </div>
                {!! Form::close() !!}
                </div>
               
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
    
@endpush('scripts')