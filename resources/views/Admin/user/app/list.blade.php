@extends('Admin.master_layout.master')

@section('title', 'User List')

@section('breadcum')
     / User List 
@endsection

@section('content')
<div class="">
    <?php
    /*
     <div class="page-title">
        <div class="title_left">
            <h3>Users</h3>
            <p>You can manage users here.</p>
            </p>
        </div>
    </div>
    */
    ?>

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Users</h4>
                    <a href="{{ url('panel/appuser/create') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add user
                    </a>
                    <!--  <a href="{{ url('admin/appuser/download/excel') }}" class="btn btn-sm btn-xs btn-warning pull-right">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export
                    </a> -->
                     <div class="clearfix"></div>
                </div>
                 {!! Form::open(array('url' => ('panel/appuser/updatemultirecode'),'method'=>'POST', 'files'=>true)) !!}
                <table id="appusers-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <?php
                        /*
                        <th>#</th>
                        */
                        ?>
                        <th>
                          <input type="checkbox" id="checkAll" />
                        </th>
                        <th>ID</th>
                        <th>Email</th>
                        <!-- <th>Phone</th> -->
                        <!-- <th>Gender</th> -->
                        <!-- <th>DOB</th>
                         -->
                         <th>Verify</th>
                        <th>Status</th>
                        <th>Action</th>

                        <?php
                        /*
                        <th>Created On</th>
                        <th>Updated On</th>
                        <th>Action</th>
                        */
                        ?>
                    </tr>
                    </thead>
                </table>
                <div id="showmultiaction" style="display: none;">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    <select name="action" class="form-control" required="">
                    <option value="active">Active</option>
                    <option value="inactive">InActive</option>
                    <option value="delete">Delete</option>
                   </select>
                </div>
                
                {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                </div>
                 {!! Form::close() !!}
                 <input type="hidden" name="pageno" id="pageno" value="{{ Session::get('AppUserPage') }}">
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">


$(document).on("click", '.delete-btn', function(event) { 
    var id = $(this).attr('data-id');
    $('#select-delete').val(id);
});
$('#delete-submit').click(function(){
    var id = $('#select-delete').val();

    var password = $(".delete-password").val();
    if (password == "") {
        $('#deletepasswordError').html('please enter password');
        return false;
    }
    $.ajax({
        url: "{{ url('panel/appuser/destroy') }}",
        type: 'GET',
        async: false,
        data: {'password':password,'id':id},
        success: function (data) {
            if(data == "true") {
                $('.delete-close').click();
                $('.delete-'+id).parent().parent().remove();
                $(document).ready(function(){
                    $.notify({
                        message: 'User deleted successfully.'
                    },{
                        type: 'success'
                    });
                });
            }else{
              $('#deletepasswordError').html('please enter valid password');
              return false;
            }
        },
        error: function () {
            $('#deletepasswordError').html("something went wrong please try later");
            return false;
        }
    });
});
        
        var table = $('#appusers-table').DataTable({
            serverSide: true,
            processing: true,
            stateSave: true,
            "order": [[1,"asc"]],
            ajax: "{{ url('/panel/appuser/array-data') }}",
            columns: [
                {data: 'checkbox',orderable: false},
                {data: 'id',orderable: true},
                {data: 'email',orderable: true, searchable: true},
                // {data: 'phone_number', orderable: true, searchable: true},
                //{data: 'gender', orderable: true},
                // {data: 'dob', orderable: true},
                {data: 'is_verified', orderable: true},
                {data: 'status', orderable: false},
              //  {data: 'created_at', searchable: false },
              //  {data: 'updated_at', searchable: false },
               {data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                     next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });

// $('#appusers-table').on( 'page.dt', function () {

//         var info = table.page.info();
//         var pageNo = info.page+1;
//         //alert(pageNo);
//         $.ajax({
//             type : 'POST',
//             url:  "{{ url('/panel/appuser/pageStoreNumber') }}",
//             data :  'pageNo='+ pageNo,
//             success : function(data){
//                 //location.reload();
//                 alert(data);
//                 table.ajax.reload(null, false);
//             }
//         });
// });
$("#checkAll").change(function () {
      $("input:checkbox").prop('checked', $(this).prop("checked"));
      if($(this).prop("checked") == true){
        $("#showmultiaction").show();
      }else if($("input[name='checkaction[]']").prop("checked") == true){
        $("#showmultiaction").show();
      }else{
        $("#showmultiaction").hide();
      }
      
});
function onclickcheck(id){
   var a = $("input[name='checkaction[]']");
  if(a.filter(":checked").length > 0){
      $("#showmultiaction").show();
  }else{
    $("#showmultiaction").hide();
  }
}
$('div#appusers-table_paginate ul.pagination li.paginate_button active a').click(function(e) {
    alert('call');
});
//alert($('ul.pagination li.active a').attr("data-dt-idx"));
</script>
@endpush('scripts')
