@extends('Admin.master_layout.master')

@section('title', 'Create User')

@section('breadcum')
    / <a href="{{ url('panel/appuser') }}">User List</a> / Create User
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create User</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/appuser/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        
                        <div class="form-group <?php  echo $errors->first('username') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('User Name', 'User Name',array('class'=>'control-label')) !!}
                            {{ Form::text('username', null, array('class' => 'form-control', 'id' => 'username')) }}
                            <span class="text-danger" >{{ $errors->first('username') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                            {{ Form::text('email', null, array('class' => 'form-control', 'id' => 'email')) }}
                            <span class="text-danger" >{{ $errors->first('email') }}</span>
                        </div>

                       
                        <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Password', 'Password',array('class'=>'control-label')) !!}
                           <!--  {{ Form::text('password', null, array('class' => 'form-control', 'id' => 'phone')) }} -->
                            <input type="password" name="password" class="form-control">
                            <span class="text-danger" >{{ $errors->first('password') }}</span>
                        </div>
                        <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Confirm Password', 'Confirm Password',array('class'=>'control-label')) !!}
                            <!-- {{ Form::text('password_confirmation', null, array('class' => 'form-control', 'id' => 'phone')) }} -->
                             <input type="password" name="password_confirmation" class="form-control">
                            <span class="text-danger" >{{ $errors->first('password_confirmation') }}</span>
                        </div>
                      
                       <!--  <div class="form-group <?php  echo $errors->first('user_role') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('gender', 'Gender',array('class'=>'control-label')) !!}
                            {!! Form::select('gender',array('1' => 'Male', '2' => 'Female'), null, array('class' => 'form-control','placeholder'=>'Select Gender')) !!}
                            <span class="text-danger" >{{ $errors->first('gender') }}</span>
                        </div> -->
                         <div class="form-group <?php  echo $errors->first('user_role') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Verify', 'Verify:',array('class'=>'control-label')) !!}
                            <label>
                                {!! Form::radio('is_verified', '1','true',['class' => 'flat']) !!}  Verified
                            </label>
                            <label>
                                {!! Form::radio('is_verified', '0','',['class' => 'flat']) !!}  Not verified
                            </label>
                            <span class="text-danger" >{{ $errors->first('is_verified') }}</span>
                        </div>
                        <div class="form-group <?php  echo $errors->first('user_role') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Status', 'Status:',array('class'=>'control-label')) !!}
                            <label>
                                {!! Form::radio('status', '1','true',['class' => 'flat']) !!}  Active
                            </label>
                            <label>
                                {!! Form::radio('status', '0','',['class' => 'flat']) !!}  Deactive
                            </label>
                            <span class="text-danger" >{{ $errors->first('status') }}</span>
                        </div>
                        <input type="hidden" value="add" name="type">
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/panel/user') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection