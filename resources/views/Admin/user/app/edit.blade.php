@extends('Admin.master_layout.master')

@section('title', 'Edit User')

@section('breadcum')
     / <a href="{{ url('panel/appuser') }}">User List</a> / Edit User 
@endsection

@section('content')

<!-- @foreach ($errors->all() as $error)
<p class="alert alert-warning">{{ $error }}</p>
@endforeach
<span class="errormessage"></span> -->
<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your request.
                    <br/>
                </div>
            @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit User</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/appuser/store'),'method'=>'POST', 'id'=>'signup','files'=>true)) !!}
                <div class="x_content">
                    <input type="hidden" name="id" value="{{ $data->id }} " >
                    <div class="form-group">

                        

                        <div class="form-group <?php  echo $errors->first('username') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('User Name', 'User Name',array('class'=>'control-label')) !!}
                            {{ Form::text('username', $data->username, array('class' => 'form-control', 'id' => 'username')) }}
                            <span class="text-danger" >{{ $errors->first('username') }}</span>
                        </div>
                        <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                            {{ Form::text('email', $data->email, array('class' => 'form-control', 'id' => 'email', 'readonly' => 'true')) }}
                            <span class="text-danger" >{{ $errors->first('email') }}</span>
                        </div>
                       <!--  <div class="form-group">
                            <label for="gender">{!! Form::label('gender', 'Gender:')   !!}</label>
                            @if ($data->gender == 1)
                            {!! Form::radio('gender', '1',['checked' => 'checked'],['class' => 'flat']) !!} Male
                            {!! Form::radio('gender', '2','',['class' => 'flat']) !!} Female
                            @else
                            {!! Form::radio('gender', '1','',['class' => 'flat']) !!} Male
                            {!! Form::radio('gender', '2',['checked' => 'checked'],['class' => 'flat']) !!} Female
                            @endif
                        </div> -->
                        <div class="form-group">
                            <label for="status">{!! Form::label('status', 'Status:')   !!}</label>
                            @if ($data->status == 1)
                            {!! Form::radio('status', '1',['checked' => 'checked'],['class' => 'flat']) !!} Active
                            {!! Form::radio('status', '0','',['class' => 'flat']) !!} InActive
                            @else
                            {!! Form::radio('status', '1','',['class' => 'flat']) !!} Active
                            {!! Form::radio('status', '0',['checked' => 'checked'],['class' => 'flat']) !!} InActive
                            @endif
                        </div>
                        <!-- <div class="form-group pull-right">
                            <button type="button" class="btn btn-danger submit" data-toggle="modal" data-target=".bs-example-modal-reset">Reset Password</button>    
                        </div> --><!-- 'id'=>'reset-password', -->

                    </div>
                    <input type="hidden" value="edit" name="type">
                    <div class="box-footer">
                        {!! Form::submit('Save', array('class' => 'btn btn-primary edit-submit hide')) !!}
                        <button type="submit" class="btn btn-primary submit" data-toggle="modal" data-target=".bs-example-modal-edit">Save</button>

                        <a class="btn btn-default btn-close" href="{{ URL::to('/panel/appuser') }}">Cancel</a>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
@endpush('scripts')