@extends('Admin.master_layout.master')

@section('title', 'Expert List')

@section('breadcum')
     / Expert List 
@endsection

@section('content')
<div class="">
    <?php
    /*
     <div class="page-title">
        <div class="title_left">
            <h3>Users</h3>
            <p>You can manage users here.</p>
            </p>
        </div>
    </div>
    */
    ?>

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Expert</h4>
                    <a href="{{ url('panel/user/create') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add Expert
                    </a>
                     <!-- <a href="{{ url('admin/user/download/excel') }}" class="btn btn-sm btn-xs btn-warning pull-right">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export
                    </a> -->
                     <div class="clearfix"></div>
                </div>
                <table id="users-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <?php
                        /*
                        <th>#</th>
                        */
                        ?>
                        <th>Email</th>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone</th>
                        <!-- <th>Role</th> -->
                        <th>Status</th>

                        <?php
                        /*
                        <th>Created On</th>
                        <th>Updated On</th>
                        
                        */
                        ?>
                        <th>Action</th>
<!--                        <tr class="filters">
                        <th>Email</th>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Action</th>

                        </tr>-->
                    </tr>
                    </thead>
                </table>

                <div class="modal fade bs-example-modal-delete" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <h5 class="modal-title" id="myModalLabel2">Verify</h5>
                            </div>
                            <div class="modal-body">
                                <p>Please enter your password to delete the user.</p>
                                {!! Form::open(array('url' => ('panel/user/destroy'),'method'=>'POST', 'files'=>true)) !!}
                                <div class="form-group">
                                    <input type="hidden" name="id" id="select-delete" >
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control delete-password" required="">
                                    <span class="text-danger" id="deletepasswordError"></span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <!-- <button type="button" class="btn btn-primary" id="delete-submit">Submit</button> -->
                                {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                                <button type="button" class="btn btn-default delete-close" data-dismiss="modal">Close</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">


$(document).on("click", '.delete-btn', function(event) { 
    var id = $(this).attr('data-id');
    $('#select-delete').val(id);
});
$('#delete-submit').click(function(){
    var id = $('#select-delete').val();

    var password = $(".delete-password").val();
    if (password == "") {
        $('#deletepasswordError').html('please enter password');
        return false;
    }
    $.ajax({
        url: "{{ url('panel/user/destroy') }}",
        type: 'GET',
        async: false,
        data: {'password':password,'id':id},
        success: function (data) {
            //alert(data);
            if(data == "true") {
                $('.delete-close').click();
                $('.delete-'+id).parent().parent().remove();
                $(document).ready(function(){
                    $.notify({
                        message: 'User deleted successfully.'
                    },{
                        type: 'success'
                    });
                });
            }else{
              $('#deletepasswordError').html('You entered the wrong current password');
              return false;
            }
        },
        error: function () {
            $('#deletepasswordError').html("something went wrong please try later");
            return false;
        }
    });
});
 
    
       var table = $('#users-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: "{{ url('panel/user/array-data') }}",
            columns: [
               // {data: 'id', orderable: true, searchable: true },
                {data: 'email',orderable: true, searchable: true},
                {data: 'username',orderable: true, searchable: true},
                {data: 'first_name',orderable: true, searchable: true},
                {data: 'last_name',orderable: true, searchable: true},
                {data: 'phone_number', orderable: true, searchable: true},
                //{data: 'user_role', orderable: false},
                {data: 'status', orderable: false,searchable: true},
              //  {data: 'created_at', searchable: false },
              //  {data: 'updated_at', searchable: false },
                {data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                     next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
   $("#users-table thead .filters th").each(function (i) {
        if (i == 7) {
        } else {
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control" style="width:126px;" placeholder="Search ' + title + '" />');
        }
    });

    // DataTable

    // Apply the search
    table.columns().every(function () {
        var that = this;

        $('input', this.header()).on('keyup change', function () {
            var colid = that[0][0];
            activearray = ['a', 'ac', 'act', 'acti', 'activ', 'active'];
            deactivearray = ['i', 'in', 'ina', 'inac', 'inact', 'inacti', 'inactiv', 'inactive'];
            var newval = '';
            if (colid == 4) {
                if ($.inArray(this.value, activearray) != -1) {
                    newval = 1;
                }
                if ($.inArray(this.value, deactivearray) != -1) {
                    newval = 0;
                }
            }
            if (that.search() !== this.value) {
                if (newval == '') {
                    newval = this.value;
                }
                that
                        .search(newval)
                        .draw();
            }
        });
    });
</script>
@endpush('scripts')
