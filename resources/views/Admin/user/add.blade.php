@extends('Admin.master_layout.master')

@section('title', 'Create Expert')

@section('breadcum')
    / <a href="{{ url('panel/user') }}">Expert List</a>  / Create Expert
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create Expert</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/user/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">

                        <div class="form-group <?php  echo $errors->first('first_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('First Name', 'First Name',array('class'=>'control-label')) !!}
                            {{ Form::text('first_name', null,array('class' => 'form-control', 'id' => 'first_name')) }}
                            <span class="text-danger" >{{ $errors->first('first_name') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('last_name') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Last Name', 'Last Name',array('class'=>'control-label')) !!}
                            {{ Form::text('last_name', null, array('class' => 'form-control', 'id' => 'last_name')) }}
                            <span class="text-danger" >{{ $errors->first('last_name') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('username') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('User Name', 'User Name',array('class'=>'control-label')) !!}
                            {{ Form::text('username', null, array('class' => 'form-control', 'id' => 'username')) }}
                            <span class="text-danger" >{{ $errors->first('username') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('email') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('email', 'Email',array('class'=>'control-label')) !!}
                            {{ Form::text('email', null, array('class' => 'form-control', 'id' => 'email')) }}
                            <span class="text-danger" >{{ $errors->first('email') }}</span>
                        </div>

                        <div class="form-group <?php  echo $errors->first('phone_number') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Phone', 'Phone',array('class'=>'control-label')) !!}
                            {{ Form::text('phone_number', null, array('class' => 'form-control', 'id' => 'phone')) }}
                            <span class="text-danger" >{{ $errors->first('phone_number') }}</span>
                        </div>
                        
                        <!-- <div class="form-group <?php  //echo $errors->first('user_role') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('user_role', 'Role',array('class'=>'control-label')) !!}
                            {!! Form::select('user_role', $user_role, null, array('class' => 'form-control','placeholder'=>'Select user role')) !!}
                            <span class="text-danger" >{{ $errors->first('user_role') }}</span>
                        </div> -->
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/panel/user') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection