@extends('Admin.master_layout.master')

@section('title', 'View Order')

@section('breadcum')
/ <a href="{{ url('panel/orders') }}">Orders List</a>  / View Order 
@endsection

@section('content')

<!-- @foreach ($errors->all() as $error)
<p class="alert alert-warning">{{ $error }}</p>
@endforeach
<span class="errormessage"></span> -->
<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
            @endif
        </div>


        <div class="right_0" role="main" style="min-height: 1102px;">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Orders Invoice </h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <section class="content invoice">
                                    <!-- title row -->
                                    <div class="row">
                                        <div class="col-xs-12 invoice-header">
                                            <div class="pull-left">
                                                <h1 class="invoice_logo">
                                                    <i class="fa fa-adjust"></i> Ord<span class="logo-s">er</span>
                                                </h1>
                                            </div>
                                            <div class="pull-right duedate"> 
                                                <h3>INVOICE #{{ $PaymentOrder->id }}</h3>
                                                <p class="text-right">Date: <strong>{{ Carbon\Carbon::parse($PaymentOrder->created_at)->format('d/m/Y') }}</strong></p>



                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <hr>
                                  
                                    <!-- info row -->
                                    <div class="row invoice-info">
                                        <!-- /.col -->
                                        <div class="col-sm-4 col-xs-6 invoice-col">
                                            <p class="invoice_to_details">To:</p>
                                            <address>        
                                                <strong>{{ $getUsernamer['username'] }}</strong> <br>
                                                @if($response['status'] == 'succeeded')
                                               @if($response['source']['address_city'] != '') {{ $response['source']['address_city'] }} <br> @endif

                                                @if($response['source']['address_country'] != ''){{ $response['source']['address_country'] }} <br> @endif
                                                @if($response['source']['address_line2'] != '') {{ $response['source']['address_line2'] }} <br> @endif

                                                @if($response['source']['address_state'] != '') {{ $response['source']['address_state'] }} <br> @endif
                                                @if($response['source']['address_zip'] != '') {{ $response['source']['address_zip'] }} <br> @endif
                                                {{ $response['source']['country'] }} <br>
                                                @endif
                                                <a style="color: #36c6d3;" href="mailto:"{{ $getUsernamer['email'] }}">{{ $getUsernamer['email'] }}</a>
                                                 
                                            </address>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-4 col-xs-6 invoice-col">
                                             <p class="invoice_to_details">Payment Details:</p>
                                           @if($response['status'] == 'succeeded')
                                           <b>Charge id : </b> {{ $response['id'] }} <br>
                                           <b>Card id : </b> {{ $response['source']['id'] }} <br>
                                           <b>Amount : </b> {{ $response['amount'] }} <br>
                                           <b>Balance Transaction : </b> {{ $response['balance_transaction'] }} <br>
                                           <b>Currency : </b> {{ $response['currency'] }} <br>
                                           <b>Customer : </b> {{ $response['customer'] }} <br>
                                           <b>Card Brand : </b> {{ $response['source']['brand'] }} <br>
                                           <b>Exp Month : </b> {{ $response['source']['exp_month'] }} <br>
                                           <b>Exp Year : </b> {{ $response['source']['exp_year'] }} <br>
                                           <b>Funding : </b> {{ $response['source']['funding'] }} <br>
                                           @endif
                                            
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
 <br/><br/>
@if($response['description'] != '') <b>Description:</b> {{ $response['description'] }} @endif
 <br/><br/>
                                    <!-- Table row -->
                                    <div class="row">
                                        <div class="col-xs-12 table">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th class="w5proc">Expert Name</th>
                                                        <th class="w5proc">Category</th>
                                                        <th class="w5proc">Booking Date</th>
                                                        <th class="w5proc">Start Time</th>
                                                        <th class="w5proc">End Time  </th>
                                                        <!-- <th class="w5proc">title</th> -->
                                                        <th class="w5proc">Description</th>
                                                        <th class="w5proc">Date</th>
                                                        
                                                        
<!--                                                        <th class="w50proc">Price</th>
                                                        <th class="w50proc">Pay Amount</th>-->
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td> {{ $getExpertName }}</td>
                                                        <td>{{ $getCategoryName }}</td>
                                                        <td>
                                                {{ Carbon\Carbon::parse($Appointment->booking_date)->format('d/m/Y') }} 

                                                        </td>
                                                        <td>
                                              {{ $Appointment->start_time }}
                                                        </td>
                                                        <td>
                                                {{ $Appointment->end_time }}
                                                        </td>
                                                        <!-- <td>
                                                {{ $Appointment->title }}  
                                                        </td> -->
                                                        <td>
                                                {{ $Appointment->description }}
                                                        </td>
                                                    <td>{{ Carbon\Carbon::parse($Appointment->created_at)->format('d/m/Y') }} </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->

                                    <div class="row">
                                         
                                        <div class="col-xs-4 pull-right total_invoice">
                                            <p class="lead">Price Due</p>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody>
                                                        <tr style="border-top:none !important;">
                                                            <th style="width:50%">Price:</th>
                                                            <td class="text-right">${{ $PaymentOrder->amount }}</td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <th>Pay Amount:</th>
                                                            <td class="text-right"><p class="grand_total">${{ $PaymentOrder->amount }}</p></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                         
                                    </div>
                                    <!-- /.row -->
                                    
                                    <!-- this row will not appear when printing -->
                                    
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('footer')
@parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>

@endpush('scripts')