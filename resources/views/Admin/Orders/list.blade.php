@extends('Admin.master_layout.master')

@section('title', 'Orders List')

@section('breadcum')
     / Orders List 
@endsection

@section('content')
<div class="">

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Orders Info</h4>
                   <!--  <a  href="{{ url('admin/donate/add') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        {{ trans('Add Item') }}
                    </a> -->
                   
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                     {!! Form::open(array('url' => ('panel/orders/updatemultirecode'),'method'=>'POST', 'files'=>true)) !!}
                   <table id="data-table" class="table table-bordered">
                        <thead>
                        <tr>
                             <th>
                          <input type="checkbox" id="checkAll" />
                        </th>
                            <th>Id</th>
                            <th>Charge Id</th>
                            <th>Name</th>
                            <th>Customer Id</th>
                            <th>Transaction</th>
                           <!--  <th>Currency</th> -->
                            <th>Card Brand</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                     <div id="showmultiaction" style="display: none;">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    <select name="action" class="form-control" required="">
                    <!-- <option value="active">Active</option>
                    <option value="inactive">InActive</option> -->
                    <option value="delete">Delete</option>
                   </select>
                </div>
                
                {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                </div>
                 {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>

<script type="text/javascript">
   
        $('#data-table').DataTable({
            serverSide: true,
            processing: true,
            stateSave: true,
            "order": [],
            ajax: "{{ url('/panel/orders/array-data') }}",
            columns: [
             {data: 'checkbox',orderable: false},
                {data: 'id',name: 'payment_order.id',orderable: false, searchable: true},
                {data: 'charge_id',name: 'payment_order.charge_id',orderable: false, searchable: true},
                {data: 'user_id',name: 'appuser.username',orderable: false, searchable: true},
                {data: 'customer',name: 'payment_order.customer',orderable: false, searchable: true},
                {data: 'balance_transaction',name: 'payment_order.balance_transaction',orderable: false},
                // {data: 'currency', orderable: true},
                {data: 'brand', name: 'payment_order.brand',orderable: false},
                {data: 'status',name: 'status', orderable: false},
                {data: 'action', name: 'action',orderable: false},
            ],
            "orders": {
                "paginate": {
                    next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
 $("#checkAll").change(function () {

      $("input:checkbox").prop('checked', $(this).prop("checked"));
      if($(this).prop("checked") == true){
        $("#showmultiaction").show();
      }else if($("input[name='checkaction[]']").prop("checked") == true){
        $("#showmultiaction").show();
      }else{
        $("#showmultiaction").hide();
      }
      
});
function onclickcheck(id){
  var a = $("input[name='checkaction[]']");
  if(a.filter(":checked").length > 0){
      $("#showmultiaction").show();
  }else{
    $("#showmultiaction").hide();
  }
   //  if(a.length == a.filter(":checked").length){
   //      alert('all checked');
   //  }
   // if($("input[name='checkaction[]']").prop("checked") == true){
   //      $("#showmultiaction").show();
   // }else if($("#checkaction"+id).prop("checked") == true){
   //      $("#showmultiaction").show();
   // }else if($("input[name='checkaction[]']").prop("checked") == true){
   //  //alert($("input[name='checkaction[]']").prop("checked"));
   //       //$("#showmultiaction").hide();
   // }
}
</script>
@endpush('scripts')