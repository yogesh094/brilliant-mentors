@extends('Admin.master_layout.master')

@section('title', 'General setting')

@section('breadcum')
     / Setting
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>General Setting</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/general-setting/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                        
                        @foreach($dataSetting as $key=>$field)
                            @if($field->slug == "appname")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            
                            @if($field->slug == "firebug_key")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::textarea('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug,'size'=>'3x3')) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            
                            @if($field->slug == "sandbox_pem")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {!! Form::File('setting['.$field->id.']['.$field->slug.']', ['class'=>'form-control certificate', 'placeholder'=>'Certificate','id' => $field->slug]) !!}
                                    <?php  
                                       $url = url('public/uploads/general_setting');
        
                                        if(!empty($field->value)) {
                                            echo "<span>".$url."/".$field->value."</span>";
                                        }
                                    ?>
                                </div>
                            @endif
                            
                            @if($field->slug == "live_pem")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {!! Form::File('setting['.$field->id.']['.$field->slug.']', ['class'=>'form-control certificate', 'placeholder'=>'Certificate','id' => $field->slug]) !!}
                                    <?php  
                                       $url = url('public/uploads/general_setting');
        
                                        if(!empty($field->value)) {
                                            echo "<span>".$url."/".$field->value."</span>";
                                        }
                                    ?>
                                </div>
                            @endif
                            
                            @if($field->slug == "env")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {!! Form::select('setting['.$field->id.']['.$field->slug.']',["0"=>"Development","1"=>"Live"], $field->value, array('class' => 'form-control','placeholder'=>'Select user environment')) !!}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "currency")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "start_time")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "end_time")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "email")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "address")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::textarea('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug,'size'=>'3x3')) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "facebook")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "twitter")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "google")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "video-consultaion")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {!! Form::File('setting['.$field->id.']['.$field->slug.']', ['class'=>'form-control certificate', 'placeholder'=>'Certificate','id' => $field->slug]) !!}
                                    <?php  
                                       $url = url('public/uploads/slider');
        
                                        if(!empty($field->value)) {
                                            echo "<span>".$url."/".$field->value."</span>";
                                        }
                                    ?>
                                </div>
                            @endif
                             @if($field->slug == "reschedule")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                             @if($field->slug == "refund")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            @if($field->slug == "reschedule-payment")
                                <div class="form-group">
                                    {!! Form::label($field->title, $field->title,array('class'=>'control-label')) !!}
                                    {{ Form::text('setting['.$field->id.']['.$field->slug.']', !empty($field->value)?$field->value:'',array('class' => 'form-control', 'id' => $field->slug)) }}
                                    <span class="text-danger" ></span>
                                </div>
                            @endif
                            
                        @endforeach

                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/panel/general-setting') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection