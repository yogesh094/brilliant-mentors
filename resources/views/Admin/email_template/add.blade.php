@extends('Admin.master_layout.master')

@section('title', 'Create Email Template')

@section('breadcum')
    / <a href="{{ url('panel/email-template') }}">List</a> / Create Email Template
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create Email Template</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/email-template/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                    <div class="form-group">
                        {!! Form::label('Language', 'Language') !!}
                        
                        <select name="language_id" class="form-control" id="language_id" required="">
                            <option value="">Select Language</option>
                            @if(count($languages) > 0)
                            @foreach($languages as $key => $lang)
                            <option value="{{ $lang['id'] }}">{{ $lang['name'] }}</option>
                            @endforeach

                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('language_id') }}</span>
                    </div>
                        <div class="form-group">
                            {!! Form::label('Title', 'Title : ',array('class'=>'control-label')) !!}

                            <input type="text" name="title" class="form-control" required >

                            <span class="text-danger" >{{ $errors->first('title') }}</span>
                        </div>


                       <div class="form-group ">
                            {!! Form::label('Subject', 'Subject : ',array('class'=>'control-label')) !!}
                             <input type="text" name="subject" class="form-control" required >
                            <span class="text-danger" >{{ $errors->first('subject') }}</span>
                        </div>

                        <div class="form-group ">
                            {!! Form::label('Body', 'Body : ',array('class'=>'control-label')) !!}
                             
                             <textarea name="body" class="form-control" id="summary-ckeditor" rows="15"></textarea>

                            <span class="text-danger" >{{ $errors->first('body') }}</span>
                        </div>

                       <div class="form-group">
                            {!! Form::label('status', 'Status:')   !!}
                            {!! Form::radio('status', '1',['checked' => 'checked'],['class' => 'flat']) !!} Active
                            {!! Form::radio('status', '0','',['class' => 'flat']) !!} InActive
                            
                            
                        </div>

                        
                       
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/panel/email-template') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    //CKEDITOR.replace( 'summary-ckeditor' );
    CKEDITOR.replace('summary-ckeditor', {
    allowedContent:true,
});
</script>
    @parent
@endsection