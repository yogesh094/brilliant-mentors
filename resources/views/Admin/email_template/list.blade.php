@extends('Admin.master_layout.master')

@section('title', 'Email Template List')

@section('breadcum')
     / Email Template List 
@endsection

@section('content')
<div class="">
    
    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Email Template</h4>
                    <a href="{{ url('panel/email-template/create') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add Email Template
                    </a>
                   
                     <div class="clearfix"></div>
                </div>
                <table id="email-template-table" class="table table-bordered">
                    <thead>
                    <tr>
                       
                        <th>ID</th>
                        <th>title</th>
                        <th>subject</th>
                        <th>Status</th>
                        <th>Action</th>
                       
                    </tr>
                    </thead>
                </table>

                
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">
    
    $(document).on("click", '.delete-btn', function(event) { 
    var id = $(this).attr('data-id');
    $('#select-delete').val(id);
});

$('#email-template-table').DataTable({
            serverSide: true,
            processing: true,
            stateSave: true,
            ajax: "{{ url('/panel/email-template/array-data') }}",
            columns: [
                {data: 'id',orderable: true},
                {data: 'title',orderable: true, searchable: true},
                {data: 'subject',orderable: true, searchable: true},
                {data: 'status', orderable: true},
                {data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                     next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
   
</script>
@endpush('scripts')
