@extends('Admin.master_layout.master')

@section('title', 'Slider List')

@section('breadcum')
     / Slider List 
@endsection

@section('content')
<div class="">
    <?php
    /*
     <div class="page-title">
        <div class="title_left">
            <h3>Users</h3>
            <p>You can manage users here.</p>
            </p>
        </div>
    </div>
    */
    ?>

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Slider</h4>
                    <a href="{{ url('panel/slider/create') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add Slider
                    </a>
                    <!--
                <a href="{{ url('services/download/excel') }}" class="btn btn-sm btn-xs btn-warning pull-right">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export
                    </a>
                -->
                     
                     <div class="clearfix"></div>
                </div>
                <table id="cms-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <?php
                        /*
                        <th>#</th>
                        */
                        ?>
                        <th>id</th>
                        <th>Slider</th>
                        <th>title</th>
                        <th>Status</th>
                        <th>Action</th>
                       

                        <?php
                        /*
                        <th>Created On</th>
                        <th>Updated On</th>
                        <th>Action</th>
                        */
                        ?>
                    </tr>
                    </thead>
                </table>

                
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">
   
         $('#cms-table').DataTable({
            serverSide: true,
            processing: true,
            stateSave: true,
            ajax: "{{ url('/panel/slider/array-data') }}",
            columns: [
                {data: 'id',orderable: true, searchable: true},
                {data: 'slider_image',orderable: true, searchable: true},
                {data: 'title',orderable: true, searchable: true},
                {data: 'status', orderable: true},
                {data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                     next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
   
</script>
@endpush('scripts')
