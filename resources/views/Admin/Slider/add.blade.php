@extends('Admin.master_layout.master')

@section('title', 'Create Slider')

@section('breadcum')
     / <a href="{{ url('panel/slider') }}">Slider List</a> / Create Slider
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create Slider</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/slider/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                    <div class="form-group">
                        {!! Form::label('Language', 'Language') !!}
                        
                        <select name="language_id" class="form-control" id="language_id" required="">
                            <option value="">Select Language</option>
                            @if(count($languages) > 0)
                            @foreach($languages as $key => $lang)
                            <option value="{{ $lang['id'] }}">{{ $lang['name'] }}</option>
                            @endforeach

                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('language_id') }}</span>
                    </div>
                        <div class="form-group">
                            {!! Form::label('Title', 'Title : ',array('class'=>'control-label')) !!}

                            <input type="text" name="title" class="form-control" required >

                            <span class="text-danger" >{{ $errors->first('title') }}</span>
                        </div>


                       <div class="form-group ">
                            {!! Form::label('Name', 'Name : ',array('class'=>'control-label')) !!}
                             <input type="text" name="name" class="form-control" required >
                            <span class="text-danger" >{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group ">
                            {!! Form::label('Description', 'Description : ',array('class'=>'control-label')) !!}
                             <textarea name="description" class="form-control"></textarea>
                            <span class="text-danger" >{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group ">
                            {!! Form::label('Slider', 'Slider : ',array('class'=>'control-label')) !!}
                             <input type="file" name="slider_image" class="form-control">
                            <span class="text-danger" >{{ $errors->first('slider_image') }}</span>
                        </div>
                        <div class="clearfix"></div><br>
                        <div class="form-group">
                            {!! Form::label('type', 'Type:')   !!}

                            {!! Form::radio('type', '1',['checked' => 'checked'],['class' => 'flat']) !!} Advertisement
                            {!! Form::radio('type', '2','',['class' => 'flat']) !!} App Slider
                            {!! Form::radio('type', '3','',['class' => 'flat']) !!} Web Slider
                            
                        </div>
                        <br>
                       <div class="form-group">
                            {!! Form::label('status', 'Status:')   !!}

                            {!! Form::radio('status', '0',['checked' => 'checked'],['class' => 'flat']) !!} InActive
                            {!! Form::radio('status', '1','',['class' => 'flat']) !!} Active
                            
                        </div>

                        
                       
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/panel/slider') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    //CKEDITOR.replace( 'summary-ckeditor' );
    CKEDITOR.replace('summary-ckeditor', {
   allowedContent:true,
});
</script>
    @parent
@endsection