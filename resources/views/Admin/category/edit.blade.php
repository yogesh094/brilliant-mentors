@extends('Admin.master_layout.master')

@section('title', 'Edit Category')

@section('breadcum')
     / <a href="{{ url('panel/category') }}">Category List</a> / Edit Category 
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">

            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <!-- <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul> -->
                </div>
            @endif
            
            <!-- if there are creation errors, they will show here -->
            <span class="errormessage"></span>
        </div>    
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
                {!! Form::open(array('url' => ('/panel/category/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="box-body">

                    {!! Form::hidden('id',"$category->id",['class'=>'form-control']) !!}
                    <div class="form-group">
                        {!! Form::label('Language', 'Language') !!}
                        
                    <select name="language_id" class="form-control" required="" id="language_id" onchange="getLanguageID(this.value);">
                            <option value="">Select Language</option>
                            @if(count($languages) > 0)
                            @foreach($languages as $key => $lang)
                            <option value="{{ $lang['id'] }}" @if(isset($category->language_id) && $category->language_id == $lang['id'])selected=""@endif>{{ $lang['name'] }}</option>
                            @endforeach

                            @endif
                    </select>

                        <span class="text-danger">{{ $errors->first('language_id') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('Category Name', 'Category Name') !!}
                        {!! Form::text('name',"$category->category",['class'=>'form-control']) !!}

                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('Sub Category', 'Sub Category') !!}
                        
                        <select name="is_parent" class="form-control" id="is_parent" required="">
                            <option value="0" selected="">None</option>
                            @if(count($parentCategory) > 0)
                            @foreach($parentCategory as $key => $categorys)
                            <option value="{{ $categorys->id }}" @if($category->is_parent == $categorys->id)  selected="" @endif>{{ $categorys->category }}</option>
                            @endforeach
                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('is_parent') }}</span>
                    </div> 
                     <div class="form-group">
                        {!! Form::label('is_seasonal', 'Is Seasonal:')   !!}
                        @if ($category->is_seasonal == 1)
                        {!! Form::checkbox('is_seasonal', '1',true,['class' => 'flat']) !!}
                        @else
                        {!! Form::checkbox('is_seasonal', '1',false,['class' => 'flat']) !!}
                        @endif
                    </div>
                   <div class="form-group <?php  echo $errors->first('status') !== '' ? 'has-error' : '';?>">
                            {!! Form::label('Status', 'Status',array('class'=>'control-label')) !!}
                           @if ($category->status == 1)
                            {!! Form::radio('status', '1',['checked' => 'checked'],['class' => 'minimal']) !!} Active
                            {!! Form::radio('status', '0','',['class' => 'minimal']) !!} InActive
                            @else
                            {!! Form::radio('status', '1','',['class' => 'minimal']) !!} Active
                            {!! Form::radio('status', '0',['checked' => 'checked'],['class' => 'minimal']) !!} InActive
                            @endif
                            <span class="text-danger" >{{ $errors->first('Status') }}</span>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('/panel/category') }}">Cancel</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection
@push('scripts')
<script type="text/javascript">

function getLanguageID(id){
    $.ajax({
            type : 'get',
            url:  "{{ url('/panel/category/getParentCategory') }}",
            data :  'language_id='+ id, //Pass $id
            success : function(data){
              $('#is_parent').html(data);
            }
        });

}
</script>
@endpush('scripts')