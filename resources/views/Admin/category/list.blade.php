@extends('Admin.master_layout.master')

@section('title', 'Category List')

@section('breadcum')
     / Category List 
@endsection

@section('content')
<div class="">

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Category Info</h4>
                    <a  href="{{ url('panel/category/add') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        {{ trans('Add Category') }}
                    </a>
                   
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    {!! Form::open(array('url' => ('panel/category/updatemultirecode'),'method'=>'POST', 'files'=>true)) !!}
                   <table id="data-table" class="table table-bordered">
                        <thead>
                        <tr>
                           <!--  <th>Language</th> -->
                            <th>
                          <input type="checkbox" id="checkAll" />
                        </th>
                            <th>ID</th>
                            <th>Category Name</th>
                            <th>Seasonal</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                         <tbody>
                            @if(count($category) > 0)
                            @foreach($category as $key => $value)
                            <tr>
                                <td><input class='' type='checkbox' id='checkaction{{ $value->id }}' name='checkaction[]' value="{{ $value->id }}" onclick='onclickcheck("{{ $value->id }}");'></td>
                                <td>{{ $value->id }}</td>
                                <td><a href="{{ url('panel/category/edit/') }}/{{ $value->id }}" class="btn btn-link btn-sm" title='Edit'>{{ $value->category }}</a></td>
                                <td>
                                    @if($value->is_seasonal == 1)
                                    <span class="badge bg-blue">Seasonal</span>
                                    @else
                                    <span class="badge bg-black">No Seasonal</span>
                                    @endif
                                </td>
                                <td> 
                                    @if($value->status == 1)
                                        <span class="badge bg-green">Active</span>
                                    @else
                                        <span class="badge bg-red">Inactive</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('panel/category/edit/') }}/{{ $value->id }}" class="btn btn-success btn-sm" title='Edit'><i class="fa fa-pencil"></i></a>
                                    <a href="{{ url('panel/category/delete/') }}/{{ $value->id }}" class="btn btn-danger btn-sm" onclick="if (confirm('Are You Sure? Do You Want To Delete?')) {
                                         return true;
                                         } else {
                                         return false;
                                         }" title='Delete'>
                                         <i class="fa fa-trash"></i>
                                     </a>
                                </td>
                            </tr>
                            @if(count(\App\Models\Category::where('is_parent',$value->id)->get()) > 0)
                            @foreach(\App\Models\Category::where('is_parent',$value->id)->get() as $per => $parentCategoty)
                            <tr>
                                <td><input class='' type='checkbox' id='checkaction{{ $parentCategoty->id }}' name='checkaction[]' value="{{ $parentCategoty->id }}" onclick='onclickcheck("{{ $parentCategoty->id }}");'></td>
                                <td>{{ $parentCategoty->id }}</td>
                                <td><a href="{{ url('panel/category/edit/') }}/{{ $parentCategoty->id }}" class="btn btn-link btn-sm" title='Edit'>----{{ $parentCategoty->category }}</a></td>
                                <td>
                                    @if($parentCategoty->is_seasonal == 1)
                                    <span class="badge bg-blue">Seasonal</span>
                                    @else
                                    <span class="badge bg-black">No Seasonal</span>
                                    @endif
                                </td>
                                <td> 
                                    @if($parentCategoty->status == 1)
                                        <span class="badge bg-green">Active</span>
                                    @else
                                        <span class="badge bg-red">Inactive</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('panel/category/edit/') }}/{{ $parentCategoty->id }}" class="btn btn-success btn-sm" title='Edit'><i class="fa fa-pencil"></i></a>
                                    <a href="{{ url('panel/category/delete/') }}/{{ $parentCategoty->id }}" class="btn btn-danger btn-sm" onclick="if (confirm('Are You Sure? Do You Want To Delete?')) {
                                         return true;
                                         } else {
                                         return false;
                                         }" title='Delete'>
                                         <i class="fa fa-trash"></i>
                                     </a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                           
                            @endforeach 
                            @endif
                         </tbody>
                        </table>
                        <div id="showmultiaction" style="display: none;">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    <select name="action" class="form-control" required="">
                    <option value="active">Active</option>
                    <option value="inactive">InActive</option>
                    <option value="delete">Delete</option>
                   </select>
                </div>
                
                {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                </div>
                 {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>

<script type="text/javascript">
   
        // $('#data-table').DataTable({
        //     serverSide: true,
        //     processing: true,
        //     "order": [[1,"asc"]],
        //     ajax: "{{ url('/admin/category/array-data') }}",
        //     columns: [
        //         {data: 'checkbox',orderable: false},
        //         {data: 'id',orderable: true},
        //         {data: 'category',orderable: true, searchable: true},
        //         {data: 'is_seasonal',orderable: true, searchable: false},
        //         {data: 'status', orderable: true},
        //         {data: 'action', orderable: true},
        //         //{data: 'charges', searchable: true },
        //        // {data: 'updated_at', searchable: false },
        //       //  {data: 'action', name:'action', searchable: false, orderable: false }
        //     ],
        //     "category": {
        //         "paginate": {
        //             next: '<i class="fa fa-angle-right"></i>',
        //             previous: '<i class="fa fa-angle-left"></i>'
        //         }
        //     }
        // });

        $('#data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'info': true,
            'autoWidth': false,
            stateSave: true,
            columnDefs: [
                // {orderable: false, targets: 3},
                // {orderable: false, targets: 2},
                // {orderable: true, targets: 1},
                // {orderable: true, targets: 0}
            ]
        })
 $("#checkAll").change(function () {
      $("input:checkbox").prop('checked', $(this).prop("checked"));
      if($(this).prop("checked") == true){
        $("#showmultiaction").show();
      }else if($("input[name='checkaction[]']").prop("checked") == true){
        $("#showmultiaction").show();
      }else{
        $("#showmultiaction").hide();
      }
      
});
function onclickcheck(id){
   var a = $("input[name='checkaction[]']");
  if(a.filter(":checked").length > 0){
      $("#showmultiaction").show();
  }else{
    $("#showmultiaction").hide();
  }
}
</script>
@endpush('scripts')