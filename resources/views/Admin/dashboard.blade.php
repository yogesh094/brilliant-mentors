@extends('Admin.master_layout.master')

@section('title', 'Dashboard')

@section('breadcum')
     / Dashboard 
@endsection

@section('content')

<!--Action boxes-->
<div class="container-fluid">
    <div class="quick-actions_homepage">
      <h1>Welcome,  {{ ucfirst(Auth::user()->first_name) }} </h1>
      	<div class="row top_tiles">
                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-red">
                                <div class="icon"><i class="fa fa-question"></i></div>
                                <h3 class="sizeofont">TOTAL QUESTION</h3>
                                <div class="count">
                                  @if(Auth::user()->user_role == 1)
                                    {{ App\Models\Question::count() }}
                                  @else
                                    {{ App\Models\Question::where('expert_id',Auth::user()->id)->count() }}
                                  @endif
                                </div>
                                
                                <p><a class="tile-white-color" href="{{ url('panel/question') }}">More Info</a></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-red">
                                <div class="icon"><i class="fa fa-question"></i></div>
                                <h3 class="sizeofont">COMPLETED QUESTION</h3>
                                <div class="count">
                                  @if(Auth::user()->user_role == 1)
                                    {{ App\Models\Question::whereNotNull('answer')->count() }}
                                  @else
                                    {{ App\Models\Question::where('expert_id',Auth::user()->id)->whereNotNull('answer')->count() }}
                                  @endif
                                </div>
                                
                                
                                <p><a class="tile-white-color" href="{{ url('panel/question') }}">More Info</a></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-red">
                                <div class="icon"><i class="fa fa-question"></i></div>
                                <h3 class="sizeofont">PENDING QUESTION</h3>
                                <div class="count">
                                  @if(Auth::user()->user_role == 1)
                                    {{ App\Models\Question::whereNull('answer')->count() }}
                                  @else
                                    {{ App\Models\Question::where('expert_id',Auth::user()->id)->whereNull('answer')->count() }}
                                  @endif
                                </div>
                                                               
                                <p><a class="tile-white-color" href="{{ url('panel/question') }}">More Info</a></p>
                            </div>
                        </div>
                      </div>
                      <div class="row top_tiles">
                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-purple">
                                <div class="icon"><i class="fa fa-calendar"></i></div>
                              <h3 class="sizeofont">TOTAL APPOINTMENT</h3>
                                <div class="count">
                                @if(Auth::user()->user_role == 1)
                                    {{ App\Models\Appointment::count() }}
                                  @else
                                    {{ App\Models\Appointment::where('expert_id',Auth::user()->id)->count() }}
                                  @endif
                              </div>
                                
                                <p><a class="tile-white-color" href="{{ url('panel/appointment') }}">More Info</a></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">

                            <div class="tile-stats tile-white bg-purple">
                                <div class="icon"><i class="fa fa-calendar"></i></div>
                                <h3 class="sizeofont">COMPLETED APPOINTMENT</h3>
                                <div class="count">
                                @if(Auth::user()->user_role == 1)
                                    {{ App\Models\Appointment::where('status',2)->count() }}
                                  @else
                                    {{ App\Models\Appointment::where('expert_id',Auth::user()->id)->where('status',2)->count() }}
                                  @endif
                              </div>
                                
                                <p><a class="tile-white-color" href="{{ url('panel/appointment') }}">More Info</a></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-purple">
                                <div class="icon"><i class="fa fa-calendar"></i></div>
                                <h3 class="sizeofont">PENDING APPOINTMENT</h3>
                                <div class="count">
                                @if(Auth::user()->user_role == 1)
                                    {{ App\Models\Appointment::whereNotIn('status',[2])->count() }}
                                  @else
                                    {{ App\Models\Appointment::where('expert_id',Auth::user()->id)->whereNotIn('status',[2])->count() }}
                                  @endif
                              </div>
                               
                                <p><a class="tile-white-color" href="{{ url('panel/appointment') }}">More Info</a></p>
                            </div>
                        </div>
                      </div>
                      <div class="row top_tiles">
                        @if(Auth::user()->user_role == 1)
                         <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-blue-sky">
                                <div class="icon"><i class="fa fa-shopping-cart"></i></div>
                                <h3 class="sizeofont">ORDERS</h3>
                                <div class="count">{{ App\Models\PaymentOrder::count() }}</div>
                                
                                <p><a class="tile-white-color" href="{{ url('panel/orders') }}">More Info</a></p>
                            </div>
                        </div>

                        <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-blue-sky">
                                <div class="icon"><i class="fa fa-users"></i></div>
                                <h3 class="sizeofont">USERS</h3>
                                <div class="count">{{ App\Models\AppUser::count() }}</div>
                                
                                <p><a class="tile-white-color" href="{{ url('panel/appuser') }}">More Info</a></p>
                            </div>
                        </div>
                        @endif
                    

      @if(Auth::user()->user_role == 1)              
      
      	<div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
         <div class="tile-stats tile-white bg-blue-sky">
          <div class="icon"><i class="fa fa-users"></i></div>
          <h3 class="sizeofont"> EXPERT USERS </h3>
            <div class="count">{{ App\Models\User::count() }}</div>
            
             <p><a class="tile-white-color" href="{{ url('panel/user') }}">More Info</a></p>
           </div>
     </div>
         
      
      @endif
      </div>
    </div>
</div>
<!--End-Action boxes-->    

@endsection

@section('footer')
    @parent
@endsection
