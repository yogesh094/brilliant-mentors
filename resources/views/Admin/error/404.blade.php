@extends('Admin.master_layout.master')
@section('title', '404')

@section('breadcum')
     / 404
@endsection

@section('content')

 <div class="col-md-12">
                    <div class="col-middle text-center">
                        <div class="text-center">
                            <h1 class="error-number">404</h1>
                            <div>
                                <h2>Oops! You're lost.</h2>
                                <p>We can not find the page you're looking for. <a href="{{ url('/') }}" class="error_page_return">Return home</a> </p>
                                <div class="mid_center">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection

@section('footer')
    @parent
@endsection

@push('scripts')