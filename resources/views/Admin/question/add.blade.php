@extends('Admin.master_layout.master')

@section('title', 'Add Question')

@section('breadcum')
     / <a href="{{ url('panel/question') }}">Question List</a> / Add Question
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                </div>
            @endif
            <!-- if there are creation errors, they will show here -->
            <span class="errormessage"></span>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
                {!! Form::open(array('url' => ('panel/question/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('Language', 'Language') !!}
                        
                        <select name="language_id" class="form-control" id="language_id" required="">
                            <option value="">Select Language</option>
                            @if(count($languages) > 0)
                            @foreach($languages as $key => $lang)
                            <option value="{{ $lang['id'] }}">{{ $lang['name'] }}</option>
                            @endforeach

                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('language_id') }}</span>
                    </div>

                    <div class="form-group">
                        {!! Form::label('Expert', 'Expert') !!}
                        
                        <select name="expert_id" class="form-control" required="" >
                            <option value="">Select expert</option>
                            @if(count($User) > 0)
                            @foreach($User as $key => $users)
                            <option value="{{ $users['id'] }}">{{ $users['first_name'] }} {{ $users['last_name'] }}</option>
                            @endforeach

                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('expert_id') }}</span>
                    </div>

                    <div class="form-group">
                        {!! Form::label('Category', 'Category') !!}
                        
                        <select name="category_id" class="form-control" id="category_id">
                            <option value="">Select Category</option>
                            @if(count($Category) > 0)
                            @foreach($Category as $key => $Categorys)
                            <option value="{{ $Categorys['id'] }}">{{ $Categorys['category'] }}</option>
                            @endforeach

                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('category_id') }}</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('Sub Category', 'Sub Category') !!}
                        
                        <select name="parent_category" class="form-control" id="parent_category">
                            <option value="0">None</option>
                        </select>
                    </div>
                    <!--<div class="form-group">-->
                    <!--    {!! Form::label('Topic', 'Topic') !!}-->
                        
                    <!--    <select name="topic_id" class="form-control" id="topic_id">-->
                    <!--        <option value="">Select Topic</option>-->
                    <!--        @if(count($Topic) > 0)-->
                    <!--        @foreach($Topic as $key => $Topics)-->
                    <!--        <option value="{{ $Topics['id'] }}">{{ $Topics['topic'] }}</option>-->
                    <!--        @endforeach-->

                    <!--        @endif-->
                    <!--    </select>-->

                    <!--    <span class="text-danger">{{ $errors->first('topic_id') }}</span>-->
                    <!--</div>-->


                    <!-- key title  -->
                    <div class="form-group">
                        {!! Form::label('Question', 'Question') !!}
                        {!! Form::text('question',"",['class'=>'form-control']) !!}

                        <span class="text-danger">{{ $errors->first('question') }}</span>
                    </div>
                     <div class="form-group">
                        {!! Form::label('answer', 'Answer') !!}
                        {!! Form::textarea('answer',"",['class'=>'form-control','id' => 'answer-ckeditor']) !!}

                        <span class="text-danger">{{ $errors->first('answer') }}</span>
                    </div>
                     <div class="form-group">
                        {!! Form::label('is_private', 'Is Private:')   !!}
                        {!! Form::checkbox('is_private', '1',false,['class' => 'flat']) !!} 
                    </div>
                    <div class="form-group">
                        {!! Form::label('status', 'Status:')   !!}
                        {!! Form::radio('status', '1',true,['class' => 'minimal']) !!}  Active &nbsp;
                        {!! Form::radio('status', '0','',['class' => 'minimal']) !!}  InActive &nbsp;
                    </div>
                </div>

                <div class="box-footer">
                    {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('/panel/question') }}">Cancel</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#category_id').on('change', function (e) {
            var id = $(this).find(":selected").val();
            $.ajax({
                type : 'get',
                url:  "{{ url('/panel/question/getParentCategory') }}",
                data :  'id='+ id, //Pass $id
                success : function(response){

                    var $select = $('#parent_category');
                    $select.find('option').remove();
                    if(response == ''){
                        $select.append('<option value="0">None</option>');
                    }
                    
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value['id'] + '>' + value['category'] + '</option>'); // return empty
                    });
                
                }
            }); 
         });
        $('#language_id').on('change', function (e) {
            var id = $(this).find(":selected").val();
            $.ajax({
                type : 'get',
                url:  "{{ url('/panel/question/getCategoryByLanguage') }}",
                data :  'id='+ id, //Pass $id
                success : function(response){
                    var $select = $('#category_id');
                    $select.find('option').remove();
                    $select.append('<option value="">Select Category</option>');
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value['id'] + '>' + value['category'] + '</option>'); // return empty
                    });
                
                }
            }); 

            $.ajax({
                type : 'get',
                url:  "{{ url('/panel/question/getTopicByLanguage') }}",
                data :  'id='+ id, //Pass $id
                success : function(response){
                    var $select = $('#topic_id');
                    $select.find('option').remove();
                    $select.append('<option value="">Select Topic</option>');
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value['id'] + '>' + value['topic'] + '</option>'); // return empty
                    });
                
                }
            });   
        });
    });
</script>
@endpush('scripts')
@section('footer')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('answer-ckeditor', {
    allowedContent:true,
});
</script>
    @parent
@endsection
