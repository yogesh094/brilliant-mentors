@extends('Admin.master_layout.master')

@section('title', 'Edit Language')

@section('breadcum')
     / <a href="{{ url('panel/question') }}">Question List</a> / Edit Language 
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">

            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <!-- <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul> -->
                </div>
            @endif
            
            <!-- if there are creation errors, they will show here -->
            <span class="errormessage"></span>
        </div>    
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
                {!! Form::open(array('url' => ('panel/question/store'),'method'=>'POST', 'files'=>true)) !!}
                <?php   
                    if(Auth::user()->user_role != 1){
                        $disabled = 'disabled';
                        $hide ='hide';
                    }else{
                        $disabled = ''; 
                        $hide='';
                    }
                ?>
                <div class="box-body">

                    {!! Form::hidden('id',"$Question->id",['class'=>'form-control']) !!}

                    <div class="form-group">
                        {!! Form::label('Language', 'Language') !!}
                        
                        <select name="language_id" class="form-control" required="" id="language_id">
                            <option value="">Select Language</option>
                            @if(count($languages) > 0)
                            @foreach($languages as $key => $lang)
                            <option value="{{ $lang['id'] }}" @if(isset($Question->language_id) && $Question->language_id == $lang['id'])selected=""@endif>{{ $lang['name'] }}</option>
                            @endforeach

                            @endif
                        </select>
                        <span class="text-danger">{{ $errors->first('language_id') }}</span>
                    </div>
                    <div class="form-group <?php echo $hide;?>" >
                        {!! Form::label('Expert', 'Expert') !!}
                        
                        <select name="expert_id" class="form-control" required="">
                            <option value="">Select Expert</option>
                            @if(count($User) > 0)
                            @foreach($User as $key => $users)
                            <option value="{{ $users['id'] }}" @if(isset($Question->expert_id) && $Question->expert_id == $users['id'])selected=""@endif>{{ $users['first_name'] }} {{ $users['last_name'] }}</option>
                            @endforeach

                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('expert_id') }}</span>
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('Category', 'Category') !!}
                        
                        <select name="category_id" class="form-control" id="category_id" required="">
                            <option value="">Select Category</option>
                            @if(count($Category) > 0)
                            @foreach($Category as $key => $Categorys)
                            <option value="{{ $Categorys['id'] }}"@if(isset($Question->category_id) && $Question->category_id == $Categorys['id'])selected=""@endif>{{ $Categorys['category'] }}</option>
                            @endforeach

                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('category_id') }}</span>
                    </div>
                   <div class="form-group">
                        {!! Form::label('Sub Category', 'Sub Category') !!}
                        
                        <select name="parent_category" class="form-control" id="parent_category">
                            
                            @if(count($parentCategory) > 0)
                            @foreach($parentCategory as $key => $parentCategorys)
                            <option value="{{ $parentCategorys['id'] }}"@if(isset($Question->parent_category) && $Question->parent_category == $parentCategorys['id'])selected=""@endif>{{ $parentCategorys['category'] }}</option>
                            @endforeach
                            @else
                            <option value="0">None</option>
                            @endif
                        </select>
                    </div>

                    <!--<div class="form-group">-->
                    <!--    {!! Form::label('Topic', 'Topic') !!}-->
                        
                    <!--    <select name="topic_id" class="form-control" id="topic_id" required="">-->
                    <!--        <option value="">Select Topic</option>-->
                    <!--        @if(count($Topic) > 0)-->
                    <!--        @foreach($Topic as $key => $Topics)-->
                    <!--        <option value="{{ $Topics['id'] }}" @if(isset($Question->topic_id) && $Question->topic_id == $Topics['id'])selected=""@endif>{{ $Topics['topic'] }}</option>-->
                    <!--        @endforeach-->

                    <!--        @endif-->
                    <!--    </select>-->

                    <!--    <span class="text-danger">{{ $errors->first('topic_id') }}</span>-->
                    <!--</div>-->


                    <!-- key title  -->
                    <div class="form-group">
                        {!! Form::label('Question', 'Question') !!}
                        <!-- 
                        {!! Form::text('question',$Question->question,['class'=>'form-control']) !!} -->
                        <input type="text" name="question" class="form-control" value="{{ $Question->question }}" required="">
                        <span class="text-danger">{{ $errors->first('question') }}</span>
                    </div>
                     <div class="form-group">
                        {!! Form::label('answer', 'Answer') !!}
                        {!! Form::textarea('answer',$Question->answer,['class'=>'form-control','id' => 'answer-ckeditor']) !!}
                        <span class="text-danger">{{ $errors->first('answer') }}</span>
                    </div>
                     <div class="form-group">
                        {!! Form::label('is_private', 'Is Private:')   !!}
                        @if ($Question->is_private == 1)
                        {!! Form::checkbox('is_private', '1',true,['class' => 'flat']) !!}
                        @else
                        {!! Form::checkbox('is_private', '1',false,['class' => 'flat']) !!}
                        @endif
                    </div>
                    <div class="form-group <?php  echo $errors->first('status') !== '' ? 'has-error' : '';?>">
                        {!! Form::label('Status', 'Status',array('class'=>'control-label')) !!}
                       @if ($Question->status == 1)
                        {!! Form::radio('status', '1',['checked' => 'checked'],['class' => 'minimal']) !!} Active
                        {!! Form::radio('status', '0','',['class' => 'minimal']) !!} InActive
                        @else
                        {!! Form::radio('status', '1','',['class' => 'minimal']) !!} Active
                        {!! Form::radio('status', '0',['checked' => 'checked'],['class' => 'minimal']) !!} InActive
                        @endif
                        <span class="text-danger" >{{ $errors->first('Status') }}</span>
                    </div>
                    </div>
                </div>

                <div class="box-footer">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('/panel/question') }}">Cancel</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#category_id').on('change', function (e) {
            var id = $(this).find(":selected").val();
            $.ajax({
                type : 'get',
                url:  "{{ url('/panel/question/getParentCategory') }}",
                data :  'id='+ id, //Pass $id
                success : function(response){
                    var $select = $('#parent_category');
                    $select.find('option').remove();
                    if(response == ''){
                    $select.append('<option value="0">None</option>');
                    }
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value['id'] + '>' + value['category'] + '</option>'); // return empty
                    });
                
                }
            }); 
         });
        $('#language_id').on('change', function (e) {
            var id = $(this).find(":selected").val();
            $.ajax({
                type : 'get',
                url:  "{{ url('/panel/question/getCategoryByLanguage') }}",
                data :  'id='+ id, //Pass $id
                success : function(response){
                    var $select = $('#category_id');
                    $select.find('option').remove();
                    $select.append('<option value="">Select Category</option>');
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value['id'] + '>' + value['category'] + '</option>'); // return empty
                    });
                
                }
            }); 

            $.ajax({
                type : 'get',
                url:  "{{ url('/panel/question/getTopicByLanguage') }}",
                data :  'id='+ id, //Pass $id
                success : function(response){
                    var $select = $('#topic_id');
                    $select.find('option').remove();
                    $select.append('<option value="">Select Topic</option>');
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value['id'] + '>' + value['topic'] + '</option>'); // return empty
                    });
                
                }
            });   
        });
    });
</script>
@endpush('scripts')
@section('footer')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace('answer-ckeditor', {
    allowedContent:true,
});
</script>
    @parent
@endsection