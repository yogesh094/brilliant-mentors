@extends('Admin.master_layout.master')

@section('title', 'Question List')

@section('breadcum')
/ Question List 
@endsection

@section('content')
<div class="">

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Question Info</h4>

                    @if (Auth::user()->user_role == 1)
                    <a  href="{{ url('panel/question/add') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        {{ trans('Add Question') }}
                    </a>
                    @endif
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    {!! Form::open(array('url' => ('panel/question/updatemultirecode'),'method'=>'POST', 'files'=>true)) !!}
                    <table id="data-table" class="table table-bordered" style="width: 100%;">
                        <thead>
                            <tr>

                                <th style="width: 5% !important;">
                                    <input type="checkbox" id="checkAll" />
                                </th>
                                <th style="width: 2% !important;">ID</th>
                                <th style="width: 5% !important;">Expert Assign</th>
                                <th style="width: 30% !important;">Question</th>
                                <th style="width: 5% !important;">Answer</th>
                                <th style="width: 5% !important;">Status</th>
                                <th style="width: 25% !important;">Action</th>
                                <?php /*    <th>Created On</th>
                                  <th>Updated On</th>
                                 */
                                ?>
                                <?php
                                /*
                                  <th>Action</th>
                                 */
                                ?>
                            </tr>
                        </thead>
                    </table>

                    <div id="showmultiaction" style="display: none;">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <select name="action" class="form-control" required="">
                                <option value="active">Active</option>
                                <option value="inactive">InActive</option>
                                <option value="delete">Delete</option>
                            </select>
                        </div>

                        {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
@parent

<div id="ExperienceModel" class="file-zoom-dialog modal fade in" tabindex="-1" aria-labelledby="kvFileinputModalLabel" >
    <div class="modal-dialog modal-lg" role="document" style="width: 400px;">
        <div class="modal-content">
            <div class="modal-header">
                <div class="kv-zoom-actions pull-right">
                    <button type="button" class="btn btn-default btn-close" title="Close detailed preview" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button></div>
                <h3 class="modal-title">Expert Assign</h3>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class=" box-primary">
                        <!--  {!! Form::open(array('url' => ('admin/question/Assignquestion'),'method'=>'POST', 'files'=>true)) !!} -->
                        <div class="ajaxerrorreg"></div>
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="language_id" id="language_id" value="">
                        <input type="hidden" name="question" id="question" value="">
                        <div class="box-body">

                            <div class="form-group">
                                {!! Form::label('Assign', 'Assign') !!}

                                <select name="expert_id" id="expert_id" class="form-control">
                                    <option value="">Select Assign</option>
                                    @if(count($User) > 0)
                                    @foreach($User as $key => $users)
                                    <option value="{{ $users['id'] }}">{{ $users['first_name'] }} {{ $users['last_name'] }}</option>
                                    @endforeach

                                    @endif
                                </select>
                                <span class="text-danger">{{ $errors->first('expert_id') }}</span>
                            </div>
                            <div class="box-footer">
                                <!-- {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!} -->
                                <a class="btn btn-primary submit Assign" href="javascript:void(0)" > Submit </a>
                                <a class="btn btn-default btn-close" href="javascript:void(0)" data-dismiss="modal" aria-hidden="true">Cancel</a>
                                <div class="loading" style="display: none;"><img src="../img/loading.gif" style="width: 20px;"></div>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>

<script type="text/javascript">

var table = $('#data-table').DataTable({
    serverSide: true,
    processing: true,
    //iDisplayStart: {{ Session::get('AppQuestionPage') }},
    stateSave: true,
    autoWidth: false,
    "order": [[1, "asc"]],
    ajax: "{{ url('/panel/question/array-data') }}",
    columns: [
        //{data: 'id', orderable: true, searchable: true },
        {data: 'checkbox', orderable: false, "width": "5%"},
        {data: 'id', name: 'question.id', orderable: true, "width": "2%"},
        {data: 'expert_id', name: 'user.first_name', orderable: true, searchable: true, "width": "5%"},
        {data: 'question', name: 'question.question', orderable: true, searchable: true, "width": "30%"},
        {data: 'answer', name: 'answer', orderable: false, "width": "5%"},
        {data: 'status', name: 'status', orderable: false, "width": "5%"},
        {data: 'action', name: 'action', orderable: false, "width": "25%"},
                // {data: 'created_at', searchable: false },
                // {data: 'updated_at', searchable: false },
                //  {data: 'action', name:'action', searchable: false, orderable: false }
    ],
    "language": {
        "paginate": {
            next: '<i class="fa fa-angle-right"></i>',
            previous: '<i class="fa fa-angle-left"></i>'
        }
    }
});
$('#data-table').on('page.dt', function () {

    var info = table.page.info();
    var pageNo = info.page + 1;
    //alert(pageNo);
    $.ajax({
        type: 'POST',
        url: "{{ url('/panel/question/pageStoreNumber') }}",
        data: 'pageNo=' + pageNo,
        success: function (data) {
            //location.reload();
            table.ajax.reload(null, false);
        }
    });
});
$(document).ready(function () {
    $('#ExperienceModel').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');

        $.ajax({
            type: 'get',
            url: "{{ url('/panel/question/getquestion') }}",
            data: 'id=' + rowid, //Pass $id
            success: function (data) {

                $('#id').val(data.id);
                $('#language_id').val(data.language_id);
                $('#question').val(data.question);
                if (data.expert_id != '') {
                    $('select[name^="expert_id"] option[value="' + data.expert_id + '"]').attr("selected", "selected");
                }



            }
        });


    });

});
$(".Assign").click(function () {
    $('.loading').show();
    $.ajax({

        //dataType : 'json',
        type: "POST",
        url: "{{ url('/panel/question/Assignquestion') }}",
        //contentType: false,
        processData: true,
        data: {
            id: $("#id").val(),
            language_id: $("#language_id").val(),
            question: $("#question").val(),
            expert_id: $("#expert_id").val(),
            //_token: '{!! csrf_token() !!}',
        },
        success: function (data) {
            //alert(data);
            if (data.status == true) {
                table.ajax.reload(null, false);
                $('#ExperienceModel').modal('hide');
                $('.loading').hide();
            }
            if (data.status == false) {
                $('.ajaxerrorreg').html('<div class="alert alert-danger">' + data.message + '</div>');
                $('.loading').hide();
                return false;
            }
        },
        error: function (result) {
            // alert("Error chk");
        }
    });

});
$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
    if ($(this).prop("checked") == true) {
        $("#showmultiaction").show();
    } else if ($("input[name='checkaction[]']").prop("checked") == true) {
        $("#showmultiaction").show();
    } else {
        $("#showmultiaction").hide();
    }

});

function onclickcheck(id) {

    var a = $("input[name='checkaction[]']");
    if (a.filter(":checked").length > 0) {
        $("#showmultiaction").show();
    } else {
        $("#showmultiaction").hide();
    }
}

</script>
@endpush('scripts')