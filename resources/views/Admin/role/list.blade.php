@extends('Admin.master_layout.master')

@section('title', 'Role List')

@section('breadcum')
     / Role List 
@endsection

@section('content')
<div class="">
    <?php
    /*
     <div class="page-title">
        <div class="title_left">
            <h3>Users</h3>
            <p>You can manage users here.</p>
            </p>
        </div>
    </div>
    */
    ?>

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Roles</h4>
                    <!-- <a href="{{ url('admin/role/add') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add Role
                    </a> -->
                    <!-- <a href="{{ url('appuser/download/excel') }}" class="btn btn-sm btn-xs btn-warning pull-right">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export
                    </a> -->
                     <div class="clearfix"></div>
                </div>
                <table id="role-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Display Name</th>
                        <!-- <th>Description</th> -->
                        <!-- <th>Created At</th> -->
                        <!-- <th>Updated At</th> -->
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#role-table').DataTable({
        serverSide: true,
        processing: true,
        ajax: "{{ url('/panel/role/list') }}",
        columns: [
            {data: 'name',orderable: true, searchable: true},
            {data: 'display_name',orderable: true, searchable: true},
            // {data: 'description', orderable: true},
            // {data: 'created_at', orderable: true},
            // {data: 'updated_at', orderable: true},
           {data: 'action', name:'action', searchable: false, orderable: false }
        ],
        "language": {
            "paginate": {
                 next: '<i class="fa fa-angle-right"></i>',
                previous: '<i class="fa fa-angle-left"></i>'
            }
        }
    });
});
   
</script>
@endpush('scripts')
