@extends('Admin.master_layout.master')

@section('title', 'Add Role')

@section('breadcum')
/ Add Role
@endsection

@section('content')

<div class="row">
    {!! Form::open(array('url' => ('panel/role/store'),'method'=>'POST', 'files'=>true)) !!}
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
            <?php
            // echo "<pre>";print_r($errors);die;
            ?>
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
            @endif
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Add Role</h4>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    {{ Form::hidden('id', isset($role->id)?$role->id:null) }}
                    <div class="form-group <?php echo $errors->first('name') !== '' ? 'has-error' : ''; ?>">
                        {!! Form::label('Name', 'Name',array('class'=>'control-label')) !!}
                        {{ Form::text('name', isset($role->name)?$role->name:null, array('placeholder' => 'Name','class' => 'form-control', 'id' => 'name')) }}
                        <span class="text-danger" >{{ $errors->first('name') }}</span>
                    </div>

                    <div class="form-group <?php echo $errors->first('display_name') !== '' ? 'has-error' : ''; ?>">
                        {!! Form::label('Display Name', 'Display Name', array('class'=>'control-label')) !!}
                        {!! Form::text('display_name', isset($role->display_name)?$role->display_name:null, array('placeholder' => 'Display Name','class' => 'form-control','id'=>'display_name')) !!}
                        <span class="text-danger" >{{ $errors->first('display_name') }}</span>
                    </div>

                    <div class="form-group <?php echo $errors->first('description') !== '' ? 'has-error' : ''; ?>">
                        {!! Form::label('Description', 'Description',array('class'=>'control-label')) !!}
                        {!! Form::textarea('description', isset($role->description)?$role->description:null, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px','id'=>'description')) !!}
                        <span class="text-danger" >{{ $errors->first('description') }}</span>
                    </div>
                </div>
            </div>
        </div>







    </div>

    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Role Permission</h4>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            @foreach($permissionArr as $module => $permission)
                            <label class="module">{{ucfirst(str_replace('_',' ',$module))}}</label>
                            @foreach($permission as $value)
                            <div class="permission">
                                <?php
                                $selPermission = false;
                                ?>
                                @if(count($permissionRoleList) > 0)
                                @foreach($permissionRoleList as $key=>$permissionVal)
                                @if($value->id == $permissionVal->permission_id)
                                <?php $selPermission = true; ?>
                                @endif
                                @endforeach
                                @endif
                                <div class="checkbox">
                                    <label class="">
                                        {{ Form::checkbox('permission[]', $value->id, $selPermission, array('class' => 'name flat')) }}
                                        {{ ucfirst($value->name) }}
                                    </label>
                                </div>
                            </div>
                            @endforeach
                            <br>
                            @endforeach

                            <span class="text-danger" >{{ $errors->first('permission') }}</span>
                        </div>
                    </div>

                    <div class="box-footer" style="margin-top: 150px;">
                        {!! Form::submit('SUBMIT', array('class' => 'btn btn-primary submit')) !!}
                        <a class="btn btn-default btn-close" href="{{ URL::to('/panel/role') }}">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer')
@parent
@endsection