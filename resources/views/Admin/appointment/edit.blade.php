@extends('Admin.master_layout.master')

@section('title', 'Edit Appointment')

@section('breadcum')
     / <a href="{{ url('panel/appointment') }}">Appointment List</a> / Edit Appointment 
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
 @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
            
            <!-- if there are creation errors, they will show here -->
            <span class="errormessage"></span>
        </div>    
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
                {!! Form::open(array('url' => ('/panel/appointment/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="box-body">

                    {!! Form::hidden('id',"$appointment->id",['class'=>'form-control']) !!}

                    <div class="form-group">
                        {!! Form::label('Language', 'Language') !!}
                        <select name="language_id" class="form-control" required="" id="language_id">
                            <option value="">Select Language</option>
                            @if(count($languages) > 0)
                                @foreach($languages as $key => $lang)
                                    <option value="{{ $lang['id'] }}" 
                                        @if(isset($appointment->language_id) && $appointment->language_id == $lang['id'])selected=""@endif>{{ $lang['name'] }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        <span class="text-danger">{{ $errors->first('language_id') }}</span>
                    </div>

                    <?php if(Auth::user()->user_role == 1){?>
                    <div class="form-group">
                        {!! Form::label('Expert', 'Expert') !!}
                        <select name="expert_id" class="form-control">
                            <option value="">Select Expert</option>
                            @if(count($User) > 0)
                                @foreach($User as $key => $users)
                                    <option value="{{ $users['id'] }}" 
                                        @if(isset($appointment->expert_id) && $appointment->expert_id == $users['id'])selected=""@endif>{{ $users['first_name'] }} {{ $users['last_name'] }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        <span class="text-danger">{{ $errors->first('expert_id') }}</span>
                    </div>
                    <?php } ?>

                    <div class="form-group">
                        {!! Form::label('App User', 'App User') !!}
                        <select name="appuser_id" class="form-control" required="">
                            <option value="">Select User</option>
                            @if(count($AppUser) > 0)
                                @foreach($AppUser as $key => $users)
                                    <option value="{{ $users['id'] }}" 
                                        @if(isset($appointment->appuser_id) && $appointment->appuser_id == $users['id'])selected=""@endif>
                                        @if(!empty($users['username']))
                                        {{ $users['username'] }}
                                        @else
                                        {{ $users['email'] }}
                                        @endif
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        <span class="text-danger">{{ $errors->first('appuser_id') }}</span>
                    </div>


                    <!-- <div class="form-group">
                        {!! Form::label('Title', 'Title') !!}
                        {!! Form::text('title',"$appointment->title",['class'=>'form-control']) !!}
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    </div> -->

                     
                    <div class="form-group">
                        {!! Form::label('Category', 'Category') !!}
                        <select name="category_id" class="form-control" id="category_id" required="">
                            <option value="">Select Category</option>
                            @if(count($Category) > 0)
                                @foreach($Category as $key => $Categorys)
                                    <option value="{{ $Categorys['id'] }}"
                                        @if(isset($appointment->category_id) && $appointment->category_id == $Categorys['id'])selected=""@endif>{{ $Categorys['category'] }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        <span class="text-danger">{{ $errors->first('category_id') }}</span>
                    </div>

                    <!-- <div class="form-group">
                        {!! Form::label('Topic', 'Topic') !!}
                        <select name="topic_id" class="form-control" id="topic_id" required="">
                            <option value="">Select Topic</option>
                            @if(count($Topic) > 0)
                                @foreach($Topic as $key => $Topics)
                                    <option value="{{ $Topics['id'] }}" 
                                        @if(isset($appointment->topic_id) && $appointment->topic_id == $Topics['id'])selected=""@endif>{{ $Topics['topic'] }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        <span class="text-danger">{{ $errors->first('topic_id') }}</span>
                    </div> -->

                    <div class="form-group">
                        {!! Form::label('Booking Date', 'Booking Date') !!}
                        <div class="clear"></div>
                        <!-- {!! Form::text('booking_date',$appointment->booking_date,['class'=>'form-control']) !!} -->
                        <!-- 04/17/2018 -->
                        <?php
                            $booking_date = date('m/d/Y',strtotime($appointment->booking_date));
                        ?>
                        <div class="col-md-3">
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="col-md-12 xdisplay_inputx form-group has-feedback booking_date">
                                            <input type="text" name="booking_date" class="form-control has-feedback-left" id="single_cal1" placeholder="Booking Date" aria-describedby="inputSuccess2Status" value="{{ $booking_date }}" required="">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <span class="text-danger">{{ $errors->first('booking_date') }}</span>
                    </div>

                    <div class="clear"></div>
                    <!-- <div class="form-group">
                        {!! Form::label('Starting Time', 'Starting Time') !!}
                        {!! Form::text('start_time',$appointment->start_time,['class'=>'form-control','id' => 'start_time','required' => '']) !!}
                        <span class="text-danger">{{ $errors->first('start_time') }}</span>
                    </div>

                    <div class="form-group">
                        {!! Form::label('Ending Time', 'Ending Time') !!}
                        {!! Form::text('end_time',$appointment->end_time,['class'=>'form-control','id' => 'end_time','required' => '']) !!}
                        <span class="text-danger">{{ $errors->first('end_time') }}</span>
                    </div> -->
                <div class="form-group">
                        {!! Form::label('Select Time', 'Select Time') !!}
                        
                <select class="form-control" name="select_time">
                        @if(count($getTime) > 0)
                        @foreach($getTime as $key => $value)
                        <option value="{{ $key }}" @if($select_time == $key) selected="" @endif>{{ $value }}</option>
                        @endforeach
                        @endif
                </select>

                        <span class="text-danger">{{ $errors->first('select_time') }}</span>
                    </div>

                    <div class="form-group">
                        {!! Form::label('Description', 'Description') !!}
                        {!! Form::textarea('description',$appointment->description,['class'=>'form-control','required' => '']) !!}
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    </div>

                    <div  class="form-group <?php  echo $errors->first('status') !== '' ? 'has-error' : '';?> hide" >
                            {!! Form::label('Status', 'Status',array('class'=>'control-label')) !!}
                           @if ($appointment->status == 1)
                            {!! Form::radio('status', '1',['checked' => 'checked'],['class' => 'minimal']) !!} Assigned
                            {!! Form::radio('status', '0','',['class' => 'minimal']) !!} Not Assign
                            @else
                            {!! Form::radio('status', '1','',['class' => 'minimal']) !!} Assigned
                            {!! Form::radio('status', '0',['checked' => 'checked'],['class' => 'minimal']) !!} Not Assign
                            @endif
                            <span class="text-danger" >{{ $errors->first('Status') }}</span>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('/panel/appointment') }}">Cancel</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('resources/admin-assets/js/daterangepicker.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('#start_time').daterangepicker({
            autoApply:true,
            singleDatePicker: true,
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'h:mm'
            }
        });

        $('#end_time').daterangepicker({
            autoApply:true,
            singleDatePicker: true,
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'h:mm'
            }
        });

    
        $('#language_id').on('change', function (e) {
            var id = $(this).find(":selected").val();
            $.ajax({
                type : 'get',
                url:  "{{ url('/panel/appointment/getCategoryByLanguage') }}",
                data :  'id='+ id, //Pass $id
                success : function(response){
                    var $select = $('#category_id');
                    $select.find('option').remove();
                    $select.append('<option value="">Select Category</option>');
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value['id'] + '>' + value['category'] + '</option>'); // return empty
                    });
                
                }
            }); 

            $.ajax({
                type : 'get',
                url:  "{{ url('/panel/appointment/getTopicByLanguage') }}",
                data :  'id='+ id, //Pass $id
                success : function(response){
                    var $select = $('#topic_id');
                    $select.find('option').remove();
                    $select.append('<option value="">Select Topic</option>');
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value['id'] + '>' + value['topic'] + '</option>'); // return empty
                    });
                
                }
            });   
        });
    });
</script>
@endpush('scripts')
@section('footer')
    @parent
@endsection