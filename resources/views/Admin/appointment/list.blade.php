@extends('Admin.master_layout.master')

@section('title', 'Appointment List')

@section('breadcum')
     / Appointment List 
@endsection

@section('content')
<div class="">

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Appointment Info</h4>
                    <a  href="{{ url('panel/appointment/add') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        {{ trans('Add Appointment') }}
                    </a>
                   
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                   {!! Form::open(array('url' => ('panel/appointment/updatemultirecode'),'method'=>'POST', 'files'=>true)) !!}
                   <table id="data-table" class="table table-bordered" style="width: 100%;">
                        <thead>
                        <tr>
                           <th style="width: 5% !important;">
                          <input type="checkbox" id="checkAll" />
                        </th>
                        <th style="width: 2% !important;">ID</th>
                        <th style="width: 5% !important;">Expert Assign</th>
                            <!-- <th>Language</th> -->
                            <th style="width: 5% !important;">Username</th>
                            <!-- <th>Title</th> -->
                            <th style="width: 5% !important;">Category</th>
                           <!--  <th>Topic</th> -->
                            <th style="width: 5% !important;">Appointment Date</th>
                            <th style="width: 5% !important;">Appointment Time</th>
                           <!--  <th>Description</th> -->
                            <th style="width: 5% !important;">Status</th>
                            <th style="width: 5% !important;">Payment Status</th>
                            <th style="width: 5% !important;">Action</th>
                        </tr>
                        </thead>
                    </table>
                    <div id="showmultiaction" style="display: none;">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    <select name="action" class="form-control" required="">
                   <!--  <option value="active">Active</option>
                    <option value="inactive">InActive</option> -->
                    <option value="delete">Delete</option>
                   </select>
                </div>
                
                {!! Form::submit('Submit', array('class' => 'btn btn-primary submit')) !!}
                </div>
                 {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
<div id="expertModal" class="file-zoom-dialog modal fade in" tabindex="-1" aria-labelledby="kvFileinputModalLabel" >
  <div class="modal-dialog modal-lg" role="document" style="width: 400px;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="kv-zoom-actions pull-right">
          <button type="button" class="btn btn-default btn-close" title="Close detailed preview" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button></div>
        <h3 class="modal-title">Expert Assign</h3>
      </div>
      <div class="modal-body">
         <div class="">
              <div class=" box-primary">
                   <!-- {!! Form::open(array('url' => ('admin/appointment/assignAppoinment'),'method'=>'POST', 'files'=>true)) !!} -->
                    <div class="ajaxerrorreg"></div>
                   <input type="hidden" name="id" id="id" value="">
                   <input type="hidden" name="booking_date" id="booking_date" value="">
                   <input type="hidden" name="start_time" id="start_time" value="">
                   <input type="hidden" name="end_time" id="end_time" value="">
                   <div class="box-body">
                      <div class="form-group">
                      {!! Form::label('Assign', 'Assign') !!}
                          
                      <select name="expert_id" id="expert_id" class="form-control">
                              <option value="">Select Assign</option>
                              @if(count($User) > 0)
                              @foreach($User as $key => $users)
                              <option value="{{ $users['id'] }}">{{ $users['first_name'] }} {{ $users['last_name'] }}</option>
                              @endforeach
                              @endif
                          </select>

                          <span class="text-danger">{{ $errors->first('expert_id') }}</span>
                      </div>
                       <div class="box-footer">
                      <!-- {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                      <a class="btn btn-default btn-close" href="javascript:void(0)" data-dismiss="modal" aria-hidden="true">Cancel</a> -->
                      <a class="btn btn-primary submit Assign" href="javascript:void(0)" > Submit </a>
                      <a class="btn btn-default btn-close" href="javascript:void(0)" data-dismiss="modal" aria-hidden="true">Cancel</a>
                      <div class="loading" style="display: none;"><img src="../img/loading.gif" style="width: 20px;"></div>
                  </div>
                  </div>
                  <!--  {!! Form::close() !!} -->

              </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>

<script type="text/javascript">
  
        $(document).ready(function(){
            $('#expertModal').on('show.bs.modal', function (e) {
                var rowid = $(e.relatedTarget).data('id');
                $('.ajaxerrorreg').html('');
                $.ajax({
                    type : 'get',
                    url:  "{{ url('/panel/appointment/getAppoinment') }}",
                    data :  'id='+ rowid, //Pass $id
                    success : function(data){
                      
                        $('#id').val(data.id);
                        $('#booking_date').val(data.booking_date);
                        $('#start_time').val(data.start_time);
                        $('#end_time').val(data.end_time);
                        if(data.expert_id != ''){
                             $('select[name^="expert_id"] option[value="'+data.expert_id+'"]').attr("selected","selected");
                        }
                    }
                });

                
             });
        });
        var table = $('#data-table').DataTable({
            serverSide: true,
            processing: true,
            autoWidth: false,
            stateSave: true,
            "order": [],
            ajax: "{{ url('/panel/appointment/array-data') }}",
            columns: [
                {data: 'checkbox',orderable: false, "width": "5%"},
                {data: 'id',name: 'appointment.id',orderable: false, "width": "2%"},
                {data: 'expert_id',name: 'user.first_name',orderable: false, "width": "5%"},
                {data: 'appuser_id',name: 'appuser.username',orderable: false, searchable: true, "width": "5%"},
                {data: 'category_id',name: 'category_appointment.category', orderable: false, "width": "5%"},
                {data: 'booking_date',name: 'appointment.booking_date', orderable: false, "width": "5%"},
                {data: 'start_time',name: 'appointment.start_time', orderable: false, "width": "5%"},
                {data: 'status',name: 'status', orderable: false, "width": "5%"},
                {data: 'payment_status',name: 'payment_status', orderable: false, "width": "5%"},
                {data: 'action',name: 'action', orderable: false, "width": "25%"},
            ],
            "appointment": {
                "paginate": {
                    next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
        $(".Assign").click(function(){
            $('.loading').show();
           
            if($("#expert_id").val() == ''){
              $('.ajaxerrorreg').html('<div class="alert alert-danger">Please Select Expert</div>');
              $('.loading').hide();
              return false;
            }else{
              $('.ajaxerrorreg').hide();
            }
            $.ajax({
            dataType : 'json',
            type: "POST",
            url: "{{ url('/panel/appointment/assignAppoinment') }}",
            //contentType: false,
            processData: true,
            data: {
                id: $("#id").val(),
                expert_id: $("#expert_id").val(),
                booking_date: $("#booking_date").val(),
                start_time: $("#start_time").val(),
                end_time: $("#end_time").val(),
                 //_token: '{!! csrf_token() !!}',
            },
            success: function (data) {
              //alert(data);
              if(data.status == true){
                table.ajax.reload(null, false);
                $('#expertModal').modal('hide');
                $('.loading').hide();
                $('.ajaxerrorreg').hide();
              }
              if(data.status == false){
                $('.ajaxerrorreg').show();
                $('.ajaxerrorreg').html('<div class="alert alert-danger">'+data.message+'</div>');
                $('.loading').hide();
                return false;
              }
            },
            error: function (result) {
               // alert("Error chk");
            }
     });

    });
 $("#checkAll").change(function () {
      $("input:checkbox").prop('checked', $(this).prop("checked"));
      if($(this).prop("checked") == true){
        $("#showmultiaction").show();
      }else if($("input[name='checkaction[]']").prop("checked") == true){
        $("#showmultiaction").show();
      }else{
        $("#showmultiaction").hide();
      }
      
});
function onclickcheck(id){
   var a = $("input[name='checkaction[]']");
  if(a.filter(":checked").length > 0){
      $("#showmultiaction").show();
  }else{
    $("#showmultiaction").hide();
  }
}
</script>
@endpush('scripts')