@extends('Admin.master_layout.master')

@section('title', 'Language List')

@section('breadcum')
     / Language List 
@endsection

@section('content')
<div class="">

    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>Language Info</h4>
                    <!-- <a  href="{{ url('admin/language/add') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        {{ trans('Add Language') }}
                    </a> -->
                   
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                   <table id="data-table" class="table table-bordered">
                        <thead>
                        <tr>
                         <?php
                         /*
                            <th>#</th>
                        */
                        ?>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Status</th>
                            <th>Action</th>
                        <?php /*    <th>Created On</th>
                            <th>Updated On</th>
                            */
                        ?>
                        <?php
                        /*
                            <th>Action</th>
                        */
                        ?>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('public/js/bootstrap-notify.js') }}"></script>

<script type="text/javascript">
   
        $('#data-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: "{{ url('/panel/language/array-data') }}",
            columns: [
                //{data: 'id', orderable: true, searchable: true },
                {data: 'name',orderable: true, searchable: true},
                {data: 'code',orderable: true, searchable: true},
                {data: 'status', orderable: false},
                {data: 'action', orderable: false},
               // {data: 'created_at', searchable: false },
               // {data: 'updated_at', searchable: false },
              //  {data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                    next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
 
</script>
@endpush('scripts')