@extends('Admin.master_layout.master')

@section('title', 'Add Language')

@section('breadcum')
     / Add Language
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <!-- <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul> -->
                </div>
            @endif
            <!-- if there are creation errors, they will show here -->
            <span class="errormessage"></span>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class=" box-primary">
                {!! Form::open(array('url' => ('panel/language/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="box-body">

                    <!-- key title  -->
                    <div class="form-group">
                        {!! Form::label('Name', 'Name') !!}
                        {!! Form::text('name',"",['class'=>'form-control']) !!}

                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                     <div class="form-group">
                        {!! Form::label('Code', 'Code') !!}
                        {!! Form::text('code',"",['class'=>'form-control']) !!}

                        <span class="text-danger">{{ $errors->first('code') }}</span>
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('status', 'Status:')   !!}
                        {!! Form::radio('status', '1',true,['class' => 'minimal']) !!}  Active &nbsp;
                        {!! Form::radio('status', '0','',['class' => 'minimal']) !!}  InActive &nbsp;
                    </div>
                </div>

                <div class="box-footer">
                    {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                    <a class="btn btn-default btn-close" href="{{ URL::to('/panel/language') }}">Cancel</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection
