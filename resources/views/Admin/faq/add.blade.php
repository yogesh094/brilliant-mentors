@extends('Admin.master_layout.master')

@section('title', 'Create FAQ')

@section('breadcum')
    / <a href="{{ url('panel/faq') }}">FAQ List</a> / Create FAQ
@endsection

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Create FAQ</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('panel/faq/store'),'method'=>'POST', 'files'=>true)) !!}
                <div class="x_content">
                    <div class="form-group">
                        {!! Form::label('Language', 'Language') !!}
                        
                        <select name="language_id" class="form-control" id="language_id" required="">
                            <option value="">Select Language</option>
                            @if(count($languages) > 0)
                            @foreach($languages as $key => $lang)
                            <option value="{{ $lang['id'] }}">{{ $lang['name'] }}</option>
                            @endforeach

                            @endif
                        </select>

                        <span class="text-danger">{{ $errors->first('language_id') }}</span>
                    </div>
                         <div class="form-group ">
                            {!! Form::label('Question', 'Question : ',array('class'=>'control-label')) !!}
                             <textarea name="question" class="form-control" required rows="5"></textarea>
                            <span class="text-danger" >{{ $errors->first('question') }}</span>
                        </div>

                        <div class="form-group ">
                            {!! Form::label('Answer', 'Answer : ',array('class'=>'control-label')) !!}
                            <textarea name="answer" class="form-control" required cols="5" rows="10"></textarea>
                            <span class="text-danger" >{{ $errors->first('answer') }}</span>
                        </div>


                       <div class="form-group">
                            {!! Form::label('status', 'Status:')   !!}

                            {!! Form::radio('status', '0',['checked' => 'checked'],['class' => 'flat']) !!} InActive
                            {!! Form::radio('status', '1','',['class' => 'flat']) !!} Active
                            
                        </div>

                        
                       
                        <div class="box-footer">
                            {!! Form::submit('Add', array('class' => 'btn btn-primary submit')) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/panel/faq') }}">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection

@section('footer')
    @parent
@endsection