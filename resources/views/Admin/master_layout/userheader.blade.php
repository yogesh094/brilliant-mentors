<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

                <div class="">
                    <div class="col-md-2 logo_mid">
                       <a href="{{ url('panel/dashboard') }}" class="site_title">
                            <img height="80%" src="{{ url('img/logo.png') }}" class="img-responsive">
                        </a>
                    </div>
                </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <!-- <img src="images/user-card-1.jpg" alt=""> -->
                        <span class="top_menu_user_name">
                            {{ ucfirst(Auth::user()->first_name) }}
                        </span>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right profile-dropdown">
                        <li><a href="{{ url('panel/profile') }}"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a></li>
                        <li><a href="{{ url('panel/change-password') }}"><i class="fa fa-lock" aria-hidden="true"></i>Change Password</a></li>
                        <?php
                        /*
                        <li><a href="profile.html"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                        <li><a href="task_list.html"><i class="fa fa-list-ul" aria-hidden="true"></i><span class="badge bg-green pull-right">5</span>Tasks</a></li>
                        <li><a href="messages.html"><i class="fa fa fa-envelope-o" aria-hidden="true"></i>Messages</a></li> 
                        */
                        ?>
                        <li class="top-menu-border-top"><a href="{{ url('/panel/logout') }}"><i class="fa fa-sign-out"></i>Log Out</a></li>
                    </ul>
                </li>
                <?php
                /*
                <!-- notification mail menu start  -->
                <!-- <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-green">6</span>
                    </a>

                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        <li class="msg_list_menu">
                            <a>
                                <div>
                                    <span class="image"><img src="images/user-card-3.jpg" alt="Profile Image" /></span>
                                    <span>
                                        <span class="name">Ed Stafford</span><br/>
                                        <span class="time">Just Now</span>
                                    </span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh...
                                    </span>
                                </div>
                            </a>
                            <a>
                                <div>
                                    <span class="image"><img src="images/user-card-2.jpg" alt="Profile Image" /></span>
                                    <span>
                                        <span class="name">Jessy</span><br/>
                                        <span class="time">5 mins ago</span>
                                    </span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh...
                                    </span>
                                </div>
                            </a>                                                
                            <a>
                                <div>
                                    <span class="image"><img src="images/user-card-4.jpg" alt="Profile Image" /></span>
                                    <span>
                                        <span class="name">Mary Jane</span><br/>
                                        <span class="time">2 hrs</span>
                                    </span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh...
                                    </span>
                                </div>
                            </a>
                            <a>
                                <div>
                                    <span class="image"><img src="images/user-card-3.jpg" alt="Profile Image" /></span>
                                    <span>
                                        <span class="name">Ed Stafford</span><br/>
                                        <span class="time">7 hrs</span>
                                    </span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh...
                                    </span>
                                </div>
                            </a>
                            <a>
                                <div>
                                    <span class="image"><img src="images/user-card-2.jpg" alt="Profile Image" /></span>
                                    <span>
                                        <span class="name">Jessy</span><br/>
                                        <span class="time">2 days ago</span>
                                    </span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh...
                                    </span>
                                </div>
                            </a>                                                
                            <a>
                                <div>
                                    <span class="image"><img src="images/user-card-4.jpg" alt="Profile Image" /></span>
                                    <span>
                                        <span class="name">Mary Jane</span><br/>
                                        <span class="time">3 days ago</span>
                                    </span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh...
                                    </span>
                                </div>
                            </a>   
                        </li>

                        <li class="see_more_menu">
                            <div class="msg">
                                <p><span>You have</span> 6 new 
                                    <span>Messages</span>
                                </p>
                            </div>
                            <div class="view_more">
                                <a href="#">
                                    <span>View All</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li> -->
                <!-- notification mail menu end  -->

                <!-- notification menu start  -->
                <!-- <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell-o"></i>
                        <span class="badge bg-green">5</span>
                    </a>

                    <ul id="menu2" class="dropdown-menu list-unstyled msg_list" role="menu">
                        <li class="msg_list_menu notification_menu">
                            <a>
                                <div>
                                    <span class="notify bg-red">
                                        <i class="fa fa-cart-plus"></i>
                                    </span>
                                    <span>
                                        <span>order #24779627 has been placed</span><br/>
                                        <span class="time">Just Now</span>
                                    </span>
                                </div>
                            </a>
                            <a>
                                <div>
                                    <span class="notify bg-blue">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <span>
                                        <span>Event started</span><br/>
                                        <span class="time">5 mins ago</span>
                                    </span>
                                </div>
                            </a>
                            <a>
                                <div>
                                    <span class="notify bg-green">
                                        <i class="fa fa-user-plus"></i>
                                    </span>
                                    <span>
                                        <span>New user registered</span><br/>
                                        <span class="time">2 hrs ago</span>
                                    </span>
                                </div>
                            </a>
                            <a>
                                <div>
                                    <span class="notify bg-orange">
                                        <i class="fa fa-bullhorn"></i>
                                    </span>
                                    <span>
                                        <span>monthly salary was paid</span><br/>
                                        <span class="time">5 hrs ago</span>
                                    </span>
                                </div>
                            </a>
                            <a>
                                <div>
                                    <span class="notify bg-red">
                                        <i class="fa fa-cart-plus"></i>
                                    </span>
                                    <span>
                                        <span>order #24779627 has been placed</span><br/>
                                        <span class="time">Just Now</span>
                                    </span>
                                </div>
                            </a>
                            <a>
                                <div>
                                    <span class="notify bg-blue">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <span>
                                        <span>Event started</span><br/>
                                        <span class="time">5 mins ago</span>
                                    </span>
                                </div>
                            </a>
                        </li>

                        <li class="see_more_menu">
                            <div class="msg">
                                <p>12 pending
                                    <span>notifications</span>
                                </p>
                            </div>
                            <div class="view_more">
                                <a href="#">
                                    <span>View All</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li> -->
                <!-- notification menu end -->
                */
                ?>
            </ul>
        </nav>
    </div>
</div>