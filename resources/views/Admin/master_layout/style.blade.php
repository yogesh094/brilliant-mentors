<!-- Require style sheet -->

	<!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('resources/admin-assets/css/bootstrap/dist/css/bootstrap.min.css') }}" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('resources/admin-assets/css/font-awesome/css/font-awesome.min.css') }}" />
    <!-- NProgress -->
    <link href="{{ asset('resources/admin-assets/css/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ asset('resources/admin-assets/css/animate.css/animate.min.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('resources/admin-assets/css/iCheck/skins/square/blue.css') }}" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="{{ asset('resources/admin-assets/css/custom.min.css') }}" rel="stylesheet">
    <!-- user Custom Style -->
    <link href="{{ asset('resources/admin-assets/css/mycustom.css') }}" rel="stylesheet">
    <!-- datatabele -->
    <link rel="stylesheet" href="{{ asset('resources/admin-assets/css/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('resources/admin-assets/css/daterangepicker.css') }}" />
    @stack('styles')