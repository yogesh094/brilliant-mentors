<!DOCTYPE html>
<html lang="en">

<!-- header start -->
<!-- header end -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('resources/web-assets/images/fav-icon.png') }}">
    <title>@yield('title') - Islamhub</title>

    <!-- Favicon -->
    <!-- <link rel="shortcut icon" type="image/icon" href="./images/favicon.ico"/> -->
    
    @section('styles')
        @include('Admin.master_layout.style')
    @show
</head>

<body class="nav-md">
        <div class="container body 555">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            
                        </div>

                        <div class="clearfix"></div>
                        <?php
                        /*
                        <!-- menu quick search -->
                        <!-- <div class="profile clearfix">
                            <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                <div class="input-group sidemenu-search-div">
                                    <input type="text" class="form-control side-menu-search" placeholder="Search...">
                                    <span class="input-group-btn" style="">
                                        <a href="javascript:;" class="btn submit sidemenu-search-icon">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </span>
                                </div>
                            </form>
                        </div> -->
                        <!-- /menu quick search -->
                        */
                        ?>
                        <br />

                        @section('sidebar_menu')
                        <!-- sidebar menu -->
                        @include('Admin.master_layout.sidebar')
                        <!-- /sidebar menu -->
                        @show

                        <?php
                        /*
                        <!-- menu footer buttons -->
                        <!-- <div class="sidebar-footer hidden-small">
                            <a href="../documentation/index.html" data-toggle="tooltip" data-placement="top" title="Documentation">
                                <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen" onclick="toggleFullScreen();">
                                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                        </div> -->
                        <!-- /menu footer buttons -->
                         */
                        ?>
                    </div>
                </div>
                @section('header_menu')
                <!-- top navigation -->
                @include('Admin.master_layout.userheader')
                <!-- /top navigation -->
                @show


                <!-- breadcrumb -->
                <div class="breadcrumb_content">
                    <div class="breadcrumb_text"><a href="{{ url('/panel/dashboard') }}">Home</a> @section('breadcum') / @show
                    </div>
                </div>
                <!-- /breadcrumb -->
                
                <!-- page content -->
                <div class="right_col" role="main">

                    @section('content')
                    <!-- top tiles -->
                    <div class="row top_tiles">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-blue-sky">
                                <div class="icon"><i class="fa fa-shopping-cart"></i></div>
                                <div class="count">349</div>
                                <h3>ORDERS</h3>
                                <p>+15% From previous month</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-purple">
                                <div class="icon"><i class="fa fa-comments-o"></i></div>
                                <div class="count">58,880 $</div>
                                <h3>INCOM</h3>
                                <p>+25% From previous month</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-red">
                                <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                                <div class="count">15,790</div>
                                <h3>VISITORS</h3>
                                <p>+25% From previous month</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats tile-white bg-blue">
                                <div class="icon"><i class="fa fa-check-square-o"></i></div>
                                <div class="count">3,271</div>
                                <h3>CUSTOMERS</h3>
                                <p>+15% From previous month</p>
                            </div>
                        </div>
                    </div>
                    <!-- /top tiles -->
                    @show

                    
                </div>
                <!-- /page content -->

                @section('footer')
                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        <p>Powered By Religion.© <?php echo date("Y"); ?> All Rights Reserved.</p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                @show
                <!-- /footer content -->
            </div>
        </div>

        @section('scripts')
            @include('Admin.master_layout.script')
            <script type="text/javascript">
                $('input[type=password]').keypress(function( e ) {
                    if(e.which === 32) 
                        return false;
                });
            </script>
        @show

        
    </body>
</html>
@show
