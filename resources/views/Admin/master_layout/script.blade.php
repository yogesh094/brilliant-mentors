<!-- Require script -->

<!-- jQuery -->
<script src="{{ asset('resources/admin-assets/js/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('resources/admin-assets/js/bootstrap.min.js') }}"></script>

<!-- NProgress -->
<script src="{{ asset('resources/admin-assets/css/nprogress/nprogress.js') }}"></script>
<!-- Chart.js -->

<!-- datatable js -->
<script src="{{ asset('resources/admin-assets/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/dataTables.scroller.min.js') }}"></script>
<!-- datatable js -->

<!-- iCheck -->
<script src="{{ asset('resources/admin-assets/css/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/moment/moment.js') }}"></script>

@stack('scripts')
<!-- Custom Theme Scripts -->
<script src="{{ asset('resources/admin-assets/js/custom.js') }}"></script>
<script type="text/javascript">
	var base_url = "{{ url('/') }}";
</script>



