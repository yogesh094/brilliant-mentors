<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section"> 
        <h3>General</h3>
        <ul class="nav side-menu">
            <?php
            /*
            <!-- <li>
                <a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="index.html">Dashboard 1</a></li>
                    <li><a href="dashboard_2.html">Dashboard 2</a></li>
                </ul>
            </li> -->
            */
            ?>
            <li> 
                <a href="{{ url('panel/dashboard') }}"><i class="fa fa-tachometer"></i> Dashboard </a>
            </li>
            <?php if(Auth::user()->user_role == 1){?>
                <li><a><i class="fa fa-users"></i> Expert Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ url('panel/user') }}">Expert</a></li>
                        <li><a href="{{ url('panel/role/edit/2') }}">Expert Role</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ url('panel/appuser') }}"><i class="fa fa-users"></i> Users  </a>
                </li>
                <li>
                    <a href="{{ url('panel/question') }}"><i class="fa fa-question"></i> Question  </a>
                </li>
                <li>
                    <a><i class="fa fa-language"></i> Language <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none;">
                        <li> <a href="{{ url('panel/language') }}"> Language </a></li>
                        
                        <li> <a href="{{ url('panel/language/keyword') }}">Keyword</a></li>
                        
                    </ul>
                </li>
                <li>
                    <a href="{{ url('panel/cms') }}"><i class="fa fa-file"></i>CMS</a>
                </li>
                <li>
                    <a href="{{ url('panel/faq') }}"><i class="fa fa-file"></i>FAQ</a>
                </li>
                <!--<li>-->
                <!--    <a href="{{ url('admin/topic') }}"><i class="fa fa-list-ul"></i>Topic</a>-->
                <!--</li>-->
                <li>
                    <a href="{{ url('panel/category') }}"><i class="fa fa-list-ul"></i>Question Category</a>
                </li>
                <li>
                    <a href="{{ url('panel/appointment/category') }}"><i class="fa fa-list-ul"></i>Appointment Category</a>
                </li>
                <li>
                    <a href="{{ url('panel/appointment') }}"><i class="fa fa-calendar"></i>Appointment</a>
                </li>
                <li>
                    <a href="{{ url('panel/orders') }}"><i class="fa fa-shopping-cart"></i> Orders  </a>
                </li>
                <li>
                    <a href="{{ url('panel/slider') }}"><i class="fa fa-cube"></i> Slider  </a>
                </li>
                <li>
                    <a href="{{ url('panel/email-template') }}"><i class="fas fa-envelope"></i> Email Template  </a>
                </li>
                <li>
                    <a href="{{ url('panel/general-setting') }}"><i class="fa fa-cog"></i> General Setting  </a>
                </li>
            <?php }else{ ?>
                <li>
                    <a href="{{ url('panel/question') }}"><i class="fa fa-question"></i> Question  </a>
                </li>
                <li>
                    <a href="{{ url('panel/appointment') }}"><i class="fa fa-calendar"></i>Appointment</a>
                </li>
            <?php } ?>
            
            
          
            <?php
            /*
            <!-- <li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="form_general.html">General</a></li>
                    <li><a href="form_advanced.html">Advanced Components</a></li>
                    <li><a href="form_validation.html">Validation</a></li>
                    <li><a href="form_wizards.html">Wizard</a></li>
                    <li><a href="form_upload.html">Upload</a></li>
                    <li><a href="form_cropping.html">Cropping</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="ui_general.html">General</a></li>
                    <li><a href="buttons.html">Buttons</a></li>
                    <li><a href="gallery.html">Gallery</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="icons.html">Icons</a></li>
                    <li><a href="widgets.html">Widgets</a></li>
                    <li><a href="calendar.html">Calendar</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="tables.html">Tables</a></li>
                    <li><a href="tables_dynamic.html">Table Dynamic</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-bar-chart-o"></i> Charts <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="chartjs.html">Chart JS</a></li>
                    <li><a href="morisjs.html">Moris JS</a></li>
                    <li><a href="echarts.html">ECharts</a></li>
                </ul>
            </li>
            <li>
                <a href="maps.html"><i class="fa fa-map-o" aria-hidden="true"></i> Maps</a>
            </li>
            <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="blank_page.html">Blank Page</a></li>
                    <li><a href="blank_light_page.html">Blank Light Page</a></li>
                    <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>
                    <li><a href="fixed_footer.html">Fixed Footer</a></li>
                </ul>
            </li> -->
            */
            ?>
        </ul>
    </div>
    <?php
    /*
    <!-- <div class="menu_section">
        <h3>Extra</h3>
        <ul class="nav side-menu">
            <li>
                <a href="messages.html"><i class="fa fa-envelope-o" aria-hidden="true"></i> Messages </a>
            </li>
            <li><a><i class="fa fa-tasks" aria-hidden="true"></i> Task Manager <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="task_list.html">Task List</a></li>
                    <li><a href="task_detailed.html">Task View</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-laptop"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="page_404.html">404 Error</a></li>
                    <li><a href="page_500.html">500 Error</a></li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="login.html">Login Page</a></li>
                    <li><a href="pricing_tables.html">Pricing Tables</a></li>
                    <li><a href="contacts.html">Contacts</a></li>
                    <li><a href="profile.html">Profile</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-shopping-cart" aria-hidden="true"></i> E-commerce <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="product_list.html">Product List</a></li>
                    <li><a href="product_edit.html">Product Edit</a></li>
                    <li><a href="orders.html">Order List</a></li>
                    <li><a href="order_view.html">Order View</a></li>
                    <li><a href="customers.html">Customers</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="#level1_1">Level One</a>
                    <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#level1_2">Level One</a>
                    </li>
                </ul>
            </li>                  
        </ul>
    </div> -->
    */
    ?>
</div>
<!-- /sidebar menu