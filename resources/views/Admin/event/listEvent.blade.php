@extends('Admin.master_layout.master')

@section('title', 'Event')

@section('breadcum')
     / Event 
@endsection

@section('content')
<div class="">
    <div class="clearfix"></div>
    @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">{!! Session::get('success') !!}</div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>contact Info</h4>
                    <a href="{{ url('event/add') }}" class="btn btn-sm btn-xs btn-success pull-right">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add Event
                    </a>
                     <a class="btn btn-sm btn-xs btn-warning pull-right">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="event-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>EVent</th>
                            <th>Event Type</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @parent
@endsection

@push('scripts')
<script src="{{ asset('resources/admin-assets/js/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/dataTables.bootstrap.min.js') }}"></script>
         <script src="{{ asset('resources/admin-assets/js/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
         <script src="{{ asset('resources/admin-assets/js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
         <script src="{{ asset('resources/admin-assets/js/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/responsive.bootstrap.js') }}"></script>
        <script src="{{ asset('resources/admin-assets/js/dataTables.scroller.min.js') }}"></script>
<script>
    $(function () {
        $('#event-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: "{{ url('/event/ajax-list') }}",
            columns: [
                //{data: 'id', orderable: true, searchable: true },
                {data: 'event_title',orderable: true, searchable: true},
                {data: 'event_type',orderable: true, searchable: true},
                {data: 'start_date',orderable: true, searchable: true},
                {data: 'end_date', orderable: true},
                {data: 'status', orderable: true},
               // {data: 'created_at', searchable: false },
               // {data: 'updated_at', searchable: false },
                //{data: 'action', name:'action', searchable: false, orderable: false }
            ],
            "language": {
                "paginate": {
                    next: '<i class="fa fa-angle-right"></i>',
                    previous: '<i class="fa fa-angle-left"></i>'
                }
            }
        });
    });
</script>
@endpush('scripts')