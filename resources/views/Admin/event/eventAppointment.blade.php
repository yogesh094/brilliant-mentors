
@extends('Admin.master_layout.master')

@section('title', 'Edit Event')

@section('breadcum')
     / Edit Event
@endsection

@push('styles')

<!-- <link href="{{ asset('resources/admin-assets/css/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet" > -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />

<link href="{{ asset('resources/admin-assets/css/fullcalendar/dist/fullcalendar.print.css') }}" rel="stylesheet" media="print" >

@endpush

@section('content')

<div class="page-title">
    <div class="title_left">
        <h3>Calendar <small>Click to add events</small></h3>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <!-- is all day -->
                            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                                <label class="col-md-2 col-sm-2 col-xs-4 control-label">Is all day?  </label>
                                <span class="col-md-10 col-sm-10 col-xs-8 control-label">
                                    <input type="checkbox" name="is_all_day" id="is_all_day" class="" checked="checked">
                                </span>
                            </div>

            <div class="x_content">

                <div id='event_calendar'></div>

            </div>
        </div>

        
    </div>
</div>

<!-- calendar modal --> 
<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">New Appointment </h4>
            </div>
            <div class="modal-body">
                <div id="testmodal" style="padding: 5px 20px;">
                    <form id="eventAppointment" class="calender" name="eventAppointment" method="post">

                        <div class="x_content">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input type="hidden" name="event_id" value="{{ $event->id }}">
                            <!-- item name -->
                            <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                    Item
                                </label>
                                {{ Form::text('item', isset($eventAppointment->item)?$eventAppointment->item:'',array('class' => 'form-control','placeholder' => 'Appointment Item', 'id' => 'appointment_item')) }}
                                
                                <span class="text-danger" id="err_item" ></span>
                            </div>

                            <!-- Event name -->
                            <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                    Event
                                </label>
                                {{ Form::text('event_title', isset($event->event_title)?$event->event_title:'',array('class' => 'form-control','placeholder' => 'Event Title', 'id' => 'event_title', 'disabled' => 'disabled')) }}
                                
                                <span class="text-danger" id="err_event_title"></span>
                            </div>

                            <!-- is all day -->
                            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                                <label class="col-md-2 col-sm-2 col-xs-4 control-label">Is all day?  &nbsp;&nbsp;&nbsp;<input type="checkbox" name="is_all_day" id="is_all_day" value="1" class="" checked="checked"> </label>
                            </div>

                            <!-- Start date -->
                            <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                    Start date
                                </label>
                                {{ Form::text('start_date', isset($eventAppointment->start_date)?$eventAppointment->start_date:'',array('class' => 'form-control','placeholder' => 'Start Date', 'id' => 'start_date')) }}
                                
                                <span class="text-danger" id="err_start_date"></span>
                            </div>

                            <!-- End date -->
                            <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                    End date
                                </label>
                                {{ Form::text('end_date', isset($eventAppointment->end_date)?$eventAppointment->end_date:'',array('class' => 'form-control','placeholder' => 'End Date', 'id' => 'end_date')) }}
                                <span class="text-danger" id="err_end_date"></span>
                            </div>

                            <span class="appointment_time_section" style="display: none;">
                                <!-- Start Time -->
                                <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                                    <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                        Start Time
                                    </label>
                                    {{ Form::select('start_time', $timeArr,isset($eventAppointment->start_time)?$eventAppointment->start_time:null,['id' => 'start_time','class' => 'form-control','placeholder' => 'Select Start Time']) }}
                                    <span class="text-danger" id="err_start_time"></span>
                                </div>

                                <!-- End Time -->
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                        End Time
                                    </label>
                                    {{ Form::select('end_time', $timeArr,isset($eventAppointment->end_time)?$eventAppointment->end_time:null,['id' => 'end_time','class' => 'form-control','placeholder' => 'Select End Time']) }}
                                    <span class="text-danger" id="err_end_time"></span>
                                </div>
                            </span>

                            <!-- Location -->
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                    Location
                                </label>
                                {{ Form::textarea('location', isset($eventAppointment->location)?$eventAppointment->location:'',array('class' => 'form-control','placeholder' => 'Location', 'id' => 'location','rows' => 2)) }}
                                
                                <span class="text-danger" id="err_location"></span>
                            </div>

                            <!-- Notes -->
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                    Notes
                                </label>
                                {{ Form::textarea('notes', isset($eventAppointment->notes)?$eventAppointment->notes:'',array('class' => 'form-control','placeholder' => 'Notes', 'id' => 'notes','rows' => 2)) }}

                                <span class="text-danger" id="err_notes"></span>
                            </div>

                            <!-- set reminder -->
                            <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                                <label class="col-md-6 col-sm-6 col-xs-12 control-label">Set reminder? &nbsp;&nbsp;&nbsp; <input type="checkbox" name="is_reminder_set" id="is_reminder_set" value="1" class="" > </label>
                                
                            </div>

                            <span class="appointment_set_reminder" style="display: none;">
                                <!-- reminder time -->
                                <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                                    
                                    <span id="setRemiderTimeDrop">
                                        <select name="reminder_time" id="reminder_time" class="form-control">
                                            <option value=""></option>
                                            
                                        </select>
                                    </span>
                                    
                                    <span class="text-danger" id="err_event"></span>
                                </div>

                                <!-- reminder Interval -->
                                <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                                    
                                    {{ Form::select('reminder_interval', $reminderArr,isset($eventAppointment->reminder_interval)?$eventAppointment->reminder_interval:null,['id' => 'reminder_interval','class' => 'form-control']) }}

                                    <span class="text-danger" id="err_reminder_interval"></span>
                                </div>

                                <!-- reminder option -->
                                <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                                    <label class="col-md-6 col-sm-6 col-xs-6 control-label"><input type="checkbox" name="send_email" id="send_email" value="1" class="" >&nbsp;&nbsp;Send email?</label>

                                    <label class="col-md-6 col-sm-6 col-xs-6 control-label"><input type="checkbox" name="send_msg" id="send_msg" value="1" class="" >&nbsp;&nbsp;Send text message?</label>
                                    
                                </div>
                                        
                            </span>
                        </div>
                        <input type="hidden" id="apptStartTime"/>
                        <input type="hidden" id="apptEndTime"/>
                        <input type="hidden" id="apptAllDay" /> 
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose closeEventAppointment" data-dismiss="modal">Close</button>
                <button type="submit" name="submit" class="btn btn-primary submitAppointment">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel2">Edit Calendar Entry</h4>
            </div>
            <div class="modal-body">

                <div id="testmodal2" style="padding: 5px 20px;">
                    <form id="antoform2" class="form-horizontal calender" >
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="title2" name="title2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" style="height:55px;" id="descr2" name="descr"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="color" value="#ed6b75" id="start_date1" />
                                <label for="red" class="red"></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Color</label>
                            <div class="col-sm-9 calendar-color">
                                <input type="radio" name="color" value="#36c6d3" />
                                <label for="blue" class="blue"></label>

                                <input type="radio" name="color" value="#b06ccc" />
                                <label for="purple" class="purple"></label>

                                <input type="radio" name="color" value="#35aa47" />
                                <label for="green" class="green"></label>

                                <input type="radio" name="color" value="#9CC2CB" />
                                <label for="aero" class="aero"></label>

                                <input type="radio" name="color" value="#ed6b75" />
                                <label for="red" class="red"></label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary submitAppointment">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
<div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>
<!-- /calendar modal -->

@endsection

@push('scripts')

<script src="{{ asset('resources/admin-assets/js/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/css/fullcalendar/dist/fullcalendar.min.js') }}" ></script>
<script src="{{ asset('resources/admin-assets/js/daterangepicker.js') }}"></script>

<script type="application/javascript">

    $(document).ready(function() {

        $('#start_date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,        
            singleClasses: "picker_2"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#end_date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,        
            singleClasses: "picker_2"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $(document).on('click','#is_all_day',function() {
            if($(this).is(":checked")) {
                $(".appointment_time_section").hide();
            } else {
                $(".appointment_time_section").show();
            }
        });

        $(document).on('click','#is_reminder_set',function() {
            if($(this).is(":checked")) {
                $(".appointment_set_reminder").show();
            } else {
                $(".appointment_set_reminder").hide();
            }
        });

        $(".submitAppointment").on("click",function () {
            
            $.ajax({
                type    : "post",
                url     : "{{ url('/event/appointment/store') }}",
                data    : $("#eventAppointment").serialize(),
                
                success : function (data) {
                    response = jQuery.parseJSON(data);
                    if(response.status) {
                        console.log(response.status);
                        $('.closeEventAppointment').click();
                    }
                    console.log(response.status);
                }
            });
        });
        
        //set reminder start
        function remiderTimeData() {
            
            var intervalType        = $('select[name="reminder_interval"]').val();
            var intervalTypeRang    = 0;
            if(intervalType == 1) { //minutes
                intervalTypeRang    = 60;
            } else if(intervalType == 2) { //hours
                intervalTypeRang    = 23;
            } else if(intervalType == 3) { //days
                intervalTypeRang    = 60;
            } else if(intervalType == 4) { //week
                intervalTypeRang    = 8;
            }
            
            var optionList  = '<select name="reminder_time" id="reminder_time" class="form-control">';
            for(var i = 1; i <= intervalTypeRang; i++) {
                optionList  += "<option value='"+i+"'>"+i+"</option>";
            }

            optionList      += "</select>";
            $("#setRemiderTimeDrop").html(optionList);
        }

        $('select[name="reminder_interval"]').on('change', function(){       
            if( $(this).has('option').length > 0 ) {
                remiderTimeData();
            }
        })
        remiderTimeData();
        //set reminder end
        
        // calendar code start 
        var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;
         
        var calendar = $('#event_calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            },
            selectable: true,
            selectHelper: true,
            select: function (start, end, allDay) {

                var started     = start;
                var ended       = end;

                var sd = new Date(started);
                var started_date_str = sd.toDateString()+","+sd.getHours()+":"+sd.getMinutes();
                var appoint_start_date  = (sd.getMonth()+1)+"/"+sd.getDate()+"/"+sd.getFullYear();

                //put "started" into function intensionally
                var ed = new Date(started);
                var end_date = ed.toDateString()+","+ed.getHours()+":"+ed.getMinutes();
                var appoint_end_date  = (ed.getMonth()+1)+"/"+ed.getDate()+"/"+ed.getFullYear();
                $("#start_date").val(appoint_start_date);
                $("#end_date").val(appoint_end_date);

                console.log("start date time : "+started.toISOString());
                console.log("end date time : "+end_date);
                
                $('#fc_create').click();

                $(".antosubmit").on("click", function () {
                    var title = $("#title").val();
                    if (end) {
                        ended = end;
                    }
                    categoryClass = $("#event_type").val();
                    var color = $('input[name=color]:checked').val();
                    if (title) {
                        calendar.fullCalendar('renderEvent', {
                            title: title,
                            start: started,
                            end: end,
                            color: color,
                            allDay: allDay
                        },
                                true // make the event "stick"
                                );
                    }

                    $('#title').val('');

                    calendar.fullCalendar('unselect');

                    $('.antoclose').click();

                    return false;
                });
            },
            editable: true,
            events: [{
                    title: 'All Day Event',
                    color: '#3498DB',
                    start: new Date(y, m, 1)
                }, {
                    title: 'Long Event',
                    color: '#E74C3C',
                    start: new Date(y, m, d - 5),
                    end: new Date(y, m, d - 2)
                }, {
                    title: 'Meeting',
                    color: '#35aa47',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false
                }, {
                    title: 'Lunch',
                    color: '#b06ccc',
                    start: new Date(y, m, d + 14, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false
                }, {
                    title: 'Birthday Party',
                    color: '#36c6d3',
                    start: new Date(y, m, d + 1, 19, 0),
                    end: new Date(y, m, d + 1, 22, 30),
                    allDay: false
                }, {
                    title: 'Click for Google',
                    color: '#36c6d3',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/'
                }]
        });
        // calendar code end 


    });
</script>
@endpush
