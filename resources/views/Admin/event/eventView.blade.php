@extends('Admin.master_layout.master')

@section('title', 'Edit Event')

@section('breadcum')
     / Edit Event
@endsection

@push('styles')
<link href="{{ asset('resources/admin-assets/css/daterangepicker.css') }}" rel="stylesheet">

<link href="{{ asset('resources/admin-assets/css/iCheck/skins/square/blue.css') }}" rel="stylesheet">

<link href="{{ asset('resources/admin-assets/css/editor/font-awesome.min.css') }}" rel="stylesheet">

<link href="{{ asset('resources/admin-assets/css/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
@endpush

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <!-- <div class="x_title">
                    <h4>Edit Event</h4>
                    <div class="clearfix"></div>
                </div> -->
                <div class="col-md-6 col-sm-6 col-xs-6 x_title eventViewHeader">
                    <h3 class="text-bold"> {{ $event->event_title }} </h3>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 eventViewHeaderRight text-right">
                    <!-- {{ $eventTypeArr }} -->
                    <h4>
                        {{ date("D. M j, Y",strtotime($event->start_date)) }}
                    </h4>
                    <h5>
                        {{ $timeArr[$event->start_time] }} - {{ $timeArr[$event->end_time] }}
                    </h5>
                    
                </div>

            </div>
        </div> 
        
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel">
        <div class="">
            <div class="x_title">
                <h5 class="text-bold">Event Tools</h5>
                <div class="clearfix"></div>
            </div>
            <div>
                <table class="table table-borderless table-xs content-group-sm">
                    <tbody>
                        <tr>
                            <td class="border-top-none sorting_1">
                                <a class="task_name" href="javascript:void(0);">
                                    <i class="fa fa-book" aria-hidden="true"></i> Appointment
                                </a>
                            </td>
                            <td class="border-top-none text-right"></td>
                        </tr>
                        <tr>
                            <td class="sorting_1">
                                <a class="task_name" href="javascript:void(0);">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i> Check In
                                </a>
                            </td>
                            <td class="text-right"></td>
                        </tr>
                        <tr>
                            <td class="sorting_1">
                                <a class="task_name" href="javascript:void(0);">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i> Check List
                                </a>
                            </td>
                            <td class="text-right"></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>

@endsection

@push('scripts')

<script src="{{ asset('resources/admin-assets/js/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/daterangepicker.js') }}"></script>
<script src="{{ asset('resources/admin-assets/css/iCheck/icheck.min.js') }}"></script>

<!-- Bootstrap Colorpicker -->
<script src="{{ asset('resources/admin-assets/css/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>

<script type="application/javascript">

    $(document).ready(function(){

        $('.eventTypeOther').css('display','none');

        $('#start_date').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#end_date').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $(function () {
            $('#color_code').colorpicker();
        });
    });
</script>
@endpush
