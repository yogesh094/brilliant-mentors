@extends('Admin.master_layout.master')

@section('title', 'Edit Event')

@section('breadcum')
     / Edit Event
@endsection

@push('styles')
<link href="{{ asset('resources/admin-assets/css/daterangepicker.css') }}" rel="stylesheet">

<link href="{{ asset('resources/admin-assets/css/iCheck/skins/square/blue.css') }}" rel="stylesheet">

<link href="{{ asset('resources/admin-assets/css/editor/font-awesome.min.css') }}" rel="stylesheet">

<link href="{{ asset('resources/admin-assets/css/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
@endpush

@section('content')

<div class="row">
    <div class="x_panel">
        <div class="col-md-12 col-sm-12 col-xs-12">
        @if(count($errors))
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your request.
                <br/>
            </div>
        @endif
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="box-primary">
                <div class="x_title">
                    <h4>Edit Event</h4>
                    <div class="clearfix"></div>
                </div>
                {!! Form::open(array('url' => ('event/store'),'method'=>'POST', 'files'=>true)) !!}
                    <div class="x_content">

                        {{ Form::hidden('id', $event->id) }}

                        <!-- color -->
                        <!-- <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                Color
                            </label>
                            <span class="pull-left input-group demo2">
                                <input type="text" value="{{ $event->color_code }}" name="color_code" class="form-control" id="color_code" />
                                <span class="input-group-addon"><i></i></span>
                            </span>
                            <span class="text-danger" >{{ $errors->first('color_code') }}</span>
                        </div> -->

                        <!-- event title -->
                        <div class="form-group col-md-12 col-sm-12 col-xs-12 ">
                            <label class="col-md-6 col-sm-6 col-xs-12 control-label">
                                Event Title
                            </label>
                            {{ Form::text('event_title', isset($event->event_title)?$event->event_title:'',array('class' => 'form-control','placeholder' => 'Event Title', 'id' => 'event_title')) }}

                            <span class="text-danger" >{{ $errors->first('event_title') }}</span>
                        </div>

                        <!-- color -->
                        <!-- <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                Color
                            </label>
                            <span class="pull-left input-group demo2">
                                <input type="text" value="#e01ab5" name="color_code" class="form-control" id="color_code" />
                                <span class="input-group-addon"><i></i></span>
                            </span>
                            <span class="text-danger" >{{ $errors->first('color_code') }}</span>
                        </div> -->

                        <!-- Event type -->
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                Event Type
                            </label>
                            {{ Form::select('event_type', $eventTypeArr ,isset($event->event_type)?$event->event_type:0,['id' => 'event_type','class' => 'form-control','placeholder' => 'Select Event Type']) }}

                            
                            {{ Form::text('event_type_other', isset($event->event_type_other)?$event->event_type_other:'',array('class' => 'form-control eventTypeOther','placeholder' => 'Other Event Type', 'id' => 'event_type_other')) }}

                            <span class="text-danger" >{{ $errors->first('event_type') }}</span>
                        </div>

                        <!-- Budget Target -->
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                Target Budget
                            </label>
                            {{ Form::text('target_budget', isset($event->target_budget)?$event->target_budget:0,array('class' => 'form-control','placeholder' => 'Target Budget', 'id' => 'target_budget')) }}

                            <span class="text-danger" >{{ $errors->first('target_budget') }}</span>
                        </div>

                        <!-- Start Date -->
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                Start Date
                            </label>
                            {{ Form::text('start_date', isset($event->start_date)?date("m-d-Y",strtotime($event->start_date)):'',array('class' => 'form-control','placeholder' => 'Start Date', 'id' => 'start_date')) }}

                            <span class="text-danger" >{{ $errors->first('start_date') }}</span>
                        </div>

                        <!-- Start Time -->
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                Start Time
                            </label>
                            {{ Form::select('start_time', $timeArr,isset($event->start_time)?$event->start_time:null,['id' => 'start_time','class' => 'form-control','placeholder' => 'Select Start Time']) }}
                            
                            <span class="text-danger" >{{ $errors->first('start_time') }}</span>
                        </div>

                        <!-- End Date -->
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                End Date
                            </label>
                            {{ Form::text('end_date', isset($event->end_date)?date("m-d-Y",strtotime($event->end_date)):'',array('class' => 'form-control','placeholder' => 'End Date', 'id' => 'end_date')) }}

                            <span class="text-danger" >{{ $errors->first('end_date') }}</span>
                        </div>

                        <!-- End Time -->
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label class="col-md-12 col-sm-12 col-xs-12 control-label">
                                End Time
                            </label>
                            {{ Form::select('end_time', $timeArr,isset($event->end_time)?$event->end_time:null,['id' => 'end_time','class' => 'form-control','placeholder' => 'Select End Time']) }}

                            <span class="text-danger" >{{ $errors->first('end_time') }}</span>
                        </div>

                        <!-- submit -->
                        <div class="box-footer col-md-12 col-sm-12 col-xs-12 col-md-offset-1 col-sm-offset-1">
                            {!! Form::submit('Submit', array('class' => 'btn btn-primary submit','name' => 'submit_event' )) !!}
                            <a class="btn btn-default btn-close" href="{{ URL::to('/event') }}">Cancel</a>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div> 
    </div>
</div>

@endsection

@push('scripts')

<script src="{{ asset('resources/admin-assets/js/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('resources/admin-assets/js/daterangepicker.js') }}"></script>
<script src="{{ asset('resources/admin-assets/css/iCheck/icheck.min.js') }}"></script>

<!-- Bootstrap Colorpicker -->
<script src="{{ asset('resources/admin-assets/css/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>

<script type="application/javascript">

    $(document).ready(function(){

        $('.eventTypeOther').css('display','none');

        $('#start_date').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#end_date').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $(function () {
            $('#color_code').colorpicker();
        });
    });
</script>
@endpush
